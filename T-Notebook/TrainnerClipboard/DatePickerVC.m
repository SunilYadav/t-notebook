//
//  DatePickerVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "DatePickerVC.h"

@interface DatePickerVC ()

@property (nonatomic , weak) IBOutlet  UIDatePicker                     *datePicker;

@end

@implementation DatePickerVC
@synthesize isFromParQ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
    [tmpdt setDateFormat:kAppDateFormat];
    
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    NSDate * bDate = [tmpdt dateFromString:self.dateString];
    
    
    if (bDate != nil) {
        [self.datePicker setDate:bDate];
    }
    else {
        [self.datePicker setDate:[NSDate date]];
    }	

    
}


//---------------------------------------------------------------------------------------------------------------

-(IBAction) getDate: (id)sender {
    @try {
        NSDate * dt = self.datePicker.date;
        NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
        [dateformatter setDateFormat:kAppDateFormat];
        self.date1=[dateformatter stringFromDate:dt];
        [self.datePickDelegate getDateFormPicker :self.date1:self.tag1];
    }
    @catch (NSException * e) {
        
    }
    @finally {
        
    }
}

//---------------------------------------------------------------------------------------------------------------

- (CGSize)contentSizeForViewInPopoverView {
    return CGSizeMake(300, 216);
}


//--------------------------------------------------------------------------------------------------

-(void)done:(id) sender {
    
    @try {
        
        NSDate * dt = self.datePicker.date;
        NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
        [dateformatter setDateFormat:kAppDateFormat];
        self.date1=[dateformatter stringFromDate:dt];
        [self.datePickDelegate getDateFormPicker :self.date1:self.tag1];
        [self.datePickDelegate doneWithDate];
    }
    @catch (NSException * e) {
        
    }
    @finally {
        
    }
}

//---------------------------------------------------------------------------------------------------------------

-(void)currentDate: (id) sender {
    @try {
        NSDate * today= [NSDate date];
        self.datePicker.date=today;
        NSDate * dt = self.datePicker.date;
        NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
        [dateformatter setDateFormat:kAppDateFormat];
        
        _date1=[dateformatter stringFromDate:dt];
        
        [self.datePickDelegate getDateFormPicker:self.date1 :self.tag1];
    }
    @catch (NSException * e) {
        
    }
    @finally {
        
    }
    
}


//---------------------------------------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
