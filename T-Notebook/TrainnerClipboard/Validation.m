//
//  Validation.m
//  ValidationClasses
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "Validation.h"
#import "HelperClass.h"


@implementation Validation


+ (BOOL) isEmpty:(NSString *)value {
    BOOL success = YES;
    if ([value isEqual:[NSNull null]]) {
        value = @"";
    }
    value = allTrim(value);
    if ([value length] == 0) {
        success = NO;
    }
    return success;
}

//----------------------------------------------------------------------------------

+ (BOOL) isValidEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//----------------------------------------------------------------------------------

+ (BOOL) isValidNumber:(NSString *)number {
    NSString *numberRegex = @"^[0-9]+$";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:number];
}

//----------------------------------------------------------------------------------

+ (BOOL) isValidPhoneNumber:(NSString *)phoneNumber {
    BOOL isValidNo = [self isValidNumber:phoneNumber];
    if (isValidNo && [phoneNumber length]==10) {
        return YES;
    }
    else
    {
        return NO;
    }
}

//----------------------------------------------------------------------------------

+ (BOOL) isAlphaNumeric:(NSString *)value {
    NSString *valueRegex = @"^[a-zA-Z0-9]*$";
    NSPredicate *valueTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", valueRegex];
    return [valueTest evaluateWithObject:value];
}

//----------------------------------------------------------------------------------

+ (BOOL) isValidUrl:(NSString *)url {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:url];
}

//----------------------------------------------------------------------------------

+ (BOOL) isValidZipCode:(NSString *)zipCode{
    if([[NSNumberFormatter alloc] numberFromString:zipCode] == NULL || [zipCode length] != 6 ) {
        return NO;
    }
    return YES;
}


@end
