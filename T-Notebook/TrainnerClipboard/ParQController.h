//
//  ParQController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParQquestionView.h"
#import "DatePickerVC.h"
#import "SignatureVC.h"

@protocol PARQViewControllerViewDelegate

- (void) displayClientID:(NSString *)clientID :(NSString *)clientName;
-(void)presentCamera;
-(void)setEmailIdForClient:(NSString *)emailId;
-(void)hideOptionsViewIfDisplayed;
-(void)setDataSavePending;
-(void)setDataSavePendingWithValues:(BOOL)withValues;
-(void)noValuesEnteredIsSafeToExit:(BOOL)canExit;

@end


@interface ParQController : UIViewController <UIScrollViewDelegate,UIPopoverControllerDelegate,DateParQDelegate,UITextFieldDelegate,ParQQuestionViewDelegate,signatureViewDelegate,UIImagePickerControllerDelegate>{

}


@property (nonatomic , weak) IBOutlet         UIScrollView                          *scrollVew;
@property (nonatomic , weak) IBOutlet        UIButton                               *btnProfilePic;
@property (nonatomic , weak) IBOutlet        UIButton                               *btnSaveParq;
@property (nonatomic , weak) IBOutlet        UIImageView                           *imgViewAstric1;
@property (nonatomic , weak) IBOutlet        UIImageView                           *imgVIewAstric2;
@property (nonatomic , weak) IBOutlet        UIImageView                           *imgviewAstric3;
@property (nonatomic , weak) IBOutlet        UIImageView                           *imgviewAstric4;

@property (nonatomic , strong)  DatePickerVC                                        *datePicker;
@property (nonatomic, strong)  UIPopoverController                                  *popOverControllerForGallery;
@property (nonatomic , strong) UIPopoverController                                  *pop;

@property (nonatomic , strong) NSString                                              *strClientId;

@property (nonatomic) BOOL                                                          setAsLandscape;

@property (nonatomic , strong) id <PARQViewControllerViewDelegate>   parqDelegate;

-(void)setUpLandscapeOrientation;
-(void)setupPortraitOrientation;
- (void)saveProfileImageWithClientId:(NSString *) clientId;
-(void)checkIfAnyOftheDataIsPresent;

//SUNIL For save par-q data on click home button
-(BOOL)saveParqViewData;
@end
