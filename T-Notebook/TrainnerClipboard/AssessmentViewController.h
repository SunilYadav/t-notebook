//
//  AssessmentViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Show listing of Assesment sheets


#import <UIKit/UIKit.h>

@protocol AssessmentDelegate <NSObject>

-(void)hideOptionsViewIfShowing;
-(void)updateLasteEditedSheetForClientID:(NSString*)strClientId;

@end


@interface AssessmentViewController : BaseViewController{
    

}

@property (nonatomic, strong) NSString                                 * strClientID;
@property (nonatomic, strong) NSString                                 * strClientName;
@property (nonatomic , weak) IBOutlet         UIScrollView              *scrollViewAssesmet;
@property (nonatomic , weak) IBOutlet         UIButton            *btnPhotos;
@property (nonatomic , weak) IBOutlet         UIButton            *btnAssessment;
@property (nonatomic , weak) IBOutlet         UIScrollView      *scrollViewData;
@property (nonatomic , weak)  IBOutlet        UIView               *viewPhotos;
@property (nonatomic , weak)  IBOutlet        UIScrollView      *scrollViewPhotos;
@property (nonatomic) NSInteger                                        currentSheet;
@property (nonatomic) NSInteger                                        currentPage;
@property (nonatomic) BOOL                                             isLandscapeSet;

@property (nonatomic , strong) id <AssessmentDelegate>     assmntDelegate;


- (void) saveAssessmentData;
-(NSData *) getScreenImageData ;

- (IBAction)btnEditAssessmentSheetNameClicked:(id)sender;
-(IBAction)deleteClientAssessmentSheet:(id)sender;


-(void)changeTheButtonTitles;
-(void)setupLandscapeOrientation;
-(void)setupPortraitOrientation;

@end
