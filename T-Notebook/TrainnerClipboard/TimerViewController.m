//
//  TimerViewController.m
//  T-Notebook
//
//  Created by WLI on 01/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//



#import "TimerViewController.h"
#import "TimerManager.h"
#import "PlayListVC.h"
#import "AudioPlayer.h"

#define kSetTag            501
#define kCircuitTag        502
#define kSetRestTag        503
#define kWorkTag           504
#define kRestTag           505
#define kPresetTag         5000

@interface TimerViewController ()<TimerDelegate> {
    
    TimerManager                        *timeManager;
   
}

@property (nonatomic , weak) IBOutlet   UIImageView     *imgVewBackground;

// Timeer Title Label

@property (nonatomic , weak) IBOutlet  UILabel          *lblSetsTitle;
@property (nonatomic , weak) IBOutlet  UILabel          *lblCircuiTitle;
@property (nonatomic , weak) IBOutlet  UILabel          *lblSessionTitle;
@property (nonatomic , weak) IBOutlet  UILabel          *lblWorkTitle;
@property (nonatomic , weak) IBOutlet  UILabel          *lblRestTitle;
@property (nonatomic , weak) IBOutlet  UILabel          *lblSetRestTitle;

// Timeer Label

@property (nonatomic , weak) IBOutlet      UILabel      *lblSets;
@property (nonatomic , weak) IBOutlet      UILabel      *lblSessionTime;
@property (nonatomic , weak) IBOutlet      UILabel      *lblCircuits;
@property (nonatomic , weak) IBOutlet      UILabel      *lblCenterTime;
@property (nonatomic , weak) IBOutlet      UILabel      *lblWork;
@property (nonatomic , weak) IBOutlet      UILabel      *lblRest;
@property (nonatomic , weak) IBOutlet      UILabel      *lblSetRest;
@property (nonatomic , weak) IBOutlet      UILabel      *lblMiliseconds;
@property (nonatomic , weak) IBOutlet      UILabel       *lblPhaseName;

@property (nonatomic , weak) IBOutlet      UIButton     *btnSet;
@property (nonatomic , weak) IBOutlet      UIButton     *btnCircuits;
@property (nonatomic , weak) IBOutlet      UIButton     *btnSetRest;
@property (nonatomic , weak) IBOutlet      UIButton     *btnWork;
@property (nonatomic , weak) IBOutlet      UIButton     *btnRest;

@property (weak, nonatomic) IBOutlet UIButton *btnPreset1;
@property (weak, nonatomic) IBOutlet UIButton *btnPreset2;
@property (weak, nonatomic) IBOutlet UIButton *btnPreset3;
@property (weak, nonatomic) IBOutlet UIButton *btnPreset4;
@property (weak, nonatomic) IBOutlet UIButton *btnPreset5;




@property (nonatomic , weak) IBOutlet      UIImageView   *bigTimerImage;

@property (nonatomic , weak) IBOutlet      UIButton     *btnStart;
@property (nonatomic , weak) IBOutlet      UIButton     *btnAdd;
@property (nonatomic , weak) IBOutlet      UIButton     *btnMinus;
@property (nonatomic , weak) IBOutlet      UIButton     *btnStop;


@property (nonatomic , weak) IBOutlet      UIImageView      *imgvewSetSel;
@property (nonatomic , weak) IBOutlet      UIImageView      *imgVewCircuitSel;
@property (nonatomic , weak) IBOutlet      UIImageView      *imgvewSerRSel;
@property (nonatomic , weak) IBOutlet      UIImageView     *imgvewWrkSel;
@property (nonatomic , weak) IBOutlet      UIImageView      *imgvewrestSel;

@property (nonatomic , weak) IBOutlet      UIView           *vewSets;
@property (nonatomic , weak) IBOutlet      UIView           *vewSession;
@property (nonatomic , weak) IBOutlet      UIView           *vewCircuit;
@property (nonatomic , weak) IBOutlet      UIView           *vewSetRest;
@property (nonatomic , weak) IBOutlet      UIView           *vewWork;
@property (nonatomic , weak) IBOutlet      UIView           *VewRest;
@property (nonatomic , weak) IBOutlet      UIView           *viewCenterTimer;
@property (nonatomic , weak) IBOutlet      UIView           *vewPresets;
@property (nonatomic , weak) IBOutlet      UIImageView      *topNavigation;
@property (nonatomic , weak) IBOutlet      UILabel          *lblTrainer;
@property (nonatomic , weak) IBOutlet      UILabel          *lblNotebook;
@property (nonatomic , weak) IBOutlet      UIButton         *btnMusic;
@property (nonatomic , weak) IBOutlet      UILabel          *lblMusic;

//Sunil
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSets;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSessionTimer;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCircuit;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSetRest;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewWork;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewRest;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;




// Timer Constants

@property (nonatomic) BOOL                                 *setIsTimerPaused;
@property (nonatomic) BOOL                                 *setIsTimerRunning;

@property (nonatomic , strong) NSMutableArray                      *arrPresets;
@property (nonatomic) NSInteger                                    intSelectedPreset;
@property (nonatomic) NSInteger                                    intSelectedSource;
@property (nonatomic) NSInteger                                    totSets;
@property (nonatomic) NSInteger                                    totCircuits;
@property (nonatomic) NSInteger                                    intSetRestTime;
@property (nonatomic) NSInteger                                    intWorkTime;
@property (nonatomic) NSInteger                                    intRestTime;
@property (nonatomic) NSInteger                                    intNumberOfSets;
@property (nonatomic) NSInteger                                    intNumberOfCircuits;
@property (nonatomic) NSInteger                                    intTotalTime;
@property (nonatomic) NSInteger                                    intSelectedSet;
@property (nonatomic) NSInteger                                    intRemaningTimeForPhase;



@property (nonatomic, strong) NSTimer                             * timerInterVal;
@property (nonatomic, strong) NSString                            * strMilsecond;
@property (nonatomic) BOOL                                        isTimerRunning;
@property (nonatomic) BOOL                                        isTimerPaused;
@property (nonatomic) BOOL                                        isSourceSelected;
@property (nonatomic , strong)  NSTimer                           *minusTimer;
@property (nonatomic , strong)  NSTimer                           *plusTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblBack;


@end

@implementation TimerViewController

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Memory Management methods

//---------------------------------------------------------------------------------------------------------------

- (void)dealloc {
    
    self.arrPresets = nil;
    [TimerManager sharedInstance].timeDelegate = nil;
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark :  Custom Methods

//---------------------------------------------------------------------------------------------------------------


-(void) setupInitialSettings {
    
    [self.imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage"]];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kInterValNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(increaseTimerFromBackground :) name:kInterValNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTimerNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateValues:) name:kTimerNotification object:nil];
    
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]) {
        // DLOG(@" Presets data %@",[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]);
        self.arrPresets =[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData];
        
    } else {
        
        self.arrPresets = [[NSMutableArray alloc] init];
        for (int i =1; i <= 5; i++){
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:@"01",kTimer_Sets   ,@"08",kTimer_Circuits,@"00:00",kTimer_SetRest,@"00:00",kTimer_Work,@"00:20",kTimer_Rest,@"00:10",kTimer_SessionTime,[NSString stringWithFormat:@"Preset %d",i],kTimer_PresentName,[NSNumber numberWithBool:FALSE],@"isPausedState",nil];
            [self.arrPresets addObject:dicTemp];
        }
        [[NSUserDefaults standardUserDefaults]setObject:self.arrPresets forKey:kPresetsData];

        self.intSelectedPreset = 0;
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:self.intSelectedPreset] forKey:kSelectedPreset];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    
    // get the selected presets
    
    if([[NSUserDefaults standardUserDefaults]valueForKey:kSelectedPreset]) {
        self.intSelectedPreset =  [[[NSUserDefaults standardUserDefaults]valueForKey:kSelectedPreset] intValue];
        [self changeImageForPresets];
    }
    
    self.btnStop.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.btnStop.layer.cornerRadius = 5.0;
    self.btnStop.layer.borderWidth = 1.0;
    
    
    if(!appDelegate.appIsTimerRunning) {
        
        NSDictionary *dict = [self.arrPresets objectAtIndex:self.intSelectedPreset];
        
        if([[dict objectForKey:@"isPausedState"]boolValue]) {
            
            [self.lblSets setText:[self getValuesInFormat:[NSString stringWithFormat:@"%d",[[dict objectForKey:@"pausedSets"] intValue]]]];
            [self.lblCircuits setText:[self getValuesInFormat:[NSString stringWithFormat:@"%d",[[dict objectForKey:@"pauseCircuit"] intValue]]]];
            [self.lblSetRest setText:[dict objectForKey:kTimer_SetRest]];
            [self.lblWork setText:[dict objectForKey:kTimer_Work]];
            [self.lblRest setText:[dict objectForKey:kTimer_Rest]];
            [self.lblSessionTime setText:[dict objectForKey:@"PausesSessionTime"]];
            [self.lblCenterTime setText:[dict objectForKey:@"PausesCenterTime"]];
            self.totSets  = [[dict objectForKey:@"pausedSets"] intValue];
            self.totCircuits = [[dict objectForKey:@"pauseCircuit"] intValue];
            //[self setInTimeFormatForPausedState];
            
            timeManager.intRemaningTimeForPhase = [[dict objectForKey:@"pausedRemainingTime"] intValue];
            timeManager.intTotalTime = [[dict objectForKey:@"PausedTotalTime"]intValue];
            timeManager.strCenterTimer = self.lblCenterTime.text;
            timeManager.strSessionTimer = self.lblSessionTime.text;
            timeManager.totSets = self.totSets;
            timeManager.totCircuits =self.totCircuits;
            timeManager.strWork = self.lblWork.text;
            timeManager.strSets = self.lblSets.text;
            timeManager.strRest = self.lblRest.text;
            timeManager.strCircuit = self.lblCircuits.text;
            timeManager.strSetrest = self.lblSetRest.text;
            timeManager.intSelectedSet = [[dict objectForKey:@"pausedSelectedSet"] intValue];
            
            [self.lblMiliseconds setText:@"00"];
            [AppDelegate sharedInstance].appIsTimerPaused = TRUE;
            [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
            
            self.isSourceSelected = FALSE;
            self.intRestTime = timeManager.intRestTime;
            self.intWorkTime = timeManager.intWorkTime;
            self.intSetRestTime = timeManager.intSetRestTime;
            self.intTotalTime = timeManager.intTotalTime;
            
            if(timeManager.intSelectedSet == 1) {
                [self changePhaseOfTimerWithSelector:kisWork];
                
            } else if (timeManager.intSelectedSet == 2) {
                [self changePhaseOfTimerWithSelector:kisRest];
                
            } else if (timeManager.intSelectedSet == 3) {
                [self changePhaseOfTimerWithSelector:kisSetRest];
            }
            
            if([AppDelegate sharedInstance].appIsTimerPaused || [AppDelegate sharedInstance].appIsTimerRunning) {
                [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
            } else {
                [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
                
            }
            
        } else {

            [self.lblSets setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Sets]];
            [self.lblCircuits setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset] objectForKey:kTimer_Circuits]];
            [self.lblSetRest setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset] objectForKey:kTimer_SetRest]];
            [self.lblWork setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Work]];
            [self.lblRest setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Rest]];
            [self.lblSessionTime setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_SessionTime]];
            self.totSets = [[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Sets] intValue];
            self.totCircuits = [[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Circuits] intValue];
            [self.lblCenterTime setText:@"00:00:"];
            [self.lblMiliseconds setText:@"00"];
            
            timeManager.strCenterTimer = self.lblCenterTime.text;
            timeManager.strSessionTimer = self.lblSessionTime.text;
            timeManager.totSets = self.totSets;
            timeManager.totCircuits = self.totCircuits;
            timeManager.strWork = self.lblWork.text;
            timeManager.strRest= self.lblRest.text;
            timeManager.strSets = self.lblSets.text;
            timeManager.strSetrest = self.lblSetRest.text;
            timeManager.strCircuit= self.lblCircuits.text;
            
            self.intRestTime = timeManager.intRestTime;
            self.intWorkTime = timeManager.intWorkTime;
            self.intSetRestTime = timeManager.intSetRestTime;
            self.intTotalTime = timeManager.intTotalTime;
            
            [self.lblCenterTime setText:@"00:00"];
            
            self.isTimerRunning = FALSE;
            self.isSourceSelected = FALSE;
            [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
        }
        
    } else {
        
        self.lblCenterTime.text =  timeManager.strCenterTimer;
        self.lblSessionTime.text =  timeManager.strSessionTimer;
        self.totSets = timeManager.totSets;
        self.totCircuits = timeManager.totCircuits;
        self.lblWork.text =  timeManager.strWork;
        self.lblRest.text =  timeManager.strRest;
        self.lblSetRest.text =  timeManager.strSetrest;
        self.lblSets.text =  timeManager.strSets;
        self.lblCircuits.text =  timeManager.strCircuit;
        self.intRestTime =  timeManager.intSetRestTime;
        self.intWorkTime =  timeManager.intWorkTime;
        self.intRestTime =  timeManager.intRestTime;
        self.intTotalTime =  timeManager.intTotalTime;

    }
}

//-----------------------------------------------------------------------

- (void) setSoundUrls {
    [self setupPlayerArray:@"Session_Start"];          // Session Start soundFile
    [self setupPlayerArray:@"Session_Complition"];     // Session Complete soundFile
    [self setupPlayerArray:@"Work_Start"];             // Work Start soundFile
    [self setupPlayerArray:@"Complete_IntervalTimer"]; // Timer complete SoundFile
    [self setupPlayerArray:@"LapInsert_StopWatch"];    // Lap Insert SoundFile
    [self setupPlayerArray:@"TimerCompleted"];         // Timer Completed SoundFile
}

//-----------------------------------------------------------------------

- (void)setupPlayerArray : (NSString *)strSoundFileName {
    
    AudioPlayer * player;
    [AppDelegate sharedInstance].arrPlayers = [[NSMutableArray alloc]init];
    // Session Start soundFile
    NSString * audioPAth = [[NSBundle mainBundle] pathForResource:strSoundFileName ofType:@"wav"];
    NSURL * pathUrl=nil;
    
    @try {
        if ([FileUtility fileExists:audioPAth]) {
            pathUrl= [NSURL fileURLWithPath:audioPAth];
        }
        else{
            pathUrl= nil;
        }
        
        if(player == nil){
            player = [[AudioPlayer alloc]init];
            player.apDelegate = (id)self;
            if (pathUrl != nil) {
                [player setAudioFromPathUrl:pathUrl];
                [[AppDelegate sharedInstance].arrPlayers addObject:player];
            }
            player = nil;
        }
        
    }
    @catch (NSException *exception) {
        // // DLOG(@"Error: %@",[exception description]);
    }
}

//---------------------------------------------------------------------------------------------------------------

- (NSString *)getStringInTimeFormat : (NSNumber *)timeInterVal {
    
    NSString * strTime = nil;
    int totalsecond = [timeInterVal intValue];
    int sec = totalsecond % 60;
    int min = (totalsecond / 60) % 60;
    int hour = totalsecond / 3600;
    
    NSString *hr;
    NSString *mints;
    NSString *secnd;
    
    if(sec < 10) {
        if(sec<0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",sec] componentsSeparatedByString:@"-" ];
            secnd = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            secnd = [NSString stringWithFormat:@"0%d",sec];
        }
    }else {
        secnd = [NSString stringWithFormat:@"%d",sec];
    }
    
    if(min < 10) {
        if(min <0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",min] componentsSeparatedByString:@"-" ];
            secnd = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            mints = [NSString stringWithFormat:@"0%d",min];
        }
    } else {
        mints = [NSString stringWithFormat:@"%d",min];
    }
    
    if(hour < 10) {
        if(hour < 0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",hour] componentsSeparatedByString:@"-" ];
            secnd = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            hr = [NSString stringWithFormat:@"0%d",hour];
        }
    } else {
        hr = [NSString stringWithFormat:@"%d",hour];
    }
    
    if(hour > 0){
        strTime = [NSString stringWithFormat:@"%@:%@:%@",hr,mints,secnd];
    } else {
        strTime = [NSString stringWithFormat:@"%@:%@",mints,secnd];
    }

    return strTime;
}

//---------------------------------------------------------------------------------------------------------------

- (void) increaseValue {
    
    self.arrPresets =  [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]];
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:[self.arrPresets objectAtIndex:self.intSelectedPreset]];
    [dict1 setObject:[NSNumber numberWithBool:FALSE] forKey:@"isPausedState"];
    [self.arrPresets replaceObjectAtIndex:self.intSelectedPreset withObject:dict1];
    [[NSUserDefaults standardUserDefaults]setObject:self.arrPresets forKey:kPresetsData];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    // DLOG(@"increaseValue");
    NSString * strText = @"";
    int val;
    UILabel * lbl = nil;
    if (self.intSelectedSource == kSetTag || self.intSelectedSource == kCircuitTag) {
        if (self.intSelectedSource == kSetTag) {
            strText = self.lblSets.text;
            if (self.totSets + 1 <= 99) {
                self.totSets++;
            }
        }
        else if (self.intSelectedSource == kCircuitTag) {
           
            strText = self.lblCircuits.text;
            if (self.totCircuits + 1 <= 99) {
                self.totCircuits++;
            }
        }
        
        val = [strText intValue];
        
        if (val + 1 <= 99) {
            val++;
        }
        if (val<=9) {
            strText = [NSString stringWithFormat:@"0%d",val];
            if(self.intSelectedSource == kSetTag) {
                [self.lblSets setText:strText];
            } else if (self.intSelectedSource == kCircuitTag) {
                [self.lblCircuits setText:strText];
            }

        }
        else {
            strText = [NSString stringWithFormat:@"%d",val];
            if(self.intSelectedSource == kSetTag) {
                [self.lblSets setText:strText];
            } else if (self.intSelectedSource == kCircuitTag) {
                [self.lblCircuits setText:strText];
            }

        }
        
    }
    else {
        if (self.intSelectedSource == kSetRestTag) {
            
            strText = self.lblSetRest.text;
        }
        else if (self.intSelectedSource == kWorkTag) {
           
            strText = self.lblWork.text;
        }
        else if (self.intSelectedSource == kRestTag) {
            
            strText = self.lblRest.text;
        }
        
        NSArray * arrStr = [strText componentsSeparatedByString:@":"];
        
        int mm1 = 0;
        int ss1 = 0;
        int hh1 = 0;
        
        if ([arrStr count] == 2) {
            mm1 = [[arrStr objectAtIndex:0] intValue];
            ss1 = [[arrStr objectAtIndex:1] intValue];
        }
        else {
            hh1 = [[arrStr objectAtIndex:0] intValue];
            mm1 = [[arrStr objectAtIndex:1] intValue];
            ss1 = [[arrStr objectAtIndex:2] intValue];
        }
        
        ss1++;
        if (ss1 + 1 > 60) {
            ss1 = 0;
            mm1++;
            if (mm1+1 > 60) {
                mm1--;
                return;
            }
        }
        
        strText = @"";
        if (hh1 > 0) {
            if (hh1<=9) {
                strText = [strText stringByAppendingFormat:@"0%d:",hh1];
            }
            else {
                strText = [strText stringByAppendingFormat:@"%d:",hh1];
            }
            
        }
        
        if (mm1<=9) {
            strText = [strText stringByAppendingFormat:@"0%d:",mm1];
        }
        else {
            strText = [strText stringByAppendingFormat:@"%d:",mm1];
        }
        
        if (ss1<=9) {
            strText = [strText stringByAppendingFormat:@"0%d",ss1];
        }
        else {
            strText = [strText stringByAppendingFormat:@"%d",ss1];
        }
        
    }
    [lbl setText:strText];
    if(self.intSelectedSource == kSetRestTag) {
        [self.lblSetRest setText:strText];
    } else if (self.intSelectedSource == kWorkTag) {
        [self.lblWork setText:strText];
    } else if (self.intSelectedSource == kRestTag) {
        [self.lblRest setText:strText];
    }
    
    [self setTotalTime];

}

//---------------------------------------------------------------------------------------------------------------

- (void) decreaseValue {
    
    self.arrPresets =  [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]];
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:[self.arrPresets objectAtIndex:self.intSelectedPreset]];
    [dict1 setObject:[NSNumber numberWithBool:FALSE] forKey:@"isPausedState"];
    [self.arrPresets replaceObjectAtIndex:self.intSelectedPreset withObject:dict1];
    [[NSUserDefaults standardUserDefaults]setObject:self.arrPresets forKey:kPresetsData];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString * strText = @"";
    int val;
    UILabel * lbl = nil;
    if (self.intSelectedSource == kSetTag || self.intSelectedSource == kCircuitTag) {
        if (self.intSelectedSource == kSetTag) {
            strText = self.lblSets.text;
            if (self.totSets - 1 > 0) {
                self.totSets--;
            }
        }
        else if (self.intSelectedSource == kCircuitTag) {
            strText = self.lblCircuits.text;
            if (self.totCircuits - 1 > 0) {
                self.totCircuits--;
            }
        }
        
        val = [strText intValue];
        
        if (val - 1 > 0) {
            val--;
        }
        
        if (val<=9) {
            strText = [NSString stringWithFormat:@"0%d",val];
            if(self.intSelectedSource == kSetTag) {
                [self.lblSets setText:strText];
                timeManager.strSets = self.lblSets.text;
            } else if (self.intSelectedSource == kCircuitTag) {
                [self.lblCircuits setText:strText];
                timeManager.strCircuit = self.lblCircuits.text;
            }
        }
        else {
            strText = [NSString stringWithFormat:@"%d",val];
            if(self.intSelectedSource == kSetTag) {
                [self.lblSets setText:strText];
                timeManager.strSets = self.lblSets.text;

            } else if (self.intSelectedSource == kCircuitTag) {
                [self.lblCircuits setText:strText];
                timeManager.strCircuit = self.lblCircuits.text;
            }

            
        }
    }
    else {
        if (self.intSelectedSource == kSetRestTag) {
            strText = self.lblSetRest.text;
        }
        else if (self.intSelectedSource == kWorkTag) {
          
            strText = self.lblWork.text;
        }
        else if (self.intSelectedSource == kRestTag) {
           
            strText = self.lblRest.text;
        }
        
        NSArray * arrStr = [strText componentsSeparatedByString:@":"];
        
        int hh1 = 0;
        int mm1 = 0;
        int ss1 = 0;
        
        if ([arrStr count] == 2) {
            mm1 = [[arrStr objectAtIndex:0] intValue];
            ss1 = [[arrStr objectAtIndex:1] intValue];
        }
        else {
            hh1 = [[arrStr objectAtIndex:0] intValue];
            mm1 = [[arrStr objectAtIndex:1] intValue];
            ss1 = [[arrStr objectAtIndex:2] intValue];
        }
        
        if (ss1 - 1 >= 0) {
            ss1--;
        }
        else {
            if (mm1-1 >= 0) {
                ss1 = 59;
                mm1 --;
            }
            else {
                if (hh1-1 >= 0) {
                    mm1 = 59;
                    hh1--;
                }
                else {
                    return;
                }
            }
        }
        strText = @"";
        if (hh1>0) {
            if (hh1<=9) {
                strText = [NSString stringWithFormat:@"0%d:",hh1];
            }
            else {
                strText = [NSString stringWithFormat:@"%d:",hh1];
            }
        }
        
        if (mm1<=9) {
            strText = [strText stringByAppendingFormat:@"0%d:",mm1];
        }
        else {
            strText = [strText stringByAppendingFormat:@"%d:",mm1];
        }
        
        if (ss1<=9) {
            strText = [strText stringByAppendingFormat:@"0%d",ss1];
        }
        else {
            strText = [strText stringByAppendingFormat:@"%d",ss1];
        }
      
    }
    [lbl setText:strText];
    
    if(self.intSelectedSource == kSetRestTag) {
        [self.lblSetRest setText:strText];
       timeManager.strSetrest = self.lblSetRest.text;
    } else if (self.intSelectedSource == kWorkTag) {
        [self.lblWork setText:strText];
        timeManager.strWork = self.lblWork.text;
    } else if (self.intSelectedSource == kRestTag) {
        [self.lblRest setText:strText];
        timeManager.strRest = self.lblRest.text;
    }

    [self setTotalTime];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)changeImageForPresets {
    
    NSInteger selPreset = kPresetTag + self.intSelectedPreset + 1;
    
    for (int i = 5001; i <= 5005; i++) {
        if(i == selPreset) {
            UIButton *btn = (UIButton *) [self.view viewWithTag:i];
            [btn setBackgroundImage:[UIImage imageNamed:@"Preset_Selected"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        } else {
            UIButton *btn = (UIButton *) [self.view viewWithTag:i];
            [btn setBackgroundImage:[UIImage imageNamed:@"Preset_Normal"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)disableControlls {
    
    [self.btnAdd setUserInteractionEnabled:FALSE];
    [self.btnMinus setUserInteractionEnabled:FALSE];
    for (int i = 5001; i <= 5005; i++) {
        UIButton *btn = (UIButton *) [self.view viewWithTag:i];
        [btn setUserInteractionEnabled:FALSE];
    }
}

//---------------------------------------------------------------------------------------------------------------

-(void)enableControlls {
    [self.btnAdd setUserInteractionEnabled:TRUE];
    [self.btnMinus setUserInteractionEnabled:TRUE];
    
    for (int i = 5001; i <= 5005; i++) {
        UIButton *btn = (UIButton *) [self.view viewWithTag:i];
        [btn setUserInteractionEnabled:TRUE];
    }

}


//---------------------------------------------------------------------------------------------------------------

- (void)setupFontsForScreen {
    
    // DLOG(@"Setfont called");
    
    [self.lblSets setFont:[UIFont fontWithName:kGotham_XLight size:55]];
    [self.lblSessionTime setFont:[UIFont fontWithName:kGotham_XLight size:55]];
    [self.lblCircuits setFont:[UIFont fontWithName:kGotham_XLight size:55]];
    [self.lblCenterTime setFont:[UIFont fontWithName:kGotham_XLight size:73]];
    [self.lblWork setFont:[UIFont fontWithName:kGotham_XLight size:37]];
    [self.lblRest setFont:[UIFont fontWithName:kGotham_XLight size:37]];
    [self.lblSetRest setFont:[UIFont fontWithName:kGotham_XLight size:37]];
    
    [self.lblPhaseName setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:20]];
    [self.lblSetsTitle setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    [self.lblSetRestTitle setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    [self.lblSessionTitle setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    [self.lblWorkTitle setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    [self.lblRestTitle setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    [self.lblSetRestTitle setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    [self.lblCircuiTitle  setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:17]];
    
    for (int i = 5001; i <= 5005; i++) {
        UIButton *btn = (UIButton *) [self.view viewWithTag:i];
        [btn.titleLabel setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:16]];
    }
 
}

//---------------------------------------------------------------------------------------------------------------

- (void)longPressForAdding:(UILongPressGestureRecognizer*)gesture {
    
    if(self.isSourceSelected) {
        if(gesture.state == UIGestureRecognizerStateBegan) {
            _plusTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(increaseValue) userInfo:nil repeats:YES];
        }
        if ( gesture.state == UIGestureRecognizerStateEnded ) {
           [self.plusTimer invalidate];
        }
   }
}

//---------------------------------------------------------------------------------------------------------------


- (void)longPressForMinus:(UILongPressGestureRecognizer*)gesture {
    
    if(self.isSourceSelected) {
        if(gesture.state == UIGestureRecognizerStateBegan) {
            _minusTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(decreaseValue) userInfo:nil repeats:YES];
        }
        
        if ( gesture.state == UIGestureRecognizerStateEnded ) {
            [self.minusTimer invalidate];
        }
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void) storeRemainingTimeinPhaseForPreset {
    
    NSMutableArray *presetData = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]];
    
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:[presetData objectAtIndex:self.intSelectedPreset]];
    [dict1 setObject:[NSNumber numberWithBool:TRUE] forKey:@"isPausedState"];
    [dict1 setObject:[NSNumber numberWithInteger:timeManager.totCircuits] forKey:@"pauseCircuit"];
    [dict1 setObject:[NSNumber numberWithInteger:timeManager.totSets] forKey:@"pausedSets"];
    [dict1 setObject:timeManager.strSessionTimer forKey:@"PausesSessionTime"];
    [dict1 setObject:[NSNumber numberWithInteger:timeManager.intSelectedSet] forKey:@"pausedSelectedSet"];
    [dict1 setObject:[NSNumber numberWithInteger:timeManager.intTotalTime] forKey:@"PausedTotalTime"];
    [dict1 setObject:timeManager.strCenterTimer forKey:@"PausesCenterTime"];
    [dict1 setObject:[NSNumber numberWithInteger:timeManager.intRemaningTimeForPhase] forKey:@"pausedRemainingTime"];
    [presetData replaceObjectAtIndex:self.intSelectedPreset withObject:dict1];
    
    [[NSUserDefaults standardUserDefaults]setObject:presetData forKey:kPresetsData];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // DLOG(@"Presets paused state %@",presetData);
    presetData = nil;
    dict1 = nil;
}

//-----------------------------------------------------------------------
- (void)localizedControls {
    
    UIFontDescriptor * fontD = [self.lblTrainer.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold
                                ];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language isEqualToString:@"en"] || [language isEqualToString:@"zh-Hans"] || [language isEqualToString:@"nb"]) {
        [self.lblTrainer setFont:[UIFont fontWithDescriptor:fontD size:24.0]];
        [self.lblNotebook setFont:[UIFont fontWithName:@"Proxima Nova Alt" size:24.0]];
    }
    
    
    self.lblNotebook.text = NSLocalizedString(self.lblNotebook.text, nil);
    /*CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblNotebook.text font:self.lblNotebook.font height:21];
    [self.lblNotebook setFrame:CGRectMake(self.lblNotebook.frame.origin.x, self.lblNotebook.frame.origin.y, rect.size.width + 5, self.lblNotebook.frame.size.height)];*/
    
    
    self.lblTrainer.text = NSLocalizedString(self.lblTrainer.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblTrainer.text font:self.lblTrainer.font height:21];
//    [self.lblTrainer setFrame:CGRectMake(self.lblTrainer.frame.origin.x, self.lblTrainer.frame.origin.y, rect.size.width + 5, self.lblTrainer.frame.size.height)];
    
    self.lblCircuiTitle.text = NSLocalizedString(self.lblCircuiTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblCircuiTitle.text font:self.lblCircuiTitle.font height:21];
//    [self.lblCircuiTitle setFrame:CGRectMake(self.lblCircuiTitle.frame.origin.x, self.lblCircuiTitle.frame.origin.y, rect.size.width + 5, self.lblCircuiTitle.frame.size.height)];
    
    self.lblSetsTitle.text =  NSLocalizedString(self.lblSetsTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblSetsTitle.text font:self.lblSetsTitle.font height:21];
//    [self.lblSetsTitle setFrame:CGRectMake(self.lblSetsTitle.frame.origin.x, self.lblSetsTitle.frame.origin.y, rect.size.width + 5, self.lblSetsTitle.frame.size.height)];
    
    self.lblRestTitle.text =  NSLocalizedString(self.lblRestTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblRestTitle.text font:self.lblRestTitle.font height:21];
//    [self.lblRestTitle setFrame:CGRectMake(self.lblRestTitle.frame.origin.x, self.lblRestTitle.frame.origin.y, rect.size.width + 5, self.lblRestTitle.frame.size.height)];
    
     self.lblCircuiTitle.text =  NSLocalizedString(self.lblCircuiTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblCircuiTitle.text font:self.lblCircuiTitle.font height:21];
//    [self.lblCircuiTitle setFrame:CGRectMake(self.lblCircuiTitle.frame.origin.x, self.lblCircuiTitle.frame.origin.y, rect.size.width + 5, self.lblCircuiTitle.frame.size.height)];
    
    self.lblWorkTitle.text =  NSLocalizedString(self.lblWorkTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblWorkTitle.text font:self.lblWorkTitle.font height:21];
//    [self.lblWorkTitle setFrame:CGRectMake(self.lblWorkTitle.frame.origin.x, self.lblWorkTitle.frame.origin.y, rect.size.width + 5, self.lblWorkTitle.frame.size.height)];
    
     self.lblSetRestTitle.text =  NSLocalizedString(self.lblSetRestTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblSetRestTitle.text font:self.lblSetRestTitle.font height:21];
//    [self.lblSetRestTitle setFrame:CGRectMake(self.lblSetRestTitle.frame.origin.x, self.lblSetRestTitle.frame.origin.y, rect.size.width + 5, self.lblSetRestTitle.frame.size.height)];
    
    self.lblSessionTitle.text =  NSLocalizedString(self.lblSessionTitle.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblSessionTitle.text font:self.lblSessionTitle.font height:21];
//    [self.lblSessionTitle setFrame:CGRectMake(self.lblSessionTitle.frame.origin.x, self.lblSessionTitle.frame.origin.y, rect.size.width + 5, self.lblSessionTitle.frame.size.height)];
    
    self.lblPhaseName.text =  NSLocalizedString(self.lblPhaseName.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblPhaseName.text font:self.lblPhaseName.font height:21];
//    [self.lblPhaseName setFrame:CGRectMake(self.lblPhaseName.frame.origin.x, self.lblPhaseName.frame.origin.y, rect.size.width + 5, self.lblPhaseName.frame.size.height)];
    
    
    [self.btnStop setTitle:NSLocalizedString(self.btnStop.titleLabel.text, nil) forState:0];
    [self.btnPreset1 setTitle:NSLocalizedString(self.btnPreset1.titleLabel.text, nil) forState:0];
    [self.btnPreset2 setTitle:NSLocalizedString(self.btnPreset2.titleLabel.text, nil) forState:0];
    [self.btnPreset3 setTitle:NSLocalizedString(self.btnPreset3.titleLabel.text, nil) forState:0];
    [self.btnPreset4 setTitle:NSLocalizedString(self.btnPreset4.titleLabel.text, nil) forState:0];
    [self.btnPreset5 setTitle:NSLocalizedString(self.btnPreset5.titleLabel.text, nil) forState:0];
    
    self.lblMusic.text =  NSLocalizedString(self.lblMusic.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblMusic.text font:self.lblMusic.font height:21];
//    [self.lblMusic setFrame:CGRectMake(self.lblMusic.frame.origin.x, self.lblMusic.frame.origin.y, rect.size.width + 5, self.lblMusic.frame.size.height)];

    self.lblBack.text =  NSLocalizedString(self.lblBack.text, nil);
//    rect = [[AppDelegate sharedInstance] getWidth:self.lblBack.text font:self.lblBack.font height:21];
//    [self.lblBack setFrame:CGRectMake(self.lblBack.frame.origin.x, self.lblBack.frame.origin.y, rect.size.width + 5, self.lblBack.frame.size.height)];

}


//---------------------------------------------------------------------------------------------------------------

#pragma mark: Timer Methods

//---------------------------------------------------------------------------------------------------------------


- (void) getTimerTimes {
    
    NSArray * arrTime = [self.lblSetRest.text componentsSeparatedByString:@":"];
    self.intSetRestTime = [[arrTime objectAtIndex:0] intValue] * 60;
    self.intSetRestTime = self.intSetRestTime + [[arrTime objectAtIndex:1] intValue];

    timeManager.intSetRestTime = self.intSetRestTime;
    
    arrTime = [self.lblWork.text componentsSeparatedByString:@":"];
    self.intWorkTime = [[arrTime objectAtIndex:0] intValue] * 60;
    self.intWorkTime = self.intWorkTime + [[arrTime objectAtIndex:1] intValue];
    
    timeManager.intWorkTime = self.intWorkTime;
    
    arrTime = [self.lblRest.text componentsSeparatedByString:@":"];
    self.intRestTime = [[arrTime objectAtIndex:0] intValue] * 60;
    self.intRestTime = self.intRestTime + [[arrTime objectAtIndex:1] intValue];
    
    timeManager.intRestTime = self.intRestTime;
    
    self.intNumberOfSets = [self.lblSets.text intValue];
    self.intNumberOfCircuits = [self.lblCircuits.text intValue];
    
    timeManager.intNumberOfSets = self.intNumberOfSets;
    timeManager.intNumberOfCircuits = self.intNumberOfCircuits;
    timeManager.strSessionTimer = self.lblSessionTime.text;
    
    arrTime = [self.lblSessionTime.text componentsSeparatedByString:@":"];

    
    int mm = 0;
    
    if ([arrTime count] == 3) {  // total time in hours
        
        mm = [[arrTime objectAtIndex:1] intValue];
        mm = ([[arrTime objectAtIndex:0] intValue] * 60) + mm;
        
        self.intTotalTime = mm * 60;
        self.intTotalTime = self.intTotalTime + [[arrTime objectAtIndex:2] intValue];
        
    } else { // total time in mins
        
        self.intTotalTime = [[arrTime objectAtIndex:0] intValue] * 60;
        self.intTotalTime = self.intTotalTime + [[arrTime objectAtIndex:1] intValue];
    }
    
    
  
    self.arrPresets =  [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]];
    NSDictionary *dict = [self.arrPresets objectAtIndex:self.intSelectedPreset];
    if([[dict objectForKey:@"isPausedState"] boolValue]) {
        
        timeManager.intRemaningTimeForPhase = [[dict objectForKey:@"pausedRemainingTime"] intValue];
        timeManager.intTotalTime = [[dict objectForKey:@"PausedTotalTime"]intValue];
        timeManager.intSelectedSet = [[dict objectForKey:@"pausedSelectedSet"] intValue];

    } else {
        timeManager.intTotalTime = self.intTotalTime;
        appDelegate.remainTimeInterVal_Timer = [NSNumber numberWithInteger:self.intTotalTime];
        appDelegate.actualTimeInterVal_Timer = [NSNumber numberWithInteger:self.intTotalTime];

    }
    

}


//---------------------------------------------------------------------------------------------------------------

- (void) setTotalTime {
    
    NSInteger dCycle = [self.lblSets.text intValue];
    NSInteger dCircuits = [self.lblCircuits.text intValue];
    
    NSArray * arrStr = [self.lblSetRest.text componentsSeparatedByString:@":"];
    NSInteger dCycleRestMin = [[arrStr objectAtIndex:0] intValue];
    NSInteger dCycleRestSec = [[arrStr objectAtIndex:1] intValue];
    
    arrStr = [self.lblWork.text componentsSeparatedByString:@":"];
    NSInteger dWorkMin = [[arrStr objectAtIndex:0] intValue];
    NSInteger dWorkSec = [[arrStr objectAtIndex:1] intValue];
    
    arrStr = [self.lblRest.text componentsSeparatedByString:@":"];
    NSInteger dRestMin = [[arrStr objectAtIndex:0] intValue];
    NSInteger dRestSec = [[arrStr objectAtIndex:1] intValue];
    
    NSInteger totdCycleRest = (dCycleRestMin * 60) + dCycleRestSec;
    NSInteger totCycleRest = totdCycleRest * (dCircuits - 1);
    
    NSInteger totdWork = (dWorkMin * 60) + dWorkSec;
    NSInteger totdRest = (dRestMin * 60) + dRestSec;
    NSInteger totworkTime = totdWork *dCycle;
    totworkTime*=dCircuits;
    
    NSInteger totrestTime = totdRest * ((dCircuits * dCycle) - 1);
    NSInteger totCircuitsTime = totrestTime + totworkTime;
    NSInteger finalTime = totCycleRest + totCircuitsTime; // All in Seconds
    
    NSInteger sec = finalTime%60;
    NSInteger min = finalTime/60;
    NSInteger hour = min/60;
    min = min%60;
    
    
    NSString * result =  @"";
    
    if (hour != 0) {
        if (hour<10) {
            result = [result stringByAppendingFormat:@"0%ld:",(long)hour];
        }
        else {
            result = [result stringByAppendingFormat:@"%ld:",(long)hour];
        }
    }
    
    if (min<10) {
        result = [result stringByAppendingFormat:@"0%ld:",(long)min];
    }
    else {
        result = [result stringByAppendingFormat:@"%ld:",(long)min];
    }
    if (sec<10) {
        result = [result stringByAppendingFormat:@"0%ld",(long)sec];
    }
    else {
        result = [result stringByAppendingFormat:@"%ld",(long)sec];
    }
    
    [self.lblSessionTime setText:result];
    [self.lblCenterTime setText:@"00:00:"];
    
    timeManager.intSelectedSet = 1;
    timeManager.strCenterTimer = self.lblCenterTime.text;
    timeManager.strSession = self.lblSessionTime.text;
    timeManager.intRemaningTimeForPhase = self.intWorkTime;
    
    timeManager.strWork = self.lblWork.text;
    timeManager.strRest = self.lblRest.text;
    timeManager.strSetrest = self.lblSetRest.text;
    timeManager.strCircuit = self.lblCircuits.text;
    timeManager.strSets = self.lblSets.text;
    timeManager.strCenterTimer = [NSString stringWithFormat:@"%@:", self.lblWork.text];
    timeManager.totSets = dCycle;
    timeManager.totCircuits = dCircuits;
    
    [self.bigTimerImage setImage:[UIImage imageNamed:@"Work-timer"]];
    [self.lblPhaseName setText:NSLocalizedString(@"Work", nil)];
    [self.lblCenterTime setText:@"00:00"];
    [self.lblMiliseconds setText:@"00"];
    
    NSArray * arrStr1 = [self.lblSessionTime.text componentsSeparatedByString:@":"];
    NSInteger dWorkMin1 = [[arrStr1 objectAtIndex:0] intValue];
    NSInteger dWorkSec1 = [[arrStr1 objectAtIndex:1] intValue];
    double totdWork1 = (dWorkMin1 * 60 * 60) + (dWorkSec1 * 60);
    
    [AppDelegate sharedInstance].intRemainingTimeOfPhase = self.intWorkTime;
    [AppDelegate sharedInstance].remainTimeInterVal_Timer = [NSNumber numberWithDouble:totdWork1];
    [AppDelegate sharedInstance].actualTimeInterVal_Timer = [NSNumber numberWithDouble:totdWork1];
    
    [self storeArrayValues];
}


//---------------------------------------------------------------------------------------------------------------

- (void) storeArrayValues {

    NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:self.lblSets.text,kTimer_Sets,self.lblCircuits.text,kTimer_Circuits,self.lblWork.text,kTimer_Work,self.lblSetRest.text,kTimer_SetRest,self.lblRest.text,kTimer_Rest,self.lblSessionTime.text,kTimer_SessionTime,[[self.arrPresets objectAtIndex:self.intSelectedPreset]valueForKey:kTimer_PresentName],kTimer_PresentName, nil];
    
    self.arrPresets = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:kPresetsData]];
    [self.arrPresets replaceObjectAtIndex:self.intSelectedPreset withObject:dicTemp];
    
    [[NSUserDefaults standardUserDefaults]setObject: self.arrPresets forKey:kPresetsData];
    [[NSUserDefaults standardUserDefaults]synchronize];

    
}

//---------------------------------------------------------------------------------------------------------------

- (void)updateValues:(NSNotification *)notification {
    
    NSDictionary * dicData = [notification userInfo];

    
    if([[dicData valueForKey:kTimerNotification_IsTimeCompleted]boolValue]){ // Timer is Completed
        
        [[AppDelegate sharedInstance]pausedTimer];
         [AppDelegate sharedInstance].remainTimeInterVal_Timer =  [AppDelegate sharedInstance].actualTimeInterVal_Timer;
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPlaying];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.btnStart setEnabled:TRUE];
        [[self.navigationItem.rightBarButtonItems objectAtIndex:1] setEnabled:TRUE];
        [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
        
    }else{ // Timer is Continue
        
        if(![[AppDelegate sharedInstance]isFromBackground]) {
            
            NSArray * arrStr1 = [timeManager.strSessionTimer componentsSeparatedByString:@":"];
            NSInteger dWorkMin1 = [[arrStr1 objectAtIndex:0] intValue];
            NSInteger dWorkSec1 = [[arrStr1 objectAtIndex:1] intValue];
            double totdWork1 = (dWorkMin1 * 60 * 60) + (dWorkSec1 * 60);
            
             [AppDelegate sharedInstance].actualTimeInterVal_Timer = [NSNumber numberWithDouble:totdWork1];
             [AppDelegate sharedInstance].remainTimeInterVal_Timer = [NSNumber numberWithDouble:totdWork1];
            
            if([AppDelegate sharedInstance].appIsTimerPaused)  {
                [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
            }
            
        }
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void) increaseTimerFromBackground : (NSNotification *) notification {
    
    NSNumber * number = [[notification userInfo]valueForKey:@"TimeElaps"];
    for (int i = 0; i<[number intValue]; i++) {
        [timeManager runTimer];
    }
    
    [timeManager setBlinkImage];
    [AppDelegate sharedInstance].remainTimeInterVal_Timer = [NSNumber numberWithInteger: timeManager.intTotalTime];
    [AppDelegate sharedInstance].actualTimeInterVal_Timer = [NSNumber numberWithInteger: timeManager.intTotalTime];
}

//---------------------------------------------------------------------------------------------------------------
/*
- (void)setInTimeFormatForPausedState {
    
    NSInteger dCycle = [self.lblSets.text intValue];
    NSInteger dCircuits = [self.lblCircuits.text intValue];
    
    NSArray * arrStr = [self.lblSetRest.text componentsSeparatedByString:@":"];
    NSInteger dCycleRestMin = [[arrStr objectAtIndex:0] intValue];
    NSInteger dCycleRestSec = [[arrStr objectAtIndex:1] intValue];
    
    arrStr = [self.lblWork.text componentsSeparatedByString:@":"];
    NSInteger dWorkMin = [[arrStr objectAtIndex:0] intValue];
    NSInteger dWorkSec = [[arrStr objectAtIndex:1] intValue];
    
    arrStr = [self.lblRest.text componentsSeparatedByString:@":"];
    NSInteger dRestMin = [[arrStr objectAtIndex:0] intValue];
    NSInteger dRestSec = [[arrStr objectAtIndex:1] intValue];
    
    NSInteger totdCycleRest = (dCycleRestMin * 60) + dCycleRestSec;
    NSInteger totCycleRest = totdCycleRest * (dCircuits - 1);
    
    NSInteger totdWork = (dWorkMin * 60) + dWorkSec;
    NSInteger totdRest = (dRestMin * 60) + dRestSec;
    NSInteger totworkTime = totdWork *dCycle;
    totworkTime*=dCircuits;
    
    NSInteger totrestTime = totdRest * ((dCircuits * dCycle) - 1);
    NSInteger totCircuitsTime = totrestTime + totworkTime;
    NSInteger finalTime = totCycleRest + totCircuitsTime; // All in Seconds
    
    NSInteger sec = finalTime%60;
    NSInteger min = finalTime/60;
    NSInteger hour = min/60;
    min = min%60;
    
    
    NSString * result =  @"";
    
    if (hour != 0) {
        if (hour<10) {
            result = [result stringByAppendingFormat:@"0%d:",hour];
        }
        else {
            result = [result stringByAppendingFormat:@"%d:",hour];
        }
    }
    
    if (min<10) {
        result = [result stringByAppendingFormat:@"0%d:",min];
    }
    else {
        result = [result stringByAppendingFormat:@"%d:",min];
    }
    if (sec<10) {
        result = [result stringByAppendingFormat:@"0%d",sec];
    }
    else {
        result = [result stringByAppendingFormat:@"%d",sec];
    }
    
    //[self.lblSessionTime setText:result];
    //timeManager.strSession = self.lblSessionTime.text;
    
//    NSArray * arrStr1 = [self.lblSessionTime.text componentsSeparatedByString:@":"];
//    NSInteger dWorkMin1 = [[arrStr1 objectAtIndex:0] intValue];
//    NSInteger dWorkSec1 = [[arrStr1 objectAtIndex:1] intValue];
//    double totdWork1 = (dWorkMin1 * 60 * 60) + (dWorkSec1 * 60);
//    

}
*/
//---------------------------------------------------------------------------------------------------------------

- (NSString*)getValuesInFormat:(NSString *) value {
    
    NSString *newVal;
    NSInteger val = [value intValue];
    
    if (val <= 9) {
        newVal = [NSString stringWithFormat:@"0%ld",(long)val];
        
    } else {
        newVal = [NSString stringWithFormat:@"%ld",(long)val];
    }

    return newVal;
}




//---------------------------------------------------------------------------------------------------------------

#pragma mark : Timer manager delegate methods

//---------------------------------------------------------------------------------------------------------------

-(void)changeValuesOnTimer:(NSString*)changedValue ForTheSelector:(NSString *)selector  {
    
    
    if([selector isEqualToString:kisWork]) {
        
        [self.lblWork setText:changedValue];
        
    } else if ([selector isEqualToString:kisRest]) {
        
        [self.lblRest setText:changedValue];
        
    } else if ([selector isEqualToString:kisSet]) {
        
        [self.lblSets setText:changedValue];
        
    }else if ([selector isEqualToString:kisSetRest]) {
        
        [self.lblSetRest setText:changedValue];
        
    } else if ([selector isEqualToString:kisCircuit]) {
        
        [self.lblCircuits setText:changedValue];
        
    } else if ([selector isEqualToString:kisCenterTime]) {
        
        changedValue = [changedValue substringToIndex:([changedValue length] - 1)];
        [self.lblCenterTime setText:changedValue];
        
    } else if ([selector isEqualToString:kisSessionTime]) {
        // DLOG(@"session time %@",changedValue);
        [self.lblSessionTime setText:changedValue];
    }

}

//---------------------------------------------------------------------------------------------------------------

-(void)changePhaseOfTimerWithSelector:(NSString *) selector {
    
    
    if([selector isEqualToString:kisWork]) {
        
        [self.bigTimerImage setImage:[UIImage imageNamed:@"Work-timer"]];
        [self.lblPhaseName setText:NSLocalizedString(@"Work", nil)];
        
    } if([selector isEqualToString:kisRest]) {
        
         [self.bigTimerImage setImage:[UIImage imageNamed:@"Rest-timer"]];
        [self.lblPhaseName setText:NSLocalizedString(@"Rest", nil)];
        
    } if([selector isEqualToString:kisSetRest]) {
        
         [self.bigTimerImage setImage:[UIImage imageNamed:@"Big-timer"]];
        [self.lblPhaseName setText:NSLocalizedString(@"Set Rest", nil)];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)timerTimeCompleteResetValues {
    
    [AppDelegate sharedInstance].remainTimeInterVal_Timer = [NSNumber numberWithDouble:0];
    [AppDelegate sharedInstance].actualTimeInterVal_Timer = [NSNumber numberWithDouble:0];
    
    NSMutableArray *presetData = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]];
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:[presetData objectAtIndex:self.intSelectedPreset]];
    [dict1 setObject:[NSNumber numberWithBool:FALSE] forKey:@"isPausedState"];
    [presetData replaceObjectAtIndex:self.intSelectedPreset withObject:dict1];
    [[NSUserDefaults standardUserDefaults]setObject:presetData forKey:kPresetsData];
    [[NSUserDefaults standardUserDefaults]synchronize];


    [self.lblSets setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Sets]];
    [self.lblCircuits setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset] objectForKey:kTimer_Circuits]];
    [self.lblSetRest setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset] objectForKey:kTimer_SetRest]];
    [self.lblWork setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Work]];
    [self.lblRest setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Rest]];
    [self.lblSessionTime setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_SessionTime]];
    self.totSets = [[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Sets] intValue];
    self.totCircuits = [[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Circuits] intValue];
    [self.lblCenterTime setText:@"00:00:"];
    [self.lblMiliseconds setText:@"00"];
    
    timeManager.strCenterTimer = self.lblCenterTime.text;
    timeManager.strSessionTimer= self.lblSessionTime.text;
    timeManager.totSets = self.totSets;
    timeManager.totCircuits =self.totCircuits;
    timeManager.strWork = self.lblWork.text;
    timeManager.strRest = self.lblRest.text;
    timeManager.strSets = self.lblSets.text;
    timeManager.strSetrest = self.lblSetRest.text;

    self.intRestTime =timeManager.intRestTime;
    self.intWorkTime = timeManager.intWorkTime;
    self.intRestTime = timeManager.intRestTime;
    self.intTotalTime = timeManager.intTotalTime;
    [self.lblCenterTime setText:@"00:00"];
    
    
    self.isTimerRunning = FALSE;
    self.isSourceSelected = FALSE;
    [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
   
    [[AppDelegate sharedInstance]pauseMusic];
    [self enableControlls];
    
    [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
    [self changePhaseOfTimerWithSelector:kisWork];
    
    presetData = nil;
    dict1 = nil;
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)setBlinkImagesFor:(NSString*)selector andType:(BOOL)forBlink {
    
    if([selector isEqualToString:kisWork]) {
        
        if(forBlink) {
            [self.bigTimerImage setImage:[UIImage imageNamed:@"BlinkWork"]];
        } else {
            [self.bigTimerImage setImage:[UIImage imageNamed:@"Work-timer"]];
        }
        
    }
    
    if([selector isEqualToString:kisRest]) {
        
        if(forBlink) {
            //BlinkRest
              [self.bigTimerImage setImage:[UIImage imageNamed:@"BlinkRest"]];
        } else {
             [self.bigTimerImage setImage:[UIImage imageNamed:@"Rest-timer"]];
        }
        
        
    }
    
    if([selector isEqualToString:kisSetRest]) {
        
        if(forBlink) {
             [self.bigTimerImage setImage:[UIImage imageNamed:@"BlinkSetRest"]];
        } else {
             [self.bigTimerImage setImage:[UIImage imageNamed:@"Big-timer"]];
        }
    }
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Action Methods

//---------------------------------------------------------------------------------------------------------------

-(IBAction)addButtonTapped:(id)sender {
    if(self.isSourceSelected) {
         [self increaseValue];
    }
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)minusButtonTapped:(id)sender {
    if(self.isSourceSelected) {
        [self decreaseValue];
    }
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)selectSourceToAddTime:(id)sender {
    
    
    // DLOG(@"Selected source tapped");
    
    if(![AppDelegate sharedInstance].appIsTimerRunning) {
        
        self.isSourceSelected = TRUE;
        NSInteger  sourceArrowTag = 0;
        UIButton *btnSource = (UIButton *) sender;
        if(btnSource.tag == kSetTag) {
            self.intSelectedSource = kSetTag;
            sourceArrowTag = 1;
        } else if (btnSource.tag == kCircuitTag) {
            self.intSelectedSource = kCircuitTag;
            sourceArrowTag = 2;
        } else if (btnSource.tag == kSetRestTag) {
            self.intSelectedSource = kSetRestTag;
            sourceArrowTag = 3;
        } else if (btnSource.tag == kWorkTag) {
            self.intSelectedSource = kWorkTag;
            sourceArrowTag = 4;
        } else if (btnSource.tag == kRestTag) {
            self.intSelectedSource = kRestTag;
            sourceArrowTag = 5;
        }
        
        NSInteger visibleArrow = 9000 + sourceArrowTag;
        for (int i =9001; i <= 9005; i++) {
            UIImageView *imgvew = (UIImageView*) [self.view viewWithTag:i];
            if(i== visibleArrow) {
                [imgvew setHidden:FALSE];
            } else {
                [imgvew setHidden:TRUE];
            }
            
        }
}
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)btnStartTapped:(id)sender {
    
    // DLOG(@"Btn Start Tapped");
    [[AppDelegate sharedInstance] performSelector:@selector(setSoundUrls) withObject:nil afterDelay:2.0f];
    
    self.isSourceSelected = FALSE;
    
    [AppDelegate sharedInstance].isAppPlayerPlaying = @"Pause";
    
        if([AppDelegate sharedInstance].appIsTimerRunning){ // Timer is running and user want's to pause it
            
            [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
            
            [self setIsTimerRunning:FALSE];
            [self setIsTimerPaused:TRUE];
            
            [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
            [AppDelegate sharedInstance].appIsTimerPaused = TRUE;
        
            [self.timerInterVal invalidate];
    
            [[AppDelegate sharedInstance] pausedTimer];
            [timeManager pauseTimer];
         //   [appDelegate pauseMusic];
             [AppDelegate sharedInstance].isAppPlayerPlaying = @"Play";
             [[AppDelegate sharedInstance] pauseMusic];
            
            [[NSUserDefaults standardUserDefaults] setValue:@"btnPause" forKey:@"btnPlayPause"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            //[self disableControlls];
            [self enableControlls];
            
            [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:kIsIntervalPlaying];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            //============ changes to restore the current Time =========================/
        
            [self storeRemainingTimeinPhaseForPreset];
        
            //================================================================//
            
        } else { // Timer is in paused state or not started yet
            
            if([AppDelegate sharedInstance].appIsTimerPaused){ // Check is timer is paused
                
                [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kIsIntervalPlaying];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self setIsTimerRunning:TRUE];
                [self setIsTimerPaused:FALSE];
                
                [AppDelegate sharedInstance].appIsTimerRunning = TRUE;
                [AppDelegate sharedInstance].appIsTimerPaused = FALSE;
                
                [self getTimerTimes];
                
                if(self.intWorkTime <=0) {
                    return;
                }
                
                [timeManager startTimer];
                [[AppDelegate sharedInstance] getRemainTime_Timer];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"btnPlay" forKey:@"btnPlayPause"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [appDelegate playMusic];
                [self disableControlls];
                
            } else { // User start timer very first time
                
                [self getTimerTimes];
                
                if(self.intWorkTime <= 0){ // check for work time is greater than 0.
                    return;
                    
                }else{ // User is entered proper time and now started
                    
                    [self hideSelectedSetImage];
                    [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
                     [self disableControlls];
                   [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kIsIntervalPlaying];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    [self setIsTimerRunning:TRUE];
                    [AppDelegate sharedInstance].appIsTimerRunning = TRUE;
                    if(![AppDelegate sharedInstance].appIsTimerPaused) {
                        
                       [self setIntSelectedSet:1]; // set the selected state
                       timeManager.intSelectedSet = 1;
                       self.intRemaningTimeForPhase = self.intWorkTime;
                       timeManager.intRemaningTimeForPhase = self.intWorkTime;
                       timeManager.strWork = self.lblWork.text;
                       timeManager.strRest = self.lblRest.text;
                       timeManager.strSetrest = self.lblSetRest.text;
                       timeManager.strCircuit = self.lblCircuits.text;
                       timeManager.strSets = self.lblSets.text;
                        
                        [AppDelegate sharedInstance].intRemainingTimeOfPhase = self.intWorkTime;
                        timeManager.strCenterTimer = [NSString stringWithFormat:@"%@:", self.lblWork.text];
                        [self.lblCenterTime setText:[NSString stringWithFormat:@"%@", self.lblWork.text]];
                        [self changePhaseOfTimerWithSelector:kisWork];
                        NSDictionary *dict = @{@"selector" : kisWork };
                        [[NSNotificationCenter defaultCenter] postNotificationName:kphaseImageNotification object:nil userInfo:dict];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"btnPlay" forKey:@"btnPlayPause"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [appDelegate playMusic];
                        
                    }
                    
                    [[AppDelegate sharedInstance] playSound:2];
                    [[AppDelegate sharedInstance] getRemainTime_Timer];
                    [timeManager startTimer];
                    
                }
            }
        }
  
}

//---------------------------------------------------------------------------------------------------------------

- (void)hideSelectedSetImage {
    
    for (int i =9001; i <= 9005; i++) {
        UIImageView *imgvew = (UIImageView *) [self.view viewWithTag:i];
        [imgvew setHidden:TRUE];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)presetButtomTapped:(id)sender {
    
    START_METHOD
    
   
    @try {
        
        UIButton *btn = (UIButton *) sender;
        
        //Sunil
        [self SetSelectedImageButton:btn];
        
        
        int preTag = (int) [btn tag] - kPresetTag -1;
        self.intSelectedPreset = preTag;
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:self.intSelectedPreset] forKey:kSelectedPreset];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self changeImageForPresets];
    
        NSMutableArray *aryData = [NSMutableArray array];
        aryData = [[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData];
        NSDictionary *dict = [aryData objectAtIndex:self.intSelectedPreset];
        
        if([[dict objectForKey:@"isPausedState"]boolValue]) {
            
            // DLOG(@"Paused state dict %@",dict);
            
            [self.lblSets setText:[self getValuesInFormat:[NSString stringWithFormat:@"%d",[[dict objectForKey:@"pausedSets"] intValue]]]];
            [self.lblCircuits setText:[self getValuesInFormat:[NSString stringWithFormat:@"%d",[[dict objectForKey:@"pauseCircuit"] intValue]]]];
            
            [self.lblSetRest setText:[dict objectForKey:kTimer_SetRest]];
            [self.lblWork setText:[dict objectForKey:kTimer_Work]];
            [self.lblRest setText:[dict objectForKey:kTimer_Rest]];
            [self.lblSessionTime setText:[dict objectForKey:@"PausesSessionTime"]];
            [self.lblCenterTime setText:[dict objectForKey:@"PausesCenterTime"]];
            self.totSets  = [[dict objectForKey:@"pausedSets"] intValue];
            self.totCircuits = [[dict objectForKey:@"pauseCircuit"] intValue];
            
            //[self setInTimeFormatForPausedState];
            
            timeManager.intRemaningTimeForPhase = [[dict objectForKey:@"pausedRemainingTime"] intValue];
            timeManager.intTotalTime = [[dict objectForKey:@"PausedTotalTime"]intValue];
            timeManager.strCenterTimer = self.lblCenterTime.text;
            timeManager.strSessionTimer = self.lblSessionTime.text;
            timeManager.totSets = self.totSets;
            timeManager.totCircuits =self.totCircuits;
            timeManager.strWork = self.lblWork.text;
            timeManager.strSets = self.lblSets.text;
            timeManager.strRest = self.lblRest.text;
            timeManager.strCircuit = self.lblCircuits.text;
            timeManager.strSetrest = self.lblSetRest.text;
            
            timeManager.intSelectedSet = [[dict objectForKey:@"pausedSelectedSet"] intValue];
            self.intSelectedSet = timeManager.intSelectedSet;
            
            NSString  *changedValue = [self.lblCenterTime.text substringToIndex:([self.lblCenterTime.text length] - 1)];
            [self.lblCenterTime setText:changedValue];
            
            [self.lblMiliseconds setText:@"00"];
            [AppDelegate sharedInstance].appIsTimerPaused = TRUE;
            [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
            self.isSourceSelected = FALSE;
            
            
            if(timeManager.intSelectedSet == 1) {
                [self changePhaseOfTimerWithSelector:kisWork];
                
            } else if (timeManager.intSelectedSet == 2) {
                [self changePhaseOfTimerWithSelector:kisRest];
                
            } else if (timeManager.intSelectedSet == 3) {
                [self changePhaseOfTimerWithSelector:kisSetRest];
            }
            
            if([AppDelegate sharedInstance].appIsTimerPaused || [AppDelegate sharedInstance].appIsTimerRunning) {
                [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
            } else {
                [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
                
            }
            
        } else {
            
            
            self.arrPresets =  [[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData];
            
            // DLOG(@"Selected Preset %@", [self.arrPresets objectAtIndex:self.intSelectedPreset]);
            [self.lblSets setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Sets]];
            [self.lblCircuits setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset] objectForKey:kTimer_Circuits]];
            [self.lblSetRest setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset] objectForKey:kTimer_SetRest]];
            [self.lblWork setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Work]];
            [self.lblRest setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Rest]];
            [self.lblSessionTime setText:[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_SessionTime]];
            self.totSets = [[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Sets] intValue];
            self.totCircuits = [[[self.arrPresets objectAtIndex:self.intSelectedPreset]objectForKey:kTimer_Circuits] intValue];
            [self.lblCenterTime setText:@"00:00:"];
            [self.lblMiliseconds setText:@"00"];
            
            timeManager.strCenterTimer = self.lblCenterTime.text;
            timeManager.strSessionTimer = self.lblSessionTime.text;
            timeManager.totSets = self.totSets;
            timeManager.totCircuits =self.totCircuits;
            timeManager.strWork = self.lblWork.text;
            timeManager.strSets = self.lblSets.text;
            timeManager.strRest = self.lblRest.text;
            timeManager.strCircuit = self.lblCircuits.text;
            timeManager.strSetrest = self.lblSetRest.text;
            
            self.intRestTime = timeManager.intRestTime;
            self.intWorkTime = timeManager.intWorkTime;
            self.intSetRestTime = timeManager.intSetRestTime;
            self.intTotalTime = timeManager.intTotalTime;
            [self.lblCenterTime setText:@"00:00"];
            
            
            [self.lblCenterTime setText:@"00:00"];
            self.isTimerRunning = FALSE;
            self.isSourceSelected = FALSE;
            [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
            [AppDelegate sharedInstance].appIsTimerPaused = FALSE;
            
            if([AppDelegate sharedInstance].appIsTimerPaused || [AppDelegate sharedInstance].appIsTimerRunning) {
                [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
            } else {
                [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
            }
            
            if(timeManager.intSelectedSet == 1) {
                [self changePhaseOfTimerWithSelector:kisWork];
                
            } else if (timeManager.intSelectedSet == 2) {
                [self changePhaseOfTimerWithSelector:kisRest];
                
            } else if (timeManager.intSelectedSet == 3) {
                [self changePhaseOfTimerWithSelector:kisSetRest];
            }

        }
        
        //Sunil
        [self SetSelectedImageButton:btn];
    }
    @catch (NSException *exception) {
        // NSLog(@"Ex: %@",exception);
    }
    
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)homeBtnTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)stopButtonTapped:(id)sender {

    // DLOG(@"Btn Stop Tapped");
    [[TimerManager sharedInstance]pauseTimer];
   // [AppDelegate sharedInstance].isAppPlayerPlaying = @"Play";
    [[AppDelegate sharedInstance] pauseMusic];
    [[AppDelegate sharedInstance] pausedTimer];
    
    
    self.arrPresets =  [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:kPresetsData]];
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:[self.arrPresets objectAtIndex:self.intSelectedPreset]];
    [dict1 setObject:[NSNumber numberWithBool:FALSE] forKey:@"isPausedState"];
    [self.arrPresets replaceObjectAtIndex:self.intSelectedPreset withObject:dict1];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.arrPresets forKey:kPresetsData];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self timerTimeCompleteResetValues];
    
    
    NSDictionary *dict = @{@"Time" : @"00:00:" , @"selector" : kisWork };
    [[NSNotificationCenter defaultCenter] postNotificationName:kDashboardNotification object:nil userInfo:dict];
    
    NSDictionary *dict2 = @{@"selector" : @"" };
    [[NSNotificationCenter defaultCenter] postNotificationName:kphaseImageNotification object:nil userInfo:dict2];

}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)musicButtonTapped:(id)sender {
    
    PlayListVC *playlistVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayListView"];
    [playlistVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [playlistVC setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:playlistVC animated:YES completion:^{
    }];

}

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        [self setUpLandscapeOrientation];
        
        //Sunil Invoke method fror set view
        [self actionChnageSkin:LANDSCAPE];
        
    } else {
        [self setupPotraitOrientation];
        
        //Sunil Invoke method fror set view
        [self actionChnageSkin:PORTRAIT];
    }

}

//-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
    
    [self.imgVewBackground setFrame:CGRectMake(0, 20, kLandscapeWidth, kLandscapeHeight)];
    [self.imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage_iPad"]];
    
    [self.topNavigation setFrame:CGRectMake(0, 20, kLandscapeWidth, 57)];
    [self.topNavigation setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];
    
    self.lblTrainer.text = NSLocalizedString(self.lblTrainer.text, nil);
    self.lblNotebook.text = NSLocalizedString(self.lblNotebook.text, nil);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTrainer.text font:self.lblTrainer.font height:self.lblTrainer.frame.size.height];
    self.lblTrainer.frame = CGRectMake(385, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblNotebook.text font:self.lblNotebook.font height:self.lblNotebook.frame.size.height];
    self.lblNotebook.frame = CGRectMake(self.lblTrainer.frame.origin.x + self.lblTrainer.frame.size.width , 23,rect.size.width + 5 ,48);
    
    //[self.lblTrainer setFrame:CGRectMake(380, 23,170 ,48 )];
    //[self.lblNotebook setFrame:CGRectMake(550, 23,170 ,48 )];
    
    
    [self.btnMusic setFrame:CGRectMake(kLandscapeWidth-66, 20, 66, 55)];
    [self.lblMusic setFrame:CGRectMake(kLandscapeWidth - 66, 54, 66, 21)];

    
    //top layout
    [self.vewSets setFrame:CGRectMake(84, 145, 160, 134)];
    [self.vewSession setFrame:CGRectMake(258, 145, 253, 133)];
    [self.vewCircuit setFrame:CGRectMake(524, 146, 160, 134)];
    
    // center layout
    [self.viewCenterTimer setFrame:CGRectMake(329, 316, 337, 346)];
    [self.btnAdd setFrame:CGRectMake(138, 401, 138, 55)];
    [self.btnMinus setFrame:CGRectMake(138, 464, 138, 55)];
    [self.btnStop setFrame:CGRectMake(138, 527, 138, 55)];
    
    //bottom layout
    [self.vewSetRest setFrame:CGRectMake(kLandscapeWidth-189-50, 140, 185, 189)];
    [self.vewWork setFrame:CGRectMake(kLandscapeWidth-189-50, 344, 185, 189)];
    [self.VewRest setFrame:CGRectMake(kLandscapeWidth-189-50, 548, 185, 189)]; //518
    [self.vewPresets setFrame:CGRectMake(90, kLandscapeHeight - 70, 594, 45)];
    
}


//-----------------------------------------------------------------------

-(void)setupPotraitOrientation {
    
    [self.imgVewBackground setFrame:CGRectMake(0, 20, kPotraitWidth, kPotraitHeight)];
    [self.imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage"]];

    [self.topNavigation setFrame:CGRectMake(0, 20, kPotraitWidth, 57)];
    [self.topNavigation setImage:[UIImage imageNamed:@"Top-Bar"]];
    
    
    self.lblTrainer.text = NSLocalizedString(self.lblTrainer.text, nil);
    self.lblNotebook.text = NSLocalizedString(self.lblNotebook.text, nil);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTrainer.text font:self.lblTrainer.font height:self.lblTrainer.frame.size.height];
    self.lblTrainer.frame = CGRectMake(255 + 35, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblNotebook.text font:self.lblNotebook.font height:self.lblNotebook.frame.size.height];
    self.lblNotebook.frame = CGRectMake(self.lblTrainer.frame.origin.x + self.lblTrainer.frame.size.width , 23,rect.size.width + 5 ,48);
    
    
  //  [self.lblTrainer setFrame:CGRectMake(255 + 30, 23,170 ,48 )];
  //  [self.lblNotebook setFrame:CGRectMake(424 +30, 23,170 ,48 )];
    
    
    [self.btnMusic setFrame:CGRectMake(kPotraitWidth - 66, 20, 66, 55)];
    [self.lblMusic setFrame:CGRectMake(kPotraitWidth - 66, 54, 66, 21)];
    

    
    [self.vewSets setFrame:CGRectMake(84, 145, 160, 134)];
    [self.vewSession setFrame:CGRectMake(258, 145, 253, 133)];
    [self.vewCircuit setFrame:CGRectMake(524, 146, 160, 134)];
    
    [self.viewCenterTimer setFrame:CGRectMake(329, 316, 337, 346)];
    [self.btnAdd setFrame:CGRectMake(138, 401, 138, 55)];
    [self.btnMinus setFrame:CGRectMake(138, 464, 138, 55)];
    [self.btnStop setFrame:CGRectMake(138, 527, 138, 55)];
    
    [self.vewSetRest setFrame:CGRectMake(84, 675, 185, 189)];
    [self.vewWork setFrame:CGRectMake(292, 675, 185, 189)];
    [self.VewRest setFrame:CGRectMake(499, 675, 185, 189)];
    [self.vewPresets setFrame:CGRectMake(90, 900, 594, 45)];
    
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : View Lifecycle mathods

//---------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    timeManager = [TimerManager sharedInstance];
    [self prefersStatusBarHidden];
    [self setNavigationBarButton];
    [[TimerManager sharedInstance] setTimeDelegate:self];
    
    for (int i =9001; i <= 9005; i++) {
        UIImageView *imgvew = (UIImageView *) [self.view viewWithTag:i];
        [imgvew setHidden:TRUE];
    }
    
    UILongPressGestureRecognizer *longPressAdd = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressForAdding:)];
    longPressAdd.minimumPressDuration = 0.5;
    longPressAdd.allowableMovement = 20;
    [self.btnAdd addGestureRecognizer:longPressAdd];
    
    UILongPressGestureRecognizer *longPressMinus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressForMinus:)];
    longPressMinus.minimumPressDuration = 0.5;
    longPressMinus.allowableMovement = 20;
    [self.btnMinus addGestureRecognizer:longPressMinus];
   
   
    
}

//---------------------------------------------------------------------------------------------------------------

-(void) viewWillAppear:(BOOL)animated {
    START_METHOD
    [super viewWillAppear:animated];
    [[TimerManager sharedInstance] setTimeDelegate:self];
    [self setupInitialSettings];
    [self  setupFontsForScreen];
    [self localizedControls];
    if([AppDelegate sharedInstance].appIsTimerRunning || [AppDelegate sharedInstance].appIsTimerPaused) {
        
        NSString *changedVal = timeManager.strCenterTimer;
        changedVal = [changedVal substringToIndex:([changedVal length] - 1)];
        [self.lblCenterTime setText:changedVal];
        
        self.lblSessionTime.text = timeManager.strSessionTimer;
        self.lblSets.text = timeManager.strSets;
        self.lblCircuits.text = timeManager.strCircuit;
        self.lblRest.text = timeManager.strRest;
        self.lblWork.text = timeManager.strWork;
        self.lblSetRest.text = timeManager.strSetrest;
        self.intNumberOfSets = timeManager.intNumberOfSets;
        self.intNumberOfCircuits = timeManager.intNumberOfCircuits;
        self.intRestTime = timeManager.intRestTime;
        self.intWorkTime =timeManager.intWorkTime;
        self.intRestTime =timeManager.intWorkTime;
        self.intTotalTime = timeManager.intTotalTime;
        
        if(timeManager.intSelectedSet == 1) {
            [self changePhaseOfTimerWithSelector:kisWork];
            
        } else if (timeManager.intSelectedSet == 2) {
            [self changePhaseOfTimerWithSelector:kisRest];
            
        } else if (timeManager.intSelectedSet == 3) {
            [self changePhaseOfTimerWithSelector:kisSetRest];
        }
        
        if([AppDelegate sharedInstance].appIsTimerPaused || [AppDelegate sharedInstance].appIsTimerRunning) {
            [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
        } else {
            [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
        }
    }
    
    [self enableControlls];
    
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [self setUpLandscapeOrientation];
        
        //Sunil Invoke method fror set view
        [self actionChnageSkin:LANDSCAPE];
 
    } else {
        [self setupPotraitOrientation];
        
        //Sunil Invoke method fror set view
        [self actionChnageSkin:PORTRAIT];
    }
    
    
    
}

//---------------------------------------------------------------------------------------------------------------

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self  setupFontsForScreen];
    
    if([AppDelegate sharedInstance].appIsTimerPaused || [AppDelegate sharedInstance].appIsTimerRunning) {
        [self.btnStart setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
    } else {
        [self.btnStart setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
    }
}


-(void)actionChnageSkin:(int )tag{
    MESSAGE(@"tag of button : %d",tag);
    //Sunil Background image dynamicaly -->
    
   
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    
    switch (tag) {
        case PORTRAIT:
            
            //<---
            [self.imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage"]];
            
            
            [self setViewChnagesOnColorPortraitScape:skin];
            
            
            break;
            
        case LANDSCAPE:
            
            //<---
            
            [self.imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage_iPad"]];
            
            [self setViewChnagesOnColorLandScape:skin];
            
            
            break;
            
            
        default:
            break;
    }
}

//Sunil
-(void)setViewChnagesOnColorPortraitScape:(int)skin{
    START_METHOD
    
    _lblPhaseName.textColor = [UIColor whiteColor];
    _lblCenterTime.textColor = [UIColor whiteColor];
    
    if(skin!=OLD_TYPE){
    [_btnPreset2 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    [_btnPreset3 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    [_btnPreset4 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    [_btnPreset5 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    }
    switch (skin) {
        case FIRST_TYPE:
                [_imgVewBackground setImage:[UIImage imageNamed:@"Default-Portrait.png"]];
                [self.topNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            _imgViewSets.image          = [UIImage imageNamed:@"sets_green-gray.png"];
            _imgViewSessionTimer.image  = [UIImage imageNamed:@"session-timer_gray-green.png"];
            _imgViewCircuit.image       = [UIImage imageNamed:@"sets_green-gray.png"];
            
            _imgViewSetRest.image       = [UIImage imageNamed:@"set-rest-timer_orange.png"];
            _imgViewWork.image          = [UIImage imageNamed:@"green_work_time_ellipse.png"];
            _imgViewRest.image          = [UIImage imageNamed:@"pink_rest_time_ellipse.png"];
            _bigTimerImage.image        = [UIImage imageNamed:@"Big-timer_orange.png"];
            
            
            [_btnHome setImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
             [_btnMusic setBackgroundImage:[UIImage imageNamed:@"music-btn1.png"] forState:UIControlStateNormal];
            
            
            
           [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"green_preset_button.png"] forState:UIControlStateNormal];
            
            [_btnAdd setBackgroundImage:[UIImage imageNamed:@"plus-gray.png"] forState:UIControlStateNormal];
            
              [_btnMinus setBackgroundImage:[UIImage imageNamed:@"minus-gray.png"] forState:UIControlStateNormal];
            
            
            [_btnStop setBackgroundColor:[UIColor colorWithRed:60/255.0f green:72/255.0f blue:77/255.0f alpha:1.0]];
       
            
            [_btnStop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            _lblTrainer.textColor = [UIColor whiteColor];
            _lblNotebook.textColor = kFirstSkin;
            _lblSets.textColor = [UIColor whiteColor];
            _lblSessionTime.textColor = [UIColor whiteColor];
            
            _lblCircuits.textColor = [UIColor whiteColor];
            _lblPhaseName.textColor = [UIColor whiteColor];
            _lblCenterTime.textColor = [UIColor whiteColor];
            _lblPhaseName.textColor = [UIColor whiteColor];
            
            _lblSetRest.textColor = [UIColor whiteColor];
            _lblSetRestTitle.textColor = kOrangeColor;
            _lblWork.textColor = [UIColor whiteColor];
            _lblWorkTitle.textColor = kFirstSkin;
            
            _lblRest.textColor = [UIColor whiteColor];
            _lblRestTitle.textColor = kThirdSkin;
            
            
            
            break;
            
        case SECOND_TYPE:
            
                [_imgVewBackground setImage:[UIImage imageNamed:@"Default-Portrait.png"]];
                [self.topNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            
            _imgViewSets.image          = [UIImage imageNamed:@"sets_gray.png"]; //sets_light-gray.png
            
            _imgViewSessionTimer.image  = [UIImage imageNamed:@"session-timer_gray.png"];//session-timer_light-gray.png
            
            _imgViewCircuit.image       = [UIImage imageNamed:@"sets_gray.png"];
            _imgViewSetRest.image       = [UIImage imageNamed:@"set-rest-timer_orange.png"];
            _imgViewWork.image          = [UIImage imageNamed:@"green_work_time_ellipse.png"];
            _imgViewRest.image          = [UIImage imageNamed:@"pink_rest_time_ellipse.png"];
            _bigTimerImage.image        = [UIImage imageNamed:@"Big-timer_orange.png"];

            
            
            
            [_btnHome setImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
             [_btnMusic setBackgroundImage:[UIImage imageNamed:@"music-btn1.png"] forState:UIControlStateNormal];
            [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"red_preset_button.png"] forState:UIControlStateNormal];
            
            [_btnAdd setBackgroundImage:[UIImage imageNamed:@"plus-gray.png"] forState:UIControlStateNormal];
            
            [_btnMinus setBackgroundImage:[UIImage imageNamed:@"minus-gray.png"] forState:UIControlStateNormal];
            
            
            [_btnStop setBackgroundColor:[UIColor colorWithRed:60/255.0f green:72/255.0f blue:77/255.0f alpha:1.0]];
            
            [_btnStop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            
            _lblTrainer.textColor = [UIColor whiteColor];
            _lblNotebook.textColor = ksecondSkin;
            _lblSets.textColor = [UIColor whiteColor];
            _lblSessionTime.textColor = [UIColor whiteColor];
            
            _lblCircuits.textColor = [UIColor whiteColor];
            
            _lblPhaseName.textColor = [UIColor whiteColor];
            
            _lblSetRest.textColor = [UIColor whiteColor];
            _lblSetRestTitle.textColor = kOrangeColor;
            _lblWork.textColor = [UIColor whiteColor];
            _lblWorkTitle.textColor = kFirstSkin;
            
            _lblRest.textColor = [UIColor whiteColor];
            _lblRestTitle.textColor = kThirdSkin;
            
            break;
            
            
        case THIRD_TYPE:
            
                [_imgVewBackground setImage:[UIImage imageNamed:@"Default4-Portrait.png"]];
                [self.topNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            
            _imgViewSets.image          = [UIImage imageNamed:@"sets_light-gray.png"];
            
            _imgViewSessionTimer.image  = [UIImage imageNamed:@"session-timer_light-gray.png"];
            
            _imgViewCircuit.image       = [UIImage imageNamed:@"sets_light-gray.png"];
            
            _imgViewSetRest.image       = [UIImage imageNamed:@"set-rest-timer_orange.png"];
            _imgViewWork.image          = [UIImage imageNamed:@"green_work_time_ellipse.png"];
            _imgViewRest.image          = [UIImage imageNamed:@"pink_rest_time_ellipse.png"];
            
            
            _bigTimerImage.image        =   [UIImage imageNamed:@"pink_set_rest_big_ellipse.png"];

            [_btnHome setImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
             [_btnMusic setBackgroundImage:[UIImage imageNamed:@"music-btn1.png"] forState:UIControlStateNormal];
            
             [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"pink_preset_button.png"] forState:UIControlStateNormal];
            
            [_btnMinus setBackgroundImage:[UIImage imageNamed:@"minus-white.png"] forState:UIControlStateNormal];
            
            [_btnAdd setBackgroundImage:[UIImage imageNamed:@"plus-white.png"] forState:UIControlStateNormal];
            
           [_btnStop setBackgroundColor:[UIColor whiteColor]];
            
            [_btnStop setTitleColor: kThirdSkin forState:UIControlStateNormal];

            
            _lblTrainer.textColor = [UIColor whiteColor];
            _lblNotebook.textColor = kThirdSkin;
            
            break;
            
            
        default:
            
                [_imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage"]];
                [self.topNavigation setImage:[UIImage imageNamed:@"Top-Bar"]];
            
            
//            _imgViewSets.image          = [UIImage imageNamed:@""];
//            _imgViewSessionTimer.image  = [UIImage imageNamed:@""];
//            _imgViewCircuit.image       = [UIImage imageNamed:@""];
//            _imgViewSetRest.image       = [UIImage imageNamed:@""];
//            _imgViewWork.image          = [UIImage imageNamed:@""];
//            _imgViewRest.image          = [UIImage imageNamed:@""];
//            [_btnHome setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            break;
    }
    
    END_METHOD
}

//Sunil
-(void)setViewChnagesOnColorLandScape:(int)skin{
    START_METHOD
    _lblPhaseName.textColor = [UIColor whiteColor];
    _lblCenterTime.textColor = [UIColor whiteColor];
    
    if(skin!=OLD_TYPE){
    [_btnPreset2 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    [_btnPreset3 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    [_btnPreset4 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    
    [_btnPreset5 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
    }
    
    if(skin==THIRD_TYPE){
        [_btnPreset2 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset3 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset4 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset5 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
    }
    
    switch (skin) {
        case FIRST_TYPE:
            [_imgVewBackground setImage:[UIImage imageNamed:@"Default-Landscape-ipad.png"]];
            [self.topNavigation setImage:[UIImage imageNamed:@"nav-bg_landscape.png"]];

            
            _imgViewSets.image          = [UIImage imageNamed:@"sets_green-gray.png"];
            _imgViewSessionTimer.image  = [UIImage imageNamed:@"session-timer_gray-green.png"];
            _imgViewCircuit.image       = [UIImage imageNamed:@"sets_green-gray.png"];
          
            _imgViewSetRest.image       = [UIImage imageNamed:@"set-rest-timer_orange.png"];
            _imgViewWork.image          = [UIImage imageNamed:@"green_work_time_ellipse.png"];
            _imgViewRest.image          = [UIImage imageNamed:@"pink_rest_time_ellipse.png"];
            
            _bigTimerImage.image        = [UIImage imageNamed:@"Big-timer_orange.png"];

            
            [_btnHome setImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            
             [_btnMusic setBackgroundImage:[UIImage imageNamed:@"music-btn1.png"] forState:UIControlStateNormal];
            
            [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"green_preset_button.png"] forState:UIControlStateNormal];
            
            [_btnAdd setBackgroundImage:[UIImage imageNamed:@"plus-gray.png"] forState:UIControlStateNormal];
            
            [_btnMinus setBackgroundImage:[UIImage imageNamed:@"minus-gray.png"] forState:UIControlStateNormal];
            
            
   [_btnStop setBackgroundColor:[UIColor colorWithRed:60/255.0f green:72/255.0f blue:77/255.0f alpha:1.0]];
            [_btnStop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            _lblTrainer.textColor = [UIColor whiteColor];
            _lblNotebook.textColor = kFirstSkin;
            _lblSets.textColor = [UIColor whiteColor];
            _lblSessionTime.textColor = [UIColor whiteColor];
            
            _lblCircuits.textColor = [UIColor whiteColor];
            _lblPhaseName.textColor = [UIColor whiteColor];
            _lblCenterTime.textColor = [UIColor whiteColor];
            _lblPhaseName.textColor = [UIColor whiteColor];
            
            _lblSetRest.textColor = [UIColor whiteColor];
            _lblSetRestTitle.textColor = kOrangeColor;
            _lblWork.textColor = [UIColor whiteColor];
            _lblWorkTitle.textColor = kFirstSkin;
            
            _lblRest.textColor = [UIColor whiteColor];
            _lblRestTitle.textColor = kThirdSkin;
            
            
            break;
            
        case SECOND_TYPE:
            
            [_imgVewBackground setImage:[UIImage imageNamed:@"Default-Landscape-ipad.png"]];
            [self.topNavigation setImage:[UIImage imageNamed:@"nav-bg_landscape.png"]];

            _imgViewSets.image          = [UIImage imageNamed:@"sets_gray.png"]; //sets_light-gray.png
            
            _imgViewSessionTimer.image  = [UIImage imageNamed:@"session-timer_gray.png"];//session-timer_light-gray.png
            
            _imgViewCircuit.image       = [UIImage imageNamed:@"sets_gray.png"];
     
            _imgViewSetRest.image       = [UIImage imageNamed:@"set-rest-timer_orange.png"];
            _imgViewWork.image          = [UIImage imageNamed:@"green_work_time_ellipse.png"];
            _imgViewRest.image          = [UIImage imageNamed:@"pink_rest_time_ellipse.png"];
            
            _bigTimerImage.image        = [UIImage imageNamed:@"Big-timer_orange.png"];

            [_btnHome setImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
             [_btnMusic setBackgroundImage:[UIImage imageNamed:@"music-btn1.png"] forState:UIControlStateNormal];
            
            [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"red_preset_button.png"] forState:UIControlStateNormal];
            
            [_btnAdd setBackgroundImage:[UIImage imageNamed:@"gray_plus_button.png"] forState:UIControlStateNormal];
            
            [_btnMinus setBackgroundImage:[UIImage imageNamed:@"gray_plus_button.png"] forState:UIControlStateNormal];
            
 [_btnStop setBackgroundColor:[UIColor colorWithRed:60/255.0f green:72/255.0f blue:77/255.0f alpha:1.0]];
        
            [_btnStop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            _lblTrainer.textColor = [UIColor whiteColor];
            _lblNotebook.textColor = ksecondSkin;
            _lblSets.textColor = [UIColor whiteColor];
            _lblSessionTime.textColor = [UIColor whiteColor];
            
            _lblCircuits.textColor = [UIColor whiteColor];
            _lblPhaseName.textColor = [UIColor whiteColor];
            _lblCenterTime.textColor = [UIColor whiteColor];
            _lblPhaseName.textColor = [UIColor whiteColor];
            
            _lblSetRest.textColor = [UIColor whiteColor];
            _lblSetRestTitle.textColor = kOrangeColor;
            _lblWork.textColor = [UIColor whiteColor];
            _lblWorkTitle.textColor = kFirstSkin;
            
            _lblRest.textColor = [UIColor whiteColor];
            _lblRestTitle.textColor = kThirdSkin;
            
            break;
            
        case THIRD_TYPE:
            
            [_imgVewBackground setImage:[UIImage imageNamed:@"Default4-Landscape-ipad.png"]];
            [self.topNavigation setImage:[UIImage imageNamed:@"nav-bg_landscape.png"]];

            
            _imgViewSets.image          = [UIImage imageNamed:@"sets_light-gray.png"];
            
            _imgViewSessionTimer.image  = [UIImage imageNamed:@"session-timer_light-gray.png"];
            
            _imgViewCircuit.image       = [UIImage imageNamed:@"sets_light-gray.png"];
            _imgViewSetRest.image       = [UIImage imageNamed:@"set-rest-timer_orange.png"];
            _imgViewWork.image          = [UIImage imageNamed:@"green_work_time_ellipse.png"];
            _imgViewRest.image          = [UIImage imageNamed:@"pink_rest_time_ellipse.png"];
            
            
            _bigTimerImage.image        =   [UIImage imageNamed:@"pink_set_rest_big_ellipse.png"];

            [_btnHome setImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            
            [_btnMusic setBackgroundImage:[UIImage imageNamed:@"music-btn1.png"] forState:UIControlStateNormal];
            
            [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"pink_preset_button.png"] forState:UIControlStateNormal];
            
            [_btnMinus setBackgroundImage:[UIImage imageNamed:@"minus-white.png"] forState:UIControlStateNormal];
            
            [_btnAdd setBackgroundImage:[UIImage imageNamed:@"plus-white.png"] forState:UIControlStateNormal];
            
            [_btnStop setBackgroundColor:[UIColor whiteColor]];
            [_btnStop setTitleColor:kThirdSkin forState:UIControlStateNormal];

            
            _lblTrainer.textColor = [UIColor whiteColor];
            _lblNotebook.textColor = kThirdSkin;
            
            break;
            
            
        default:
            
            [_imgVewBackground setImage:[UIImage imageNamed:@"BackgroundImage"]];
            [self.topNavigation setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];

//            _imgViewSets.image          = [UIImage imageNamed:@""];
//            _imgViewSessionTimer.image  = [UIImage imageNamed:@""];
//            _imgViewCircuit.image       = [UIImage imageNamed:@""];
//            _imgViewSetRest.image       = [UIImage imageNamed:@""];
//            _imgViewWork.image          = [UIImage imageNamed:@""];
//            _imgViewRest.image          = [UIImage imageNamed:@""];
//            [_btnHome setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            
            break;
    }
    
    END_METHOD
}

//sunil
-(void)SetSelectedImageButton:(UIButton *)button{
    START_METHOD
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;

    
    if (skin == SECOND_TYPE || skin == FIRST_TYPE) {
        
        [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset2 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset3 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset4 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset5 setBackgroundImage:[UIImage imageNamed:@"gray_preset_button.png"] forState:UIControlStateNormal];
        
        if(skin==FIRST_TYPE)
         [button setBackgroundImage:[UIImage imageNamed:@"green_preset_button.png"] forState:UIControlStateNormal];
        else
             [button setBackgroundImage:[UIImage imageNamed:@"red_preset_button.png"] forState:UIControlStateNormal];
    }
   
    if (skin == THIRD_TYPE) {
        [_btnPreset1 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset2 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset3 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset4 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [_btnPreset5 setBackgroundImage:[UIImage imageNamed:@"white_preset_button.png"] forState:UIControlStateNormal];
        
        [button setBackgroundImage:[UIImage imageNamed:@"pink_preset_button.png"] forState:UIControlStateNormal];
    }
    
    
    END_METHOD
}


//---------------------------------------------------------------------------------------------------------------

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
