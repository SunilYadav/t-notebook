//
//  AppController.m
//  
//
//  Created by Sunil on 21/05/15.
//
//

#import "AppController.h"

@implementation AppController

-(UIWindow *)mainWindow{
    return nil;
}

#pragma mark ProgessHud --

- (MBProgressHUD *)showHUDonView:(UIView *)view
{
    NSLog(@"AppController-->showHUDonView");
    if (!self.sharedHud) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:[TheAppController mainWindow] animated:YES];
            self.sharedHud = nil;
            self.sharedHud = [MBProgressHUD showHUDAddedTo:[TheAppController mainWindow] animated:YES];
            
            self.sharedHud.detailsLabelText = @"Please Wait...";
        });
    }
    return self.sharedHud;
}

- (MBProgressHUD *)showHUDonView:(UIView *)view andTheLabel:(NSString *)strTitle
{
    NSLog(@"AppController-->showHUDonView");
    if (!self.sharedHud) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:[TheAppController mainWindow] animated:YES];
            self.sharedHud = nil;
            self.sharedHud = [MBProgressHUD showHUDAddedTo:[TheAppController mainWindow] animated:YES];
            
            self.sharedHud.detailsLabelText = strTitle;
        });
    }
    return self.sharedHud;
}

- (void)hideHUDAfterDelay:(NSTimeInterval)delay
{
    MESSAGE(@"AppController-->hideHUDAfterDelay");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.sharedHud hide:YES afterDelay:delay];
        if (self.sharedHud) {
            self.sharedHud = nil;
        }
    });
}

@end
