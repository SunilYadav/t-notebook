//
//  AddNewSheetViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AddNewSheetDelegate
-(void) addNewSheet:(NSString *)Sheet;
-(void) closeTagPopover;
-(void) editedSheet:(NSString *)Sheet;
@end

@interface AddNewSheetViewController : BaseViewController<UITextFieldDelegate>
{
}
- (void) cancelAddSheet;
- (void) addSheet;

@end
