//
//  HelpViewController.m
//  T-Notebook
//
//  Created by WLI on 01/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "HelpViewController.h"


@interface HelpViewController () <UIScrollViewDelegate>

@property (strong, nonatomic)  UIScrollView                         *scrollview;
@property (strong, nonatomic)  UIPageControl                        *pageController;
@property (nonatomic) NSInteger                                      currentPage;
@property (strong , nonatomic) UIView                               *helpView;
@property (strong , nonatomic) UITapGestureRecognizer               *tapGesture;
@property (nonatomic) BOOL                                          isLandScapeMode;
@property (nonatomic , strong) UIImageView                          *imgvewTopNav;
@property (nonatomic, weak) IBOutlet     UILabel                    *lblTraining;
@property (nonatomic, weak) IBOutlet     UILabel                    *lblNotebook;
@property (weak, nonatomic) IBOutlet UILabel *lblBack;

@end

@implementation HelpViewController

//-----------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.lblNotebook.text = NSLocalizedString(self.lblNotebook.text, nil);
    self.lblTraining.text = NSLocalizedString(self.lblTraining.text, nil);
    self.lblBack.text = NSLocalizedString(self.lblBack.text, nil);


    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setTranslucent:TRUE];
     [self setupScrollView];
}


//---------------------------------------------------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    START_METHOD
    [super viewWillAppear:animated];
   
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isAlreayGoThroughHelp"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAlreayGoThroughHelp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self setupHelpView];
    }
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [self setUpLandscapeOrientation];
    } else {
        [self setupPortraitOrientation];
    }
    

}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Action Method 

//---------------------------------------------------------------------------------------------------------------

-(IBAction)moveToHomeVIew:(id)sender {
    
[self.navigationController popViewControllerAnimated:YES];
    
}


//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

-(void) setupHelpView {
    
    _helpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.helpView setBackgroundColor:[UIColor blackColor]];
    [self.helpView setAlpha:0.7];
    [self.scrollview addSubview:self.helpView];
    
    UILabel *lblInfo = [[UILabel alloc]initWithFrame:CGRectMake(0, 270,self.view.frame.size.width, 150)];
    lblInfo.textAlignment = NSTextAlignmentCenter;
    [lblInfo setText:NSLocalizedString(@"Swipe left to view more", nil)];
    [lblInfo setTextColor:[UIColor whiteColor]];
    [lblInfo setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:35]];
    [self.helpView addSubview:lblInfo];
    
    
    // 221 95
    UIImageView  *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(250, 420, 221, 95)];
    [imgArrow setBackgroundColor:[UIColor clearColor]];
    [imgArrow setImage:[UIImage imageNamed:@"HelpArrow"]];
    [self.helpView addSubview:imgArrow];
    
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeHelpView:)];
    [self.helpView addGestureRecognizer:self.tapGesture];
}
//---------------------------------------------------------------------------------------------------------------

-(void)removeHelpView:(UITapGestureRecognizer*)gesture {
    
    [self.helpView removeFromSuperview];
    self.helpView = nil;

}


//---------------------------------------------------------------------------------------------------------------

-(void) setupScrollView {
    
    //add the scrollview to the view
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    self.imgvewTopNav = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 768, 1024)];
    [self.imgvewTopNav setImage:[UIImage imageNamed:@"Top-Bar"]];
    [self.view addSubview:self.imgvewTopNav];
    
     self.scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 78,self.view.frame.size.width,self.view.frame.size.height- 78)];
    self.scrollview.delegate = self;
    self.scrollview.pagingEnabled = YES;
    [self.scrollview setBackgroundColor:[UIColor clearColor]];
    [self.scrollview setAlwaysBounceVertical:NO];
    [self.view addSubview:self.scrollview];
    
    _pageController = [[UIPageControl alloc]initWithFrame:CGRectZero];
    self.pageController.numberOfPages = 6;
    self.pageController.currentPage = 0;
    [self.pageController setAutoresizingMask:UIViewAutoresizingNone];
    [self.view addSubview:self.pageController];
    self.currentPage = 0;
    
 }

//---------------------------------------------------------------------------------------------------------------

-(BOOL)prefersStatusBarHidden {
    
    return  NO;
}


//-----------------------------------------------------------------------

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : ScrollView Delegate Methods

//---------------------------------------------------------------------------------------------------------------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.helpView)
    {
        [self.helpView removeFromSuperview];
        self.helpView = nil;
    }
  
    
    NSInteger offsetPont = scrollView.contentOffset.x;
    if(self.isLandScapeMode) {
        switch (offsetPont) {
            case 1024*1:
                self.currentPage = 1;
                break;
                
            case 1024 *2:
                self.currentPage = 2;
                break;
                
            case 1024*3:
                self.currentPage = 3;
                break;
                
            case 1024*4:
                self.currentPage = 4;
                break;
                
            case 1024*5:
                self.currentPage = 5;
                break;
                
            case 1024*6:
                self.currentPage = 6;
                break;
                
            case 0:
                self.currentPage = 0;
                break;
                
            default:
                break;
        }

    } else {
        switch (offsetPont) {
            case 768*1:
                self.currentPage = 1;
                break;
                
            case 768 *2:
                self.currentPage = 2;
                break;
                
            case 768*3:
                self.currentPage = 3;
                break;
                
            case 768*4:
                self.currentPage = 4;
                break;
                
            case 768*5:
                self.currentPage = 5;
                break;
                
            case 768*6:
                self.currentPage = 6;
                break;
                
            case 0:
                self.currentPage = 0;
                break;
                
            default:
                break;
        }
  
    }
    
      self.pageController.currentPage = self.currentPage;
}

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}



//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        // landscape orientation
        [self setUpLandscapeOrientation];
    } else {
        // portrait orientation
       [self setupPortraitOrientation];
        
    }

}

//-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
    
    self.isLandScapeMode = TRUE;
    self.imgvewTopNav.image = nil;
    [self.view sendSubviewToBack:self.imgvewTopNav];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTraining.text font:self.lblTraining.font height:self.lblTraining.frame.size.height];
    self.lblTraining.frame = CGRectMake(257+150, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblNotebook.text font:self.lblNotebook.font height:self.lblNotebook.frame.size.height];
    self.lblNotebook.frame = CGRectMake(self.lblTraining.frame.origin.x + self.lblTraining.frame.size.width , 23,rect.size.width + 5 ,48);
    
    [self.imgvewTopNav setFrame:CGRectMake(0,20,kLandscapeWidth,57)];
    [self.imgvewTopNav setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];
    [self.scrollview setFrame:CGRectMake(0,78,kLandscapeWidth,kLandscapeHeight-78)];
    
    //pageController
    [self.pageController setFrame:CGRectMake(460,kLandscapeHeight- 40,100, 50)];
    
    [self setScrollViewContent];
    [self.view bringSubviewToFront:self.pageController];
    
}

//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
    
    self.isLandScapeMode = FALSE;
    self.imgvewTopNav.image = nil;
    [self.view sendSubviewToBack:self.imgvewTopNav];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTraining.text font:self.lblTraining.font height:self.lblTraining.frame.size.height];
    self.lblTraining.frame = CGRectMake(257+42, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblNotebook.text font:self.lblNotebook.font height:self.lblNotebook.frame.size.height];
    self.lblNotebook.frame = CGRectMake(self.lblTraining.frame.origin.x + self.lblTraining.frame.size.width , 23,rect.size.width + 5 ,48);

    [self.imgvewTopNav setFrame:CGRectMake(0, 20,kPotraitWidth,57)];
    [self.imgvewTopNav setImage:[UIImage imageNamed:@"Top-Bar"]];
    [self.scrollview setFrame:CGRectMake(0, 78,kPotraitWidth,kPotraitHeight-78)];
    
    [self.pageController setFrame:CGRectMake(330,kPotraitHeight - 50, 100, 50)];
    [self setScrollViewContent];
    [self.view bringSubviewToFront:self.pageController];
}


//-----------------------------------------------------------------------
-(void)setScrollViewContent {
    
    NSArray *aryContents = [self.scrollview subviews];
    [aryContents enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[UIImageView class]]) {
            [obj removeFromSuperview];
        }
    }];
    
    if(self.isLandScapeMode) {
      [self.scrollview setContentSize:CGSizeMake(kLandscapeWidth,kLandscapeHeight - 78)];
    } else {
      [self.scrollview setContentSize:CGSizeMake(kPotraitWidth, kPotraitHeight)];
    }
    
    
    
    NSInteger numberOfViews = 6;

    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
        numberOfViews = 5;
        self.pageController.numberOfPages = 5;
    }
    
    CGFloat xOrigin = 0;
    
    for (int i = 1; i <=numberOfViews; i++) {

        NSString *name;
        if(i >= 4) {
            if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
                if(self.isLandScapeMode) {
                     name = [NSString stringWithFormat:@"%d_Landscape.png",i+1];
                } else {
                   name = [NSString stringWithFormat:@"%d.png",i+1];
                }
              
            } else {
                if(self.isLandScapeMode) {
                    name = [NSString stringWithFormat:@"%d_Landscape.png",i];
                } else {
                   name = [NSString stringWithFormat:@"%d.png",i];
                }
               
            }
        } else {
            if(self.isLandScapeMode) {
              name = [NSString stringWithFormat:@"%d_Landscape.png",i];
            } else {
              name = [NSString stringWithFormat:@"%d.png",i];
            }
            
        }
        
        UIImage *snapImg = [UIImage imageNamed:name];
        UIImageView *imageVew = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 10,snapImg.size.width,snapImg.size.height-84)];
        imageVew.image = [UIImage imageNamed:name];
        imageVew.contentMode = UIViewContentModeScaleAspectFit;
        imageVew.backgroundColor = [UIColor clearColor];
        [self.scrollview addSubview:imageVew];
        
        xOrigin = xOrigin + imageVew.frame.size.width;
        
    }
    
    if(self.isLandScapeMode) {
       self.scrollview.contentSize = CGSizeMake(kLandscapeWidth*numberOfViews,kLandscapeHeight-178);
    } else {
      self.scrollview.contentSize = CGSizeMake(kPotraitWidth*numberOfViews,kPotraitHeight-78);
    }
    
    
}

//-----------------------------------------------------------------------

#pragma mark - Memory Management Methods

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//---------------------------------------------------------------------------------------------------------------

-(void)dealloc {
    
    // DLOG(@"HelpVie Dealloc called");
}


@end
