//
//  PurchaseVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PurchaseVC : BaseViewController<CStoreKitDelegate>

-(void)buyProduct;

@end
