//
//  AppConstant.h
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>

//SUNIL
#define NSFoundationVersionNumber_iOS_8_0  1140.11
#define NSFoundationVersionNumber_iOS_7_0  1047.20

#define kAppTintColor [UIColor colorWithRed:75/255.0f green:197/255.0f blue:181/255.0f alpha:1.0]
#define kAppTintColor [UIColor colorWithRed:75/255.0f green:197/255.0f blue:181/255.0f alpha:1.0]
#define kModalTransitionStyleCoverVertical  UIModalTransitionStyleFlipHorizontal

//Sunil
//#define iOS_7    (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) && ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0))

#define iOS_7    ((floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_7_0) && (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_8_0))


//#define iOS_8    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define iOS_8    (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_8_0)

#define kisFullVersion              @"isFullVersion"
#define kliteMsg                      NSLocalizedString(@"T-Notebook Lite version have limited access to various features. In order to activate please purchase a product which activates all the features. Would you like to purchase?",nil)

#define kFullAccessProductId                @"com.thetrainingnotebook.TNotebookLite.FullAccess"
#define kAppraterIdFullVersion              @"505906865"
#define kAppraterIdLiteVersion               @"505911901"
#define kHockeyAppId                            @""
#define KHockeyFullBetaAppId              @"d3446deedab6f60a06ce16c6f963b1bc"
#define kHockeyFullLiveAppId                @"8e81dae346e81d07ccde76a77aa8eb76"

#define kHockeyLiteBetaAppId               @"c0e41a1e407981e9e92da349c3203c91"
#define kHockeyLiteLiveId                       @"8e8f2a5de7c533cbc74d27ce654b3fa9"

#define  ADMOB_ID                                @"ca-app-pub-3833158715595946/3705586713"
#define kAppDateFormat                         @"MM/dd/yyyy"

#define kCategoryId					 @"nCategoryID"
#define kCategoryName				 @"sCategoryName"
#define kAppName                     NSLocalizedString(@"The Training Notebook",nil)

#define kAppUpdated                  @"AppUpdated"
#define kReloadTable                     @"reloadTable"
			
#define kQuestionId					@"nQuestionID"
#define kQuestion					    @"sQuestion"
#define kQuestionBool				@"bBooleanQuestion"
#define kQuestionText				    @"bTextualQuestion"
#define kQuestionCateId 			@"Question CategoryID"
#define kAnswerId					    @"AnswerID"
#define kClientId					        @"ClientID"
#define kBoolAnswer					@"bAnswer"
#define kTextAnswer					@"sAnswer"

#define kFirstName					   @"sFirstName"
#define kLastName					   @"sLastName"
#define kEmail1						   @"sEmailAddress"
#define kEmail2						   @"sEmailAddress2"
#define kPday                            @"sPhone_Day"
#define kPeve                            @"sPhone_Evening"
#define kPcell                            @"sPhone_cell"
#define kHeight						   @"fHeight"
#define kWeight						   @"fWeight"
#define kPhysicianname			   @"Physician Name"
#define kPhysicianphone		   @"Physician Phone"
#define kUpdatedDate				   @"UpdatedDate"
#define kMigrated				   @"migrated"

#define kAge						        @"nAge"
#define kGereralFatPercent        @"fGenereal_FatPercent"
#define kExtraValue                    @"fExtra_Value"
#define kExtraFieldName             @"sExtra_FieldName"

#define kProgramData                @"programData"
#define kWorkoutData                @"WorkoutData"


#define kEmergencyConName1			@"sEmergencyContactName1"	
#define kEmergencyConName2			@"sEmergencyContactName2"
#define kEmergencyConWNo1			@"sEmergencyContact1_Phone_Work"
#define kEmergencyConWNo2			@"sEmergencyContact2_Phone_Work"
#define kEmergencyConHNo1			@"sEmergencyContact1_Phone_Home"
#define kEmergencyConHNo2			@"sEmergencyContact2_Phone_Home"
#define kEmergencyConCNo1			@"sEmergencyContact1_Phone_Cell"
#define kEmergencyConCNo2			@"sEmergencyContact2_Phone_cell"
#define kEmergencyRel1				@"sEmergencyContact1_Relationship"
#define kEmergencyRel2				@"sEmergencyContact2_Relationship"
#define kBirthDate					@"dBirthDate"
#define kgender						@"sGender"
#define kLastReviewdDate			@"dlastReviewdDate"
#define knoData						@"noData"

#define kImageID					@"nImageID"
#define kImageName					@"sImageLabel"
#define khasImage					@"hasImage"

#define ksheetID					@"sheetID"
#define kUpdateStatus               @"nUpdateStatus"
#define ksheetName					@"sheetName"
#define ksProgramPurpose					@"sPurpose"
#define ksBlockTitle					@"sBlockTitle"
#define kTrainerName                @"trainerName"
#define kRowNo					@"nRowNo"

#define kAddTemplateMessage					@"ADD NEW PROGRAM TEMPLATE"

		
#define kdetailID					@"detailID"
#define kRestingHeartBeat			@"nRestingHeartBeat"
#define kBloodPressure				@"sBloodPressure"
#define kThree_Data1				@"Three_Male_Chest/Three_Female_Triceps"			
#define kThree_Data2				@"Three_Male_Abdomen/Three_Female_Suprailiac"
#define kThree_Data3				@"Three_Male_Thigh/Three_Female_Thigh"
#define kfThree_Result				@"fThree_Result"
#define kFour_Abdomen				@"fFour_Abdomen"
#define kFour_Suprailiac			@"fFour_Suprailiac"
#define kFour_Triceps				@"fFour_Triceps"
#define kFour_Thigh					@"fFour_Thigh"
#define kFour_Result				@"fFour_Result"
#define kSeven_Chest				@"fSeven_Chest"
#define kSeven_Midaxillary			@"fSeven_Midaxillary"
#define kSeven_Triceps				@"fSeven_Triceps"
#define kSeven_subscapular			@"fSeven_subscapular"
#define kSeven_Abdomen				@"fSeven_Abdomen"
#define kSeven_Suprailiac			@"fSeven_Suprailiac"
#define kSeven_Thigh				@"fSeven_Thigh"
#define kfSeven_Result				@"fSeven_Result"
#define kCM_RightUpperArm			@"fCM_RightUpperArm"
#define kCM_RightForeArm			@"fCM_RightForeArm"
#define kCM_Chest					@"fCM_Chest"
#define kCM_Abdominal				@"fCM_Abdominal"
#define kCM_HipButtocks				@"fCM_HipButtocks"
#define kCM_RightThing				@"fCM_RightThing"
#define kCM_RightCalf				@"fCM_RightCalf"
#define kBMI_Pound_Weight			@"fBMI_Pound_Weight"
#define kBMI_Pound_Height			@"fBMI_Pound_Height"
#define kBMI_Pound_Result			@"fBMI_Pound_Result"
#define kBMI_Metrics_Weight			@"fBMI_Metrics_Weight"
#define kBMI_Metrics_Height			@"fBMI_Metrics_Height"
#define kBMI_Metrics_Result			@"fBMI_Metrics_Result"
#define kCM_Result					@"fCM_Result"
#define kAssUpdatedDate				@"dUpdated_date"
#define ksAssessment_Goal			@"sAssessment_Goal"


// ********************** Constant defined by Dhaval ****************

#define kBlockDataId				@"nBlockDataID"
#define PROGRAM_EXERCISE_ID         @"program_exercise_id"
#define kBlockId					@"nBlockID"
#define kSheetId					@"nSheetID"
#define kExercise					@"sProgram"
#define kLBValue					@"sLBValue"
#define kRepValue					@"sRepValue"
#define kSetValue						@"sSetValue"
#define kBlockNo					@"nBlockNo"
#define kRowNo						@"nRowNo"
#define kDate                       @"sDate"
#define kEXDate						@"dEXDate"
#define kBlockDate					@"dBlockdate"
#define kBlockTitle					@"sBlockTitle"
#define kBlockNotes					@"sBlockNotes"
#define kColor                      @"sColor"


#define kAmericanTypewriterStd                      @"AmericanTypewriterStd-Med"
#define kHelveticaNeueLTCom_Md                      @"HelveticaNeueLTCom-Md"
#define kHelveticaNeueLTCom_Roman                   @"HelveticaNeueLTCom-Roman"
#define kHelveticaNeueLTStd_Roman                   @"HelveticaNeueLTStd-Roman"
#define kHelveticaNeueLTStd_Md                      @"HelveticaNeueLTStd-Md "
#define kOpenSans_Regular                           @"OpenSans-Regular"
#define kProximaNovaAltBold_Italic                  @"ProximaNovaA-BoldIt"  //  //Proxima Nova Alt-Bold-Italic
#define kProximaNovaAlt_Light                       @"ProximaNovaA-Light "   // Proxima Nova Alt-Light
#define kProximaNova_Regular				        @"ProximaNova-Regular"    //Proxima Nova Regular
#define kproximanova_semibold                       @"ProximaNova-Semibold"  //proximanova-semibold
#define krade_Gothic_LT_Bold_Condensed              @"TradeGothic LT CondEighteen"  //TradeGothic LT CondEighteen
#define kGotham_XLight                              @"Gotham XLight" //Gotham XLight


#define kDocumentTicket				@"DocTicket"
#define kSpreadSheetListOk			@"spreadSheetListed"
#define kSpreadSheetCreated			@"spreadSheetCreated"
#define kWorksheetListOk			@"WorkSheetListed"
#define kWorksheetCreated			@"WorksheetCreated"
#define kWorksheetRename            @"WorksheetRename"
#define kAddDefaultEntry			@"AddDefaultEntry"
#define kListFeed					@"ListFeedEntry"
#define kUpdateData					@"UpdateData"
#define kWorksheetDeleted			@"WorksheetDeleted"


//New One
#define kfieldName					@"fieldName"
#define kfieldValue					@"fieldValue"
#define kfieldName2					@"fieldName2"
#define kfieldValue2				@"fieldValue2"
#define kDictType					@"DictType"

#define kGoogleEmail				@"Email"
#define kGooglePass					@"Password"
#define kDefaultExe                 100



//=================================Clock View Controllers =========================//

#define kPresetsData                    @"PresetsData"
#define kTimer_Sets                     @"Timer_Sets"
#define kTimer_Circuits                @"Timer_Circuits"
#define kTimer_SetRest                @"Timer_SetRest"
#define kTimer_Work                    @"Timer_Work"
#define kTimer_Rest                     @"Timer_Rest"
#define kTimer_SessionTime        @"Timer_SessionTime"
#define kTimer_PresentName       @"Timer_PresetName"
#define kIsTimerPlaying                 @"isTimerPlaying"
#define kIsTimerPaused                 @"isTimerPaused"

#define kTimerNotification                                  @"TimerNotification"
#define kTimerNotification_Value                       @"TimerNotification_Value"
#define kTimerNotification_IsTimeCompleted    @"TimerNotification_IsTimeCompleted"
#define kInterValNotification                               @"intervalNotification"
#define kBackgroundTime_Timer                       @"BackgroundTime_Timer"
#define kBackgroundTime_InterVal                    @"BackgroundTime_InterVal"
#define kIsIntervalPlaying                                  @"IsIntervalPlaying"

#define kStartTime                @"StartTime"
#define kEndTime                  @"EndTime"
#define kIsSoundOn                @"IsSoundOn"

//===== Timer delegate constants ========

#define kisWork                      @"isWork"
#define kisRest                       @"isRest"
#define kisSet                         @"isSet"
#define kisSetRest                  @"isSetRest"
#define kisCircuit                    @"isCircuit"
#define kisCenterTime           @"isCenterTime"
#define kisSessionTime          @"isSessionTime"
#define kDashboardNotification @"DashboardNotification"
#define kphaseImageNotification @"PhaseImageNotification"
#define kisForBlink                       @"isForBlink"
#define kblinkTimerNotification    @"blinkTimerNotification"

//============= Screen Name =================

#define kisParq                       @"PAR-Q"
#define kisProgram                 @"Program"
#define kisAssessment           @"Assessment"
#define kisNotes                     @"Notes"
#define kisViewAllPrograms           @"viewAllPrograms"
#define kProgramTemplate                    @"ProgramTemplate"
#define kIsCopyLine                         @"isCopyLine"
#define kCopyBlock                          @"copiedBlock"
#define kCopyProgramBlock                   @"copiedProgramBlock"

#define kCopiedLinesForBlock                @"copiedLinesForBlock"
#define kupdateVersionNotification  @"updateVersionNotification"
#define kLiteVersion  @"liteVersion"
#define kBlockCurrentCopiedSheetID  @"blockCurrentCopiedSheetID"
#define kCopiedBlockIDSheetID  @"copiedBlockIDSheetID"
#define kCopiedClientID          @"copiedClientID"


//=================================Clock View Controllers =========================//

#define kSelectedPreset			@"SelectedPreset"
#define kisTimerOn					@"isTimerOn"
#define kPresets1					@"presets1"
#define kPresets2					@"presets2"
#define kPresets3					@"presets3"
#define kPresets4					@"presets4"
#define kPresets5					@"presets5"
#define kbackgroundTime		@"backgroundTime"

#define kstrCycle					@"strCycle"
#define kstrCircuits				@"strCircuits"
#define kstrCycleRestMin			@"strCycleRest"
#define kstrWorkMin					@"strWork"
#define kstrRestMin					@"strRest"
#define kstoreLastTime				@"storeLastTime"
#define ktotalTime					@"totalTime"
#define kselectedImage				@"selectedImage"
#define kDeselectedImage			@"DeselectedImage"

#define kHH							@"HH"
#define kMM							@"MM"
#define kSS							@"SS"
#define kcycleRest					@"cycleRest"
#define krest						@"rest"
#define kwork						@"work"
#define kphase						@"phase"// 1-cycle rest, 2-work,3-rest
#define klastRemainingPhaseTime		@"lastRemainingPhaseTime"
#define ktotRemainingCycles			@"totRemainingCycles"// 1-cycle rest, 2-work,3-rest
#define ktotRemainingCircuits		@"totRemainingCircuits"
#define kStopTime					@"StopTime"

#define kMusicMode					@"MusicMode"
#define kSuffleMode					@"SuffleMode"
#define kRepeatMode					@"RepeatMode"
#define kPlayListName				@"PlayListName"

#define kAppComesInForground		@"AppComesInForground"

#define kLandscape					@"Landscape"
#define kPortrait					@"Potrait"

#define kWorkSoundFile              @"workSoundFile"
#define kRestSoundFile              @"restSoundFile"
#define kCycleRestSoundFile         @"cycleRestSoundFile"
#define kCompleteTimerSoundFile     @"completeTimerSoundFile"


//---------------------------Note View Constant---------------------------------------------------------------
#define kSectionToAddNotes      @"sSectionToAddNotes"
#define kMedicalInformation     @"sMedicalInformation"
#define kExerciseHistory        @"sExerciseHistory"
#define kClientGoals            @"sClientGoals"
#define kPersonalRecordsAndMore @"sPersonalRecordsAndMore"


//---------------------------NSUserDefaults For TemplateView Controller class ------------------------------

#define kTemplateBlockData                     @"TemplateData"
#define kTemplateIdForTemplate                 @"TemplateID"
#define kTemplateSheetIDForTemplate            @"SheetID"
#define kTemplateSheetBlockIDForTemplate       @"BlockID"

//For copy the sheet.

#define kTemplateSheetID                       @"SheetID"
#define kTemplateSheetData                     @"TemplateSheet" 


//---------------------------NSUserDefaults For ProgramView Controller class ------------------------------

#define kProgramBlockData       @"blockData"
#define kclientIdForProgram     @"clientID"
#define kSheetIdForProgram      @"sheetID"
#define kBlockIdForProgram      @"blockID"   


#define kisnsUserDefaultAvilable    @"isnsUserDefaultAvailable"

#define JQueryFileName @"index"

//--------------------------------------------------------------------------------------------------

#define kLandscapeWidth  1024
#define kLandscapeHeight 768
#define kPotraitWidth   768
#define kPotraitHeight  1024




#define kFirstSkin      [UIColor colorWithRed:136/255.0f green:218.0/255.0f blue:8/255.0f alpha:1.0]
#define ksecondSkin     [UIColor colorWithRed:237/255.0f green:27.0/255.0f blue:35/255.0f alpha:1.0]
#define kThirdSkin      [UIColor colorWithRed:233/255.0 green:39.0/255.0 blue:103/255.0 alpha:1.0]
#define kOldSkin        [UIColor colorWithRed:75/255.0f green:197/255.0f blue:181/255.0f alpha:1.0]
#define kOptionButtonColor        [UIColor colorWithRed:190/255.0f green:198/255.0f blue:194/255.0f alpha:1.0]


#define kOrangeColor    [UIColor colorWithRed:242/255.0f green:158.0/255.0f blue:0/255.0f alpha:1.0]

#define kBoxGrayColor    [UIColor colorWithRed:178/255.0f green:186/255.0f blue:189/255.0f alpha:1.0f];


#define kBoxDarkGrayColor    [UIColor colorWithRed:57/255.0f green:63/255.0f blue:70/255.0f alpha:1.0f];
#define kBoxDarkGrayColorr    [UIColor colorWithRed:57/255.0f green:63/255.0f blue:70/255.0f alpha:1.0f]


enum SKIN_TYPE                  {OLD_TYPE, FIRST_TYPE,SECOND_TYPE,THIRD_TYPE};

enum MODE_TYPE                  {PORTRAIT, LANDSCAPE};


//---> SUNIL
#define  USER_ACCESS_TOKEN                          [commonUtility retrieveValue:KEY_ACCESS]


//WEB HOST
#define   WEB_HOST                                  @"http://tnotebook.com/index.php/welcome/"


//#define     HOST                                @"52.39.220.36/index.php/mobile/" //Host Name
//#define     HOST                               @"52.26.135.139/index.php/mobile/"  //Development  Name
#define     HOST                                 @"35.166.235.18/index.php/mobile/" //ELASTIC Name

#define     HOST_URL                            @"http://"HOST@""   //Host Name full url
#define     NULL_STRING                         @""


//Parameter for SignUp/login
#define     USERNAME                            @"username"
#define     FIRST_NAME                          @"first_name"
#define     MIDDLE_NAME                         @"middle_name"
#define     LAST_NAME                           @"last_name"
#define     EMAIL                               @"email"
#define     USER_TYPE_OLD                       @"user_type_old"
#define     USER_ID                             @"user_ID"
#define     PASSWORD                            @"password"
#define     FB_PASSWORD                         @"fbpassword"
#define     FB_EMAIL                            @"fbemail"
#define     AUTO_LOGIN                          @"autologin"

#define     DEVICE_UDID                         @"device_udid"
#define     RESULT                              @"result"
#define     CODE_RETURN                         @"code"
#define     PASSWORD_RESET_CODE                 @"password_reset_code"
#define     FORGOT_RESPONCE                     @"The email address that was submitted was not found in the database."
#define     PASSWORD_CHANGED                    @"The password was successfully updated to the newly submitted password."


#define KEY_BACKGROUND_IMAGE                    @"backgroundImage"

//TODO: USER DEFAULTS KEYS
#define KEY_SKIN                                @"skin"
#define KEY_MODE                                @"mode"
#define KEY_UPGRADE_STATUS                      @"upgrade"
#define KEY_OLD_USER                            @"oldUser"
#define TOTAL_CLIENT                            @"total_client"
#define KEY_STATUS_ADD_CLIENT                   @"addNewClient"
#define KEY_TRAINER_ID                          @"trainerId"
#define KEY_LOGIN_STATUS                        @"login"
#define KEY_USER_ID                             @"userId"
#define KEY_APP_VERSION                         @"version"
#define KEY_TEMPALET_ID_PASTE                   @"templateIdForPaste"
#define KEY_ACCESS                              @"access_key"


//Server keys
#define  PAYLOAD                                @"payload"
#define  ACTION                                 @"action"
#define  ADD_CLIENT                             @"addClient"
#define  UPDATE_CLIENT                          @"updateClient"
#define METADATA                                @"metadata"
#define ERROR                                   @"error"
#define POPUPTEXT                               @"popuptext"
#define LIST_KEY                                @"list"
#define STATUS_LIST                             @"status_list"
#define STATUS                                  @"status"
#define CLIENT_STATUS                           @"client_status"
#define RESPONSE_DATA                           @"data"
#define PLAN_METHOD                             @"plan_method"
#define PLAN_ID                                 @"plan_id"
#define ID                                      @"id"

#define PROGRAM_ID                              @"program_id"
