//
//  TimerManager.h
//  T-Notebook
//
//  Created by WLI on 03/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@protocol TimerDelegate <NSObject>

-(void)changeValuesOnTimer:(NSString*)changedValue ForTheSelector:(NSString *)selector ;
-(void)changePhaseOfTimerWithSelector:(NSString *) selector;
-(void)timerTimeCompleteResetValues;
-(void)setBlinkImagesFor:(NSString*)selector andType:(BOOL)forBlink;

@end


@interface TimerManager : NSObject {
    
}

@property (nonatomic) NSInteger                                               totSets;
@property (nonatomic) NSInteger                                               totCircuits;
@property (nonatomic) NSInteger                                               intSetRestTime;
@property (nonatomic) NSInteger                                               intWorkTime;
@property (nonatomic) NSInteger                                               intRestTime;
@property (nonatomic) NSInteger                                               intNumberOfSets;
@property (nonatomic) NSInteger                                               intNumberOfCircuits;
@property (nonatomic) NSInteger                                               intTotalTime;
@property (nonatomic) NSInteger                                               intSelectedSet;
@property (nonatomic) NSInteger                                               intRemaningTimeForPhase;

@property (nonatomic , strong) NSString                      *strWork;
@property (nonatomic , strong) NSString                      *strRest;
@property (nonatomic , strong) NSString                      *strSetrest;
@property (nonatomic , strong) NSString                      *strSets;
@property (nonatomic , strong) NSString                      *strCircuit;
@property (nonatomic , strong) NSString                      *strMilsecond;
@property (nonatomic , strong) NSString                      *strSession;
@property (nonatomic , strong) NSString                      *strCenterTimer;
@property (nonatomic , strong) NSString                      *strSessionTimer;


@property (nonatomic) BOOL                                        isTimerRunning;
@property (nonatomic) BOOL                                        isTimerPaused;
@property (nonatomic , strong) NSTimer                      *intervalTimer;
@property (nonatomic) BOOL                                        isBlinkImageSet;

@property (nonatomic, unsafe_unretained)  id<TimerDelegate>          timeDelegate;


+(TimerManager *) sharedInstance;
-(void)startTimer;
- (void) runTimer;
-(void)pauseTimer;
-(void)setBlinkImage;

@end


