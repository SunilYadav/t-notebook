//
//  ProgramTemplateViewController.h
//  T-Notebook
//
//  Created by WLI on 23/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProgramTemplateViewController : BaseViewController{
    
    __weak IBOutlet UIButton *btnSelectFromAll;
    __weak IBOutlet UIButton *updateTemplateName; 
    
    __weak IBOutlet UIView *viewOptions;
}
@property (nonatomic , strong) NSString             * strLableText;
@property (nonatomic) BOOL                         isNewTemplate;

@property (nonatomic) NSInteger                    intCurrentSheetID;
@property (nonatomic) NSInteger                    intCurrentTemplateID;
@property (nonatomic) NSInteger                    intCurrentBlockID;
@property (nonatomic) NSInteger                    intStoreFirstSheetId;
@property (nonatomic) NSInteger                    intTemplateSheets;
@property (nonatomic) NSDictionary                   *dictTemplateInfo;
@end
