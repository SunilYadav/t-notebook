//
//  TearmsAndConditionVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "TearmsAndConditionVC.h"

@interface TearmsAndConditionVC () <UIWebViewDelegate>
{
    //sunil
}

@property (nonatomic , weak) IBOutlet   UIWebView            *webView;

@end

@implementation TearmsAndConditionVC

- (void)viewDidLoad {
    
    START_METHOD
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor clearColor]];
    UIButton * btnHome = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnHome setBackgroundColor:[UIColor colorWithRed:73.0f/255 green:197.0f/255 blue:186.0f/255 alpha:1.0f]];
    [btnHome setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnHome setTitle:NSLocalizedString(@"Home", nil) forState:UIControlStateNormal];
    [btnHome addTarget:self action:@selector(homeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnHome setFrame:CGRectMake(95, 10, 50,29)];
    UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc] initWithCustomView:btnHome];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray * toolItems = [NSArray arrayWithObjects:leftBtn,space, nil];
    
    UIToolbar *termsToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,550 + 47, 48)];
    [termsToolBar setBackgroundColor:[UIColor grayColor]];
    termsToolBar.items = toolItems;
    
    [self.view addSubview:termsToolBar];
    
    
  
}

//---------------------------------------------------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    START_METHOD
    [super viewWillAppear:animated];
    [self openPdf];
}

//---------------------------------------------------------------------------------------------------------------

-(void)openPdf {
    
    NSString * docPath = [[NSBundle mainBundle]pathForResource:@"Policy" ofType:@"pdf"];
    NSURL *targetURL = [NSURL URLWithString:docPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    
    [self.webView setBackgroundColor:[UIColor whiteColor]];
    [self.webView loadRequest:request];

}

//---------------------------------------------------------------------------------------------------------------

-(void)homeBtnClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark : Webview Delegate Methods

//---------------------------------------------------------------------------------------------------------------


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    
}


//---------------------------------------------------------------------------------------------------------------


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
}


//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
