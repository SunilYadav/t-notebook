//
//  AddNewTemplateControllers.m
//  T-Notebook
//
//  Created by WLI on 20/06/12.
//  Copyright (c) 2012 WLI. All rights reserved.
//

#import "AddNewTemplateVC.h"
@interface AddNewTemplateVC(){
    
    UIPickerView                 *   pickerSheets;
    NSArray                      *   arrayNumberOfSheets;
    __weak IBOutlet UILabel      *   lblTemplate;
    __weak IBOutlet UITextField  *   txtTemplateName;
   __weak IBOutlet UIButton      *   btnAdd;
    __weak IBOutlet UILabel      *   lblSheetsNumber;
    NSInteger                         selectedsheets;
}
@end

@implementation AddNewTemplateVC
@synthesize delegateTemplate;

//----------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

//----------------------------------------------------

#pragma mark -
#pragma mark - View lifecycle

-(void)viewDidLoad{
    [super viewDidLoad];
    
    UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
    lblTitle.text = NSLocalizedString(@"Add Template", nil);
    lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
    lblTitle.textColor = kAppTintColor;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.adjustsFontSizeToFitWidth=YES;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=lblTitle;
    
    arrayNumberOfSheets = [NSArray arrayWithObjects:@"1",@"2",@"3", nil];
    [btnAdd setTitle:NSLocalizedString(btnAdd.titleLabel.text, nil) forState:0];
    btnAdd.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnAdd.layer.cornerRadius = 4.0;
    btnAdd.backgroundColor = kAppTintColor;
    

    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(btnCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -15;
    UIBarButtonItem *leftBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, leftBtn, nil] animated:NO];
    
    txtTemplateName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    [txtTemplateName setText:self.strTemplateName];
    txtTemplateName.delegate = self;
    txtTemplateName.returnKeyType = UIReturnKeyDone;

    [self createHorizontalPicker];
     [self localizeControls];
}

- (void)viewWillAppear:(BOOL)animated {
    // Weblineindia //
    START_METHOD
    if (iOS_7) {
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight)];
        } else {
            self.view.frame = CGRectMake(0, 0, kPotraitWidth, kPotraitHeight);
        }
        self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 250+44);
        self.navigationController.view.superview.center = self.view.center;
    }
}
//-----------------------------------------------------------------------------------------------------------------------------

#if defined(__IPHONE_6_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0

-(BOOL)shouldAutorotate {
    return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

-(NSUInteger)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskAll;
}

#endif

//-----------------------------------------------------------------------------------------------------------------------------


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

//----------------------------------------------------
#pragma mark -
#pragma mark - Custom  Methods

//----------------------------------------------------

-(void) createHorizontalPicker{
    
    pickerSheets = [[UIPickerView alloc] initWithFrame:CGRectZero];
	pickerSheets.delegate = self;
	pickerSheets.dataSource = self;
	pickerSheets.showsSelectionIndicator = YES;
    
	CGAffineTransform t0 = CGAffineTransformMakeTranslation (0, pickerSheets.bounds.size.height/2);
	CGAffineTransform s0 = CGAffineTransformMakeScale(0.7, 0.7);
	CGAffineTransform t1 = CGAffineTransformMakeTranslation (0, -pickerSheets.bounds.size.height/2);

    selectedsheets = 1;
    pickerSheets.frame = CGRectMake(296,120, 90 , 180);
    
    pickerSheets.transform = CGAffineTransformConcat(t0, CGAffineTransformConcat(s0, t1));
    
	pickerSheets.transform = CGAffineTransformRotate(pickerSheets.transform, -M_PI/2);
	[self.view addSubview:pickerSheets];
}

//-----------------------------------------------------------------------

-(IBAction)btnCancelClicked:(id)sender{
    [self.navigationController dismissViewControllerAnimated:true completion:nil];

}

//-----------------------------------------------------------------------

-(IBAction)btnAddClicked:(id)sender{
    
    if([txtTemplateName.text length] <= 0){
        DisplayAlertWithTitle(NSLocalizedString(@"Please Enter Template Name", nil), kAppName);
    }else{
        [delegateTemplate addNewTemplate:txtTemplateName.text :selectedsheets];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
    }
}

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    lblTemplate.text = NSLocalizedString(lblTemplate.text, nil);
    lblSheetsNumber.text = NSLocalizedString(lblSheetsNumber.text, nil);
}



//----------------------------------------------------

#pragma mark -
#pragma mark - Pickerview delegate methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return 3;
}

//----------------------------------------------------

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectedsheets = [[arrayNumberOfSheets objectAtIndex:row] intValue];
}

//----------------------------------------------------
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

//----------------------------------------------------

-(UIView *) pickerView: (UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
	CGRect rect = CGRectMake(0, 0, 30, 21);
	UILabel * label = [[UILabel alloc]initWithFrame:rect];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25]];
	label.text = [arrayNumberOfSheets objectAtIndex:row];
	label.opaque = NO;
	label.textAlignment = NSTextAlignmentCenter;
	label.backgroundColor = [UIColor clearColor];    
	label.clipsToBounds = YES;
    label.transform = CGAffineTransformRotate(label.transform, M_PI/2);
	return label;
}
//----------------------------------------------------

#pragma mark -
#pragma mark - memory management methods

- (void)viewDidUnload{
    
    lblTemplate = Nil;
    txtTemplateName = Nil;
    _strTemplateName = Nil;
    pickerSheets = nil;
    arrayNumberOfSheets = Nil;
    [super viewDidUnload];
}
//----------------------------------------------------



@end
