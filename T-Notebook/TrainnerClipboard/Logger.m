//
//  Logger.h
//  
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//


#import "Logger.h"


@implementation Logger

+ (void) debugLog:(NSString *)string{
#ifdef LOG	
#endif
}

+ (void) displayLog:(id)object withString:(NSString *)String {	
#ifdef LOG	
#endif	
}

+ (void) logInt:(int)integer withString:(NSString *)andString {
	NSNumber *number=[NSNumber numberWithInt:integer];
	[self displayLog:number withString:andString];
}

+ (void) logFloat:(float)Float withString:(NSString *)andString {
	NSNumber *number=[NSNumber numberWithFloat:Float];
	[self displayLog:number withString:andString];
}

+ (void) logBool:(BOOL)boolean withString:(NSString *)andString {
	NSNumber *number=[NSNumber numberWithBool:boolean];
	[self displayLog:number withString:andString];
}

@end
