//
//  SelectAllTemplatesVC.m
//  T-Notebook
//
//  Created by WLI on 25/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "SelectAllTemplatesVC.h"

@interface SelectAllTemplatesVC ()
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@end

@implementation SelectAllTemplatesVC



- (void)viewDidLoad {
    
    [super viewDidLoad];

    self.view.superview.layer.cornerRadius = 0.0;
    [self.tableview setFrame:CGRectMake(10, 10, 330, 180)];
    [self.tableview setDataSource:(id)self];
    [self.tableview setDelegate:(id)self];

}
//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    [super viewWillAppear:animated];
    [self.tableview reloadData];
}

//-----------------------------------------------------------------------


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//-----------------------------------------------------------------------

#pragma mark - TableView mMethods

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

//-----------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    NSInteger currentSheetID = [[[self.arrayProgramSheet objectAtIndex:section] valueForKey:@"sheetID"] integerValue];
    NSArray *arrayBlocks = [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:currentSheetID]];
    return arrayBlocks.count;

}

//-----------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.arrayProgramSheet.count;
}
//-----------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setBackgroundColor:[UIColor clearColor]];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSInteger currentSheetID = [[[self.arrayProgramSheet objectAtIndex:indexPath.section] valueForKey:@"sheetID"] integerValue];
    NSArray *arrayBlocks = [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:currentSheetID]];
    if (arrayBlocks.count) {
         cell.textLabel.text = [[arrayBlocks objectAtIndex:indexPath.row] valueForKey:@"sBlockTitle"];
    }
    return cell;
}

//-----------------------------------------------------------------------

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate selectItemAtBlockIndex:indexPath.row AtSheetIndex :indexPath.section];
}
//-----------------------------------------------------------------------

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // to remove seprator space
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 30)];
    
    [view setBackgroundColor:[UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 3, 340, 20)];
    [label setText:[[self.arrayProgramSheet objectAtIndex:section] valueForKey:@"sheetName"]];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    [view addSubview:label];
    return view;
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc]initWithFrame:CGRectZero];
}
@end
