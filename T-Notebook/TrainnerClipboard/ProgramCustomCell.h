//
//  ProgramCustomCell.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCopy;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnEditLine;


@property (strong, nonatomic) IBOutlet UILabel *lblExercise;
@property (strong, nonatomic) IBOutlet UILabel *lblWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblSet;
@property (strong, nonatomic) IBOutlet UILabel *lblReps;
@property (strong, nonatomic) IBOutlet UILabel *lblRestTime;

@property (strong, nonatomic) IBOutlet UIImageView *imgvFirstLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvSecondLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvThirdLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvFourthLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvFifthLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvSixLine;
@property (strong, nonatomic) IBOutlet UILabel     *lblCellsepetator;
@property (nonatomic) BOOL                         setLandscapeMode;
@property (weak, nonatomic) IBOutlet UILabel *lblColor;



-(void)configCell :(NSDictionary *)str;
-(void)setupLayoutForCell;

@end
