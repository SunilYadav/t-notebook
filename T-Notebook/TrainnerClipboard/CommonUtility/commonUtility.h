//
//  commonUtility.h
//  TrainnerClipboard
//
//  Created by Sunil on 07/01/16.
//  Copyright (c) 2016 WiOS Monika R. Brahmbhatt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface commonUtility : NSObject

//Method fro save Version
+(void)saveUpdatedVersion: (NSString *)appVersion;
//Retrieve
+(NSString  *)retrieveUpdatedVersion;

//Check Version
+(BOOL)isUpdatedVersionApp;


+(void)saveValue: (NSString *)name andKey:(NSString *)keyName;
+(NSString  *)retrieveValue:(NSString *)keyName;
+(NSString *)escapeSingleQute:(NSString *)strText;
+(NSString *)addSingleQute:(NSString *)str;

//For Show Alert
+(void)alertMessage:(NSString *)msg;


//Validate Name
+(BOOL) validateName:(NSString *)strName;

//Validate Email
+ (BOOL)validateEmailWithString:(NSString*)email;

//Convert null to blank for all values in dictionry
+(NSDictionary *) dictionaryByReplacingNullsWithStrings:(NSDictionary *)dict;

//method for Post Request
+(void)postRequest:(NSString * )  strFilePath;


//Log out
+(void)logOut;
+(NSString *)getUDID;
+(NSString *)getDeviceInfo;

@end
