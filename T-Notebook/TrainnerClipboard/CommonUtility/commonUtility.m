//
//  commonUtility.m
//  TrainnerClipboard
//
//  Created by Sunil on 07/01/16.
//  Copyright (c) 2016 WiOS Monika R. Brahmbhatt. All rights reserved.
//


#define DISMIS_ALERT_INDEX          -1


#import "commonUtility.h"

@implementation commonUtility


//TODO: SAVE/RETRIVE NAME AND PASSWORD
/*!
 @Description : Method of save value for use in next time in NSUserDefaults
 @Param      : NSString - name
 @Return      : void
 */
+(void)saveValue: (NSString *)name andKey:(NSString *)keyName
{
    START_METHOD
    // Store the Name
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject : name forKey : keyName ];
    [defaults synchronize];
    NSLog(@"Value saved : %@ and key: %@",name,keyName);
    END_METHOD
}

/*
 @Description : Method of retrieve value by key from NSUserDefaults
 @Params      : Nothing
 Return Value : NSString
 */
+(NSString  *)retrieveValue:(NSString *)keyName
{
    START_METHOD
    // Get the stored data before the view loads
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString       *name     = [defaults objectForKey:keyName];
    NSLog(@"the  keyName: %@ aand value :%@",keyName,name);
    END_METHOD
    return name;
    
}

/*!
 @Description : Methods for remove '' to '
 @Param       :
 @Return      :
 */
+(NSString *)escapeSingleQute:(NSString *)strText{
    NSMutableString *strTemp    =   [strText mutableCopy];
    
    [strTemp replaceOccurrencesOfString:@"''" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, strTemp.length)];
    
    return  strTemp;
}



/*!
 @Description : Methods for return string after escape
 @Param       :
 @Return      :
 */
+(NSString *)addSingleQute:(NSString *)str{
    START_METHOD
    if (str.length>0) {
        NSMutableString *strTemp    =   [str mutableCopy];
        [strTemp replaceOccurrencesOfString:@"'" withString:@"''" options:NSCaseInsensitiveSearch range:NSMakeRange(0, strTemp.length)];
        
       
        
        
        MESSAGE(@"escape string : %@",strTemp);
        
        return strTemp;
    }
    return nil;
    END_METHOD
}


//TODO: Alert
/*
 @Description : Method for Show Alert
 @Params      : NSString
 Return Value : void
 */
+(void)alertMessage:(NSString *)msg
{
    START_METHOD
    //create alertView
    UIAlertView *alertMessage = [[UIAlertView alloc]
                                 initWithTitle:@"" message:[NSString stringWithString:msg]
                                 delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    //Show alertView
    [alertMessage show];
    
    //For close automatically alert view after 2Second
    [self performSelector:@selector(autoHide:) withObject:alertMessage afterDelay:2.0];
    END_METHOD
}
/*
 @Description : Method for auto hide Alert
 @Params      : NSString
 Return Value : void
 */
+(void)autoHide:(UIAlertView*)alertView
{
    [alertView dismissWithClickedButtonIndex:DISMIS_ALERT_INDEX animated:YES];
}


/*
 @Description : Method for validate NAme With String
 @Params      : NSString
 Return Value : BOOL
 */
+(BOOL) validateName:(NSString *)strName
{
    NSString *stricterFilterString = @"[A-Za-z ]*";
    
    NSPredicate *stringTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [stringTest evaluateWithObject:strName];
}
/*
 @Description : Method for validate Email With String
 @Params      : NSString
 Return Value : BOOL
 */
+ (BOOL)validateEmailWithString:(NSString*)email
{
    START_METHOD
    NSString    *emailChar = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailChar];
    END_METHOD
    return [emailTest evaluateWithObject:email];
}



/*!
 @Description : Methods for Compare Version App
 @Param       :
 @Return      : Retus If Updated
 */
+(BOOL)isUpdatedVersionApp{
    
    START_METHOD
    NSString *strVersion =   [NSString stringWithFormat:@"Version %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    MESSAGE(@"strVersion-------> %@",strVersion);
    
    //If Current Version is updated version then Returns YES else NO
    if([[commonUtility retrieveUpdatedVersion] isEqualToString:strVersion]){
        
        MESSAGE(@"strVersion------->alreday :  %@",strVersion);
        
        return  YES;
        
    }else{
        
        MESSAGE(@"strVersion-------> %@",strVersion);
        
        //If version updated Then Save current as previous
        [commonUtility saveUpdatedVersion:strVersion];
        
        return  NO;
        
    }
    END_METHOD
}



/*!
 @Description : Method of save LANG_ID for next time LOgin
 @Param      : NSString - name
 @Return      : void
 */
+(void)saveUpdatedVersion: (NSString *)appVersion
{
    START_METHOD
    // Store the Name
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject : appVersion forKey : KEY_APP_VERSION ];
    [defaults synchronize];
    MESSAGE(@"savePreviousVersion saved : %@",appVersion);
    END_METHOD
}


/*
 @Description : Method of retrieve LANG_IDe
 @Params      : Nothing
 Return Value : NSString
 */
+(NSString  *)retrieveUpdatedVersion{
    START_METHOD
    // Get the stored data before the view loads
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString       *name     = [defaults objectForKey:KEY_APP_VERSION];
    MESSAGE(@"retrieveUpdatedVersion  retiorive: %@ ",name);
    END_METHOD
    return name;
    
}


//TODO: Dictionry -->
#pragma mark - Convert Null values with blank for all key in dictionry

+(NSDictionary *) dictionaryByReplacingNullsWithStrings:(NSDictionary *)dict {
    
    MESSAGE(@"dictionaryByReplacingNullsWithStrings-> %@",dict);
    
    const NSMutableDictionary *replaced = [dict mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in [replaced allKeys]) {
        const id object = [replaced objectForKey: key];
        if (object == nul) {
            [replaced setObject: blank forKey: key];
        }
        else if ([object isKindOfClass: [NSDictionary class]]) {
            MESSAGE(@"found null inside and key is %@", key);
            [replaced setObject:[self replaceNullInNested:object] forKey:key];
        }
        
    }
    MESSAGE(@"replaced: %@", replaced);
    return (NSDictionary *)replaced;
    
}

//If nested dictionry
+(NSMutableDictionary *)replaceNullInNested:(NSDictionary *)targetDict
{
    //make it to be NSMutableDictionary in case that it is nsdictionary
    NSMutableDictionary *m = [targetDict mutableCopy];
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary: m];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in [replaced allKeys]) {
        const id object = [replaced objectForKey: key];
        if (object == nul) {
            [replaced setObject: blank forKey: key];
        }
        else if ([object isKindOfClass: [NSArray class]]) {
            MESSAGE(@"found null inside and key is %@", key);
            //make it to be able to set value by create a new one
            NSMutableArray *a = [object mutableCopy];
            for (int i =0; i< [a count]; i++) {
                
                for (NSString *subKey in [[a objectAtIndex:i] allKeys]) {
                    //                    NSLog(@"key: %@", subKey);
                    //                    NSLog(@"value: %@", [[object objectAtIndex:i] valueForKey:subKey]);
                    if ([[object objectAtIndex:i] valueForKey:subKey] == nul) {
                        [[object objectAtIndex:i] setValue:blank forKey:subKey];
                    }
                }
            }
            //replace the updated one with old one
            [replaced setObject:a forKey:key];
            
        }
        
    }
    
    return replaced;
}
//TODO: Dictionry <----




//TODO : Move to Dashboard for lOg out
+(void)logOut{
    START_METHOD
    
    //Save lout Key
    [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
    
     //Push view
    UINavigationController *nav=[[AppDelegate sharedInstance]nav];
    
    [nav popViewControllerAnimated:NO];
}



//Post Requset

//TODO: POST MIGRATE TEXT FILE

//Method for post request for SIGNIN
+(void)postRequest:(NSString * )  strFilePath  {
    START_METHOD
    /*
    
    //Carete URL by string
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:strFilePath];
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             USERNAME           :   @"",
                             
                             };
    
    
    //Make url for hit the sign api
    
    NSString *strUrl =[NSString stringWithFormat:@"%@migrationFileUpload",HOST_URL];
    
    MESSAGE(@"postRequestToMigrateAllData-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postDataWithFile:strUrl parameters:params filePath:fileURL
                         success:^(NSDictionary *responseDataDictionary) {
                             
                             MESSAGE(@"responce from file: %@", responseDataDictionary);
                             
                             //Hide the indicator
                             [TheAppController hideHUDAfterDelay:0];
                             
                             [commonUtility alertMessage:@"Successfully Saved"];
                             
                         }failure:^(NSError *error) {
                             MESSAGE(@"Eror : %@",error);
                             
                             //Hide the indicator
                             [TheAppController hideHUDAfterDelay:0];
                             
                             //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                            
                             
                         }];
     */
    END_METHOD
}


+(NSString *)getUDID{
    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    MESSAGE(@"output is : %@", Identifier);
    
    if(Identifier && Identifier.length>1){
    return Identifier;
    }else{
        
       return @"fakeId";
    }
    
}


//Get the device info
+(NSString *)getDeviceInfo{

        NSString* Identifier                = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSString* strDeviceName             = [UIDevice currentDevice].name;
        NSString* strDeviceModel            = [UIDevice currentDevice].model;
        NSString* strDeviceSyatemName       = [UIDevice currentDevice].systemName;
        NSString* strDeviceLocalizedModel   = [UIDevice currentDevice].localizedModel;
        NSString* strDeviceSystemVersion    = [UIDevice currentDevice].systemVersion;
    
    
    //Set full info in one string
    NSString *strDeviceInfo =   [NSString stringWithFormat:@"Identifier: %@ strDeviceName:%@ strDeviceModel:%@ strDeviceSyatemName:%@ strDeviceLocalizedModel:%@ strDeviceSystemVersion:%@",Identifier,strDeviceName,strDeviceModel,strDeviceSyatemName,strDeviceLocalizedModel,strDeviceSystemVersion];
    
    
    MESSAGE(@"output is : %@", strDeviceInfo);
    
    if(strDeviceInfo && strDeviceInfo.length>1){
        return strDeviceInfo;
    }else{
        
        return @"NoDeviceInfo";
    }
    
}
@end


