//
//  NotesCustomCell.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDate;
@property (strong, nonatomic) IBOutlet UILabel *lblDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnEditNotes;
@property (nonatomic)                  BOOL     setLandscapeCell;
@property (nonatomic) NSString *str;
-(void)configCell :(NSString *)str;

@end
