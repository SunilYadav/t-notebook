//
//  AppDelegate.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "NavigationViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "SignViewController.h"

#import "DataManager.h"
#import "AudioPlayer.h"
#import "FileUtility.h"
#import "ParQController.h"
#import "Appirater.h"
#import <HockeySDK/HockeySDK.h>

@interface AppDelegate () <AVAudioSessionDelegate> {
    AudioPlayer * player;
    MPMusicPlayerController *c;
    MPMediaItem *currentSongForiPodMusicPlayer;
    NSTimeInterval currentPlaybackTimeForIpodMusicPlayer;
    MPMediaItem *currentSongForApplicationPlayer;
    NSTimeInterval currentPlaybackTimeForApplicationPlayer;
    BOOL isAppPlayerSet;
}
@property (nonatomic) BOOL						isReachable;
@property (assign, nonatomic) NSTimeInterval    playbackTime;


@end

@implementation UINavigationController(KeyboardDismiss)



- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end
@implementation AppDelegate

@synthesize window = _window;

#pragma mark - Class Helper Methods -

-(UIWindow *)mainWindow{
    return self.window;
}



@synthesize isReachable;
@synthesize intRemaningTimeForPhase;
@synthesize isFieldUpdated,isAutoSave;

NSString * const VERSION_KEY = @"version";

#pragma mark -
#pragma mark Application lifecycle

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

static AppDelegate * myDelegate = nil;
+ (AppDelegate *) sharedInstance
{
    if (myDelegate == nil) {
        myDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        return myDelegate;
    }
    return myDelegate;
    
}

//-----------------------------------------------------------------------

- (MPMusicPlayerController *)applicationMusicPlayer {
    static dispatch_once_t onceToken;
    __strong static MPMusicPlayerController *applicationMusicPlayer;
    dispatch_once(&onceToken, ^{
        applicationMusicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    });
    return applicationMusicPlayer;
}

//-----------------------------------------------------------------------

- (MPMusicPlayerController *)ipodMusicPlayer {
    static dispatch_once_t onceToken;
    __strong static MPMusicPlayerController *ipodMusicPlayer;
    dispatch_once(&onceToken, ^{
        ipodMusicPlayer = [MPMusicPlayerController iPodMusicPlayer];
    });
    return ipodMusicPlayer;
}

//-----------------------------------------------------------------------

//- (void)NowPlayingItemChanged:(NSNotification *)notification {
//
//}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    //Check the Updeatd version of app and Set Current version
     [commonUtility isUpdatedVersionApp];

    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    MESSAGE(@"language = %@",language);
    
    MESSAGE(@"Locale Identifier : %@", [[NSLocale currentLocale] localeIdentifier]);
    
    MESSAGE(@"NSUserDefaults AppleLanguages : %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);
    
    _isPausePlayer = @"DontKnow";
    self.isiPadPlaying = @"NO";
    

    _appPlayer = [[AppDelegate sharedInstance] applicationMusicPlayer];
    
    if([MPMusicPlayerController iPodMusicPlayer].playbackState == MPMusicPlaybackStatePlaying) {
        _ipodPlayer = [[AppDelegate sharedInstance] ipodMusicPlayer];
        [AppDelegate sharedInstance].isIpodPlaying = @"Play";
    } else {
        _ipodPlayer = nil;
    }
    

    self.IsPreviousVersion = TRUE;

    [self copyDatabaseIfNeeded];
    
    //==========Add Last edited screen to client table ================//
    
    [[DataManager initDB] addNewFieldToClientTableIfDoesNotExist];
   
    
    //=====================================================//
    
    
    SignViewController *ViewControllerVCOBJ =  [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignViewController"];
    [self makeRootView:ViewControllerVCOBJ];
    
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kisnsUserDefaultAvilable]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kisnsUserDefaultAvilable];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    [self performSelector:@selector(CreateTableIfNotExist) withObject:nil afterDelay:0.2];
    [self performSelector:@selector(CreateTableIfNotExist_OLD) withObject:nil afterDelay:0.5];
    
    
    
    isFieldUpdated = NO;
    //set All Default Data
    
    @try {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] == nil) {
            NSDictionary * dict = [[NSBundle mainBundle] infoDictionary];
            if([[dict objectForKey:@"isFullVersion"] boolValue]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:kisFullVersion];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    @catch (NSException *exception) {
    }
    
    
    isAutoSave = TRUE;
    [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"AutoSave"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    @try {
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"AutoSave"] == nil) {
            
            [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"AutoSave"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            isAutoSave = YES;
            
        } else {
            if([[NSUserDefaults standardUserDefaults]boolForKey:@"AutoSave"]) {
                isAutoSave = YES;
                
            } else {
                isAutoSave = FALSE;
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
    
    @try {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kIsTimerPlaying] == nil) {
            [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:kIsTimerPlaying];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    @catch (NSException *exception) {
    }
    
    
    @try {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kgender] == nil) {
            [[NSUserDefaults standardUserDefaults] setObject:@"F" forKey:kgender];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    @catch (NSException *exception) {
    }
    
    isFieldUpdated = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    if (!self.isReachable) {
        wifiReach = [Reachability reachabilityForLocalWiFi];
        [wifiReach startNotifier];
        [self updateInterfaceWithReachability: wifiReach];
    }
    
    [[NSUserDefaults standardUserDefaults ] setValue:@"Weekly" forKey:@"SelectedCalenderEvent"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsIntervalPlaying];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPaused];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPlaying];

    
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    // ======== Apprater Integration ===========//
    
    if([bundleIdentifier isEqualToString:@"com.thetrainingnotebook.TNotebook"]) {
        
        [Appirater setAppId:kAppraterIdFullVersion];
        
    } else if ([bundleIdentifier isEqualToString:@"com.thetrainingnotebook.TNotebookLite"]) {
        
        [Appirater setAppId:kAppraterIdLiteVersion];
    }
    
#ifdef DEBUG
    [Appirater setDebug:NO];
#else
    [Appirater setDebug:NO];
#endif
    [Appirater setDaysUntilPrompt:7];
    [Appirater setUsesUntilPrompt:5];
    [Appirater appLaunched:YES];
    
    
    
    //========= HockeySDK Integration========//
    
    // get bundle identifier
    
    if([bundleIdentifier isEqualToString:@"com.thetrainingnotebook.TNotebook"]) {
        
        // is full version
#ifdef BETA
        // configure beta
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:KHockeyFullBetaAppId];
        
#else
        // Configure live
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:kHockeyFullLiveAppId];
        
#endif
        
    } else if([bundleIdentifier isEqualToString:@"com.thetrainingnotebook.TNotebookLite"]) {
        
        // lite Version
#ifdef BETA
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:kHockeyLiteBetaAppId];
        
#else
        [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:kHockeyLiteLiveId];
        
#endif
    }
    
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    
    
    
    return YES;
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)makeRootView:(UIViewController *)vc {
    
    self.nav=[[UINavigationController alloc]initWithRootViewController:vc];
    self.nav.navigationBar.translucent=false;
    
    UIWindow *window=[[AppDelegate sharedInstance]window];
    
    window.rootViewController=self.nav;
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark Reachability methods

- (void) checkReachable: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];

    switch (netStatus)
    {
        case NotReachable:
        {
            if (curReach==internetReach) {
                self.isReachable = FALSE;
            }
            else if(curReach==wifiReach)
            {

            }
            break;
        }
            
        case ReachableViaWWAN:
        {
            self.isReachable = TRUE;
            break;
        }
        case ReachableViaWiFi:
        {
            self.isReachable = TRUE;
            break;
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    if(curReach == internetReach)
    {
        [self checkReachable:internetReach];
    }
    else if(curReach == wifiReach)
    {
        [self checkReachable:wifiReach];
    }
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) reachabilityChanged: (NSNotification* )note
{
    
    Reachability* curReach = [note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    [self updateInterfaceWithReachability: curReach];
    
}

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (NSString *) checkForNull:(NSString *)strVal {
    if (!strVal || [strVal isEqual:[NSNull null]] || [strVal isEqualToString:@"(null)"] || [allTrim(strVal) length] == 0) {
        return @"";
    }
    return allTrim(strVal);
}

//-----------------------------------------------------------------------

- (BOOL)isNotEmptyArray:(NSArray *)array {
    if(array != nil && ![array isEqual:[NSNull null]] && array.count > 0) {
        return YES;
    }
    return NO;
}

//-----------------------------------------------------------------------

- (CGRect)getWidth:(NSString *)strData font:(UIFont *)font height:(CGFloat)height {
    
    if (!strData || [strData isEqual:[NSNull null]] || [strData isEqualToString:@"(null)"] || [allTrim(strData) length] == 0) {
        return CGRectMake(0, 0, 0, 0);
    } else {
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:strData
                                                                               attributes:@{NSFontAttributeName:font}];
        CGRect rect;
        rect = [attributedString boundingRectWithSize:CGSizeMake(FLT_MAX,height) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        return rect;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Timer Methods

//-----------------------------------------------------------------------

- (void) getRemainTime_Timer {
    
    BOOL isTimeCompleted = FALSE;
    
    self.strCurrentTime_Timer = [self getStringInTimeFormat:self.remainTimeInterVal_Timer];
    self.remainTimeInterVal_Timer = [NSNumber numberWithInt:([self.remainTimeInterVal_Timer intValue] - 1)];
    int value = [self.remainTimeInterVal_Timer intValue];
    
    if(value < 0) {
        
        self.remainTimeInterVal_Timer = self.actualTimeInterVal_Timer;
        isTimeCompleted = TRUE;
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPlaying];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getRemainTime_Timer) object:nil];
        [self playSound:6];
    } else {
        if(!isTimeCompleted) {
            [self performSelector:@selector(getRemainTime_Timer) withObject:nil afterDelay:1.0];
        }
    }
    
}

//-----------------------------------------------------------------------

- (void) pausedTimer {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getRemainTime_Timer) object:nil];
}

//-----------------------------------------------------------------------

-(void)createNotification {
    
    UILocalNotification * alarm = [[UILocalNotification alloc] init];
    if (alarm) {
        alarm.fireDate = [[NSDate date] dateByAddingTimeInterval:[self.remainTimeInterVal_Timer doubleValue]];
        alarm.repeatInterval = 0;
        alarm.timeZone = [NSTimeZone defaultTimeZone];
        [alarm setAlertBody:[NSString stringWithFormat:NSLocalizedString(@"Your Timer is completed", nil)]];
        [alarm setAlertAction:@"Show Now"];
        [[UIApplication sharedApplication] scheduleLocalNotification:alarm];
    }
}
//-----------------------------------------------------------------------

- (void) setupData_FromForeGround {
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    NSDate * BackgroundTime = [[NSUserDefaults standardUserDefaults] valueForKey:kBackgroundTime_Timer];
    double interval = [[NSDate date] timeIntervalSinceDate:BackgroundTime];
    
    if(interval >= [self.remainTimeInterVal_Timer doubleValue]){
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPlaying];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPaused];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self pausedTimer];
    }else{
        self.remainTimeInterVal_Timer = [NSNumber numberWithDouble:([self.remainTimeInterVal_Timer doubleValue] - interval)];
    }
    
}


//---------------------------------------------------------------------------------------------------------------

- (NSString *) getStringInTimeFormat : (NSNumber *) timeInterVal {
    
    NSString * strTime = nil;
    int totalsecond = [timeInterVal intValue];
    int sec = totalsecond % 60;
    int min = (totalsecond / 60) % 60;
    int hour = totalsecond / 3600;
    
    NSString *hr;
    NSString *mints;
    NSString *secnd;
    
    if(sec < 10) {
        if(sec<0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",sec] componentsSeparatedByString:@"-" ];
            secnd = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            secnd = [NSString stringWithFormat:@"0%d",sec];
        }
    }else {
        secnd = [NSString stringWithFormat:@"%d",sec];
    }
    
    if(min < 10) {
        if(min <0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",min] componentsSeparatedByString:@"-" ];
            secnd = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            mints = [NSString stringWithFormat:@"0%d",min];
        }
    } else {
        mints = [NSString stringWithFormat:@"%d",min];
    }
    
    if(hour < 10) {
        if(hour < 0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",hour] componentsSeparatedByString:@"-" ];
            secnd = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            hr = [NSString stringWithFormat:@"0%d",hour];
        }
    } else {
        hr = [NSString stringWithFormat:@"%d",hour];
    }
    
    if(hour > 0){
        strTime = [NSString stringWithFormat:@"%@:%@:%@",hr,mints,secnd];
    } else {
        strTime = [NSString stringWithFormat:@"%@:%@",mints,secnd];
    }
    return strTime;
}



//---------------------------------------------------------------------------------------------------------------

- (void) setSoundUrls {
    
    self.arrPlayers = [[NSMutableArray alloc]init];
    
    [self setupPlayerArray:@"Session_Start"];          // Session Start soundFile
    [self setupPlayerArray:@"Session_Complition"];     // Session Complete soundFile
    [self setupPlayerArray:@"Work_Start"];             // Work Start soundFile
    [self setupPlayerArray:@"Complete_IntervalTimer"]; // Timer complete SoundFile
    [self setupPlayerArray:@"LapInsert_StopWatch"];    // Lap Insert SoundFile
    [self setupPlayerArray:@"TimerCompleted"];         // Timer Completed SoundFile
}

//-----------------------------------------------------------------------

- (void) setupPlayerArray : (NSString *) strSoundFileName {
    
    // Session Start soundFile
    NSString * audioPAth = [[NSBundle mainBundle] pathForResource:strSoundFileName ofType:@"wav"];
    NSURL * pathUrl=nil;
    
    @try {
        if ([FileUtility fileExists:audioPAth]) {
            pathUrl= [NSURL fileURLWithPath:audioPAth];
        }
        else{
            pathUrl= nil;
        }
        
        if(player == nil){
            player = [[AudioPlayer alloc]init];
            player.apDelegate = (id)self;
            if (pathUrl != nil) {
                [player setAudioFromPathUrl:pathUrl];
                [self.arrPlayers addObject:player];
            }
            player = nil;
        }
        
    }
    @catch (NSException *exception) {
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void) playSound : (int) intSoundFile {
    

    if([self.arrPlayers count] >= 5){
        if(intSoundFile == 1){
            [[self.arrPlayers objectAtIndex:2] startPlaying];
        }else if (intSoundFile == 2){
            [[self.arrPlayers objectAtIndex:0] startPlaying];
        }else if (intSoundFile == 3){
            [[self.arrPlayers objectAtIndex:1] startPlaying];
        }else if (intSoundFile == 4){
            [[self.arrPlayers objectAtIndex:3] startPlaying];
        }else if (intSoundFile == 5){
            
            [[self.arrPlayers objectAtIndex:4] startPlaying];
        }else if (intSoundFile == 6){
            [[self.arrPlayers objectAtIndex:5] startPlaying];
        }
    }
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Alert Methods

//---------------------------------------------------------------------------------------------------------------

- (void) hideProgressAlert{
    if (self.progressAlert) {
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
        [self.progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        [self.progressAlert removeFromSuperview];
        self.activityIndicator=nil;
        self.progressAlert=nil;
    }
}

//--------------------------------------------------------------------------------------------------

- (void) showProgressAlert:(NSString *)alertMessage{
    if(self.progressAlert==nil){
        _progressAlert = [[UIAlertView alloc]
                          initWithTitle:kAppName
                          message:[NSString stringWithFormat:NSLocalizedString(@"%@\nPlease wait...", nil),alertMessage]
                          delegate:self cancelButtonTitle:nil
                          otherButtonTitles: nil];
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
        [self.activityIndicator setCenter:CGPointMake(140.0f, 122.0f)];
        [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [self.progressAlert show];
        [self.progressAlert addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
    }
}


//---------------------------------------------------------------------------------------------------------------

-(void)showAlert:(NSString *)alertMessage {
    
    if(self.alertView == nil) {
        _alertView = [[UIAlertView alloc]initWithTitle:kAppName message:alertMessage delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [self.alertView show];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(void) hideAlertView {
    if(self.alertView) {
        [self.progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        [self.progressAlert removeFromSuperview];
        self.progressAlert=nil;
    }
}


//-----------------------------------------------------------------------

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    int t = (int)alertView.tag;
    if (buttonIndex == 0 && t == 5000) {
        [self showProgressAlert:NSLocalizedString(@"Purchase in progress", nil)];
        [[CStoreKit sharedInstance]buy:kFullAccessProductId delegate:(id)self];
    }
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : StoreKit Delegate MEthods

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self hideProgressAlert];
    [[NSNotificationCenter defaultCenter] postNotificationName:kupdateVersionNotification object:nil];
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseFailed:(NSString*)productId error:(NSString *)error {
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction failed, Please try again", nil)];
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseCancelled:(NSString*)productId {
    
    [self hideProgressAlert];
    [self showAlert:NSLocalizedString(@"Transaction Cancelled, Please try again", nil)];
}

//-----------------------------------------------------------------------

#pragma mark - APDelegate Method

//-----------------------------------------------------------------------

-(void) stopPlayingAudio {
    
    if(self.intRemainingTimeOfPhase < 4 && self.intRemainingTimeOfPhase > 1 && [[NSUserDefaults standardUserDefaults]boolForKey:kIsIntervalPlaying]){
        [[self.arrPlayers objectAtIndex:1] startPlaying];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Music Player Methods



//-----------------------------------------------------------------------------------------------------------------------------

- (void) stopMusic {
    BOOL b = [[NSUserDefaults standardUserDefaults] boolForKey:kMusicMode];
    if (b) {
        [self.appPlayer stop];
        self.isPause = NO;
    }
    else {
        if (self.appPlayer!=nil) {
        }
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kMusicMode];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}


//-----------------------------------------------------------------------------------------------------------------------------

- (void) pauseMusic {
    
    BOOL b = [[NSUserDefaults standardUserDefaults] boolForKey:kMusicMode];
    if (b && [[AppDelegate sharedInstance].isAppPlayerPlaying isEqualToString:@"Play"]) {
        self.isPause = YES;
        currentSongForApplicationPlayer = [self.appPlayer nowPlayingItem];
        currentPlaybackTimeForApplicationPlayer = [self.appPlayer currentPlaybackTime];
        
        [self.appPlayer pause];
        [AppDelegate sharedInstance].isAppPlayerPlaying = @"Pause";
    }
    else {
        if (self.appPlayer!=nil) {
        }

    }
}

//-----------------------------------------------------------------------------------------------------------------------------


#pragma mark -
#pragma mark prior to database work methods

- (NSString *) getDBPath {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString * documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"TrainerClipboardDatabase.sqlite"];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) copyDatabaseIfNeeded {
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSError * error;
    NSString * dbPath = [self getDBPath];
    
    
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *previousVersion=[defaults objectForKey:VERSION_KEY];
    
    
    if (previousVersion==nil) {
        self.IsPreviousVersion = FALSE;
    }
    
    if(!success) {
        self.IsPreviousVersion = TRUE;
        NSString * defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"TrainerClipboardDatabase.sqlite"];
        
        MESSAGE(@"defaultDBPath: %@",defaultDBPath);
        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (NSString *)versionNumberString {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return majorVersion;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) CreateTableIfNotExist_OLD{
    NSArray *array = [[DataManager initDB]getClientsID];
    int i;
    for(i=0;i<[array count];i++){
        [[DataManager initDB] createClientTablesIfNotExist:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) CreateTableIfNotExist{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *previousVersion=[defaults objectForKey:VERSION_KEY];
    NSString *currentVersion=[self versionNumberString];
    
    if(previousVersion != nil) {
        return;
    }
    
    if(previousVersion == nil || [previousVersion compare:currentVersion options:NSNumericSearch] == NSOrderedAscending) {
        
        
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        
        
        NSArray *array = [[DataManager initDB]getClientsID];
        int i;
        
        @try {
            
            for (i = 0; i< [array count]; i++) {
                [[DataManager initDB] createClientTablesIfNotExist:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
                [[DataManager initDB] addNewFieldsToTablesIfnotAddedForClientId:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
            }
            [defaults setObject:currentVersion forKey:VERSION_KEY];
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:kAppUpdated object:nil];
            
        }
        @catch (NSException *exception) {
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:kAppUpdated object:nil];
            return;
        }
        
    }

    
}

//---------------------------------------------------------------------------------------------------------------


- (void)applicationWillResignActive:(UIApplication *)application {
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:kIsIntervalPlaying]){
        [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:kBackgroundTime_InterVal];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self createNotification];
    }
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    self.isFromBackground = YES;
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:kStartTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[AppDelegate sharedInstance] pauseMusic];
    
    if ([[AppDelegate sharedInstance].isIpodPlaying isEqualToString:@"Pause"]) {
        
        NSString * str = [[NSUserDefaults standardUserDefaults] valueForKey:@"ipodSong"];
        DebugLOG(str);
        MPMediaQuery * query = [MPMediaQuery songsQuery];
        NSArray * temp = [NSArray arrayWithArray:[query collections]];
        [self.ipodPlayer setQueueWithQuery:query];
        self.ipodPlayer.nowPlayingItem = currentSongForiPodMusicPlayer;
        [self.ipodPlayer setCurrentPlaybackTime:currentPlaybackTimeForIpodMusicPlayer];
        [self.ipodPlayer play];
        [AppDelegate sharedInstance].isIpodPlaying = @"Play";
    }
 
}

//-----------------------------------------------------------------------

- (void) playMusic {
    

    
    if (self.ipodPlayer != nil && [[AppDelegate sharedInstance].isIpodPlaying isEqualToString:@"Play"] && [[AppDelegate sharedInstance].isAppPlayerPlaying isEqualToString:@"Pause"]) {
        currentSongForiPodMusicPlayer = [self.ipodPlayer nowPlayingItem];
        currentPlaybackTimeForIpodMusicPlayer = [self.ipodPlayer currentPlaybackTime];
        NSString * strPlaylist = [currentSongForiPodMusicPlayer valueForProperty:MPMediaItemPropertyTitle];
        [[NSUserDefaults standardUserDefaults] setValue:strPlaylist forKey:@"ipodSong"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.ipodPlayer pause];
        [AppDelegate sharedInstance].isIpodPlaying = @"Pause";
    }
    
    NSString * str = [[NSUserDefaults standardUserDefaults] valueForKey:kPlayListName];
    DebugLOG(str);
    MPMediaQuery * query = [MPMediaQuery playlistsQuery];
    NSArray * temp = [NSArray arrayWithArray:[query collections]];
    BOOL b = [[NSUserDefaults standardUserDefaults] boolForKey:kMusicMode];
    if (b) {
        for (int i= 0; i<[temp count]; i++) {
            MESSAGE(@"MPMediaPlaylistPropertyName %@",[[temp objectAtIndex:i] valueForKey:MPMediaPlaylistPropertyName]);
            if ([str isEqualToString:[[temp objectAtIndex:i] valueForProperty:MPMediaPlaylistPropertyName]]) {
                self.playList = [temp objectAtIndex:i];
                b = YES;
                break;
            }
            else {
                b = NO;
            }
        }
    }
    
    
    if (self.appPlayer == nil) {
        _appPlayer = [[AppDelegate sharedInstance] applicationMusicPlayer];
    }
    if (b) {
        if (!self.isPause) {
            
            
            
            BOOL s = [[NSUserDefaults standardUserDefaults] boolForKey:kSuffleMode];
            if (s) {
                [self.appPlayer setShuffleMode:MPMusicShuffleModeSongs];
            }
            else {
                [self.appPlayer setShuffleMode:MPMusicShuffleModeOff];
            }
            BOOL r = [[NSUserDefaults standardUserDefaults] boolForKey:kRepeatMode];
            if (r) {
                [self.appPlayer setRepeatMode:MPMusicRepeatModeAll];
            }
            else {
                [self.appPlayer setRepeatMode:MPMusicRepeatModeNone];
            }
            [self.appPlayer setQueueWithItemCollection:self.playList];
            [AppDelegate sharedInstance].isAppPlayerPlaying = @"Play";
            [self.appPlayer play];
        }
        else {
            
            NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:@"btnPlayPause"];
            if ([[AppDelegate sharedInstance].isAppPlayerPlaying isEqualToString:@"Pause"] && ![str isEqualToString:@"btnPause"] ) {
                [AppDelegate sharedInstance].isAppPlayerPlaying = @"Play";
                [self.appPlayer setNowPlayingItem:currentSongForApplicationPlayer];
                [self.appPlayer setCurrentPlaybackTime:currentPlaybackTimeForApplicationPlayer];
                [self.appPlayer play];
            }
        }
    }
    else {
        if (self.appPlayer!=nil) {
        }
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kMusicMode];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//-----------------------------------------------------------------------

- (void)applicationWillEnterForeground:(UIApplication *)application {
    

    
    if ([[AppDelegate sharedInstance].isAppPlayerPlaying isEqualToString:@"Pause"]) {
        [[AppDelegate sharedInstance] playMusic];
    }
    
    NSString *strMsg = NSLocalizedString(@"You are using limited features of The Training Notebook Lite. Would you like to unlock all the features?", nil);
    
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier isEqualToString:@"com.thetrainingnotebook.TNotebookLite"]) {
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion] isEqualToString:@"N"]) {
//            DisplayAlertWithYesNo(strMsg,kAppName, self);
            
            //Remove alert for Subcription
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:kEndTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:kIsTimerPlaying]){
        [self setupData_FromForeGround];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:kIsIntervalPlaying]){ // Interval is playing when user move to background.
        
        NSDate * prevDate = [[NSUserDefaults standardUserDefaults] objectForKey:kBackgroundTime_InterVal];
        NSDate * date = [NSDate date];
        NSTimeInterval timeElaps = [date timeIntervalSinceDate:prevDate];
        self.isFromBackground = FALSE;
        [[NSNotificationCenter defaultCenter] postNotificationName:kInterValNotification object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:timeElaps],@"TimeElaps", nil]];
        
    }
    
    [Appirater appEnteredForeground:YES];
    
    self.isFromBackground = FALSE;
    
}


//-----------------------------------------------------------------------

#pragma mark - didReceiveLocalNotification Method

//-----------------------------------------------------------------------

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPlaying];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsTimerPaused];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
    [self pausedTimer];
    self.remainTimeInterVal_Timer = [NSNumber numberWithDouble:0];
    self.actualTimeInterVal_Timer = [NSNumber numberWithDouble:0];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    MESSAGE(@"applicationWillTerminate called");
}


//-----------------------------------------------------------------------

#pragma mark - Orientation Method

//-----------------------------------------------------------------------

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    return  UIInterfaceOrientationMaskAll;
}


/*
 
 Changes version wise:
 
 4.0.4-> Changes for show 10.10 (0 at lat position)
 
 1)Datamanager: Change query for create table CREATE TABLE IF NOT EXISTS \"FT%@_AssessmentSheet_Data\" take fields VARCHAR instead of float.
 2)AssessmentViewController: changes in method: setAssessmentDataForSheet for check 0 to 0.0 for blank value
 3)AssessmentViewController: changes in method: autoSaveContents for set 0.0 if value equal to 0.
 4)DATAMANAGER: - (int) updateField :(NSDictionary *) dictInfo {-- FULL
 
 
 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 31 Aug->
 //TODO: SUNIL Copy Paste exercise
//TODO: SUNIL PASTE ENTIRE PROGRAM 
 //TODO: SUNIL COPY PASTE WORKOUT
 //TODO: COPY PASTE FROM TEMPLATE
 
 PROGRAM template view:
 //TODO: SIUNIL -> COPY
 //TODO:  SUNIL = COPY WORK OUT
 void)selectedOptionsTask:(NSInteger)selT

 
 NOTE: REMOVE return
 
 programtemplateviewcontroller: d)selectedOptionsTask:(NSInteger)selTask forEvent:(NS=====> 
 if (selTask == 12) {  //TODO: Copy Sheet..!!
 return;
 [self copyBtnClicked];
 }
 else if (selTask == 41) {//TODO: copy WorkOut
 return;
 [self doCopyWorkOut];
 
 }
 else if (selTask == 42) {//TODO: past NewLines
 return;
 [self doPasteLines];
 }
 
 
----------------------- 
ClientView:programView:selectedOptionsTask->  if([evnt isEqualToString:kisProgram]) {
 //TODO: copy entire program
 return; 1481
 //TODO: Paste from template
 return;
 //TODO: delete sheet
 return;
 // delete sheet
 return;
 else if (selTask == 17)
 {
 // TODO: copy new line
 return;
 
 
 
 ProgramTemplteview:  btnAddNewLineClicked
 //TODO: PASTE Exercise
 return;
 
 
 ProgramViewController: 1882
----------------------------
 -(void)copyNewLines
 {
 
 [commonUtility alertMessage:@"Under Development!"];
 return;

 
 ProgramViewController: 273
 ----------------------------
 if (buttonIndex == 0 && t == 1001) {
 
 MESSAGE(@"Delete block-->");
 
 [commonUtility alertMessage:@"Under Development!"];
 return;

 
 */




/*
 CHNAGES: Not Fully Verified
//Commented for Update Workout SUNIL
 
 */



//TODO: APP INFO AND NOTE KEYS

/*
 
 T_NOTEBOOK——>
 
 Apple Id / Password
	
 hectorsan76@yahoo.com
 Tnotebook123!!
 
 
 
 http://35.166.235.18/pma/
 
 http://thetrainingnotebook.com/
 
 http://tnotebook.com/index.php/welcome/
 
 
 
 
 
 --> Check conditions in trainer file
 
 In loin
 Logs
 URL
 WEB_HOST
 HOST
 CHECK TESTING COMENT
 
 Check the status of Demo account : OLD -> YES
 
 */
@end
