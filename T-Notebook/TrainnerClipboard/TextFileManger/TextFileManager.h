//
//  TextFileManager.h
//  TextFileCompression
//
//  Created by Sunil Yadav on 04/08/16.
//  Copyright © 2016 chromeinfotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextFileManager : NSObject

//Create Property of path for access path
@property( nonatomic , strong)   NSString       *strTextFilePath;

//Get Shared Instance
+(TextFileManager*)getSharedInstance;

//Delete Text File
-(BOOL)deleteTextFile;

-(NSString *)writeTextToFile:(NSMutableDictionary *)dictdata;


@end
