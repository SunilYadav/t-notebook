//
//  TextFileManager.m
//  TextFileCompression
//
//  Created by Sunil Yadav on 04/08/16.
//  Copyright © 2016 chromeinfotech. All rights reserved.
//

#import "TextFileManager.h"

static TextFileManager *sharedInstance = nil;

//use macro for the File name instead of this or change here.
static NSString *fileName = @"jsonData.txt";

@implementation TextFileManager {
    
  
    NSFileManager  *_filemanger;
}
/*
 Description:get the instance of the class
 Return-type:instance of this class
 Argument:none
 */
+(TextFileManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        
        //Delete Every time
        [sharedInstance deleteTextFile];
        
        [sharedInstance copyTextFile];
    }
    return sharedInstance;
}

/*
 Description:used to copy a File from the resource directory to chace directory if file already exist the delete it and copy.
 Return-type:return true if success other wise false return
 Argument:file name
 */
-(BOOL)copyTextFile{
    
    BOOL sucess = false ;
    //work for the File
    _filemanger = [NSFileManager defaultManager];
    NSArray *dirpaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    _strTextFilePath = [dirpaths objectAtIndex:0];
    _strTextFilePath = [_strTextFilePath stringByAppendingPathComponent:fileName];
    MESSAGE(@"File path-> ===== %@",_strTextFilePath);
    

    
    
    if (![_filemanger fileExistsAtPath:_strTextFilePath])
    {
        NSString *tempPath = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:fileName];
        
        sucess = [_filemanger copyItemAtPath:tempPath toPath:_strTextFilePath error:nil];
    }
    return  sucess;
}

/*!
 @Description : Methods for delete File
 @Param       :
 @Return      :
 */
-(BOOL)deleteTextFile{
     
    NSError *error;
    BOOL success;
    
    if ([_filemanger fileExistsAtPath:_strTextFilePath]){
        success = [_filemanger removeItemAtPath:_strTextFilePath error:&error];
    }
    
    if (!success)
    {
        MESSAGE(@"Error: %@", [error localizedDescription]);
        return NO;
    }
    else{
        MESSAGE(@"Suceesfully deleted ");
        
        [sharedInstance  copyTextFile];
        return YES;
    }
     
}

//Method for Wrire in file
-(NSString *)writeTextToFile:(NSMutableDictionary *)dictdata{
   
    NSError *error;
    NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dictdata options:NSJSONWritingPrettyPrinted error:&error];
    
    if([jsondata writeToFile:_strTextFilePath atomically:YES]){
        
        return _strTextFilePath;
    }else{
        return NULL_STRING;
    }
    
}

@end