
//
//  main.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    
    @autoreleasepool {
        

        
        MESSAGE(@"Locale Identifier : %@", [[NSLocale currentLocale] localeIdentifier]);
        
        MESSAGE(@"NSUserDefaults AppleLanguages : %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppleLanguages"];
        
        NSString *testValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"];
        
        NSString *language = @"";
        if (testValue == nil) {
            language = [[NSLocale preferredLanguages] objectAtIndex:0];
        } else {
            language = @"en";
            
        }
        
        if ([language isEqualToString:@"fr"] || [language isEqualToString:@"fr-CH"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"fr", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([language isEqualToString:@"pt"] || [language isEqualToString:@"pt-PT"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"pt", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([language isEqualToString:@"zh-Hans"] || [language isEqualToString:@"zh-Hans-CH"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"zh_Hans", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([language isEqualToString:@"sv"] || [language isEqualToString:@"sv-CH"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"sv", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([language isEqualToString:@"nb"] || [language isEqualToString:@"nb-CH"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"nb", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if ([language isEqualToString:@"en"] || [language isEqualToString:@"en-CH"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else {
            [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    
}
