//
//  UpdateNotesVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol updateNoteDelegate
-(void)updatedNote:(NSString *)updateString;
@end
@interface UpdateNotesVC : BaseViewController
@property (nonatomic,strong)NSString *notes;
@property (nonatomic,unsafe_unretained) id<updateNoteDelegate>delegate;
@property (nonatomic,readwrite) BOOL isFromHelpAction;

//Sunil 
@property (nonatomic,readwrite) BOOL isInsertAction;
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
@end
