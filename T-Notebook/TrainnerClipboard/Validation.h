//
//  Validation.h
//  ValidationClasses
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validation : NSObject

+ (BOOL) isEmpty:(NSString *)value;
+ (BOOL) isValidEmail:(NSString *)email;
+ (BOOL) isValidNumber:(NSString *)number;
+ (BOOL) isValidPhoneNumber:(NSString *)phoneNumber;
+ (BOOL) isAlphaNumeric:(NSString *)value;
+ (BOOL) isValidUrl:(NSString *)url;
+ (BOOL) isValidZipCode:(NSString *)zipCode;

@end
