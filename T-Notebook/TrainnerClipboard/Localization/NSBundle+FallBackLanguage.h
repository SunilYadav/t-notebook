//
//  NSBundle+FallBackLanguage.h
//  LocalizeDemo
//
//  Created by Prashant on 03/07/15.
//  Copyright (c) 2015 webline india. All rights reserved.
//

#import <Foundation/Foundation.h>

#undef NSLocalizedString

#define NSLocalizedString(key , comment)[[NSBundle mainBundle] localizedStringForKey:(key) replaceValue:(comment)]

#define NSLocalizedString(key, comment)[[NSBundle mainBundle] localizedStringForKey:(key) replaceValue:(comment)]


@interface NSBundle (FallBackLanguage)

- (NSString *) localizedStringForKey:(NSString *)key replaceValue:(NSString *)comment;


@end
