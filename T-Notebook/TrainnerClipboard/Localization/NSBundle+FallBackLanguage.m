//
//  NSBundle+FallBackLanguage.m
//  LocalizeDemo
//
//  Created by Prashant on 03/07/15.
//  Copyright (c) 2015 webline india. All rights reserved.
//

#import "NSBundle+FallBackLanguage.h"

@implementation NSBundle (FallBackLanguage)

- (NSString *) localizedStringForKey:(NSString *)key replaceValue:(NSString *)comment {
    
    NSString *preferLang = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *localizedString;
    
    if ([@[@"zh-Hans",@"fr",@"en"] containsObject:preferLang]) {
        
        localizedString = [[NSBundle mainBundle] localizedStringForKey:key value:@"" table:nil];
    } else {
        
        NSString *fallBackLang = @"en";
        NSString *fallBackPath = [[NSBundle mainBundle] pathForResource:fallBackLang ofType:@"lproj"];
        NSBundle *fallBackBundle = [NSBundle bundleWithPath:fallBackPath];
        localizedString = [fallBackBundle localizedStringForKey:key value:comment table:nil];
        
    }
    
    return localizedString;
}

@end
