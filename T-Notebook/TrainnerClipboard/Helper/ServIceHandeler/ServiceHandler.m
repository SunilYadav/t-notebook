//
//  ServiceHandler.m
//  GoGrad
//
//  Created by Sunil Yadav on 17/05/16.
//  Copyright © 2016 chromeinfotech. All rights reserved.
//


#import "AFNetworking.h"
#import "ServiceHandler.h"

@implementation ServiceHandler

-(id)init{
    self = [super init];
    if(self) {
        MESSAGE(@"init===");
    }
    return self;
}


-(void)postRequestWitHUrl:(NSString *)urlString parameters:(id)parameters success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    // for HTTTPBody with JSON
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, id parameters, NSError * __autoreleasing * error) {
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:error];
        NSString *argString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return argString;
    }];
    
    //Post Request
    [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
      
        NSDictionary* json = (NSDictionary *)responseObject;
        
        
        MESSAGE(@"postRequestWitHUrl: json: %@", json);
        
        success(json);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        MESSAGE(@"Error: %@", error);
        failure(error);
        
    }];
}





-(void)postRequestWitHUrlWithFormData:(NSString *)urlString parameters:(id)parameters success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    /*
     // for HTTTPBody with JSON
     [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, id parameters, NSError * __autoreleasing * error) {
     
     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:error];
     NSString *argString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
     return argString;
     }];
     
     */
    //Post Request
    [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSDictionary* json = (NSDictionary *)responseObject;
        
        
        MESSAGE(@"postRequestWitHUrl: json: %@", json);
        
        success(json);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        MESSAGE(@"Error: %@", error);
        failure(error);
        
    }];
}



/*
 Description:post request on server of only data like text information
 Return-type:
 Argument:
 */

/*
-(void)postDataRequset{
    START_METHOD
    MESSAGE(@"===my deleqate===");
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    MESSAGE(@"url ====%@",self.url);
    MESSAGE(@"data ===%@",self.dataDictionary);
    [manager POST:self.url parameters:self.dataDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError * error = nil;
        NSDictionary    *responseDataDictionary = [NSJSONSerialization
                                           JSONObjectWithData:responseObject
                                           options:kNilOptions
                                           error:&error];
       
        if ([self.delegate respondsToSelector:@selector(didSuccessfullWithResponseObject:)]) {
             MESSAGE(@"===my deleqate===data=%@",responseDataDictionary);
            [self.delegate didSuccessfullWithResponseObject:responseDataDictionary];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(successFailure:)]) {
            [self.delegate successFailure:error];
        }
    }];
    END_METHOD
}
*/

/*
 Description:post request on server data text as well as image
 Return-type:
 Argument:uiimage object and image name with extension(ex.image.png)
 */

-(void)postImageWithURl:(NSString *)urlString parameters:(id)parameters andImageData: (NSData *)imageData WithImageName: (NSString *)imageName  success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
      
    
    START_METHOD
    MESSAGE(@"postImageWithURl->andImageData: %@",imageData);
        
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //send image data
        [formData appendPartWithFileData:imageData
                                    name:@"image"
                                fileName:imageName
                                mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary* json = (NSDictionary *)responseObject;

        MESSAGE(@"postImageWithURl-.responseObject: %@",responseObject);
        
        success(json);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        MESSAGE(@"Error: %@",error);
        
        failure(error);
    }];
    
    END_METHOD
}

/*
 Description:post request on server data text as well as ANY FILE
 Return-type:
 Argument:uiimage object and image name with extension(ex.image.png)
 */

-(void)postDataWithFile:(NSString *)urlString parameters:(id)parameters filePath:(NSURL *)strPath success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    
    START_METHOD
  
    MESSAGE(@"postDataWithFile-> response: strPath: %@",strPath);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
 
    [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //Append File in Request
        [formData appendPartWithFileURL:strPath name:@"fileUpload" fileName:@"jsonData.txt" mimeType:@"text/plain" error:nil];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        MESSAGE(@"success block-> progress" );

        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if(responseObject)  {
            
            NSDictionary    *responseDataDictionary = (NSDictionary *)responseObject;
            MESSAGE(@"responseDataDictionary->12 %@",responseDataDictionary);
            
            //Return Dictionry
            success(responseDataDictionary);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        MESSAGE(@"postDataWithFile-> error: %@",error);
        
        
        failure(error);

        
        
    }];
    
        
    END_METHOD
}



/*
 Description:post request on server data text as well as ANY FILE
 Return-type:
 Argument:uiimage object and image name with extension(ex.image.png)
 */

-(void)postDataWithSqliteFile:(NSString *)urlString parameters:(id)parameters filePath:(NSURL *)strPath success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    
    START_METHOD
    
    MESSAGE(@"postDataWithFile-> response: strPath: %@",strPath);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //Append File in Request
        [formData appendPartWithFileURL:strPath name:@"fileUploadSql" fileName:@"jsonData.txt" mimeType:@"text/plain" error:nil];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        MESSAGE(@"success block-> progress" );
        
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if(responseObject)  {
            
            NSDictionary    *responseDataDictionary = (NSDictionary *)responseObject;
            MESSAGE(@"responseDataDictionary->12 %@",responseDataDictionary);
            
            //Return Dictionry
            success(responseDataDictionary);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        MESSAGE(@"postDataWithFile-> error: %@",error);
        
        
        failure(error);
        
        
        
    }];
    
    
    END_METHOD
}
@end
