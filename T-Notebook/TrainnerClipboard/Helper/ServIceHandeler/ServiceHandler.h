//
//  ServiceHandler.h
//  GoGrad
//
//  Created by Sunil Yadav on 17/05/16.
//  Copyright © 2016 chromeinfotech. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ServiceHandlerDelegate <NSObject>

@optional
-(void)didSuccessfullWithResponseObject:(NSDictionary *)responseDictionary;
-(void)successFailure:(NSError *)error;

@end

//typedef void (^requestSucccessBlock) (BOOL wasSuccessful, NSDecimalNumber *price);

@interface ServiceHandler : NSObject

@property(nonatomic,strong) NSDictionary *dataDictionary;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) id<ServiceHandlerDelegate> delegate;

-(void)postDataRequset;


//- (void)requestQuoteForSymbol:(NSString *)symbol
//                 withCallback:(requestSucccessBlock)callback;
//

//Post Request for data with Dictionary
- (void)postRequestWitHUrl:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(NSDictionary *responseDataDictionary))success
              failure:(void (^)(NSError *error))failure;


//In from Data
- (void)postRequestWitHUrlWithFormData:(NSString *)URLString
                            parameters:(id)parameters
                               success:(void (^)(NSDictionary *responseDataDictionary))success
                               failure:(void (^)(NSError *error))failure;

//Post Request with dictionary and Image
-(void)postImageWithURl:(NSString *)urlString parameters:(id)parameters  andImageData: (NSData *)imageData WithImageName:(NSString *)imageName success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;




//Post Request with dictionary and Any file
-(void)postDataWithFile:(NSString *)urlString parameters:(id)parameters filePath:(NSURL *)strPath success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;


-(void)postDataWithSqliteFile:(NSString *)urlString parameters:(id)parameters filePath:(NSURL *)strPath success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;
@end
