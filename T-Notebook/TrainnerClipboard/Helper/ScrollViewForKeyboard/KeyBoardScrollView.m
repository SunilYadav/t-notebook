//
//  KeyBoardScrollView.m
//  KeyBoardTesting
//
//  Created by Ashiwani on 11/24/14.
//  Copyright (c) 2014 Chromeinfotech. All rights reserved.

//


#import "CommonUtility.h"

#import "Logs.h"

#import "KeyBoardScrollView.h"
static  UIEdgeInsets inset;
@implementation KeyBoardScrollView{
    
   
}

#pragma mark - Setup/Teardown

- (void)setup {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
   self.contentSize = [self calculatedContentSizeFromSubviewFrames];
   }

-(id)initWithFrame:(CGRect)frame {
    if ( !(self = [super initWithFrame:frame]) ) return nil;
    [self setup];
    return self;
}

-(void)awakeFromNib {
    [self setup];
}

- (void)contentSizeToFit {
    MESSAGE(@"content size to fit");
    self.contentSize = [self calculatedContentSizeFromSubviewFrames];
}
/*
 Description:
 Return-type:
 Argument:
 */
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self KeyboardAvoidingFindFirstResponder:self] resignFirstResponder];
    [super touchesEnded:touches withEvent:event];
}

- (void)keyboardWillShow:(NSNotification*)notification {
    
    START_METHOD
    inset                       = self.contentInset;
    UIView      *firstResponder = [self KeyboardAvoidingFindFirstResponder:self];
    
    CGRect      keyboardRect    = [self convertRect:[[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:nil];
   
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:[[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
    [UIView setAnimationDuration:[[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    
    UIEdgeInsets newInset       = self.contentInset;
    newInset.bottom             = keyboardRect.size.height - MAX((CGRectGetMaxY(keyboardRect) - CGRectGetMaxY(self.bounds)), 0) + 5;
    self.contentInset           = newInset;
    
    if ( [firstResponder isKindOfClass:[UITextView class]]) {
    CGRect  fieldRect           = [firstResponder convertRect:CGRectMake(0, 0, firstResponder.bounds.size.width,firstResponder.bounds.size.height) toView:self];
        [self scrollRectToVisible:fieldRect animated:NO];
    }
    [UIView commitAnimations];
    
//    
//    UIEdgeInsets contentInset  = self.contentInset;
//    NSDictionary *keyboardInfo = [notification userInfo];
//    
//    UIView *showingView = [[UIView alloc] init];
//    
//    CGRect kbRect =[[keyboardInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    CGRect fieldRect;
//    CGRect visibleRect = self.frame;
//    UIView *firstResponder = [self KeyboardAvoidingFindFirstResponder:self];
//    if(firstResponder)   {
//    showingView.frame = firstResponder.frame;
//        fieldRect = [firstResponder convertRect:firstResponder.bounds toView:self];
//    }
////    else if (_keyboardOwnedTV){
////        
////        showingView.frame = _keyboardOwnedTV.frame;
////        fieldRect = [_keyboardOwnedTV convertRect:CGRectMake(0, 0, _keyboardOwnedTV.bounds.size.width, 50) toView:_resizableView];
////    }
////
////    showingView.frame = CGRectMake(showingView.frame.origin.x, showingView.frame.origin.y, showingView.frame.size.width, 50);
//    
//    showingView.frame = CGRectMake(showingView.frame.origin.x, showingView.frame.origin.y, showingView.frame.size.width, 50);
//    visibleRect.size.height -= kbRect.size.height;
//    contentInset.bottom = kbRect.size.height + 5;
//    self.contentInset = contentInset;
//    
//    // If active text field is hidden by keyboard, scroll it so it's visible
//    // Your app might not need or want this behavior.
//    CGPoint offsetPoint = self.contentOffset;
//    if (!CGRectContainsPoint(visibleRect, showingView.frame.origin) )
//    {
//        [self scrollRectToVisible:fieldRect animated:NO];
//    }
//    
////    if (offsetPoint.y > _resizableView.contentOffset.y) {
////        
////        if(LOGS_ON) MESSAGE(@"should be sliding down");
////        _resizableView.contentOffset = CGPointMake(_resizableView.contentOffset.x, _resizableView.contentOffset.y - _extraMarginFromTop);
////    }
//
 }

- (void)keyboardWillHide:(NSNotification*)notification {
    START_METHOD
    // Restore dimensions to prior size
    [UIView     beginAnimations:nil context:NULL];
    [UIView     setAnimationCurve:[[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]];
    [UIView     setAnimationDuration:[[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]];
    
    UIEdgeInsets contentInsets  = UIEdgeInsetsZero;
    self.contentInset           = contentInsets;
    self.scrollIndicatorInsets  = contentInsets;
    [UIView commitAnimations];
}

- (UIView*)KeyboardAvoidingFindFirstResponder:(UIView*)view {
    START_METHOD
    // Search recursively for first responder
    for ( UIView *childView in view.subviews ) {
        if ( [childView respondsToSelector:@selector(isFirstResponder)] && [childView isFirstResponder] ) return childView;
        UIView *result          = [self KeyboardAvoidingFindFirstResponder:childView];
        if ( result ) return result;
    }
    return nil;
}

-(CGSize)calculatedContentSizeFromSubviewFrames {
    
    BOOL wasShowingVerticalScrollIndicator      = self.showsVerticalScrollIndicator;
    BOOL wasShowingHorizontalScrollIndicator    = self.showsHorizontalScrollIndicator;
    
    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator = NO;
    
    CGRect rect = CGRectZero;
    for ( UIView *view in self.subviews ) {
        rect = CGRectUnion(rect, view.frame);
    }
    //rect.size.height += kCalculatedContentPadding;
    rect.size.height += 10;
    rect.size.width -= 50;
    
    self.showsVerticalScrollIndicator = wasShowingVerticalScrollIndicator;
    self.showsHorizontalScrollIndicator = wasShowingHorizontalScrollIndicator;
   MESSAGE(@"rect size%f",rect.size.height);
      return rect.size;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [super dealloc];
#endif
}
@end
