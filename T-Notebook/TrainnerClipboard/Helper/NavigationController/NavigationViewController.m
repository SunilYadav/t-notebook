//
//  NavigationViewController.m
//  SideMenuBar
//
//  Created by Ashiwani on 12/11/14.
//  Copyright (c) 2014 Chromeinfotech. All rights reserved.
//
#import "NavigationViewController.h"

@interface NavigationViewController ()

@end

@implementation NavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    START_METHOD
    AppDelegate *objAppDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    objAppDelegate.nav       =   self;
    
    END_METHOD
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
