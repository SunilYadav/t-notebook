//
//  ParQController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Display client detail

//SUNIl
#import "TextFileManager.h"

#import "ParQController.h"
#import "ParQquestionView.h"
#import "FileUtility.h"
#import "UIView+firstResponder.h"
#import "TearmsAndConditionVC.h"
#import "GraphFullScreenVC.h"
#import "AddNameVC.h"
#import "KeyboardControls.h"
#import "Validation.h"

#define kTextFieldTag       100

@interface ParQController (){
    
    UIImage     *imgSignature;
    
}

//-------------------- first Row Info ---------------

@property (nonatomic , weak) IBOutlet  UITextField                 *txtDate;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtFname;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtLname;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtDateOfBirth;
@property (nonatomic , weak) IBOutlet  UIButton                     *btnDateOfBirth;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtAge;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtDayPhone;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtEvePhone;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtCell;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtEmail1;
@property (nonatomic , weak) IBOutlet  UITextField                 *txtEmail2;
@property (nonatomic , weak) IBOutlet  UIButton                     *btnMale;
@property (nonatomic , weak) IBOutlet  UIButton                     *btnFemale;

@property (nonatomic , weak) IBOutlet  UILabel                     *lblDate;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblName;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblGender;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblMale;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblFemale;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblDob;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblAge;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblPhone;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblCell;
@property (nonatomic , weak) IBOutlet  UILabel                     *lblEmail;


//------------ Emergency 1 ---------------------------

@property (nonatomic , weak) IBOutlet  UILabel                      *lblE1Emergency;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE1Name;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE1Relation;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE1PhoneCell;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE1PhoneHome;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE1Home;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE1Work;

@property (nonatomic , weak) IBOutlet  UILabel                      *lblE1Name;
@property (nonatomic , weak) IBOutlet  UILabel                      *lblE1Relation;
@property (nonatomic , weak) IBOutlet  UILabel                      *lblE1Phone;
@property (nonatomic , weak) IBOutlet  UILabel                      *lblE1Work;

//------------ Emergency 2 ---------------------------

@property (nonatomic , weak) IBOutlet  UILabel                      *lblE2Emergency;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE2Name;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE2Relation;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE2PhoneCell;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE2PhoneHome;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE2Home;
@property (nonatomic , weak) IBOutlet  UITextField                  *txtE2Work;

@property (nonatomic , weak) IBOutlet  UILabel                      *lblE2Name;
@property (nonatomic , weak) IBOutlet  UILabel                      *lblE2Relation;
@property (nonatomic , weak) IBOutlet  UILabel                      *lblE2Phone;
@property (nonatomic , weak) IBOutlet  UILabel                      *lblE2Work;


@property (nonatomic , strong) UIToolbar                              *keyBoardToolBar;

//@property (nonatomic) BOOL                                                     isTermsAccepted;
@property (nonatomic , strong) UIImagePickerController         *picker;
@property (nonatomic, strong) KeyboardControls      *keyboardControls;


@end

@implementation ParQController

@synthesize strClientId;

static int oldValue = 0;
static const CGFloat kMinimumScrollOffsetPadding = 20;

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Memory Management Methods

//---------------------------------------------------------------------------------------------------------------

- (void)dealloc {
    
}


//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : Action Methods 

//---------------------------------------------------------------------------------------------------------------

-(IBAction)clientImageBtnTapped:(id)sender {
    
    // DLOG(@"client image btn tapped");
    
}

//---------------------------------------------------------------------------------------------------------------
-(IBAction)getDateofBithBtnTapped:(id)sender {
    [self showDatePicker];
}

//---------------------------------------------------------------------------------------------------------------

- (IBAction) genderValueChanged :(id)sender {
    
    // 3001 === Male
    // 3002 === Female
    
    UIButton * btn = (UIButton *) sender;
    NSString * gender = @"";
    
    if([btn tag] == 3001) {
       
        
        //Set Aimge
        [self setMaleButtonImage];
        
        gender = @"M";
        [[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if([btn tag] == 3002) {
        
        
        //Sunil
        //Set Aimge
        [self setFemaleButtonImage];
        
        
        gender = @"F";
        [[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
  
    
    if ([gender length]==0 || [self.strClientId isEqualToString:@"0"] || ![AppDelegate sharedInstance].isAutoSave) {
        [AppDelegate sharedInstance].isFieldUpdated = YES;
        return;
    }
    
    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                               kgender,kfieldName,
                               gender,kfieldValue,
                               self.strClientId,kClientId,
                               @"0",kDictType,
                               nil];
    DebugLOG([dictInfo description]);
    
    if ([[DataManager initDB] updateParqField:dictInfo]) {
        DebugLOG(@"Not Updated");
    }
    else {
        DebugLOG(@"Updated");
        [[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
        [[NSUserDefaults standardUserDefaults] synchronize];		
    }
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)saveParQData:(id)sender {
    START_METHOD
    MESSAGE(@"Action Save par-Q");
    
    [self saveParqViewData];
}


//---------------------------------------------------------------------------------------------------------------

-(IBAction)clientPhotoBtnTapped:(id)sender {
    
    [self.parqDelegate presentCamera];
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark : Custom Methods

//--------------------------------------------------------------------------------------------------------------

-(void) setupKeyBoardAccessoryView {
    
    UIButton *btnPrev = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPrev setFrame:CGRectMake(0, 0,100, 30)];
    [btnPrev setTitle:@"Previous" forState:UIControlStateNormal];
    [btnPrev setBackgroundColor:[UIColor clearColor]];
    [btnPrev setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnPrev addTarget:self action:@selector(previousBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    btnPrev.layer.borderColor = [UIColor whiteColor].CGColor;
    btnPrev.layer.cornerRadius = 5.0f;
    btnPrev.layer.borderWidth = 1.0f;
    
    UIBarButtonItem   *btnPrevious = [[UIBarButtonItem alloc]initWithCustomView:btnPrev];
    UIButton *btnNxt = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNxt setFrame:CGRectMake(0, 0,80, 30)];
    [btnNxt setTitle:@"Next" forState:UIControlStateNormal];
    [btnNxt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnNxt setBackgroundColor:[UIColor clearColor]];
    [btnNxt addTarget:self action:@selector(nextBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    btnNxt.layer.borderColor = [UIColor whiteColor].CGColor;
    btnNxt.layer.cornerRadius = 5.0f;
    btnNxt.layer.borderWidth = 1.0f;
    
    UIBarButtonItem   *btnNext = [[UIBarButtonItem alloc]initWithCustomView:btnNxt];
    
    UIButton *btnDne = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDne setFrame:CGRectMake(0, 0,80, 30)];
    [btnDne setTitle:@"Done" forState:UIControlStateNormal];
    [btnDne setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDne setBackgroundColor:[UIColor clearColor]];
    [btnDne addTarget:self action:@selector(doneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    btnDne.layer.borderColor = [UIColor whiteColor].CGColor;
    btnDne.layer.cornerRadius = 5.0f;
    btnDne.layer.borderWidth = 1.0f;
    
    UIBarButtonItem   *btnDone = [[UIBarButtonItem alloc]initWithCustomView:btnDne];

    UIBarButtonItem *btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray *toolItems = [NSArray arrayWithObjects:btnPrevious,btnNext,btnSpace,btnDone,nil];
    
    _keyBoardToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [self.keyBoardToolBar setBarStyle:UIBarStyleBlackTranslucent];
    [self.keyBoardToolBar setBackgroundColor:[UIColor blackColor]];
    [self.keyBoardToolBar setTintColor:[UIColor blackColor]];
    [self.keyBoardToolBar setItems:toolItems];
    

}

//---------------------------------------------------------------------------------------------------------------

- (void) assignTagsToTexFields {
    
    int i = 1;
    NSArray *vewContents = [self.scrollVew subviews];
    for (id obj in vewContents) {
        if([obj isKindOfClass:[UITextField class]]) {
            UITextField *txt = (UITextField *) obj;
            [txt setTag:kTextFieldTag + i];
            i++;
        }
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void)setupQuestionnareView {
    
    self.btnProfilePic.layer.cornerRadius = 40.0f;
    self.btnProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnProfilePic.layer.borderWidth = 1.0f;
    self.btnProfilePic.clipsToBounds = TRUE;
        
    
    if ([self.view viewWithTag:2000] != nil) {
        [[self.view viewWithTag:2000] removeFromSuperview];
    }
    
    ParQquestionView *parqView;
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
     parqView = [[ParQquestionView alloc]initWithFrame:CGRectMake(self.scrollVew.frame.origin.x,780,kLandscapeWidth,2500)];
     parqView.shouldSetLandscape = TRUE;
    } else {
     parqView = [[ParQquestionView alloc]initWithFrame:CGRectMake(self.scrollVew.frame.origin.x,780,kPotraitWidth,2500)];
     parqView.shouldSetLandscape = FALSE;
    
    }
    
    [self.scrollVew addSubview:parqView];
    [parqView setParqQuestDelegate:self];
    parqView.tag = 2000;
    parqView.strClientId = self.strClientId;
    [parqView setupLayoutForViews];
    [self.scrollVew setContentSize:CGSizeMake(self.scrollVew.frame.size.width,3000)];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) showDatePicker {

    if (self.datePicker == nil) {
       
        _datePicker =   [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DatePicker"];
       
    }
    self.datePicker.isFromParQ = YES;
    self.datePicker.dateString = [self.btnDateOfBirth titleForState:UIControlStateNormal];
    self.datePicker.dateflag = 1;
    self.datePicker.datePickDelegate = self;
    
    if (self.pop== nil) {
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.datePicker];
        _pop = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        [self.pop setPopoverContentSize:CGSizeMake(320,200) animated:NO];
        self.pop.delegate = (id)self;
    }
    
    [self.pop presentPopoverFromRect:[self.btnDateOfBirth bounds] inView:self.btnDateOfBirth permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}


//---------------------------------------------------------------------------------------------------------------

- (void) displayClientDetail  {
    
    
    if(![self.strClientId isEqualToString:@"0"]) {
        
        NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientId];
        
        NSDateFormatter * dt = [[NSDateFormatter alloc] init];
        [dt setDateFormat:@"MM/dd/yy hh:mm:ss a"];
        NSDate * date = [dt dateFromString:[clientInfo objectForKey:kUpdatedDate]];
        [dt setDateFormat:@"MM/dd/yy hh:mm a"];
        NSString * strDt = [dt stringFromDate:date];
        
        self.txtDate.text = strDt;
        self.txtFname.text = [clientInfo objectForKey:kFirstName];
        self.txtLname.text = [clientInfo objectForKey:kLastName];
        self.txtAge.text = [clientInfo objectForKey:kAge];
        self.txtDayPhone.text = allTrim([clientInfo objectForKey:kPday]);
        self.txtEvePhone.text = allTrim([clientInfo objectForKey:kPeve]);
        self.txtCell.text = allTrim([clientInfo objectForKey:kPcell]);
        self.txtEmail1.text = allTrim([clientInfo objectForKey:kEmail1]);
        self.txtEmail2.text = allTrim([clientInfo objectForKey:kEmail2]);
        
        self.txtE1Name.text = [clientInfo objectForKey:kEmergencyConName1];
        self.txtE1Relation.text = [clientInfo objectForKey:kEmergencyRel1];
        self.txtE1Work.text = allTrim([clientInfo objectForKey:kEmergencyConWNo1]);
        self.txtE1Home.text = allTrim([clientInfo objectForKey:kEmergencyConHNo1]);
        self.txtE1PhoneCell.text = allTrim([clientInfo objectForKey:kEmergencyConCNo1]);
        
        self.txtE2Name.text = [clientInfo objectForKey:kEmergencyConName2];
        self.txtE2Relation.text = [clientInfo objectForKey:kEmergencyRel2];
        self.txtE2Work.text = allTrim([clientInfo objectForKey:kEmergencyConWNo2]);
        self.txtE2Home.text = allTrim([clientInfo objectForKey:kEmergencyConHNo2]);
        self.txtE2PhoneCell.text = allTrim([clientInfo objectForKey:kEmergencyConCNo2]);
        
         [self.btnMale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
         [self.btnFemale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
        
        [self.parqDelegate setEmailIdForClient:[clientInfo objectForKey:kEmail1]];
        
        if ([[clientInfo objectForKey:kgender] isEqualToString:@"F"]) {
            
            //Invoke for Set Image sunil
            [self setFemaleButtonImage];
        
        }
        else {
            //Invoke for Set Image sunil
            [self setMaleButtonImage];
        }
        
        
        [self.btnDateOfBirth setTitle:[clientInfo objectForKey:kBirthDate] forState:UIControlStateNormal];
        [self.txtDateOfBirth setText:[clientInfo objectForKey:kBirthDate]];
        
        NSString * imgPath = [NSString stringWithFormat:@"%@/ClientsProfileImage/ProfileImage%@.png",[FileUtility basePath],self.strClientId];
        UIImage * img = [UIImage imageWithContentsOfFile:imgPath];
        if(img == nil) {
           img = [UIImage imageNamed:@"ClientPlaceHolderBig"];
        }
        [self.btnProfilePic setBackgroundImage:img forState:UIControlStateNormal];
        
    } else {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"M" forKey:kgender];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        //Invoke for Set Image sunil
        [self setFemaleButtonImage];
    }
    
    //Invoke mehod for set bg
    [self setBgColor];
    
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) addleftViewToTextFields {

    NSArray *vewContents = [self.scrollVew subviews];
    NSMutableArray *arryTextFields = [[NSMutableArray alloc]init];
    for (id obj in vewContents) {
        if([obj isKindOfClass:[UITextField class]]) {
            UIView *vew = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 31)];
            [vew setBackgroundColor:[UIColor clearColor]];
            UITextField *txt = (UITextField *) obj;
            [txt setLeftView:vew];
            [txt setLeftViewMode:UITextFieldViewModeAlways];
            [txt setDelegate:self];
            if ([txt isEqual:self.txtDate ]||[txt isEqual:self.txtAge ]) {
            }
            else{
                [arryTextFields addObject:txt];
            }
            vew = nil;
        }
    }
    
    NSArray *fields = [NSArray arrayWithArray:arryTextFields];
    [self setKeyboardControls:[[KeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:(id)self];
    arryTextFields = nil;
    fields = nil;
    
}

//---------------------------------------------------------------------------------------------------------------

- (BOOL)saveParqViewData {

    START_METHOD
    NSString * bdate = [self.btnDateOfBirth.titleLabel text];
    
    if ([self.txtFname.text length] == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter first name.", nil),kAppName);
        return NO;
    }
    if ([self.txtLname.text length] == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter last name.",nil),kAppName);
        return NO;
    }
    
    if ([bdate isEqualToString:@"Select Date"] || [allTrim(bdate)  length] == 0 ) {
        if ([self.txtAge.text length] == 0) {
            DisplayAlertWithTitle(NSLocalizedString(@"Please enter birth date or age.", nil),kAppName);
            return NO;
        }
    }
    
    if ([self.txtEmail1.text length] == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter primary email address.", nil),kAppName);
        return NO;
    }
    if (![self validateEmail:self.txtEmail1.text]) {
        DisplayAlertWithTitle(NSLocalizedString(@"Invalid primary email address.", nil),kAppName);
        return NO;
    }
    
    if ([allTrim(self.txtEmail2.text) length] != 0) {
        if (![self validateEmail:self.txtEmail2.text]) {
            DisplayAlertWithTitle(NSLocalizedString(@"Invalid secondary email address.", nil),kAppName);
            return NO;
        }
    }
    
    
    NSMutableDictionary * dictInfo = [[NSMutableDictionary alloc] init];
    [dictInfo setObject:[self valueForField:self.txtFname] forKey:kFirstName];
    [dictInfo setObject:[self valueForField:self.txtLname] forKey:kLastName];
    [dictInfo setObject:[self valueForField:self.txtEmail1] forKey:kEmail1];
    [dictInfo setObject:[self valueForField:self.txtEmail2] forKey:kEmail2];
    [dictInfo setObject:[self valueForField:self.txtDayPhone] forKey:kPday];
    [dictInfo setObject:[self valueForField:self.txtEvePhone] forKey:kPeve];
    [dictInfo setObject:[self valueForField:self.txtCell] forKey:kPcell];
    [dictInfo setObject:bdate forKey:kBirthDate];
    [dictInfo setObject:[self valueForField:self.txtAge] forKey:kAge];
    [dictInfo setObject:[self valueForField:self.txtE1Name] forKey:kEmergencyConName1];
    [dictInfo setObject:[self valueForField:self.txtE1Work] forKey:kEmergencyConWNo1];
    [dictInfo setObject:[self valueForField:self.txtE1Home] forKey:kEmergencyConHNo1];
    [dictInfo setObject:[self valueForField:self.txtE1PhoneCell]forKey:kEmergencyConCNo1];
    [dictInfo setObject:[self valueForField:self.txtE1Relation] forKey:kEmergencyRel1];
    [dictInfo setObject:[self valueForField:self.txtE2Name] forKey:kEmergencyConName2];
    [dictInfo setObject:[self valueForField:self.txtE2Work] forKey:kEmergencyConWNo2];
    [dictInfo setObject:[self valueForField:self.txtE2Home] forKey:kEmergencyConHNo2];
    [dictInfo setObject:[self valueForField:self.txtE2PhoneCell] forKey:kEmergencyConCNo2];
    [dictInfo setObject:[self valueForField:self.txtE2Relation]forKey:kEmergencyRel2];
    
    NSString *gender = [[NSUserDefaults standardUserDefaults]valueForKey:kgender];
    [dictInfo setObject:gender forKey:kgender];
     [dictInfo setObject:@"YES" forKey:@"client_status"];
    
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //Get Upgarde Status
    strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    DebugLOG([dictInfo description]);
    NSString * cName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLname.text];
    
    if ([self.strClientId isEqualToString:@"0"]) {
        
        MESSAGE(@"Add new Client---->");
            //Check key for data payload
            if ([strUpgradeStatus isEqualToString:@"YES"]) {
                
                //Set Action
                [dictInfo setValue:ADD_CLIENT forKey:ACTION];
                
                //invoke methos for post file on server and save in local
                [self postRequestToServerForClient:dictInfo];
    
            }else{
                
                self.strClientId = [[DataManager initDB] insertNewClient:dictInfo];
                
                if (![self.strClientId isEqualToString:@" "]) {
                    [self.parqDelegate displayClientID:self.strClientId:cName];
                 
                    
                }
                else {
                    DisplayAlertWithTitle(NSLocalizedString(@"Data not saved successfully.", nil),kAppName);
                    return NO;
                }
            }
        
        
    }
    else {
        //Set Client Info
        [dictInfo setObject:self.strClientId forKey:kClientId];
        
        
        //Check key for data payload
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
 //TODO: UPDATE CLINET INFO
            
                    MESSAGE(@"UPDATE Client---->self.strClientId: %@",self.strClientId);
            //Set Action
            [dictInfo setValue:UPDATE_CLIENT forKey:ACTION];
            
            //invoke methos for post file on server and save in local
            [self postRequestForUpdateClientInfo:dictInfo];
            
            
        }else{
            
            self.strClientId = [[DataManager initDB] updateClientDetail:dictInfo];
            [self.parqDelegate displayClientID:self.strClientId:cName];
        }
   

    }
    
    if(![strClientId isEqualToString:@"0"]) {
        
//        [self saveProfileImageWithClientId:self.strClientId];
        
        ParQquestionView * parQOBJ = (ParQquestionView *) [self.view viewWithTag:2000];
        parQOBJ.strClientId = self.strClientId;
        [parQOBJ btnSubmit_click];
        [AppDelegate sharedInstance].isFieldUpdated = NO;
    }
   
    //Check key for data payload
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
        
    }else{
        
        //Invoke method for post on server
        [commonUtility alertMessage:(NSLocalizedString(@"Data Saved", nil))];

    }
   
    return YES;
   
}

//---------------------------------------------------------------------------------------------------------------

-(NSString*)valueForField:(UITextField*)txtField {

    NSString *strValue = txtField.text;
    if(![strValue length] > 0) {
        strValue = @"";
    }
    return strValue;
}

//---------------------------------------------------------------------------------------------------------------

- (BOOL) validateEmail: (NSString *) Email {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:Email];
}


//---------------------------------------------------------------------------------------------------------------

- (void)saveProfileImageWithClientId:(NSString *) clientId {
    
    START_METHOD
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke method for save clinet image on server
        [self postRequestForSaveProfileImageOnServer:clientId];
        
    }else{
        
        //Invoke method for save in laocl
        [self setProfileImageOfClient:clientId];
        
    }
    
    
    //Invoke method for save in laocl
//    [self setProfileImageOfClient:clientId];
    
    END_METHOD
}

//---------------------------------------------------------------------------------------------------------------

-(void)changeSignature:(NSNotification*)signNotify {
    
    START_METHOD
    
    NSString * imgPath = [NSString stringWithFormat:@"%@/ClientsSignature/Signature%@.png",[FileUtility basePath],self.strClientId];
    UIImage * img = [UIImage imageWithContentsOfFile:imgPath];
    ParQquestionView * parQOBJ = (ParQquestionView *) [self.view viewWithTag:2000];
    [parQOBJ setSignature:img];
    
    END_METHOD
}


//---------------------------------------------------------------------------------------------------------------
 
-(void)presentTermsScreen:(NSNotification*)termsNotify {
    dispatch_async(dispatch_get_main_queue(), ^{
          [self openTheTermsAndConditions];
    });
  
}

//-----------------------------------------------------------------------

- (void)autoScrollTextField:(UIView *)textField onScrollView:(UIScrollView *)scrollView {
    
    CGFloat visibleSpace = scrollView.bounds.size.height - scrollView.contentInset.top - scrollView.contentInset.bottom;
    CGPoint idealOffset;
    if(self.setAsLandscape) {
        idealOffset = CGPointMake(0, [self getContentOffsetYpositionForView:textField
                                                      withViewingAreaHeight:visibleSpace-260 forscrollView:scrollView]);
    } else {
        idealOffset = CGPointMake(0, [self getContentOffsetYpositionForView:textField
                                                      withViewingAreaHeight:visibleSpace forscrollView:scrollView]);
    }
   
    
    [UIView animateWithDuration:0.25 animations:^{
        [scrollView setContentOffset:idealOffset animated:NO];
    }];
    
}

//-----------------------------------------------------------------------

-(CGFloat)getContentOffsetYpositionForView:(UIView *)view withViewingAreaHeight:(CGFloat)viewAreaHeight forscrollView:(UIScrollView*)scrollView {
    
    CGSize contentSize = scrollView.contentSize;
    CGFloat offset = 0.0;
    
    CGRect subviewRect = [view convertRect:view.bounds toView:scrollView];
    
    CGFloat padding = (viewAreaHeight - subviewRect.size.height) / 2;
    if ( padding < kMinimumScrollOffsetPadding ) {
        padding = kMinimumScrollOffsetPadding;
    }
    
    // Ideal offset places the subview rectangle origin "padding" points from the top of the scrollview.
    // If there is a top contentInset, also compensate for this so that subviewRect will not be placed under
    // things like navigation bars.
    offset = subviewRect.origin.y - padding - scrollView.contentInset.top;
    
    // Constrain the new contentOffset so we can't scroll past the bottom. Note that we don't take the bottom
    // inset into account, as this is manipulated to make space for the keyboard.
    if ( offset > (contentSize.height - viewAreaHeight) ) {
        offset = contentSize.height - viewAreaHeight;
    }
    
    // Constrain the new contentOffset so we can't scroll past the top, taking contentInsets into account
    if ( offset < -scrollView.contentInset.top ) {
        offset = -scrollView.contentInset.top;
    }
    
    return offset;
}

//---------------------------------------------------------------------------------------------------------------

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    [self.parqDelegate hideOptionsViewIfDisplayed];
}


-(void)checkIfAnyOftheDataIsPresent {
    
    START_METHOD
    if(allTrim(self.txtFname.text).length == 0 && allTrim(self.txtLname.text).length == 0 && allTrim(self.txtDateOfBirth.text).length == 0 && allTrim(self.txtEmail1.text).length == 0) {
        
        [self.parqDelegate noValuesEnteredIsSafeToExit:TRUE];
    }
}

//-----------------------------------------------------------------------
- (void) localizeControls {
    
    self.lblDate.text = NSLocalizedString(self.lblDate.text, nil);
    self.txtFname.placeholder = NSLocalizedString(self.txtFname.placeholder, nil);
    self.txtLname.placeholder = NSLocalizedString(self.txtLname.placeholder, nil);
    self.lblName.text = NSLocalizedString(self.lblName.text, nil);
    self.lblGender.text = NSLocalizedString(self.lblGender.text, nil);
    self.lblMale.text = NSLocalizedString(self.lblMale.text, nil);
    self.lblFemale.text = NSLocalizedString(self.lblFemale.text, nil);
    self.lblDob.text = NSLocalizedString(self.lblDob.text, nil);
    self.txtDateOfBirth.placeholder = NSLocalizedString(self.txtDateOfBirth.placeholder, nil);

    self.lblPhone.text = NSLocalizedString(self.lblPhone.text, nil);
    self.txtDayPhone.placeholder = NSLocalizedString(self.txtDayPhone.placeholder, nil);
    
    self.txtEvePhone.placeholder = NSLocalizedString(self.txtEvePhone.placeholder, nil);
    self.lblCell.text = NSLocalizedString(self.lblCell.text, nil);
    
    self.lblEmail.text = NSLocalizedString(self.lblEmail.text, nil);
    self.txtEmail1.placeholder = NSLocalizedString(self.txtEmail1.placeholder, nil);
    
    self.txtEmail2.placeholder = NSLocalizedString(self.txtEmail2.placeholder, nil);
    self.lblAge.text = NSLocalizedString(self.lblAge.text, nil);
    
    
     self.lblE1Emergency.text = NSLocalizedString(self.lblE1Emergency.text, nil);
    
     self.lblE1Name.text = NSLocalizedString(self.lblE1Name.text, nil);
     self.lblE1Relation.text = NSLocalizedString(self.lblE1Relation.text, nil);
     self.lblE1Phone.text = NSLocalizedString(self.lblE1Phone.text, nil);
     self.lblE1Work.text = NSLocalizedString(self.lblE1Work.text, nil);
    self.txtE1PhoneCell.placeholder = NSLocalizedString(self.txtE1PhoneCell.placeholder, nil);
    
    self.txtE1PhoneHome.placeholder = NSLocalizedString(self.txtE1PhoneHome.placeholder, nil);
    
    
    self.lblE2Emergency.text = NSLocalizedString(self.lblE2Emergency.text, nil);
    self.lblE2Name.text = NSLocalizedString(self.lblE2Name.text, nil);
    self.lblE2Relation.text = NSLocalizedString(self.lblE2Relation.text, nil);
    self.lblE2Phone.text = NSLocalizedString(self.lblE2Phone.text, nil);
    self.lblE2Work.text = NSLocalizedString(self.lblE2Work.text, nil);
    self.txtE2PhoneCell.placeholder = NSLocalizedString(self.txtE2PhoneCell.placeholder, nil);
    
    self.txtE2PhoneHome.placeholder = NSLocalizedString(self.txtE2PhoneHome.placeholder, nil);
    
    [self.btnSaveParq setTitle:NSLocalizedString(self.btnSaveParq.titleLabel.text, nil) forState:UIControlStateNormal];
   
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark : TextField Delegate Methods

//---------------------------------------------------------------------------------------------------------------

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (self.pop) {
        [self.pop dismissPopoverAnimated:YES];
        self.pop = nil;
    }
    if(textField == self.txtDate || textField == self.txtAge) {
        return FALSE;
    }
    else if (textField== self.txtDateOfBirth) {
        [self getDateofBithBtnTapped:nil];
        [self.keyboardControls setActiveField:textField];
        return false;
    }
    [self.keyboardControls setActiveField:textField];
    
    return TRUE;
}

//---------------------------------------------------------------------------------------------------------------

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self.parqDelegate hideOptionsViewIfDisplayed];
    
    [self autoScrollTextField:textField onScrollView:self.scrollVew];
    
    
    if(textField == self.txtDate || textField == self.txtAge ) {
        return;
    }
    else if (textField== self.txtDateOfBirth) {
        [self getDateofBithBtnTapped:nil];
        return;
    }
    /*
     sunil
    if([AppDelegate sharedInstance].isAutoSave) {
           oldValue = 1;
    }
  */
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    if (textField != self.txtEmail1 && textField != self.txtEmail2) {
        textField.text = [textField.text capitalizedString];
    }
    
    /*
    //Commented by SUNIL
//    if ([self.strClientId isEqualToString:@"0"] || ![AppDelegate sharedInstance].isAutoSave) {
//        [AppDelegate sharedInstance].isFieldUpdated = YES;
//    }
//    
    
    
    if (oldValue == 1) {
        oldValue = 0;
        NSString * fieldName = @"";
        NSInteger fieldTag = textField.tag;
        switch (fieldTag) {
            case kTextFieldTag + 2:
                fieldName = kFirstName;
                break;
            case kTextFieldTag + 3:
                fieldName = kLastName;
                break;
            case kTextFieldTag + 6:
                fieldName = kPday;
                break;
            case kTextFieldTag + 7:
                fieldName = kPeve;
                break;
            case kTextFieldTag + 8:
                fieldName = kPcell;
                break;
            case kTextFieldTag + 9:
                fieldName = kEmail1;
                break;
            case kTextFieldTag + 10:
                fieldName = kEmail2;
                break;
            case kTextFieldTag + 11:
                fieldName = kEmergencyConName1;
                break;
            case kTextFieldTag + 12:
                fieldName = kEmergencyRel1;
                break;
            case kTextFieldTag + 14:
                fieldName = kEmergencyConHNo1;
                break;
            case kTextFieldTag + 13:
                fieldName = kEmergencyConCNo1;
                break;
            case kTextFieldTag + 15:
                fieldName = kEmergencyConWNo1;
                break;
            case kTextFieldTag + 16:
                fieldName = kEmergencyConName2;
                break;
            case kTextFieldTag + 17:
                fieldName = kEmergencyRel2;
                break;
            case kTextFieldTag + 19:
                fieldName = kEmergencyConHNo2;
                break;
            case kTextFieldTag + 18:
                fieldName = kEmergencyConCNo2;
                break;
            case kTextFieldTag + 20:
                fieldName = kEmergencyConWNo2;
                break;
        }
        NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   fieldName,kfieldName,
                                   textField.text,kfieldValue,
                                   self.strClientId,kClientId,
                                   @"0",kDictType,
                                   nil];
        DebugLOG([dictInfo description]);
        if ([[DataManager initDB] updateParqField:dictInfo]) {
            DebugLOG(@"Not Updated");
        }
        else {
            DebugLOG(@"Updated");
            if (fieldTag == 1 || fieldTag ==  2) {
                NSString * cName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLname.text];
                [self.parqDelegate displayClientID:self.strClientId :cName];
            }
        }
    }
    
    */
}

//---------------------------------------------------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    oldValue = 1;
    if([self.strClientId isEqualToString:@"0"]) {
        
        if([allTrim(self.txtFname.text) length]>0 && [allTrim(self.txtLname.text) length] > 0 && [allTrim(self.txtDateOfBirth.text) length] >0 && [allTrim(self.txtEmail1.text) length] >0) {
            [self.parqDelegate setDataSavePendingWithValues:TRUE];
        }else if([allTrim(self.txtFname.text) length]>0 || [allTrim(self.txtLname.text) length] > 0 || [allTrim(self.txtDateOfBirth.text) length] >0 || [allTrim(self.txtEmail1.text) length] >0) {
            
            [self.parqDelegate setDataSavePendingWithValues:FALSE];
        }
        
    }
    
    
    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
    
}

//-----------------------------------------------------------------------

#pragma mark - Keyboard Controls Delegate Methods

//-----------------------------------------------------------------------

- (void)keyboardControlsDonePressed:(KeyboardControls *)keyboardControls{
    [keyboardControls.activeField resignFirstResponder];
}

- (void)keyboardControls:(KeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(KeyboardControlsDirection)direction{
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : DatePicker Delegate Methods 

//---------------------------------------------------------------------------------------------------------------

-(void) getDateFormPicker:(NSString *)date1 :(int)tag1 {
     oldValue = 1;
    self.txtAge.text = [self calculateAge:date1];
    if([self.txtAge.text length] > 0) {
        [self.btnDateOfBirth setTitle:date1 forState:UIControlStateNormal];
        [self.txtDateOfBirth setText:date1];
    }
  
}


//---------------------------------------------------------------------------------------------------------------

- (NSString *) calculateAge :(NSString *)dob {
    NSString * finalAge = @"";
    @try {
        NSArray * arrComp = [dob componentsSeparatedByString:@"/"];
        
        NSString * month = [arrComp objectAtIndex:0];
        NSString * day = [arrComp objectAtIndex:1];
        NSString * year = [arrComp objectAtIndex:2];
        
        
        int bdate=[year intValue];
        int bmon =[month intValue];
        int bday =[day intValue];
        
        
        NSDate * date123 = [NSDate date];
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setDateFormat:kAppDateFormat];
        
        NSString * currDate = [df stringFromDate:date123];
        
        arrComp = [currDate componentsSeparatedByString:@"/"];
        
        NSString * curmonth = [arrComp objectAtIndex:0];
        NSString * curday = [arrComp objectAtIndex:1];
        NSString * curyr = [arrComp objectAtIndex:2];
        
        int curYear = [curyr intValue];
        int curMonth =[curmonth intValue];
        int curDay = [curday intValue];
        
       	
        int age = curYear-bdate;
        if (bmon>curMonth) {
            age--;
        }
        
        if (bmon==curMonth && bday>curDay) {
            age--;
        }
        
        finalAge=[NSString stringWithFormat:@"%d",age];
        
        if([finalAge integerValue] < 0) {
            [self.btnDateOfBirth setTitle:@"" forState:UIControlStateNormal];
            [self.txtDateOfBirth setText:@""];
            DisplayAlertWithTitle(NSLocalizedString(@"DOB Must be less than the current date.", nil), kAppName);
            return nil;
        }
        
        
    }
    @catch (NSException * e) {
    }
    @finally {
        
    }
    return finalAge;
}

//---------------------------------------------------------------------------------------------------------------

- (void) doneWithDate {
    
    oldValue = 1;
    if ([self.strClientId isEqualToString:@"0"] || ![AppDelegate sharedInstance].isAutoSave) {
        [AppDelegate sharedInstance].isFieldUpdated = YES;
        [self.pop dismissPopoverAnimated:YES];
        return;
    }
    
    if (oldValue == 1) {
        oldValue = 0;
        @try {
            if(![[self.btnDateOfBirth titleForState:UIControlStateNormal] length] > 0 && ![self.txtAge.text length] > 0) {
                DisplayAlertWithTitle(NSLocalizedString(@"Select Date.", nil), kAppName);
                return;
            }
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                       kBirthDate,kfieldName,
                                       [self.btnDateOfBirth titleForState:UIControlStateNormal],kfieldValue,
                                       kAge,kfieldName2,
                                       self.txtAge.text,kfieldValue2,
                                       self.strClientId,kClientId,
                                       @"1",kDictType,
                                       nil];
            DebugLOG([dictInfo description]);
            if ([[DataManager initDB] updateParqField:dictInfo]) {
                DebugLOG(@"Not Updated");
            } else {
                DebugLOG(@"Updated");
                [[NSUserDefaults standardUserDefaults] setObject:self.txtAge.text forKey:kAge];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
        }
        @catch (NSException* ex) {
            DebugSTRLOG(@"do Something failed: %@",ex);
        }	
    }
    [self.pop dismissPopoverAnimated:YES];
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark : Popover Delegate Methods 

//---------------------------------------------------------------------------------------------------------------

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    
    //Coometed by SUNIl save by save button
//    [self doneWithDate];

    return YES;
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Imagepicker Delegate Methods 

//---------------------------------------------------------------------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    START_METHOD
    UIImage *orgImage = info[UIImagePickerControllerEditedImage];
    [self.btnProfilePic setBackgroundImage:orgImage forState:UIControlStateNormal];
    [self.pop dismissPopoverAnimated:YES];
    self.pop = nil;
    
    MESSAGE(@"imagePickerController- strClientId---> %@",strClientId);
    
      if(![strClientId isEqualToString:@"0"]) {
    [self saveProfileImageWithClientId:self.strClientId];
      }
   END_METHOD
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    

}



//---------------------------------------------------------------------------------------------------------------

#pragma mark : Parq QuestionView Delegate Methods

//---------------------------------------------------------------------------------------------------------------


- (void)showSignatureViewWithImage:(UIImage *)signImage {
    
    SignatureVC *signView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignatureView"];
    [signView setModalPresentationStyle:UIModalPresentationFormSheet];
    signView.signatureDelegate = (id)self;
    signView.signImage = signImage;
    [self presentViewController:signView animated:YES completion:^{
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) textfieldBeginWithFrame:(CGRect) frame {
    [self.parqDelegate hideOptionsViewIfDisplayed];
    if(self.setAsLandscape) {
        [self.scrollVew setContentOffset:CGPointMake(0,frame.origin.y + 700) animated:YES];
  
    }else {
        [self.scrollVew setContentOffset:CGPointMake(0,frame.origin.y + 500) animated:YES];
  
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) textfieldEndWithFrame : (CGRect) frame {
   
}

//---------------------------------------------------------------------------------------------------------------
-(void)createOrUpdateSheetForName:(NSString*)strSheetName isUpdate:(BOOL)isUpadate{
    [self openTheTermsAndConditions];
}

//-----------------------------------------------------------------------

- (void)openTheTermsAndConditions {

    
    ParQquestionView * parQOBJ = (ParQquestionView *) [self.view viewWithTag:2000];
    imgSignature = [parQOBJ getImageIfExistForSignature];
    
    
    NSString *strFirstName = self.txtFname.text;
    NSString *strLastName= self.txtLname.text;
    

     if (!([[NSUserDefaults standardUserDefaults] valueForKey:kTrainerName])) {
        
        AddNameVC *objAddNameVC = (AddNameVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddNameView"];
        objAddNameVC.strScreenName = @"EnterTrainerName";
        objAddNameVC.strFiledContent = nil;
         objAddNameVC.delegate = (id)self;
        UINavigationController *modalController = [[UINavigationController alloc]initWithRootViewController:objAddNameVC];
        modalController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        modalController.modalPresentationStyle =  UIModalPresentationFormSheet;

        
        if (iOS_8) {
            CGPoint frameSize = CGPointMake(350, 160);
            modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        
       [self.view.window.rootViewController presentViewController:modalController animated:YES completion:^{
        }];
         

         
        return;
    }
    else if (allTrim(strFirstName).length == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Plaese add your first name.", nil),kAppName);
        return;
    }
    else if (allTrim(strLastName).length == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Plaese add your last name.", nil),kAppName);
        return;
    }
    else{
        
            UIStoryboard*  storybBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            GraphFullScreenVC * graphFullScreenObj = [storybBoard  instantiateViewControllerWithIdentifier:@"GraphFullScreenView"];
            if (imgSignature) {
                graphFullScreenObj.imgSignature = imgSignature;
            }
            graphFullScreenObj.strName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLname.text];
            graphFullScreenObj.strClientID = self.strClientId;
            graphFullScreenObj.strEmail = self.txtEmail1.text;
            graphFullScreenObj.isFromTermsAndConditions = true;
            if(self.setAsLandscape || UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                graphFullScreenObj.isLandscapeMode = TRUE;
            } else {
                graphFullScreenObj.isLandscapeMode = FALSE;
            }
        
        
            UINavigationController *navcGraphViewController = [[UINavigationController alloc]initWithRootViewController:graphFullScreenObj];
            AppDelegate *delegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];

            [delegate.window.rootViewController presentViewController:navcGraphViewController animated:YES completion:^{
            }];
 
    }
}

//---------------------------------------------------------------------------------------------------------------
-(void)saveParQDataFromView {
    
    START_METHOD
     [self saveParqViewData];
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : signature View Delegate

//---------------------------------------------------------------------------------------------------------------

-(void) DoneImage :(UIImage *) image {
    
    START_METHOD
    imgSignature = image;
    ParQquestionView * parQOBJ = (ParQquestionView *) [self.view viewWithTag:2000];
    [parQOBJ setSignature:image];
    
    END_METHOD
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : Keyboard Toolbar  Methods 

//---------------------------------------------------------------------------------------------------------------

- (void)previousBtnTapped {
    
    
    UITextField *txt = (UITextField *) [self.scrollVew findFirstResponder];
     [txt resignFirstResponder];
    
    if([txt tag] == kTextFieldTag + 1) {
        [self.scrollVew setContentOffset:CGPointMake(0, 0) animated:YES];
        
    } else {
        UITextField *newTxt = (UITextField * )[self.scrollVew viewWithTag:[txt tag] - 1];
        [newTxt becomeFirstResponder];
        
        if(newTxt == self.txtAge) {
            [newTxt resignFirstResponder];
            UITextField *nxt = (UITextField * )[self.scrollVew viewWithTag:[newTxt tag] - 2];
            [nxt becomeFirstResponder];
            [self.scrollVew setContentOffset:CGPointMake(0, nxt.frame.origin.y -140) animated:YES];
        }
        
        [self.scrollVew setContentOffset:CGPointMake(0, newTxt.frame.origin.y -140) animated:YES];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)nextBtnTapped {
    
    
    UITextField *txt = (UITextField *) [self.scrollVew findFirstResponder];
    [txt resignFirstResponder];
    
    if(txt == self.txtE2Work) {
        [txt resignFirstResponder];
        return;
    }
    
    UITextField *newTxt = (UITextField * )[self.scrollVew viewWithTag:[txt tag] + 1];
    [newTxt becomeFirstResponder];
    
    if(newTxt == self.txtDateOfBirth) {
        [newTxt resignFirstResponder];
        UITextField *nxt = (UITextField * )[self.scrollVew viewWithTag:[newTxt tag] + 2];
        [nxt becomeFirstResponder];
    }
  
    [self.scrollVew setContentOffset:CGPointMake(0, newTxt.frame.origin.y - 150) animated:YES];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)doneButtonTapped {
    
    UITextField *txt = (UITextField *) [self.scrollVew findFirstResponder];
    [txt resignFirstResponder];
    
}


//---------------------------------------------------------------------------------------------------------------
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}


//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
   
    //propic layout
    
    CGRect rectOfbtnSaveParQ = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnSaveParq.titleLabel.text, nil) font:self.btnSaveParq.titleLabel.font height:30];
    [self.btnProfilePic setFrame:CGRectMake((self.scrollVew.frame.size.width - (rectOfbtnSaveParQ.size.width + 80 + 150)), 32, 80, 80)];
    [self.btnSaveParq setFrame:CGRectMake((self.btnProfilePic.frame.origin.x + self.btnProfilePic.frame.size.width + 10), 32, rectOfbtnSaveParQ.size.width + 10, 30)];
    
    
    // first row
    [self.lblDate setFrame:CGRectMake(156, 104, 84, 26)];
    [self.txtDate setFrame:CGRectMake(264, 102, 211, 30)];
    [self.lblName setFrame:CGRectMake(156, 137, 84, 31)];
    [self.txtFname setFrame:CGRectMake(264, 139, 211, 30)];
    [self.txtLname setFrame:CGRectMake(497, 139, 211, 30)];
    [self.lblGender setFrame:CGRectMake(145, 175, 95, 31)];
    [self.btnMale setFrame:CGRectMake(270, 178, 26, 25)];
    [self.lblMale setFrame:CGRectMake(304, 175, 70, 31)];
    [self.btnFemale setFrame:CGRectMake(380, 178, 26, 25)];
    [self.lblFemale setFrame:CGRectMake(410, 175, 73, 31)];
    [self.lblDob setFrame:CGRectMake(184, 210, 56, 31)];
    [self.txtDateOfBirth setFrame:CGRectMake(264, 211, 211, 30)];
    [self.lblAge setFrame:CGRectMake(497, 210, 146, 31)];
    [self.txtAge setFrame:CGRectMake(540, 211, 48, 30)];
    [self.lblPhone setFrame:CGRectMake(180, 250, 62, 31)];
    [self.txtDayPhone setFrame:CGRectMake(264, 250, 211, 30)];
    [self.txtEvePhone setFrame:CGRectMake(497, 250, 211, 30)];
    [self.lblCell setFrame:CGRectMake(184, 289, 56, 31)];
    [self.txtCell setFrame:CGRectMake(264, 289, 211, 30)];
    [self.lblEmail setFrame:CGRectMake(135, 328, 105, 30)];
    [self.txtEmail1 setFrame:CGRectMake(264, 328, 211, 30)];
    [self.txtEmail2 setFrame:CGRectMake(497, 328, 211, 30)];
    
    [self.imgViewAstric1 setFrame:CGRectMake(479, 137, 7, 8)];
    [self.imgVIewAstric2 setFrame:CGRectMake(713, 138, 7, 8)];
    [self.imgviewAstric3 setFrame:CGRectMake(479, 212, 7, 8)];
    [self.imgviewAstric4 setFrame:CGRectMake(478, 330, 7, 8)];
    
    // second row
    
    [self.lblE1Emergency setFrame:CGRectMake(132, 370, 230, 31)];
    [self.lblE1Name setFrame:CGRectMake(184, 409, 56, 31)];
    [self.txtE1Name setFrame:CGRectMake(264, 409, 211, 30)];
    [self.lblE1Relation setFrame:CGRectMake(114, 448, 126, 31)];
    [self.txtE1Relation setFrame:CGRectMake(264, 448, 211, 30)];
    [self.lblE1Phone setFrame:CGRectMake(125, 487, 115, 31)];
    [self.txtE1PhoneCell setFrame:CGRectMake(264, 486, 211, 30)];
    [self.txtE1PhoneHome setFrame:CGRectMake(497, 486, 211, 30)];
    [self.lblE1Work setFrame:CGRectMake(140, 526, 100, 31)];
    [self.txtE1Work setFrame:CGRectMake(264, 526, 211, 30)];
    
    
    // third row
    [self.lblE2Emergency setFrame:CGRectMake(132, 565, 230, 31)];
    [self.lblE2Name setFrame:CGRectMake(184, 598, 56, 31)];
    [self.txtE2Name setFrame:CGRectMake(264, 598, 211, 30)];
    [self.lblE2Relation setFrame:CGRectMake(114, 637, 126, 31)];
    [self.txtE2Relation setFrame:CGRectMake(264, 638, 211, 30)];
    [self.lblE2Phone setFrame:CGRectMake(125, 676, 115, 31)];
    [self.txtE2PhoneCell setFrame:CGRectMake(264, 676, 211, 30)];
    [self.txtE2PhoneHome setFrame:CGRectMake(497, 676, 211, 30)];
    [self.lblE2Work setFrame:CGRectMake(140, 715, 100, 31)];
    [self.txtE2Work setFrame:CGRectMake(264, 715, 211, 30)];

    
    [self.scrollVew setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight - 180)];
   
    if ([self.view viewWithTag:2000] != nil) {
        ParQquestionView *parqView = (ParQquestionView *)[self.view viewWithTag:2000];
        [parqView setLandscapeLayout];
    }else {
        [self setupQuestionnareView];
  
    }
     [self.scrollVew setContentSize:CGSizeMake(kLandscapeWidth,2900)];
}


//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
    
    //propic changes
    

    CGRect rectOfbtnSaveParQ = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnSaveParq.titleLabel.text, nil) font:self.btnSaveParq.titleLabel.font height:30];
    
    [self.btnProfilePic setFrame:CGRectMake((self.scrollVew.frame.size.width - (rectOfbtnSaveParQ.size.width + 80 + 90)), 32, 80, 80)];
    [self.btnSaveParq setFrame:CGRectMake((self.btnProfilePic.frame.origin.x + self.btnProfilePic.frame.size.width + 10), 32, rectOfbtnSaveParQ.size.width + 10, 30)];
    
    
    // first row
    [self.lblDate setFrame:CGRectMake(56, 104, 84, 26)];
    [self.txtDate setFrame:CGRectMake(164, 102, 211, 30)];
    [self.lblName setFrame:CGRectMake(56, 137, 84, 31)];
    [self.txtFname setFrame:CGRectMake(164, 139, 211, 30)];
    [self.txtLname setFrame:CGRectMake(397, 139, 211, 30)];
    [self.lblGender setFrame:CGRectMake(45, 175, 95, 31)];
    [self.btnMale setFrame:CGRectMake(170, 178, 26, 25)];
    [self.lblMale setFrame:CGRectMake(204, 175, 60, 31)];
    [self.btnFemale setFrame:CGRectMake(270, 178, 26, 25)];
    [self.lblFemale setFrame:CGRectMake(304, 175, 68, 31)];
    [self.lblDob setFrame:CGRectMake(84, 210, 56, 31)];
    [self.txtDateOfBirth setFrame:CGRectMake(164, 211, 211, 30)];
    [self.lblAge setFrame:CGRectMake(397, 210, 146, 31)];
    [self.txtAge setFrame:CGRectMake(440, 211, 48, 30)];
    [self.lblPhone setFrame:CGRectMake(80, 250, 62, 31)];
    [self.txtDayPhone setFrame:CGRectMake(164, 250, 211, 30)];
    [self.txtEvePhone setFrame:CGRectMake(397, 250, 211, 30)];
    [self.lblCell setFrame:CGRectMake(84, 289, 56, 31)];
    [self.txtCell setFrame:CGRectMake(164, 289, 211, 30)];
    [self.lblEmail setFrame:CGRectMake(35, 328, 105, 30)];
    [self.txtEmail1 setFrame:CGRectMake(164, 328, 211, 30)];
    [self.txtEmail2 setFrame:CGRectMake(397, 328, 211, 30)];
    
    [self.imgViewAstric1 setFrame:CGRectMake(379, 137, 7, 8)];
    [self.imgVIewAstric2 setFrame:CGRectMake(613, 138, 7, 8)];
    [self.imgviewAstric3 setFrame:CGRectMake(379, 212, 7, 8)];
    [self.imgviewAstric4 setFrame:CGRectMake(378, 330, 7, 8)];
    
    // second row
    
    [self.lblE1Emergency setFrame:CGRectMake(32, 370, 230, 31)];
    [self.lblE1Name setFrame:CGRectMake(84, 409, 56, 31)];
    [self.txtE1Name setFrame:CGRectMake(164, 409, 211, 30)];
    [self.lblE1Relation setFrame:CGRectMake(14, 448, 126, 31)];
    [self.txtE1Relation setFrame:CGRectMake(164, 448, 211, 30)];
    [self.lblE1Phone setFrame:CGRectMake(25, 487, 115, 31)];
    [self.txtE1PhoneCell setFrame:CGRectMake(164, 486, 211, 30)];
    [self.txtE1PhoneHome setFrame:CGRectMake(397, 486, 211, 30)];
    [self.lblE1Work setFrame:CGRectMake(40, 526, 100, 31)];
    [self.txtE1Work setFrame:CGRectMake(164, 526, 211, 30)];
    
    
    // third row
    [self.lblE2Emergency setFrame:CGRectMake(32, 565, 230, 31)];
    [self.lblE2Name setFrame:CGRectMake(84, 598, 56, 31)];
    [self.txtE2Name setFrame:CGRectMake(164, 598, 211, 30)];
    [self.lblE2Relation setFrame:CGRectMake(14, 637, 126, 31)];
    [self.txtE2Relation setFrame:CGRectMake(164, 638, 211, 30)];
    [self.lblE2Phone setFrame:CGRectMake(25, 676, 115, 31)];
    [self.txtE2PhoneCell setFrame:CGRectMake(164, 676, 211, 30)];
    [self.txtE2PhoneHome setFrame:CGRectMake(397, 676, 211, 30)];
    [self.lblE2Work setFrame:CGRectMake(40, 715, 100, 31)];
    [self.txtE2Work setFrame:CGRectMake(164, 715, 211, 30)];

   
    [self.scrollVew setFrame:CGRectMake(0, 0, kPotraitWidth, kPotraitHeight- 78)];
    
    
    if ([self.view viewWithTag:2000] != nil) {
        ParQquestionView *parqView = (ParQquestionView *)[self.view viewWithTag:2000];
        [parqView setPotraitLayout];
    }else {
        [self setupQuestionnareView];
        
    }
    [self.scrollVew setContentSize:CGSizeMake(kPotraitWidth,3000)];
    
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : View Lifecycle Methods

//---------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self localizeControls];
    // Do any additional setup after loading the view
    [self setupKeyBoardAccessoryView];
    [self addleftViewToTextFields];
    [self assignTagsToTexFields];
    [self displayClientDetail];
    [self setupQuestionnareView];
    
    self.scrollVew.delegate = self;
    [self.scrollVew setBounces:YES];
    [self.scrollVew setScrollEnabled:YES];
    
    if([self.strClientId isEqualToString:@"0"]) {
        NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
        [tmpdt setDateFormat:@"MM/dd/yy hh:mm a"];
        NSString * updatedDate = [tmpdt stringFromDate:[NSDate date]];
        self.txtDate.text = updatedDate;
    }

   
    //UpdateSignatute
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"UpdateSignatute" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeSignature:) name:@"UpdateSignatute" object:nil];
    
    //ShowTermsScreen
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"ShowTermsScreen" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(presentTermsScreen:) name:@"ShowTermsScreen" object:nil];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.scrollVew addGestureRecognizer:singleTap];
    
    
    
}


//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
   START_METHOD
    [super viewWillAppear:animated];
    
    //Sunil
  //invoke method for set view
    [self setViewOnBasisSkin];
    
    
}

//Sunil
-(void)setViewOnBasisSkin{
    //Sunil
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            [self.btnSaveParq setBackgroundColor:kFirstSkin];
            break;
            
        case SECOND_TYPE:
            [self.btnSaveParq setBackgroundColor:ksecondSkin];
            break;
            
            
        case THIRD_TYPE:
            [self.btnSaveParq setBackgroundColor:kThirdSkin];
            break;
            
        default:
            [self.btnSaveParq setBackgroundColor:kOldSkin];
            break;
    }
}

//sunil

-(void)setMaleButtonImage{
    START_METHOD
    //Sunil
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            [self.btnMale setImage:[UIImage imageNamed:@"green_gender_icon.png"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
             [self setObjectsColor:[UIColor whiteColor]];
            break;
            
        case SECOND_TYPE:
            [self.btnMale setImage:[UIImage imageNamed:@"red_gender_icon.png"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            
             [self setObjectsColor:[UIColor whiteColor]];
            
            break;
            
            
        case THIRD_TYPE:
            [self.btnMale setImage:[UIImage imageNamed:@"pink_gender_icon.png"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            
            
             [self setObjectsColor:[UIColor grayColor]];
            break;
            
        default:
            [self.btnMale setImage:[UIImage imageNamed:@"Button-Checked"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            
            [self setObjectsColor:[UIColor grayColor]];
            break;
    }
}

-(void)setFemaleButtonImage{
    START_METHOD
    //Sunil
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
 
    switch (skin) {
        case FIRST_TYPE:
            [self.btnMale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"green_gender_icon.png"] forState:UIControlStateNormal];
            
            [self setObjectsColor:[UIColor whiteColor]];
            
            break;
            
        case SECOND_TYPE:
            [self.btnMale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"red_gender_icon.png"] forState:UIControlStateNormal];
            
             [self setObjectsColor:[UIColor whiteColor]];
            break;
            
            
        case THIRD_TYPE:
            [self.btnMale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"pink_gender_icon.png"] forState:UIControlStateNormal];
            
            [self setObjectsColor:[UIColor grayColor]];
            
            break;
            
        default:
            [self.btnMale setImage:[UIImage imageNamed:@"Button-Unchecked"] forState:UIControlStateNormal];
            [self.btnFemale setImage:[UIImage imageNamed:@"Button-Checked"] forState:UIControlStateNormal];
            
           [self setObjectsColor:[UIColor grayColor]];
            
            break;
    }
    
    
    END_METHOD
}

//sunil
-(void)setBgColor{
    START_METHOD
    
    END_METHOD
}

//sunil
-(void)setObjectsColor:(UIColor *)color{
 
    
    self.lblAge.textColor = color;
    self.lblCell.textColor = color;
    self.lblDate.textColor = color;
    self.lblDob.textColor = color;
    
    self.lblE1Emergency.textColor = color;
    self.lblE1Name.textColor = color;
    self.lblE1Phone.textColor = color;
    self.lblE1Relation.textColor = color;
    
    self.lblE1Name.textColor = color;
    self.lblE1Work.textColor = color;
    self.lblE2Emergency.textColor = color;
    self.lblE2Name.textColor = color;
    
    
    self.lblE2Phone.textColor = color;
    self.lblE2Relation.textColor = color;
    self.lblE2Work.textColor = color;
    self.lblEmail.textColor = color;
    
    self.lblEmail.textColor = color;
    self.lblFemale.textColor = color;
    self.lblGender.textColor = color;
    self.lblMale.textColor = color;
    
    self.lblName.textColor = color;
    self.lblPhone.textColor = color;
   
}
//---------------------------------------------------------------------------------------------------------------

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
          [self.scrollVew setContentSize:CGSizeMake(kLandscapeWidth,2900)];
    } else {
        [self.scrollVew setContentSize:CGSizeMake(kPotraitWidth,3000)];
    }
  
  }


//---------------------------------------------------------------------------------------------------------------

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//--> Sunil
//TODO: POST REQUEST



//Method for post request add client
-(void)postRequestToServerForClient:(NSMutableDictionary *)dictInfo{
    START_METHOD
    
    //Set Trainer ID
    [dictInfo setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
   
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    
    
    UIImage * imageClient = (UIImage *)[self.btnProfilePic backgroundImageForState:UIControlStateNormal];
    NSData  *imageData;
    MESSAGE(@"imageClient: : %@:",imageClient);
    
    
    if(imageClient == nil) {
        UIImage * imageTemp = nil;
        //Get Image data
        imageData   =   UIImageJPEGRepresentation(imageTemp, 1.0f);
    }else{
        
        //Get Image data
        imageData   =   UIImageJPEGRepresentation(imageClient, 1.0f);
        
    }
    
    
    MESSAGE(@"postRequestToServerForClient-> params=: %@ and Url : %@",dictInfo,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //Invoke method for post on server with Images
    [objService postImageWithURl:strUrl parameters:dictInfo andImageData:imageData WithImageName:@"clientProfileImage.png" success:^(NSDictionary *responseDataDictionary) {
        
        MESSAGE(@"responce from file: %@", responseDataDictionary);
        
        //Hide the indicator
        [TheAppController hideHUDAfterDelay:0];
        
        //If Success
        if([responseDataDictionary objectForKey:PAYLOAD]){
            
        
        //************  GET Client's Profile AND SAVE IN LOCAL DB
        NSDictionary *dictClinetInfo =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
        
        MESSAGE(@"arrayUserData : dictClinetInfo: %@",dictClinetInfo);
        
        NSString *strAction =   [dictClinetInfo objectForKey:ACTION];
        
        if([strAction isEqualToString:ADD_CLIENT]){
            
            //Set Client ID
            self.strClientId    =   [NSString stringWithFormat:@"%@",[dictClinetInfo objectForKey:kClientId]];
            
            //Invoke method for save in loacl db
            [self insertNewClientInLocalDb:dictInfo];
            
            //Invoke method for Get Add more client Status for user
            [self checkAddClientStatus];
            
        }else{
            
            //Invoke method for update client imfo
            [self updateClientInfo:dictInfo ];
            
        }
        }else{//If Server respose a Error
            
            //Check
            if([responseDataDictionary objectForKey:METADATA]){
                
                //Get Dictionary
                NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                
                //Show Error Alert
                [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                
                //Hide Indicator
                [TheAppController hideHUDAfterDelay:0];
                
                
                //For Unauthorized user --->
                if( [dictError objectForKey:LIST_KEY]){
                    
                    NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                    
                    if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                        
                        //Show Error Alert
                        [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                        
                        
                        //Move to dashbord for login
                        [commonUtility  logOut];
                    }
                    
                }
                
                //<-----
                
                
            }
        }
        
        
    }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
        
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
        
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
        
                           }];
    END_METHOD
}



-(void)postRequestForUpdateClientInfo:(NSMutableDictionary *)dictInfo{
    START_METHOD
    
    //Set Trainer ID
    [dictInfo setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"postRequestToServerForClient-> params=: %@ and Url : %@",dictInfo,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    
    //Invoke method for post on server with Images
    [objService postRequestWitHUrlWithFormData:strUrl parameters:dictInfo success:^(NSDictionary *responseDataDictionary) {
        
        MESSAGE(@"responce from file: %@", responseDataDictionary);
        
        //Hide the indicator
        [TheAppController hideHUDAfterDelay:0];
        
        //If Success
        if([responseDataDictionary objectForKey:PAYLOAD]){
            
            
            //************  GET Client's Profile AND SAVE IN LOCAL DB
            NSDictionary *dictClinetInfo =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
            
            MESSAGE(@"arrayUserData : dictClinetInfo: %@",dictClinetInfo);
            
            NSString *strAction =   [dictClinetInfo objectForKey:ACTION];
            
            if([strAction isEqualToString:ADD_CLIENT]){
                
                //Set Client ID
                self.strClientId    =   [NSString stringWithFormat:@"%@",[dictClinetInfo objectForKey:kClientId]];
                
                //Invoke method for save in loacl db
                [self insertNewClientInLocalDb:dictInfo];
                
            }else{
                
                //Invoke method for update client imfo
                [self updateClientInfo:dictInfo ];
                
            }
        }else{//If Server respose a Error
            
            //Check
            if([responseDataDictionary objectForKey:METADATA]){
                
                //Get Dictionary
                NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                
                //Show Error Alert
                [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                
                //Hide Indicator
                [TheAppController hideHUDAfterDelay:0];
                
                
                //For Unauthorized user --->
                if( [dictError objectForKey:LIST_KEY]){
                    
                    NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                    
                    if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                      
                        //Show Error Alert
                        [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                        
                        
                        //Move to dashbord for login
                        [commonUtility  logOut];
                    }
                    
                }
                
                //<-----
                
                
            }
        }
        
        
    }failure:^(NSError *error) {
        MESSAGE(@"Eror : %@",error);
        
        //Hide the indicator
        [TheAppController hideHUDAfterDelay:0];
        
        //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
        
    }];
    END_METHOD
}




-(void)insertNewClientInLocalDb:(NSMutableDictionary *)dictInfo{
    START_METHOD
    
    //set clientd id
    [dictInfo setValue:self.strClientId forKey:kClientId];
    [dictInfo setValue:[dictInfo objectForKey:CLIENT_STATUS] forKey:STATUS];
   
    
    //Insert into db
     [[DataManager initDB] insertNewClientUpgraded:dictInfo];
    
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    //If not Upgrade then show alert for upgrade
    if([strUpgradeStatus isEqualToString:@"YES"]){
    
    //Invoke method for update the client status Migrated YES
    [[DataManager initDB] updateClientMigrationStatus:dictInfo ];
    }
    
    
    
    NSString * cName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLname.text];
    
        if (![self.strClientId isEqualToString:@" "]) {
            [self.parqDelegate displayClientID:self.strClientId:cName];
            
            
            //Invoke method for post on server
             [commonUtility alertMessage:(NSLocalizedString(@"Data Saved", nil))];
            
        }
        else {
            DisplayAlertWithTitle(NSLocalizedString(@"Data not saved successfully.", nil),kAppName);
            return;
        }
    
    if(![strClientId isEqualToString:@"0"]) {
        
          MESSAGE(@"![strClientId isEqualToString:");
        
        [self saveProfileImageWithClientId:self.strClientId];
        ParQquestionView * parQOBJ = (ParQquestionView *) [self.view viewWithTag:2000];
        parQOBJ.strClientId = self.strClientId;
        [parQOBJ btnSubmit_click];
        [AppDelegate sharedInstance].isFieldUpdated = NO;
    }
    
    //Invoke method for carete Default Asssessemnt for Clinet
    [self careteDefaultAssessment];
    
    //Invoke method for post client's answers to server
    [self craeteDictForAnswer];
    END_METHOD
}

-(void)updateClientInfo:(NSMutableDictionary *)dictInfo{
    
    START_METHOD
    NSString * cName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLname.text];
    [dictInfo setValue:[dictInfo objectForKey:CLIENT_STATUS] forKey:STATUS];
    
    self.strClientId = [[DataManager initDB] updateClientDetail:dictInfo];
    [self.parqDelegate displayClientID:self.strClientId:cName];
    
    //Show alert
//    [commonUtility alertMessage:(NSLocalizedString(@"Data Saved", nil))];
    
    if(![strClientId isEqualToString:@"0"]) {
        
          MESSAGE(@"![strClientId isEqualToString:");
        
        
//        [self saveProfileImageWithClientId:self.strClientId];
        
        ParQquestionView * parQOBJ = (ParQquestionView *) [self.view viewWithTag:2000];
        parQOBJ.strClientId = self.strClientId;
        [parQOBJ btnSubmit_click];
        [AppDelegate sharedInstance].isFieldUpdated = NO;
    }
    
    //Invoke method for client's answers
    [self craeteDictForAnswer];
    END_METHOD
}

//Method for post Answers to server
-(void)craeteDictForAnswer{
    START_METHOD
     NSArray     *arrayAnswer    =   [NSArray arrayWithArray:[[DataManager initDB] getAllAnswersByClientID:self.strClientId]];
    
    MESSAGE(@"arrayAllClients-> arrayAnswer: %@",arrayAnswer);
    
    //Dictionry of answers
    NSMutableDictionary     *dictAnswers    =   [[NSMutableDictionary alloc]init];
    [dictAnswers setValue:arrayAnswer forKey:@"answers"];
    [dictAnswers setValue:@"clientAnswers" forKey:ACTION];
    
    //Invoke method for post on server
    [self postRequestToServerForUpdateAnswers:dictAnswers ];
    
    END_METHOD
}

//Method for post request add client
-(void)postRequestToServerForUpdateAnswers:(NSMutableDictionary *)dictAnswers{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictAnswers=: %@ and Url : %@",dictAnswers,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictAnswers
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictAnswerTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send dictAnswerTemp Dictonary : %@ \n and response from server dictAnswerTemp: %@",dictAnswers,dictAnswerTemp);
                              
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}



-(void)careteDefaultAssessment{
    START_METHOD
    
    NSDictionary *dictAssessment =@{
                                    ksheetName : @"Assessment-1",
                                    kClientId :self.strClientId,
                                    ACTION:@"addAssessment",
                                    @"user_id":[commonUtility retrieveValue:KEY_TRAINER_ID]

                                    };
    
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictAssessment=: %@ and Url : %@",dictAssessment,strUrl);
    
    
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictAssessment
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce dictAssessment from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictAssesmentTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send dictAssessment Dictonary : %@ \n and response from server dictAssessment: %@",dictAssessment,dictAssesmentTemp);
                                   
                                   
                                   NSString *strAction =   [dictAssesmentTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addAssessment"]){
                                   
                                       [[DataManager initDB] insertNewAssessmentSheetCleintID:self.strClientId sheetName:[dictAssesmentTemp objectForKey:ksheetName] andSheetID:[dictAssesmentTemp objectForKey:@"nDetailID" ]];
                                   }
                                   
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                       
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                               
                               
                           }];
    END_METHOD
}






//TODO: post Request For Save Signautre Image On Server

-(void)postRequestForSaveProfileImageOnServer :(NSString *) clientId{
    
    UIImage * img = (UIImage *)[self.btnProfilePic backgroundImageForState:UIControlStateNormal];


        NSData *imageData;
    
    if(img == nil){
        
        UIImage *imgTemp    =   nil;
        imageData       =   UIImageJPEGRepresentation(imgTemp, 1.0f);
    }else{
        
        imageData       =   UIImageJPEGRepresentation( img, 1.0f);
    }
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //Craete Dictionry
    NSMutableDictionary *objDict            =   [[NSMutableDictionary alloc]init];
    
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN     forKey:@"access_key"];
    [objDict setValue:@"client_image"       forKey:ACTION];
    [objDict setValue:self.strClientId           forKey:kClientId];
    
    
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"ProfileImage%@.jpeg",self.strClientId];
        
    }else{
        strFileName     =   @"";
    }
    
    
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    
    MESSAGE(@"strUrl: %@ \n\n\nand strfilename: %@ \n\n\nandimageData: %@ \n\n\nand objDict:%@",strUrl,strFileName,imageData,objDict);
    
    
    if(imageData!=nil){
    
        [TheAppController showHUDonView:nil];
        
        
    //Craete object of service handeler
    ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
    
    //Invoke method for post images on server
    [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                 success:^(NSDictionary *responseDataDictionary) {
                                     
                                     //Hide Indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"postClientsImageOnServer success dictionary: %@",responseDataDictionary);
                                     
                                     //Invoke method for save in laocl
                                     [self setProfileImageOfClient:clientId];
                                     
                                     [TheAppController hideHUDAfterDelay:0.0];
                                     
                                     
                                 } failure:^(NSError *error) {
                                     MESSAGE(@"Error");
                                     
                                     
                                     //Hide Indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     
                                 }];    
    }else{
        return;
    }
    
    END_METHOD
}





//Method for save profile image in local
-(void)setProfileImageOfClient :(NSString *) clientId{
    
    START_METHOD
    if(![clientId isEqualToString:@"0"]) {
        
        UIImage * img = (UIImage *)[self.btnProfilePic backgroundImageForState:UIControlStateNormal];
        [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsProfileImage",[FileUtility basePath]]];
        NSString * imgNm = [NSString stringWithFormat:@"ProfileImage%@",clientId];
        NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
        [FileUtility createFileInFolder:@"ClientsProfileImage" withName:imgNm withData:imageData];
    }
    END_METHOD
}



//TODO: Send mail to the User for Purchase new subcription
-(void)checkAddClientStatus{
    START_METHOD
    MESSAGE(@"checkAddClientStatus--> ");
    
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             @"user_id"          :   [commonUtility retrieveValue:KEY_TRAINER_ID],
                             
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@trainerAddClientStatus/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                                       success:^(NSDictionary *responseDataDictionary) {
                                           
                                           MESSAGE(@"responce:checkAddClientStatus: %@", responseDataDictionary);
                                           
                                           //If Success
                                           if([responseDataDictionary objectForKey:PAYLOAD]){
                                               
                                               //************  GET Trainer's Profile AND SAVE IN LOCAL DB
                                               NSDictionary *dictTrainerProfile =  [ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] ;
                                               MESSAGE(@"arrayUserData : dictTrainerProfile: %@",dictTrainerProfile);
                                               
                                               //For Old User, He can Add any number of clients for full version
                                               [commonUtility saveValue:[dictTrainerProfile objectForKey:@"status"] andKey:KEY_STATUS_ADD_CLIENT];
                                               
                                           }
                                           
                                           //Check if get error for Expire token
                                           if([responseDataDictionary objectForKey:METADATA]){
                                               
                                               //Get Dictionary
                                               NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                               
                                               NSString *strErrorMessage = [dictError objectForKey:POPUPTEXT];
                                               
                                               
                                               if(strErrorMessage.length>0){
                                                   //Show Error Alert
                                                   [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                               }
                                               
                                               
                                           }
                                           
                                       }failure:^(NSError *error) {
                                           MESSAGE(@"Eror : %@",error);
                                           
                                           
                                       }];
    END_METHOD
}



@end
