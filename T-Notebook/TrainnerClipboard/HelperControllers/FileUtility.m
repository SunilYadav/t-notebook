//  
//
//  Created by WLI.
//  Copyright 2011 WLI. All rights reserved.
//

#import "FileUtility.h"

@implementation FileUtility

// ----------------------------------------------------------------------------

#pragma mark -
#pragma mark Path methods

// -----------------------------------------------------------------------------

+ (NSString*)basePath {
    START_METHOD
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSAssert([paths count] == 1, @"");
	return [paths objectAtIndex:0];		
}

// -----------------------------------------------------------------------------

+ (NSString*)cachePath {
    START_METHOD
    
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSAssert([paths count] == 1, @"");
	return [paths objectAtIndex:0];		
}

// -----------------------------------------------------------------------------

+ (void)createDirectoryIfNeededAtPath:(NSString*)path {
    START_METHOD
	NSFileManager* fileManager = [NSFileManager defaultManager];
	
	NSError *error = nil;
	if (![fileManager fileExistsAtPath:path]) {
		[fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
	}
}

// -----------------------------------------------------------------------------

+ (void)createFolderInDocuments:(NSString*)folder {
    START_METHOD
	NSString* path = [NSString stringWithFormat:@"%@/%@", [FileUtility basePath], folder];
	[FileUtility createDirectoryIfNeededAtPath:path];
}

// -----------------------------------------------------------------------------

#pragma mark -
#pragma mark File methods

+ (void)deleteFile:(NSString*)filename {
    START_METHOD
	NSFileManager* manager = [NSFileManager defaultManager];
	NSError* error = nil;
	if (![manager removeItemAtPath:filename error:&error]) {
		DebugSTRLOG(@"File not deleted:",filename);
	}	
}

// -----------------------------------------------------------------------------

+ (NSString *)pathForResource:(NSString *)name ofType:(NSString *)extension {
    START_METHOD
	NSString* result = nil;
	NSString* path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
	if (path == nil) {
		return nil;
	}
	
	result = [[NSString alloc] initWithString:path];
	return result ;
}

// -----------------------------------------------------------------------------

+ (NSString *)pathForDocResource:(NSString *)filename {
    START_METHOD
	NSString* result = nil;
	NSString * filePath = [NSString stringWithFormat:@"%@/%@.xml", [FileUtility basePath],filename];
	if (filePath == nil) {
		return nil;
	}	
	result = [[NSString alloc] initWithString:filePath];
	return result ;
}

// -----------------------------------------------------------------------------

+ (BOOL)fileExists:(NSString *)filename {
    START_METHOD
	NSFileManager* manager = [NSFileManager defaultManager];
	return [manager fileExistsAtPath:filename];
}

// -----------------------------------------------------------------------------

+ (void)createFile:(NSString *)filename {
    START_METHOD
	NSFileManager* manager = [NSFileManager defaultManager];
	NSString * filePath = [NSString stringWithFormat:@"%@/%@.xml", [FileUtility basePath],filename];
	if (![FileUtility fileExists:filePath]){
		[manager createFileAtPath:filePath contents:nil attributes:nil];
	}else {
		DebugSTRLOG(@"File present: ",filename);
	}
}

// -----------------------------------------------------------------------------

+ (void) createFileInFolder:(NSString *)folder withName:(NSString *)fileName withData:(NSData *)contentData {
    START_METHOD
	NSString * filePath = [NSString stringWithFormat:@"%@/%@/%@.png", [FileUtility basePath],folder,fileName];
	BOOL success = [[NSFileManager defaultManager] createFileAtPath:filePath contents:contentData attributes:nil];
	if (success) {
		DebugSTRLOG(@"File Stored at: ",filePath);
        MESSAGE(@"filePath->%@",filePath);
        
	}
	else {
		DebugLOG(@"File not Stored");
        MESSAGE(@"filePath->%@",filePath);
	}

}

@end
