//
//  OptionsViewVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "OptionsViewVC.h"
#import "AppDelegate.h"

#define kVewBGColor [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]

@interface OptionsViewVC (){
    
}

@property (nonatomic , strong) UISwitch                 *switchAutoSave;
@end

@implementation OptionsViewVC
  int countForOption = 0;

@synthesize typeOfOptions;
//---------------------------------------------------------------------------------------------------------------

-(id)initWIthFrame:(CGRect)frame andcontroller:(NSString *)Cntroller {
    
    if(self) {
        [self.view setFrame:frame];
        self.typeOfOptions = Cntroller;
        [self.view setBackgroundColor:[UIColor whiteColor]];
         }
    return self;
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Custom Methods

//---------------------------------------------------------------------------------------------------------------


-(void) setupLayout {
    
     if([self.typeOfOptions isEqualToString:kisParq] || [self.typeOfOptions isEqualToString:kisAssessment])
     {
    
    }
    
    if([self.typeOfOptions isEqualToString:kisParq]) {
        
        UIButton *btn1 =[self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Email to Client", nil)];
        [btn1 addTarget:self action:@selector(exportParQData:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn1];
    
    }
    

    if([self.typeOfOptions isEqualToString:kisAssessment]) {
        
        UIButton *btn1 =[self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Save", nil)];
        [btn1 addTarget:self action:@selector(saveAssessmentData:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn1];
        
        UIButton *btn2 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Delete Assessment", nil)];
        [btn2 addTarget:self action:@selector(deleteAssessmentSheet:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn2];
        

        UIButton *btn4 = [self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:NSLocalizedString(@"Body Fat Norms", nil)];
        [btn4 addTarget:self action:@selector(assessmentHelp:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn4];
        
        UIButton *btn5 = [self btnWithFrame:CGRectMake(15,160, 225, 40) andTitle:NSLocalizedString(@"Email to Client", nil)];
        [btn5 addTarget:self action:@selector(exportTheAssessment:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn5];
    }
    
    if([self.typeOfOptions isEqualToString:kisViewAllPrograms]) {
        UIButton *btn5 = [self btnWithFrame:CGRectMake(15,10, 225, 40) andTitle:NSLocalizedString(@"Email to Client", nil)];
        [btn5 addTarget:self action:@selector(exportViewAllPrograms:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn5];
    }
    
   if ([self.typeOfOptions isEqualToString:kisProgram]) {
        
        UIButton *btn0 =[self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Add New Program", nil)];
        [btn0 addTarget:self action:@selector(AddProgram:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn0];

        UIButton *btn2;
        
        NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
        
        NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
        NSString *strProgramClientID = [[defauts valueForKey:kProgramData] valueForKey:kClientId];
        NSString *strProgramSheetID = [[defauts valueForKey:kProgramData] valueForKey:ksheetID];
        
        if ((strProgramClientID == nil || strProgramSheetID == nil) && strTemplateSheetId == nil) {
             btn2 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Copy Entire Program", nil)];
        }
        else{
            if([strProgramSheetID isEqualToString:self.currentSheetId] && [strProgramClientID isEqualToString:self.strClientId]){
                btn2 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Copy Entire Program", nil)];
            }
            else{
                btn2 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Paste Entire Program", nil)];
            }
        }
        
        [btn2 addTarget:self action:@selector(copyProgram:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn2];
        
        UIButton *btn9;

        NSString *strCurrentCopiedBlockId = [defauts valueForKey:kCopiedBlockIDSheetID];
        NSString *strCurrentSheetID =[defauts valueForKey:kBlockCurrentCopiedSheetID] ;
        NSString *strCurrentCopiedClientID = [defauts valueForKey:kCopiedClientID];
        
        if ([strCurrentCopiedBlockId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.blockSheetNo]] && [strCurrentSheetID isEqualToString:self.currentSheetId] &&  [strCurrentCopiedClientID isEqualToString:self.strClientId]) {
            btn9 = [self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:NSLocalizedString(@"Copy Current Workout", nil)];
        }
        else if([defauts valueForKey:kCopyProgramBlock] || [defauts valueForKey:kCopyBlock]){
            btn9 = [self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:NSLocalizedString(@"Paste Current Workout", nil)];
        }
        else{
            btn9 = [self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:NSLocalizedString(@"Copy Current Workout", nil)];
        }
        
        
        [btn9 addTarget:self action:@selector(CopyWorkoutData:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn9];
       

        
        UIButton *btn5 = [self btnWithFrame:CGRectMake(15, 160, 225, 40) andTitle:NSLocalizedString(@"Edit Program Name", nil)];
        [btn5 addTarget:self action:@selector(editSheetName:) forControlEvents:UIControlEventTouchUpInside];
       
        UIButton *btn10 = [self btnWithFrame:CGRectMake(15, 210, 225, 40) andTitle:NSLocalizedString(@"Edit Workout Name", nil)];
        [btn10 addTarget:self action:@selector(editWorkoutName:) forControlEvents:UIControlEventTouchUpInside];
       
        
        int yPosition = 110;
         if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
          
         }
         else{
             UIButton *btn4 = [self btnWithFrame:CGRectMake(15, 160, 225, 40) andTitle:NSLocalizedString(@"Paste From Pre-Made Templates", nil)];
             [btn4 addTarget:self action:@selector(pasteFromTemplate:) forControlEvents:UIControlEventTouchUpInside];
             [self.view addSubview:btn4];
             yPosition = 160;
            
         }
         yPosition = yPosition+50;
        
        UIButton *btn3 = [self btnWithFrame:CGRectMake(15, yPosition, 225, 40) andTitle:NSLocalizedString(@"Clear Clipboard", nil)];
        [btn3 addTarget:self action:@selector(clearClipboard:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn3];
        
        yPosition = yPosition+50;
        UIButton *btn6 = [self btnWithFrame:CGRectMake(15, yPosition, 225, 40) andTitle:NSLocalizedString(@"Delete Current Workout", nil)];
        [btn6 addTarget:self action:@selector(deleteSheet:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn6];
        
        yPosition = yPosition+50;
        UIButton *btn1 =[self btnWithFrame:CGRectMake(15, yPosition, 225, 40) andTitle:NSLocalizedString(@"Email to Client", nil)];
        [btn1 addTarget:self action:@selector(exportProgram:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn1];
       
        yPosition = yPosition+50;
       UIButton *btnFB =[self btnWithFrame:CGRectMake(15, yPosition, 225, 40) andTitle:@"Facebook Sharing"];
       [btnFB setBackgroundImage:[UIImage imageNamed:@"Facebook_Sharing"] forState:UIControlStateNormal];
       [btnFB addTarget:self action:@selector(facebookShareClicked:) forControlEvents:UIControlEventTouchUpInside];
       [self.view addSubview:btnFB];

        
    }
    
    
    if ([self.typeOfOptions isEqualToString:kProgramTemplate])
    {

        
        UIButton *btn2;
        NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
        
        NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
        NSString *strProgramClientID = [[defauts valueForKey:kProgramData] valueForKey:kClientId];
        NSString *strProgramSheetID = [[defauts valueForKey:kProgramData] valueForKey:ksheetID];
        
        if ((strProgramClientID == nil || strProgramSheetID == nil) && strTemplateSheetId == nil) {
            btn2 = [self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Copy Entire Program", nil)];
        }
        else{
            if([strTemplateSheetId isEqualToString:self.currentSheetId]){
                btn2 = [self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Copy Entire Program", nil)];
            }
            else{
                btn2 = [self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Paste Entire Program", nil)];
            }
        }
        
        [btn2 addTarget:self action:@selector(copyProgram:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn2];
        
        UIButton *btn8;
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];
        if ([strCurrentCopiedBlockId isEqualToString:self.currentBlockId] ) {
             btn8 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Copy Current Workout", nil)];
        }
        else if([defaults valueForKey:kCopyBlock]||[defaults valueForKey:kCopyProgramBlock]){
            btn8 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Paste Current Workout", nil)];
        }
        else{
            btn8 = [self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Copy Current Workout", nil)];
        }
        [btn8 addTarget:self action:@selector(copyWorkOut:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn8];
        
        UIButton *btn5 = [self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:NSLocalizedString(@"Edit Sheet Name", nil)];
        [btn5 addTarget:self action:@selector(editSheetName:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *btn10 = [self btnWithFrame:CGRectMake(15, 160, 225, 40) andTitle:NSLocalizedString(@"Edit Workout Name", nil)];
        [btn10 addTarget:self action:@selector(editWorkOutName:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *btn3 = [self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:NSLocalizedString(@"Clear Clipboard", nil)];
        [btn3 addTarget:self action:@selector(clearClipboard:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn3];
        
        
    }
   
    
    [self.view setClipsToBounds:TRUE];
    
}

-(void)copyWorkOut:(id)sender {
    [self.optionDelegate selectedOptionsTask:41 forEvent:kisProgram];
    
}
-(void)pastNewLine:(id)sender {
    [self.optionDelegate selectedOptionsTask:42 forEvent:kisProgram];
    
}
-(void)editWorkOutName:(id)sender {
    [self.optionDelegate selectedOptionsTask:43 forEvent:kisProgram];
    
}
//---------------------------------------------------------------------------------------------------------------

-(UIButton *)btnWithFrame:(CGRect)frame andTitle:(NSString *)title {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:frame];
    
    if (![title isEqualToString:@"Facebook Sharing"]) {
        btn.layer.cornerRadius = 8.0f;
        btn.layer.borderColor = kVewBGColor.CGColor;
        btn.layer.borderWidth = 1.0f;
        [btn.titleLabel setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:16]];
        [btn.titleLabel setTextColor:[UIColor whiteColor]];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setBackgroundColor:kVewBGColor];
    }
    return btn;
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)autoSaveSwitchValChanged:(id)sender {
   
    if([self.switchAutoSave isOn]) {
        [[AppDelegate sharedInstance]setIsAutoSave:TRUE];
        [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"AutoSave"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    } else {
        [[AppDelegate sharedInstance]setIsAutoSave:FALSE];
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"AutoSave"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : ParQ options 

//---------------------------------------------------------------------------------------------------------------

-(void)exportParQData:(id)sender {
    
    [self.optionDelegate selectedOptionsTask:2 forEvent:kisParq];
    
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark: Program options Events

-(void)CopyWorkoutData:(id)sender {

    [self.optionDelegate selectedOptionsTask:20 forEvent:kisProgram];
    
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)mailProgramSheetData:(id)sender {
    [self.optionDelegate selectedOptionsTask:11 forEvent:kisProgram];

}

//---------------------------------------------------------------------------------------------------------------

-(void)copyProgram:(id)sender {
    [self.optionDelegate selectedOptionsTask:12 forEvent:kisProgram];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)clearClipboard:(id)sender {
    countForOption = 0;
    [self.optionDelegate selectedOptionsTask:13 forEvent:kisProgram];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)pasteFromTemplate:(id)sender {
    [self.optionDelegate selectedOptionsTask:14 forEvent:kisProgram];
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)editWorkoutName:(id)sender
{
    [self.optionDelegate selectedOptionsTask:19 forEvent:kisProgram];

}

//---------------------------------------------------------------------------------------------------------------

-(void)editSheetName:(id)sender {
    [self.optionDelegate selectedOptionsTask:15 forEvent:kisProgram];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)deleteSheet:(id)sender {
    [self.optionDelegate selectedOptionsTask:16 forEvent:kisProgram];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)AddProgram:(id)sender {
    START_METHOD
    [self.optionDelegate selectedOptionsTask:10 forEvent:kisProgram];
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)copyAddNewLine:(id)sender {
    [self.optionDelegate selectedOptionsTask:17 forEvent:kisProgram];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)exportProgram:(id)sender {
    
    [self.optionDelegate selectedOptionsTask:18 forEvent:kisProgram];
    
}


//-----------------------------------------------------------------------

- (void) facebookShareClicked:(id) sender {
    
    [self.optionDelegate selectedOptionsTask:100 forEvent:kisProgram];
    
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark: Assessment options Events 

//---------------------------------------------------------------------------------------------------------------

//  3== save  , 4 == DontSave , 5 == Help

-(void)saveAssessmentData:(id)sender {
    
    [self.optionDelegate selectedOptionsTask:3 forEvent:kisAssessment];

}

//---------------------------------------------------------------------------------------------------------------

-(void)donotSaveAssessment:(id)sender {
    
    [self.optionDelegate selectedOptionsTask:4 forEvent:kisAssessment];
}

//---------------------------------------------------------------------------------------------------------------

-(void)assessmentHelp:(id)sender {
    
    [self.optionDelegate selectedOptionsTask:5 forEvent:kisAssessment];
}

//---------------------------------------------------------------------------------------------------------------

-(void)btnEditSheetClicked:(id)sender {
   [self.optionDelegate selectedOptionsTask:6 forEvent:kisAssessment];
}

//---------------------------------------------------------------------------------------------------------------

-(void)exportTheAssessment:(id)sender {
    
    [self.optionDelegate selectedOptionsTask:7 forEvent:kisAssessment];
    
}

//-----------------------------------------------------------------------

-(void)exportViewAllPrograms:(id)sender{
     [self.optionDelegate selectedOptionsTask:55 forEvent:kisViewAllPrograms];
}


//---------------------------------------------------------------------------------------------------------------

-(void)deleteAssessmentSheet:(id)sender {
    
    START_METHOD
    
    //Delete Assessment
//    [commonUtility alertMessage:@"Under Development!"];
//    return;
    
    [self.optionDelegate selectedOptionsTask:8 forEvent:kisAssessment];
    
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : ViewLifeCycle  Methods

//---------------------------------------------------------------------------------------------------------------


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 
}

//---------------------------------------------------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    START_METHOD
    [super viewWillAppear:animated];
    [self setupLayout];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
