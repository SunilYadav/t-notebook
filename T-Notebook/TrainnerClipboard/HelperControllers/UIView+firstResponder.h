//
//  UIView+firstResponder.h
//  Color Calc
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (firstResponder)

- (UIView *)findFirstResponder;

@end

