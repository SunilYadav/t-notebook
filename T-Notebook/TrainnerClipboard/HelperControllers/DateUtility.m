//
//  DateUtility.m
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//


#import "DateUtility.h"

@implementation DateUtility


#pragma mark -
#pragma mark DateUtility methods

//---------------------------------------------------------------------------------------------

+ (id)currentLocale {
	return [NSLocale currentLocale];
}

//---------------------------------------------------------------------------------------------

+ (NSString *)stringInFormat:(NSString *)format fromDate:(NSDate *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:format];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;

}

//---------------------------------------------------------------------------------------------

+ (NSDate *)dateInFormat:(NSString *)format fromString:(NSString *)dateString {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *formattedDate = [dateFormatter dateFromString:dateString];
	return formattedDate;
}

//---------------------------------------------------------------------------------------------

+ (NSDate *) dateBeforeDays:(int) days fromDate:(NSDate *) date {
    NSDate * expectedDate = nil;
    NSTimeInterval  timeInterval = 60*60*24*days;
    timeInterval = 0-timeInterval;
    expectedDate = [date dateByAddingTimeInterval:timeInterval];
    return expectedDate;
}

//---------------------------------------------------------------------------------------------

+ (NSDate *) dateAfterDays:(int) days fromDate:(NSDate *) date {
    NSDate * expectedDate = nil;
    NSTimeInterval  timeInterval = 60*60*24*days;
    expectedDate = [date dateByAddingTimeInterval:timeInterval];
    return expectedDate;
}

//---------------------------------------------------------------------------------------------

+ (NSInteger)monthsBetween:(NSDate *)fromDate toDate:(NSDate *)toDate {
	NSDateComponents *dateDifferenceComponents = [[NSCalendar currentCalendar]
												  components:NSMonthCalendarUnit
												  fromDate:fromDate
												  toDate:toDate
												  options:0];
	
	NSInteger monthDifference = [dateDifferenceComponents month];
	return monthDifference;
}

//---------------------------------------------------------------------------------------------


+ (NSInteger)daysBetween:(NSDate *)fromDate toDate:(NSDate *)toDate {
	NSDateComponents *dateDifferenceComponents = [[NSCalendar currentCalendar]components:NSDayCalendarUnit fromDate:fromDate toDate:toDate options:0];
	NSInteger dayDifference = [dateDifferenceComponents day];
	return dayDifference;
}

//---------------------------------------------------------------------------------------------

+ (NSInteger)hoursBetween:(NSDate *)fromDate toDate:(NSDate *)toDate {
	NSDateComponents *dateDifferenceComponents = [[NSCalendar currentCalendar]
												  components:NSHourCalendarUnit
												  fromDate:fromDate
												  toDate:toDate
												  options:0];
	
	NSInteger hourDifference = [dateDifferenceComponents hour];
	return hourDifference;
}

//---------------------------------------------------------------------------------------------


+ (NSInteger)minutesBetween:(NSDate *)fromDate toDate:(NSDate *)toDate {
	NSDateComponents *dateDifferenceComponents = [[NSCalendar currentCalendar]
												  components:NSMinuteCalendarUnit
												  fromDate:fromDate
												  toDate:toDate
												  options:0];
	
	NSInteger minuteDifference = [dateDifferenceComponents minute];
	return minuteDifference;
}

//---------------------------------------------------------------------------------------------

+ (NSString *)yearFromDate:(NSDate *) date {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy"];
    NSString * yearString = [dateFormatter stringFromDate:date];
	return yearString;
}

//---------------------------------------------------------------------------------------------

+ (NSString *)monthFromDate:(NSDate *) date {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMMM"];
    NSString * monthString = [dateFormatter stringFromDate:date];
	return monthString;
}

//---------------------------------------------------------------------------------------------

+ (NSString *)dayFromDate:(NSDate *) date {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd"];
    NSString * dayString = [dateFormatter stringFromDate:date];
	return dayString;
}

//---------------------------------------------------------------------------------------------

+ (NSInteger) daysInMonth:(NSDate *) date {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
	[comp setDay:0];
	[comp setMonth:comp.month+1];
	
	NSInteger days = [[gregorian components:NSDayCalendarUnit fromDate:[gregorian dateFromComponents:comp]] day];
	
	return days;
}

//---------------------------------------------------------------------------------------------

+ (NSInteger)weekdayForDate:(NSDate *) date {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
	NSInteger weekday = [comps weekday];
	
	return weekday;
}


//---------------------------------------------------------------------------------------------

+ (NSInteger)weekdayWithMondayFirstForDate:(NSDate *) date {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
	NSInteger weekday = [comps weekday];
	
	CFCalendarRef currentCalendar = CFCalendarCopyCurrent();
    
    weekday -= 1;
    if (weekday == 0) {
        weekday = 7;
    }
    

	CFRelease(currentCalendar);
	
	return weekday;
}


//---------------------------------------------------------------------------------------------

+ (NSString *) timeInFormat:(NSString *) format forDate:(NSDate *) date {
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setLocale:[DateUtility currentLocale]];
	[outputFormatter setDateFormat:format];
	NSString* dateString = [outputFormatter stringFromDate:date];
	return dateString;
}

//---------------------------------------------------------------------------------------------

+ (NSDate*)firstDateOfCurrentMonth {
	NSDate *day = [NSDate date];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:day];
	[comp setDay:1];
    [comp setMonth:comp.month];
	return [gregorian dateFromComponents:comp];
}

//---------------------------------------------------------------------------------------------

+ (NSDate*)lastDateOfCurrentMonth {
	NSDate *day = [NSDate date];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:day];
	[comp setDay:0];
	[comp setMonth:comp.month+1];
    
	return [gregorian dateFromComponents:comp];
}

//---------------------------------------------------------------------------------------------

+ (NSString *)timelessDateFromDate:(NSDate *) date {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    date = [gregorian dateFromComponents:comp];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
	return [formatter stringFromDate:date];
}

//---------------------------------------------------------------------------------------------

+ (NSDate *) firstDateOfNextMonthForDate:(NSDate *) date {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
	[comp setDay:1];
	[comp setMonth:comp.month+1];
    
	return [gregorian dateFromComponents:comp];
}

//---------------------------------------------------------------------------------------------

+ (NSDate *) firstDateOfPrevMonthForDate:(NSDate *) date {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
	[comp setDay:1];
	[comp setMonth:comp.month-1];
    
	return [gregorian dateFromComponents:comp];
}

//---------------------------------------------------------------------------------------------


+ (BOOL)isToday:(NSDate *) date {
    NSCalendar* calendar = [NSCalendar currentCalendar];
	NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
	NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
    
	return ([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day]);
}

//---------------------------------------------------------------------------------------------


@end
