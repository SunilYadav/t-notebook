    //
//  DatePickerController.m
//  PatientTracker
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "DatePickerController.h"

@implementation DatePickerController

@synthesize datepicker,delegate, date1, tag1,flag,dateString,dateflag;
@synthesize isFromParQ;

#pragma mark -
#pragma mark view lifecycle methods

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done:)];

	if (isFromParQ) {
	}
	else {

	}	
		
	NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
	[tmpdt setDateFormat:kAppDateFormat];
	NSDate * bDate = [tmpdt dateFromString:self.dateString];
	
	if (bDate != nil) {
		[self.datepicker setDate:bDate];
	}
	else {
		[self.datepicker setDate:[NSDate date]];
	}	
}

//--------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark custom methods

- (CGSize)contentSizeForViewInPopoverView {
    return CGSizeMake(300, 216);
}

//--------------------------------------------------------------------------------------------------

-(IBAction) getDate: (id)sender {
	@try {
		NSDate * dt = datepicker.date;
		NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
		[dateformatter setDateFormat:kAppDateFormat];
		date1=[dateformatter stringFromDate:dt];
		[delegate getDateFormPicker :date1:tag1];		
	}
	@catch (NSException * e) {
		
	}
	@finally {
		
	}	
}

//--------------------------------------------------------------------------------------------------

-(void)done:(id) sender {
	@try {
		[delegate doneWithDate];		
	}
	@catch (NSException * e) {
		
	}
	@finally {
		
	}
}

//--------------------------------------------------------------------------------------------------

-(void)currentDate: (id) sender {
	@try {
		NSDate * today= [NSDate date];
		datepicker.date=today;
		NSDate * dt = datepicker.date;
		NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
		[dateformatter setDateFormat:kAppDateFormat];
		
		date1=[dateformatter stringFromDate:dt];
		
		[delegate getDateFormPicker:date1 :tag1];
	}
	@catch (NSException * e) {
		
	}
	@finally {
		
	}
	
}

//--------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark interfaceOrientation methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

#if defined(__IPHONE_6_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0

-(BOOL)shouldAutorotate {
    return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

-(NSUInteger)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskAll;
}

#endif

//-----------------------------------------------------------------------------------------------------------------------------


#pragma mark -
#pragma mark memory management methods


//--------------------------------------------------------------------------------------------------

@end
