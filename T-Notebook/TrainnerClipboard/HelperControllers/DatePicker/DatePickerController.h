//
//  DatePickerController.h
//  PatientTracker
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerDelegate

@optional
-(void) doneWithDate;
-(void) getDateFormPicker:(NSString *)date1 :(int)tag1;
-(void) closeDatePopover;

@end

@interface DatePickerController : BaseViewController {
	id<DatePickerDelegate>				__unsafe_unretained delegate;
	IBOutlet UIDatePicker				* datepicker;
	NSString							* date1;
	int									tag1;
	BOOL								flag;
	NSString							* dateString;
	int									dateflag;
	BOOL								isFromParQ;
}
@property (nonatomic) BOOL		isFromParQ;
@property (nonatomic, unsafe_unretained) id<DatePickerDelegate>		delegate;
@property(nonatomic,strong)IBOutlet UIDatePicker			* datepicker;
@property(nonatomic,strong) NSString						* date1;
@property(nonatomic) int									tag1;
@property(nonatomic) BOOL									flag;
@property(nonatomic,strong)NSString							* dateString;
@property(nonatomic)int										dateflag;

-(IBAction) getDate: (id)sender;

@end
