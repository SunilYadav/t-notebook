//
//  DateUtility.h
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//


#import <Foundation/Foundation.h>

@interface DateUtility : NSObject

+ (NSString *)stringInFormat:(NSString *)format fromDate:(NSDate *)date;
+ (NSDate *)dateInFormat:(NSString *)format fromString:(NSString *)dateString;

+ (NSDate *) dateBeforeDays:(int) days fromDate:(NSDate *) date;
+ (NSDate *) dateAfterDays:(int) days fromDate:(NSDate *) date;

+ (NSInteger)monthsBetween:(NSDate *)fromDate toDate:(NSDate *)toDate;
+ (NSInteger)daysBetween:(NSDate *)fromDate toDate:(NSDate *)toDate ;
+ (NSInteger)hoursBetween:(NSDate *)fromDate toDate:(NSDate *)toDate;
+ (NSInteger)minutesBetween:(NSDate *)fromDate toDate:(NSDate *)toDate;

+ (NSString *)yearFromDate:(NSDate *) date;
+ (NSString *)monthFromDate:(NSDate *) date;
+ (NSString *)dayFromDate:(NSDate *) date;
+ (NSInteger) daysInMonth:(NSDate *) date;
+ (NSInteger)weekdayForDate:(NSDate *) date;
+ (NSInteger)weekdayWithMondayFirstForDate:(NSDate *) date;

+ (NSString *) timeInFormat:(NSString *) format forDate:(NSDate *) date;
+ (NSDate *)firstDateOfCurrentMonth;
+ (NSDate *)lastDateOfCurrentMonth;

+ (NSDate *)timelessDateFromDate:(NSDate *) date;
+ (NSDate *) firstDateOfNextMonthForDate:(NSDate *) date;
+ (NSDate *) firstDateOfPrevMonthForDate:(NSDate *) date;

+ (BOOL)isToday:(NSDate *) date;
@end
