//
//  OptionsViewVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OptionsDelegate

-(void)selectedOptionsTask:(NSInteger)selTask forEvent:(NSString*)evnt;

@end



@interface OptionsViewVC : UIViewController {
    
}

@property (nonatomic , strong) NSString                       *typeOfOptions;
@property (nonatomic , strong) NSString                       *strClientId;
@property (nonatomic , strong) NSString                       *currentSheetId;
@property (nonatomic , strong) NSString                       *currentBlockId;
@property (nonatomic) NSInteger                               blockSheetNo;


@property (nonatomic , strong) id <OptionsDelegate>   optionDelegate;

-(id)initWIthFrame:(CGRect)frame andcontroller:(NSString *)Cntroller;




@end
