//
//  ClientViewController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Add par-q , program , assesment , notes in client view.



#import "ClientViewController.h"
#import "TimerViewController.h"
#import "PurchaseVC.h"
#import "ViewAllProgramVC.h"



#define kSelectedBtnBGColor [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]

#define kNotSelectedBtnBGColor [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1.0]

#define kSelectedBtnBGColorFirstSkin [UIColor colorWithRed:0/255.0 green:200.0/255.0 blue:0/255.0 alpha:1.0]

#define kNotSelectedBtnBGColorFirstSkin [UIColor colorWithRed:63.0/255.0 green:72.0/255.0 blue:77.0/255.0 alpha:1.0]

static ClientViewController *sharedInstance = nil;


@interface ClientViewController ()
{
    NotesViewController       *notesVCOBJ;
    ProgramViewController   *programVCOBJ;
    ViewAllProgramVC         *viewAllProgOBJ;
    ParQController                *parqVCOBJ;
    NSInteger                       selectedScrrenID;
    NSDictionary                  *dictNotesClient;
     int                            _selectedTabTag ;//For save par q
    int tagSelectedTab ;
}

@property (weak, nonatomic) IBOutlet UIButton           *btnHome;
@property (strong, nonatomic) IBOutlet UIImageView       *backgroundImgv;
@property (nonatomic , weak) IBOutlet  UIButton          *btnOptions;
@property (nonatomic , weak) IBOutlet  UIButton          *btnShowTimer;
@property (nonatomic , weak) IBOutlet  UILabel           *lblTitleTraining;
@property (nonatomic , weak) IBOutlet  UILabel           *lblTitleNotebook;
@property (nonatomic , weak) IBOutlet  UIImageView       *imgTopNavigation;


@property (nonatomic , weak) IBOutlet  UIButton          *btnParq;
@property (nonatomic , weak) IBOutlet  UIButton          *btnProgram;
@property (nonatomic , weak) IBOutlet  UIButton          *btnAssessment;
@property (nonatomic , weak) IBOutlet  UIButton          *btnViewAllNote;
@property (nonatomic , weak) IBOutlet  UIImageView       *seprator1;
@property (nonatomic , weak) IBOutlet  UIImageView       *seprator2;
@property (nonatomic , weak) IBOutlet  UIImageView       *seprator3;

@property (nonatomic , strong)   OptionsViewVC                                   *optionsVCOBJ;
@property (nonatomic , strong) UIImagePickerController                           *imgPicker;
@property (nonatomic , strong) UIPopoverController                               *pop;

@property (nonatomic) BOOL                              isLandscapeMode;


@end

@implementation ClientViewController
@synthesize selectedScreen;
@synthesize isOtherOptionsSelected;

//static UINavigationController *ClientnavigationController=nil;
static  UINavigationController *objNavcAssessmentVC = nil;


+ (ClientViewController *)sharedInstance {
    
    if (sharedInstance == nil) {
        sharedInstance = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ClientView"];
    }

    
    return sharedInstance;
}
//---------------------------------------------------------------------------------------------------------------

#pragma mark - IBAction Methods

//---------------------------------------------------------------------------------------------------------------

-(IBAction)showTimerBtnTapped:(id)sender {
    
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        DisplayAlertWithYesNo(kliteMsg, kAppName, self);
        return;
    }
    
    TimerViewController *timerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TimerViewController"];
    [self.navigationController pushViewController:timerVC animated:YES];
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)optionsButtonTapped:(id)sender {
    
    START_METHOD
    if(self.optionsVCOBJ) {
        
        [self hideAnnimatingOptionView];
        
    } else {
        
        if ([self.selectedScreen isEqualToString:kisViewAllPrograms]) {
            
            _optionsVCOBJ = [[OptionsViewVC alloc]initWIthFrame:CGRectMake(self.view.frame.size.width - 250, 75, 250,60) andcontroller:kisViewAllPrograms];
            self.optionsVCOBJ.typeOfOptions = kisViewAllPrograms;
            self.optionsVCOBJ.optionDelegate = self;
            [self.view addSubview:self.optionsVCOBJ.view];
            [self openAnimatingOptionView];
        }
        
        if([self.selectedScreen isEqualToString:kisParq]) {
            
            if([self.strClientId isEqual:@"0"]) {
                return;
            }
            
            _optionsVCOBJ = [[OptionsViewVC alloc]initWIthFrame:CGRectMake(self.view.frame.size.width - 250, 75, 250, 60) andcontroller:kisParq];
            self.optionsVCOBJ.typeOfOptions = kisParq;
            self.optionsVCOBJ.optionDelegate = self;
            [self.view addSubview:self.optionsVCOBJ.view];
            [self openAnimatingOptionView];
            
        }
        
        if ([self.selectedScreen isEqualToString:kisProgram]) {
            
            int height = 460-150;
            if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]){
                height = 370 ;
            }
            else{
                height = 410;
            }
            _optionsVCOBJ = [[OptionsViewVC alloc]initWIthFrame:CGRectMake(self.view.frame.size.width - 250, 75, 250, height) andcontroller:kisProgram];
            self.optionsVCOBJ.strClientId = programVCOBJ.strClientId;
            self.optionsVCOBJ.blockSheetNo = programVCOBJ.blockSheetNo;
            self.optionsVCOBJ.currentSheetId = [NSString stringWithFormat:@"%ld",(long)programVCOBJ.currentSheetId] ;
            self.optionsVCOBJ.typeOfOptions = kisProgram;
            self.optionsVCOBJ.optionDelegate = self;
            [self.view addSubview:self.optionsVCOBJ.view];
            [self openAnimatingOptionView];
            
        }
        
        if ([self.selectedScreen isEqualToString:kisAssessment]) {
            _optionsVCOBJ = [[OptionsViewVC alloc]initWIthFrame:CGRectMake(self.view.frame.size.width - 250, 75, 250,210) andcontroller:kisAssessment];
            self.optionsVCOBJ.typeOfOptions = kisAssessment;
            self.optionsVCOBJ.optionDelegate = self;
            [self.view addSubview:self.optionsVCOBJ.view];
            [self openAnimatingOptionView];
        }
        
    }
    
}

//---------------------------------------------------------------------------------------------------------------


- (IBAction)tabBarButtonAction:(id)sender {
    START_METHOD
    
    MESSAGE(@"VselectedScrrenID-> %ld",(long)selectedScrrenID);
    
    if ([[commonUtility retrieveValue:KEY_LOGIN_STATUS] isEqualToString:@"NO"]){
        [self logOut];
    }
    
    //For Save Par-Q DATA
    if (_selectedTabTag==101){
        //TODO: Save par-Q data on switch view
        
        //Invoke method for save par-Q data
        [parqVCOBJ saveParqViewData];
        
        //For after some time
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            //Invoke for action
            [self actionOnButton:sender];
            
        });
    
        
    }else{
        //Invoke for action
        [self actionOnButton:sender];

    }
    
    UIButton *btn=(UIButton *)sender;
    _selectedTabTag =   (int)btn.tag;;
  
}

-(void)actionOnButton:(id)sender{
    
    START_METHOD
    
    UIButton *btn=(UIButton *)sender;
    tagSelectedTab  = (int)btn.tag;
    MESSAGE(@"Button tyag : %ld",(long)btn.tag);
    
    
    int count = 1;
    [self hideOptionsViewOnTap];
    
    self.isOtherOptionsSelected = true;
    
    for(UIView *subview in [self.backgroundAddView subviews]) {
        [subview removeFromSuperview];
    }
    
    for (int i = 101; i<=104; i++) {
        UIButton *btn = (UIButton *) sender;
        NSInteger selTag = [btn tag];
        
        UIButton *tabBtn = (UIButton *) [self.view viewWithTag:i];
        if(tabBtn.tag == selTag) {
            //Sunil
            //            [tabBtn setBackgroundColor:kSelectedBtnBGColor];
            //Invoke method for set back ground color
            [self setSelectedButtonColor:tabBtn];
            
        } else {
            //sunil
            //[tabBtn setBackgroundColor:kNotSelectedBtnBGColor];
            [self setUnSelectedButtonColor:tabBtn];
        }
        
    }
    
    //==================
    
    if ([sender tag]==101) {
        
        selectedScrrenID = 1;
        //ParQ
        [self.btnOptions setHidden:FALSE];
        self.isDefaultVcChange = NO;
        self.selectedScreen = kisParq;
        
        if(parqVCOBJ) {
            
            //REmove from superview
            [parqVCOBJ.view removeFromSuperview];
        }
        
        
        if(parqVCOBJ == nil) {
            
            parqVCOBJ  = (ParQController *) [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ParQVC"];
        }
        parqVCOBJ.strClientId = self.strClientId;
        parqVCOBJ.parqDelegate = self;
        
        
        [self.backgroundAddView addSubview:parqVCOBJ.view];
        
        
        //Sunil ParQ-Background"
        
        //[self.backgroundAddView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ParQ-Background"]]];
        
        
        [self.backgroundAddView clipsToBounds];
        
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)){
            
            
            CGRect backgroundRect = self.backgroundAddView.frame;
            backgroundRect.size.height = kLandscapeHeight - 170;
            [self.backgroundAddView setFrame:backgroundRect];
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            
            
            [parqVCOBJ setUpLandscapeOrientation];
            
            //For set view
            [self actionChnageSkin: LANDSCAPE];
            
            
        } else {
            CGRect backgroundRect = self.backgroundAddView.frame; // h =598
            backgroundRect.size.height = 848; // 678
            [self.backgroundAddView setFrame:backgroundRect];
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            
            
            [parqVCOBJ setupPortraitOrientation];
            
            //For set view
            [self actionChnageSkin: PORTRAIT];
            
        }
        
    } else if ([sender tag]==102) {
        selectedScrrenID = 2;
        [self.btnOptions setHidden:FALSE];
        
        self.isDefaultVcChange = YES;
        self.selectedScreen = kisProgram;
        count = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
        if (count==0) {
            
            ProgramNameViewController *ProgramNameVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getProgram"];
            UINavigationController*  ClientnavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramNameVCOBJ];
            [ClientnavigationController.navigationBar setTranslucent:NO];
            ProgramNameVCOBJ.delegate = self;
            ClientnavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
            ClientnavigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            if (iOS_8) {
                CGPoint frameSize = CGPointMake(500, 250);
                ClientnavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
            }
            [self presentViewController:ClientnavigationController animated:YES completion:nil];
        }
        else
        {
            
            programVCOBJ = [ProgramViewController sharedInstance];
            programVCOBJ.strClientId = self.strClientId;
            programVCOBJ.strClientName = self.strClientName;
            
            
            CGRect backgroundRect = self.backgroundAddView.frame;
            backgroundRect.size.height = 678;
            
            [self.backgroundAddView setFrame:backgroundRect]; //programBack_Landscape
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromNotes"])
            {
                programVCOBJ.selectedSheet = [dictNotesClient valueForKey:@"nSheetID"];
                programVCOBJ.blockSheetNo = [[dictNotesClient valueForKey:@"nBlockNo"] integerValue];
                
            }
            
            programVCOBJ.delegate = self;
            
            if (programVCOBJ.isViewAllVisible) {
                viewAllProgOBJ = [ViewAllProgramVC sharedInstance];
                if (![self.backgroundAddView.subviews containsObject:viewAllProgOBJ.view]) {
                    [self.backgroundAddView addSubview:viewAllProgOBJ.view];
                }
                [viewAllProgOBJ.view setHidden:NO];
                programVCOBJ.isViewAllVisible = YES;
            }
            else{
                
                
                [programVCOBJ.view setHidden:NO];
                [self.backgroundAddView addSubview:programVCOBJ.view];
                programVCOBJ.isViewAllVisible = NO;
            }
            //[self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
            
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
                [self setUpLandscapeLayout];
                [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
                
                
                //Invoke methos for chnage skin
                [self actionChnageSkin:LANDSCAPE];
            }
            else{
                [self setUpPortraitLayout];
                [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
                
                //Invoke methos for chnage skin
                [self actionChnageSkin:PORTRAIT];
            }
        }
        
    } else if ([sender tag]==103){
        selectedScrrenID = 3;
        [self.btnOptions setHidden:FALSE];
        self.isDefaultVcChange = YES;
        self.selectedScreen = kisAssessment;
        
        if(parqVCOBJ) {
            [parqVCOBJ.view removeFromSuperview];
            parqVCOBJ =  nil;
        }
        
        // Assesment
        if (objNavcAssessmentVC == nil) {
            AssessmentViewController* objAssessmentVieController =  [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AssessmentView"];
            objNavcAssessmentVC = [[UINavigationController alloc]initWithRootViewController:objAssessmentVieController];
            objNavcAssessmentVC.navigationBarHidden = true;
        }
        AssessmentViewController *objAssesmetsVC = (AssessmentViewController*)(objNavcAssessmentVC.viewControllers[0]);
        objAssesmetsVC.assmntDelegate = self;
        objAssesmetsVC.strClientID = self.strClientId;
        objAssesmetsVC.currentSheet = 1;
        CGRect rect = objNavcAssessmentVC.view.frame;
        
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            // setup Landscape orientation
            objAssesmetsVC.isLandscapeSet = TRUE;
            rect.size.width = kLandscapeWidth;
            [objAssesmetsVC setupLandscapeOrientation];
            
            CGRect backgroundRect = self.backgroundAddView.frame; // h =598
            backgroundRect.size.height = 678;
            [self.backgroundAddView setFrame:backgroundRect];
            
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
        } else {
            
            rect.size.width = kPotraitWidth;
            objAssesmetsVC.isLandscapeSet = FALSE;
            [objAssesmetsVC setupPortraitOrientation];
            
            
            CGRect backgroundRect = self.backgroundAddView.frame; // h =598
            backgroundRect.size.height = 878; // 678
            [self.backgroundAddView setFrame:backgroundRect];
            
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
        }
        
        
        rect.origin.x = -18;
        objNavcAssessmentVC.view.frame = rect;
        
        [self.backgroundAddView addSubview:objNavcAssessmentVC.view];
        
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
        
        
        
    }
    else
    {
        //        [self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
        self.isDefaultVcChange = YES;
        //Notes
        selectedScrrenID = 4;
        [self.btnOptions setHidden:TRUE];
        self.selectedScreen = kisNotes;
        
        if (notesVCOBJ == nil) {
            UIStoryboard *storyboard =  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            notesVCOBJ =  (NotesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"notes"];
        }
        notesVCOBJ.delegate = (id)self;
        notesVCOBJ.strClientId = self.strClientId;
        notesVCOBJ.strClientName = self.strClientName;
        [self.backgroundAddView addSubview:notesVCOBJ.view];
        
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            
            CGRect backgroundRect = self.backgroundAddView.frame;
            backgroundRect.size.height = kLandscapeHeight - 170;
            [self.backgroundAddView setFrame:backgroundRect];
            //   [self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            [notesVCOBJ setLandscapeOrientation];
            
            //For set view
            [self actionChnageSkin: LANDSCAPE];
        } else {
            
            CGRect backgroundRect = self.backgroundAddView.frame; // h =598
            backgroundRect.size.height = 878; // 678
            [self.backgroundAddView setFrame:backgroundRect];
            //     [self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            [notesVCOBJ setportraitOrientation];
            
            //For set view
            [self actionChnageSkin: PORTRAIT];
        }
        
        
    }
    
    
    
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        
        //For set view
        [self actionChnageSkin: LANDSCAPE];
    }else{
        //For set view
        [self actionChnageSkin: PORTRAIT];
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)NavButtonAction:(id)sender {
    
    START_METHOD
    self.isOtherOptionsSelected = false;
    
    if ([self.strClientId isEqualToString:@"0"]) {
        
        if(parqVCOBJ) {
            
            MESSAGE(@"if(parqVCOBJ) {");
            [parqVCOBJ checkIfAnyOftheDataIsPresent];
        }
        
        if(self.isDataToBeEntered) {
            DisplayAlertWithTitle(NSLocalizedString(@"Please fill in all required fields.", nil), kAppName);
            return;
        }
        
        if(self.isDataToBeSaved) {
            DisplayAlertWithTitle(NSLocalizedString(@"Save Par-Q to continue.", nil), kAppName);
            return;
        }
        
    }
    
    if ([sender tag]==201) {
        //Home Button
        
        if(objNavcAssessmentVC != nil) {
            if([selectedScreen isEqualToString:kisAssessment]) {
                AssessmentViewController *objAssesmetsVC = (AssessmentViewController*)(objNavcAssessmentVC.viewControllers[0]);
                if([objAssesmetsVC isKindOfClass:[AssessmentViewController class]]) {
                    NSInteger sheetInfo = objAssesmetsVC.currentSheet;
                    NSInteger currentPage = objAssesmetsVC.currentPage;
                    NSString *strSheetAndPage = [NSString stringWithFormat:@"%ld,%ld",(long)currentPage,(long)sheetInfo];
                    [self updateTheLastEditedPageWithDetail:strSheetAndPage ForClient:self.strClientId];
                    [objAssesmetsVC changeTheButtonTitles];
                    
                    
                }
            }
            
        }
        
        if(programVCOBJ != nil) {
            if([selectedScreen isEqualToString:kisProgram]) {
                NSInteger curntSheet = programVCOBJ.currentSheet;
                NSInteger currentBlock = programVCOBJ.currentBlockNo;
                NSString *strsheetAndeBlock = [NSString stringWithFormat:@"%ld,%ld",(long)curntSheet,(long)currentBlock];
                [self updateTheLastEditedPageWithDetail:strsheetAndeBlock ForClient:self.strClientId];
            }
        }
        
        if(parqVCOBJ) {
            
            if(self.strClientId.length<1 || [self.strClientId isEqualToString:@"0"]){
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                
                MESSAGE(@"save if(parqVCOBJ) {");
                
                
                //TODO: SUNIL SAVE PAR- Q data by click on home
                //Invoke to save par-Q data > 17 Oct for check Condition
              BOOL boolStatus =   [parqVCOBJ saveParqViewData];
                
                if(boolStatus){
                //For after some time
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    
                    [self updateClientSelectedView];
                    [self.navigationController popViewControllerAnimated:YES];
                });
                }
                
            }
            
            
            
        }else{
        
        [self updateClientSelectedView];
        [self.navigationController popViewControllerAnimated:YES];

        }
        
    }
}

//-----------------------------------------------------------------------

#pragma mark - NotesView DelegateMethods

//-----------------------------------------------------------------------

-(void)cancelProgramAndGoToParq {
    
    [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:101]];
}

//-----------------------------------------------------------------------

-(void)setProgrameWorkoutModuleForNotes:(NSDictionary*)dictNotes{
    
    dictNotesClient =[NSDictionary dictionaryWithDictionary:dictNotes];
    UIButton *btnPrograme = (UIButton*)[self.view viewWithTag:102];
    [self tabBarButtonAction:btnPrograme];
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void) openAnimatingOptionView {
    
    CGRect actualFrame = self.optionsVCOBJ.view.frame;
    CGRect modifiedFrame = actualFrame;
    modifiedFrame.origin.y =actualFrame.origin.y ;
    modifiedFrame.size.height = 0;
    self.optionsVCOBJ.view.alpha = 0.0;
    self.optionsVCOBJ.view.frame = modifiedFrame;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.optionsVCOBJ.view.frame = actualFrame;
        self.optionsVCOBJ.view.alpha = 0.7;
    } completion:^(BOOL finished) {
        self.optionsVCOBJ.view.alpha = 1.0;
        
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)hideAnnimatingOptionView {
    
    CGRect actualFrame = self.optionsVCOBJ.view.frame;
    [UIView animateWithDuration:0.5 animations:^{
        
        CGRect modifiedFrame = actualFrame;
        modifiedFrame.origin.y =actualFrame.origin.y ;
        modifiedFrame.size.height = 0;
        self.optionsVCOBJ.view.frame = modifiedFrame;
        self.optionsVCOBJ.view.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        [self.optionsVCOBJ.view removeFromSuperview];
        self.optionsVCOBJ = nil;
        
    }];
    
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-(void) addNewProgram:(NSString *)ProgramName : (NSString *)ProgramPurpose : (NSString *)workoutName
{
    START_METHOD
    
    if ([ProgramName length]!=0) {
        
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
        MESSAGE(@"Add clinet's first program done");
        
        //Carete Dictioanry for Add forst program
        NSMutableDictionary *dictProgram    =   [[NSMutableDictionary alloc]init];
        [dictProgram setValue:ProgramName forKey:ksheetName];
        [dictProgram setValue:ProgramPurpose forKey:ksProgramPurpose];
        [dictProgram setValue:ProgramPurpose forKey:@"goal_name"];
        [dictProgram setValue:self.strClientId forKey:kClientId];
        [dictProgram setValue:workoutName forKey:kBlockTitle];
        
        
        
        //Invoke method for add client's first Program
        [self postRequestForClientFirstProgram: dictProgram];
        }else{
            
            MESSAGE(@"Add clinet's first program");
            
            programVCOBJ = [ProgramViewController sharedInstance];
            int i = [[DataManager initDB] addNewSheet:ProgramName ForClient:self.strClientId ProgramPurpose:ProgramPurpose withWorkoutName:workoutName ForBlockNo:@"1"];
            
            if(i==0) {
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                }];
                
                programVCOBJ.strClientId = self.strClientId;
                programVCOBJ.strClientName = self.strClientName;
                programVCOBJ.selectedSheet = @"1";
                programVCOBJ.No_OfSheet = i;
                programVCOBJ.delegate = self;
                programVCOBJ.strProgramSheetName = ProgramName;
                
                [self.backgroundAddView addSubview:programVCOBJ.view];
                [self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
                
                UIButton *selectedBtn = (UIButton *)[self.view viewWithTag:102];
                //            [selectedBtn setBackgroundColor:kSelectedBtnBGColor];
                //Invoke method for set back ground color
                [self setSelectedButtonColor:selectedBtn];
                
                
                MESSAGE(@"Add clinet's first program done");
                
            } else {
                
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                }];
                [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:101]];
                DisplayAlertWithTitle(NSLocalizedString(@"There seems to be an issue with program creation. Please try again in sometime.", nil), kAppName);
            }
            
        }
        
    }else{
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter Sheet Name", nil),kAppName);
    }

}

//---------------------------------------------------------------------------------------------------------------

-(void) addNewSheet:(NSString *)WorkoutSheet : (NSString *)WorkoutPurpose
{
}

-(void)openMail:(NSString *)mailID
{
    [self sendMailWithAttachment:FALSE forType:kisProgram];
    
}

//-----------------------------------------------------------------------

-(void)hideOptionsViewOnTap{
    
    if (self.optionsVCOBJ) {
        CGRect actualFrame = self.optionsVCOBJ.view.frame;
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect modifiedFrame = actualFrame;
            modifiedFrame.origin.y =actualFrame.origin.y ;
            modifiedFrame.size.height = 0;
            self.optionsVCOBJ.view.frame = modifiedFrame;
            self.optionsVCOBJ.view.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            [self.optionsVCOBJ.view removeFromSuperview];
            self.optionsVCOBJ = nil;
            
        }];
    }
    
}

//-----------------------------------------------------------------------

-(void)viewAllDelegate:(NSString*)clientID
{
    START_METHOD
    viewAllProgOBJ  = [ViewAllProgramVC sharedInstance];
    viewAllProgOBJ.strClientId = clientID;
    [viewAllProgOBJ.view setHidden:NO];
    viewAllProgOBJ.delegate = (id)self;
    self.selectedScreen = kisViewAllPrograms;
    self.backgroundAddView.backgroundColor = [UIColor clearColor];
    viewAllProgOBJ.view.backgroundColor = [UIColor clearColor]; ;
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [self setUpLandscapeLayout];
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
    } else {
        [self setUpPortraitLayout];
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    }
    
    
    if ([self.backgroundAddView.subviews containsObject:viewAllProgOBJ.view]) {
        [viewAllProgOBJ reloadViewAllTable];
        // DLOG(@"contains");
    } else{
        // DLOG(@"not contains");
        [self.backgroundAddView addSubview:viewAllProgOBJ.view];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50;
    transition.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromRight;
    [viewAllProgOBJ.view.layer addAnimation:transition forKey:@"ViewAllAnimation"];
    
    
}

//---------------------------------------------------------------------------------------------------------------
-(void)changeOptionScreen {
    
    self.selectedScreen = kisProgram;
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)defaultSelectedViewController
{
    if(self.isFromClientTable) {
        parqVCOBJ.strClientId = self.strClientId;
        [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:101]];
    } else {
        parqVCOBJ.strClientId = @"0";
        [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:101]];
    }
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)setRoundRectForView :(UIView *) view withCorner :(UIRectCorner )rectCorner {
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                     byRoundingCorners:rectCorner
                                           cornerRadii:CGSizeMake(8.0,8.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    view.layer.masksToBounds = YES;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)btnDeleteSheet_Click
{
    
    [programVCOBJ deleteSheet];
    int count = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
    if (count<=0) {
        UIButton *button = (UIButton *)[self.view viewWithTag:101];
        [self tabBarButtonAction:button];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)updateToFullVersion {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


//---------------------------------------------------------------------------------------------------------------

-(void)sendMailWithAttachment:(BOOL)isWithAttachment forType:(NSString *)screenType {
    
    
    NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientId];
    
    NSString *clientEmail = [clientInfo objectForKey:kEmail1];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController * mailComposer = [[MFMailComposeViewController alloc] init];
        NSArray *recep = [NSArray arrayWithObject:clientEmail];
        [mailComposer setToRecipients:recep];
        
        if(isWithAttachment) {
            
            if([screenType isEqualToString:kisParq]) {
                [mailComposer setSubject:@"Client Details"];
                
                NSString * imgPath = [NSString stringWithFormat:@"%@/ParQData/ParQData%@.png",[FileUtility basePath],self.strClientId];
                UIImage *imageToAttacg = [UIImage imageWithContentsOfFile:imgPath];
                if(!imageToAttacg) {
                }
                NSData *imgData = UIImagePNGRepresentation(imageToAttacg);
                [mailComposer addAttachmentData:imgData mimeType:@"image/png" fileName:@"ClientDetail.png"];
            }
            
            if([screenType isEqualToString:kisProgram]) {
                [mailComposer setSubject:@"Program Details"];
                NSString * imgPath = [NSString stringWithFormat:@"%@/ProgramData/ProgramData%@.png",[FileUtility basePath],self.strClientId];
                UIImage *imageToAttacg = [UIImage imageWithContentsOfFile:imgPath];
                if(!imageToAttacg) {

                }
                NSData *imgData = UIImagePNGRepresentation(imageToAttacg);
                [mailComposer addAttachmentData:imgData mimeType:@"image/png" fileName:@"ProgramData.png"];
            }
            
            if([screenType isEqualToString:kisAssessment]) {
                [mailComposer setSubject:@"Assessment Details"];
                NSString * imgPath = [NSString stringWithFormat:@"%@/AssessmentData/AssessmentData%@.png",[FileUtility basePath],self.strClientId];
                UIImage *imageToAttacg = [UIImage imageWithContentsOfFile:imgPath];
                if(!imageToAttacg) {

                }
                NSData *imgData = UIImagePNGRepresentation(imageToAttacg);
                [mailComposer addAttachmentData:imgData mimeType:@"image/png" fileName:@"AssessmentData.png"];
            }
        }
        
        if (IOS7) {
            [mailComposer.navigationBar setTintColor:[UIColor grayColor]];
        }
        [mailComposer setMessageBody:@"" isHTML:YES];
        [mailComposer setMailComposeDelegate:self];
        [self.navigationController presentViewController:mailComposer animated:YES completion:^{
        }];
        
    } else {
        DisplayAlertWithTitle(NSLocalizedString(@"Please Configure Email", nil), kAppName) ;
        return;
    }
    
}

//---------------------------------------------------------------------------------------------------------------
-(void)removePopOverForAddProgram {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------------
- (void) localizeControls {
    
   [self.btnHome setTitle:NSLocalizedString(self.btnHome.titleLabel.text, @"") forState:UIControlStateNormal];
    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnHome.titleLabel.text, nil) font:self.btnHome.titleLabel.font height:self.btnHome.titleLabel.frame.size.height];
    
    [self.btnHome setFrame:CGRectMake(self.btnHome.frame.origin.x, self.btnHome.frame.origin.y, rect.size.width + 30, self.btnHome.frame.size.height)];
    
    [self.btnOptions setTitle:NSLocalizedString(self.btnOptions.titleLabel.text, @"") forState:UIControlStateNormal];
    [self.btnParq setTitle:NSLocalizedString(self.btnParq.titleLabel.text, @"") forState:UIControlStateNormal];
    [self.btnAssessment setTitle:NSLocalizedString(self.btnAssessment.titleLabel.text, @"") forState:UIControlStateNormal];
    [self.btnProgram setTitle:NSLocalizedString(self.btnProgram.titleLabel.text, @"") forState:UIControlStateNormal];
    [self.btnViewAllNote setTitle:NSLocalizedString(self.btnViewAllNote.titleLabel.text, @"") forState:UIControlStateNormal];
    [self.btnShowTimer setTitle:NSLocalizedString(self.btnShowTimer.titleLabel.text, @"") forState:UIControlStateNormal];
    
    UIFontDescriptor * fontD = [self.lblTitleTraining.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold
                                ];

    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language isEqualToString:@"en"] || [language isEqualToString:@"zh-Hans"] || [language isEqualToString:@"nb"]) {
        [self.lblTitleTraining setFont:[UIFont fontWithDescriptor:fontD size:24.0]];
        [self.lblTitleNotebook setFont:[UIFont fontWithName:@"Proxima Nova Alt" size:24.0]];
    }
        
    self.lblTitleNotebook.text = NSLocalizedString(self.lblTitleNotebook.text, nil);
    self.lblTitleTraining.text = NSLocalizedString(self.lblTitleTraining.text, nil);
}


//---------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------

#pragma mark
#pragma mark  MFMailComposer Delegate Method

//-----------------------------------------------------------------------

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultSent:
                MESSAGE(@"Sent--->");
            DisplayLocalizedAlertWithTitle(NSLocalizedString(@"EmailSent", nil), kAppName);
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultSaved:
                MESSAGE(@"Sent--->");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultCancelled:
                MESSAGE(@"Sent--->");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultFailed:
                MESSAGE(@"Sent--->");
            DisplayLocalizedAlertWithTitle(NSLocalizedString(@"EmailSendError", nil), kAppName);
            break;
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : ParQcontroller Delegate Methods

//---------------------------------------------------------------------------------------------------------------

-(void)openMailForScreenShotImage:(UIImage*)image{
    START_METHOD
    // DLOG(@"Image Taken %@",image);
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ProgramData",[FileUtility basePath]]];
    NSString * imgNm = [NSString stringWithFormat:@"ProgramData%@",self.strClientId];
    [FileUtility createFileInFolder:@"ProgramData" withName:imgNm withData:data];
    [self sendMailWithAttachment:TRUE forType:kisProgram];
    END_METHOD
}

//-----------------------------------------------------------------------
- (void) openFbForScreenShotImage:(UIImage *)image {
    
    SLComposeViewController *facebook =[    SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [facebook addImage:image];
    [self presentViewController:facebook animated:YES completion:nil];
    
    
}

//-----------------------------------------------------------------------

- (void) displayClientID:(NSString *)clientID :(NSString *)clientName {
    
    
    self.strClientId = clientID;
    self.strClientName = clientName;
    
    for (int i = 101 ; i <= 104; i++) {
        UIButton *btn = (UIButton *) [self.tabView viewWithTag:i];
        [btn setEnabled:TRUE];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) hideOptionView {
    if(self.optionsVCOBJ) {
        [self.optionsVCOBJ.view removeFromSuperview];
        self.optionsVCOBJ = nil;
        
    }
}

//---------------------------------------------------------------------------------------------------------------

-(void)presentCamera {
    
    
    UIActionSheet * actionSheet = [[UIActionSheet alloc] init];
    [actionSheet setDelegate:(id)self];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Gallery", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [actionSheet setCancelButtonIndex:2];
    [actionSheet setTitle:NSLocalizedString(@"Add Photo", nil)];
    [actionSheet showInView:self.view];
    
    
}


//---------------------------------------------------------------------------------------------------------------

- (void) openCamera {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        if([self.presentedViewController isKindOfClass:[UIAlertController class]]) {
            [self.presentedViewController dismissViewControllerAnimated:FALSE completion:nil];
        }
        _imgPicker = [[UIImagePickerController alloc] init];
        [self.imgPicker setDelegate:(id)self];
        [self.imgPicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        self.imgPicker.allowsEditing = TRUE;
        [self presentViewController:self.imgPicker animated:YES completion:nil];
        
    } else {
        DisplayAlertWithTitle(NSLocalizedString(@"This device do not support camera", nil),kAppName);
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) openGallery {
    
    UIImagePickerController * galleryPicker = [[UIImagePickerController alloc] init];
    [galleryPicker setDelegate:(id)self];
    galleryPicker.allowsEditing = TRUE;
    [galleryPicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    
    if([self.presentedViewController isKindOfClass:[UIAlertController class]]) {
        [self.presentedViewController dismissViewControllerAnimated:FALSE completion:nil];
    }
    
    if (self.pop== nil) {
        _pop = [[UIPopoverController alloc] initWithContentViewController:galleryPicker];
        [self.pop setPopoverContentSize:CGSizeMake(300, 600) animated:NO];
        self.pop.delegate = (id)self;
    }
    
    CGRect rect = CGRectMake(540, 150, 100, 100);
    [self.pop presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    
}


//---------------------------------------------------------------------------------------------------------------
-(void)setEmailIdForClient:(NSString *)emailId {
    
    self.strClientEmail = emailId;
}

//---------------------------------------------------------------------------------------------------------------

-(void)hideOptionsViewIfDisplayed {
    [self hideAnnimatingOptionView];
}

//---------------------------------------------------------------------------------------------------------------

-(void)setDataSavePending {
    
    self.isDataSavePending = TRUE;
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)setDataSavePendingWithValues:(BOOL)withValues {
    
    if(withValues) {
        self.isDataToBeSaved = TRUE;
        self.isDataToBeEntered = FALSE;
    } else {
        self.isDataToBeEntered = TRUE;
        self.isDataToBeSaved = FALSE;
    }
    
}


-(void)noValuesEnteredIsSafeToExit:(BOOL)canExit {
    
    if(canExit) {
        self.isDataToBeEntered = FALSE;
        self.isDataToBeSaved = FALSE;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark : Options Delegate Methods


//-----------------------------------------------------------------------

-(void)selectedOptionsTask:(NSInteger)selTask forEvent:(NSString*)evnt {
    
    START_METHOD
    
    
    MESSAGE(@"selectedOptionsTask-->tag: %ld  and evnt : %@",(long)selTask,evnt);
    
    
    [self hideOptionView];
    
    if([evnt isEqualToString:kisViewAllPrograms]) {
        if(selTask == 55)  {
            viewAllProgOBJ  = [ViewAllProgramVC sharedInstance];
            [viewAllProgOBJ takeScreenShot];
        }
    }
    
    if([evnt isEqualToString:kisParq]) {
        
        if(selTask == 2)  {
            
            if(![self.strClientId isEqualToString:@"0"]) {
                
                
                CGRect scrolOrgFrame = parqVCOBJ.scrollVew.frame;
                
                parqVCOBJ.scrollVew.contentOffset = CGPointZero;
                parqVCOBJ.scrollVew.frame = CGRectMake(0, 0, parqVCOBJ.scrollVew.contentSize.width, parqVCOBJ.scrollVew.contentSize.height);
                [parqVCOBJ.scrollVew.layer setBorderWidth:0];
                
                
                //SUNIL SET IMAGE BACK GROUND COLR
                int intSkin = [commonUtility retrieveValue:KEY_SKIN].intValue;
                
                switch (intSkin) {
                    case FIRST_TYPE:
                    SECOND_TYPE:
                           [parqVCOBJ.scrollVew setBackgroundColor:[UIColor colorWithRed:25/255.f green:33/255.f blue:39/255.f alpha:1.0f] ];

                        
                        break;
                        
                    case THIRD_TYPE:
                        
                         [parqVCOBJ.scrollVew setBackgroundColor:[UIColor  colorWithRed:247/255.f green:245/255.f blue:242/255.f alpha:1.0f] ];
                        
                        break;
                    default:
                        
                        [parqVCOBJ.scrollVew setBackgroundColor:[UIColor   colorWithRed:191/255.f green:196/255.f blue:200/255.f alpha:1.0f] ];
                        
                        break;
                }
                
                
             
                [parqVCOBJ.btnSaveParq setHidden:TRUE];
                [parqVCOBJ.imgViewAstric1 setHidden:TRUE];
                [parqVCOBJ.imgVIewAstric2 setHidden:TRUE];
                [parqVCOBJ.imgviewAstric3 setHidden:TRUE];
                [parqVCOBJ.imgviewAstric4 setHidden:TRUE];
                
                //========================================================
                
                UIGraphicsBeginImageContext(parqVCOBJ.scrollVew.contentSize);
                
                [parqVCOBJ.scrollVew.layer renderInContext:UIGraphicsGetCurrentContext()];
                
                UIImage *screenImg = UIGraphicsGetImageFromCurrentImageContext();
                
                UIGraphicsEndImageContext();
                
                NSData *data = UIImageJPEGRepresentation(screenImg, 1.0);
                
                
                [parqVCOBJ.scrollVew setBackgroundColor:[UIColor clearColor]];
                
                
                //========================================================
                
                parqVCOBJ.scrollVew.frame = scrolOrgFrame;
                [parqVCOBJ.btnSaveParq setHidden:FALSE];
                [parqVCOBJ.imgViewAstric1 setHidden:FALSE];
                [parqVCOBJ.imgVIewAstric2 setHidden:FALSE];
                [parqVCOBJ.imgviewAstric3 setHidden:FALSE];
                [parqVCOBJ.imgviewAstric4 setHidden:FALSE];
                
                [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ParQData",[FileUtility basePath]]];
                NSString * imgNm = [NSString stringWithFormat:@"ParQData%@",self.strClientId];
                [FileUtility createFileInFolder:@"ParQData" withName:imgNm withData:data];
                [self sendMailWithAttachment:TRUE forType:kisParq];
                
            }
            
            
        }
    }
    
    if([evnt isEqualToString:kisAssessment]) {
        
        //  3== save  , 4 == DontSave , 5 == Help , 6 == Edit Sheet Name
        if(selTask == 3) {
            AssessmentViewController *assessment = (AssessmentViewController *) [[objNavcAssessmentVC viewControllers]objectAtIndex:0];
            [assessment saveAssessmentData];
        }
        
        if(selTask == 4) {
            [self updateClientSelectedView];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        if(selTask == 5) {
            
            // Help
            UpdateNotesVC *UpdateNotesVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"updatenote"];
            UpdateNotesVCOBJ.isFromHelpAction = true;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:UpdateNotesVCOBJ];
            [nav.navigationBar setTranslucent:NO];
            nav.modalPresentationStyle = UIModalPresentationFormSheet;
            nav.modalTransitionStyle = kModalTransitionStyleCoverVertical;
            [self.view.window.rootViewController presentViewController:nav animated:YES completion:nil];
            
        }
        if(selTask == 6) {
            AssessmentViewController *assessment = (AssessmentViewController *) [[objNavcAssessmentVC viewControllers]objectAtIndex:0];
            [assessment btnEditAssessmentSheetNameClicked:nil];
            
            
        }
        
        
        if(selTask == 7) {
            
            // DLOG(@"Exported Assessment");
            AssessmentViewController *assessment = (AssessmentViewController *) [[objNavcAssessmentVC viewControllers]objectAtIndex:0];
            if(![self.strClientId isEqualToString:@"0"]) {
                if([assessment.btnAssessment isSelected]) {
                    CGRect originalFrame = assessment.scrollViewAssesmet.frame;
                    
                    assessment.scrollViewAssesmet.contentOffset = CGPointZero;
                    assessment.scrollViewAssesmet.frame = CGRectMake(0, 0,originalFrame.size.width + 50, assessment.scrollViewAssesmet.contentSize.height + 200);
                    

                    
                    CGRect frame = assessment.view.frame;
                    frame.size = assessment.scrollViewAssesmet.frame.size;
                    
                    if(self.isLandscapeMode) {
                        frame.size.width = frame.size.width + 300;
                    } else {
                        assessment.view.frame = frame;
                    }
                    assessment.view.frame = frame;
                    
                    
                    [assessment.view setBackgroundColor:[UIColor whiteColor]];
                    [assessment.scrollViewAssesmet setBackgroundColor:[UIColor whiteColor]];
                    
                    //====================================================
                    
                    UIGraphicsBeginImageContextWithOptions(assessment.view.frame.size, TRUE, 0.0);
                    
                    [assessment.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                    
                    UIImage *screenImg = UIGraphicsGetImageFromCurrentImageContext();
                    
                    UIGraphicsEndImageContext();
                    
                    NSData *data = UIImageJPEGRepresentation(screenImg, 1.0);
                    
                    assessment.scrollViewAssesmet.frame = originalFrame;
                    
                    
                    //========================================================
                    
                    [assessment.view setBackgroundColor:[UIColor clearColor]];
                   
                    [assessment.scrollViewAssesmet setBackgroundColor:[UIColor clearColor]];
                    
                    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/AssessmentData",[FileUtility basePath]]];
                    NSString * imgNm = [NSString stringWithFormat:@"AssessmentData%@",self.strClientId];
                    [FileUtility createFileInFolder:@"AssessmentData" withName:imgNm withData:data];
                    [self sendMailWithAttachment:TRUE forType:kisAssessment];
                    
                } else {
                    
                    // Photo Image  Screen Selected
                    
                    CGRect originalFrame = assessment.scrollViewData.frame;
                    
                    CGRect orgPhotoFrame = assessment.scrollViewPhotos.frame;
                    
                    assessment.scrollViewPhotos.contentOffset = CGPointZero;
                    assessment.scrollViewPhotos.frame = CGRectMake(assessment.scrollViewPhotos.frame.origin.x, 0, assessment.scrollViewPhotos.frame.size.width, assessment.scrollViewPhotos.contentSize.height);
                    
                    
                    assessment.scrollViewData.contentOffset = CGPointZero;
                    assessment.scrollViewData.frame = CGRectMake( assessment.scrollViewData.frame.origin.x, 0,assessment.viewPhotos.frame.size.width, assessment.scrollViewData.contentSize.height + 200);
                    
                    
                    CGRect frame = assessment.view.frame;
                    frame.size = assessment.scrollViewData.frame.size;
                    
                    if(self.isLandscapeMode) {
                        frame.size.width = frame.size.width + 300;
                    }else {
                        frame.size.width = 768;
                    }
                    
                    assessment.view.frame = frame;
                   
                    //SUNIL SET IMAGE BACK GROUND COLR
                    int intSkin = [commonUtility retrieveValue:KEY_SKIN].intValue;
                    
                    switch (intSkin) {
                        case FIRST_TYPE:
                  
                            
                            [assessment.view setBackgroundColor:[UIColor colorWithRed:25/255.f green:33/255.f blue:39/255.f alpha:1.0f] ];
                            [assessment.scrollViewData setBackgroundColor:[UIColor colorWithRed:25/255.f green:33/255.f blue:39/255.f alpha:1.0f] ];
                            [assessment.scrollViewPhotos setBackgroundColor:[UIColor colorWithRed:25/255.f green:33/255.f blue:39/255.f alpha:1.0f] ];
                            
                            break;
                            
                        case THIRD_TYPE:
                          
                            [assessment.view setBackgroundColor:[UIColor  colorWithRed:247/255.f green:245/255.f blue:242/255.f alpha:1.0f] ];
                            [assessment.scrollViewData setBackgroundColor:[UIColor  colorWithRed:247/255.f green:245/255.f blue:242/255.f alpha:1.0f] ];
                            [assessment.scrollViewPhotos setBackgroundColor:[UIColor  colorWithRed:247/255.f green:245/255.f blue:242/255.f alpha:1.0f] ];
                            
                            break;
                        default:
                            
                            [assessment.view setBackgroundColor:[UIColor   colorWithRed:191/255.f green:196/255.f blue:200/255.f alpha:1.0f] ];
                            [assessment.scrollViewData setBackgroundColor:[UIColor   colorWithRed:191/255.f green:196/255.f blue:200/255.f alpha:1.0f] ];
                            [assessment.scrollViewPhotos setBackgroundColor:[UIColor   colorWithRed:191/255.f green:196/255.f blue:200/255.f alpha:1.0f] ];
                            
                            
                            break;
                    }
                    
                    
                    
                    
                    
                    
                    //=======================================================
                    
                    
                    UIGraphicsBeginImageContextWithOptions(assessment.view.frame.size, TRUE, 0.0);
                    
                    [assessment.view.layer renderInContext:UIGraphicsGetCurrentContext()];
                    
                    UIImage *screenImg = UIGraphicsGetImageFromCurrentImageContext();
                    
                    UIGraphicsEndImageContext();
                    
                    NSData *data = UIImageJPEGRepresentation(screenImg, 1.0);
                    
                    assessment.scrollViewData.frame = originalFrame;
                    assessment.scrollViewPhotos.frame = orgPhotoFrame;
                    
                    //========================================================
                    
                    [assessment.view setBackgroundColor:[UIColor clearColor]];
                    [assessment.scrollViewData setBackgroundColor:[UIColor clearColor]];
                    [assessment.scrollViewPhotos setBackgroundColor:[UIColor clearColor]];
                    
                    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/AssessmentData",[FileUtility basePath]]];
                    NSString * imgNm = [NSString stringWithFormat:@"AssessmentData%@",self.strClientId];
                    [FileUtility createFileInFolder:@"AssessmentData" withName:imgNm withData:data];
                    [self sendMailWithAttachment:TRUE forType:kisAssessment];
                    
                }
                
            }
        }
        
        if(selTask == 8) {
            //delete client sheet
            AssessmentViewController *assessment = (AssessmentViewController *) [[objNavcAssessmentVC viewControllers]objectAtIndex:0];
            [assessment deleteClientAssessmentSheet:nil];
        }
        
        
    }
    
    if([evnt isEqualToString:kisProgram]) {
        if (selTask == 11)
        {
            
            // Email Snapshot
            [self sendMailWithAttachment:FALSE forType:kisProgram];
        }
        else if (selTask == 12)
        {
//            [commonUtility alertMessage:@"Under Development!"];
//            return;
//            
            
            //TODO: copy entire program
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
            
        }
        else if (selTask == 13)
        {
            //Clear Clipboard
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
        }
        else if (selTask == 14)
        {
//            //TODO: Paste from template
//            [commonUtility alertMessage:@"Under Development!"];
//
//            return;
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
           
        }
        else if (selTask == 15)
        {
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
            
            // Edit sheet name
        }
        else if (selTask == 16)
        {
            //TODO: delete sheet  delete workout
//            [commonUtility alertMessage:@"Under Development!"];
//            
//            return;
            UIAlertView * alertShow = [[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Are you sure you want to delete this Workout?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",nil),NSLocalizedString(@"No",nil),nil];
            [alertShow setTag:1002];
            [alertShow show];
            
        }
        else if (selTask == 10)
        {
            
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
            
        } else if (selTask == 18) {
            
            // Export the program screen
            if(![self.strClientId isEqualToString:@"0"]) {
                
                ProgramViewController *program = [ProgramViewController sharedInstance];
                [program takeScreenShot];
                
            }
            
            
        } else if (selTask == 100) {
            
            if(![self.strClientId isEqualToString:@"0"]) {
                
                ProgramViewController *program = [ProgramViewController sharedInstance];
                [program takeScreenShotAndShareToFB];
                
            }
        }
        else if (selTask == 17)
        {
//            // TODO: copy new line
//            [commonUtility alertMessage:@"Under Development!"];
//
//            return;
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
            
        }
        else if (selTask == 20)
        {
              //TODO: copy workout
//            [commonUtility alertMessage:@"Under Development!"];
//
//            return;
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
          
        }
        else if (selTask == 19)
        {
            [programVCOBJ selectedOptionsTask:selTask forEvent:evnt];
            // Edit workout
        }
        
    }
    
}


//---------------------------------------------------------------------------------------------------------------

-(void)hideOptionsViewIfShowing {
    [self hideAnnimatingOptionView];
}

//---------------------------------------------------------------------------------------------------------------

-(void)updateLasteEditedSheetForClientID:(NSString*)strClientId{
    
    if(self.isOtherOptionsSelected) {
        if(objNavcAssessmentVC != nil) {
            AssessmentViewController *objAssesmetsVC = (AssessmentViewController*)(objNavcAssessmentVC.viewControllers[0]);
            if([objAssesmetsVC isKindOfClass:[AssessmentViewController class]]) {
                NSInteger sheetInfo = objAssesmetsVC.currentSheet;
                NSInteger currentPage = objAssesmetsVC.currentPage;
                [objAssesmetsVC changeTheButtonTitles];
                NSString *strSheetAndPage = [NSString stringWithFormat:@"%ld,%ld",(long)currentPage,(long)sheetInfo];
                [self updateTheLastEditedPageWithDetail:strSheetAndPage ForClient:strClientId];
            }
        }
        
    }
}

//---------------------------------------------------------------------------------------------------------------

-(void)updateLastEditedProgramForClient:(NSString *)strClientId {
    
    if(isOtherOptionsSelected) {
        if(programVCOBJ != nil) {
            NSInteger curntSheet = programVCOBJ.currentSheet;
            NSInteger currentBlock = programVCOBJ.currentBlockNo;
            NSString *strsheetAndeBlock = [NSString stringWithFormat:@"%ld,%ld",(long)curntSheet,(long)currentBlock];
            [self updateTheLastEditedPageWithDetail:strsheetAndeBlock ForClient:strClientId];
        }
        
    }
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Imagepicker Delegate Methods

//---------------------------------------------------------------------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    START_METHOD
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *originalImage = info[UIImagePickerControllerEditedImage];
    UIImageWriteToSavedPhotosAlbum(originalImage, nil, nil, nil);
    [parqVCOBJ.btnProfilePic setBackgroundImage:originalImage forState:UIControlStateNormal];
    
    MESSAGE(@"imagePickerController- strClientId---> %@",self.strClientId);

    if(![self.strClientId isEqualToString:@"0"]) {

    [parqVCOBJ saveProfileImageWithClientId:self.strClientId];
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Alertview Delegate Methods

//---------------------------------------------------------------------------------------------------------------

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    int t = (int)alertView.tag;
    if (buttonIndex == 0 && t == 5000) {
        [[CStoreKit sharedInstance]buy:kFullAccessProductId delegate:(id)self];
    }
    else if (buttonIndex == 0 && t == 1002)
    {
        //Invoke method for delete ASSESSMENT SHEET detail
        [self btnDeleteSheet_Click];
    }
    
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark-UIActionsheet delegate methods

//---------------------------------------------------------------------------------------------------------------

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:{
            [self openCamera];
            break;
        }
        case 1:{
            [self openGallery];
            break;
        }
    }
    
}

//---------------------------------------------------------------------------------------------------------------'

- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    
}

//-----------------------------------------------------------------------

-(void)updateClientSelectedView {
    START_METHOD
    if (![self.strClientId isEqualToString:@"0"]) {
        NSInteger t = [[DataManager initDB] updateLastViewedScreen:selectedScrrenID forClient:self.strClientId];
        if (t == 0) {
            MESSAGE(@"Data Updated successfully");
        }
        else {
            MESSAGE(@"Data Not Updated successfully");
        }
        
    }
}
//---------------------------------------------------------------------------------------------------------------

-(void)updateTheLastEditedPageWithDetail:(NSString*)editedPageInfo ForClient:(NSString*)clientId {
    
    if(![self.strClientId isEqualToString:@"0"]) {
        
        if([selectedScreen isEqualToString:kisParq]) {
            int t = [[DataManager initDB]updateLastEditedScreen:@"0" forClientId:clientId];
            if(t==0) {
            }
        }
        
        if([selectedScreen isEqualToString:kisAssessment]) {
            int t= [[DataManager initDB]updateLastEditedScreen:editedPageInfo forClientId:clientId];
            if(t==0) {
            } else {
            }
        }
        
        if([selectedScreen isEqualToString:kisProgram]) {
            
            int t= [[DataManager initDB]updateLastEditedScreen:editedPageInfo forClientId:clientId];
            if(t==0) {
            }
            
        }
        
        if([selectedScreen isEqualToString:kisNotes]) {
            
            int t = [[DataManager initDB]updateLastEditedScreen:@"0" forClientId:clientId];
            if(t==0) {
            }
            
        }
    }
    
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark : Csstorkit Delegate Method

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[NSNotificationCenter defaultCenter] postNotificationName:kupdateVersionNotification object:nil];
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseFailed:(NSString*)productId error:(NSString *)error {
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction failed, Please try again", nil)];
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseCancelled:(NSString*)productId {
    
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction Cancelled, Please try again", nil)];
    
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - Life Cycle Methods


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    
    [super viewDidLoad];

    _selectedTabTag =   101;
    
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isFromViewAllTable"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isFromNotes"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    appDelegate.isFieldUpdated = NO;
    [self localizeControls];
    [self.navigationController.navigationBar setHidden:YES];
    
    
    if (![self.strClientId isEqualToString:@"0"]) {
        
        NSString * gender = [[DataManager initDB] getClientsGender:self.strClientId];
        [[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString * age = [[DataManager initDB] getClientsAge:self.strClientId];
        [[NSUserDefaults standardUserDefaults] setObject:age forKey:kAge];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        selectedScrrenID = [[DataManager initDB] getLastViewdScreenForClient:self.strClientId];
        
        NSString *lastVisitedScreen = [[DataManager initDB]getLastEditedScreenForClient:self.strClientId];
        
        
        if (selectedScrrenID == 0  || !selectedScrrenID) {
            [self defaultSelectedViewController];
        } else{
            if (selectedScrrenID == 4) {
                [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"FromLastVisitedNotes"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:selectedScrrenID+100]];
                
            }
            else if (selectedScrrenID != 2)
            {
                [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:selectedScrrenID+100]];
                
            }
            else{
                  
            }
        }
        
        
    } else {
        
        for (int i = 101 ; i <= 104; i++) {
            UIButton *btn = (UIButton *) [self.view viewWithTag:i];
            if([btn tag] == 101) {
                [btn setEnabled:TRUE];
            } else {
                [btn setEnabled:FALSE];
            }
        }
        
        if([self.selectedScreen isEqualToString:kisParq]) {
            
            for(UIView *subview in [self.backgroundAddView subviews]) {
                [subview removeFromSuperview];
            }
            for (UIView *view in self.tabView.subviews) {
                if ([view isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)view;
                    [btn setBackgroundColor:[UIColor clearColor]];
                }
            }
            
        }
        [self defaultSelectedViewController];
        
    }
    
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    START_METHOD
    
    [super viewWillAppear:animated];
    
   
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        self.isLandscapeMode = TRUE;
        [self setUpLandscapeLayout];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
        
    } else {
        self.isLandscapeMode = FALSE;
        [self setUpPortraitLayout];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    }
    
    
}

//-----------------------------------------------------------------------

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [self setUpLandscapeLayout];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
        
    } else {
        [self setUpPortraitLayout];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    }
    
    
    if ((![self.strClientId isEqualToString:@"0"]) && selectedScrrenID==2) {
        
        NSInteger selScr = [[DataManager initDB] getLastViewdScreenForClient:self.strClientId];
        [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:selScr+100]];
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    if(self.optionsVCOBJ) {
        [self performSelector:@selector(optionsButtonTapped:) withObject:nil];
    }
    
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL) prefersStatusBarHidden {
    return NO;
}


//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    if([self.navigationController.visibleViewController isKindOfClass:[AutoSuggestViewController class]]){
        AutoSuggestViewController *objProgramGetVC = (AutoSuggestViewController*)self.navigationController.visibleViewController;
        if ([objProgramGetVC respondsToSelector:@selector(didRotateFromInterfaceOrientation:)]) {
            [objProgramGetVC didRotateFromInterfaceOrientation:fromInterfaceOrientation];
        }
        
    }
    
    [self hideAnnimatingOptionView];
    if(UIInterfaceOrientationIsLandscape(fromInterfaceOrientation)) {
        // set Landscape setup
         [self setUpPortraitLayout];
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    } else {
        
        // set potrait setup
       
        [self setUpLandscapeLayout];
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
    }

}

//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    [self hideAnnimatingOptionView];
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        // set Landscape setup
        [self setUpLandscapeLayout];
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
    } else {
        
        // set potrait setup
        [self setUpPortraitLayout];
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    }
    
}


//-----------------------------------------------------------------------

-(void)setUpLandscapeLayout {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight)];
      
    [self.imgTopNavigation setFrame:CGRectMake(0, 20, kLandscapeWidth, 57)];
    [self.imgTopNavigation setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];
    [self.backgroundImgv setFrame:CGRectMake(0, 20, kLandscapeWidth, kLandscapeHeight)];
    [self.backgroundImgv setImage:[UIImage imageNamed:@"BackgroundImage_iPad"]];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTitleTraining.text font:self.lblTitleTraining.font height:self.lblTitleTraining.frame.size.height];
    self.lblTitleTraining.frame = CGRectMake(257+140, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblTitleNotebook.text font:self.lblTitleNotebook.font height:self.lblTitleNotebook.frame.size.height];
    self.lblTitleNotebook.frame = CGRectMake(self.lblTitleTraining.frame.origin.x + self.lblTitleTraining.frame.size.width , 23,rect.size.width + 5 ,48);


    
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnShowTimer.titleLabel.text, nil) font:self.btnShowTimer.titleLabel.font height:self.btnShowTimer.titleLabel.frame.size.height];
    CGRect rect1 = [[AppDelegate sharedInstance] getWidth:self.btnShowTimer.titleLabel.text font:self.btnShowTimer.titleLabel.font height:self.btnShowTimer.titleLabel.frame.size.height];
    
    if (rect.size.width < 90) {
        rect = [[AppDelegate sharedInstance] getWidth:self.btnOptions.titleLabel.text font:self.btnOptions.titleLabel.font height:self.btnOptions.titleLabel.frame.size.height];
        [self.btnOptions setFrame:CGRectMake(self.view.frame.size.width - (rect.size.width + 30) - (rect1.size.width + 30)  , 20, rect.size.width + 30, 55)];
    } else {
        rect = [[AppDelegate sharedInstance] getWidth:self.btnOptions.titleLabel.text font:self.btnOptions.titleLabel.font height:self.btnOptions.titleLabel.frame.size.height];
        [self.btnOptions setFrame:CGRectMake(self.view.frame.size.width - (rect.size.width + 30) - (rect1.size.width + 30), 20, rect.size.width + 30, 55)];
    }
    
    rect = [[AppDelegate sharedInstance] getWidth:self.btnShowTimer.titleLabel.text font:self.btnShowTimer.titleLabel.font height:self.btnShowTimer.titleLabel.frame.size.height];
    [self.btnShowTimer setFrame:CGRectMake(self.btnOptions.frame.origin.x + self.btnOptions.frame.size.width + 1, 20, rect.size.width + 30, 55)];
    
    [self.tabView setFrame:CGRectMake(50, 115, kLandscapeWidth - 100, 35)];
    [self.backgroundAddView setFrame:CGRectMake(49, 147, kLandscapeWidth-100, kLandscapeHeight-170)];
    
    
    [self.btnParq setFrame:CGRectMake(0, 0, 229, 35)];
    [self.btnProgram setFrame:CGRectMake(230, 0, 229, 35)];
    [self.btnAssessment setFrame:CGRectMake(459, 0, 229, 35)];
    [self.btnViewAllNote setFrame:CGRectMake(689, 0, 233, 35)];
    [self.seprator1 setFrame:CGRectMake(229, 0, 1, 35)];
    [self.seprator2 setFrame:CGRectMake(458, 0, 1, 35)];
    [self.seprator3 setFrame:CGRectMake(688, 0, 1, 35)];
    
    UIRectCorner rectCorner1 = UIRectCornerTopLeft | UIRectCornerTopRight;
    [self setRoundRectForView:self.tabView withCorner:rectCorner1];
    UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
    
    if ([self.selectedScreen isEqualToString:kisViewAllPrograms ]) {
        viewAllProgOBJ = [ViewAllProgramVC sharedInstance];
        viewAllProgOBJ.setAsLandscape = TRUE;
        [viewAllProgOBJ setUpLandscapeOrientation];
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
    }
    
    
    
    else if(selectedScrrenID == 1) {
        parqVCOBJ.setAsLandscape = TRUE;
        
        CGRect backgroundRect = self.backgroundAddView.frame;
        backgroundRect.size.height = kLandscapeHeight - 170;
        [self.backgroundAddView setFrame:backgroundRect];
        
        UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
        [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
        
        [parqVCOBJ setUpLandscapeOrientation];
    }
    
    else if(selectedScrrenID == 2) {
        programVCOBJ = [ProgramViewController sharedInstance];
        programVCOBJ.setAsLandscape = TRUE;
        [programVCOBJ setUpLandscapeOrientation];
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
        
        CGRect backgroundRect = self.backgroundAddView.frame;
        backgroundRect.size.height = 678;
        
        [self.backgroundAddView setFrame:backgroundRect]; //programBack_Landscape
        
        UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
        [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
        
    }
    
    if(selectedScrrenID == 3) {
        if (objNavcAssessmentVC != nil) {
            
            CGRect backgroundRect = self.backgroundAddView.frame;
            backgroundRect.size.height = 678;
            [self.backgroundAddView setFrame:backgroundRect];
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            AssessmentViewController *objAssesmetsVC = (AssessmentViewController*)(objNavcAssessmentVC.viewControllers[0]);
            objAssesmetsVC.isLandscapeSet = TRUE;
            self.backgroundAddView.backgroundColor = [UIColor clearColor];
            [objAssesmetsVC setupLandscapeOrientation];
        }
    }
    
    if(selectedScrrenID == 4) {
        notesVCOBJ.setAsLandscape = TRUE;
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
        [notesVCOBJ setLandscapeOrientation];
    }
    
}


//-----------------------------------------------------------------------

-(void)setUpPortraitLayout {
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.imgTopNavigation setFrame:CGRectMake(0, 20, kPotraitWidth, 57)];
    [self.imgTopNavigation setImage:[UIImage imageNamed:@"Top-Bar"]];
    [self.backgroundImgv setFrame:CGRectMake(0,20, kPotraitWidth, kPotraitHeight)];
    [self.backgroundImgv setImage:[UIImage imageNamed:@"BackgroundImage"]];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTitleTraining.text font:self.lblTitleTraining.font height:self.lblTitleTraining.frame.size.height];
    self.lblTitleTraining.frame = CGRectMake(257+ 45, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblTitleNotebook.text font:self.lblTitleNotebook.font height:self.lblTitleNotebook.frame.size.height];
    self.lblTitleNotebook.frame = CGRectMake(self.lblTitleTraining.frame.origin.x + self.lblTitleTraining.frame.size.width , 23,rect.size.width + 5 ,48);
    

    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnShowTimer.titleLabel.text, nil) font:self.btnShowTimer.titleLabel.font height:self.btnShowTimer.titleLabel.frame.size.height];
    CGRect rect1 = [[AppDelegate sharedInstance] getWidth:self.btnShowTimer.titleLabel.text font:self.btnShowTimer.titleLabel.font height:self.btnShowTimer.titleLabel.frame.size.height];
    
    if (rect.size.width < 90) {
        rect = [[AppDelegate sharedInstance] getWidth:self.btnOptions.titleLabel.text font:self.btnOptions.titleLabel.font height:self.btnOptions.titleLabel.frame.size.height];
        [self.btnOptions setFrame:CGRectMake(self.view.frame.size.width - (rect.size.width + 30) - (rect1.size.width + 30)  , 20, rect.size.width + 30, 55)];
    } else {
        rect = [[AppDelegate sharedInstance] getWidth:self.btnOptions.titleLabel.text font:self.btnOptions.titleLabel.font height:self.btnOptions.titleLabel.frame.size.height];
        [self.btnOptions setFrame:CGRectMake(self.view.frame.size.width - (rect.size.width + 30) - (rect1.size.width + 30), 20, rect.size.width + 30, 55)];
    }
    
    rect = [[AppDelegate sharedInstance] getWidth:self.btnShowTimer.titleLabel.text font:self.btnShowTimer.titleLabel.font height:self.btnShowTimer.titleLabel.frame.size.height];
    [self.btnShowTimer setFrame:CGRectMake(self.btnOptions.frame.origin.x + self.btnOptions.frame.size.width + 1, 20, rect.size.width + 30, 55)];
    

    
    [self.tabView setFrame:CGRectMake(18, 115, 729, 35)];
    [self.backgroundAddView setFrame:CGRectMake(17, 147, 728, 854)];
      
    
    [self.btnParq setFrame:CGRectMake(0, 0, 175, 35)];
    [self.btnProgram setFrame:CGRectMake(176, 0, 180, 35)];
    [self.btnAssessment setFrame:CGRectMake(356, 0, 189, 35)];
    [self.btnViewAllNote setFrame:CGRectMake(547, 0, 184, 35)];
    [self.seprator1 setFrame:CGRectMake(175, 0, 1, 35)];
    [self.seprator2 setFrame:CGRectMake(355, 0, 1, 35)];
    [self.seprator3 setFrame:CGRectMake(546, 0, 1, 35)];
    
    UIRectCorner rectCorner1 = UIRectCornerTopLeft | UIRectCornerTopRight;
    [self setRoundRectForView:self.tabView withCorner:rectCorner1];
    UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
    
    if ([self.selectedScreen isEqualToString:kisViewAllPrograms ]) {
        viewAllProgOBJ.setAsLandscape = FALSE;
        [viewAllProgOBJ setupPortraitOrientation];
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
        
    }
    
    else if(selectedScrrenID== 1) {
        parqVCOBJ.setAsLandscape = FALSE;
        [parqVCOBJ setupPortraitOrientation];
        CGRect backgroundRect = self.backgroundAddView.frame; // h =598
        backgroundRect.size.height = 854; // 878
        [self.backgroundAddView setFrame:backgroundRect];
        
        UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
        [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
    }
    
    else if(selectedScrrenID == 2) {
        programVCOBJ = [ProgramViewController sharedInstance];
        programVCOBJ.setAsLandscape = FALSE;
        [programVCOBJ setupPortraitOrientation];
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
    }
    
    if(selectedScrrenID == 3) {
        if (objNavcAssessmentVC != nil) {
            AssessmentViewController *objAssesmetsVC = (AssessmentViewController*)(objNavcAssessmentVC.viewControllers[0]);
            objAssesmetsVC.isLandscapeSet = false;
            self.backgroundAddView.backgroundColor = [UIColor clearColor];
            
            CGRect backgroundRect = self.backgroundAddView.frame; // h =598
            backgroundRect.size.height = 878; // 678
            [self.backgroundAddView setFrame:backgroundRect];
            
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:self.backgroundAddView withCorner:rectCorner2];
            
            [objAssesmetsVC setupPortraitOrientation];
        }
    }
    
    if(selectedScrrenID == 4) {
        notesVCOBJ.setAsLandscape = FALSE;
        self.backgroundAddView.backgroundColor = [UIColor clearColor];
        [notesVCOBJ setportraitOrientation];
    }
    
    
}



//-----------------------------------------------------------------------

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//Sunil


-(void)actionChnageSkin:(int )tag{
    MESSAGE(@"tag of button : %d",tag);
    //Sunil Background image dynamicaly -->
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    [self.imgTopNavigation setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];
    
    [self.backgroundImgv setImage:[UIImage imageNamed:@"BackgroundImage_iPad"]];
    
    switch (tag) {
        case PORTRAIT:
            
            //<---
            
                [self.backgroundImgv setFrame:CGRectMake(0,20, kPotraitWidth, kPotraitHeight)];
            [self setViewChnagesOnColorPortraitScape:skin];
            
           
            
            break;
            
        case LANDSCAPE:
            
            //<---
            [self.backgroundImgv setFrame:CGRectMake(0, 20, kLandscapeWidth, kLandscapeHeight)];
            
            [self setViewChnagesOnColorLandScape:skin];
            
          
            break;
            
            
        default:
            break;
    }
}

-(void)setViewChnagesOnColorPortraitScape:(int)skin{
    
    MESSAGE(@"setViewChnagesOnColorPortraitScape--> %d",skin);
 
    switch (skin) {
        case FIRST_TYPE:
            [self.backgroundImgv setImage:[UIImage imageNamed:@"Default-Portrait.png"]];
            _lblTitleNotebook.textColor = kFirstSkin;
            _lblTitleTraining.textColor = [UIColor whiteColor];
            [self.btnHome setBackgroundImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            
             [self.btnShowTimer setBackgroundImage:[UIImage imageNamed:@"Timer-btn.png"] forState:UIControlStateNormal];
            
            [self.btnOptions setBackgroundImage:[UIImage imageNamed:@"Options-btn.png"] forState:UIControlStateNormal];
            
            
            
            [self.imgTopNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            
            [self bgImageForParQ:@"programbg_green.png"];
            
            
            
            
            
            break;
        case SECOND_TYPE:
            [self.backgroundImgv setImage:[UIImage imageNamed:@"Default-Portrait.png"]];
            _lblTitleNotebook.textColor = ksecondSkin;
            _lblTitleTraining.textColor = [UIColor whiteColor];
             [self.btnHome setBackgroundImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            
            [self.btnShowTimer setBackgroundImage:[UIImage imageNamed:@"Timer-btn.png"] forState:UIControlStateNormal];
            
            [self.btnOptions setBackgroundImage:[UIImage imageNamed:@"Options-btn.png"] forState:UIControlStateNormal];
             [self.imgTopNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            
             [self bgImageForParQ:@"programbg_green.png"];
            
            break;
        case THIRD_TYPE:
            [self.backgroundImgv setImage:[UIImage imageNamed:@"Default4-Portrait.png"]];
            _lblTitleNotebook.textColor = kThirdSkin;
            _lblTitleTraining.textColor = [UIColor whiteColor];
            
             [self.btnHome setBackgroundImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            [self.btnShowTimer setBackgroundImage:[UIImage imageNamed:@"Timer-btn.png"] forState:UIControlStateNormal];
            
            [self.btnOptions setBackgroundImage:[UIImage imageNamed:@"Options-btn.png"] forState:UIControlStateNormal];
             [self.imgTopNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            [self bgImageForParQ:@"white_assessmentbg-1.png"];
            [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notes-bg"]]];
            
            break;
  
        default:
            _lblTitleNotebook.textColor = kOldSkin;
            _lblTitleTraining.textColor = [UIColor grayColor];
             [self bgImageForParQ:@"programbg"];
            [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notes-bg"]]];
            
            
            break;
    }
    END_METHOD
}


//--> Sunil
/*!
 @Description : Methods for Set View base on bg color
 @Param       :
 @Return      :
 */
-(void)setViewChnagesOnColorLandScape:(int)skin{
    START_METHOD
    
    
    switch (skin) {
        case FIRST_TYPE:
            
            [self.backgroundImgv setImage:[UIImage imageNamed:@"Default-Landscape-ipad.png"]];
            _lblTitleNotebook.textColor = kFirstSkin;
            _lblTitleTraining.textColor = [UIColor whiteColor];
            
            [self.btnHome setBackgroundImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            [self.btnShowTimer setBackgroundImage:[UIImage imageNamed:@"Timer-btn.png"] forState:UIControlStateNormal];
            
            [self.btnOptions setBackgroundImage:[UIImage imageNamed:@"Options-btn.png"] forState:UIControlStateNormal];
             [self.imgTopNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            [self bgImageForParQ:@"programbg_iPad_gray.png"];
            
            [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notesbg_gray.png"]]];
          
           break;
        case SECOND_TYPE:
            [self.backgroundImgv setImage:[UIImage imageNamed:@"Default-Landscape-ipad.png"]];
            _lblTitleNotebook.textColor = ksecondSkin;
            _lblTitleTraining.textColor = [UIColor whiteColor];
             [self.btnHome setBackgroundImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            [self.btnShowTimer setBackgroundImage:[UIImage imageNamed:@"Timer-btn.png"] forState:UIControlStateNormal];
            
            [self.btnOptions setBackgroundImage:[UIImage imageNamed:@"Options-btn.png"] forState:UIControlStateNormal];
             [self.imgTopNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            [self bgImageForParQ:@"programbg_iPad_gray.png"];
            
            [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notesbg_gray.png"]]];
            
            break;
        case THIRD_TYPE:
            [self.backgroundImgv setImage:[UIImage imageNamed:@"Default2-Landscape-ipad.png"]];
            _lblTitleNotebook.textColor = kThirdSkin;
            _lblTitleTraining.textColor = [UIColor whiteColor];
             [self.btnHome setBackgroundImage:[UIImage imageNamed:@"Home-btn.png"] forState:UIControlStateNormal];
            [self.btnShowTimer setBackgroundImage:[UIImage imageNamed:@"Timer-btn.png"] forState:UIControlStateNormal];
            
            [self.btnOptions setBackgroundImage:[UIImage imageNamed:@"Options-btn.png"] forState:UIControlStateNormal];
             [self.imgTopNavigation setImage:[UIImage imageNamed:@"nav-bg1.png"]];
            
            [self bgImageForParQ:@"white_assessmentbg-1.png"];
            
           [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"NotesBg_Landscape"]]];
            break;
            
        default:
            _lblTitleNotebook.textColor = kOldSkin;
            _lblTitleTraining.textColor = [UIColor grayColor];
        
        
        
             [self bgImageForParQ:@"programbg1_iPad.png"];
            
          [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"NotesBg_Landscape"]]];
            break;
    }

}

//sunil
-(void)setSelectedButtonColor:(UIButton *)tabBtn{
    START_METHOD
   
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    switch ( skin) {
        case FIRST_TYPE:
             [tabBtn setBackgroundColor:kFirstSkin];
            break;
        case SECOND_TYPE:
            [tabBtn setBackgroundColor:ksecondSkin];
            break;
        case THIRD_TYPE:
            [tabBtn setBackgroundColor:kThirdSkin];
             break;
            
        default:
            [tabBtn setBackgroundColor:kOldSkin];

            break;
    }
}

-(void)setUnSelectedButtonColor:(UIButton *)tabBtn{
 START_METHOD
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    switch ( skin) {
        case FIRST_TYPE:
            [tabBtn setBackgroundColor:kNotSelectedBtnBGColorFirstSkin];
            break;
        case SECOND_TYPE:
            [tabBtn setBackgroundColor:kNotSelectedBtnBGColorFirstSkin];
            break;
        case THIRD_TYPE:
            [tabBtn setBackgroundColor:kNotSelectedBtnBGColorFirstSkin];
            break;
            
        default:
            [tabBtn setBackgroundColor:kNotSelectedBtnBGColor];
            
            break;
    }
}

/*!
 @Description : Methods for set Full bg of view sunil
 @Param       :
 @Return      :
 */
-(void)bgImageForParQ:(NSString *)strImage{
    START_METHOD
    UIColor *colorWhite = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.7];

    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch ( skin) {
        case FIRST_TYPE:
            [self.backgroundAddView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:strImage]]];
            
            MESSAGE(@"self.backgroundAddView: color: %@",self.backgroundAddView.backgroundColor);
            
            [parqVCOBJ.btnSaveParq setBackgroundColor:kFirstSkin];
            
           [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:strImage]]];
            
             [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notesbg_gray.png"]]];
            
            
            break;
            
        case SECOND_TYPE:
            [self.backgroundAddView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:strImage]]];
            
            [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:strImage]]];
            
            [parqVCOBJ.btnSaveParq setBackgroundColor: ksecondSkin];

           
            
            [notesVCOBJ.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notesbg_gray.png"]]];
            break;
            
        case THIRD_TYPE:
                 MESSAGE(@" if(tagSelectedTab==103){--------out > %d",tagSelectedTab);
            if(tagSelectedTab==103){
                
                MESSAGE(@" if(tagSelectedTab==103){--------> %d",tagSelectedTab);
                [self.backgroundAddView setBackgroundColor:colorWhite];
                
                [ programVCOBJ.programView setBackgroundColor:colorWhite];
           
            }else{
                  [self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
                parqVCOBJ.scrollVew.backgroundColor = colorWhite;
                
                [ programVCOBJ.programView setBackgroundColor:colorWhite];
                
            }
            [parqVCOBJ.btnSaveParq setBackgroundColor:kThirdSkin];
            

            
            break;
            
        default:
//         [self.backgroundAddView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:strImage]]];
          [ programVCOBJ.programView setBackgroundColor:[UIColor clearColor]];
   [ programVCOBJ.programView setBackgroundColor:colorWhite];
//            
//            [parqVCOBJ.btnSaveParq setBackgroundColor: kOldSkin];

         
            
//
//
            
            
            if(tagSelectedTab==103){
                
            [self.backgroundAddView setBackgroundColor:colorWhite];
            }
            
            
            parqVCOBJ.scrollVew.backgroundColor = colorWhite;
            break;
    }
    END_METHOD
}


//TODO: POST REQUESTS

//Method for post client's first program
-(void)postRequestForClientFirstProgram:(NSMutableDictionary *)dictProgram{
    START_METHOD
   
    //Set Action
    [dictProgram setValue:@"addProgram" forKey:ACTION];
    [dictProgram setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params addProgram=: %@ and Url : %@",dictProgram,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictProgram
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictProgramTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData add program fi=rst : dictProgram: %@",dictProgramTemp);
                                   
                                   NSString *strAction =   [dictProgramTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addProgram"]){
                  
                                       //Invoke method for save in loacl db
                                       [self saveFirstProgramInLocal:[dictProgramTemp mutableCopy]];
                                       
                                   }
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                       
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}


-(void)saveFirstProgramInLocal:(NSMutableDictionary *)dictProgram{
    
    MESSAGE(@"Add clinet's first program: %@", dictProgram);
   
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:kAppDateFormat];
    NSString *result = [formater stringFromDate:[NSDate date]];
    
    
    programVCOBJ = [ProgramViewController sharedInstance];
    int i;
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
    i= [[DataManager initDB] addNewSheet:[dictProgram objectForKey:ksheetName] ForClient:self.strClientId ProgramPurpose:[dictProgram objectForKey:ksProgramPurpose] withWorkoutName:[dictProgram objectForKey:kBlockTitle] ForBlockNo:[dictProgram objectForKey:kSheetId]];
        
        
        [[DataManager initDB]insertSheetBlocksForSheet:[dictProgram objectForKey:kSheetId] date:result blockNo:[dictProgram objectForKey:kBlockNo] clientId:[dictProgram objectForKey:kClientId] blockTitle:[dictProgram objectForKey:kBlockTitle] blockNotes:@"" andBlockId:[dictProgram objectForKey:kBlockNo]];
        
        //sunil-> date: 02 sept 2016
        
        
    }else{
    
     i = [[DataManager initDB] addNewSheet:[dictProgram objectForKey:ksheetName] ForClient:self.strClientId ProgramPurpose:[dictProgram objectForKey:ksProgramPurpose] withWorkoutName:[dictProgram objectForKey:kBlockTitle] ForBlockNo:@"1"];
    }
    
    if(i==0) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
        }];
        
        programVCOBJ.strClientId = self.strClientId;
        programVCOBJ.strClientName = self.strClientName;
        programVCOBJ.selectedSheet = @"1";
        programVCOBJ.No_OfSheet = i;
        programVCOBJ.delegate = self;
        programVCOBJ.strProgramSheetName = [dictProgram objectForKey:ksheetName];
        
        [self.backgroundAddView addSubview:programVCOBJ.view];
        [self.backgroundAddView setBackgroundColor:[UIColor clearColor]];
        
        UIButton *selectedBtn = (UIButton *)[self.view viewWithTag:102];
        //            [selectedBtn setBackgroundColor:kSelectedBtnBGColor];
        //Invoke method for set back ground color
        [self setSelectedButtonColor:selectedBtn];
                
        
    } else {
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
        }];
        [self tabBarButtonAction:(UIButton*)[self.view viewWithTag:101]];
        DisplayAlertWithTitle(NSLocalizedString(@"There seems to be an issue with program creation. Please try again in sometime.", nil), kAppName);
    }
    
}

//TODO : Move to Dashboard for lOg out
-(void)logOut{
    START_METHOD
    
    //Save lout Key
    [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
    
    //Create object of trainer profile view
    ViewController *objMenuView  = (ViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    //Push view
    [self.navigationController popViewControllerAnimated:NO];
}



//TODO: DELET THE current workout

-(void)postRequestForDeleteWorkout:(NSDictionary *)params{
    START_METHOD
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@deleteData/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"requestForPassword-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                               
                               //Invoke method for delete Workout detail
                               [self btnDeleteSheet_Click];
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                               
                               
                               
                               
                           }];
    END_METHOD
    
}

@end
