//  
//
//  Created by WLI.
//  Copyright 2011 WLI. All rights reserved.
//

@interface FileUtility : NSObject {
}

// Folder methods
+ (NSString*)basePath;
+ (NSString*)cachePath;
+ (void)createDirectoryIfNeededAtPath:(NSString*)path;
+ (NSString *)pathForResource:(NSString *)name ofType:(NSString *)extension;
+ (NSString *)pathForDocResource:(NSString *)filename;
+ (void)createFolderInDocuments:(NSString*)folder;
+ (void)deleteFile:(NSString*)filename;
+ (BOOL)fileExists:(NSString *)filename;
+ (void)createFile:(NSString *)filename;
+ (void)createFileInFolder:(NSString *)folder withName:(NSString *)fileName withData:(NSData *)contentData;
@end
