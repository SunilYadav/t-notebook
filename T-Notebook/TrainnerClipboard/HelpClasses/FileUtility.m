//  
//
//  Created by WLI.
//  Copyright 2011 WLI. All rights reserved.
//

#import "FileUtility.h"

@implementation FileUtility

// ----------------------------------------------------------------------------

#pragma mark -
#pragma mark Path methods

// -----------------------------------------------------------------------------

+ (NSString*)basePath {
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    DLOG(@"Path >> %@",[paths objectAtIndex:0]);
	NSAssert([paths count] == 1, @"");
	return [paths objectAtIndex:0];		
}

// -----------------------------------------------------------------------------

+ (NSString*)cachePath {
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSAssert([paths count] == 1, @"");
	return [paths objectAtIndex:0];		
}

// -----------------------------------------------------------------------------

+ (void)createDirectoryIfNeededAtPath:(NSString*)path {
	NSFileManager* fileManager = [NSFileManager defaultManager];
	
	NSError *error = nil;
	if (![fileManager fileExistsAtPath:path]) {
		[fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
	}
}

// -----------------------------------------------------------------------------

+ (void)createFolderInDocuments:(NSString*)folder {
	NSString* path = [NSString stringWithFormat:@"%@/%@", [FileUtility basePath], folder];
	[FileUtility createDirectoryIfNeededAtPath:path];
}

// -----------------------------------------------------------------------------

#pragma mark -
#pragma mark File methods

+ (void)deleteFile:(NSString*)filename {
	NSFileManager* manager = [NSFileManager defaultManager];
	NSError* error = nil;
	if (![manager removeItemAtPath:filename error:&error]) {
		DebugSTRLOG(@"File not deleted:",filename);
	}	
}

// -----------------------------------------------------------------------------

+ (NSString *)pathForResource:(NSString *)name ofType:(NSString *)extension {
	NSString* result = nil;
	//NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	NSString* path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
	if (path == nil) {
		//[pool release];
		return nil;
	}
	
	result = [[NSString alloc] initWithString:path];
	//[pool release];
	return result ;
}

// -----------------------------------------------------------------------------

+ (NSString *)pathForDocResource:(NSString *)filename {
	NSString* result = nil;
	//NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	NSString * filePath = [NSString stringWithFormat:@"%@/%@.xml", [FileUtility basePath],filename];
	if (filePath == nil) {
		//[pool release];
		return nil;
	}	
	result = [[NSString alloc] initWithString:filePath];
	//[pool release];
	return result ;
}

// -----------------------------------------------------------------------------

+ (BOOL)fileExists:(NSString *)filename {
	NSFileManager* manager = [NSFileManager defaultManager];
	return [manager fileExistsAtPath:filename];
}

// -----------------------------------------------------------------------------

+ (void)createFile:(NSString *)filename {
	NSFileManager* manager = [NSFileManager defaultManager];
	NSString * filePath = [NSString stringWithFormat:@"%@/%@.xml", [FileUtility basePath],filename];
	if (![FileUtility fileExists:filePath]){
		[manager createFileAtPath:filePath contents:nil attributes:nil];
	}else {
		DebugSTRLOG(@"File present: ",filename);
	}
}

// -----------------------------------------------------------------------------

+ (void) createFileInFolder:(NSString *)folder withName:(NSString *)fileName withData:(NSData *)contentData {
	NSString * filePath = [NSString stringWithFormat:@"%@/%@/%@.png", [FileUtility basePath],folder,fileName];
	BOOL success = [[NSFileManager defaultManager] createFileAtPath:filePath contents:contentData attributes:nil];
	if (success) {
		DebugSTRLOG(@"File Stored at: ",filePath);
	}
	else {
		DebugLOG(@"File not Stored");
	}

}

@end
