//
//  AudioPlayer.h
//  IPH_3in1Timer
//
//  Created by WLI.
//  Copyright 2011 WLI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol APDelegate

-(void)stopPlayingAudio;

@end

@interface AudioPlayer : NSObject <AVAudioPlayerDelegate>{
	AVAudioPlayer		* myaudioplayer;
	AVAudioRecorder		* myaudioRecorder;
	double currTime;
	int recordtime;
	int recordEncoding;
    BOOL                _isPlaying;
	BOOL                _isPause;
	enum
	{
        //kAudioSessionProperty_OverrideCategoryMixWithOthers        = 'cmix',
		ENC_AAC = 1,
		ENC_ALAC = 2,
		ENC_IMA4 = 3,
		ENC_ILBC = 4,
		ENC_ULAW = 5,
		ENC_PCM = 6,
	} encodingTypes;
}

@property (nonatomic, unsafe_unretained)  id<APDelegate>          apDelegate;
@property (nonatomic, strong) AVAudioPlayer			* myaudioplayer;
@property (nonatomic, strong) AVAudioRecorder		* myaudioRecorder;
@property (nonatomic)BOOL                     isPlaying;
@property (nonatomic)BOOL                     isPause;

-(void) setAudioFromPathUrl:(NSURL *) audiourl;
-(void) recordAndSaveAudioAtPathUrl:(NSURL *) audiourl forDuration:(int) duration; 
-(void) checkTime;
-(void) startPlaying;
-(void) pausePlaying;
-(void) stopPlaying;
-(void) playFromPause;

//+ (AudioPlayer *) sharedInstance;
@end
