//
//  CStoreKit.m
//  E-Cards
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "CStoreKit.h"

static CStoreKit     *storeKitObject;

@implementation CStoreKit

@synthesize state = _state;
@synthesize delegate = _delegate;

- (void)setProductList:(NSArray *)productList {
    if ([_products count]>0){
        [_products removeAllObjects];
    }
    [_products addObjectsFromArray:productList];
}

- (NSArray*)productList {
    NSArray * array = [NSArray arrayWithArray:_products];
    return array;
}
- (NSArray*)validProductList {
    NSArray * array = [NSArray arrayWithArray:_validProducts];
    return array;
}

- (NSInteger)numberOfValidProducts {
    return [_validProducts count];
}

- (NSInteger)numberOfInvalidProducts {
    return ([_products count] - [_validProducts count]);
}

#pragma mark - state change methods

- (void)changeStateTo:(CStorekitState)state {
	
	if (state == _state) {
		return;
	}
	
	_state = state;

}

#pragma mark transaction handling

- (void)transactionIsValidated:(SKPaymentTransaction*)transaction restored:(BOOL)restored {
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
                           transaction.payment.productIdentifier,@"productID",
                           [NSString stringWithFormat:@"%ld",(long)transaction.payment.quantity] ,@"quantity",
                           transaction.transactionIdentifier,@"transaction",
                           transaction.transactionDate,@"date",
                           [NSNumber numberWithBool:restored],@"restored"
                           , nil];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if ([_delegate respondsToSelector:@selector(purchaseDone:purchase:)]) {
        [_delegate purchaseDone:transaction.payment.productIdentifier purchase:dict];
    }
}

- (void)purchaseIsFailed:(SKPaymentTransaction*)transaction {

    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if (transaction.error.code == SKErrorPaymentCancelled) {
		[_delegate purchaseCancelled:transaction.payment.productIdentifier];
    }
	else {
		[_delegate purchaseFailed:transaction.payment.productIdentifier error:[transaction.error.userInfo objectForKey:@"NSLocalizedDescription"]];
	}
}

- (void)verificationIsFailed:(SKPaymentTransaction*)transaction {
    
}

#pragma mark - validation service

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self transactionIsValidated:transaction restored:NO];
        
    });
}

- (void)restoreTransaction:(SKPaymentTransaction*)transaction {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self transactionIsValidated:transaction restored:YES];
    });
}

#pragma mark - purchase methods

- (void)buy:(NSString *)productIdentifier delegate:(id<CStoreKitDelegate>)delegate {
    if (_state != CStoreKitStateReady) {
        [self changeStateTo:CStoreKitStateReady];
    }
    
    self.delegate = delegate;
    SKPayment* payment;
    SKProduct *product = [[SKProduct alloc] init];
    [product setValue:productIdentifier forKey:@"productIdentifier"];
    payment = [SKPayment paymentWithProduct:product];
    
	SKPaymentQueue* paymentQueue = [SKPaymentQueue defaultQueue];
	[paymentQueue addTransactionObserver:self];
	[paymentQueue addPayment:payment];
}

#pragma mark - restore methods

- (void)restore:(id<CStoreKitDelegate>)delegate{
    self.delegate = delegate;
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark -
#pragma mark StoreKit handling

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    if (request == _productsRequest) {
        if ([_validProducts count]>0) {
            [_validProducts removeAllObjects];
        }
        for (SKProduct * product in response.products) {
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [formatter setLocale:product.priceLocale];
            NSString *currencyString = [formatter stringFromNumber:product.price];
            NSMutableDictionary * mutDict;
            for (NSDictionary * dic in _products) {
                if ([product.productIdentifier isEqualToString:[dic objectForKey:@"productId"]]) {
                    mutDict = [NSMutableDictionary dictionaryWithDictionary:dic];
                    break;
                }
            }
            [mutDict setObject:currencyString forKey:@"priceWithCurrency"];
            NSDictionary * dict = [NSDictionary dictionaryWithDictionary:mutDict];
            mutDict = nil;

            [_validProducts addObject:dict];
        }
        
        [self changeStateTo:CStoreKitStateReady];
    }
    
    _productsRequest = nil;
}

//---------------------------------------------------------------------------------------------------------------

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                break;
                
            case SKPaymentTransactionStatePurchased:
                 [self completeTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                [[AppDelegate sharedInstance]showAlert:@"Purchase failed,try again later"];
                [self purchaseIsFailed:transaction];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Restoring transactions delegate

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    
	if ([_delegate respondsToSelector:@selector(paymentQueueRestoreCompletedTransactionsFinished:)]) {
		[_delegate paymentQueueRestoreCompletedTransactionsFinished:queue];
	}
    
}

// -----------------------------------------------------------------------------

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    
	if ([_delegate respondsToSelector:@selector(paymentQueue:restoreCompletedTransactionsFailedWithError:)]) {
		[_delegate paymentQueue:queue restoreCompletedTransactionsFailedWithError:error];
	}
	
}


#pragma mark - start methods

- (void)initialize {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [self changeStateTo:CStoreKitStateInitialize];
        
        if (_productListURLString!=nil) {
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",_productListURLString]];
            
            __autoreleasing NSMutableURLRequest * theRequest = [[NSMutableURLRequest alloc]initWithURL:url];
            [theRequest setHTTPMethod:@"POST"];
            NSURLResponse * resp = nil;
            NSError * error = nil;
            NSData * response = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&resp error:&error];
            if (error == nil) {
                __autoreleasing NSString * json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
                
                NSDictionary *productData = [NSJSONSerialization JSONObjectWithData:response options:NSUTF8StringEncoding error:nil];
                
                NSArray * productArray = [productData objectForKey:@"sData"];
                for (NSDictionary * dict in productArray) {
                    NSDictionary * newDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [dict objectForKey:@"nPackageID"],@"productSerialId",
                                              [dict objectForKey:@"sPackageName"],@"productName",
                                              [dict objectForKey:@"fAmount"],@"price",
                                              [dict objectForKey:@"sProductID"],@"productId",
                                              [dict objectForKey:@"nCredits"],@"credits",
                                              nil];
                    [_products addObject:newDict];
                    
                }

            }else {
                [self changeStateTo:CStoreKitStateInitializeFailed];
                return;
            }
                        
        }
        
        if ([_products count] == 0) {
            [self changeStateTo:CStoreKitStateInitializeFailed];
            return;
        }
        
        NSMutableSet* possibleProducts = [NSMutableSet new];

        
        for (NSDictionary * dict in _products) {
            [possibleProducts addObject:[dict objectForKey:@"productId"]];
        }
        
        [self changeStateTo:CStoreKitStateInitialized];
        
        _productsRequest = nil;
        _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:possibleProducts];
        _productsRequest.delegate = self;
        [_productsRequest start];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
    });
}

- (void)startWithProducts:(NSArray *)products {
    switch (_state) {
        case CStoreKitStateInitialize:
        case CStoreKitStateInitialized:
            return;
            break;
        case CStoreKitStateReady:
            return;
        case CStoreKitStateInitializeFailed:
        case CStoreKitStateNone:
        default:
            break;
    }
    [self setProductList:products];
    _productListURLString = nil;
    [self performSelector:@selector(initialize)];
    
}

- (void)startWithProductsUrl:(NSString *)productUrl {
    switch (_state) {
        case CStoreKitStateInitialize:
        case CStoreKitStateInitialized:
            return;
            break;
        case CStoreKitStateReady:
            return;
        case CStoreKitStateInitializeFailed:
        case CStoreKitStateNone:
        default:
            break;
    }
    _productListURLString = [NSString stringWithFormat:@"%@",productUrl];
    [self performSelector:@selector(initialize)];
}


#pragma mark - initialize methods

+ (CStoreKit *)sharedInstance {
    
    if(storeKitObject == nil) {
        storeKitObject = [[CStoreKit alloc]init];
    }
    return storeKitObject;
}


//---------------------------------------------------------------------------------------------------------------

- (id)init {
	if (self = [super init]) {
        
        storeKitObject = self;
		_products = [[NSMutableArray alloc] init];
		_validProducts = [[NSMutableArray alloc] init];
		
	}
	return storeKitObject;
}



@end
