//
//  IAPHelper.m
//  DualVideo
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "IAPHelper.h"
#import <StoreKit/StoreKit.h>


@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>{
    
    SKProductsRequest * _productsRequest;
    RequestProductsCompletionHandler _completionHandler;
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;

    
}

@end

@implementation IAPHelper

//-----------------------------------------------------------------------

+(IAPHelper*)sharedInstance{
    
    static dispatch_once_t once;
    static IAPHelper * sharedInstance;
    
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      kFullAccessProductId,
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    
    return sharedInstance;
}

//-----------------------------------------------------------------------

- (BOOL)productPurchased:(NSString *)productIdentifier {
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

//-----------------------------------------------------------------------

- (void)buyProduct:(SKProduct *)product {
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

//-----------------------------------------------------------------------

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if ((self = [super init])) {
    
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
        // Check for previously purchased products
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
            } else {
            }
        }
        
        // Add self as transaction observer
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
    }
    return self;
    
}

//-----------------------------------------------------------------------

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {

    _completionHandler = [completionHandler copy];
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}

//-----------------------------------------------------------------------

#pragma mark - SKProductsRequestDelegate

//-----------------------------------------------------------------------

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
  
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
    
    for (SKProduct * skProduct in skProducts) {

    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    
}

//-----------------------------------------------------------------------

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

//-----------------------------------------------------------------------

#pragma mark SKPaymentTransactionOBserver

//-----------------------------------------------------------------------

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    

    
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

//-----------------------------------------------------------------------

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    

    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

//-----------------------------------------------------------------------

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {

    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

//-----------------------------------------------------------------------

- (void)failedTransaction:(SKPaymentTransaction *)transaction {

    if (transaction.error.code != SKErrorPaymentCancelled)
    {
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

//-----------------------------------------------------------------------

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IAPHelperProductPurchasedNotification" object:productIdentifier userInfo:nil];
    
}

//-----------------------------------------------------------------------

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

@end
