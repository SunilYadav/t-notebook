//
//  CStoreKit.h
//
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <StoreKit/StoreKit.h>

typedef enum {
    CStoreKitStateNone = 0,
    CStoreKitStateInitialize,
	CStoreKitStateInitialized,
	CStoreKitStateInitializeFailed,
	CStoreKitStateReady
}CStorekitState;

@protocol CStoreKitDelegate <NSObject>

@required

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase;
- (void)purchaseFailed:(NSString*)productId error:(NSString *)error;
- (void)purchaseCancelled:(NSString*)productId;

@optional

- (void)verificationFailed:(NSString*)productId;
- (void)verificationRetryOk:(NSString*)productId;
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue;
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error;

@end

@interface CStoreKit : NSObject <SKProductsRequestDelegate,SKPaymentTransactionObserver> {
    NSMutableArray * _products;
    NSMutableArray * _validProducts;
    SKProductsRequest * _productsRequest;
    CStorekitState _state;
    NSString* _productListURLString;
    id<CStoreKitDelegate>__unsafe_unretained _delegate;
}
@property (readonly) CStorekitState state;
@property (nonatomic,unsafe_unretained)id<CStoreKitDelegate>delegate;

+ (CStoreKit *)sharedInstance;

// start methods - call this methods from app delegate
- (void)startWithProducts:(NSArray *)products; //call this method if we have static product ids
- (void)startWithProductsUrl:(NSString *)productUrl; // call this method if the product ids are included in file on server.

// other methods
- (void)setProductList:(NSArray *)productList;
- (NSArray*)productList;
- (NSArray*)validProductList;

- (NSInteger)numberOfValidProducts;
- (NSInteger)numberOfInvalidProducts;
- (void)buy:(NSString *)productIdentifier delegate:(id<CStoreKitDelegate>)delegate;
- (void)restore:(id<CStoreKitDelegate>)delegate;

@end
