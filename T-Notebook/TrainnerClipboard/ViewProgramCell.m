//
//  ViewProgramCell.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "ViewProgramCell.h"
#import "DateUtility.h"

@implementation ViewProgramCell
- (IBAction)btnShowDetailClick:(id)sender {
}

- (void)awakeFromNib {
}


-(void)configCell :(NSDictionary *)detail {
    
    
    [self.lblProgramName setText:[detail valueForKey:@"sBlockTitle"]];
    [self.lblNotes setText:[detail valueForKey:@"sBlockNotes"]];
    NSString *tempdate = [NSString stringWithFormat:@"%@ %@",[self getDayFromDate:[detail valueForKey:@"dBlockdate"]],[detail valueForKey:@"dBlockdate"]];
    
    [self.lblDayDate setText:tempdate];
}

-(NSString *)getDayFromDate :(NSString *)date {
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:kAppDateFormat];
        NSDate *today = [formatter dateFromString:date];
    
    if ([today isEqual:nil] || !today)
    {
        return @"";
    }
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:today];
    NSInteger weekday = [comps weekday];
    
    switch (weekday) {
        case 1:
            return @"Sunday";
            break;
        case 2:
            return @"Monday";
            break;
        case 3:
            return @"Tuesday";
            break;
        case 4:
            return @"Wednesday";
            break;
        case 5:
            return @"Thursday";
            break;
        case 6:
            return @"Friday";
            break;
        case 7:
            return @"Saturday";
            break;
            
        default:
            break;
    }
    return nil;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
