//
//  ProgramGetDetailVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutoSuggestViewController.h"

@protocol NewExerciseDelegate
-(void) addNewSheet:(NSDictionary *)detailsExercise;
-(void) editNewLine:(NSDictionary *)detailsExercise;
@end

@interface ProgramGetDetailVC : BaseViewController <AutoSuggestViewDelegate>
@property (nonatomic, unsafe_unretained) id<NewExerciseDelegate>			delegate;
@property (nonatomic, strong) NSDictionary *dictLineToShow;
@property (nonatomic, strong) NSString *strRowIdtoReload;
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
@end
