//
//  AppDelegate.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//



// Sunil Get
#import "AppController.h"
#import "NavigationViewController.h"

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <MediaPlayer/MediaPlayer.h>


@interface UINavigationController (KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal;

@end

@interface AppDelegate : AppController <UIApplicationDelegate>
{
    Reachability                                          * hostReach;
    Reachability                                          * internetReach;
    Reachability                                          * wifiReach;
    BOOL                                                    isReachable;
    
    
   
}


//SUNIL property for check the internet in app
@property (nonatomic, assign)   BOOL        isNetworkAvailable;


@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) UINavigationController *nav;

@property (nonatomic) BOOL                                                      isFieldUpdated;
@property (nonatomic) BOOL                                                      isAutoSave;
@property (nonatomic) BOOL                                                      isPause;



@property (nonatomic) NSInteger                                                       intRemaningTimeForPhase;
@property (nonatomic, strong) NSNumber                                          * seconds_InterVal;

@property (nonatomic, strong) NSNumber                                          * remainTimeInterVal_Timer;
@property (nonatomic, strong) NSNumber                                          * actualTimeInterVal_Timer;
@property (nonatomic, strong) NSString                                          * strCurrentTime_Timer;
@property (nonatomic) BOOL                                                      isFromBackground;
@property (nonatomic, strong) NSMutableArray                                    * arrPlayers;
@property (nonatomic) NSInteger                                                 intRemainingTimeOfPhase;

@property (nonatomic , strong) MPMediaPlaylist                                  * playList;
@property (nonatomic , strong) MPMediaPlaylist                                  * playListForIpod;
@property (nonatomic , strong)  MPMusicPlayerController                         * appPlayer;
@property (nonatomic , strong)  MPMusicPlayerController                         * ipodPlayer;

@property (nonatomic) BOOL                                                        appIsTimerRunning;
@property (nonatomic) BOOL                                                        appIsTimerPaused;
@property (nonatomic) BOOL                                                        IsPreviousVersion;
@property (nonatomic , strong) NSTimer                                       *appTimerInterVal;
@property (nonatomic , strong)  UIAlertView                                 * progressAlert;
@property (nonatomic , strong) UIActivityIndicatorView					* activityIndicator;

@property (nonatomic , strong) UIAlertView                                  *alertView;
@property (nonatomic , strong) NSString                                     *isPausePlayer;
@property (nonatomic , strong) NSString                                     *isFromPlayList;
@property (nonatomic , strong) NSString                                     *isiPadPlaying;
@property (nonatomic , strong) NSString                                     *isAppPlayerPlaying;
@property (nonatomic , strong) NSString                                     *isIpodPlaying;

@property (nonatomic , strong) NSString                                     *isFirstTime;

- (NSString *) getStringInTimeFormat : (NSNumber *) timeInterVal;
- (void) playSound : (int) intSoundFile;
-(void)makeRootView:(UIViewController *)vc ;
- (NSString *) getDBPath;
- (void) copyDatabaseIfNeeded;
-(void)pausedTimer;
- (void) getRemainTime_Timer;
- (void) playMusic;
- (void) stopMusic;
- (void) pauseMusic;
- (void) hideProgressAlert;
- (void) showProgressAlert:(NSString *)alertMessage;
- (void)showAlert:(NSString *)alertMessage;
- (void)hideAlertView;
- (NSString *) checkForNull:(NSString *)strVal;
- (BOOL)isNotEmptyArray:(NSArray *)array;
- (CGRect)getWidth:(NSString *)strData font:(UIFont *)font height:(CGFloat)height;

+ (AppDelegate *) sharedInstance;
- (MPMusicPlayerController *)applicationMusicPlayer;
- (MPMusicPlayerController *)ipodMusicPlayer;

@end

