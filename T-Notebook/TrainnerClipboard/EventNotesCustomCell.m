//
//  EventNotesCustomCell.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2011 WLI All rights reserved.
//

#import "EventNotesCustomCell.h"


@implementation EventNotesCustomCell

@synthesize lblDate = _lblDate;
@synthesize lblCName = _lblCName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		
		UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(-10, 0, 600, 40)];
		img.image = [UIImage imageNamed:@""];
		
		_lblCName = [[UILabel alloc] initWithFrame:CGRectMake(16, 7, 160, 40)];
		[_lblCName setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:14]];
		[_lblCName setBackgroundColor:[UIColor clearColor]];
		[_lblCName setTextAlignment:NSTextAlignmentLeft];
        [_lblCName setLineBreakMode:NSLineBreakByTruncatingTail];
        [_lblCName setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];

		[self addSubview:_lblCName];
        _lblDate1 = [[UILabel alloc] initWithFrame:CGRectMake(165, 4, 100, 40)];
        [_lblDate1 setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:14]];
        [_lblDate1 setTextAlignment:NSTextAlignmentRight];
        [self.lblDate1 setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        [_lblDate1 setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];
        [self addSubview:_lblDate1];
        
		_lblDate = [[UILabel alloc] initWithFrame:CGRectMake(180, 4, 155, 40)];
		[_lblDate setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:14]];
		[_lblDate setTextAlignment:NSTextAlignmentRight];
		[self.lblDate setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        [_lblDate setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];

		[self addSubview:_lblDate];
	}
    return self;
}


//Sunil -> weekly cell text 
- (void) displayTableData :(NSDictionary *) dictInfo {

    self.lblDate.hidden = false;
    self.lblDate1.hidden = false;
	if ([[dictInfo objectForKey:@"type"] isEqualToString:@"1"]) {
        
        // Events
		NSDateFormatter * dt = [[NSDateFormatter alloc] init];
		
		if ([[dictInfo objectForKey:@"isDaily"] isEqualToString:@"1"]) {
			[dt setDateFormat:@"@ hh:mm a"];
             self.lblDate.hidden = true;
		}
		else {
			[dt setDateFormat:@"MM/dd/yy @ hh:mm a"];
            self.lblDate1.hidden = true;
		}
		
		NSString * strDt = [dt stringFromDate:[dictInfo objectForKey:@"date"]];
		strDt = [strDt lowercaseString];
		self.lblCName.text = [dictInfo objectForKey:@"title"];
		self.lblDate.text = strDt;
        self.lblDate1.text = [dictInfo objectForKey:@"evntDate"];
        
        
	} else {
		NSDateFormatter * dt = [[NSDateFormatter alloc] init];
		[dt setDateFormat:@"MM/dd/yy @ hh:mm:ss a"];
		NSDate * date = [dt dateFromString:[dictInfo objectForKey:@"date"]];
		[dt setDateFormat:@"MM/dd/yy @ hh:mm a"];
		NSString * strDt = [dt stringFromDate:date];
		self.lblCName.text = [NSString stringWithFormat:@"%@ %@",[dictInfo objectForKey:@"notes"],[dictInfo objectForKey:@"title"]];
		self.lblDate.text = strDt;
        self.lblDate.hidden = false;
        self.lblDate1.hidden = false;
	}
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
}



@end
