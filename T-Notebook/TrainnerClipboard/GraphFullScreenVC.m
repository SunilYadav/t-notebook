//
//  GraphFullScreenVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Show Graph in fill screen


#import "GraphFullScreenVC.h"
#import "FileUtility.h"
#import "SignatureVC.h"
#import "ParQquestionView.h"
#import "ClientViewController.h"


@interface GraphFullScreenVC (){
    UIImageView *imageViewSignature;
    UIButton * btnSignatureView;
}

@property (nonatomic , strong) UIScrollView     *scrollView;
@property (nonatomic , strong) UIView              *scrolContentvew;

@end

@implementation GraphFullScreenVC

int contentTagW = 9000;
//-----------------------------------------------------------------------

#pragma mark- Custom Methods

//-----------------------------------------------------------------------

- (NSString* ) stringFromArray :(NSArray *) array {
    NSMutableString * string = [[NSMutableString alloc] init];
    [string appendString:@"["];
    
    
    for (int i = 0; i<[array count]; i++) {
        if ([[array objectAtIndex:i] isKindOfClass:[NSArray class]] || [[array objectAtIndex:i] isKindOfClass:[NSMutableArray class]]) {
            if ([[array objectAtIndex:i] count] > 0) {
                if ([string isEqualToString:@"["]) {
                    
                    for (int j = 0; j<[[array objectAtIndex:i] count]; j++) {
                        if (j == 0) {
                            if (j == [[array objectAtIndex:i] count] - 1) {
                                [string appendString:[NSString stringWithFormat:@"['0','%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            } else {
                                [string appendString:[NSString stringWithFormat:@"['0','%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            }
                        } else if (j == [[array objectAtIndex:i] count] - 1) {
                            [string appendString:[NSString stringWithFormat:@",'%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        } else {
                            [string appendString:[NSString stringWithFormat:@",'%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        }
                    }
                } else {
                    for (int j = 0; j<[[array objectAtIndex:i] count]; j++) {
                        if (j == 0) {
                            if (j == [[array objectAtIndex:i] count]-1) {
                                [string appendString:[NSString stringWithFormat:@",['0','%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            } else {
                                [string appendString:[NSString stringWithFormat:@",['0','%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            }
                        } else if (j == [[array objectAtIndex:i] count]-1) {
                            [string appendString:[NSString stringWithFormat:@",'%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        } else {
                            [string appendString:[NSString stringWithFormat:@",'%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        }
                    }
                }
            }
            
        } else {
            if ([[array objectAtIndex:i] length] > 0) {
                if ([string isEqualToString:@"["]) {
                    [string appendString:[NSString stringWithFormat:@"'0','%.2f'",[[array objectAtIndex:i] floatValue]]];
                } else {
                    [string appendString:[NSString stringWithFormat:@",'%.2f'",[[array objectAtIndex:i] floatValue]]];
                }
            }
        }
    }
    
    [string appendString:@"]"];
    return (NSString *) string;
}

//-----------------------------------------------------------------------

-(void) displayGraphView {
    
    NSArray *viewSubviews = [self.view subviews];
    [viewSubviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[UIWebView class]]) {
            [obj removeFromSuperview];
        }
    }];
    
    
    NSArray * weightArr;
    if ([self.chartTag intValue] == 11) {
        weightArr = [[DataManager initDB] getClientsWeightHeight:self.strClientID];
    }
    else if ([self.chartTag intValue] == 12) {
        weightArr = [[DataManager initDB] getClientsBodyFatPercentage:self.strClientID];
    }
    else if ([self.chartTag intValue] == 13) {
        weightArr = [[DataManager initDB] getClientscircumferenceMeasurement:self.strClientID];
    }
    else if ([self.chartTag intValue] == 14) {
        weightArr = [[DataManager initDB] getClientsBMIResult:self.strClientID];
    }
    else {
        weightArr = [[DataManager initDB] getClientsGeneralBodyFatPercentage:self.strClientID];
    }
    
    MESSAGE(@"Wieght Array : %@, %ld",weightArr, weightArr.count);
    
    
    NSArray * dataArray;
    if ([self.chartTag intValue] == 11) {
        dataArray = [NSArray arrayWithObjects:weightArr,nil];
    }
    else if ([self.chartTag intValue] == 12) {
        dataArray = [NSArray arrayWithObjects:[weightArr objectAtIndex:0],[weightArr objectAtIndex:1],[weightArr objectAtIndex:2],nil];
    }
    else if ([self.chartTag intValue] == 13) {
        dataArray = [NSArray arrayWithObjects:[weightArr objectAtIndex:0],[weightArr objectAtIndex:1],[weightArr objectAtIndex:2],[weightArr objectAtIndex:3],[weightArr objectAtIndex:4],[weightArr objectAtIndex:5],[weightArr objectAtIndex:6],nil];
    }
    else  if ([self.chartTag intValue] == 14) {
        dataArray = [NSArray arrayWithObjects:[weightArr objectAtIndex:0],[weightArr objectAtIndex:1],nil];
    }
    else{
            dataArray = [NSArray arrayWithObjects:weightArr,nil];
    }
    
    

    //For Ingraph values
//    NSString *strIngraphArray = [self getArrayofIngrapghLabel:dataArray];
    NSString * dataStr = [self stringFromArray:dataArray];
    
    int height = 0;
    int width = 0;
    
    
    if(self.isLandscapeMode) {
        [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, self.view.frame.size.height)];
        

    } else {
        [self.view setFrame:CGRectMake(0,0, kPotraitWidth, self.view.frame.size.height)];
       
    }
    
    height = 500;
    width = 500;
    

//Set web view frame
    UIWebView * _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 500, 500)];
    
    
    for (id subview in _webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:JQueryFileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];

    
    _webView.center = self.view.center;
    BOOL bIsNegativeVal = FALSE;
    for (int i = 0; i < [dataArray count]; i++) {
        for (int j = 0; j < [[dataArray objectAtIndex:i] count]; j++){
            if ([[[dataArray objectAtIndex:i] objectAtIndex:j] integerValue] < 0) {
                bIsNegativeVal = TRUE;
            }
        }
    }
    
    if (bIsNegativeVal) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"center"];
    }else{
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"bottom"];
    }
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#XDATA#" withString:self.strAxies];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#DATA#" withString:dataStr];
    
    //For Ingraph values
//    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#INGRAPHLABELDATA#" withString:strIngraphArray];
    
    
//SUNIl Set fill graph for only for one value 
     if (weightArr.count==1 ){
      htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"true"];
    }
     else {
         htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"false"];

     }
    
    htmlString = [self setGraphLioneColor:htmlString];;
    
    MESSAGE(@"htmlString-> graph : %@",htmlString);
    [_webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView scalesPageToFit];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    
    //Sunil Web view for show Graph in web view
    [self.view addSubview:_webView];
    
    
}

- (CGRect)getSizeOfLabel:(NSAttributedString *)strData font:(UIFont *)font {

    CGRect rect;
    if(self.isLandscapeMode) {
         rect = [strData boundingRectWithSize:CGSizeMake(kLandscapeWidth - 50, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    } else {
        rect = [strData boundingRectWithSize:CGSizeMake(738, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    }
    return rect;
}

//-----------------------------------------------------------------------

- (CGRect)getWidth:(NSString *)strData font:(UIFont *)font {
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:strData];
    
    CGRect rect;
    rect = [attributedString boundingRectWithSize:CGSizeMake(FLT_MAX,21) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return rect;
}

//-----------------------------------------------------------------------

-(void)setupWaiverScreenLayout {
    
    NSArray *contentArray = [self.view subviews];
    if([contentArray count] > 0) {
        [contentArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [obj removeFromSuperview];
        }];
    }
    
   
    
    if(self.isLandscapeMode) {
      _scrollView  = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight-90)];
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundImage_iPad"]]];
    } else {
       _scrollView  = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 758, 959-20)];
       [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundImage"]]];
    }
    self.scrollView.opaque = YES;
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView setTag:contentTagW];
    contentTagW++;
    
    UITextView *txtView;
    if(self.isLandscapeMode) {
     txtView = [[UITextView alloc]initWithFrame:CGRectMake(15, 0, kLandscapeWidth - 50,kLandscapeHeight + 540 + 300)];
    } else {
      txtView = [[UITextView alloc]initWithFrame:CGRectMake(15, 0,738, 1024+650 + 300)];
    }
    
    txtView.editable = false;
    txtView.backgroundColor = [UIColor clearColor];
    [txtView setTag:contentTagW];
    contentTagW ++;
   
    
    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc]initWithString:@""];
    
    NSAttributedString *titleString = [[NSAttributedString alloc]initWithString:NSLocalizedString(@"ASSUMPTION",nil) attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:22]}];
    [muAtrStr appendAttributedString:titleString];
    
    NSString *strTempDeclare = @"DECLARATIONS: This Agreement is entered into between personal trainer ________________ ";
    
    strTempDeclare = NSLocalizedString(@"strTempDeclare", nil);
    
    NSAttributedString *string = [[NSAttributedString alloc]initWithString:[[NSUserDefaults standardUserDefaults] valueForKey:kTrainerName] attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:20]}];
    
    strTempDeclare = [strTempDeclare stringByReplacingOccurrencesOfString:@"____________________" withString:@""];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:strTempDeclare attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:20]}];
    
    [str appendAttributedString:string];
    
    [muAtrStr appendAttributedString:str];
    
    NSString *strTemp =  @"DECLARATIONS: This Agreement is entered into between personal trainer ________________ (“Trainer”) and the undersigned (“Client”). The provision of personal training services by Trainer to Client, and Client’s use of any premises, facilities or equipment are contingent upon this Agreement.ASSUMPTION OF RISK: You agree that if you engage in any physical exercise or activity, including personal training, or enter our premises or use any facility or equipment on our premises for any purpose, you do so at your own risk and assume the risk of any and all injury and/or damage you may suffer, whether while engaging in physical exercise or not. This includes injury or damage sustained while and/or resulting from using any premises or facility, or using any equipment, whether provided to you by Trainer or otherwise, including injuries or damages arising out of the negligence of Trainer, whether active or passive, or any of Trainer’s affiliates, employees, agents, representatives, successors, and assigns. Your assumption of risk includes, but is not limited to, your use of any exercise equipment (mechanical or otherwise), sports fields, courts, or other areas, locker rooms, sidewalks, parking lots, stairs, pools, whirlpools, saunas, steam rooms, lobby or other general areas of any facilities, or any equipment. You assume the risk of your participation in any activity, class, program, instruction, or event, including but not limited to weightlifting, walking, jogging, running, aerobic activities, aquatic activities, tennis, basketball, volleyball, racquetball, or any other sporting or recreational endeavor. You agree that you are voluntarily participating in the aforementioned activities and assume all risk of injury, illness, damage, or loss to you or your property that might result, including, without limitation, any loss or theft of any personal property, whether arising out of the negligence of Trainer or otherwise.\n\nRELEASE: You agree on behalf of yourself (and all your personal representatives, heirs, executors, administrators, agents, and assigns) to release and discharge Trainer (and Trainer’s affiliates, related entities, employees, agents, representatives, successors, and assigns) from any and all claims or causes of action (known or unknown) arising out of the negligence of Trainer, whether active or passive, or any of Trainer’s affiliates, employees, agents, representatives, successors, and assigns. This waiver and release of liability includes, without limitation, injuries which may occur as a result of (a) your use of any exercise equipment or facilities which may malfunction or break, (b) improper maintenance of any exercise equipment, premises or facilities, (c) negligent instruction or supervision, including personal training, (d) negligent hiring or retention of employees, and/or (e) slipping or tripping and falling while on any portion of a premises or while traveling to or from personal training, including injuries resulting from Trainer’s or anyone else’s negligent inspection or maintenance of the facility or premises.\n\nINDEMNIFICATION: By execution of this agreement, you hereby agree to indemnify and hold harmless Trainer from any loss, liability, damage, or cost Trainer may incur due to the provision of personal training by Trainer to you. ACKNOWLEDGMENTS: You expressly agree that the foregoing release, waiver, assumption of risk and indemnity agreement is intended to be as broad and inclusive,and that if any portion thereof is held invalid, it is agreed that the balance shall,notwithstanding, continue in full legal force and effect. You acknowledge that Trainer offers a service to his/her clients encompassing the entire recreational and/or fitness spectrum. Trainer is not in the business of selling weightlifting equipment, exercise equipment, or other such products to the public, and the use of such items is incidental to the service provided by Trainer. You acknowledge and agree that Trainer does not place such items into the stream of commerce. This release is not intended as an attempted release of claims of gross negligence or intentional acts.\n\n";
    strTemp = NSLocalizedString(@"DECLARATIONS", nil);
    
    
    //NSAttributedString *boldStr = [[NSAttributedString alloc]initWithString:strBold attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Semibold" size:20]}];
    
    NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:strTemp attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:20]}];
    [muAtrStr appendAttributedString:atrStr];
    
    
    NSString *strBold =  @"\n\nYou acknowledge that you have carefully read this waiver and release and fully understand that it is a release of liability, express assumption of risk and indemnity agreement. You are aware and agree that by executing this waiver and release, you are giving up your right to bring a legal action or assert a claim against trainer for trainer’s negligence, or for any defective product used while receiving personal training from trainer. You have read and voluntarily signed the waiver and release and further agree that no oral representations, statements, or inducement apart from the foregoing written agreement have been made.\n\n";
    strBold = NSLocalizedString(@"ACKNOLEDGEMENT", nil);
    NSAttributedString *boldStr = [[NSAttributedString alloc]initWithString:strBold attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Semibold" size:20]}];
    [muAtrStr appendAttributedString:boldStr];
    
    NSString *strBottom;
    if(self.isLandscapeMode) {
      
        strBottom =  @" Date: _____________                      Print Name: __________________                Sign Name: ____________";
        strBottom = NSLocalizedString(@"DateLandscape", nil);
    } else {
        strBottom =  @" Date: _________ Print Name: __________________ Sign Name: ____________";
        strBottom = NSLocalizedString(@"DatePortrait", nil);
    }
    
    
    
    txtView.userInteractionEnabled = NO;
    txtView.backgroundColor = [UIColor clearColor];
    
    
    
    if(self.isLandscapeMode) {
      imageViewSignature = [[UIImageView alloc]initWithFrame:CGRectMake(610+230,self.scrollView.contentSize.height-150-56-25, 100, 50)]; //
    } else {
      imageViewSignature = [[UIImageView alloc]initWithFrame:CGRectMake(610, self.scrollView.contentSize.height-150-40-25, 100, 50)];
    }
    
    if (self.imgSignature) {
        imageViewSignature.image = self.imgSignature;
        [imageViewSignature setBackgroundColor:[UIColor clearColor]];
    }
    [imageViewSignature setTag:contentTagW];
    contentTagW ++;
    //[self.scrollView addSubview:imageViewSignature];
    
    
    UILabel *lblDate;
    if(self.isLandscapeMode) {
      lblDate = [[UILabel alloc]initWithFrame:CGRectMake(73+ 10, self.scrollView.contentSize.height-150-45-25-200, 105, 21)]; //-150-860   //-173
    } else {
       lblDate = [[UILabel alloc]initWithFrame:CGRectMake(73, self.scrollView.contentSize.height-173-25-200, 105, 21)];
    }
    
    lblDate.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
    lblDate.backgroundColor = [UIColor clearColor];
    lblDate.textAlignment = NSTextAlignmentCenter;
    [lblDate setTag:contentTagW];
    contentTagW++;
    //[self.scrollView addSubview:lblDate];
    
    NSDateFormatter  *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:kAppDateFormat];
    lblDate.text = [formatter stringFromDate:[NSDate date]];
    
    UILabel *lblName;
    if(self.isLandscapeMode) {
         lblName = [[UILabel alloc]initWithFrame:CGRectMake(73+215+120,self.scrollView.contentSize.height-150-45-25, 205, 21)];
    } else {
         lblName = [[UILabel alloc]initWithFrame:CGRectMake(73+215, self.scrollView.contentSize.height-173-25, 205, 21)];
    }
    
   
    lblName.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textAlignment = NSTextAlignmentCenter;
   // [self.scrollView addSubview:lblName];
    [lblName setTag:contentTagW];
    contentTagW ++;
    lblName.text = self.strName;
    
    UILabel *lblTrainerName;
    if(self.isLandscapeMode) {
       lblTrainerName = [[UILabel alloc]initWithFrame:CGRectMake(20 + 650,80, 180, 21)];
    } else {
       lblTrainerName = [[UILabel alloc]initWithFrame:CGRectMake(20, 110, 180, 21)];
    }
    [lblTrainerName setTag:contentTagW];
    contentTagW ++;
    lblTrainerName.font = [UIFont fontWithName:@"ProximaNova-Regular" size:20];
    //lblTrainerName.backgroundColor = [UIColor clearColor];
    lblTrainerName.textAlignment = NSTextAlignmentCenter;
  //  [self.scrollView addSubview:lblTrainerName];
    lblTrainerName.text = [[NSUserDefaults standardUserDefaults] valueForKey:kTrainerName];
    
    
    
   
    
    UILabel *lblDateTitle = [[UILabel alloc] init];
    [lblDateTitle setText:NSLocalizedString(@"Date", nil)];
    
    UILabel *lblPrintTitle = [[UILabel alloc] init];
    [lblPrintTitle setText:NSLocalizedString(@"Print", nil)];
    
    UILabel *lblSignTitle = [[UILabel alloc] init];
    [lblSignTitle setText:NSLocalizedString(@"Sign", nil)];
    
    if(self.isLandscapeMode) {
        
        [lblDateTitle setFrame:CGRectMake(15,txtView.frame.size.height, 500, 21)];
        [lblPrintTitle setFrame:CGRectMake(15,lblDateTitle.frame.origin.y + lblDateTitle.frame.size.height, 500, 21)];
        [lblSignTitle setFrame:CGRectMake(15,lblPrintTitle.frame.origin.y + lblPrintTitle.frame.size.height, 500, 21)];
    } else {
        [lblDateTitle setFrame:CGRectMake(15, txtView.frame.size.height, 500, 21)];
        [lblPrintTitle setFrame:CGRectMake(15,lblDateTitle.frame.origin.y + lblDateTitle.frame.size.height, 500, 21)];
        [lblSignTitle setFrame:CGRectMake(15,lblPrintTitle.frame.origin.y + lblPrintTitle.frame.size.height, 500, 21)];
    }
    
    NSAttributedString *strlblName = [[NSAttributedString alloc]initWithString:lblName.text attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)}];
    
    [lblName setAttributedText:strlblName];
    

    
    
    
    NSString *strBottomDate, *strBottomPrint, *strBottomSign;
    strBottomDate = NSLocalizedString(@"Date", nil);
    strBottomPrint = NSLocalizedString(@"Print", nil);
    strBottomSign = NSLocalizedString(@"Sign", nil);
    
    strBottomDate = [strBottomDate stringByReplacingOccurrencesOfString:@"_____________" withString:@""];
    strBottomPrint = [strBottomPrint stringByReplacingOccurrencesOfString:@"_____________" withString:@""];
    strBottomSign = [strBottomSign stringByReplacingOccurrencesOfString:@"_____________" withString:@""];
    
    NSMutableAttributedString *strABottom = [[NSMutableAttributedString alloc] initWithString:strBottomDate attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:22]}];
    
    NSAttributedString *btmStr = [[NSAttributedString alloc]initWithString:lblDate.text attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:22]}];
    
    [strABottom appendAttributedString:btmStr];
    
    [muAtrStr appendAttributedString:strABottom];
    
    //----------------------------------------------------------------------------------------
    strABottom = [[NSMutableAttributedString alloc]initWithString:strBottomPrint attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:22]}];
    btmStr = [[NSAttributedString alloc]initWithString:lblName.text attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:22]}];
    
    [strABottom appendAttributedString:btmStr];
    
    [muAtrStr appendAttributedString:strABottom];
    //----------------------------------------------------------------------------------------
    strABottom = [[NSMutableAttributedString alloc]initWithString:strBottomSign attributes:@{NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:22]}];
//    btmStr = [[NSAttributedString alloc]initWithString:lblName.text attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)}];
    
    //[strABottom appendAttributedString:btmStr];
    
    
    
    [muAtrStr appendAttributedString:strABottom];
    
    
    [txtView setAttributedText:muAtrStr];
    
    CGRect rectOftxtView = [self getSizeOfLabel:muAtrStr font:nil];
    //[txtView setAttributedText:muAtrStr];
    
    if(self.isLandscapeMode) {
        [txtView setFrame:CGRectMake(15, 0, kLandscapeWidth - 50,rectOftxtView.size.height + 100)];
    } else {
        [txtView setFrame:CGRectMake(15, 0,738, rectOftxtView.size.height + 100)];
    }
    
    [self.scrollView addSubview:txtView];
    if(self.isLandscapeMode) {
        self.scrollView.contentSize = CGSizeMake(kLandscapeWidth, txtView.frame.size.height + 300); //1775+40
    } else {
        self.scrollView.contentSize = CGSizeMake(748, txtView.frame.size.height + 300);
    }
    
    [imageViewSignature setFrame:CGRectMake(150,txtView.frame.size.height - 90, 100, 50)];
    [self.scrollView addSubview:imageViewSignature];
    
   // [self.scrollView addSubview:lblDateTitle];
  //  [self.scrollView addSubview:lblPrintTitle];
    //[self.scrollView addSubview:lblSignTitle];
    
    
    [lblDate setFrame:CGRectMake(lblDateTitle.frame.origin.x + 15,lblDateTitle.frame.origin.y, 205, 21)];
    [lblName setFrame:CGRectMake((lblPrintTitle.frame.size.width / 2) - 150,lblPrintTitle.frame.origin.y, 205, 21)];
  //  [imageViewSignature setFrame:CGRectMake((lblSignTitle.frame.size.width / 2) - 150,lblSignTitle.frame.origin.y, 100, 50)];
    
   // [self.scrollView addSubview:lblDate];
   // [self.scrollView addSubview:lblName];
   // [self.scrollView addSubview:imageViewSignature];
    
    btnSignatureView = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if(self.isLandscapeMode) {
      btnSignatureView.frame = CGRectMake(236 + 130,self.scrollView.contentSize.height - 150, 300, 100); //self.scrollView.contentSize.height-173
    } else {
       btnSignatureView.frame = CGRectMake(236,self.scrollView.contentSize.height - 150, 300, 100);
    }
    
    btnSignatureView.backgroundColor = [UIColor whiteColor];
    btnSignatureView.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    btnSignatureView.layer.borderWidth = 1.0;
    [btnSignatureView setTag:contentTagW];
    contentTagW ++;
    [btnSignatureView addTarget:self action:@selector(openSignatureView) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:btnSignatureView];
    
    if (self.imgSignature) {
        [btnSignatureView setImage:self.imgSignature forState:UIControlStateNormal];
    }
   // txtView.backgroundColor = [UIColor clearColor];
    
    if(self.isLandscapeMode) {
        _scrolContentvew = [[UIView alloc]initWithFrame:CGRectMake(10, 55+ 20, kLandscapeWidth - 20, kLandscapeHeight - 90)]; // 768 , 969
 
    } else {
        _scrolContentvew = [[UIView alloc]initWithFrame:CGRectMake(10, 55 + 20, 748, 959 - 20)]; // 768 , 969
  
    }
    [self.scrolContentvew setBackgroundColor:[UIColor whiteColor]];
    [self.scrolContentvew setTag:contentTagW];
    self.scrolContentvew.layer.cornerRadius = 10;
    self.scrolContentvew.alpha = 0.699999988079071;
    [self.scrolContentvew addSubview:self.scrollView];
    [self.view addSubview:self.scrolContentvew];
    
    self.navigationItem.title = NSLocalizedString(@"Terms And Conditions", nil);
    
}




//---------------------------------------------------------------------------------------------------------------

- (IBAction) doneBtnClicked:(id) sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}




-(IBAction)exportWeiverBtnTapped:(id)sender {
    
    
    NSArray *viewContent = [self.scrolContentvew subviews];
    // DLOG(@"Content views %@",viewContent);
  
    CGRect scrolOrgFrame = self.scrollView.frame;
    CGRect scrolContentFrame = self.scrolContentvew.frame;
    self.scrollView.contentOffset = CGPointZero;
    self.scrollView.frame = CGRectMake(0, 0, self.scrollView.contentSize.width, self.scrollView.contentSize.height);
    [self.scrollView.layer setBorderWidth:0];
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    [self.scrollView setOpaque:YES];
   
    CGRect newFrame = self.scrolContentvew.frame;
    newFrame.size.height = self.scrollView.frame.size.height;
    self.scrolContentvew.frame = newFrame;
    self.scrolContentvew.alpha = 1;

    //========================================================
    
    UIGraphicsBeginImageContext(self.scrolContentvew.frame.size);
    
    [self.scrolContentvew.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *screenImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSData *data = UIImageJPEGRepresentation(screenImg, 1.0);
  
    
    //========================================================
    
    self.scrollView.frame = scrolOrgFrame;
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrolContentvew setFrame:scrolContentFrame];
     self.scrolContentvew.alpha = 0.699999988079071;
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController * mailComposer = [[MFMailComposeViewController alloc] init];
        NSArray *recep = [NSArray arrayWithObject:self.strEmail];
        [mailComposer setToRecipients:recep];
        
        [mailComposer setSubject:@"Client waiver"];
        [mailComposer addAttachmentData:data mimeType:@"image/png" fileName:@"TermsAndConditions.png"];
        
        if (IOS7) {
            [mailComposer.navigationBar setTintColor:[UIColor grayColor]];
        }
        [mailComposer setMessageBody:@"" isHTML:YES];
        [mailComposer setMailComposeDelegate:self];
        [self.navigationController presentViewController:mailComposer animated:YES completion:^{
        }];
        
    } else {
        DisplayAlertWithTitle(NSLocalizedString(@"Please Configure Email", nil), kAppName) ;
        return;
    }

}

//-----------------------------------------------------------------------

#pragma mark - orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return TRUE;
}



//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
   
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        self.isLandscapeMode = TRUE;
    } else {
        self.isLandscapeMode = FALSE;
    }
    
    if(self.isFromTermsAndConditions) {
       [self setupWaiverScreenLayout];
    } else{
         [self displayGraphView];
    }
    
}

//-----------------------------------------------------------------------

#pragma mark
#pragma mark  MFMailComposer Delegate Method

//-----------------------------------------------------------------------

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultSent:
                MESSAGE(@"Sent--->");
            DisplayLocalizedAlertWithTitle(NSLocalizedString(@"EmailSent", nil), kAppName);
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultSaved:
                MESSAGE(@"Sent--->");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultCancelled:
                MESSAGE(@"Sent--->");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultFailed:
                MESSAGE(@"Sent--->");
            DisplayLocalizedAlertWithTitle(NSLocalizedString(@"EmailSendError", nil), kAppName);
            break;
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------------


#pragma mark Orientation Methods

//-----------------------------------------------------------------------

//#if defined(__IPHONE_6_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0
//
//-(BOOL)shouldAutorotate {
//    return YES;
//}
//
////-----------------------------------------------------------------------
//
//-(NSUInteger)supportedInterfaceOrientations {
//    return  UIInterfaceOrientationMaskAll;
//}
//
//#endif
//
////-----------------------------------------------------------------------
//
//- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return YES;
//}

//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    NSArray * arraySubView = [self.view subviews];
//    if ([arraySubView count] != 0) {
//        for (int i = 0; i<[arraySubView count]; i++) {
//            UIView * v2 = (UIView *)[arraySubView objectAtIndex:i];
//            
//            NSString * str = [NSString stringWithFormat:@"%@",[v2 class]];
//            
//            if ([str isEqualToString:@"TKGraphView"]) {
//                [v2 removeFromSuperview];
//            } else if([str isEqualToString:@"UIWebView"]) {
//                [v2 removeFromSuperview];
//            }
//        }
//    }
//    if (toInterfaceOrientation==UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
//        appDelegate.isLandscape = NO;
//    }
//    else {
//        appDelegate.isLandscape = YES;
//    }
//    [self displayGraphView];
//}

//-----------------------------------------------------------------------

-(BOOL)prefersStatusBarHidden
{
    if(self.isFromTermsAndConditions) {
       return NO;
    } else {
        return FALSE;
    }
    
}





//-----------------------------------------------------------------------

#pragma mark - View lifeCycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    START_METHOD
    [super viewDidLoad];
    
    if (self.isFromTermsAndConditions) {
        if(self.isLandscapeMode) {
            [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, self.view.frame.size.height)];
        } else {
            [self.view setFrame:CGRectMake(0,0, kPotraitWidth, self.view.frame.size.height)];
        }
        [self setupWaiverScreenLayout];
        
    } else{
        
        if(self.isLandscapeMode) {
            [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, self.view.frame.size.height)];
        } else {
            [self.view setFrame:CGRectMake(0,0, kPotraitWidth, self.view.frame.size.height)];
        }


        if ([self.chartTag intValue] == 11){
            
            self.navigationItem.title =  NSLocalizedString(@"Weight Tracker", nil);
        }
        else if ([self.chartTag intValue] == 12){
            self.navigationItem.title = NSLocalizedString(@"Body Fat Tracker", nil);
        }
        else if ([self.chartTag intValue] == 13){
            self.navigationItem.title =  NSLocalizedString(@"Circumference Measurement", nil);
        }
        else  if ([self.chartTag intValue] == 14) {
            self.navigationItem.title = NSLocalizedString(@"BMI Tracker", nil);
        }
        else{
            self.navigationItem.title = NSLocalizedString(@"General Body Fat Tracker", nil);
        }
        
        [self displayGraphView];
    }
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: kAppTintColor,NSFontAttributeName : [UIFont fontWithName:@"ProximaNova-Regular" size:23.0]};
    self.navigationController.navigationBarHidden = NO;
    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(doneBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setTag:201];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -15;
    UIBarButtonItem *leftBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    
    
    if(self.isFromTermsAndConditions) {
        UIButton *btnExport = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnExport addTarget:self action:@selector(exportWeiverBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        [btnExport setImage:[UIImage imageNamed:@"Export"] forState:UIControlStateNormal];
        [btnExport setTag:202];
        [btnExport setFrame:CGRectMake(0, 5, 34,34)];
        UIBarButtonItem *expBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnExport];
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,leftBtn,expBtn, nil] animated:NO];

    } else {
         [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,leftBtn,nil] animated:NO];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        self.isLandscapeMode = TRUE;
    } else {
        self.isLandscapeMode = FALSE;
    }
    
    if(self.isFromTermsAndConditions) {
        [self setupWaiverScreenLayout];
    } else{
        [self displayGraphView];
    }

}
//-----------------------------------------------------------------------

-(void)openSignatureView{
    if(imageViewSignature.image != nil) {
        [self showSignatureViewWithImage:imageViewSignature.image];
    } else {
        [self showSignatureViewWithImage:nil];
    }
}
//-----------------------------------------------------------------------

- (void)showSignatureViewWithImage:(UIImage *)signImage {
    
    SignatureVC *signView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignatureView"];
    [signView setModalPresentationStyle:UIModalPresentationFormSheet];
    signView.signatureDelegate = (id)self;
    signView.signImage = signImage;
    [self presentViewController:signView animated:YES completion:^{
    }];
    
}

//-----------------------------------------------------------------------

-(void) DoneImage :(UIImage *) image {
    
    imageViewSignature.image = image;
    [btnSignatureView setImage:image forState:UIControlStateNormal];
    
    self.imgSignature = image;
    
    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsSignature",[FileUtility basePath]]];
    NSString * imgNm = [NSString stringWithFormat:@"Signature%@",self.strClientID];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [FileUtility createFileInFolder:@"ClientsSignature" withName:imgNm withData:imageData];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"UpdateSignatute" object:nil];
    
}


- (void)viewDidUnload {
    
    self.strClientID = nil;
    self.chartTag = nil;
    [super viewDidUnload];
}


//SUNIL
-(NSString * )setGraphLioneColor:(NSString *)htmlString{
    START_METHOD
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    NSString *strColor;
    NSString *strColorGraph;
    
    
    //    green:#88d208 red:#ed1b23 pink:#e92767 white:#ffffff
    switch (skin) {
        case FIRST_TYPE:
        strColor= @"#88d208";
        //       line.Set('chart.fillstyle',['Gradient(#FF3D4C:#FF5C73: #FF7A99:#FF99BF:#FFB8E6)']);
        
        strColorGraph= @"#88d208:#7ab815:#679427:#4e673e:#414e49:#3e484d";
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:strColorGraph];
    
        
        break;
        
        case SECOND_TYPE:
        strColor= @"#ed1b23";
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:@"#ed1b23:#ef3037:#f4767a:#f89ea1:#ffffff:#ffffff"];
 
        
        break;
        case THIRD_TYPE:
        strColor= @"#e92767";
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
        
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:@"#e92767:#ed4b81:#f1759e:#f498b7:#fac9d9:#ffffff:#ffffff"];
      
        
        break;
        
        default:
            //#48c5b8     //Gray color code
            strColor= @"#8b9091";
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
            //#4ac3ba:#68cdc5:#87d7d1:#b1e5e1:#d3f1ef:#ffffff   // for gradient of sky color
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:@"#8b9091:#8b9091:#a2a6a7:#c1c4c4:#dfe0e0:#ffffff"];            break;
    }
    
    MESSAGE(@"colorLine----------> %@",htmlString);
    
    return htmlString;
    END_METHOD
}



-(NSString *)getArrayofIngrapghLabel:(NSArray *)tempArray{
    START_METHOD
    MESSAGE(@"getArrayofIngrapghLabel-> %@",tempArray);
    
  
    NSString *strIngraph;
    
    NSMutableArray *arrayForSort = [[NSMutableArray alloc]init];
    
    //Set values in albel
    for (int i =0 ; i<tempArray.count; i++) {
        
        
        
        NSArray *arrayInGraph =[[NSArray alloc]initWithArray:[tempArray objectAtIndex:i ]];
        MESSAGE(@"arrayInGraph:  : %@",arrayInGraph);
        
        if(arrayInGraph.count>0){
            [arrayForSort addObject:[arrayInGraph objectAtIndex:0]];
            
        }
        
    }
    
     MESSAGE(@"arrayForSort-> lll %@",arrayForSort);
    
    int intArrayCount = (int) arrayForSort.count;
    
    
        switch (intArrayCount) {
            case 0:
                
                
                break;
            case 1:
                
                MESSAGE(@"case : 1-> strIngraph: %@",strIngraph);
                strIngraph = @"[['0','black','white' ,0,3],['hello_1','black','#f0f' ,0,3]]";
                
                

                break;
            case 2:
                strIngraph = @"[['','red','yellow' ,0,3],  ['hello_1','black','#f0f' ,0,3],      ['0','black','white' ,0,3],['hello_2','black','#0f0' ,0,3]]";
                
                break;
            case 3:
                strIngraph = @"[  ['','white','white' ,0,3],['hello_1','black','#88d208' ,0,3],['','black','white' ,0,3], ['','white','#0f0' ,0,3]['','white','white' ,0,3],['hello_3','black','#0f0' ,0,3], ['   0    ','black','white' ,0,3],['hello_2','black','blue' ,0,3]]";
                            
                
                break;
            case 4:
                strIngraph = @"[['','red','yellow' ,0,3], ['hello_1','black','#88d208' ,0,3], ['','black','white' ,0,3], ['','white','#0f0' ,0,3] ['','red','yellow' ,0,3],          ['hello_3','black','#0f0' ,0,3],   ['','black','white' ,0,3],['','white','blue' ,0,3]             ['','red','yellow' ,0,3], ['hello_2','black','blue' ,0,3], ['   0   ','black','white' ,0,3],                ['hello_4','black','#f0f' ,0,3]]";
                
                
                break;
            case 5:
                
                strIngraph = @"[['','white','white' ,0,3],['hello_2','black','#88d208' ,0,3],['','white','white',0,3 ],     ['hello_1','black','#0f0' ,0,3],['','white','white' ,0,3],['hello_3','black','#00f' ,0,3],['','white','white' ,0,3],     ['hello_4','black','#f0f' ,0,3],['   0   ','black','white' ,0,3], ['hello_5','black','#ff0' ,0,3],     ['','white','white' ,0,3],['hello_2','black','#0ff' ,0,3],['','white','white' ,0,3],     ['hello_3','black','#000' ,0,3]]";
                
                
                break;
            case 6:
                 strIngraph = @"[['','red','yellow' ,0,3],['hello_2','black','#88d208' ,0,3],['','red','white',0,3 ],     ['hello_1','black','#0f0' ,0,3],['','red','white' ,0,3],['hello_3','black','#00f' ,0,3],['','white','green' ,0,3],     ['hello_4','black','#f0f' ,0,3],['','white','yellow' ,0,3], ['hello_6','black','#ff0' ,0,3],     ['   0   ','black','white' ,0,3],['hello_7','black','#0ff' ,0,3],['   0   ','black','white' ,0,3],     ['hello_6','black','#000' ,0,3]]";
                
                
                break;
                
            case 7:
                
            strIngraph = @"[['','red','yellow' ,0,3],['hello_2','black','#88d208' ,0,3],['','red','white',0,3 ],     ['hello_1','black','#0f0' ,0,3],['','red','white' ,0,3],['hello_7','black','#00f' ,0,3],['','white','green' ,0,3],     ['hello_4','black','#f0f' ,0,3],['','white','yellow' ,0,3], ['hello_3','black','#ff0' ,0,3],     ['','white','white' ,0,3],['hello_5','black','#0ff' ,0,3],['   0   ','black','white' ,0,3],     ['hello_6','white','#000' ,0,3]]";
                
                break;
                
            default:
                break;
        }
    
    
    
     NSMutableArray *arraySorted =[[self sortedArray:arrayForSort] mutableCopy];
    
    
    
    /*
    //Set values in albel
    for (int i =0 ; i<arraySorted.count; i++) {

        
        
        
        switch (i) {
           
            case 0:
                
                MESSAGE(@"case : 1->afetr: befor stringByReplacingOccurrencesOfString: %@",strIngraph);
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_1" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];

                MESSAGE(@"case : 1->afetr:  stringByReplacingOccurrencesOfString: %@",strIngraph);
                
                break;
            case 1:
                
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_2" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];
                
                break;
            case 2:
                
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_3" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];
                
                break;
            case 3:
                
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_4" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];
                
                break;
            case 4:
                
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_5" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];
                break;
            case 5:
                
                
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_6" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];
                
                break;
                
            case 6:
                
                strIngraph = [strIngraph stringByReplacingOccurrencesOfString:@"hello_7" withString:[NSString stringWithFormat:@"%@",[arraySorted objectAtIndex:i]]];
                break;
                
            default:
                break;
        }
    }
     */
    
    
    MESSAGE( @"string for ingraph : %@",strIngraph);
    return strIngraph;
    
    END_METHOD
}


-(NSArray *)sortedArray:(NSArray *)array{
    
    START_METHOD
    //sfloats would be your [myDict allKeys]
    NSArray *myArray = [array sortedArrayUsingDescriptors:
                        @[[NSSortDescriptor sortDescriptorWithKey:@"doubleValue"
                                                        ascending:YES]]];
    
    MESSAGE(@"Sorted: %@", myArray);

    return myArray;
    
}
@end

