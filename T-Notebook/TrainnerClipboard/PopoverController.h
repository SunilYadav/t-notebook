//
//  PopoverController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol popoverDelegate
-(void)test : (NSInteger)currentSheetID : (NSInteger)blockID;
@end
@interface PopoverController : UIViewController
@property (unsafe_unretained,nonatomic) id<popoverDelegate> delegate;
@property (nonatomic, strong) NSMutableArray	* arrayProgramSheet;
@property (nonatomic, strong) NSString				* strClientID;


@end
