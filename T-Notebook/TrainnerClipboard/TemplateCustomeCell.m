//
//  TemplateCustomeCell.m
//  T-Notebook
//
//  Created by WLI on 26/06/12.
//  Copyright (c) 2012 WLI. All rights reserved.
//

#import "TemplateCustomeCell.h"

@implementation TemplateCustomeCell
@synthesize lblTemplateName = _lblTemplateName;
@synthesize lblSheetContained = _lblSheetContained;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.lblTemplateName = [[UILabel alloc]initWithFrame:CGRectMake(53, 10, 140, 40)];
        [self.lblTemplateName setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:16]];
        [self.lblTemplateName setBackgroundColor:[UIColor clearColor]];
        //[self.lblTemplateName setTextAlignment:NSTextAlignmentLeft];
        [self.lblTemplateName setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];
        
        [self addSubview:self.lblTemplateName];
        
        self.lblSheetContained = [[UILabel alloc]initWithFrame:CGRectMake(16, 10, 37, 40)];
        [self.lblSheetContained setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:16]];
        [self.lblSheetContained setBackgroundColor:[UIColor clearColor]];
        //[self.lblSheetContained setTextAlignment:NSTextAlignmentRight];
        [self.lblSheetContained setTextColor:[UIColor whiteColor]];
        
        
        [self addSubview:self.lblSheetContained];
    }
    return self;
}

- (void) displayTableData :(NSDictionary *) dictInfo {
	
    [self.lblTemplateName setText:[dictInfo valueForKey:@"templateName"]];
    [self.lblSheetContained setText:[dictInfo valueForKey:@"SheetContained"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
