//
//  ViewAllProgramVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgramViewController.h"
#import "ClientViewController.h"
@class ClientViewController;

@protocol viewallProgramDelegate<NSObject>
-(void)openMailForScreenShotImage:(UIImage*)image;
-(void)changeOptionScreen;
@end


@interface ViewAllProgramVC : BaseViewController
@property (nonatomic, strong) NSString				* strClientId;
@property (nonatomic, weak) id<viewallProgramDelegate>		              delegate;

@property (nonatomic) BOOL                                                          setAsLandscape;
-(void)setUpLandscapeOrientation;
-(void)setupPortraitOrientation;

+ (id)sharedInstance;
-(void)optionsButtonClicked:(id)sender;
-(void)takeScreenShot;
-(void)reloadViewAllTable;
@end
