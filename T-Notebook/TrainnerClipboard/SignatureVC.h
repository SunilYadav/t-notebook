//
//  SignatureVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol signatureViewDelegate

-(void) DoneImage :(UIImage *) image;

@end


@interface SignatureVC : UIViewController

@property (nonatomic , strong) UIImageView                          *drawImage;
@property (nonatomic , strong) UIImage                                 *signImage;

@property (nonatomic , strong) id <signatureViewDelegate>   signatureDelegate;

@end
