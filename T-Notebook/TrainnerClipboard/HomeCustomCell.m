//
//  HomeCusetomCell.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2011 WLI All rights reserved.
//

#import "HomeCustomCell.h"

@implementation HomeCustomCell

@synthesize lblCName = _lblCName;
@synthesize ImgTable;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        //SUNIL: Set the frame for large name  : v_5.0
        _lblCName = [[UILabel alloc] initWithFrame:CGRectMake(70, 12, 160, 50)];
        [_lblCName setFont:[UIFont fontWithName:kproximanova_semibold size:16]];
        [_lblCName setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];
        
         //SUNIL: Set the allignment for large name  : v_5.0
        [_lblCName setTextAlignment:NSTextAlignmentLeft];
        [_lblCName setNumberOfLines:0];


        [_lblCName setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:_lblCName];
        
		
        
        ImgTable = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 50, 50)];
        ImgTable.image = [UIImage imageNamed:@"Help-btn"];
        ImgTable.layer.cornerRadius = 25;
        ImgTable.clipsToBounds = TRUE;
        [self.contentView addSubview:ImgTable];
        
        
        
        //TODO: new features SUNIL  : v_5.0
       _mySwitch= [[UISwitch alloc] initWithFrame:CGRectMake(230, 20, 0, 0)];
        [_mySwitch setTintColor:[UIColor lightGrayColor]];
        [_mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        
        [self.contentView addSubview:_mySwitch];
        
        
        
        //For Workouts
        //SUNIL: Create lable for workout name
        _lblCNameWorkout = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 220, 30)];
        [_lblCNameWorkout setFont:[UIFont fontWithName:kproximanova_semibold size:16]];
        [_lblCNameWorkout setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];
        
        //SUNIL: Set the allignment for large name  : v_5.0
        [_lblCNameWorkout setTextAlignment:NSTextAlignmentLeft];
        [_lblCNameWorkout setNumberOfLines:0];
        
        
        [_lblCNameWorkout setBackgroundColor:[UIColor clearColor]];
        _lblCNameWorkout.hidden = YES;
        [self.contentView addSubview:_lblCNameWorkout];
        
        
        
        //SUNIL: Create lable for workout Date
        _lblCNameWorkoutDate = [[UILabel alloc] initWithFrame:CGRectMake(250, 0, 160, 30)];
        [_lblCNameWorkoutDate setFont:[UIFont fontWithName:kproximanova_semibold size:16]];
        [_lblCNameWorkoutDate setTextColor:[UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0]];
        
        //SUNIL: Set the allignment for large name  : v_5.0
        [_lblCNameWorkoutDate setTextAlignment:NSTextAlignmentLeft];
        [_lblCNameWorkoutDate setNumberOfLines:0];
        
                _lblCNameWorkoutDate.hidden = YES;
        [_lblCNameWorkoutDate setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:_lblCNameWorkoutDate];
        
        
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
}




/// Switch Action
- (void)changeSwitch:(id)sender{
    if([sender isOn]){
        // Execute any code when the switch is ON
        MESSAGE(@"Switch is ON");
    } else{
        // Execute any code when the switch is OFF
        MESSAGE(@"Switch is OFF");
    }
}



@end

