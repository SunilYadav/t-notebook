//
//  ParQquestionView.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ParQQuestionViewDelegate

- (void)showSignatureViewWithImage:(UIImage *)signImage;
- (void) textfieldBeginWithFrame : (CGRect) frame;
- (void) textfieldEndWithFrame : (CGRect) frame;
- (void)openTheTermsAndConditions;
-(void)saveParQDataFromView;

@end

@interface ParQquestionView : UIView <UITextViewDelegate>

@property (nonatomic , strong) NSArray                                              *aryCategory;
@property (nonatomic , strong) NSMutableArray                                 *aryQuestions;
@property (nonatomic , strong) NSMutableArray                                 *aryAnswers;
@property (nonatomic , strong) UIImage                                              *imgSelected;
@property (nonatomic , strong) UIImage                                              *imgUnselected;
@property (nonatomic , strong) NSString                                             *strClientId;
@property (nonatomic , strong) id <ParQQuestionViewDelegate>       parqQuestDelegate;
@property (nonatomic) BOOL                                                               termsAccepted;
@property (nonatomic) BOOL                                                          shouldSetLandscape;

-(id)initWithFrame:(CGRect)frame;
-(void)setupLayoutForViews;
- (void) setSignature :(UIImage *) image;
-(void)btnSubmit_click;
- (UIImage *)getImageIfExistForSignature;
- (int) setLandscapeLayout;
- (int) setPotraitLayout ;


@end
