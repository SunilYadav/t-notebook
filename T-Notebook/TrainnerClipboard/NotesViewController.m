//
//  NotesViewController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Show listing of Notes 

#import "TextFileManager.h"
#import "NotesViewController.h"
#import "NotesCustomCell.h"
#import "UpdateNotesVC.h"

#define kFont [UIFont fontWithName:@"ProximaNova-Regular" size:18]
#define kSelectedBtnBGColor [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]
#define  kProximaRegular @"ProximaNova-Regular"

@interface NotesViewController () <UITableViewDataSource,UITableViewDelegate>{
    NSString *currentSheetId,*blockSheetNo;
    NSString *_strExistingNotes;//For update notes with existing note for compare Sunil 5 Apr
    
    //TODO: Migration
    NSString *blockId;
}
@end

@implementation NotesViewController
@synthesize strClientId;
@synthesize strClientName;
@synthesize delegate;

//-----------------------------------------------------------------------

#pragma mark - Memory Management Methods

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    START_METHOD
    [super didReceiveMemoryWarning];
}

//-----------------------------------------------------------------------

#pragma mark - Action Methods

//-----------------------------------------------------------------------

- (IBAction)newNoteTap:(id)sender {
}

//-----------------------------------------------------------------------

-(void)btnDateClicked:(UIButton*)sender{
    START_METHOD
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"isFromNotes"];
    [self.delegate setProgrameWorkoutModuleForNotes:[self.arrtemp objectAtIndex:sender.tag]];
}

//-----------------------------------------------------------------------

-(void)updatedNote:(NSString *)updateString
{
    START_METHOD
    //Sunil Check condition
    updateString= [updateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if(updateString.length>0){
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
       
            
            //TODO: SUNIL-> Update Notes
            //Craete dict
            NSDictionary *dictNote =@{
                                      kBlockNotes : updateString,
                                      ksheetID:currentSheetId,
                                      PROGRAM_ID:currentSheetId,
                                      kBlockNo :blockSheetNo,
                                      kClientId :self.strClientId,
                                      kBlockId:blockId,
                                      @"existingNote":_strExistingNotes,
                                      ACTION:@"updateNotes"
                                      };
            
            //Invoke method for post request for Add notes
            [self postRequestForNotes:[dictNote mutableCopy]];
            
        }else{
            
            
            
            NSInteger success = [[DataManager initDB] updateNotes:updateString blockTitle:currentSheetId ForBlock:blockSheetNo forClient:self.strClientId andexistingNotes: _strExistingNotes];
            
            
            [self.arrtemp  removeAllObjects];
            [self getDataOfNotes];
            

        }
        
        
    }else{
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text", nil), kAppName);

    }
    
    
}

//-----------------------------------------------------------------------

-(void)btnEditNotesClicked:(UIButton*)sender{
    START_METHOD
    
    UpdateNotesVC *updateNotesVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"updatenote"];
     UINavigationController* updateNotesNavigationController = [[UINavigationController alloc] initWithRootViewController:updateNotesVCOBJ];
    [updateNotesNavigationController.navigationBar setTranslucent:NO];
    updateNotesNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    updateNotesNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    
    updateNotesVCOBJ.notes =   [[self.arrtemp objectAtIndex:sender.tag] valueForKey:@"sBlockNotes"];
    _strExistingNotes      = [[NSString alloc]initWithFormat:@"%@",updateNotesVCOBJ.notes];
    
    
    updateNotesVCOBJ.delegate =(id) self;
    currentSheetId = [NSString stringWithFormat:@"%ld",(long)[[[self.arrtemp objectAtIndex:sender.tag] valueForKey:@"nSheetID"] integerValue]];
      blockId = [NSString stringWithFormat:@"%ld",(long)[[[self.arrtemp objectAtIndex:sender.tag] valueForKey:kBlockId] integerValue]];
    blockSheetNo = [NSString stringWithFormat:@"%ld",(long)[[[self.arrtemp objectAtIndex:sender.tag] valueForKey:kBlockNo] integerValue]];
    if (iOS_8) {
        // Weblineindia //
       CGPoint frameSize = CGPointMake(500, 320);
        updateNotesNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    [self.view.window.rootViewController presentViewController:updateNotesNavigationController animated:YES completion:nil];
   
    
}

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (CGFloat)getHeightForText:(NSString *)strText {
    START_METHOD
    CGSize constraintSize = CGSizeMake(600, MAXFLOAT);
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:19];
    [style setMaximumLineHeight:20];

    
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc]initWithString:strText attributes:@{NSFontAttributeName:kFont}];

    
    NSDictionary *attributes = @{NSFontAttributeName :kFont,
                                 NSParagraphStyleAttributeName : style,
                                 };
    
    [attrString addAttributes:attributes range:NSMakeRange(0, attrString.length)];
    
    CGRect rect1 = [attrString boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading  context:nil];
    
    return rect1.size.height;
}

//-------------------------------------------------------------------------------------------------------------

-(void)getDataOfNotes {
    START_METHOD
    [self.lblClientName setFont:[UIFont fontWithName:kproximanova_semibold size:24]];
    
    NSString *strName = strClientName;
    strName = [strName stringByAppendingString:NSLocalizedString(@"'s NOTES", nil)];
    self.lblClientName.text = strName;
    [self.lblClientName setTextColor:kAppTintColor];


    NSInteger length = self.lblClientName.attributedText.length -6;
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithAttributedString: self.lblClientName.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName value:kAppTintColor range:NSMakeRange(0, length)];
    [self.lblClientName setAttributedText: text];
    

    NSMutableArray *sheetArray = [[NSMutableArray alloc]init];
    NSMutableArray *notesArray = [[NSMutableArray alloc]init];
    
    NSString *clientId = self.strClientId;
    NSArray *ary = [[DataManager initDB]getTotalSheetsForClient:clientId];
    [sheetArray addObjectsFromArray:ary];
        
    [sheetArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = obj;
        NSString *sheetId = [dict objectForKey:@"nSheetID"];
        
        NSArray *ary = [[DataManager initDB]getNotesForClient:clientId andSheets:sheetId];
        
        MESSAGE(@"notes array: %@",ary);
        
        
        [ary enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *dict = obj;
            NSString *notesDesc = [dict objectForKey:kBlockNotes];
            if([allTrim(notesDesc) length] > 0) {
                 [notesArray addObject:dict];
            }
        }];
     }];
    
    MESSAGE(@"notes notesArray: %@",notesArray);

    [self.arrtemp addObjectsFromArray:notesArray];
    
       MESSAGE(@"notes self.arrtemp: %@",self.arrtemp);
    
    sheetArray = nil;
    notesArray = nil;
    [self.tableview reloadData];
    

}


//-----------------------------------------------------------------------

#pragma mark - TableView Datasource Methods

//-----------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    START_METHOD
    return 1;
}

//-----------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    START_METHOD
    NotesCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.lblDetails setFont:kFont];

    [cell.btnDate setTitle:[[self.arrtemp objectAtIndex:indexPath.section]valueForKey:kBlockDate] forState:UIControlStateNormal];
    cell.btnDate.tag = indexPath.section;
    [cell.btnDate addTarget:self action:@selector(btnDateClicked:) forControlEvents:UIControlEventTouchUpInside];
    
     cell.btnEditNotes.tag = indexPath.section;
    [cell.btnEditNotes addTarget:self action:@selector(btnEditNotesClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.setAsLandscape) {
        cell.setLandscapeCell = TRUE;
    } else {
        cell.setLandscapeCell = FALSE;
    }
    
    [cell configCell:[[self.arrtemp objectAtIndex:indexPath.section]valueForKey:kBlockNotes]];
    
    //Sunil inoke method for set color of view
    [self setViewOnBasisSkin:cell];
    
    
    return cell;
}

//-----------------------------------------------------------------------

#pragma mark - TableView Delegate Methods

//-----------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    START_METHOD
    
    return   self.arrtemp.count;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    START_METHOD
    CGFloat height = [self getHeightForText:[[self.arrtemp objectAtIndex:indexPath.section]valueForKey:kBlockNotes]];
    
    return  height+20; //55
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    START_METHOD
    return 0.0f;
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    START_METHOD
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    START_METHOD
    return 20.0f;
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    START_METHOD
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
    
}

//-----------------------------------------------------------------------

-(void)setLandscapeOrientation {
    START_METHOD

    self.setAsLandscape = TRUE;
    [self.notesview setFrame:CGRectMake(0, 0, 924 , kLandscapeHeight - 78)]; // h = -78
    [self.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"NotesBg_Landscape"]]];
    [self.lblClientName setFrame:CGRectMake(0, 36, kLandscapeWidth - 98, 35)];
    [self.tableview setFrame:CGRectMake(0, 155 + 20, kLandscapeWidth-98, kLandscapeHeight - 330)]; // y = 155
    [self.tableview reloadData];
    END_METHOD
}


//-----------------------------------------------------------------------

-(void)setportraitOrientation {
    
    START_METHOD
    self.setAsLandscape = FALSE;
    [self.notesview setFrame:CGRectMake(0, 0, 740, 850)];
    [self.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notes-bg"]]];
    [self.lblClientName setFrame:CGRectMake(0, 36, 740, 35)];
    [self.tableview setFrame:CGRectMake(0, 155 + 10, 740, 695)];
    [self.tableview reloadData];
    END_METHOD
    
}


//-----------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.notesview setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"notes-bg"]]];
    self.arrtemp = [[NSMutableArray alloc]init];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FromLastVisitedNotes"]) {
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"FromLastVisitedNotes"];
        [self.arrtemp  removeAllObjects];
        [self getDataOfNotes];

    }
    [self setClientsNameColorOnBasisSkin];
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    [super viewWillAppear:animated];
    [self.arrtemp  removeAllObjects];
     [self getDataOfNotes];
    
   
}

//-----------------------------------------------------------------------

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    
   
    
}

//Sunil
-(void)setViewOnBasisSkin:(NotesCustomCell *)cell{
    START_METHOD
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            _lblClientName.textColor =  kFirstSkin;
            cell.lblDetails.textColor =  [UIColor whiteColor];
            [cell.btnDate setTitleColor:kFirstSkin forState:UIControlStateNormal];
            [cell.btnEditNotes setImage:[UIImage imageNamed:@"gray_edit1.png"] forState:UIControlStateNormal];
           
            break;
            
        case SECOND_TYPE:
            _lblClientName.textColor =  ksecondSkin;
            cell.lblDetails.textColor =  [UIColor whiteColor];
            [cell.btnDate setTitleColor:ksecondSkin forState:UIControlStateNormal];
            [cell.btnEditNotes setImage:[UIImage imageNamed:@"gray_edit1.png"] forState:UIControlStateNormal];
            
            break;
            
        case THIRD_TYPE:
            _lblClientName.textColor =  kThirdSkin;
            cell.lblDetails.textColor =  [UIColor grayColor];
            [cell.btnDate setTitleColor:kThirdSkin forState:UIControlStateNormal];
            [cell.btnEditNotes setImage:[UIImage imageNamed:@"gray_edit1.png"] forState:UIControlStateNormal];
            
            break;
            
            
        default:
            break;
    }
    END_METHOD
}


//Sunil
-(void)setClientsNameColorOnBasisSkin{
    
    START_METHOD
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            _lblClientName.textColor =  kFirstSkin;
             break;
            
        case SECOND_TYPE:
            _lblClientName.textColor =  ksecondSkin;
           break;
            
        case THIRD_TYPE:
            _lblClientName.textColor =  kThirdSkin;
            break;
            
            
        default:
            break;
    }
    END_METHOD
}

//-----------------------------------------------------------------------


//TODO: SUNIL-> POST REQUEST TO SERVER
//Method for post request for Add new workout
-(void)postRequestForNotes:(NSMutableDictionary *)dictNotes{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictNotes,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictNotes
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictNoteTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictNotes,dictNoteTemp);
                                   
                                   NSString *strAction =   [dictNoteTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"updateNotes"]){
                                       
                                       [self updateNote:dictNotes];
                                   }
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                               
                               
                           }];
    END_METHOD
}


//MEthod for post the request to update the notes
-(void)updateNote:(NSDictionary *)dictNote{
    
    /*
     "sBlockNotes": "vika1111111111s",
     "nBlockNo": "2503",
     "ClientID": "1580",
     "program_id": "10621",
     "action": "updateNotes",
     "nBlockID": 1402
     
     */
    
    NSInteger success = [[DataManager initDB] updateNotes:[dictNote objectForKey:kBlockNotes] blockTitle:[dictNote objectForKey:PROGRAM_ID] ForBlock:[dictNote objectForKey:kBlockNo] forClient:self.strClientId andexistingNotes: _strExistingNotes];
    
    
    [self.arrtemp  removeAllObjects];
    [self getDataOfNotes];
    
}

//TODO : Move to Dashboard for lOg out
-(void)logOut{
    START_METHOD
    
    //Save lout Key
    [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
    
    //Create object of trainer profile view
    ViewController *objMenuView  = (ViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SignViewController"];
    
    //Push view
    //Push view
     UINavigationController *nav=[[AppDelegate sharedInstance]nav];
    
    [nav popViewControllerAnimated:NO];
}

@end

