    //
//  ProgramGetDetailVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "ProgramGetDetailVC.h"
#import "ProgramViewController.h"
#import "KeyboardControls.h"
#import "UIView+firstResponder.h"

@interface ProgramGetDetailVC ()<KeyboardControlsDelegate, UITextFieldDelegate> {
    __weak IBOutlet UIButton *btnAdd;
    int selectedColor;
    
}
@property (weak, nonatomic) IBOutlet UIView        *backgroundView;

@property (weak, nonatomic) IBOutlet UIScrollView  *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *txtWeight;
@property (strong, nonatomic) IBOutlet UITextField *txtExercise;
@property (strong, nonatomic) IBOutlet UITextField *txtSet;
@property (strong, nonatomic) IBOutlet UITextField *txtReps;
@property (strong, nonatomic) IBOutlet UITextField *txtRest;
@property (weak, nonatomic) IBOutlet UIButton      *btnNoColor;

@property (weak, nonatomic) IBOutlet UILabel *lblExercise;
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblSets;
@property (weak, nonatomic) IBOutlet UILabel *lblReps;
@property (weak, nonatomic) IBOutlet UILabel *lblRestTime;


@property (strong, nonatomic)  UITextField *activeTxt;
@property (nonatomic, strong) NSString* strSelectedEx;
@property (nonatomic,strong) UIPopoverController		* pop;


@property (nonatomic) BOOL								isPopover;
@property (nonatomic) NSInteger						    selectedTag;
@property (nonatomic, strong) KeyboardControls      *keyboardControls;


@end

@implementation ProgramGetDetailVC


#pragma mark -
#pragma mark AutoSave methods


#pragma mark -
#pragma mark AutoSuggestViewDelegate methods

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (void)closeExercisePopover {
    [self.txtWeight becomeFirstResponder];
    self.isPopover = NO;
    [self.pop dismissPopoverAnimated:NO];
}

-(void) getExercise:(NSString *)Exercise {
    [self.txtWeight becomeFirstResponder];
    self.txtExercise.text = Exercise;
    self.strSelectedEx = Exercise;
    NSMutableArray * defaultEx = [[NSMutableArray alloc] init];
    [defaultEx setArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"defaultExercise"]];
    if ([Exercise length]!=0) {
        if (![defaultEx containsObject:Exercise]) {
            [defaultEx addObject:Exercise];
            if ([defaultEx count]>kDefaultExe) {
                [defaultEx removeObjectAtIndex:0];
            }
        }
    }
    NSArray * arrTemp = [NSArray arrayWithArray:defaultEx];
    [[NSUserDefaults standardUserDefaults] setObject:arrTemp forKey:@"defaultExercise"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    defaultEx = nil;
    self.isPopover = NO;
    [self.pop dismissPopoverAnimated:NO];

}

//-----------------------------------------------------------------------

- (void) exerciseValueChanged:(NSString *)exe {
    self.txtExercise.text = exe;

}


//-----------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self localizeControls];
    
    [self addNavBarButtons];
    
    btnAdd.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnAdd.layer.cornerRadius = 4.0;
    btnAdd.backgroundColor = kAppTintColor;
    self.txtWeight.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtExercise.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtSet.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtReps.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtRest.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    selectedColor = 0;
    
    for (UIView *view in [self.scrollView subviews]) {
        
        if (view.tag == 1801 || view.tag == 1802 ||view.tag == 1803 || view.tag == 1804 || view.tag == 1805 || view.tag == 1806) {
            UIButton *button = (UIButton *)view;
            [[button layer] setBorderWidth:0.0];
            [[button layer] setBorderColor:[[UIColor clearColor] CGColor]];
        }
    }

    [[[self.view viewWithTag:1806] layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[[self.view viewWithTag:1806] layer] setBorderWidth:2.0];
    
    if (self.dictLineToShow != nil) {
        if ([[self.dictLineToShow valueForKey:@"sLBValue"] isEqualToString:@"NA"]) {
            self.txtWeight.text = @"";
        }
        else
        {
            self.txtWeight.text = allTrim([self.dictLineToShow valueForKey:@"sLBValue"]);
        }
        
        if ([[self.dictLineToShow valueForKey:@"sProgram"] isEqualToString:@"NA"]) {
            self.txtExercise.text = @"";
        }
        else
        {
            self.txtExercise.text = allTrim([self.dictLineToShow valueForKey:@"sProgram"]);
        }
        
        if ([[self.dictLineToShow valueForKey:@"sSetValue"] isEqualToString:@"NA"]) {
            self.txtSet.text = @"";
        }
        else
        {
            self.txtSet.text = allTrim([self.dictLineToShow valueForKey:@"sSetValue"]);
        }
        
        if ([[self.dictLineToShow valueForKey:@"sRepValue"] isEqualToString:@"NA"]) {
            self.txtReps.text = @"";
        }
        else
        {
            self.txtReps.text = allTrim([self.dictLineToShow valueForKey:@"sRepValue"]);
        }
        
        if ([[self.dictLineToShow valueForKey:@"dEXDate"] isEqualToString:@"NA"]) {
            self.txtRest.text = @"";
        }
        else
        {
            self.txtRest.text = allTrim([self.dictLineToShow valueForKey:@"dEXDate"]);
        }
        
       selectedColor =  [[self.dictLineToShow valueForKey:@"sColor"] intValue];
        for (UIView *view in [self.scrollView subviews]) {
            
            if (view.tag == 1801 || view.tag == 1802 ||view.tag == 1803 || view.tag == 1804 || view.tag == 1805 || view.tag == 1806) {
                UIButton *button = (UIButton *)view;
                [[button layer] setBorderWidth:0.0];
                [[button layer] setBorderColor:[[UIColor clearColor] CGColor]];
            }
        }

        if (!selectedColor) {
            [[[self.view viewWithTag:1806 + selectedColor] layer] setBorderColor:[[UIColor blackColor] CGColor]];
            [[[self.view viewWithTag:1806 + selectedColor] layer] setBorderWidth:2.0];
        }
        else
        {
        [[[self.view viewWithTag:1800 + selectedColor] layer] setBorderColor:[[UIColor blackColor] CGColor]];
            [[[self.view viewWithTag:1800 + selectedColor] layer] setBorderWidth:2.0];
        }
    }

    UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
    lblTitle.text = NSLocalizedString(@"Add New Exercise", nil);
    lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
    lblTitle.textColor = kAppTintColor;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.adjustsFontSizeToFitWidth=YES;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=lblTitle;
    
    self.txtWeight.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    self.txtExercise.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    self.txtReps.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    self.txtSet.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    self.txtRest.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    
}

//-----------------------------------------------------------------------

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    NSArray *fields = @[self.txtExercise,self.txtWeight,self.txtSet,self.txtReps,self.txtRest];
    [self setKeyboardControls:[[KeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    fields = nil;
    
}

//-----------------------------------------------------------------------

#pragma mark - Keyboard Controls Delegate Methods

//-----------------------------------------------------------------------

- (void)keyboardControlsDonePressed:(KeyboardControls *)keyboardControls{
    [self.pop dismissPopoverAnimated:false];
    [keyboardControls.activeField resignFirstResponder];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}
//-----------------------------------------------------------------------

- (void)keyboardControls:(KeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(KeyboardControlsDirection)direction{
    
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated
{
    START_METHOD
    [super viewWillAppear:animated];
    [self setTitle:@"Add New Line"];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (iOS_7) {
            [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight)];
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 500);
            self.navigationController.view.superview.center = self.view.center;
        }
        if (iOS_8) {
            [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight)];
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 500);
            self.navigationController.view.superview.center = self.view.center;
        }
        
    } else {
        if (iOS_7) {
            [self.view setFrame:CGRectMake(0, 0, kPotraitWidth, kPotraitHeight)];
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 500,500);
            self.navigationController.view.superview.center = self.view.center;
        }
        
        if (iOS_8) {

        }
        
    }
    
    
}
//-----------------------------------------------------------------------

#pragma mark - Memory Management Methods

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (void)deviceOrientationDidChange:(id *)center {
    UIView *view = [self.view findFirstResponder];
    if (view == self.txtRest) {
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x,110)];
        } else {
            [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x,20)];
        }
    }
}

//-----------------------------------------------------------------------

#pragma mark - Action Method

//-----------------------------------------------------------------------

- (IBAction)btnColorTapped:(id)sender {
    
    for (UIView *view in [self.scrollView subviews]) {
       
        if (view.tag == 1801 || view.tag == 1802 ||view.tag == 1803 || view.tag == 1804 || view.tag == 1805 || view.tag == 1806) {
            UIButton *button = (UIButton *)view;
            [[button layer] setBorderWidth:0.0];
            [[button layer] setBorderColor:[[UIColor clearColor] CGColor]];
        }
    }
    UIButton *button = sender;
    [[button layer] setBorderWidth:3.0];
    [[button layer] setBorderColor:[[UIColor blackColor] CGColor]];
    
    if ([sender tag] == 1801 ) {
        
       selectedColor = 1;
    } else  if ([sender tag] == 1802 ) {
        selectedColor = 2;
    }else  if ([sender tag] == 1803 ) {
        selectedColor = 3;
    } else  if ([sender tag] == 1804 ) {
        selectedColor = 4;
    }else  if ([sender tag] == 1805 ) {
        selectedColor = 5;
    }else  if ([sender tag] == 1806 ) {
        selectedColor = 0;
    }
    
}

//-----------------------------------------------------------------------
-(IBAction)btnAddClicked:(id)sender{
    
    START_METHOD
    // done button
    
    [self.activeTxt resignFirstResponder];
    NSString *strProgramName = self.txtExercise.text;
    if (allTrim(strProgramName).length == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter Exercise name.", nil), kAppName);
        return;
    }

    NSDictionary *Detailsdict;
    if (self.dictLineToShow == nil) {
        
        Detailsdict =              @{@"exercise" : self.txtExercise.text,
                                     @"set" : self.txtSet.text,
                                     @"rep" :self.txtReps.text,
                                     @"weight" :self.txtWeight.text,
                                     @"restTime" :self.txtRest.text,
                                     @"selectedColor":[NSNumber numberWithInt:selectedColor]
                                     };
    }
    else{
        
        
        Detailsdict =        @{@"sProgram" : self.txtExercise.text,
                               @"sSetValue" : self.txtSet.text,
                               @"sRepValue" :self.txtReps.text,
                               @"sLBValue" :self.txtWeight.text,
                               @"dEXDate" :self.txtRest.text,
                               @"selectedColor":[NSNumber numberWithInt:selectedColor],
                               @"nBlockDataID" :[self getStringValueFor:[self.dictLineToShow valueForKey:@"nBlockDataID"]],
                               @"nBlockID" :[self getStringValueFor:[self.dictLineToShow valueForKey:@"nBlockID"]],
                               @"nRowNo" :[self getStringValueFor:[self.dictLineToShow valueForKey:@"nRowNo"]],
                               @"nSheetID" :[self getStringValueFor:[self.dictLineToShow valueForKey:@"nSheetID"]],
                               @"nBlockNo" :[self getStringValueFor:[self.dictLineToShow valueForKey:@"nBlockNo"]],
                               @"RowId":[self getStringValueFor:self.strRowIdtoReload],
                               kBlockDataId :[self getStringValueFor:[self.dictLineToShow valueForKey:kBlockDataId]],
                               };
    }
    
    MESSAGE(@"Program deatil : %@",Detailsdict);
    ProgramViewController *progVCOBJ = [ProgramViewController sharedInstance];
    progVCOBJ.detailsExercise = Detailsdict;
    
    
     MESSAGE(@"Ebetred deatil :progVCOBJ.detailsExercise: %@",progVCOBJ.detailsExercise);
    self.keyboardControls.delegate = nil;
    self.keyboardControls = nil;
    if (self.dictLineToShow == nil) {
        [_delegate addNewSheet:Detailsdict];
    }
    else{
        [self.delegate editNewLine:Detailsdict];
    }
    
    self.txtWeight.text = nil;
    self.txtExercise.text = nil;
    self.txtSet.text = nil;
    self.txtReps.text = nil;
    self.txtRest.text = nil;
    Detailsdict = nil;
    [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];

}


//-----------------------------------------------------------------------

#pragma mark - TextFieldDelegate

//-----------------------------------------------------------------------

- (void)textFieldDidBeginEditing:(UITextField *)textField {

    START_METHOD
    [self.keyboardControls setActiveField:textField];

}

//-----------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    START_METHOD
    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

//-----------------------------------------------------------------------

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return NO;
}

//-----------------------------------------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSArray *arrayFields = self.keyboardControls.fields;
    NSInteger index = [arrayFields indexOfObject:textField];
    if (index <arrayFields.count-1) {
        UITextField *nextTextField = [arrayFields objectAtIndex:index+1];
        [nextTextField becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }

    return YES;
}

//-----------------------------------------------------------------------
-(void)changePopOverFrame{
    // [self.pop dismissPopoverAnimated:true];
    [self performSelector:@selector(openPopOver) withObject:nil afterDelay:0.0];
}

//-----------------------------------------------------------------------


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSInteger tag1 = textField.tag;

    [self.pop dismissPopoverAnimated:true];
    
    if ([textField isEqual:self.txtRest]) {
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            if (self.scrollView.contentOffset.y < 110) {
                [UIView animateWithDuration:0.3 animations:^{
                    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x, self.scrollView.contentOffset.y + 110)];
                }];
            }
        } else {
            if (self.scrollView.contentOffset.y < 20) {
                [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x, self.scrollView.contentOffset.y + 20)];
            }
        }
    }
    
    if ([textField isEqual:self.txtExercise]) {
        
        [self.scrollView setContentOffset:CGPointMake(0,0)];
        
        AutoSuggestViewController * autoSuggestViewOBJ = nil;
        autoSuggestViewOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AutoSuggestView"];
        autoSuggestViewOBJ.delegate = self;
        if ([textField.text length] != 0) {
            self.strSelectedEx = textField.text;
            autoSuggestViewOBJ.strSelExercise = self.strSelectedEx;
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:autoSuggestViewOBJ];
        
        if (self.pop != nil) {
            self.pop.delegate = nil;
            self.pop = nil;
        }
        
        self.pop = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.pop.delegate = (id)self;
        [self performSelector:@selector(openPopOver) withObject:nil afterDelay:0.7];
        
        self.selectedTag = tag1;
        self.isPopover = YES;
        navigationController = nil;
        autoSuggestViewOBJ = nil;
        
        return NO;
        
        
    }
    return YES;
}
-(void)openPopOver{
    self.pop.popoverContentSize = CGSizeMake(300, 300);
    [self.pop presentPopoverFromRect:self.txtExercise.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    [btnAdd setTitle:NSLocalizedString(btnAdd.titleLabel.text, nil) forState:0];
    [self.btnNoColor setTitle:NSLocalizedString(self.btnNoColor.titleLabel.text, nil) forState:0];
    
    _lblExercise.text = NSLocalizedString(_lblExercise.text, nil);
    _lblWeight.text = NSLocalizedString(_lblWeight.text, nil);
    _lblSets.text = NSLocalizedString(_lblSets.text, nil);
    _lblReps.text = NSLocalizedString(_lblReps.text, nil);
    _lblRestTime.text = NSLocalizedString(_lblRestTime.text, nil);
    
    self.txtExercise.placeholder = NSLocalizedString(self.txtExercise.placeholder, nil);
    self.txtWeight.placeholder = NSLocalizedString(self.txtWeight.placeholder, nil);
    self.txtReps.placeholder = NSLocalizedString(self.txtReps.placeholder, nil);
    self.txtRest.placeholder = NSLocalizedString(self.txtRest.placeholder, nil);
    self.txtSet.placeholder = NSLocalizedString(self.txtSet.placeholder, nil);
}


//-----------------------------------------------------------------------

-(void)NavBarBtnTap :(UIBarButtonItem *)sender {
    
    if ([sender tag] == 201) {
        // back button
        self.txtWeight.text = nil;
        self.txtExercise.text = nil;
        self.txtSet.text = nil;
        self.txtReps.text = nil;
        self.txtRest.text = nil;
        [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
    } else if ([sender tag] == 202) {
        // done button
        [self.activeTxt resignFirstResponder];
        NSDictionary *Detailsdict;
        if (self.dictLineToShow == nil) {
            
            Detailsdict =      @{@"exercise" : self.txtExercise.text,
                                 @"set" : self.txtSet.text,
                                 @"rep" :self.txtReps.text,
                                 @"weight" :self.txtWeight.text,
                                 @"restTime" :self.txtRest.text,
                                 @"selectedColor":[NSNumber numberWithInt:selectedColor]
                                 };
            
        } else{
            
            Detailsdict =     @{@"sProgram" : self.txtExercise.text,
                                @"sSetValue" : self.txtSet.text,
                                @"sRepValue" :self.txtReps.text,
                                @"sLBValue" :self.txtWeight.text,
                                @"dEXDate" :self.txtRest.text,
                                @"selectedColor":[NSNumber numberWithInt:selectedColor],
                                @"nBlockDataID" :[self.dictLineToShow valueForKey:@"nBlockDataID"],
                                @"nBlockID" :[self.dictLineToShow valueForKey:@"nBlockID"],
                                @"nRowNo" :[self.dictLineToShow valueForKey:@"nRowNo"],
                                @"nSheetID" :[self.dictLineToShow valueForKey:@"nSheetID"],
                                @"nBlockNo" :[self.dictLineToShow valueForKey:@"nBlockNo"],
                                @"RowId":self.strRowIdtoReload,
                                };
            
        }
        
        BOOL isvalid = [self checkBlankValidation];
        if (isvalid) {
            ProgramViewController *progVCOBJ = [ProgramViewController sharedInstance];
            MESSAGE(@"Detailsdict: %@",Detailsdict);
            progVCOBJ.detailsExercise = Detailsdict;
            if (self.dictLineToShow == nil) {
                MESSAGE(@"self.dictLineToShow ----> 1");
                [_delegate addNewSheet:Detailsdict];
            }
            else{
                 MESSAGE(@"self.dictLineToShow ----> else 1");
                [self.delegate editNewLine:Detailsdict];
            }
            
            self.txtWeight.text = nil;
            self.txtExercise.text = nil;
            self.txtSet.text = nil;
            self.txtReps.text = nil;
            self.txtRest.text = nil;
            Detailsdict = nil;
            [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            DisplayAlertWithTitle(NSLocalizedString(@"Please Enter all values.", nil), kAppName);
        }
    }
}

//-----------------------------------------------------------------------

-(NSString*)getStringValueFor:(NSString*)strTemp{
    if (strTemp == nil || allTrim(strTemp).length == 0) {
        return @"0";
    }
    return strTemp;
}


//-----------------------------------------------------------------------

-(BOOL)checkBlankValidation {
    START_METHOD
    BOOL isValid = NO;
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *txt = (UITextField *)view;
            if (txt.text.length == 0) {
                isValid = NO;
                break;
            } else {
                isValid = YES;
            }
        }
    }
    return isValid;
}
//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}



//-----------------------------------------------------------------------

@end
