//
//  TemplateCustomeCell.h
//  T-Notebook
//
//  Created by WLI on 26/06/12.
//  Copyright (c) 2012 WLI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TemplateCustomeCell : UITableViewCell{
    UILabel         * _lblTemplateName;
    UILabel         * _lblSheetContained;
}
@property (nonatomic,strong)UILabel    * lblTemplateName;
@property (nonatomic,strong)UILabel    * lblSheetContained;


- (void) displayTableData :(NSDictionary *) dictInfo;

@end
