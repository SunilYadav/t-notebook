//
//  AssessmentViewController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "TextFileManager.h"
#import "AssessmentViewController.h"
#import "KeyboardControls.h"
#import "UIView+firstResponder.h"
#import "AddAssessmentPhotoVC.h"
#import "FileUtility.h"
#import "GraphFullScreenVC.h"
#import "AppDelegate.h"
#import "AddNameVC.h"
#import "CStoreKit.h"
#import "UIImage-Extensions.h"


#define kOFFSET_FOR_KEYBOARD 80.0
#define kDeleteBtnTag    5000
#define kphotoTitleTag    9000
#define kZoomBtnTag      6000

static const CGFloat kMinimumScrollOffsetPadding = 20;
@interface AssessmentViewController ()<KeyboardControlsDelegate,CStoreKitDelegate>

{
    
    __weak IBOutlet UITextView *textViewBMI;
    __weak IBOutlet UILabel *lblFitnessAssessmentName;
       __weak IBOutlet UIView *viewAssessment;

    
    __weak IBOutlet UILabel *lblAddPhoto5;
    __weak IBOutlet UILabel *lblAddPhoto4;
    __weak IBOutlet UILabel *lblAddPhoto3;
    __weak IBOutlet UILabel *lblAddPhoto2;
    __weak IBOutlet UILabel *lblAddPhoto1;
    
    __weak IBOutlet UIButton *btnForPreviousAssessmentPages;
    __weak IBOutlet UIButton *btnForNextAssessmentPages;
    __weak IBOutlet UIButton *btnAssessmentPage1;
    __weak IBOutlet UIButton *btnAssessmentPage2;
    __weak IBOutlet UIButton *btnAssessmentPage3;
    __weak IBOutlet UILabel *lblSheetName;
    
    
    
    __weak IBOutlet UILabel *lblBasicMeasurement;
    __weak IBOutlet UILabel *lblBMWeight;
    __weak IBOutlet UILabel *lblBMHeight;
    __weak IBOutlet UILabel *lblBMHeartRate;
    __weak IBOutlet UILabel *lblBMBloodPresure;
    
    
    __weak IBOutlet UITextField		* txtBMHeight;
    __weak IBOutlet UITextField		* txtBMWeight;
    __weak IBOutlet UITextField		* txtBMHeartRate;
    __weak IBOutlet UITextField		* txtBMBloodPressure;
    
    
    
    __weak IBOutlet UILabel *lblBodyFatPer;
    __weak IBOutlet UILabel *lbl3STriceps;
    __weak IBOutlet UILabel *lbl3sSuprailiac;
    __weak IBOutlet UILabel *lbl3sThigh;
    __weak IBOutlet UILabel *lbl3sResult;
    
    
    
    __weak IBOutlet UITextField		* txt3STriceps;
    __weak IBOutlet UITextField		* txt3SSuprailiac;
    __weak IBOutlet UITextField		* txt3SThigh;
    __weak IBOutlet UITextField		* txt3SSum;
    
    // --
    __weak IBOutlet UILabel *lbl3Site;
    
    __weak IBOutlet UILabel *lbl4Site;
    
    __weak IBOutlet UILabel *lbl4SAdbomen;
    
    __weak IBOutlet UILabel *lbl4SSupariliac;
    __weak IBOutlet UILabel *lbl4STriceps;
    __weak IBOutlet UILabel *lbl4STHigh;
    __weak IBOutlet UILabel *lbl4SResult;
    
    
    
    __weak IBOutlet UITextField		* txt4STriceps;
    __weak IBOutlet UITextField		* txt4SAbdomen;
    __weak IBOutlet UITextField		* txt4SIllium;
    __weak IBOutlet UITextField		* txt4SThigh;
    __weak IBOutlet UITextField		* txt4SSum;
    
    // ---
    __weak IBOutlet UILabel *lbl7Site;
    __weak IBOutlet UILabel *lbl7SChest;
    __weak IBOutlet UILabel *lbl7sAbdominal;
    __weak IBOutlet UILabel *lbl7STHigh;
    __weak IBOutlet UILabel *lbl7STriceps;
    __weak IBOutlet UILabel *lbl7sSubscapullar;
    __weak IBOutlet UILabel *lbl7sSuparlic;
    __weak IBOutlet UILabel *lbl7sMidaxillary;
    __weak IBOutlet UILabel *lbl7sResult;
    
    
    
    
    __weak IBOutlet UITextField		* txt7SChest;
    __weak IBOutlet UITextField		* txt7SAbdominal;
    __weak IBOutlet UITextField		* txt7SThig;
    __weak IBOutlet UITextField		* txt7STriceps;
    __weak IBOutlet UITextField		* txt7SSubscapular;
    __weak IBOutlet UITextField		* txt7SSuparilic;
    __weak IBOutlet UITextField		* txt7SMidaxilari;
    __weak IBOutlet UITextField		* txt7SSum;
    
    
    __weak IBOutlet UILabel *lblGenralBodyFat;
    //-----
    __weak IBOutlet UILabel *lblCircumfrenceMesurements;
    
    __weak IBOutlet UILabel *lblCmRightUpperArm;
    __weak IBOutlet UILabel *lblCmRightForeArm;
    __weak IBOutlet UILabel *lblCmChest;
    __weak IBOutlet UILabel *lblCmAbdominal;
    __weak IBOutlet UILabel *lblCmHip;
    __weak IBOutlet UILabel *lblCmRightThigh;
    __weak IBOutlet UILabel *lblCmRightCalf;
    
    
    
    __weak IBOutlet UITextField		* txtCmRightUpeerArm;
    __weak IBOutlet UITextField		* txtCmRightForeArm;
    __weak IBOutlet UITextField		* txtCmChest;
    __weak IBOutlet UITextField		* txtCmAbdominal;
    __weak IBOutlet UITextField		* txtCmHipButtocks;
    __weak IBOutlet UITextField		* txtCmRightThigh;
    __weak IBOutlet UITextField		* txtCmRightCalf;
    
    __weak IBOutlet UITextField		* txtBMIHeight;
    __weak IBOutlet UITextField		* txtBMIWeight;
    __weak IBOutlet UITextField		* txtBMIResult;
    __weak IBOutlet UITextField     *txtGeneralBodyFat;
    __weak IBOutlet UITextField     *txtKeyExtra;
    __weak IBOutlet UITextField     *txtValueExtra;
    
    //--
    
    __weak IBOutlet UILabel *lblBodyMass;
    
    __weak IBOutlet UILabel *lblBMIResult;
    
    __weak IBOutlet UIButton         *btnStandard;
    __weak IBOutlet UIButton         *btnMetric;
    __weak IBOutlet UILabel          *lblBMIHeight;
    __weak IBOutlet UILabel          *lblBMIWeight;
    __weak IBOutlet UILabel          *lblTriceps;
    __weak IBOutlet UILabel          *lblSupraialiac;
    
    __weak IBOutlet UIView          *viewWeightTracker;
    __weak IBOutlet UIView          *viewBodyFatTracker;
    __weak IBOutlet UIView          *viewCircumferenceMeasurements;
    __weak IBOutlet UIView          *viewBMITracker;
    __weak IBOutlet UIView          *viewGeneralBodyFatTracker;
    
    __weak IBOutlet UIWebView       *webViewBodyFatPercentage;
    __weak IBOutlet UITextView      *txtViewGoals;
    
    __weak IBOutlet UIButton        *btnnewAssessment;
    __weak IBOutlet UIButton        *btnDate;
    __weak IBOutlet UILabel         *lblDate;
    __weak IBOutlet UIImageView     *imgInnerBackground;
    __weak IBOutlet UIButton        *btnAddnewAssessment;
    
    __weak IBOutlet UILabel *lblWeightTracker;
    __weak IBOutlet UILabel *lblBodyFatTracker;
    __weak IBOutlet UILabel *lblGeneralBodyFatTracker;
    __weak IBOutlet UILabel *lblCircumfrenceMeasurements;
    __weak IBOutlet UILabel *lblBMITracker;
    
    __weak IBOutlet UILabel *lblBodyMassIndex;
    __weak IBOutlet UILabel *lblUnderWeight;
    __weak IBOutlet UILabel *lblBelow;
    __weak IBOutlet UILabel *lblNormal;
    __weak IBOutlet UILabel *lblObesity;
    __weak IBOutlet UILabel *lblOverWeight;
    __weak IBOutlet UILabel *lblAbove;
    __weak IBOutlet UILabel *lblGoals;
    
    
    NSArray *arraySheets;
    NSArray *arrayImages;
    
    BOOL autoChangePageIfNextPageClicked;
    
    NSString *strStandardHeight,*strStandardWeight,*strMetricHeight,*strMetricWeight;
    

    //Sunil
    __weak IBOutlet UIView *viewBasicMeasurement;
    __weak IBOutlet UIImageView *imgViewBasivMesurementTitle;
    
    __weak IBOutlet UIView *viewBodyFatPercentage;
    __weak IBOutlet UIImageView *imgViewBodyFatPercentageTitle;
    
    __weak IBOutlet UIView *viewgeneralBodyFat;
    __weak IBOutlet UIImageView *imgViewgeneralBodyFatTitle;
    
    __weak IBOutlet UIView *viewCircumMeasur;
    __weak IBOutlet UIImageView *imgviewCircumMeasurTitle;
    
    
    __weak IBOutlet UIView *viewBodyMassage;
    
    __weak IBOutlet UIImageView *imgViewBodyMassageTitle;
    
    
    __weak IBOutlet UIView *viewBodyMassage1;
    
    __weak IBOutlet UIImageView *imgViewBodyMassage1Title;
    
    __weak IBOutlet UIView *viewWieghtTracker1;
    
    __weak IBOutlet UIImageView *imgViewWieghtTracker1Title;
    __weak IBOutlet UIView *viewBodyFatTracker1;
    
    __weak IBOutlet UIImageView *imgViewbodyFatTrackerTitle;
   
    __weak IBOutlet UIView *viewGernalBodyFatTracker1;
    
    __weak IBOutlet UIImageView *imgViewGernalBodyFatTracker1title;

    __weak IBOutlet UIView *viewCircumfrenceMeasure1;
    
    __weak IBOutlet UIImageView *imgViewCircumfrenceMeasure1Title;
    
    __weak IBOutlet UIView *viewBmiTracker1;
    
    __weak IBOutlet UIImageView *imgViewBMITrackerTitlw;
    
    __weak IBOutlet UIView *viewBodyMassIndexResult;
    
    __weak IBOutlet UIImageView *imgViewBodyMassIndexResultTitle;
    __weak IBOutlet UIView *viewGoals;
    __weak IBOutlet UIImageView *imgViewGoals;
}

@property (nonatomic, strong) KeyboardControls              *keyboardControls;
@property (nonatomic) NSInteger                               deleteImgTag;
@property (nonatomic) BOOL                                  isLandscapeMode;
@end

@implementation AssessmentViewController
@synthesize currentSheet;
@synthesize currentPage;
static UIView *viewCommonAssessment = nil;

static UIPopoverController *popoverController = nil;
static UIDatePicker *datePicker = nil;

//-----------------------------------------------------------------------

#pragma mark - Memory Methods

//-----------------------------------------------------------------------

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

//-----------------------------------------------------------------------

#pragma mark - AddPhoto Delegate Methods

//-----------------------------------------------------------------------

-(void)setNewImageToAssessmentsWithImag:(UIImage*)image withTitle:(NSString*)photoTitle andWithPhotoButtonTag:(NSInteger)photoButtonTag{
    START_METHOD

    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];

    
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke method for save the assessment image on server
        [self postRequestForAssessmentImage:image withTitle:photoTitle andWithPhotoButtonTag:photoButtonTag];
        
    }else{
        
        //Invoke method for save the assessment image
        [self saveAssessmentImageInLocal:image withTitle:photoTitle andWithPhotoButtonTag:photoButtonTag];
    
    }
    
    //Invoke method for save the assessment image
//    [self saveAssessmentImageInLocal:image withTitle:photoTitle andWithPhotoButtonTag:photoButtonTag];
    
    END_METHOD
}

//-----------------------------------------------------------------------

#pragma mark - Action Methods

//-----------------------------------------------------------------------

- (IBAction)btnViewFullGraphClicked:(UIButton *)sender {
    
    
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        DisplayAlertWithYesNo(kliteMsg, kAppName, self);
        return;
    }
    
    UIStoryboard*  storybBoard = [UIStoryboard storyboardWithName:@"Main"
                                                           bundle:nil];
    GraphFullScreenVC * graphFullScreenObj = [storybBoard  instantiateViewControllerWithIdentifier:@"GraphFullScreenView"];
    graphFullScreenObj.strClientID = self.strClientID;
    graphFullScreenObj.chartTag = [NSNumber numberWithInteger:(NSInteger)sender.tag];
    graphFullScreenObj.strAxies = [self stringForXaxies];
    [graphFullScreenObj setModalPresentationStyle:UIModalPresentationFullScreen];
    
    UINavigationController *navcGraphViewController = [[UINavigationController alloc]initWithRootViewController:graphFullScreenObj];
    AppDelegate *delegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate.window.rootViewController presentViewController:navcGraphViewController animated:YES completion:^{
    }];
    
}

//-----------------------------------------------------------------------

- (IBAction)btnPhotoClicked:(UIButton*)sender {
    
    START_METHOD
    [self.assmntDelegate hideOptionsViewIfShowing];
    
    UIStoryboard*  storybBoard = [UIStoryboard storyboardWithName:@"Main"
                                                           bundle:nil];
    AddAssessmentPhotoVC* addAssessmentPhotoVC = [storybBoard instantiateViewControllerWithIdentifier:@"AddAssessmentPhotoView"];
    addAssessmentPhotoVC.imageAddPhoto = nil;
    addAssessmentPhotoVC.photoTag = sender.tag;
    addAssessmentPhotoVC.delegate = (id)self;

    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
    NSString * dirPath = [NSString stringWithFormat:@"%@/%@/",[FileUtility basePath],imageFolder];
    arrayImages = [self getAllAssessmentImagesWithSheetId:sheetId];
    
    NSDictionary * dictInfo = [arrayImages objectAtIndex:sender.tag-101];
    
    
    MESSAGE(@"dictInfo===> %@",dictInfo);
    
    
    int intTag = [[dictInfo objectForKey:kImageID] intValue];
    
    int intReminder = intTag%5;
    
    if (intReminder==0) {
        
        intReminder = 5;
    }
    
    NSString * strPath = [NSString stringWithFormat:@"%@%d.png",dirPath,intReminder];
    if ([FileUtility fileExists:strPath]) {
        
        
        MESSAGE(@"[UIImage imageWithContentsOfFile:strPath]: %@   AND strPath: %@",[UIImage imageWithContentsOfFile:strPath],strPath);
        
        
        addAssessmentPhotoVC.imageAddPhoto = [UIImage imageWithContentsOfFile:strPath];
    }
    if ([[[arrayImages objectAtIndex:sender.tag-101] valueForKey:@"sImageLabel"] length]>0) {
        addAssessmentPhotoVC.strImageTitle = [[arrayImages objectAtIndex:sender.tag-101] valueForKey:@"sImageLabel"];
    }
    
    UINavigationController *editProfileVC = [[UINavigationController alloc]initWithRootViewController:addAssessmentPhotoVC];
    [editProfileVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [editProfileVC setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    AppDelegate *delegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate.window.rootViewController presentViewController:editProfileVC animated:YES completion:^{
        
    }];
    
}
//-----------------------------------------------------------------------

- (IBAction)btnDateClicked:(id)sender {
    
    
    if (popoverController == nil) {
        
        datePicker = nil;
        UIViewController* popoverContent = [[UIViewController alloc] init];
        UIView*  popoverView = [[UIView alloc] init];
        popoverView.backgroundColor = [UIColor whiteColor];
        
        datePicker=[[UIDatePicker alloc]init];
        datePicker.frame=CGRectMake(0,44,320, 216);
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setMinuteInterval:5];
        [datePicker setTag:10];
        [popoverView addSubview:datePicker];
        
        UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
        [btnDone addTarget:self action:@selector(seDateFromDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDone.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
        btnDone.frame = CGRectMake(5, 7, 60, 30);
        btnDone.layer.cornerRadius = 4.0;
        btnDone.backgroundColor = kAppTintColor;
        
       
        [popoverView addSubview:btnDone];
        
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 4, 320, 36)];
        [lblTitle setText:NSLocalizedString(@"Select Date", nil)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        
        
        [lblTitle setTextColor:kAppTintColor];
        lblTitle.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
        [popoverView addSubview:lblTitle];
        
        UILabel *lblLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 42, 320, 1)];
        [lblLine setBackgroundColor:kAppTintColor];
        [popoverView addSubview:lblLine];
        
        popoverContent.view = popoverView;
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
        popoverController.delegate=(id)self;
        
    }
    
    
    [popoverController setPopoverContentSize:CGSizeMake(320, 264) animated:NO];
    
    if([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
        popoverController = nil;
    } else {
        [popoverController presentPopoverFromRect:btnDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES]; //btnSheetDate.frame
    }
    
    
 
}

//-----------------------------------------------------------------------

- (IBAction)btnBMIChangeUnitsClicked:(UIButton*)sender {
    
    
    if ((!btnStandard.selected) &&  sender.tag == 1) {
        
        [btnMetric setSelected:false];
        [btnStandard setSelected:true];
        lblBMIHeight.text = NSLocalizedString(@"HEIGHT (IN FEET)", nil);
        lblBMIWeight.text = NSLocalizedString(@"WEIGHT (IN POUNDS)", nil);
        
        btnMetric.backgroundColor = [UIColor clearColor];
        [btnMetric setTitleColor:kAppTintColor forState:UIControlStateNormal];
        btnMetric.layer.borderColor = kAppTintColor.CGColor;
        btnMetric.layer.borderWidth = 2.0f;
        
        btnStandard.backgroundColor = kAppTintColor;
        [btnStandard setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnStandard.layer.borderWidth = 0.0f;
        
        strMetricHeight = txtBMIHeight.text ;
        strMetricWeight = txtBMIWeight.text;
        
        txtBMIHeight.text = strStandardHeight;
        txtBMIWeight.text = strStandardWeight;
        
        //Sunil invoke method
        [self setButtotnMetricSeledted];
        
    }
    else if ((!btnMetric.selected) && sender.tag == 2){
        
        [btnMetric setSelected:true];
        [btnStandard setSelected:false];
        lblBMIHeight.text = NSLocalizedString(@"HEIGHT (IN CMS)", nil);
        lblBMIWeight.text = NSLocalizedString(@"WEIGHT (IN KGS)", nil);
        
        btnStandard.backgroundColor = [UIColor clearColor];
        [btnStandard setTitleColor:kAppTintColor forState:UIControlStateNormal];
        btnStandard.layer.borderColor = kAppTintColor.CGColor;
        btnStandard.layer.borderWidth = 2.0f;
        
        btnMetric.backgroundColor = kAppTintColor;
        [btnMetric setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnMetric.layer.borderWidth = 0.0f;
        
        strStandardHeight = txtBMIHeight.text ;
        strStandardWeight = txtBMIWeight.text;
        
        txtBMIHeight.text = strMetricHeight;
        txtBMIWeight.text = strMetricWeight;
        
        //Sunil invoke method
        [self setButtotnStandardSeledted];
        
    }
    
    [self caluclateAndSetBMIResult];
}

//-----------------------------------------------------------------------

- (IBAction)btnForPreviousAssessmentPagesClicked:(id)sender {
    
    if (currentPage>0) {
        currentPage--;
        btnAssessmentPage1.hidden = false;
        btnAssessmentPage2.hidden = false;
        btnAssessmentPage3.hidden = false;
        [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)(((currentPage)*3)+1)] forState:UIControlStateNormal];
        [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+2)] forState:UIControlStateNormal];
        [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+3)] forState:UIControlStateNormal];
        
        currentSheet = (currentPage*3)+3;
        [self btnAssessmentPageClicked:btnAssessmentPage3];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self drawGraphForWeightTraker];
                [self drawGraphForBodyFatPercentage];
                [self drawGraphForBMITraker];
                [self drawGraphForCircumferenceMeasurement];
                [self drawGraphForGeneralBodyFatPercentageTracker];
            });
            
        });
    }
    
    if (currentPage == 0) {
    }
    
}

//-----------------------------------------------------------------------

- (IBAction)btnForNextAssessmentPagesClicked:(id)sender {
    
    NSInteger totalNumberOfPages  = arraySheets.count;
    NSInteger tempCurrentPage = currentPage+1;
    
    if (totalNumberOfPages > (tempCurrentPage*3)) {
        
        currentPage++;
        
        if (totalNumberOfPages-(currentPage*3) == 1) {
            btnAssessmentPage1.hidden = false;
            btnAssessmentPage2.hidden = true;
            btnAssessmentPage3.hidden = true;
            [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+1)] forState:UIControlStateNormal];
        }
        else if (totalNumberOfPages-(currentPage*3) == 2){
            btnAssessmentPage1.hidden = false;
            btnAssessmentPage2.hidden = false;
            btnAssessmentPage3.hidden = true;
            [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+1)] forState:UIControlStateNormal];
            [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+2)] forState:UIControlStateNormal];
        }
        else{
            btnAssessmentPage1.hidden = false;
            btnAssessmentPage2.hidden = false;
            btnAssessmentPage3.hidden = false;
            [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+1)] forState:UIControlStateNormal];
            [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+2)] forState:UIControlStateNormal];
            [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+3)] forState:UIControlStateNormal];
            
        }
        
        if (autoChangePageIfNextPageClicked) {
            currentSheet = (currentPage*3)+1;
            [self btnAssessmentPageClicked:btnAssessmentPage1];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self drawGraphForWeightTraker];
                    [self drawGraphForBodyFatPercentage];
                    [self drawGraphForBMITraker];
                    [self drawGraphForCircumferenceMeasurement];
                    [self drawGraphForGeneralBodyFatPercentageTracker];
                });
                
            });
        }
        autoChangePageIfNextPageClicked = true;
    }
    
}

//-----------------------------------------------------------------------

- (IBAction)btnAssessmentPageClicked:(UIButton *)sender {
    
    currentSheet = [sender.currentTitle intValue];
    int intSkin = [commonUtility retrieveValue:KEY_SKIN].intValue;

    
    if (sender == btnAssessmentPage1) {
        
        switch (intSkin) {
            case FIRST_TYPE:
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"next_green-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"next_green-assessment_without-arrow.png"] forState:UIControlStateNormal];
                break;
                
            case SECOND_TYPE:
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"next_red-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"next_red-assessment_without-arrow.png"] forState:UIControlStateNormal];
                
                break;
                
            case THIRD_TYPE:
                
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"next_pink-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"next_pink-assessment_without-arrow.png"] forState:UIControlStateNormal];
                
                break;
                
                
            default:
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
                break;
        }
        
        
        [btnAssessmentPage1 setImage:[UIImage imageNamed:@"page-number"] forState:UIControlStateNormal];

    }
    else if (sender == btnAssessmentPage2){
        
        
        //Sunil Change Bugtton color of selected on asesement for page

        switch (intSkin) {
            case FIRST_TYPE:
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"next_green-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"next_green-assessment_without-arrow.png"] forState:UIControlStateNormal];
                break;
                
            case SECOND_TYPE:
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"next_red-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"next_red-assessment_without-arrow.png"] forState:UIControlStateNormal];
                
                break;
                
            case THIRD_TYPE:
                
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"next_pink-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"next_pink-assessment_without-arrow.png"] forState:UIControlStateNormal];
                
                break;
                
                
            default:
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
                [btnAssessmentPage3 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
                break;
        }
        
        
        [btnAssessmentPage2 setImage:[UIImage imageNamed:@"page-number"] forState:UIControlStateNormal];

        
    }
    else if (sender == btnAssessmentPage3){
        
        
        //Sunil Change Bugtton color of selected on asesement for page
        
        switch (intSkin) {
            case FIRST_TYPE:
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"next_green-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"next_green-assessment_without-arrow.png"] forState:UIControlStateNormal];
                break;
                
            case SECOND_TYPE:
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"next_red-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"next_red-assessment_without-arrow.png"] forState:UIControlStateNormal];
                
                break;
                
            case THIRD_TYPE:
         
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"next_pink-assessment_without-arrow.png"] forState:UIControlStateNormal];
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"next_pink-assessment_without-arrow.png"] forState:UIControlStateNormal];
                
                break;
                
                
            default:
                [btnAssessmentPage1 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
                [btnAssessmentPage2 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
                break;
        }
        
      
        [btnAssessmentPage3 setImage:[UIImage imageNamed:@"page-number"] forState:UIControlStateNormal];
        
    }
    [self setAssessmentDataForSheet];
}

//-----------------------------------------------------------------------

- (IBAction)btnPhotosClicked:(id)sender {
    
    [self.assmntDelegate hideOptionsViewIfShowing];
    
    if (![self.btnPhotos isSelected]) {
        [[self.view findFirstResponder] resignFirstResponder];
        
        [self.btnPhotos setSelected:true];
        
        
        //Sunil
        [self setButtonPhotosSelectionIcons];
        
        
        
        [self.view bringSubviewToFront:self.btnPhotos];
        
        [viewCommonAssessment removeFromSuperview];
        viewCommonAssessment.frame = CGRectMake(50, 0, viewCommonAssessment.frame.size.width, viewCommonAssessment.frame.size.height);
        [self.scrollViewData addSubview:viewCommonAssessment];
        
        viewAssessment.hidden = true;
        self.viewPhotos.hidden = false;
    }
    
    
}

//-----------------------------------------------------------------------

- (IBAction)btnAssesmetsClicked:(id)sender {
    
    START_METHOD
    [self.assmntDelegate hideOptionsViewIfShowing];
    
    if (![self.btnAssessment isSelected]) {
        
        [self.btnPhotos setSelected:false];
        [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        //Sunil
        [self setButtonAssesmentSelectionIcons];
        
        viewAssessment.hidden = false;
        self.viewPhotos.hidden = true;
        
        [self.view bringSubviewToFront:self.btnAssessment];
        
        [viewCommonAssessment removeFromSuperview];
        viewCommonAssessment.frame = CGRectMake(45, 0, viewCommonAssessment.frame.size.width, viewCommonAssessment.frame.size.height);
        [self.scrollViewAssesmet addSubview:viewCommonAssessment];
        
        
    }
}

//-----------------------------------------------------------------------

- (IBAction)btnAddNewAssessmentClicked:(id)sender {
    
    // Add new Sheet for assessment
    
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        if([arraySheets count] >=1) {
            DisplayAlertWithYesNo(kliteMsg, kAppName, self);
            return;
        }
    }
    
    AddNameVC *objAddNameVC = (AddNameVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddNameView"];
    objAddNameVC.strScreenName = @"EnterSheetName";
    objAddNameVC.strFiledContent = nil;
    objAddNameVC.delegate = (id)self;
    objAddNameVC.isUpdate = false;
    UINavigationController *modalController = [[UINavigationController alloc]initWithRootViewController:objAddNameVC];
    modalController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    modalController.modalPresentationStyle =  UIModalPresentationFormSheet;
    
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(350, 160);
        modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    
    [self.navigationController presentViewController:modalController animated:YES completion:^{
    }];
    
}

//-----------------------------------------------------------------------

- (IBAction)btnEditAssessmentSheetNameClicked:(id)sender {
    
    START_METHOD
    AddNameVC *objAddNameVC = (AddNameVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddNameView"];
    objAddNameVC.strScreenName = @"EnterSheetName";
    objAddNameVC.strFiledContent = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    objAddNameVC.delegate = (id)self;
    objAddNameVC.isUpdate = true;
    UINavigationController *modalController = [[UINavigationController alloc]initWithRootViewController:objAddNameVC];
    modalController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    modalController.modalPresentationStyle =  UIModalPresentationFormSheet;
    
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(350, 160);
        modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    
    [self.navigationController presentViewController:modalController animated:YES completion:^{
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)btnDeleteAssessmentPhoto:(id)sender {
    
    UIButton *deletBtn = (UIButton *) sender;
    
    self.deleteImgTag = deletBtn.tag;
    
    UIAlertView * alertShow = [[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Are you sure you want to delete this image?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes", nil),NSLocalizedString(@"No", nil),nil];
    alertShow.tag = 900;
    [alertShow setCancelButtonIndex:1];
    [alertShow show];
    
}


//---------------------------------------------------------------------------------------------------------------

-(IBAction)openFullScreenImage:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    NSInteger picTag = btn.tag - kZoomBtnTag;
    
    UIStoryboard*  storybBoard = [UIStoryboard storyboardWithName:@"Main"
                                                           bundle:nil];
    AddAssessmentPhotoVC* addAssessmentPhotoVC = [storybBoard instantiateViewControllerWithIdentifier:@"AddAssessmentPhotoView"];
    addAssessmentPhotoVC.imageAddPhoto = nil;
    addAssessmentPhotoVC.photoTag = picTag;
    addAssessmentPhotoVC.delegate = (id)self;
    
    //Set Image if Already exists.
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)currentSheet];
    NSString * dirPath = [NSString stringWithFormat:@"%@/%@/",[FileUtility basePath],imageFolder];
    arrayImages = [self getAllAssessmentImages];
    NSDictionary * dictInfo = [arrayImages objectAtIndex:picTag-101];
    NSString * strPath = [NSString stringWithFormat:@"%@%@.png",dirPath,[dictInfo objectForKey:kImageID]];
    if ([FileUtility fileExists:strPath]) {
        addAssessmentPhotoVC.imageAddPhoto = [UIImage imageWithContentsOfFile:strPath];
    }
    if ([[[arrayImages objectAtIndex:picTag-101] valueForKey:@"sImageLabel"] length]>0) {
        addAssessmentPhotoVC.strImageTitle = [[arrayImages objectAtIndex:picTag-101] valueForKey:@"sImageLabel"];
    }
    
    UINavigationController *editProfileVC = [[UINavigationController alloc]initWithRootViewController:addAssessmentPhotoVC];
    [editProfileVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [editProfileVC setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    AppDelegate *delegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate.window.rootViewController presentViewController:editProfileVC animated:YES completion:^{
        
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)deleteClientAssessmentSheet:(id)sender {
    
    
    NSString *strMsg = [NSString stringWithFormat:@"Are you sure you want to delete %@ Sheet ?",lblSheetName.text];
    UIAlertView *delAlertView = [[UIAlertView alloc]initWithTitle:kAppName message:strMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes", nil),NSLocalizedString(@"No", nil), nil];
    delAlertView.tag = 9000;
    [delAlertView show];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event  {
    
    [self.assmntDelegate hideOptionsViewIfShowing];
}


//-----------------------------------------------------------------------

#pragma mark - Add Edit Sheet Delegate

//-----------------------------------------------------------------------

-(void)createOrUpdateSheetForName:(NSString*)strSheetName isUpdate:(BOOL)isUpadate{
    
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];

    if (isUpadate) {
        
        NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
        
        NSInteger t = [[DataManager initDB] updateAssessmentSheetNameFor:self.strClientID ForSheetID:sheetId toSheetName:strSheetName];
        if (t == 0) {
            DebugLOG(@"Data updated");
            [self getAllSheetsList];
            
            
 //TODO: SUNIL UPDATE Assessment name
            [self postRequestForUpdateSheetName];
        }
        else {
            DebugLOG(@"Data Not updated");
        }
    }
    else{
        
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            //TODO: SUNIL Add new Assessment
            [self postRequestForAddNewAssessment:strSheetName];
            
        }else{
            
            [self createNewAssesmentSheetForName:strSheetName andDict:nil];
            
        }
        
    }
    
}

//-----------------------------------------------------------------------

#pragma mark - UIAlertView Delegate Method

//-----------------------------------------------------------------------

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    int t = (int)alertView.tag;
    if (buttonIndex == 0 && t == 5000) {
        
        [[AppDelegate sharedInstance] showProgressAlert:NSLocalizedString(@"Purchase in progress", nil)];
        [[CStoreKit sharedInstance]buy:kFullAccessProductId delegate:(id)self];
    }
    
    if(alertView.tag == 9000) {
        if(buttonIndex == 0) {
            
            
            
            //Get Upgrade status
            NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            //If not Upgrade then show alert for upgrade
            if([strUpgradeStatus isEqualToString:@"YES"]){
                

                
                //Create dictionaryb for send param in api for Trainer's Profile
                NSDictionary *params = @{
                                         kClientId              :   self.strClientID,
                                         ACTION                 :   @"deleteAssessment",
                                         @"client_assessment_id"               :   [[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"]
                                         };
                
                //Invoke method for post request to delete client
                [self postRequestForDeleteAssessment:params];
                
            }else{
                
                //Invoke method for delete ASSESSMENT SHEET detail
                [self deleteAllAssessmentDataForClient];
                
            }
            
        }
    }
    
    if (alertView.tag == 400 && buttonIndex == 1) {
        [self createNewAssesmentSheetForName:((UITextField*)[alertView textFieldAtIndex:0]).text andDict:nil] ;
    }
    else if (alertView.tag == 500 && buttonIndex == 1) {
        NSInteger t = [[DataManager initDB] updateAssessmentSheetNameFor:self.strClientID ForSheetID:currentSheet toSheetName:((UITextField*)[alertView textFieldAtIndex:0]).text];
        if (t == 0) {
            DebugLOG(@"Data updated");
            [self getAllSheetsList];
        }
        else {
            DebugLOG(@"Data Not updated");
        }
    }else if (alertView.tag == 900 && buttonIndex == 0) {
        
        
        [self deleteImageOfAssessment];
        
    }
    
    //For PUrchase a new subcription plan
    if (t == 903) {
        
        if(buttonIndex==0){
            
            //Open the web app of T-notebook for purchase new subcription
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://52.26.135.139/index.php/welcome/"]];
            
            return;
          
            
        }
    }
    
}

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------


- (void) localizeControls {
    
    lblAddPhoto1.text = NSLocalizedString(@"ADD PHOTO", nil);
    lblAddPhoto2.text = NSLocalizedString(@"ADD PHOTO", nil);
    lblAddPhoto3.text = NSLocalizedString(@"ADD PHOTO", nil);
    lblAddPhoto4.text = NSLocalizedString(@"ADD PHOTO", nil);
    lblAddPhoto5.text = NSLocalizedString(@"ADD PHOTO", nil);
    
    lblBasicMeasurement.text = NSLocalizedString(lblBasicMeasurement.text, nil);
    lblBMWeight.text =  NSLocalizedString(lblBMWeight.text, nil);
    lblBMHeight.text =  NSLocalizedString(lblBMHeight.text, nil);
    lblBMBloodPresure.text =  NSLocalizedString(lblBMBloodPresure.text, nil);
    lblBMHeartRate.text =  NSLocalizedString(lblBMHeartRate.text, nil);
    
    
    lblBodyFatPer.text = NSLocalizedString(lblBodyFatPer.text, nil);
    lbl3Site.text =  NSLocalizedString(lbl3Site.text, nil);
    lbl4Site.text =  NSLocalizedString(lbl4Site.text, nil);
    lbl3STriceps.text = NSLocalizedString(lbl3STriceps.text, nil);
    lbl3sSuprailiac.text = NSLocalizedString(lbl3sSuprailiac.text, nil);
    lbl3sThigh.text =  NSLocalizedString(lbl3sThigh.text, nil);
    lbl3sResult.text =  NSLocalizedString(lbl3sResult.text, nil);
    
    
    lbl4SAdbomen.text = NSLocalizedString(lbl4SAdbomen.text, nil);
    lbl4SResult.text =  NSLocalizedString(lbl4SResult.text, nil);
    lbl4SSupariliac.text =  NSLocalizedString(lbl4SSupariliac.text, nil);
    lbl4STHigh.text = NSLocalizedString(lbl4STHigh.text, nil);
    lbl4STriceps.text = NSLocalizedString(lbl4STriceps.text, nil);
    
    
    lbl7sAbdominal.text = NSLocalizedString(lbl4SAdbomen.text, nil);
    lbl7SChest.text =  NSLocalizedString(lbl7SChest.text, nil);
    lbl7Site.text =  NSLocalizedString(lbl7Site.text, nil);
    lbl7sMidaxillary.text = NSLocalizedString(lbl7sMidaxillary.text, nil);
    lbl7sResult.text = NSLocalizedString(lbl7sResult.text, nil);
    lbl7sSubscapullar.text =  NSLocalizedString(lbl7sSubscapullar.text, nil);
    lbl7sSuparlic.text =  NSLocalizedString(lbl7sSuparlic.text, nil);
    lbl7STHigh.text = NSLocalizedString(lbl7STHigh.text, nil);
    lbl7STriceps.text = NSLocalizedString(lbl7STriceps.text, nil);
    lbl7Site.text = NSLocalizedString(lbl7Site.text, nil);
    
    lblGenralBodyFat.text = NSLocalizedString(lblGenralBodyFat.text, nil);
    
    lblCircumfrenceMesurements.text = NSLocalizedString(lblCircumfrenceMesurements.text, nil);
    lblCmRightUpperArm.text = NSLocalizedString(lblCmRightUpperArm.text, nil);
    lblCmRightForeArm.text =  NSLocalizedString(lblCmRightForeArm.text, nil);
    lblCmChest.text =  NSLocalizedString(lblCmChest.text, nil);
    lblCmAbdominal.text = NSLocalizedString(lblCmAbdominal.text, nil);
    lblCmHip.text = NSLocalizedString(lblCmHip.text, nil);
    lblCmRightThigh.text =  NSLocalizedString(lblCmRightThigh.text, nil);
    lblCmRightCalf.text =  NSLocalizedString(lblCmRightCalf.text, nil);
    txtKeyExtra.text = NSLocalizedString(lblCmRightCalf.text, nil);
    
    lblBodyMass.text = NSLocalizedString(lblBodyMass.text, nil);
    
    [btnStandard setTitle:NSLocalizedString(btnStandard.titleLabel.text, nil) forState:UIControlStateSelected ];
    [btnStandard setTitle:NSLocalizedString(btnStandard.titleLabel.text, nil) forState:0 ];
    
    [btnMetric  setTitle:NSLocalizedString(btnMetric.titleLabel.text, nil) forState:0];
    [btnMetric  setTitle:NSLocalizedString(btnMetric.titleLabel.text, nil) forState:UIControlStateSelected];
    lblBMIHeight.text = NSLocalizedString(lblBMIHeight.text, nil);
    lblBMIWeight.text = NSLocalizedString(lblBMIWeight.text, nil);
    lblBMIResult.text = NSLocalizedString(lblBMIResult.text, nil);
    
    [self.btnPhotos setTitle:NSLocalizedString(self.btnPhotos.titleLabel.text, nil) forState:0];
    [self.btnPhotos setTitle:NSLocalizedString(self.btnPhotos.titleLabel.text, nil) forState:UIControlStateSelected];
    [self.btnAssessment setTitle:NSLocalizedString(self.btnAssessment.titleLabel.text, nil) forState:0];
    [self.btnAssessment setTitle:NSLocalizedString(self.btnAssessment.titleLabel.text, nil) forState:UIControlStateSelected];
    lblDate.text = NSLocalizedString(lblDate.text, nil);
    [btnAddnewAssessment setTitle:NSLocalizedString(btnAddnewAssessment.titleLabel.text, nil) forState:0];
    
    lblWeightTracker.text = NSLocalizedString(lblWeightTracker.text, nil);
    lblBodyFatTracker.text = NSLocalizedString(lblBodyFatTracker.text, nil);
    lblGeneralBodyFatTracker.text = NSLocalizedString(lblGeneralBodyFatTracker.text, nil);
    lblCircumfrenceMeasurements.text = NSLocalizedString(lblCircumfrenceMeasurements.text, nil);
    lblBMITracker.text = NSLocalizedString(lblBMITracker.text, nil);
    lblBodyMassIndex.text = NSLocalizedString(lblBodyMassIndex.text, nil);
    lblUnderWeight.text = NSLocalizedString(lblUnderWeight.text, nil);
    lblBelow.text = NSLocalizedString(lblBelow.text, nil);
    lblNormal.text = NSLocalizedString(lblNormal.text, nil);
    lblOverWeight.text = NSLocalizedString(lblOverWeight.text, nil);
    lblObesity.text = NSLocalizedString(lblBodyFatTracker.text, nil);
    lblAbove.text = NSLocalizedString(lblAbove.text, nil);
    lblGoals.text = NSLocalizedString(lblGoals.text, nil);
    
    
    //Sunil Invoe for Set View
    [self setViewOnBasisSkin];

    //Sunil 18 April
    if([btnStandard isSelected]){
      
        [self setButtotnMetricSeledted];
    }
    
}

//-----------------------------------------------------------------------

- (void) getAllSheetsList {
    
    arraySheets = [[DataManager initDB] getAllAssessmentSheetsFor:self.strClientID];
    
}

//-----------------------------------------------------------------------

-(void)setSheetPageButtons{
    
    NSInteger numberOfSheets = arraySheets.count;
    
    if (numberOfSheets == 1) {
        btnAssessmentPage1.hidden = false;
        btnAssessmentPage2.hidden = true;
        btnAssessmentPage3.hidden = true;
    }
    else if (numberOfSheets == 2) {
        btnAssessmentPage1.hidden = false;
        btnAssessmentPage2.hidden = false;
        btnAssessmentPage3.hidden = true;
    }
    else{
        btnAssessmentPage1.hidden = false;
        btnAssessmentPage2.hidden = false;
        btnAssessmentPage3.hidden = false;
    }
    if (numberOfSheets<4) {
    }
}

//-----------------------------------------------------------------------

-(void)createNewAssesmentSheetForName:(NSString*)sheetName andDict:(NSDictionary *)dictAssessment{
    
    START_METHOD

    
    
    NSInteger sucess;
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
       
        
        sucess= [[DataManager initDB] insertNewAssessmentSheetCleintID:self.strClientID sheetName:[dictAssessment objectForKey:ksheetName] andSheetID:[dictAssessment objectForKey:@"nDetailID" ]];
    
    }else{
    
    sucess= [[DataManager initDB] insertNewAssessmentSheetFor:self.strClientID sheetName:sheetName];
}

    if (sucess == 0) {
        
        [self getAllSheetsList];
        currentSheet = [arraySheets count];
        
        
        if (currentSheet >3) {
            if (currentSheet/3 >0) {
                currentPage = currentSheet/3 -1;
            }
            
            if (currentSheet%3 == 0) {
                currentPage = currentPage-1;
                [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)currentSheet] forState:UIControlStateNormal];
                [self btnAssessmentPageClicked:btnAssessmentPage3];
            }
            else if (currentSheet%3 == 1) {
                [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)currentSheet] forState:UIControlStateNormal];
                [self btnAssessmentPageClicked:btnAssessmentPage1];
            }
            else if (currentSheet%3 == 2) {
                [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)currentSheet] forState:UIControlStateNormal];
                [self btnAssessmentPageClicked:btnAssessmentPage2];
            }
            
            autoChangePageIfNextPageClicked = false;
            [self btnForNextAssessmentPagesClicked:nil];
            
        }
        else{
            if (currentSheet%3 == 0) {
                btnAssessmentPage1.hidden = false;
                btnAssessmentPage2.hidden = false;
                btnAssessmentPage3.hidden = false;
                [self btnAssessmentPageClicked:btnAssessmentPage3];
            }
            else if (currentSheet%3 == 1) {
                btnAssessmentPage1.hidden = false;
                btnAssessmentPage2.hidden = true;
                btnAssessmentPage3.hidden = true;
                [self btnAssessmentPageClicked:btnAssessmentPage1];
            }
            else if (currentSheet%3 == 2) {
                btnAssessmentPage1.hidden = false;
                btnAssessmentPage2.hidden = false;
                btnAssessmentPage3.hidden = true;
                [self btnAssessmentPageClicked:btnAssessmentPage2];
            }
        }
        
        [self setAssessmentDataForSheet];
        [self drawGraphForWeightTraker];
        [self drawGraphForBodyFatPercentage];
        [self drawGraphForBMITraker];
        [self drawGraphForCircumferenceMeasurement];
        [self drawGraphForGeneralBodyFatPercentageTracker];
    }
    else {
        DebugLOG(@"Data Not added");
    }
    
    
}

//-----------------------------------------------------------------------

- (NSArray *) getAllAssessmentImages {
    
    NSArray * imagesArr = [[DataManager initDB] getAllAssessmentImagesForSheet:[NSString stringWithFormat:@"%ld",(long)currentSheet] ofClientID:self.strClientID];
    return imagesArr;
}


-(NSArray *)getAllAssessmentImagesWithSheetId:(NSInteger)sheetId {
    
    NSArray * imagesArr = [[DataManager initDB] getAllAssessmentImagesForSheet:[NSString stringWithFormat:@"%ld",(long)sheetId] ofClientID:self.strClientID];
    return imagesArr;
    
}


//-----------------------------------------------------------------------

- (void) setAssessmentImages {
   START_METHOD
    
    //Set by default all iamge set default
    [lblAddPhoto1 setHidden:FALSE];
    [lblAddPhoto2 setHidden:FALSE];
    [lblAddPhoto3 setHidden:FALSE];
    [lblAddPhoto4 setHidden:FALSE];
    [lblAddPhoto5 setHidden:FALSE];
    
    NSInteger sheetId;
    
    if ([arraySheets count] > (currentSheet - 1)){
        
        sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
        
    }else{
        
        sheetId = [[[arraySheets objectAtIndex:0] valueForKey:@"sheetID"] integerValue];
    }
    
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
    NSString * dirPath = [NSString stringWithFormat:@"%@/%@/",[FileUtility basePath],imageFolder];
    
    arrayImages = [self getAllAssessmentImagesWithSheetId:sheetId];
    
    MESSAGE(@"arrayImages---> %@",arrayImages);
    
    
    
    
    
    if([arrayImages count]>0) {
        
        for (int i = 0; i < [arrayImages count]; i++) {
            
            NSDictionary * dictInfo = [arrayImages objectAtIndex:i];
            
            
            int intTag = [[dictInfo objectForKey:kImageID] intValue];
            
            int intReminder = intTag%5;
            
            if (intReminder==0) {
                
                intReminder = 5;
            }
            

            
            
            NSString * strPath = [NSString stringWithFormat:@"%@%d.png",dirPath,intReminder];
           
            DebugLOG(strPath);
            if ([FileUtility fileExists:strPath]) {
                
                
                UIButton *btnAddPhoto = (UIButton*)[self.view viewWithTag:101+i];
                
                MESSAGE(@"btnAddPhoto--> %ld",(long)btnAddPhoto.tag);
                
                
                if (btnAddPhoto.tag == 101) {
                    
                    [lblAddPhoto1 setHidden:TRUE];
                } else if (btnAddPhoto.tag == 102) {
                    [lblAddPhoto2 setHidden:TRUE];
                } else if (btnAddPhoto.tag == 103) {
                    [lblAddPhoto3 setHidden:TRUE];
                } else if (btnAddPhoto.tag == 104) {
                    [lblAddPhoto4 setHidden:TRUE];
                } else if (btnAddPhoto.tag == 105) {
                    [lblAddPhoto5 setHidden:TRUE];
                }
                
                
                CGSize sizeforImg = CGSizeMake(211, 180);
                UIImage *img = [UIImage imageWithContentsOfFile:strPath];
                img = [img imageByScalingProportionallyToSize:sizeforImg];
                
                [btnAddPhoto setImage:img forState:UIControlStateNormal];
                [btnAddPhoto setImage:img forState:UIControlStateSelected
                 |UIControlStateHighlighted];
                
                [btnAddPhoto setTitle:@""  forState:UIControlStateNormal];
                
                
                NSString *imageTitle = [dictInfo objectForKey:@"sImageLabel"];
                
                [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+(101+i)] setHidden:FALSE];
                [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+(101+i)] setText:imageTitle];
                [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag +(101+i)] setHidden:FALSE];
                [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + (101+i)] setHidden:FALSE];
                
            } else{
                
                UIButton *btnAddPhoto = (UIButton*)[self.view viewWithTag:101+i];
                 //Sunil Set add Photo
                UIImage *image        =   [self setAddPhotos];
                [btnAddPhoto setImage:image forState:UIControlStateNormal];
                
               
                
                [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+(101+i)] setHidden:YES];
                [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+(101+i)] setText:@""];
                [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag +(101+i)] setHidden:YES];
                [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + (101+i)] setHidden:YES];
                
            }
            
        }
        
    } else {
        
        for (int i =0; i< 5; i++) {
            UIButton *btnAddPhoto = (UIButton*)[self.view viewWithTag:101+i];
            
            //Sunil Set add Photo
            UIImage *image        =   [self setAddPhotos];
            [btnAddPhoto setImage:image  forState:UIControlStateNormal];
            
            [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+(101+i)] setHidden:YES];
            [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+(101+i)] setText:@""];
            [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag +(101+i)] setHidden:YES];
            [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + (101+i)] setHidden:YES];
            
        }
        
    }
    
    
}

//-----------------------------------------------------------------------

-(void)setAllViewsToInitalState{
    
    autoChangePageIfNextPageClicked = YES;
    
    [self.btnPhotos setSelected:false];
    [self btnPhotosClicked:nil];
    

    btnStandard.selected = true;
    btnMetric.selected = false;
    btnStandard.layer.cornerRadius = 8.0;
    btnMetric.layer.cornerRadius = 8.0;
    
    btnStandard.backgroundColor = [UIColor colorWithRed:75/255.0f green:197/255.0f blue:181/255.0f alpha:1.0];
    [btnStandard setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btnMetric.backgroundColor = [UIColor clearColor];
    [btnMetric setTitleColor:[UIColor colorWithRed:75/255.0f green:197/255.0f blue:181/255.0f alpha:1.0] forState:UIControlStateNormal];
    btnMetric.layer.borderColor = [UIColor colorWithRed:75/255.0f green:197/255.0f blue:181/255.0f alpha:1.0].CGColor;
    btnMetric.layer.borderWidth = 2.0f;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btnPhotos.bounds
                                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerTopLeft)
                                                         cornerRadii:CGSizeMake(4.0, 4.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.btnPhotos.bounds;
    maskLayer.path = maskPath.CGPath;
    self.btnPhotos.layer.mask = maskLayer;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btnPhotos.bounds
                                     byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                           cornerRadii:CGSizeMake(4.0, 4.0)];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.btnAssessment.bounds;
    maskLayer1.path = maskPath.CGPath;
    self.btnAssessment.layer.mask = maskLayer1;
    
    txtKeyExtra.layer.sublayerTransform = CATransform3DMakeTranslation(5, 2, 0);
}

//-----------------------------------------------------------------------

- (void)autoScrollTextField:(UIView *)textField onScrollView:(UIScrollView *)scrollView {
    
    CGFloat visibleSpace = scrollView.bounds.size.height - scrollView.contentInset.top - scrollView.contentInset.bottom;
    CGPoint idealOffset;
    if(self.isLandscapeMode) {
        idealOffset = CGPointMake(0, [self getContentOffsetYpositionForView:textField
                                                      withViewingAreaHeight:visibleSpace-360 forscrollView:scrollView]);
    } else {
        idealOffset = CGPointMake(0, [self getContentOffsetYpositionForView:textField
                                                      withViewingAreaHeight:visibleSpace forscrollView:scrollView]);
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        [scrollView setContentOffset:idealOffset animated:NO];
    }];
    
}

//-----------------------------------------------------------------------

-(CGFloat)getContentOffsetYpositionForView:(UIView *)view withViewingAreaHeight:(CGFloat)viewAreaHeight forscrollView:(UIScrollView*)scrollView {
    
    CGSize contentSize = scrollView.contentSize;
    CGFloat offset = 0.0;
    
    CGRect subviewRect = [view convertRect:view.bounds toView:scrollView];
    
    CGFloat padding = (viewAreaHeight - subviewRect.size.height) / 2;
    if ( padding < kMinimumScrollOffsetPadding ) {
        padding = kMinimumScrollOffsetPadding;
    }
    
    // Ideal offset places the subview rectangle origin "padding" points from the top of the scrollview.
    // If there is a top contentInset, also compensate for this so that subviewRect will not be placed under
    // things like navigation bars.
    offset = subviewRect.origin.y - padding - scrollView.contentInset.top;
    
    // Constrain the new contentOffset so we can't scroll past the bottom. Note that we don't take the bottom
    // inset into account, as this is manipulated to make space for the keyboard.
    if ( offset > (contentSize.height - viewAreaHeight) ) {
        offset = contentSize.height - viewAreaHeight;
    }
    
    // Constrain the new contentOffset so we can't scroll past the top, taking contentInsets into account
    if ( offset < -scrollView.contentInset.top ) {
        offset = -scrollView.contentInset.top;
    }
    
    return offset;
}

//-----------------------------------------------------------------------

- (void) caluclateAndSetResultFor3Site{
    
    if ([txt3STriceps.text floatValue] == 0 && [txt3SSuprailiac.text floatValue] == 0 && [txt3SThigh.text floatValue] == 0) {
        txt3SSum.text = @"0.0";
        return;
    }
    MESSAGE(@"Valuse: %@ %@ %@",txt3STriceps.text,txt3SSuprailiac.text,txt3SThigh.text);
    
    NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
    float age = [[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kAge]] floatValue];
    float bodydensity = 0;
    float thigh = [[txt3SThigh text] floatValue];
    
    if ([gender isEqualToString:@"M"]) {
        
        float chest = [[txt3STriceps text] floatValue];
        float abdominal = [[txt3SSuprailiac text] floatValue];
        
        float sum = thigh + chest + abdominal;
        float sum2 = sum*sum;
        
        bodydensity =  1.10938 - (0.0008267 * sum) + (0.0000016 * sum2) - (0.000257 * age);
        bodydensity = ((4.95/ bodydensity) - 4.50) * 100;
        
        txt3SSum.text = [NSString stringWithFormat:@" %.2f",bodydensity];
    }
    else {
        float triceps = [[txt3STriceps text] floatValue];
        float suprailiac = [[txt3SSuprailiac text] floatValue];
        
        float sum = triceps + suprailiac + thigh;
        float sum2 = sum*sum;
        
        bodydensity = 1.0994921 - (0.0009929 * sum) + (0.0000023 * sum2) - (0.0001392 * age);
        bodydensity = ((4.95/ bodydensity) - 4.50) * 100;
        
        txt3SSum.text = [NSString stringWithFormat:@" %.2f",bodydensity];
    }
    
}

//-----------------------------------------------------------------------

- (void) caluclateAndSetResultFor4Site {
    
    if ([txt4SAbdomen.text floatValue] == 0 && [txt4SIllium.text  floatValue] == 0 && [txt4STriceps.text  floatValue] == 0 && [txt4SThigh.text  floatValue] == 0) {
        txt4SSum.text = @"0.0";
        return;
    }
    
    NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
    int	age = [[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kAge]] floatValue];
    
    float bodyFat  = 0;
    
    float abdomen = [[txt4SAbdomen text] floatValue];
    float suparilic = [[txt4SIllium text] floatValue];
    float triceps = [[txt4STriceps text] floatValue];
    float thigh = [[txt4SThigh text] floatValue];
    
    float sum = thigh + abdomen + suparilic + triceps;
    
    if ([gender isEqualToString:@"M"]) {
        bodyFat =  (0.29288 * sum) - (0.0005 * (sum * sum)) + (0.15845 * age) - 5.76377;
    }
    else {
        bodyFat =  (0.29669 * sum) - (0.00043 * (sum * sum)) + (0.02963 * age) + 1.4072;
    }
    txt4SSum.text = [NSString stringWithFormat:@" %.2f",bodyFat];
    
}

//-----------------------------------------------------------------------

- (void) caluclateAndSetResultFor7Site {
    
    if ([txt7SAbdominal.text floatValue] == 0 && [txt7SChest.text  floatValue] == 0 && [txt7SMidaxilari.text  floatValue] == 0 && [txt7SSubscapular.text  floatValue] == 0 && [txt7SSuparilic.text  floatValue] == 0 && [txt7SThig.text  floatValue] == 0 && [txt7STriceps.text  floatValue] == 0) {
        txt7SSum.text = @"0.0";
        return;
    }
    
    NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
    float age = [[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kAge]] floatValue];
    
    float bodyDensity  = 0;
    
    float abdominal = [[txt7SAbdominal text] floatValue];
    float chest = [[txt7SChest text] floatValue];
    float midaxilari = [[txt7SMidaxilari text] floatValue];
    float subscapular = [[txt7SSubscapular text] floatValue];
    float suparilic = [[txt7SSuparilic text] floatValue];
    float thig = [[txt7SThig text] floatValue];
    float triceps = [[txt7STriceps text] floatValue];
    
    float sum = abdominal + chest + midaxilari + subscapular + suparilic + thig + triceps;
    float sum2 = sum*sum;
    
    if ([gender isEqualToString:@"M"]) {
        bodyDensity =  1.112 - (0.00043499 * sum) + (0.00000055 * sum2 ) - (0.00028826 * age);
    }
    else {
        bodyDensity =  1.097 - (0.00046971 * sum) + (0.00000056 * sum2 ) - (0.00012828 * age);
    }
    bodyDensity =  ((4.95/ bodyDensity) - 4.50) * 100;
    
    txt7SSum.text = [NSString stringWithFormat:@" %.2f",bodyDensity];
}

//-----------------------------------------------------------------------

- (float) convertToinchfeet:(NSString *) height {
    
    NSRange range = [height rangeOfString : @"."];
    if(range.location == NSNotFound){
        float feet = [height intValue];
        float inch = 0;
        return ((feet * 12) + inch);
        
    }else{
        NSArray * arrStr = [height componentsSeparatedByString:@"."];
        float feet=[[arrStr objectAtIndex:0] intValue];
        float inch =[[arrStr objectAtIndex:1] intValue];
        return ((feet * 12) + inch);
    }
}

//-----------------------------------------------------------------------

- (void) caluclateAndSetBMIResult {
    
    float bmi = 0;
    float height = [txtBMIHeight.text floatValue];
    float weight = [txtBMIWeight.text floatValue];
    
    if (height == 0 || weight == 0) {
        txtBMIResult.text = @"0.0";
    }
    else {
        
        if (btnStandard.selected) {
            //standard
            height = [self convertToinchfeet:txtBMIHeight.text] ;
            bmi = (weight* 703)/(height*height) ;
        }
        else {
            //convert cm height in meter
            height = height/100;
            bmi = ( weight / ( height * height ) );
        }
    }
    txtBMIResult.text = [NSString stringWithFormat:@" %.2f",bmi];
    
}

//-----------------------------------------------------------------------

- (void) autoSaveContents:(UITextField *)textField {
    
    MESSAGE(@"textField--> tag:%ld %@",(long)textField.tag,textField.text);
    //TODO:SUNIl SAVE Value with .0 after decimal-->
    
    float floatNumber = [textField.text floatValue];
    
    NSString *str=[NSString stringWithFormat:@"%f",floatNumber];
    NSArray *arr=[str componentsSeparatedByString:@"."];
    int tempInt=[[arr lastObject] intValue];
    
    
    //Take string for store the  value of textfields
    NSString *strTextFiledWithFlaot;
    if(tempInt==0){
        
        //Set 1 place 0 only
        strTextFiledWithFlaot =   [NSString stringWithFormat:@"%.1f",floatNumber];
        
    }else{
        
        //Set 1 place 0 only
        strTextFiledWithFlaot =   textField.text;
        
    }
    
    
    //<----
    
    
    NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
    NSString * fieldName = @"";
    NSString * fieldName2 = @"";
    
    int grphType = 0;
    float fieldvalue2 = 0;
    int fieldTag = (int)textField.tag;
    
    switch (fieldTag) {
        case 0:
            fieldName = kWeight;
            break;
        case 1:
            fieldName = kHeight;
            grphType = 1;
            break;
        case 2:{
            fieldName = kRestingHeartBeat;
   //TODO: Set Integer heart rate    //Sunil
            
            NSArray* foo = [txtBMHeartRate.text componentsSeparatedByString: @"."];
            NSString* firstBit = [foo objectAtIndex: 0];
            textField.text  =firstBit;
            
             }
            break;
       
        case 3:
            fieldName = kBloodPressure;
            break;
        case 4:
            if ([gender isEqualToString:@"M"]) {
                fieldName = @"fThree_Male_Chest";
            }
            else {
                fieldName = @"fThree_Female_Triceps";
            }
            fieldName2 = kfThree_Result;
            fieldvalue2 = [txt3SSum.text floatValue];
            grphType = 2;
            break;
        case 5:
            if ([gender isEqualToString:@"M"]) {
                fieldName = @"fThree_Male_Abdomen";
            }
            else {
                fieldName = @"fThree_Female_Suprailiac";
            }
            fieldvalue2 = [txt3SSum.text floatValue];
            fieldName2 = kfThree_Result;
            grphType = 2;
            break;
        case 6:
            if ([gender isEqualToString:@"M"]) {
                fieldName = @"fThree_Male_Thigh";
            }
            else {
                fieldName = @"fThree_Female_Thigh";
            }
            fieldvalue2 = [txt3SSum.text floatValue];
            fieldName2 = kfThree_Result;
            grphType = 2;
            break;
        case 8:
            fieldName = kFour_Triceps;
            fieldName2 = kFour_Result;
            fieldvalue2 = [txt4SSum.text floatValue];
            grphType = 2;
            break;
        case 9:
            fieldName = kFour_Abdomen;
            fieldName2 = kFour_Result;
            fieldvalue2 = [txt4SSum.text floatValue];
            grphType = 2;
            break;
        case 10:
            fieldName = kFour_Suprailiac;
            fieldName2 = kFour_Result;
            fieldvalue2 = [txt4SSum.text floatValue];
            grphType = 2;
            break;
        case 11:
            fieldName = kFour_Thigh;
            fieldName2 = kFour_Result;
            fieldvalue2 = [txt4SSum.text floatValue];
            grphType = 2;
            break;
        case 13:
            fieldName = kSeven_Chest;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 14:
            fieldName = kSeven_Abdomen;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 15:
            fieldName = kSeven_Thigh;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 16:
            fieldName = kSeven_Triceps;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 17:
            fieldName = kSeven_subscapular;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 18:
            fieldName = kSeven_Suprailiac;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 19:
            fieldName = kSeven_Midaxillary;
            fieldName2 = kfSeven_Result;
            fieldvalue2 = [txt7SSum.text floatValue];
            grphType = 2;
            break;
        case 21:
            fieldName = kCM_RightUpperArm;
            grphType = 3;
            break;
        case 22:
            fieldName = kCM_RightForeArm;
            grphType = 3;
            break;
        case 23:
            fieldName = kCM_Chest;
            grphType = 3;
            break;
        case 24:
            fieldName = kCM_Abdominal;
            grphType = 3;
            break;
        case 25:
            fieldName = kCM_HipButtocks;
            grphType = 3;
            break;
        case 26:
            fieldName = kCM_RightThing;
            grphType = 3;
            break;
        case 27:
            fieldName = kCM_RightCalf;
            grphType = 3;
            break;
        case 28:
            if ([btnStandard isSelected]) {
                fieldName = kBMI_Pound_Height;
                fieldName2 = kBMI_Pound_Result;
            }
            else if ([btnMetric isSelected]) {
                fieldName = kBMI_Metrics_Height;
                fieldName2 = kBMI_Metrics_Result;
            }
            fieldvalue2 = [txtBMIResult.text floatValue];
            grphType = 4;
            break;
        case 29:
            if ([btnStandard isSelected]) {
                fieldName = kBMI_Pound_Weight;
                fieldName2 = kBMI_Pound_Result;
            }
            else if ([btnMetric isSelected]) {
                fieldName = kBMI_Metrics_Weight;
                fieldName2 = kBMI_Metrics_Result;
            }
            
            fieldvalue2 = [txtBMIResult.text floatValue];
            grphType = 4;
            break;
            
        case 31:
            fieldName = kGereralFatPercent;
            grphType = 5;
            break;
        case 32:
            fieldName = kExtraFieldName;
            //grphType = 5;
            break;
        case 33:
            fieldName = kExtraValue;
            //grphType = 5;
            break;
            
        default:
            break;
    }
    
    if ([fieldName length] != 0) {
        
        NSDictionary * dictInfo;
        if ([fieldName2 length] == 0) {
            NSString *dictType = @"0";
            if ([fieldName isEqualToString:kExtraFieldName]) {
                dictType = @"2";
            }
            
            
            NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
            
            dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                        fieldName,kfieldName,
                        textField.text,kfieldValue,
                        [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                        self.strClientID,kClientId,
                        dictType,kDictType,
                        nil];
        }
        else {
            
            NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
            
            dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                        fieldName,kfieldName,
                        textField.text,kfieldValue,
                        fieldName2,kfieldName2,
                        [NSString stringWithFormat:@"%f",fieldvalue2],kfieldValue2,
                        [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                        self.strClientID,kClientId,
                        @"1",kDictType,
                        nil];
        }
        
        // DLOG(@"DictInfo-->%@",[dictInfo description]);
        
        if ([[DataManager initDB] updateField:dictInfo]) {
            // DLOG(@"Not Updated");
        }
        else {
            // DLOG(@"Updated");
            if (grphType == 1) {
                [self drawGraphForWeightTraker];
            }
            else if (grphType == 2) {
                [self drawGraphForBodyFatPercentage];
            }
            else if (grphType == 3) {
                [self drawGraphForCircumferenceMeasurement];
            }
            else if (grphType == 4) {
                [self drawGraphForBMITraker];
            }
            else if (grphType == 5) {
                [self drawGraphForGeneralBodyFatPercentageTracker];
            }
            
        }
        
    }
    
    
 //TODO: POST REQUEST TO SERVER
        //invoke method for post request to save Assesment full data
    [self postRequestToUpdateAsssessment];
}


//---------------------------------------------------------------------------------------------------------------------------

- (NSString* ) stringForXaxies {
    
    NSMutableString * sAxies = [[NSMutableString alloc] init];
    for (int i = 0; i<=[arraySheets count]; i++) {
        if (i == 0) {
            [sAxies appendString:@"['0'"];
        } else if(i == [arraySheets count]){
            [sAxies appendString:[NSString stringWithFormat:@", '%d']",i]];
        } else {
            [sAxies appendString:[NSString stringWithFormat:@", '%d'",i]];
        }
    }
    return (NSString *)sAxies;
}

//---------------------------------------------------------------------------------------------------------------------------

- (NSString* ) stringFromArray :(NSArray *) array {
    NSMutableString * string = [[NSMutableString alloc] init];
    [string appendString:@"["];
    
    
    for (int i = 0; i<[array count]; i++) {
        if ([[array objectAtIndex:i] isKindOfClass:[NSArray class]] || [[array objectAtIndex:i] isKindOfClass:[NSMutableArray class]]) {
            if ([[array objectAtIndex:i] count] > 0) {
                if ([string isEqualToString:@"["]) {
                    
                    for (int j = 0; j<[[array objectAtIndex:i] count]; j++) {
                        if (j == 0) {
                            if (j == [[array objectAtIndex:i] count] - 1) {
                                [string appendString:[NSString stringWithFormat:@"['0','%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            } else {
                                [string appendString:[NSString stringWithFormat:@"['0','%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            }
                        } else if (j == [[array objectAtIndex:i] count] - 1) {
                            [string appendString:[NSString stringWithFormat:@",'%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        } else {
                            [string appendString:[NSString stringWithFormat:@",'%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        }
                    }
                } else {
                    for (int j = 0; j<[[array objectAtIndex:i] count]; j++) {
                        if (j == 0) {
                            if (j == [[array objectAtIndex:i] count]-1) {
                                [string appendString:[NSString stringWithFormat:@",['0','%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            } else {
                                [string appendString:[NSString stringWithFormat:@",['0','%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                            }
                        } else if (j == [[array objectAtIndex:i] count]-1) {
                            [string appendString:[NSString stringWithFormat:@",'%.2f']",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        } else {
                            [string appendString:[NSString stringWithFormat:@",'%.2f'",[[[array objectAtIndex:i] objectAtIndex:j] floatValue]]];
                        }
                    }
                }
            }
            
        } else {
            if ([[array objectAtIndex:i] length] > 0) {
                if ([string isEqualToString:@"["]) {
                    [string appendString:[NSString stringWithFormat:@"'0','%.2f'",[[array objectAtIndex:i] floatValue]]];
                } else {
                    [string appendString:[NSString stringWithFormat:@",'%.2f'",[[array objectAtIndex:i] floatValue]]];
                }
            }
        }
    }
    
    [string appendString:@"]"];
    return (NSString *) string;
}

//-----------------------------------------------------------------------

- (void) displaywebViewBodyFatPercentageContent {
//    webViewBodyFatPercentage.backgroundColor = [UIColor clearColor];
   
    
    /*NSString *path = [[NSBundle mainBundle] pathForResource:@"BodyChart" ofType:@"html"];
    DebugLOG(path);
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [webViewBodyFatPercentage loadRequest:[NSURLRequest requestWithURL:baseURL]];
    
    
    
 */
     
   //Sunil
     
     NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"BodyChart" ofType:@"html"];
     
     NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
    
    htmlString = [self setBodyChartOnBasisSkin:htmlString];
    
    [webViewBodyFatPercentage scalesPageToFit];
     [webViewBodyFatPercentage setBackgroundColor:[UIColor clearColor]];
     [webViewBodyFatPercentage setOpaque:NO];
     
     [webViewBodyFatPercentage loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
 
}

//-----------------------------------------------------------------------

- (void) setAssessmentDataForSheet {
    
    [self setAssessmentImages];  // TODO : Check APPstore Crash
    
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    lblSheetName.text = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    
    NSArray *arraySheetData = [[DataManager initDB] getAssessmentSheetsDataForSheet:[NSString stringWithFormat:@"%ld",(long)sheetId] ofClientID:self.strClientID];
    lblSheetName.text = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    
    NSDictionary * tempDictInfo =  (NSDictionary *) [arraySheetData objectAtIndex:0];
    //  // DLOG(@"Dict Info-->%@",[dictInfo description]);
    
    //--> SUNIL
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc]init];
    
    for(id key in tempDictInfo){
        MESSAGE(@"key=%@ value=%@", key, [tempDictInfo objectForKey:key]);
        NSString *strValue  =   [tempDictInfo objectForKey:key];
        
        //Set 0.0 if value is 0
        if(strValue.intValue==0){
            
            if([key isEqualToString:@"sExtra_FieldName"]){
                
            }else{
                
                [dictInfo setValue:@"0.0" forKey:key];
            }
            
            
            //For set text
            if([key isEqualToString:@"sAssessment_Goal"]){
                [dictInfo setValue:[tempDictInfo objectForKey:key] forKey:key];
            }
            
        }else{
            [dictInfo setValue:[tempDictInfo objectForKey:key] forKey:key];
        }
        
    }
    
    //<-----
    
//TODO : Chnage dete format from @"MM/dd/yy" to kAppDateFormat
    MESSAGE(@"dictInfo-> %@",tempDictInfo);
    
    NSString *strDate = [dictInfo objectForKey:kAssUpdatedDate];
    NSDateFormatter *parsingFormatter = [NSDateFormatter new];
    //[parsingFormatter setDateFormat:@"MM/dd/yyyy"];
    [parsingFormatter setDateFormat:kAppDateFormat];
    NSDate *date = [parsingFormatter dateFromString:strDate];
    
//    NSDateFormatter *displayingFormatter = [NSDateFormatter new];
//    [displayingFormatter setDateFormat:@"MM/dd/yy"];
    NSString *displayDate = [parsingFormatter stringFromDate:date];
    
    [btnDate setTitle:displayDate forState:UIControlStateNormal];
    btnDate.titleLabel.font = [UIFont systemFontOfSize:14];
    
    txtBMHeight.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kHeight]];
    txtBMWeight.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kWeight]];
    txtBMHeartRate.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kRestingHeartBeat]];
    txtBMBloodPressure.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kBloodPressure]];
    
    NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
    
    
    if ([gender isEqualToString:@"M"]) {
        lblTriceps.text = NSLocalizedString(@"CHEST", nil);
        lblSupraialiac.text = NSLocalizedString(@"ABDOMINAL", nil);
    }
    else {
        lblTriceps.text = NSLocalizedString(@"TRICEPS", nil);
        lblSupraialiac.text = NSLocalizedString(@"SUPRAILIAC", nil);
    }
    
    txt3STriceps.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kThree_Data1]];
    txt3SSuprailiac.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kThree_Data2]];
    txt3SThigh.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kThree_Data3]];
    
    txt4STriceps.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kFour_Triceps]];
    txt4SAbdomen.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kFour_Abdomen]];
    txt4SIllium.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kFour_Suprailiac]];
    txt4SThigh.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kFour_Thigh]];
    
    txt7SChest.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_Chest]];
    txt7SAbdominal.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_Abdomen]];
    txt7SThig.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_Thigh]];
    txt7STriceps.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_Triceps]];
    txt7SSubscapular.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_subscapular]];
    txt7SSuparilic.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_Suprailiac]];
    txt7SMidaxilari.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kSeven_Midaxillary]];
    
    txtCmRightUpeerArm.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_RightUpperArm]];
    txtCmRightForeArm.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_RightForeArm]];
    txtCmChest.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_Chest]];
    txtCmAbdominal.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_Abdominal]];
    txtCmHipButtocks.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_HipButtocks]];
    txtCmRightThigh.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_RightThing]];
    txtCmRightCalf.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kCM_RightCalf]];
    
    txtValueExtra.text = [dictInfo objectForKey:kExtraValue];
    txtGeneralBodyFat.text = [dictInfo objectForKey:kGereralFatPercent];
    txtKeyExtra.text = [dictInfo objectForKey:kExtraFieldName];
    if (txtKeyExtra.text.length == 0) {
        txtKeyExtra.text = NSLocalizedString(@"EXTRA", nil);
    }
    
    if ([btnStandard isSelected]) {
        txtBMIHeight.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kBMI_Pound_Height]];
        txtBMIWeight.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kBMI_Pound_Weight]];
    }
    else if ([btnMetric isSelected]) {
        txtBMIHeight.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kBMI_Metrics_Height]];
        txtBMIWeight.text = [NSString stringWithFormat:@"%@",[dictInfo objectForKey:kBMI_Metrics_Weight]];
    }
    
    strStandardHeight = [dictInfo objectForKey:kBMI_Pound_Height];
    strStandardWeight =[dictInfo objectForKey:kBMI_Pound_Weight];
    strMetricHeight = [dictInfo objectForKey:kBMI_Metrics_Height];
    strMetricWeight = [dictInfo objectForKey:kBMI_Metrics_Weight];
    
    txtViewGoals.text = [dictInfo objectForKey:ksAssessment_Goal];
    
    [self caluclateAndSetResultFor3Site];
    [self caluclateAndSetResultFor4Site];
    [self caluclateAndSetResultFor7Site];
    [self caluclateAndSetBMIResult];
    
}

//-----------------------------------------------------------------------

-(void)seDateFromDatePicker{
    
    START_METHOD
    NSDate * date = datePicker.date;
    NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
    
    [dateformatter setDateFormat:kAppDateFormat];//Change date format from yy to yyyy
    
    [btnDate setTitle:[dateformatter stringFromDate:date] forState:UIControlStateNormal];
    btnDate.titleLabel.font = [UIFont systemFontOfSize:14];
    if (appDelegate.isAutoSave) {
        
        NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
        
        NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   kAssUpdatedDate,kfieldName,
                                   [dateformatter stringFromDate:date],kfieldValue,
                                   [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                                   self.strClientID,kClientId,
                                   @"2",kDictType,
                                   nil];
        DebugLOG([dictInfo description]);
        if ([[DataManager initDB] updateField:dictInfo]) {
            DebugLOG(@"Not Updated");
        }
        else {
            DebugLOG(@"Updated");
        }
    }
    else if (!appDelegate.isAutoSave) {
        appDelegate.isFieldUpdated = YES;
    }
    
    [popoverController dismissPopoverAnimated:YES];
    dateformatter = nil;
    
    //Update Sheet Data
    [self postRequestToUpdateAsssessment];
    END_METHOD
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) saveAssessmentData {
    
    [[self.view findFirstResponder] resignFirstResponder];
    
    NSString *strBMIMetricResult = @"";
    NSString *strBMIStandardResult = @"";
    NSString *strBMIStandardWeight = @"";
    NSString *strBMIStandardHeight = @"";
    NSString *strBMIMetricWeight = @"";
    NSString *strBMIMetricHeight = @"";
    
    float bmi = 0;
    float height = 0;
    float weight =0;
    
    if ([btnMetric isSelected]) {
        
        strBMIMetricHeight = txtBMIHeight.text;
        strBMIMetricWeight = txtBMIWeight.text;
        height = [strBMIMetricHeight floatValue];
        weight = [strBMIMetricWeight floatValue];
        height = height/100;
        bmi = ( weight / ( height * height ) );
        strBMIMetricResult = [NSString stringWithFormat:@" %.2f",bmi];
        
        strBMIStandardHeight = strStandardHeight;
        strBMIStandardWeight = strStandardWeight;
        height = [self convertToinchfeet:strBMIStandardHeight];
        weight = [strBMIStandardWeight floatValue];
        bmi = (weight* 703)/(height*height) ;
        strBMIStandardResult = [NSString stringWithFormat:@" %.2f",bmi];
    }
    else{
        
        strBMIStandardHeight = txtBMIHeight.text;
        strBMIStandardWeight = txtBMIWeight.text;
        height = [self convertToinchfeet:strBMIStandardHeight];
        weight = [strBMIStandardWeight floatValue];
        bmi = (weight* 703)/(height*height) ;
        strBMIStandardResult = [NSString stringWithFormat:@" %.2f",bmi];
        
        strBMIMetricHeight = strMetricHeight;
        strBMIMetricWeight = strMetricWeight;
        height = [strBMIMetricHeight floatValue];
        weight = [strBMIMetricWeight floatValue];
        height = height/100;
        bmi = ( weight / ( height * height ) );
        strBMIMetricResult = [NSString stringWithFormat:@" %.2f",bmi];
        
    }
    
    NSMutableDictionary * dictInfo = [[NSMutableDictionary alloc] init];
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    // DLOG(@"sheetId%ld",(long)sheetId);
    
    [dictInfo setObject:btnDate.titleLabel.text forKey:kAssUpdatedDate];
    [dictInfo setObject:self.strClientID forKey:kClientId];
    //[dictInfo setObject:[NSString stringWithFormat:@"%d",currentSheet] forKey:ksheetID];
    [dictInfo setObject:[NSString stringWithFormat:@"%ld",(long)sheetId] forKey:ksheetID];
    
//TODO: Set Integer heart rate    //Sunil
    NSArray* foo = [txtBMHeartRate.text componentsSeparatedByString: @"."];
    NSString* firstBit = [foo objectAtIndex: 0];
    
    [dictInfo setObject:txtBMHeight.text forKey:kHeight];
    [dictInfo setObject:txtBMWeight.text forKey:kWeight];
    [dictInfo setObject:firstBit  forKey:kRestingHeartBeat];
    [dictInfo setObject:txtBMBloodPressure.text forKey:kBloodPressure];
    
    [dictInfo setObject:txt3STriceps.text forKey:kThree_Data1];
    [dictInfo setObject:txt3SSuprailiac.text forKey:kThree_Data2];
    [dictInfo setObject:txt3SThigh.text forKey:kThree_Data3];
    [dictInfo setObject:txt3SSum.text forKey:kfThree_Result];
    
    [dictInfo setObject:txt4SAbdomen.text forKey:kFour_Abdomen];
    [dictInfo setObject:txt4SIllium.text forKey:kFour_Suprailiac];
    [dictInfo setObject:txt4STriceps.text forKey:kFour_Triceps];
    [dictInfo setObject:txt4SThigh.text forKey:kFour_Thigh];
    [dictInfo setObject:txt4SSum.text forKey:kFour_Result];
    
    [dictInfo setObject:txt7SChest.text forKey:kSeven_Chest];
    [dictInfo setObject:txt7SMidaxilari.text forKey:kSeven_Midaxillary];
    [dictInfo setObject:txt7STriceps.text forKey:kSeven_Triceps];
    [dictInfo setObject:txt7SSubscapular.text forKey:kSeven_subscapular];
    [dictInfo setObject:txt7SAbdominal.text forKey:kSeven_Abdomen];
    [dictInfo setObject:txt7SSuparilic.text forKey:kSeven_Suprailiac];
    [dictInfo setObject:txt7SThig.text forKey:kSeven_Thigh];
    [dictInfo setObject:txt7SSum.text forKey:kfSeven_Result];
    
    [dictInfo setObject:txtCmRightUpeerArm.text forKey:kCM_RightUpperArm];
    [dictInfo setObject:txtCmRightForeArm.text forKey:kCM_RightForeArm];
    [dictInfo setObject:txtCmChest.text forKey:kCM_Chest];
    [dictInfo setObject:txtCmAbdominal.text forKey:kCM_Abdominal];
    [dictInfo setObject:txtCmHipButtocks.text forKey:kCM_HipButtocks];
    [dictInfo setObject:txtCmRightThigh.text forKey:kCM_RightThing];
    [dictInfo setObject:txtCmRightCalf.text forKey:kCM_RightCalf];
    
    [dictInfo setObject:[NSString stringWithFormat:@"%@",strBMIMetricWeight] forKey:kBMI_Metrics_Weight];
    [dictInfo setObject:[NSString stringWithFormat:@"%@",strBMIMetricHeight] forKey:kBMI_Metrics_Height];
    [dictInfo setObject:[NSString stringWithFormat:@"%@",strBMIMetricResult] forKey:kBMI_Metrics_Result];
    [dictInfo setObject:[NSString stringWithFormat:@"%@",strBMIStandardWeight] forKey:kBMI_Pound_Weight];
    [dictInfo setObject:[NSString stringWithFormat:@"%@",strBMIStandardHeight] forKey:kBMI_Pound_Height];
    [dictInfo setObject:[NSString stringWithFormat:@"%@",strBMIStandardResult] forKey:kBMI_Pound_Result];
    
    [dictInfo setObject:[NSString stringWithFormat:@"%@",txtGeneralBodyFat.text] forKey:kGereralFatPercent];
    [dictInfo setObject:[NSString stringWithFormat:@"%@",txtValueExtra.text] forKey:kExtraValue];
    
    if (txtKeyExtra.text.length == 0) {
        txtKeyExtra.text = NSLocalizedString(@"EXTRA", nil);
    }
    [dictInfo setObject:[NSString stringWithFormat:@"%@",txtKeyExtra.text] forKey:kExtraFieldName];
    
    [dictInfo setObject:@"0" forKey:kCM_Result];
    [dictInfo setObject: txtViewGoals.text forKey:ksAssessment_Goal];
    
    
    // DLOG(@"Assessment sheet data %@", dictInfo);
    
    
    DebugLOG([dictInfo description]);
    int t = [[DataManager initDB] updateAssessmentSheetFor:dictInfo];
    
    if (t == 0) {
        DebugLOG(@"Data Updated Successfully..");
        [self drawGraphForWeightTraker];
        [self drawGraphForBodyFatPercentage];
        [self drawGraphForBMITraker];
        [self drawGraphForCircumferenceMeasurement];
    }
    else {
        DebugLOG(@"Data Not Updated Successfully..");
    }
}

//---------------------------------------------------------------------------------------------------------------

-(NSData *) getScreenImageData {
    
    
    CGRect originalFrame = self.scrollViewAssesmet.frame;
    
    self.scrollViewAssesmet.contentOffset = CGPointZero;
    self.scrollViewAssesmet.frame = CGRectMake(0, 0,self.scrollViewAssesmet.contentSize.width, self.scrollViewAssesmet.contentSize.height);
    
    
    UIGraphicsBeginImageContext(self.scrollViewAssesmet.frame.size);
    
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *screenImg = UIGraphicsGetImageFromCurrentImageContext();
    
    
    
    UIGraphicsEndImageContext();
    
    NSData *data = UIImageJPEGRepresentation(screenImg, 1.0);
    
    self.scrollViewAssesmet.frame = originalFrame;
    
    return data;
    
    
}


//-----------------------------------------------------------------------

-(void)setupLayout {
    
    
    NSArray *scrollVewContent = [self.scrollViewPhotos subviews];
    [scrollVewContent enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if([obj isKindOfClass:[UIView class]] && ![obj isKindOfClass:[UIButton class]] & ![obj isKindOfClass:[UILabel class]]) {
            
            UIView *vew = (UIView *) obj;
            // vew.backgroundColor = [UIColor whiteColor];
            vew.layer.borderColor = [UIColor grayColor].CGColor;
            vew.layer.borderWidth = 1.5;
        }
        
    }];
}

//---------------------------------------------------------------------------------------------------------------

-(void)deleteImageOfAssessment {
    START_METHOD
    
    if (self.deleteImgTag == 5101) {
        [lblAddPhoto1 setHidden:FALSE];
    } else if (self.deleteImgTag == 5102) {
        [lblAddPhoto2 setHidden:FALSE];
    } else if (self.deleteImgTag == 5103) {
        [lblAddPhoto3 setHidden:FALSE];
    } else if (self.deleteImgTag == 5104) {
        [lblAddPhoto4 setHidden:FALSE];
    } else if (self.deleteImgTag == 5105) {
        [lblAddPhoto5 setHidden:FALSE];
    }
    
    NSInteger imgButtonTag = self.deleteImgTag - 5000;
    
    UIButton * btnimgView = (UIButton *)[self.scrollViewPhotos viewWithTag:imgButtonTag];
    
    //Sunil Set add Photo
    UIImage *image        =   [self setAddPhotos];
    [btnimgView setImage:image  forState:UIControlStateNormal];
    // [btnimgView setTitle:NSLocalizedString(@"ADD PHOTO", nil)  forState:UIControlStateNormal];
    [btnimgView setUserInteractionEnabled:YES];
    
    
    [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+imgButtonTag] setText:@""];
    
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    
    NSDictionary * selObj = [arrayImages objectAtIndex:imgButtonTag-100 -1];
    
    //SUNIL--->13 FEB
    int intTag = (int)imgButtonTag;
    
    int intReminder = intTag%5;
    
    if (intReminder==0) {
        
        intReminder = 5;
    }
    
    
    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientID,kClientId,
                               [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                               @"0",khasImage,
                               [NSString stringWithFormat:@"%d",intReminder],kImageID,
                               @"",kImageName,nil];
    
    [[DataManager initDB] updateAssessmentImageTitlefor:dictInfo];
    
    [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag + imgButtonTag] setHidden:YES];
    [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + imgButtonTag] setHidden:YES];
    
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
    NSString * dirPath = [NSString stringWithFormat:@"%@/%@/%@.png",[FileUtility basePath],imageFolder,[NSString stringWithFormat:@"%d",intReminder]];
    DebugLOG(dirPath);
    [FileUtility deleteFile:dirPath];
    
}
//---------------------------------------------------------------------------------------------------------------
/*
 -(void)deleteAllAssessmentDataForClient {
 
 if([arraySheets count] == 1) {
 
 [self resetThedataForLastSheetDelete];
 
 } else {
 
 NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
 int t =  [[DataManager initDB]deleteAssessmentSheet:sheetId forClientid:self.strClientID];
 if(t == 0) {
 // Sheet deleted
 // DLOG(@"current sheet %d",currentSheet);
 int deletedData = [[DataManager initDB]deleteAssessmentSheetDataForSheetId:sheetId forClientid:self.strClientID];
 if(deletedData == 0) {
 int imgdataDeleted =  [[DataManager initDB]deleteAssessmentImagesforSheetId:sheetId forClient:self.strClientID];
 if(imgdataDeleted == 0) {
 NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%d",self.strClientID,sheetId];
 [FileUtility deleteFile:imageFolder];
 [self resetAllTheSheetData];
 DisplayAlertWithTitle(@"Assessment data deleted", kAppName);
 }
 }
 } else {
 // unable to delete sheet
 DisplayAlertWithTitle(@"Unable to delete assessment sheet", kAppName);
 }
 
 }
 
 
 }
 */

//---------------------------------------------------------------------------------------------------------------

-(void)deleteAllAssessmentDataForClient {
    
    if([arraySheets count] == 1) {
        
        [self resetThedataForLastSheetDelete];
        
    } else {
        
        NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
        NSInteger deletedData = [[DataManager initDB]deleteAssessmentSheetDataForSheetId:sheetId forClientid:self.strClientID];
        
        if(deletedData == 0) {
            NSInteger imgdataDeleted =  [[DataManager initDB]deleteAssessmentImagesforSheetId:sheetId forClient:self.strClientID];
            if(imgdataDeleted == 0) {
                NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
                [FileUtility deleteFile:imageFolder];
                NSInteger t =  [[DataManager initDB]deleteAssessmentSheet:sheetId forClientid:self.strClientID];
                if(t== 0) {
                    [self resetAllTheSheetData];
                    DisplayAlertWithTitle(NSLocalizedString(NSLocalizedString(@"Assessment data deleted", modalController), nil), kAppName);
                }
            }
        } else {
            DisplayAlertWithTitle(NSLocalizedString(@"Unable to delete assessment sheet", nil), kAppName);
        }
        
    }
    
}




//---------------------------------------------------------------------------------------------------------------
-(void)resetThedataForLastSheetDelete {
    
    txtBMWeight.text = @"0.0",txtBMHeight.text = @"0.0",txtBMHeartRate.text = @"0",txtBMBloodPressure.text = @"0",
    txt3STriceps.text = @"0.0",txt3SSuprailiac.text = @"0.0",txt3SThigh.text = @"0.0",txt4STriceps.text = @"0.0",
    txt4SAbdomen.text = @"0.0", txt4SIllium.text = @"0.0",txt4SThigh.text = @"0.0",txt7SChest.text = @"0.0",
    txt7SAbdominal.text = @"0.0",txt7SThig.text = @"0.0",txt7STriceps.text = @"0.0",txt7SSubscapular.text = @"0.0",
    txt7SSuparilic.text = @"0.0",txt7SMidaxilari.text = @"0.0",txtGeneralBodyFat.text = @"0.0",txtCmRightUpeerArm.text = @"0.0",
    txtCmRightForeArm.text = @"0.0",txtCmChest.text = @"0.0",txtCmAbdominal.text = @"0.0",txtCmHipButtocks.text = @"0.0",
    txtCmRightThigh.text = @"0.0",txtCmRightCalf.text = @"0.0",txtKeyExtra.text = @"EXTRA",txtValueExtra.text = @"0.0",
    txtBMIHeight.text = @"0.0",txtBMIWeight.text = @"0.0",txtViewGoals.text=@"",txtBMIResult.text = @"0.0",txt3SSum.text = @"0.0",txt4SSum.text = @"0.0",txt7SSum.text = @"0.0",strMetricHeight = @"0.0",strMetricWeight = @"0.0",strStandardHeight = @"0.0",strStandardWeight = @"0.0";
    
    [self saveAssessmentData];
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    
    [[DataManager initDB] updateAssessmentSheetNameFor:self.strClientID ForSheetID:sheetId toSheetName:@"Assessment-1"];
    lblSheetName.text = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    NSArray *imagesForclient = [[DataManager initDB] getAllAssessmentImagesForSheet:[NSString stringWithFormat:@"%ld",(long)sheetId] ofClientID:self.strClientID];
    if([imagesForclient count] > 0) {
        
        for (int i= 101 ; i<105; i++) {
            
            UIButton * btnimgView = (UIButton *)[self.scrollViewPhotos viewWithTag:i];
            [btnimgView setImage:[UIImage imageNamed:@"add-photo"] forState:UIControlStateNormal];
            //  [btnimgView setTitle:@"ADD PHOTO"  forState:UIControlStateNormal];
            [btnimgView setUserInteractionEnabled:YES];
            
            [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+i] setText:@""];
            
            NSDictionary * selObj = [imagesForclient objectAtIndex:i-100 -1];
            
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientID,kClientId,
                                       [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                                       @"0",khasImage,
                                       [selObj objectForKey:kImageID],kImageID,
                                       @"",kImageName,nil];
            
            [[DataManager initDB] updateAssessmentImageTitlefor:dictInfo];
            
            [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag + i] setHidden:YES];
            [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + i] setHidden:YES];
            
            NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
            NSString * dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
            DebugLOG(dirPath);
            [FileUtility deleteFile:dirPath];
        }
    }
    [self resetAllTheSheetData];
}

//---------------------------------------------------------------------------------------------------------------

-(void)resetAllTheSheetData {
    
    //Set by default all iamge set default
    [lblAddPhoto1 setHidden:FALSE];
    [lblAddPhoto2 setHidden:FALSE];
    [lblAddPhoto3 setHidden:FALSE];
    [lblAddPhoto4 setHidden:FALSE];
    [lblAddPhoto5 setHidden:FALSE];
    
    [self displaywebViewBodyFatPercentageContent];
    [self getAllSheetsList];
    currentSheet =  [arraySheets count];
    [self resetbtnTitle];
    [self drawGraphForWeightTraker];
    [self drawGraphForBodyFatPercentage];
    [self drawGraphForBMITraker];
    [self drawGraphForCircumferenceMeasurement];
    [self drawGraphForGeneralBodyFatPercentageTracker];
    
    NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientID];
    
    NSString *strName = [[clientInfo objectForKey:kFirstName] stringByAppendingString:[NSString stringWithFormat:@" %@",[clientInfo objectForKey:kLastName]]];
    strName = [strName stringByAppendingString:NSLocalizedString(@"'s FITNESS ASSESSMENT", nil)];
    lblFitnessAssessmentName.text = strName;
  
    //Sunil
    
    [self setViewOnBasisSkin];
    
    [self.btnAssessment setSelected:false];

    
}

-(void) resetbtnTitle {
    
    
    arraySheets = [[DataManager initDB] getAllAssessmentSheetsFor:self.strClientID];
    currentSheet = [arraySheets count];
    
    if (currentSheet >3) {
        if (currentSheet/3 >0) {
            currentPage = currentSheet/3 -1;
        }
        
        [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+1)] forState:UIControlStateNormal];
        [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+2)] forState:UIControlStateNormal];
        [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+3)] forState:UIControlStateNormal];
        
        if (currentSheet%3 == 0) {
            currentPage = currentPage-1;
            [self btnAssessmentPageClicked:btnAssessmentPage3];
        }
        else if (currentSheet%3 == 1) {
            [self btnAssessmentPageClicked:btnAssessmentPage1];
        }
        else if (currentSheet%3 == 2) {
            [self btnAssessmentPageClicked:btnAssessmentPage2];
        }
        
        autoChangePageIfNextPageClicked = TRUE;
        [self btnForNextAssessmentPagesClicked:nil];
        
        
    } else {
        
        currentPage = 0;
        
        [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+1)] forState:UIControlStateNormal];
        [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+2)] forState:UIControlStateNormal];
        [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)((currentPage*3)+3)] forState:UIControlStateNormal];
        
        if (currentSheet%3 == 0) {
            btnAssessmentPage1.hidden = false;
            btnAssessmentPage2.hidden = false;
            btnAssessmentPage3.hidden = false;
            [self btnAssessmentPageClicked:btnAssessmentPage3];
        }
        else if (currentSheet%3 == 1) {
            btnAssessmentPage1.hidden = false;
            btnAssessmentPage2.hidden = true;
            btnAssessmentPage3.hidden = true;
            [self btnAssessmentPageClicked:btnAssessmentPage1];
        }
        else if (currentSheet%3 == 2) {
            btnAssessmentPage1.hidden = false;
            btnAssessmentPage2.hidden = false;
            btnAssessmentPage3.hidden = true;
            [self btnAssessmentPageClicked:btnAssessmentPage2];
        }
        
    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)checkNewFieldsInTableExistsForClientId:(NSString*)strClientId {
    [[DataManager initDB]addNewFieldsInAssessmentTableForClient:strClientId];
}

//---------------------------------------------------------------------------------------------------------------

-(void)changeTheButtonTitles {
    [btnAssessmentPage1 setTitle:@"1" forState:UIControlStateNormal];
    [btnAssessmentPage2 setTitle:@"2" forState:UIControlStateNormal];
    [btnAssessmentPage3 setTitle:@"3" forState:UIControlStateNormal];
    currentPage = 0;
    currentSheet = 1;
}

//---------------------------------------------------------------------------------------------------------------

-(void)setlastVistedPage {
    
    NSString *editedScreenInfo = [[DataManager initDB]getLastEditedScreenForClient:self.strClientID];
    NSArray *ary = [editedScreenInfo componentsSeparatedByString:@"/##/"];
    
    if([ary count] >0) {
        
        NSString *strAssessment = [ary objectAtIndex:0];
        if([strAssessment isEqualToString:@"3"]) {
            NSString *str = [ary objectAtIndex:1];
            NSArray *ary1 = [str componentsSeparatedByString:@","];
            
            if([ary1 count] == 2) {
                
                currentSheet = [[ary1 objectAtIndex:1] integerValue];
                currentPage = [[ary1 objectAtIndex:0] integerValue];
                
                NSInteger  totalsheetCount = [arraySheets count];
                if(currentSheet < totalsheetCount) {
                    if (currentSheet%3 == 0) {
                        
                        [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)(currentSheet-2)] forState:UIControlStateNormal];
                        [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)(currentSheet -1)] forState:UIControlStateNormal];
                        [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)currentSheet] forState:UIControlStateNormal];
                        [self btnAssessmentPageClicked:btnAssessmentPage3];
                        
                        
                    } else if (currentSheet%3 == 1) {
                        
                        [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)currentSheet] forState:UIControlStateNormal];
                        [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)(currentSheet + 1)] forState:UIControlStateNormal];
                        [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)(currentSheet + 2)] forState:UIControlStateNormal];
                        [self btnAssessmentPageClicked:btnAssessmentPage1];
                        
                    } else if (currentSheet%3 == 2) {
                        
                        [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)(currentSheet-1)] forState:UIControlStateNormal];
                        [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)currentSheet] forState:UIControlStateNormal];
                        [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)(currentSheet + 1)] forState:UIControlStateNormal];
                        [self btnAssessmentPageClicked:btnAssessmentPage2];
                    }
                    
                    
                    NSInteger curntPage = currentPage;
                    if (totalsheetCount > (curntPage*3)) {
                        
                        if (totalsheetCount-(curntPage*3) == 1) {
                            btnAssessmentPage1.hidden = false;
                            btnAssessmentPage2.hidden = true;
                            btnAssessmentPage3.hidden = true;
                            [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((curntPage*3)+1)] forState:UIControlStateNormal];
                        }
                        else if (totalsheetCount-(curntPage*3) == 2){
                            btnAssessmentPage1.hidden = false;
                            btnAssessmentPage2.hidden = false;
                            btnAssessmentPage3.hidden = true;
                            [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((curntPage*3)+1)] forState:UIControlStateNormal];
                            [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((curntPage*3)+2)] forState:UIControlStateNormal];
                        }
                        else{
                            btnAssessmentPage1.hidden = false;
                            btnAssessmentPage2.hidden = false;
                            btnAssessmentPage3.hidden = false;
                            [btnAssessmentPage1 setTitle:[NSString stringWithFormat:@"%ld",(long)((curntPage*3)+1)] forState:UIControlStateNormal];
                            [btnAssessmentPage2 setTitle:[NSString stringWithFormat:@"%ld",(long)((curntPage*3)+2)] forState:UIControlStateNormal];
                            [btnAssessmentPage3 setTitle:[NSString stringWithFormat:@"%ld",(long)((curntPage*3)+3)] forState:UIControlStateNormal];
                            
                        }
                    }
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self drawGraphForWeightTraker];
                            [self drawGraphForBodyFatPercentage];
                            [self drawGraphForBMITraker];
                            [self drawGraphForCircumferenceMeasurement];
                            [self drawGraphForGeneralBodyFatPercentageTracker];
                        });
                        
                    });
                    
                } else {
                    [self resetAllTheSheetData];
                }
                
                
            }
            
        } else {
            // DLOG(@"Was not the Assessment page");
            [self resetAllTheSheetData];
        }
        
    } else {
        // DLOG(@"Was not set");
        [self resetAllTheSheetData];
    }
    
}

//-----------------------------------------------------------------------

#pragma mark - Drawing Graphs

//-----------------------------------------------------------------------

- (void) drawGraphForWeightTraker {
    
    NSArray * arrarySubViews = [viewWeightTracker subviews];
    if ([arrarySubViews count] != 0) {
        [[arrarySubViews objectAtIndex:0] removeFromSuperview];
    }
    
    NSArray * weightArr = [[DataManager initDB] getClientsWeightHeight:self.strClientID];
    // // DLOG(@"weight array %@",weightArr);
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:JQueryFileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    NSString * dataStr = [self stringFromArray:weightArr];
    
    
    //
    BOOL bIsNegativeVal = FALSE;
    for (int i = 0; i < [weightArr count]; i++) {
        if ([[weightArr objectAtIndex:i] integerValue] < 0) {
            bIsNegativeVal = TRUE;
        }
    }
    if (bIsNegativeVal) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"center"];
    }else{
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"bottom"];
    }
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#XDATA#" withString:[self stringForXaxies]];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#DATA#" withString:dataStr];

    //Sunil if more than one then no fill graph
    if (weightArr.count>1){
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"false"];
    }
    else {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"true"];
        
    }
    
    htmlString = [self setGraphLioneColor:htmlString];
    
    MESSAGE(@"htmlString----> %@",htmlString);
    
    UIWebView * _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, viewWeightTracker.frame.size.width, viewWeightTracker.frame.size.height)];
    [_webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView scalesPageToFit];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    [viewWeightTracker addSubview:_webView];
    
}

//-----------------------------------------------------------------------

- (void) drawGraphForGeneralBodyFatPercentageTracker {
    
    NSArray * arrarySubViews = [viewGeneralBodyFatTracker subviews];
    if ([arrarySubViews count] != 0) {
        [[arrarySubViews objectAtIndex:0] removeFromSuperview];
    }
    
    NSArray * bodyFatArr = [[DataManager initDB] getClientsGeneralBodyFatPercentage:self.strClientID];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:JQueryFileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    NSString * dataStr = [self stringFromArray:bodyFatArr];
    
    BOOL bIsNegativeVal = FALSE;
    for (int i = 0; i < [bodyFatArr count]; i++) {
        if ([[bodyFatArr objectAtIndex:i] integerValue] < 0) {
            bIsNegativeVal = TRUE;
        }
    }
    if (bIsNegativeVal) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"center"];
    }else{
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"bottom"];
    }
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#XDATA#" withString:[self stringForXaxies]];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#DATA#" withString:dataStr];

    //Sunil if more than one then no fill graph
    if (bodyFatArr.count>1){
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"false"];
    }
    else {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"true"];
        
    }
    
     htmlString = [self setGraphLioneColor:htmlString];
    
    UIWebView * _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, viewGeneralBodyFatTracker.frame.size.width, viewGeneralBodyFatTracker.frame.size.height)];
    [_webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView scalesPageToFit];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    [viewGeneralBodyFatTracker addSubview:_webView];
    
}

//-----------------------------------------------------------------------

/*!
 sunil
 @Description : Methods for draw graph for body fat tracker
 @Param       :
 @Return      :
 */
- (void) drawGraphForBodyFatPercentage {
    START_METHOD
    NSArray * arraySubViews = [viewBodyFatTracker subviews];
    if ([arraySubViews count] > 0) {
        [[arraySubViews objectAtIndex:0] removeFromSuperview];
    }
    
    NSArray * bodyFatArr = [[DataManager initDB] getClientsBodyFatPercentage:self.strClientID];
    
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:JQueryFileName ofType:@"html"];
    
    
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
   // MESSAGE(@"htmlString->1: %@ and htmlFile: %@",htmlString,htmlFile);
    
    NSString * dataStr = [self stringFromArray:bodyFatArr];
    
    //
    BOOL bIsNegativeVal = FALSE;
    for (int i = 0; i < [bodyFatArr count]; i++) {
        for (int j = 0; j < [[bodyFatArr objectAtIndex:i] count]; j++){
            if ([[[bodyFatArr objectAtIndex:i] objectAtIndex:j] integerValue] < 0) {
                bIsNegativeVal = TRUE;
            }
        }
    }
    if (bIsNegativeVal) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"center"];
       // MESSAGE(@"htmlString->2 %@",htmlString);
    }else{
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"bottom"];
        //MESSAGE(@"htmlString->2 %@",htmlString);
    }
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#XDATA#" withString:[self stringForXaxies]];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#DATA#" withString:dataStr];
    
    //Sunil if more than one then no fill graph
    if (bodyFatArr.count>1){
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"false"];
    }
    else {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"true"];
        
    }
    
    htmlString = [self setGraphLioneColor:htmlString];
    MESSAGE(@"htmlString->4 %@",htmlString);
   
    UIWebView * _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, viewBodyFatTracker.frame.size.width, viewBodyFatTracker.frame.size.height)];
    
    MESSAGE(@"htmlString-> 5%@",htmlString);
    [_webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView scalesPageToFit];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    
  
   [viewBodyFatTracker addSubview:_webView];
    
}

//-----------------------------------------------------------------------

- (void) drawGraphForBMITraker {
    
    NSArray * arraySubViews = [viewBMITracker subviews];
    if ([arraySubViews count] > 0) {
        [[arraySubViews objectAtIndex:0] removeFromSuperview];
    }
    
    NSArray * bmiArr = [[DataManager initDB] getClientsBMIResult:self.strClientID];
    MESSAGE(@"bmiArr array %@",bmiArr);
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:JQueryFileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    NSString * dataStr  = [self stringFromArray:bmiArr];
    
    //
    BOOL bIsNegativeVal = FALSE;
    for (int i = 0; i < [bmiArr count]; i++) {
        for (int j = 0; j < [[bmiArr objectAtIndex:i] count]; j++){
            if ([[[bmiArr objectAtIndex:i] objectAtIndex:j] integerValue] < 0) {
                bIsNegativeVal = TRUE;
            }
        }
    }
    if (bIsNegativeVal) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"center"];
    }else{
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"bottom"];
    }
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#XDATA#" withString:[self stringForXaxies]];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#DATA#" withString:dataStr];

    //Sunil if more than one then no fill graph
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"false"];
    
    htmlString = [self setGraphLioneColor:htmlString];
    
    UIWebView * _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, viewBMITracker.frame.size.width, viewBMITracker.frame.size.height)];
    [_webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView scalesPageToFit];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    [viewBMITracker addSubview:_webView];
    
    
}

//-----------------------------------------------------------------------

- (void) drawGraphForCircumferenceMeasurement {
    
    
    NSArray * arraySubViews = [viewCircumferenceMeasurements subviews];
    if ([arraySubViews count] > 0) {
        [[arraySubViews objectAtIndex:0] removeFromSuperview];
    }
    NSArray * cmArr = [[DataManager initDB] getClientscircumferenceMeasurement:self.strClientID];
    // // DLOG(@"cmArr array %@",cmArr);
    
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:JQueryFileName ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    NSString * dataStr = [self stringFromArray:cmArr];
    
    BOOL bIsNegativeVal = FALSE;
    for (int i = 0; i < [cmArr count]; i++) {
        for (int j = 0; j < [[cmArr objectAtIndex:i] count]; j++){
            if ([[[cmArr objectAtIndex:i] objectAtIndex:j] integerValue] < 0) {
                bIsNegativeVal = TRUE;
            }
        }
    }
    if (bIsNegativeVal) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"center"];
    }else{
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#AxisPosition#" withString:@"bottom"];
    }
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#XDATA#" withString:[self stringForXaxies]];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#DATA#" withString:dataStr];

    //Sunil if more than one then no fill graph
    if (cmArr.count>1){
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"false"];
    }
    else {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#BOOLFILLGRAPH#" withString:@"true"];
        
    }
    
    htmlString = [self setGraphLioneColor:htmlString];
    
    UIWebView * _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, viewCircumferenceMeasurements.frame.size.width, viewCircumferenceMeasurements.frame.size.height)];
    [_webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView scalesPageToFit];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setOpaque:NO];
    [viewCircumferenceMeasurements addSubview:_webView];
    
}

//-----------------------------------------------------------------------

#pragma mark- UITextView delegate Methods

//-----------------------------------------------------------------------

- (void)keyboardWillHide {
    if ([self.btnAssessment isSelected]) {
        if ([txtViewGoals isFirstResponder]) {
            [txtViewGoals resignFirstResponder];
        }
    }
}

//-----------------------------------------------------------------------

- (void) textViewDidBeginEditing:(UITextView *)textView {
    
    if (textView == txtViewGoals) {
        self.scrollViewAssesmet.contentSize = CGSizeMake(0, 1730+200);
        [self autoScrollTextField:textView onScrollView:self.scrollViewAssesmet];
    }
    
}

//-----------------------------------------------------------------------

-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    
    return YES;
}

//-----------------------------------------------------------------------

- (void) textViewDidEndEditing:(UITextView *)textView {
    
    if (textView == txtViewGoals) {
        self.scrollViewAssesmet.contentSize = CGSizeMake(0, 1730);
        if (appDelegate.isAutoSave) {
            
            NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
            // DLOG(@"sheetId%ld",(long)sheetId);
            
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                       ksAssessment_Goal,kfieldName,
                                       txtViewGoals.text,kfieldValue,
                                       [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                                       self.strClientID,kClientId,
                                       @"2",kDictType,
                                       nil];
            DebugLOG([dictInfo description]);
            if ([[DataManager initDB] updateField:dictInfo]) {
                
            
                DebugLOG(@"Not Updated");
            }
            else {
                
                //Invoke method for update assessment gaol
                [self postRequestToUpdateAsssessment];
                
                DebugLOG(@"Updated");
            }
        }
        else if (!appDelegate.isAutoSave) {
            appDelegate.isFieldUpdated = YES;
        }
    }
    
}


//-----------------------------------------------------------------------

#pragma mark - UITextField Delegate Methods

//-----------------------------------------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSArray *arrayFields = self.keyboardControls.fields;
    NSInteger index = [arrayFields indexOfObject:textField];
    if (index <arrayFields.count-1) {
        UITextField *nextTextField = [arrayFields objectAtIndex:index+1];
        [nextTextField becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    
    return YES;
}

//-----------------------------------------------------------------------

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField.text floatValue] == 0.0f && ![textField isEqual:txtKeyExtra]) {
        textField.text = nil;
    }
    [self.keyboardControls setActiveField:textField];
    
    if ([self.btnPhotos isSelected]) {//PhotosView is selected
        [self autoScrollTextField:textField onScrollView:self.scrollViewData];
    }
    else{
        [self autoScrollTextField:textField onScrollView:self.scrollViewAssesmet];
    }
    if (textField == txtBMIHeight) {
        if ([btnStandard isSelected]) {
            strStandardHeight = txtBMIHeight.text;
        }
        else{
            strMetricHeight = txtBMIHeight.text;
        }
    }
    else if (textField == txtBMIWeight){
        if ([btnStandard isSelected]) {
            strStandardWeight= txtBMIWeight.text;
        }
        else{
            strMetricWeight= txtBMIWeight.text;
        }
    }
    
}

//-----------------------------------------------------------------------

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if ([textField.text floatValue] == 0 && ![textField isEqual:txtKeyExtra]) {
        textField.text = @"0.0";
    }
    
    if (textField == txt3STriceps || textField == txt3SSuprailiac || textField == txt3SThigh) {
        [self caluclateAndSetResultFor3Site];
    }
    else if (textField == txt4SAbdomen || textField == txt4SIllium || textField == txt4SThigh || textField == txt4STriceps ) {
        [self caluclateAndSetResultFor4Site];
    }
    else if (textField == txt7SAbdominal || textField == txt7SChest || textField == txt7SMidaxilari || textField == txt7SSubscapular || textField == txt7SSuparilic || textField == txt7SThig || textField == txt7STriceps) {
        [self caluclateAndSetResultFor7Site];
    }
    else if (textField == txtBMIHeight || textField == txtBMIWeight) {
        [self caluclateAndSetBMIResult];
    }
    
    
    if (appDelegate.isAutoSave) {
        [self autoSaveContents:textField];
    }
    else if (!appDelegate.isAutoSave) {
        appDelegate.isFieldUpdated = YES;
    }
    
    return YES;
}

//-----------------------------------------------------------------------

#pragma mark - Keyboard Controls Delegate Methods

//-----------------------------------------------------------------------

- (void)keyboardControlsDonePressed:(KeyboardControls *)keyboardControls{
    [keyboardControls.activeField resignFirstResponder];
}

//---------------------------------------------------------------------------------------------------------------

- (void)keyboardControls:(KeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(KeyboardControlsDirection)direction{
    
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : StoreKit Delegate MEthods

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[NSNotificationCenter defaultCenter] postNotificationName:kupdateVersionNotification object:nil];
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseFailed:(NSString*)productId error:(NSString *)error {
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction failed, Please try again", nil)];
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseCancelled:(NSString*)productId {
    
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction Cancelled, Please try again", nil)];
    
    
}

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------

-(void)setupLandscapeOrientation {
    START_METHOD
    // DLOG(@"Landsceape Assessment");
    self.isLandscapeMode = TRUE;
    [btnDate setFrame:CGRectMake(651+ 200, 11, 90, 21)];
    [lblDate setFrame:CGRectMake(600 + 200, 11, 50, 21)];
    [lblFitnessAssessmentName setFrame:CGRectMake(23, 18,kLandscapeWidth - 110, 45)];
    [lblSheetName setFrame:CGRectMake(31, 11, 145, 21)];
    [self.btnPhotos setFrame:CGRectMake(243+93, 75, 141, 34)];
    [self.btnAssessment setFrame:CGRectMake(381+93, 75, 145, 34)];
    
    [imgInnerBackground setFrame:CGRectMake(18, 0, kLandscapeWidth - 100, kLandscapeHeight - 190)]; //  h = 190
    [viewAssessment setFrame:CGRectMake(21 + 100, 131 - 15, 725, 644 - 185)];
    [self.scrollViewAssesmet setFrame:CGRectMake(0, 31, 725,428)];  // 610 - 180
    
    [self.viewPhotos setFrame:CGRectMake(21 + 100, 132 - 15, 725, 644- 185)];
    [self.scrollViewPhotos setFrame:CGRectMake(0, 27, 340, 431)];
    [self.scrollViewData setFrame:CGRectMake(319, 27, 407,431)];
    
    // set bottom controlls
    
    [btnForNextAssessmentPages setFrame:CGRectMake(257 +100, kLandscapeHeight - 192, 38, 34)];
    [btnForPreviousAssessmentPages setFrame:CGRectMake(83+100, kLandscapeHeight- 192, 38, 34)];
    [btnAssessmentPage1 setFrame:CGRectMake(128+100, kLandscapeHeight- 192, 38,34)];
    [btnAssessmentPage2 setFrame:CGRectMake(170+100, kLandscapeHeight-192, 38, 34)];
    [btnAssessmentPage3 setFrame:CGRectMake(212+100, kLandscapeHeight- 192, 38, 34)];
    [btnAddnewAssessment setFrame:CGRectMake(511 + 100, kLandscapeHeight- 192, 173, 34)];
    
    if([popoverController isPopoverVisible]) {
        [self btnDateClicked:nil];
    }
    
    
}


//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
    START_METHOD
    // DLOG(@"Portrait Assessment");
    self.isLandscapeMode = FALSE;
    [viewAssessment setFrame:CGRectMake(21, 313, 725, 644)];
    [self.viewPhotos setFrame:CGRectMake(21, 131, 725, 644)];
    [btnDate setFrame:CGRectMake(651, 11, 90, 21)];
    [lblDate setFrame:CGRectMake(600, 11, 50, 21)];
    [lblFitnessAssessmentName setFrame:CGRectMake(23, 18, 721, 45)];
    [lblSheetName setFrame:CGRectMake(31, 11, 145, 21)];
    [self.btnPhotos setFrame:CGRectMake(243, 75, 141, 34)];
    [self.btnAssessment setFrame:CGRectMake(381, 75, 145, 34)];
    
    [imgInnerBackground setFrame:CGRectMake(18, -37, 731, 813)];
    
    //setup view layout
    [viewAssessment setFrame:CGRectMake(21, 131, 725, 644)];
    [self.scrollViewAssesmet setFrame:CGRectMake(0, 31, 725, 610)];
    
    [self.viewPhotos setFrame:CGRectMake(21, 132, 725, 644)];
    [self.scrollViewPhotos setFrame:CGRectMake(0, 27, 340, 610)];
    [self.scrollViewData setFrame:CGRectMake(319, 27, 407, 610)];
    
    //bottom Layer View
    
    [btnForNextAssessmentPages setFrame:CGRectMake(257, 773, 38, 34)];
    [btnForPreviousAssessmentPages setFrame:CGRectMake(83, 773, 38, 34)];
    [btnAssessmentPage1 setFrame:CGRectMake(128, 773, 38, 34)];
    [btnAssessmentPage2 setFrame:CGRectMake(170, 773, 38, 34)];
    [btnAssessmentPage3 setFrame:CGRectMake(212, 773, 38, 34)];
    [btnAddnewAssessment setFrame:CGRectMake(511, 773, 173, 34)];
    
    if([popoverController isPopoverVisible]) {
        [self btnDateClicked:nil];
    }
}


//-----------------------------------------------------------------------

#pragma mark - ViewLifeCycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    //ScrollView Content Sizes..
    self.scrollViewPhotos.contentSize = CGSizeMake(0, 1200);
    self.scrollViewData.contentSize = CGSizeMake(0, 1730);
    if ([[self.view findFirstResponder] isEqual:txtViewGoals]) {
    }
    else{
        self.scrollViewAssesmet.contentSize = CGSizeMake(0, 1730);
    }
    //
}

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupLayout];
    
    viewCommonAssessment = [[[NSBundle mainBundle] loadNibNamed:@"AssessmentCommonView" owner:self options:nil] objectAtIndex:0];
    [self setAllViewsToInitalState];
    arraySheets = [NSArray array];
    arrayImages = [NSArray array];
    
    NSArray *fields = @[
                        txtBMWeight,
                        txtBMHeight,
                        txtBMHeartRate,
                        txtBMBloodPressure,
                        txt3STriceps,
                        txt3SSuprailiac,
                        txt3SThigh,
                        txt4STriceps,
                        txt4SAbdomen,
                        txt4SIllium,
                        txt4SThigh,
                        txt7SChest,
                        txt7SAbdominal,
                        txt7SThig,
                        txt7STriceps,
                        txt7SSubscapular,
                        txt7SSuparilic,
                        txt7SMidaxilari,
                        txtGeneralBodyFat,
                        txtCmRightUpeerArm,
                        txtCmRightForeArm,
                        txtCmChest,
                        txtCmAbdominal,
                        txtCmHipButtocks,
                        txtCmRightThigh,
                        txtCmRightCalf,
                        txtKeyExtra,
                        txtValueExtra,
                        txtBMIHeight,
                        txtBMIWeight
                        ];
    
    [self setKeyboardControls:[[KeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    fields = nil;
    
    txtViewGoals.autocorrectionType = UITextAutocorrectionTypeNo;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    
    [super viewWillAppear:animated];
    
    
    //SUNIL
    //Set by default all iamge set default
    [lblAddPhoto1 setHidden:FALSE];
    [lblAddPhoto2 setHidden:FALSE];
    [lblAddPhoto3 setHidden:FALSE];
    [lblAddPhoto4 setHidden:FALSE];
    [lblAddPhoto5 setHidden:FALSE];
    
    
    currentPage = 0;
    [self localizeControls];
    // [self checkNewFieldsInTableExistsForClientId:self.strClientID];
    // DLOG(@"Update Data as not updated");
    [[DataManager initDB] addNewFieldsToTablesIfnotAddedForClientId:self.strClientID];
    
    
    if (btnStandard.selected) {
        //standard
        //   [txtBMIResult setBackgroundColor:[UIColor colorWithRed:255/255 green:154/255 blue:167/255 alpha:0.5]];
        
    } else {
        //convert cm height in meter
        //   [txtBMIResult setBackgroundColor:[UIColor colorWithRed:92/255 green:255/255 blue:168/255 alpha:0.5]];
    }
    
    
    if(self.isLandscapeSet) {
        [self setupLandscapeOrientation];
    } else {
        [self setupPortraitOrientation];
    }
    
    
}
//-----------------------------------------------------------------------

-(void)viewDidAppear:(BOOL)animated{
    START_METHOD
    [super viewDidAppear:animated];
    
    
    textViewBMI.text = NSLocalizedString(@"BMI", nil);
    
    //ScrollView Content Sizes..
    self.scrollViewPhotos.contentSize = CGSizeMake(0, 1200); // 1000
    self.scrollViewData.contentSize = CGSizeMake(0, 1730);
    self.scrollViewAssesmet.contentSize = CGSizeMake(0, 1730);
    
    // [super viewDidAppear:animated];
    [self displaywebViewBodyFatPercentageContent];
    [self getAllSheetsList];
    
    if([arraySheets count] ==0) {
        [[DataManager initDB]insertNewAssessmentSheetFor:self.strClientID sheetName:@"Assessment-1"];
        [self getAllSheetsList];
    }
    
    [self setSheetPageButtons];
    [self btnAssessmentPageClicked:btnAssessmentPage1];
    [self drawGraphForWeightTraker];
    [self drawGraphForBodyFatPercentage];
    [self drawGraphForBMITraker];
    [self drawGraphForCircumferenceMeasurement];
    [self drawGraphForGeneralBodyFatPercentageTracker];
    
    
    NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientID];
    
    NSString *strName = [[clientInfo objectForKey:kFirstName] stringByAppendingString:[NSString stringWithFormat:@" %@",[clientInfo objectForKey:kLastName]]];
    strName = [strName stringByAppendingString:NSLocalizedString(@"'s FITNESS ASSESSMENT", nil)];
    lblFitnessAssessmentName.text = strName;
    NSInteger length = lblFitnessAssessmentName.attributedText.length -18;
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithAttributedString: lblFitnessAssessmentName.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName value:kAppTintColor range:NSMakeRange(0, length)];
    
    
    [lblFitnessAssessmentName setAttributedText: text];
    btnDate.titleLabel.font = [UIFont systemFontOfSize:14];
    
    
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    NSArray *imagesForclient = [[DataManager initDB] getAllAssessmentImagesForSheet:[NSString stringWithFormat:@"%ld",(long)sheetId] ofClientID:self.strClientID];
    lblSheetName.text = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    
    
    if([imagesForclient count] > 0) {
        for (int i= 101 ; i<105; i++) {
            NSInteger index = i -101;
            if([[[imagesForclient objectAtIndex:index] objectForKey:@"hasImage"] integerValue] == 0) {
                UIButton * btnimgView = (UIButton *)[self.scrollViewPhotos viewWithTag:i];
                
                //Sunil Set add Photo
                UIImage *image        =   [self setAddPhotos];
                [btnimgView setImage:image forState:UIControlStateNormal];
                //   [btnimgView setTitle:NSLocalizedString(@"ADD PHOTO", nil)  forState:UIControlStateNormal];
                [btnimgView setUserInteractionEnabled:YES];
                
                [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+i] setText:@""];
                [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag + i] setHidden:YES];
                [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + i] setHidden:YES];
            }
        }
    }
    
    [self setlastVistedPage];
    [btnForNextAssessmentPages setAutoresizingMask:UIViewAutoresizingNone];
    [btnForPreviousAssessmentPages setAutoresizingMask:UIViewAutoresizingNone];
    [btnAssessmentPage1 setAutoresizingMask:UIViewAutoresizingNone];
    [btnAssessmentPage3 setAutoresizingMask:UIViewAutoresizingNone];
    [btnAssessmentPage2 setAutoresizingMask:UIViewAutoresizingNone];
    
    [self.view bringSubviewToFront:btnAssessmentPage1];
    [self.view bringSubviewToFront:btnAssessmentPage2];
    [self.view bringSubviewToFront:btnAssessmentPage3];
    [self.view bringSubviewToFront:btnForNextAssessmentPages];
    [self.view bringSubviewToFront:btnForPreviousAssessmentPages];
    
}


-(void)viewDidDisappear:(BOOL)animated{
    //SUNIL
    //Set by default all iamge set default
    [lblAddPhoto1 setHidden:FALSE];
    [lblAddPhoto2 setHidden:FALSE];
    [lblAddPhoto3 setHidden:FALSE];
    [lblAddPhoto4 setHidden:FALSE];
    [lblAddPhoto5 setHidden:FALSE];
    
}
//sunil
-(void)setViewOnBasisSkin{
  
    START_METHOD
  
    NSInteger length = lblFitnessAssessmentName.attributedText.length -18;
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithAttributedString: lblFitnessAssessmentName.attributedText];
    
     btnDate.titleLabel.font = [UIFont systemFontOfSize:14];
   
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    //Set Color of voew
    UIColor *color ;
    UIImage *imageBoxTitle;
    
    switch (skin) {
        case FIRST_TYPE:
            
            lblFitnessAssessmentName.textColor = [UIColor whiteColor];
             [text addAttribute:NSForegroundColorAttributeName value:kFirstSkin range:NSMakeRange(0, length)];
            
            lblSheetName.textColor = [UIColor whiteColor];
            [btnDate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            lblDate.textColor = [UIColor whiteColor];
            
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:UIControlStateSelected];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-gray.png"] forState:UIControlStateNormal];
            [btnForPreviousAssessmentPages setImage:[UIImage imageNamed:@"prev_green-assessment.png"] forState:UIControlStateNormal];
            
            [btnAssessmentPage1 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnAssessmentPage2 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnAssessmentPage3 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnForNextAssessmentPages setImage:[UIImage imageNamed:@"next_green-assessment.png"] forState:UIControlStateNormal];
            
            [btnAddnewAssessment setImage:[UIImage imageNamed:@"green_add_new_assessment_button.png"] forState:UIControlStateNormal];
            [btnAddnewAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [self.btnAssessment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            
            //Result Lable SUNIL
            lbl3sResult.textColor = [UIColor whiteColor];;
            lbl4SResult.textColor = [UIColor whiteColor];;
            lbl7sResult.textColor = [UIColor whiteColor];;
            lblBMIResult.textColor = [UIColor whiteColor];;
            
            //Set Background color
            lbl3sResult.backgroundColor =kBoxDarkGrayColor;
            lbl4SResult.backgroundColor = kBoxDarkGrayColor;
            lbl7sResult.backgroundColor =  kBoxDarkGrayColor;
            lblBMIResult.backgroundColor = kBoxDarkGrayColor;
            
            //Sunil
            color=kBoxGrayColor;
            imageBoxTitle = [UIImage imageNamed:@"gray_ass-title-bg.png"];
            
            //Sunil Invoke for set color
            [self setBoxexViewColor:color andImage:imageBoxTitle];
        
          webViewBodyFatPercentage.backgroundColor = [UIColor colorWithRed:19/255.0f green:27/255.0f  blue:31/255.0f  alpha:1.0];
   
            break;
        case SECOND_TYPE:
            
            lblFitnessAssessmentName.textColor = [UIColor whiteColor];
            [text addAttribute:NSForegroundColorAttributeName value:ksecondSkin range:NSMakeRange(0, length)];
            
           lblSheetName.textColor = [UIColor whiteColor];
            
            [btnDate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            lblDate.textColor = [UIColor whiteColor];
            
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:UIControlStateSelected];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-gray.png"] forState:UIControlStateNormal];
            [btnForPreviousAssessmentPages setImage:[UIImage imageNamed:@"prev_red-assessment.png"] forState:UIControlStateNormal];
            
            
            [btnAssessmentPage1 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnAssessmentPage2 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnAssessmentPage3 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnForNextAssessmentPages setImage:[UIImage imageNamed:@"next_red-assessment.png"] forState:UIControlStateNormal];
            
            [btnAddnewAssessment setImage:[UIImage imageNamed:@"red_add_new_assessment_button.png"] forState:UIControlStateNormal];
            [btnAddnewAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.btnAssessment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            //Result Lable SUNIL
            lbl3sResult.textColor = [UIColor whiteColor];
            lbl4SResult.textColor = [UIColor whiteColor];;
            lbl7sResult.textColor = [UIColor whiteColor];;
            lblBMIResult.textColor = [UIColor whiteColor];;
            
            //Set Background color
            lbl3sResult.backgroundColor = kBoxDarkGrayColor;
            lbl4SResult.backgroundColor =  kBoxDarkGrayColor;
            lbl7sResult.backgroundColor = kBoxDarkGrayColor;
            lblBMIResult.backgroundColor =  kBoxDarkGrayColor;
 
            
           //Sunil
            color=kBoxGrayColor;
            imageBoxTitle = [UIImage imageNamed:@"light_gray_ass-title-bg.png"];
            
            //Sunil Invoke for set color
            [self setBoxexViewColor:color andImage:imageBoxTitle];
        
        
          webViewBodyFatPercentage.backgroundColor = [UIColor colorWithRed:19/255.0f green:27/255.0f  blue:31/255.0f  alpha:1.0];
            
            break;
            
        case THIRD_TYPE:
             lblSheetName.textColor = [UIColor grayColor];
            
            lblFitnessAssessmentName.textColor = [UIColor grayColor];
            [text addAttribute:NSForegroundColorAttributeName value:kThirdSkin range:NSMakeRange(0, length)];
            
            [btnDate setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            lblDate.textColor = [UIColor grayColor];
            
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-pink.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-pink.png"] forState:UIControlStateSelected];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-pink.png"] forState:UIControlStateNormal];
            
            [btnForPreviousAssessmentPages setImage:[UIImage imageNamed:@"prev_pink-assessment.png"] forState:UIControlStateNormal];
            
            [btnAssessmentPage1 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnAssessmentPage2 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnAssessmentPage3 setImage:[UIImage imageNamed:@"light-gray_assessment.png"] forState:UIControlStateNormal];
            [btnForNextAssessmentPages setImage:[UIImage imageNamed:@"next_pink-assessment.png"] forState:UIControlStateNormal];
            [btnAddnewAssessment setImage:[UIImage imageNamed:@"pink_add_new_assessment_button.png"] forState:UIControlStateNormal];
            [btnAddnewAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [self.btnAssessment setTitleColor:kThirdSkin forState:UIControlStateNormal];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            //Result Lable SUNIL
            lbl3sResult.textColor = kThirdSkin;
            lbl4SResult.textColor = kThirdSkin;
            lbl7sResult.textColor = kThirdSkin;
            lblBMIResult.textColor = kThirdSkin;
            
            //Set Background color
            lbl3sResult.backgroundColor = [UIColor whiteColor];
            lbl4SResult.backgroundColor = [UIColor whiteColor];
            lbl7sResult.backgroundColor = [UIColor whiteColor];
            lblBMIResult.backgroundColor = [UIColor whiteColor];
            
            //Sunil
            color=[UIColor whiteColor];
            imageBoxTitle = [UIImage imageNamed:@"white_gray_ass-title-bg.png"];
            
            //Sunil Invoke for set color
            [self setBoxexViewColor:color andImage:imageBoxTitle];
        
//          webViewBodyFatPercentage.backgroundColor = [UIColor colorWithRed:237/255.0f green:236/255.0f  blue:233/255.0f  alpha:1.0];
        
            break;
            
        default:
            
            [btnAddnewAssessment setImage:[UIImage imageNamed:@"Add-New-Line"] forState:UIControlStateNormal];
            [btnForPreviousAssessmentPages setImage:[UIImage imageNamed:@"prev-assessment"] forState:UIControlStateNormal];
            [btnForNextAssessmentPages setImage:[UIImage imageNamed:@"next-assessment"] forState:UIControlStateNormal];

            [btnAssessmentPage1 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
            [btnAssessmentPage2 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
            [btnAssessmentPage3 setImage:[UIImage imageNamed:@"page-number-act"] forState:UIControlStateNormal];
            
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn"] forState:UIControlStateSelected];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn"] forState:UIControlStateNormal];
            
            lblFitnessAssessmentName.textColor = [UIColor grayColor];
            [text addAttribute:NSForegroundColorAttributeName value:kAppTintColor range:NSMakeRange(0, length)];
            
            [btnDate setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            lblDate.textColor = [UIColor grayColor];
             lblSheetName.textColor = [UIColor grayColor];
            
            [btnAddnewAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.btnAssessment setTitleColor:kOldSkin forState:UIControlStateNormal];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            //Result Lable SUNIL
            lbl3sResult.textColor = kOldSkin;
            lbl4SResult.textColor = kOldSkin;
            lbl7sResult.textColor = kOldSkin;
            lblBMIResult.textColor = kOldSkin;
         
            //Sunil
            color=[UIColor whiteColor];
            imageBoxTitle = [UIImage imageNamed:@"white_gray_ass-title-bg.png"];
            
            //Sunil Invoke for set color
            [self setBoxexViewColor:color andImage:imageBoxTitle];
        
          webViewBodyFatPercentage.backgroundColor = color;
            
            break;
    }
    
    //Set Attributed Text
     [lblFitnessAssessmentName setAttributedText: text];
     
    END_METHOD
}


-(void)setButtonAssesmentSelectionIcons{
    START_METHOD
    
    int skin =  [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            
            [self.btnPhotos setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            [self.btnAssessment setSelected:true];
            [self.btnAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            break;
            
            
        case SECOND_TYPE:
            
            [self.btnPhotos setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            [self.btnAssessment setSelected:true];
            [self.btnAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            break;
            
            
            
        case THIRD_TYPE:
            
            [self.btnPhotos setTitleColor:kThirdSkin forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn-pink.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn-pink.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            [self.btnAssessment setSelected:true];
            [self.btnAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn-pink.png"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn-pink.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            break;

            
            
        default:
            [self.btnPhotos setTitleColor:kAppTintColor forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-dis-btn"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            [self.btnAssessment setSelected:true];
            [self.btnAssessment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-act-btn"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            
            break;
    }
    
    
    
    
    END_METHOD
}

-(void)setButtonPhotosSelectionIcons{
    START_METHOD
    
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setSelected:false];
            
            [self.btnAssessment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            break;
            
        case SECOND_TYPE:
            
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setSelected:false];
            
            [self.btnAssessment setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-gray.png"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-gray.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            break;
            
        case THIRD_TYPE:
            
            self.btnPhotos.backgroundColor = kThirdSkin;
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-pink.png"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn-pink.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setSelected:false];
            
            [self.btnAssessment setTitleColor:kThirdSkin forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-pink.png"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn-pink.png"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            break;
            
        default:
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnPhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn"] forState:UIControlStateNormal];
            [self.btnPhotos setImage:[UIImage imageNamed:@"left-act-btn"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            self.btnPhotos.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setSelected:false];
            
            [self.btnAssessment setTitleColor:kAppTintColor forState:UIControlStateNormal];
            self.btnAssessment.backgroundColor = [UIColor clearColor];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn"] forState:UIControlStateNormal];
            [self.btnAssessment setImage:[UIImage imageNamed:@"right-dis-btn"] forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            break;
    }
    
    
    
    
    END_METHOD
}

-(void)setBoxexViewColor:(UIColor *)color andImage:(UIImage *)image
{
    START_METHOD
    //Set Color of voew
    viewBasicMeasurement.backgroundColor =color;
    viewBodyFatPercentage.backgroundColor =color;
    viewgeneralBodyFat.backgroundColor =color;
    viewCircumMeasur.backgroundColor =color;
    viewBodyMassage.backgroundColor =color;
    viewBodyMassage1.backgroundColor =color;
    
    imgViewBasivMesurementTitle.image = image;
    imgViewBodyFatPercentageTitle.image = image;
    imgViewgeneralBodyFatTitle.image = image;
    imgviewCircumMeasurTitle.image = image;
    imgViewBodyMassageTitle.image = image;
    imgViewBodyMassage1Title.image = image;
    
    
    if([commonUtility retrieveValue:KEY_SKIN].intValue == 1 || [commonUtility retrieveValue:KEY_SKIN].intValue == 2 ){
    
        MESSAGE(@"*******************");
    viewWieghtTracker1.backgroundColor =kBoxDarkGrayColor;
    viewBodyFatTracker1.backgroundColor =kBoxDarkGrayColor;
    viewGernalBodyFatTracker1.backgroundColor =kBoxDarkGrayColor;
    viewCircumfrenceMeasure1.backgroundColor =kBoxDarkGrayColor;
    viewBmiTracker1.backgroundColor =kBoxDarkGrayColor;
    viewBodyMassIndexResult.backgroundColor =kBoxDarkGrayColor;
    viewGoals.backgroundColor =kBoxDarkGrayColor;
        
        viewWeightTracker.backgroundColor =kBoxDarkGrayColor;
        viewBodyFatTracker.backgroundColor =kBoxDarkGrayColor;
        viewGeneralBodyFatTracker.backgroundColor =kBoxDarkGrayColor;
        viewCircumferenceMeasurements.backgroundColor =kBoxDarkGrayColor;
        viewBMITracker.backgroundColor =kBoxDarkGrayColor;
        viewBodyMassIndexResult.backgroundColor =kBoxDarkGrayColor;
        viewGoals.backgroundColor =kBoxDarkGrayColor;
    
    }else{
        viewWieghtTracker1.backgroundColor =color;
        viewBodyFatTracker1.backgroundColor =color;
        viewGernalBodyFatTracker1.backgroundColor =color;
        viewCircumfrenceMeasure1.backgroundColor =color;
        viewBmiTracker1.backgroundColor =color;
        viewBodyMassIndexResult.backgroundColor =color;
        viewGoals.backgroundColor =color;
        
        viewWeightTracker.backgroundColor =color;
        viewBodyFatTracker.backgroundColor =color;
        viewGeneralBodyFatTracker.backgroundColor =color;
        viewCircumferenceMeasurements.backgroundColor =color;
        viewBMITracker.backgroundColor =color;
        viewBodyMassIndexResult.backgroundColor =color;
        viewGoals.backgroundColor =color;
    }
  
    
    imgViewWieghtTracker1Title.image = image;
    imgViewbodyFatTrackerTitle.image = image;
    imgViewGernalBodyFatTracker1title.image = image;
    imgViewCircumfrenceMeasure1Title.image = image;
    imgViewBMITrackerTitlw.image = image;
    imgViewBodyMassIndexResultTitle.image = image;
    imgViewGoals.image = image;
    
  
    
    
    END_METHOD
}

//Sunil
-(UIImage *)setAddPhotos{
    START_METHOD
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    UIImage *image;
    
    switch (skin) {
        case FIRST_TYPE:
            
            image= [UIImage imageNamed:@"green_add-photo.png"];
            
            break;
            
        case SECOND_TYPE:
            
            image= [UIImage imageNamed:@"red_add-photo.png"];
            
            break;
        case THIRD_TYPE:
            
            image= [UIImage imageNamed:@"pink_add-photo.png"];
            
            break;
  
            
        default:
            image =   [UIImage imageNamed:@"add-photo"];
            break;
    }
    
    return image;
    
    END_METHOD
}

//SUNIL
-(NSString * )setGraphLioneColor:(NSString *)htmlString{
    START_METHOD
  
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    NSString *strColor;
    NSString *strColorGraph;
    
    
    //    green:#88d208 red:#ed1b23 pink:#e92767 white:#ffffff
    switch (skin) {
        case FIRST_TYPE:
            strColor= @"#88d208";
            //       line.Set('chart.fillstyle',['Gradient(#FF3D4C:#FF5C73: #FF7A99:#FF99BF:#FFB8E6)']);

            strColorGraph= @"#88d208:#7ab815:#679427:#4e673e:#414e49:#3e484d";
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:strColorGraph];

            //Sunil invoke method
//            [self setButtotnMetricSeledted];
            
            break;
            
        case SECOND_TYPE:
            strColor= @"#ed1b23";
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:@"#ed1b23:#ef3037:#f4767a:#f89ea1:#ffffff:#ffffff"];
            
            //Sunil invoke method
//            [self setButtotnMetricSeledted];
            
            break;
        case THIRD_TYPE:
            strColor= @"#e92767";
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];

 htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:@"#e92767:#ed4b81:#f1759e:#f498b7:#fac9d9:#ffffff:#ffffff"];
            
            //Sunil invoke method
//            [self setButtotnMetricSeledted];
            
            break;
            
        default:
            
            //#48c5b8     //Gray color code
            strColor= @"#8b9091";
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLine#" withString:strColor];
//#4ac3ba:#68cdc5:#87d7d1:#b1e5e1:#d3f1ef:#ffffff   // for gradient of sky color
 htmlString = [htmlString stringByReplacingOccurrencesOfString:@"#colorLineGraph#" withString:@"#8b9091:#8b9091:#a2a6a7:#c1c4c4:#dfe0e0:#ffffff"];
            
            
            //Sunil invoke method
//            [self setButtotnMetricSeledted];
            break;
    }
    
    MESSAGE(@"colorLine----------> %@",htmlString);
    
    return htmlString;
    END_METHOD
}

//Sunil
-(void)setButtotnMetricSeledted{
    START_METHOD
    
    if([btnMetric isSelected] || [btnStandard isSelected]){
        btnMetric.selected = NO;
        btnStandard.selected = YES;
        
    }
    
    int  skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            
            [btnMetric setTitleColor:kFirstSkin forState:UIControlStateNormal];
            btnMetric.layer.borderColor = kFirstSkin.CGColor;
            btnStandard.backgroundColor = kFirstSkin;
        
            
            break;
            
        case SECOND_TYPE:
            
            [btnMetric setTitleColor:ksecondSkin forState:UIControlStateNormal];
            btnMetric.layer.borderColor = ksecondSkin.CGColor;
            btnStandard.backgroundColor = ksecondSkin;
            
            
            break;
        case THIRD_TYPE:
            
            [btnMetric setTitleColor:kThirdSkin forState:UIControlStateNormal];
            btnMetric.layer.borderColor = kThirdSkin.CGColor;
            btnStandard.backgroundColor = kThirdSkin;
            
            
            break;
            
            
        default:
            [btnMetric setTitleColor:kAppTintColor forState:UIControlStateNormal];
            btnMetric.layer.borderColor = kAppTintColor.CGColor;
            btnStandard.backgroundColor = kAppTintColor;

            break;
    }
    
    END_METHOD
}


//Sunil
-(void)setButtotnStandardSeledted{
    START_METHOD
    
    int  skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            
            
            [btnStandard setTitleColor:kFirstSkin forState:UIControlStateNormal];
            btnStandard.layer.borderColor = kFirstSkin.CGColor;
            btnMetric.backgroundColor = kFirstSkin;
            
            
            break;
            
        case SECOND_TYPE:
            
            [btnStandard setTitleColor:ksecondSkin forState:UIControlStateNormal];
            btnStandard.layer.borderColor = ksecondSkin.CGColor;
            btnMetric.backgroundColor = ksecondSkin;
            
            
            break;
            
        case THIRD_TYPE:
            
            [btnStandard setTitleColor:kThirdSkin forState:UIControlStateNormal];
            btnStandard.layer.borderColor = kThirdSkin.CGColor;
            btnMetric.backgroundColor = kThirdSkin;
     
            
            break;
            
        default:
            [btnStandard setTitleColor:kAppTintColor forState:UIControlStateNormal];
            btnStandard.layer.borderColor = kAppTintColor.CGColor;
            btnMetric.backgroundColor = kAppTintColor;
            break;
    }
    
    END_METHOD
}


//sunil
-(NSString *)setBodyChartOnBasisSkin:(NSString *)htmlString{
    START_METHOD
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    MESSAGE(@"Html body fat tracker : %@",htmlString);
    switch (skin) {
        case FIRST_TYPE:
        case SECOND_TYPE:
        
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"a7dcfc" withString:@"b1babd"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"ffcadc" withString:@"b1babd"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"b8ffbb" withString:@"b1babd"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"grey" withString:@"white"];
        
            break;
        
        case THIRD_TYPE:
        
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"a7dcfc" withString:@"FFFFFF"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"ffcadc" withString:@"FFFFFF"];
            htmlString = [htmlString stringByReplacingOccurrencesOfString:@"b8ffbb" withString:@"FFFFFF"];
        
            break;
        
        default:
     
        break;
    }
    
    return htmlString;
    END_METHOD
}


//TODO: SERVER REQUESTS

-(void)postRequestToUpdateAsssessment{
    START_METHOD
    
    //GEt Assessment sheet data
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    
    //Get Assessment name and id
    NSArray *arrayAssesmentSheetFullData  =   [[DataManager initDB]getAssessmentSheetsDataForSheet:[NSString stringWithFormat:@"%ld",(long)sheetId] ofClientID:self.strClientID];
    
    if(arrayAssesmentSheetFullData && arrayAssesmentSheetFullData.count>0){
        
        NSMutableDictionary *dictAssessments =   [arrayAssesmentSheetFullData objectAtIndex:0];
        
        [dictAssessments setValue:@"updateAssessment" forKey:ACTION];
        [dictAssessments setValue:self.strClientID forKey:kClientId];
        [dictAssessments setValue:lblSheetName.text  forKey:ksheetName];
        [dictAssessments setValue:[NSString stringWithFormat:@"%ld",(long)sheetId] forKey:@"nDetailID"];
        
        //Make url for hit the Trainer Profile
        NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
        
        MESSAGE(@"params dictAssessments=: %@ and Url : %@",dictAssessments,strUrl);
        
        //Invoke method for Encrypt the message by SHA1
        ServiceHandler *objService = [[ServiceHandler alloc]init];
        
        
        //invoke method
        [objService postRequestWitHUrl:strUrl parameters:dictAssessments
                               success:^(NSDictionary *responseDataDictionary) {
                                   
                                   MESSAGE(@"responce from file: %@", responseDataDictionary);
                                   
                                   //Hide the indicator
                                   [TheAppController hideHUDAfterDelay:0];
                                   
                                   //If Success
                                   if([responseDataDictionary objectForKey:PAYLOAD]){
                                       
                                       
                                       //************  GET Client's Profile AND SAVE IN LOCAL DB
                                       NSDictionary *dictNoteTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                       
                                       MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictAssessments,dictNoteTemp);
                                       
                                       
                                       
                                   }else{//If Server respose a Error
                                       
                                       //Check
                                       if([responseDataDictionary objectForKey:METADATA]){
                                           
                                           //Get Dictionary
                                           NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                           
                                           //Show Error Alert
                                           [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                           
                                           //Hide Indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                           
                                           
                                           
                                           //For Unauthorized user --->
                                           if( [dictError objectForKey:LIST_KEY]){
                                               
                                               NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                               
                                               if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                                   
                                                   //Show Error Alert
                                                   [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                                   
                                                   //Move to dashbord for login
                                                   [commonUtility  logOut];
                                               }
                                               
                                           }
                                           
                                           //<-----
                                           
                                           
                                           
                                           
                                       }
                                   }
                                   
                                   
                               }failure:^(NSError *error) {
                                   MESSAGE(@"Eror : %@",error);
                                   
                                   //Hide the indicator
                                   [TheAppController hideHUDAfterDelay:0];
                                   
                                   //Show Alert For error

                                   [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                                   
                               }];
    }
    END_METHOD
}

//Method for Post Updated Sheet Name
-(void)postRequestForUpdateSheetName{
    START_METHOD
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    lblSheetName.text = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    

    
    //Get Assessment name and id
    NSArray *arrayAssessmentSheet  =   [[DataManager initDB]getAssessmentSheetsFor:self.strClientID andSheetId:[NSString stringWithFormat:@"%ld",(long)sheetId]];
    
    MESSAGE(@"arraySheetData data updated: %@",arrayAssessmentSheet);
    
    if(arrayAssessmentSheet && arrayAssessmentSheet.count>0){
    //Set Updated name
    lblSheetName.text = [[arraySheets objectAtIndex:currentSheet-1] valueForKey:ksheetName];
    
   
        
        NSMutableDictionary *dictAssessments = [[NSMutableDictionary alloc]initWithDictionary:  [arrayAssessmentSheet objectAtIndex:0]];
        
        [dictAssessments setValue:@"updateAssessment" forKey:ACTION];
        [dictAssessments setValue:self.strClientID forKey:kClientId];
        [dictAssessments setValue:[dictAssessments objectForKey:ksheetID] forKey:@"nDetailID"];
        
        //Make url for hit the Trainer Profile
        NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
        
        MESSAGE(@"params dictAssessments=: %@ and Url : %@",dictAssessments,strUrl);
        
        //Invoke method for Encrypt the message by SHA1
        ServiceHandler *objService = [[ServiceHandler alloc]init];
        
        
        //invoke method
        [objService postRequestWitHUrl:strUrl parameters:dictAssessments
                               success:^(NSDictionary *responseDataDictionary) {
                                   
                                   MESSAGE(@"responce from file: %@", responseDataDictionary);
                                   
                                   //Hide the indicator
                                   [TheAppController hideHUDAfterDelay:0];
                                   
                                   //If Success
                                   if([responseDataDictionary objectForKey:PAYLOAD]){
                                       
                                       
                                       //************  GET Client's Profile AND SAVE IN LOCAL DB
                                       NSDictionary *dictNoteTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                       
                                       MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictAssessments,dictNoteTemp);
                                       
                                       
                                       
                                   }else{//If Server respose a Error
                                       
                                       //Check
                                       if([responseDataDictionary objectForKey:METADATA]){
                                           
                                           //Get Dictionary
                                           NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                           
                                           //Show Error Alert
                                           [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                           
                                           //Hide Indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                           
                                           
                                           
                                           
                                           //For Unauthorized user --->
                                           if( [dictError objectForKey:LIST_KEY]){
                                               
                                               NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                               
                                               if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                                   
                                                   //Show Error Alert
                                                   [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                                   
                                                   //Move to dashbord for login
                                                   [commonUtility  logOut];                                               }
                                               
                                           }
                                           
                                           //<-----
                                       }
                                   }
                                   
                                   
                               }failure:^(NSError *error) {
                                   MESSAGE(@"Eror : %@",error);
                                   
                                   //Hide the indicator
                                   [TheAppController hideHUDAfterDelay:0];
                                   
                                   //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                                   
                               }];
    }
    
    END_METHOD
}


-(void)postRequestForAddNewAssessment:(NSString *)strAssessment{
    START_METHOD
    
    //Set by default all iamge set default
    [lblAddPhoto1 setHidden:FALSE];
    [lblAddPhoto2 setHidden:FALSE];
    [lblAddPhoto3 setHidden:FALSE];
    [lblAddPhoto4 setHidden:FALSE];
    [lblAddPhoto5 setHidden:FALSE];
    
    
    
    
    NSDictionary *dictAssessment =@{
                              ksheetName : strAssessment,
                              kClientId :self.strClientID,
                              ACTION:@"addAssessment",
                              @"user_id":[commonUtility retrieveValue:KEY_TRAINER_ID]
                              };
    
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictAssessment=: %@ and Url : %@",dictAssessment,strUrl);
    
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictAssessment
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce dictAssessment from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictAssesmentTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send dictAssessment Dictonary : %@ \n and response from server dictAssessment: %@",dictAssessment,dictAssesmentTemp);
                          
                                   
                                   NSString *strAction =   [dictAssesmentTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addAssessment"]){
                                       
                                       [self createNewAssesmentSheetForName:@"" andDict:dictAssesmentTemp];
                                   }
                                   
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:STATUS_LIST]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:STATUS_LIST];
                                           if(objDict && [[objDict objectForKey:@"message_id"] isEqualToString:@"8"]){
                                               
                                              
                                                   //Invoke method for Show alert for Free user
                                                   [self checkFreeUserAccesForAddAssesment:[dictError objectForKey:POPUPTEXT]];
                                               
                                           }
                                       }
                                       else{
                                       
                                       
                                       
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                       
                                   }
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}


//TODO: POST REQUEST for save the assessennt image on the server

-(void)postRequestForAssessmentImage:(UIImage*)image withTitle:(NSString*)photoTitle andWithPhotoButtonTag:(NSInteger)photoButtonTag{

    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    
    NSDictionary * selObj = [arrayImages objectAtIndex:photoButtonTag-101];
    
    int intTag = (int)photoButtonTag;
    
    int intReminder = intTag%5;
    
    if (intReminder==0) {
        
        intReminder = 5;
    }
     
    
    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientID,kClientId,
                               [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                               [NSString stringWithFormat:@"%ld",(long)sheetId],kSheetId,
                               @"1",khasImage,
                               [NSString stringWithFormat:@"%d",intReminder],kImageID,
                               photoTitle,kImageName,nil];
    
    //Invoke for post
        [self postClientAssessmentImageOnServer:image andInfo:[dictInfo mutableCopy]  andWithPhotoButtonTag:photoButtonTag] ;

}


// TODO: Save assessment image in loacl
-(void)saveAssessmentImageInLocal:(UIImage*)image withTitle:(NSString*)photoTitle andWithPhotoButtonTag:(NSInteger)photoButtonTag{

    
    START_METHOD
    
    MESSAGE(@"photoButtonTag---> %ld",(long)photoButtonTag);
    
    if (photoButtonTag == 101) {
        [lblAddPhoto1 setHidden:true];
    } else if(photoButtonTag == 102) {
        [lblAddPhoto2 setHidden:true];
    } else if(photoButtonTag == 103) {
        [lblAddPhoto3 setHidden:true];
    } else if(photoButtonTag == 104) {
        [lblAddPhoto4 setHidden:true];
    } else if(photoButtonTag == 105) {
        [lblAddPhoto5 setHidden:true];
    }
    
    NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
    
    NSDictionary * selObj = [arrayImages objectAtIndex:photoButtonTag-101];
    
    int intTag = (int)photoButtonTag;
    
    int intReminder = intTag%5;
    
    if (intReminder==0) {
        
        intReminder = 5;
    }
    
    
    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientID,kClientId,
                               [NSString stringWithFormat:@"%ld",(long)sheetId],ksheetID,
                               @"1",khasImage,
                               [selObj objectForKey:kImageID],kImageID,
                               photoTitle,kImageName,nil];
    
    [[DataManager initDB] updateAssessmentImageTitlefor:dictInfo];
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder]];
    
    
    NSDictionary * lastObj = [arrayImages objectAtIndex:photoButtonTag-101];
    MESSAGE(@"lastObj---> assessment inage: %@",lastObj);
    
    
    NSString * imgNm = [NSString stringWithFormat:@"%d",intReminder];
    NSString * path = [NSString stringWithFormat:@"%@/%@/%@",[FileUtility basePath],imageFolder,imgNm];
    if ([FileUtility fileExists:path]) {
        [FileUtility deleteFile:path];
    }
    DebugLOG(path);
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [FileUtility createFileInFolder:imageFolder withName:imgNm withData:imageData];
    
    UIButton *btnPhoto = (UIButton*)[self.view viewWithTag:photoButtonTag];
    
    image = [image imageByScalingProportionallyToSize:btnPhoto.frame.size];
    [btnPhoto setImage:image forState:UIControlStateNormal];
    [btnPhoto setImage:image forState:UIControlStateSelected|UIControlStateHighlighted];
    [btnPhoto setContentMode:UIViewContentModeScaleAspectFit];
    [btnPhoto setTitle:nil  forState:UIControlStateNormal];
    
    
    [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+photoButtonTag] setHidden:FALSE];
    [(UILabel*)[self.scrollViewPhotos viewWithTag:kphotoTitleTag+photoButtonTag] setText:photoTitle];
    [(UIButton *)[self.scrollViewPhotos viewWithTag:kDeleteBtnTag +photoButtonTag] setHidden:FALSE];
    [(UIButton *)[self.scrollViewPhotos viewWithTag:kZoomBtnTag + photoButtonTag] setHidden:FALSE];
    
    
    END_METHOD
}
//---------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------


//TODO: POST CLIENT's signature on server
-(void)postClientAssessmentImageOnServer:(UIImage*)image andInfo:(NSMutableDictionary *)objDict andWithPhotoButtonTag:(NSInteger)photoButtonTag{

    START_METHOD
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    
    
    if(imageData!= nil){
    
        
        MESSAGE(@"postClientAssessmentImageOnServer 1254 -> dict: %@",objDict);

        
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN forKey:@"access_key"];
        
    //Set the image title
    [objDict setValue:[objDict objectForKey:kImageName] forKey:@"image_name"];
        
    //Set the action name
    [objDict setValue:@"assessmentImage" forKey:ACTION];
    
    
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"assessment%@_%@.jpeg",[objDict objectForKey:kClientId],[objDict objectForKey:@"nSheetID"]];
        
    }else{
        strFileName     =   @"";
    }
    MESSAGE(@"postClientAssessmentImageOnServer: %@ andimageData: %@",strFileName,imageData);
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    //Craete object of service handeler
    ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
    
    //Invoke method for post images on server
    [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                 success:^(NSDictionary *responseDataDictionary) {
                                     
                                     //Hide Indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"postClientAssessmentImageOnServer success dictionary: %@",responseDataDictionary);
                                     
                                     
                                     //Invoke method for save the assessment image
                                     [self saveAssessmentImageInLocal:image withTitle:[objDict objectForKey:@"image_name"] andWithPhotoButtonTag:photoButtonTag];
  
                                     
                                     
                                 } failure:^(NSError *error) {
                                     MESSAGE(@"Error");
                                     
                                     
                                     //Hide Indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     
                                 }];
    //Invoke method for
    
    
    }else {
      
        return;
    }
    END_METHOD

}


//TODO: POST Request for delete  Asssessmet data
-(void)postRequestForDeleteAssessment:(NSDictionary *)params{
    START_METHOD
    //Set by default all iamge set default
    [lblAddPhoto1 setHidden:FALSE];
    [lblAddPhoto2 setHidden:FALSE];
    [lblAddPhoto3 setHidden:FALSE];
    [lblAddPhoto4 setHidden:FALSE];
    [lblAddPhoto5 setHidden:FALSE];
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@deleteData/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"requestForPassword-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                    
                               
                               //Invoke method for delete ASSESSMENT SHEET detail
                               [self deleteAllAssessmentDataForClientFromServer];
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                               
                             
                               
                               
                           }];
    END_METHOD
    
}


//TODO: Delete assessment from server and local
-(void)deleteAllAssessmentDataForClientFromServer {
    MESSAGE(@"arraySheets: 1=? : %@",arraySheets);
    if([arraySheets count] == 1) {
        
        //TODO: SUNIL Add new Assessment
        [self postRequestForAddNewAssessment:@"Assessment-1"];
        
        
    }
        MESSAGE(@"arraySheets: 2=? : %@",arraySheets);
    
        NSInteger sheetId = [[[arraySheets objectAtIndex:currentSheet-1] valueForKey:@"sheetID"] integerValue];
        NSInteger deletedData = [[DataManager initDB]deleteAssessmentSheetDataForSheetId:sheetId forClientid:self.strClientID];
        
        if(deletedData == 0) {
            NSInteger imgdataDeleted =  [[DataManager initDB]deleteAssessmentImagesforSheetId:sheetId forClient:self.strClientID];
            if(imgdataDeleted == 0) {
                NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",self.strClientID,(long)sheetId];
                [FileUtility deleteFile:imageFolder];
                NSInteger t =  [[DataManager initDB]deleteAssessmentSheet:sheetId forClientid:self.strClientID];
                if(t== 0) {
                    
                    //Condition for Crash at last assessment deletion time
                    if([arraySheets count] != 1) {
                    [self resetAllTheSheetData];
                    }
                    
                    
                    DisplayAlertWithTitle(NSLocalizedString(NSLocalizedString(@"Assessment data deleted", modalController), nil), kAppName);
                }
            }
        } else {
            DisplayAlertWithTitle(NSLocalizedString(@"Unable to delete assessment sheet", nil), kAppName);
        }
        
    
}


-(void)checkFreeUserAccesForAddAssesment:(NSString *)strPopText{
    START_METHOD
    //Show Alert for Purchase Subscription
    UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(strPopText,@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
    
    
    //Set the Alert view tag
    [alertView setTag:903];
    
    //Show the alert view for confiramtion
    [alertView show];
    
    END_METHOD
}
@end
