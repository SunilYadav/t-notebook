//
//  EventNotesCustomCell.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2011 WLI All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EventNotesCustomCell : UITableViewCell {
	UILabel		* _lblDate;
	UILabel		* _lblCName;
    UILabel       *_lblDate1;
}

@property (nonatomic,strong) UILabel		* lblDate;
@property (nonatomic,strong) UILabel		* lblCName;
@property (nonatomic,strong) UILabel       * lblDate1;


- (void) displayTableData :(NSDictionary *) dictInfo;

@end