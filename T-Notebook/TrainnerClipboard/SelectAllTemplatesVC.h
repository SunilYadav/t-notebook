//
//  SelectAllTemplatesVC.h
//  T-Notebook
//
//  Created by WLI on 25/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol selectAllDelegate
-(void)selectItemAtBlockIndex:(NSInteger)blockIndex AtSheetIndex:(NSInteger)sheetIndex;
@end


@interface SelectAllTemplatesVC : UIViewController{
    
}
@property (unsafe_unretained,nonatomic) id<selectAllDelegate> delegate;
@property (nonatomic, strong) NSMutableArray				* arrayProgramSheet;
@property (nonatomic, strong) NSString				* strClientID;


@end
