//
//  ProgramCustomCell.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "ProgramCustomCell.h"

@implementation ProgramCustomCell

- (void)awakeFromNib {
   
}

//-----------------------------------------------------------------------

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}

//-----------------------------------------------------------------------

-(void)configCell :(NSDictionary *)details {
   
    START_METHOD
    NSString *lblExerciseTxt = [details valueForKey:@"sProgram"];
    NSString *lblWeightTxt =  [self getString:[details valueForKey:@"sLBValue"]];
    NSString *lblSetTxt =  [self getString:[details valueForKey:@"sSetValue"]];
    NSString *lblRepsTxt =  [self getString:[details valueForKey:@"sRepValue"]];
    NSString *lblRestTimeTxt =  [details valueForKey:@"dEXDate"];
    
    if(lblExerciseTxt == nil || [lblExerciseTxt isEqualToString:@" "] || lblExerciseTxt.length == 0) {
        lblExerciseTxt = @"NA";
    }
    
    if(lblWeightTxt == nil || [lblWeightTxt isEqualToString:@" "]|| lblWeightTxt.length == 0) {
       lblWeightTxt = @"NA";
    }
    
    if(lblSetTxt == nil || [lblSetTxt isEqualToString:@" "] || lblSetTxt.length == 0) {
        lblSetTxt = @"NA";
    }
    
    if(lblRepsTxt == nil || [lblRepsTxt isEqualToString:@" "] || lblRepsTxt.length == 0) {
        lblRepsTxt = @"NA";
    }
    
    if(lblRestTimeTxt == nil || [lblRestTimeTxt isEqualToString:@" "] || lblRestTimeTxt.length == 0) {
        lblRestTimeTxt = @"NA";
    }

    
    self.lblExercise.text = lblExerciseTxt;
    self.lblWeight.text = lblWeightTxt;
    self.lblSet.text = lblSetTxt;
    self.lblReps.text = lblRepsTxt;
    self.lblColor.text = @"";
    if ([[details valueForKey:@"dEXDate"] integerValue] != 0)
    {
        self.lblRestTime.text = [NSString stringWithFormat:@"%@",lblRestTimeTxt];
    }
    else{
        self.lblRestTime.text = lblRestTimeTxt;
    }
    
    if ([[details valueForKey:@"sColor"] intValue] == 1) {
         self.lblColor.hidden = YES;
        [self.lblColor setBackgroundColor:[UIColor grayColor]];
        [self setBackgroundColor:[UIColor colorWithRed:236.0f/255.0f green:137.0f/255.0f blue:45.0f/255.0f alpha:0.2f]];
    } else  if ([[details valueForKey:@"sColor"] intValue] == 2) {
        self.lblColor.hidden = YES;
        [self.lblColor setBackgroundColor:[UIColor yellowColor]];
        [self setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:0.0f alpha:0.2f]];
    } else  if ([[details valueForKey:@"sColor"] intValue] == 3) {
        self.lblColor.hidden = YES;
        [self.lblColor setBackgroundColor:[UIColor redColor]];
       [self setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:0.0f blue:0.0f alpha:0.2f]];
    } else  if ([[details valueForKey:@"sColor"] intValue] == 4) {
        self.lblColor.hidden = YES;
        [self.lblColor setBackgroundColor:[UIColor blueColor]];
        [self setBackgroundColor:[UIColor colorWithRed:0.0f green:0.0f blue:255.0f/255.0f alpha:0.2f]];
    } else  if ([[details valueForKey:@"sColor"] intValue] == 5) {
        self.lblColor.hidden = YES;
        [self.lblColor setBackgroundColor:[UIColor greenColor]];
       [self setBackgroundColor:[UIColor colorWithRed:0.0f green:255.0f/255.0f blue:0.0f alpha:0.2f]];
    } else  if ([[details valueForKey:@"sColor"] intValue] == 0) {
        [self.lblColor setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        self.lblColor.hidden = YES;
    }
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(NSString *)getString :(NSString *)str {
    
    START_METHOD
    MESSAGE(@"getString------->%@",str);
    NSArray *sepratestr = [str componentsSeparatedByString:@","];
    NSString *finalStr =  [sepratestr componentsJoinedByString:@"\n"];
    return finalStr;
}

//-----------------------------------------------------------------------

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


//-----------------------------------------------------------------------
-(void)setupLayoutForCell {
    
    if(self.setLandscapeMode) {
        self.btnCopy.frame = CGRectMake(0,10 ,45 , 100);
        self.btnDelete.frame = CGRectMake(883,61 ,32 , 29);
        self.btnEditLine.frame = CGRectMake(883,15 ,32 , 29);
        
        self.lblExercise.frame = CGRectMake(50,3 ,490 , 101);
        self.lblColor.frame = CGRectMake(50, self.lblExercise.frame.size.height + self.lblExercise.frame.origin.y + 2, 45, 25);
        
        self.lblReps.frame = CGRectMake(689,3 ,95 , 101);
        self.lblRestTime.frame = CGRectMake(786,3 ,82 , 101);
        self.lblSet.frame = CGRectMake(595,3,94 , 101);
        self.lblWeight.frame = CGRectMake(498,3 ,95 , 101);
        self.imgvFirstLine.frame = CGRectMake(45,0 ,1 , 135);
        self.imgvSecondLine.frame = CGRectMake(497,0 ,1 , 135);
        self.imgvThirdLine.frame = CGRectMake(594,0 ,1 , 135);
        self.imgvFourthLine.frame = CGRectMake(688,0 ,1 , 135);
        self.imgvFifthLine.frame = CGRectMake(783,0 ,1 , 135);
        self.imgvSixLine.frame = CGRectMake(868,0 ,1 , 135);
        
        
    } else {
       
        self.btnCopy.frame = CGRectMake(0,10 ,45 , 100);
        self.btnDelete.frame = CGRectMake(684,61 ,32 , 29);
        self.btnEditLine.frame = CGRectMake(683,15 ,32 , 29);
        
        self.lblExercise.frame = CGRectMake(60,3 ,248 , 101);
         self.lblColor.frame = CGRectMake(60, self.lblExercise.frame.size.height + self.lblExercise.frame.origin.y + 2, 45, 25);
        self.lblReps.frame = CGRectMake(501,3 ,95 , 101);
        self.lblRestTime.frame = CGRectMake(599,3 ,68 , 101);
        self.lblSet.frame = CGRectMake(407,3,95 , 101);
        self.lblWeight.frame = CGRectMake(311,3 ,95 , 101);
        self.imgvFirstLine.frame = CGRectMake(46,0 ,1 , 135);
        self.imgvSecondLine.frame = CGRectMake(308,0 ,1 , 135);
        self.imgvThirdLine.frame = CGRectMake(403,0 ,1 , 135);
        self.imgvFourthLine.frame = CGRectMake(499,0 ,1 , 135);
        self.imgvFifthLine.frame = CGRectMake(593,0 ,1 , 135);
        self.imgvSixLine.frame = CGRectMake(668,0 ,1 , 135);
        
    }
}

@end
