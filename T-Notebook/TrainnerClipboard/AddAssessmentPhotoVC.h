//
//  AddAssessmentPhotoVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@protocol AssessmentPhotoDelegate <NSObject>
-(void)setNewImageToAssessmentsWithImag:(UIImage*)image withTitle:(NSString*)photoTitle andWithPhotoButtonTag:(NSInteger)photoButtonTag;
@end

@interface AddAssessmentPhotoVC : UIViewController{
    id<AssessmentPhotoDelegate> __weak delegate;
}

@property (nonatomic, weak) id<AssessmentPhotoDelegate> delegate;
@property(nonatomic,assign) NSInteger photoTag;
@property(nonatomic,strong) UIImage *imageAddPhoto;
@property(nonatomic,strong) NSString *strImageTitle;
@end
