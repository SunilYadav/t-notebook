//
//  TemplateSheetNameController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReloadSheetsDelegate
-(void) reloadSheets:(int)sheetID;
@end

@interface TemplateSheetNameController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableView     * _tblTemplates;
    NSArray         * _arrTemplateNames;
    UIBarButtonItem * _btnSave;
    NSString        * _strClientId;
    NSArray         * _arrTemplateSheet; 
    NSString        * _strText1;
    NSString        * _strText2;
    NSString        * _strText3;
    
    id<ReloadSheetsDelegate>		__unsafe_unretained delegateReloadSheet;
    IBOutlet UIButton       * addBtn;
    IBOutlet UIButton       * closeBtn;

    
    int intlastSheetidOfClient;
    int blockid;
    int rowNo;
    int sheetNo;

}
@property (nonatomic, unsafe_unretained) id<ReloadSheetsDelegate>	delegateReloadSheet;
@property (nonatomic,strong)IBOutlet UITableView       * tblTemplates;
@property (nonatomic,strong)IBOutlet UIButton   * btnSave;
@property (nonatomic,strong)NSArray   * arrTemplateNames; 
@property (nonatomic,strong)NSArray   * arrTemplateSheet;
@property (nonatomic,strong)NSString  * strClientId;
@property (nonatomic,strong)NSString  * strText1;
@property (nonatomic,strong)NSString  * strText2;
@property (nonatomic,strong)NSString  * strText3;
@property (nonatomic)int  intlastSheetidOfClient;

-(IBAction)btnSave_Clicked:(id)sender;
-(IBAction)btnCancel_Clicked:(id)sender;
-(void)setLableAndTextField;
-(void)copyTemplate_SheetData;
-(void)copyTemplateSheet_BlocksAndData;

@end
