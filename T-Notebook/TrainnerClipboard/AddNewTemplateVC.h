//
//  AddNewTemplateControllers.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2012 WLI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddNewTemplateDelegate
-(void) addNewTemplate:(NSString *)Template :(NSInteger)sheetContained;
-(void) closePopOver;
@end

@interface AddNewTemplateVC : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    __weak id<AddNewTemplateDelegate>delegateTemplate;
}

@property (nonatomic , strong)         NSString     *   strTemplateName;
@property (nonatomic, weak) id<AddNewTemplateDelegate>	delegateTemplate;


@end
