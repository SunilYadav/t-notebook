//
//  PurchaseVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : In app purchase functionality


#import "PurchaseVC.h"
#import "ViewController.h"
#import "IAPHelper.h"

static  PurchaseVC       *purchaseObj;

@interface PurchaseVC (){
    NSArray *arrayProducts;
}

@property (nonatomic , weak) IBOutlet       UIButton             *btnPurchase;
@property (weak, nonatomic) IBOutlet UILabel *lblTraining;
@property (weak, nonatomic) IBOutlet UILabel *lblNotebook;

@end

@implementation PurchaseVC

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Memory Management Method

//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Custom Methods

//---------------------------------------------------------------------------------------------------------------

-(void)buyProduct {

    [[AppDelegate sharedInstance] showProgressAlert:NSLocalizedString(@"Purchase in progress", nil)];
    [[CStoreKit sharedInstance]buy:kFullAccessProductId delegate:(id)self];
    
}

//-----------------------------------------------------------------------

- (void) localizedControl {

    UIFontDescriptor * fontD = [self.lblTraining.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold
                                ];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"en"] || [language isEqualToString:@"zh-Hans"] || [language isEqualToString:@"nb"]) {
        [self.lblTraining setFont:[UIFont fontWithDescriptor:fontD size:24.0]];
        [self.lblNotebook setFont:[UIFont fontWithName:@"Proxima Nova Alt" size:24.0]];
    }
    
    self.lblTraining.text = NSLocalizedString(self.lblTraining.text, nil);
    self.lblNotebook.text = NSLocalizedString(self.lblNotebook.text, nil);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblTraining.text font:self.lblTraining.font height:self.lblTraining.frame.size.height];
    self.lblTraining.frame = CGRectMake(212, 0,rect.size.width + 5 ,51);
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblNotebook.text font:self.lblNotebook.font height:self.lblNotebook.frame.size.height];
    self.lblNotebook.frame = CGRectMake(self.lblTraining.frame.origin.x + self.lblTraining.frame.size.width , 0,rect.size.width + 5 ,51);
    
    [self.btnPurchase setTitle:NSLocalizedString(self.btnPurchase.titleLabel.text, nil) forState:0];

}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Action Methods 

//---------------------------------------------------------------------------------------------------------------

-(IBAction)buyNowBtnTapped:(id)sender {
    
   
    [self buyProduct];
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : CsStore Delegate Methods 

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[NSNotificationCenter defaultCenter] postNotificationName:kupdateVersionNotification object:nil];

    
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseFailed:(NSString*)productId error:(NSString *)error {
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction failed, Please try again", nil)];
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseCancelled:(NSString*)productId {
    
      [[AppDelegate sharedInstance] hideProgressAlert];
     [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction Cancelled, Please try again", nil)];
  
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)verificationFailed:(NSString*)productId {
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)verificationRetryOk:(NSString*)productId {
    
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : View LifeCycle methods 

//---------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    purchaseObj = self;
    [self localizedControl];
    // Do any additional setup after loading the view.
}


//---------------------------------------------------------------------------------------------------------------


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
