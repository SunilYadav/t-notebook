//
//  AddAssessmentPhotoVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Add popover for add photo in assesment sheet 


#import "AddAssessmentPhotoVC.h"
#import "UIImage-Extensions.h"
@interface AddAssessmentPhotoVC(){
    
    __weak IBOutlet UIImageView *imageViewAddPhoto;
    __weak IBOutlet UITextField* txtImageTitle;
    __weak IBOutlet UILabel *lblImageTitle;
    BOOL hasChangedImage;
    BOOL hasChangedText;
}
@property(nonatomic,strong) UIPopoverController		* popoverController;
@property(nonatomic,strong) UIImagePickerController * imagePicker;
@property(nonatomic,strong) UIActionSheet * actionSheet;
@end

@implementation AddAssessmentPhotoVC
@synthesize popoverController;
@synthesize delegate;

//-----------------------------------------------------------------------

#pragma mark - TextField Delegate

//-----------------------------------------------------------------------
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    hasChangedText = true;
    return true;
}

//-----------------------------------------------------------------------

#pragma mark - Action Methods

//-----------------------------------------------------------------------


-(void)btnDoneClicked{
    START_METHOD
    
    self.popoverController = nil;
    if (hasChangedImage) {
        
        [self.delegate setNewImageToAssessmentsWithImag:imageViewAddPhoto.image withTitle:txtImageTitle.text andWithPhotoButtonTag:self.photoTag];
    }
    else if (hasChangedText && self.imageAddPhoto){
        
        [self.delegate setNewImageToAssessmentsWithImag:self.imageAddPhoto withTitle:txtImageTitle.text andWithPhotoButtonTag:self.photoTag];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
    END_METHOD
}

//-----------------------------------------------------------------------

- (void) btnAddPhotoClicked:(id) sender {
    
    [txtImageTitle resignFirstResponder];
    if (self.actionSheet == nil) {
        self.actionSheet  = [[UIActionSheet alloc] init];
        [self.actionSheet  setDelegate:(id)self];
        [self.actionSheet  addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
        [self.actionSheet  addButtonWithTitle:NSLocalizedString(@"Photo Gallery", nil)];
        [self.actionSheet  addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
        [self.actionSheet  setCancelButtonIndex:2];
        
        if(self.imageAddPhoto != nil) {
            [self.actionSheet  setTitle:NSLocalizedString(@"Change Photo", nil)];
        } else {
            [self.actionSheet  setTitle:NSLocalizedString(@"Select Photo", nil)];
        }
    }
    [self.actionSheet  showInView:self.view];
}

//-----------------------------------------------------------------------

#pragma mark- Custom Methods

//-----------------------------------------------------------------------

- (void) openCamera {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker = nil;
        if (self.imagePicker == nil) {
            self.imagePicker = [[UIImagePickerController alloc] init];
            [self.imagePicker setDelegate:(id)self];
        }
        
        [self.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self.imagePicker setAllowsEditing:TRUE];
        [self performSelector: @selector(presentViewController) withObject: nil afterDelay: 0];
        
    }
    else {
        DisplayAlertWithTitle(NSLocalizedString(@"This device do not support camera", nil),kAppName);
    }
}

//-----------------------------------------------------------------------'

- (void) openGallery {
    
    self.popoverController = nil;
    self.imagePicker = nil;
    if (self.imagePicker == nil) {
        self.imagePicker = [[UIImagePickerController alloc] init];
        [self.imagePicker setDelegate:(id)self];
    }
    self.imagePicker.sourceType =   UIImagePickerControllerSourceTypePhotoLibrary;
    [self.imagePicker setAllowsEditing:TRUE];
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
    self.popoverController = popover;
    [self performSelector: @selector(presentModelController) withObject: nil afterDelay: 0];
    
}

//-----------------------------------------------------------------------

-(void)presentModelController{
    
    CGRect rect = CGRectMake(40, 40, 600, 600);
    [popoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:0 animated:YES];
}

//-----------------------------------------------------------------------

-(void)presentViewController{
    [self presentViewController:self.imagePicker animated:YES completion:^{
    }];
    
}

//-----------------------------------------------------------------------

#pragma mark-UIActionsheet delegate methods

//-----------------------------------------------------------------------

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:{
            [self openCamera];
            break;
        }
        case 1:{
            [self openGallery];
            break;
        }
    }
}

//-----------------------------------------------------------------------

#pragma mark - Image Picker Delegate Methods

//-----------------------------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    START_METHOD
    imageViewAddPhoto.image = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImage *orgImage = info[UIImagePickerControllerEditedImage];
        hasChangedImage = true;
        imageViewAddPhoto.image = nil;
        
        CGSize sizeforImg = CGSizeMake(492,504);
        orgImage = [orgImage imageByScalingProportionallyToSize:sizeforImg];
        [imageViewAddPhoto setImage:orgImage];
        
        [picker dismissViewControllerAnimated:true completion:nil];
        [self.popoverController dismissPopoverAnimated:YES];
        
        if ([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(orgImage, nil, nil, nil);
        }
    });
    END_METHOD
    
    
}


//-----------------------------------------------------------------------

#pragma mark - ViewLife Cycle Method

//-----------------------------------------------------------------------

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIButton * btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    btnDone.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
    btnDone.backgroundColor = kAppTintColor;
    btnDone.layer.cornerRadius = 4.0;
    [btnDone addTarget:self action:@selector(btnDoneClicked) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:btnDone.titleLabel.text font:btnDone.titleLabel.font height:btnDone.titleLabel.frame.size.height];
    
    [btnDone setFrame:CGRectMake(0, 3, rect.size.width + 10,38)];
    UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc] initWithCustomView:btnDone];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
    
    
    UIButton * btnAddPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAddPhoto.frame = CGRectMake(0, 0, 300, 30);
    btnAddPhoto.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
    [btnAddPhoto setTitle:NSLocalizedString(@"Add Photo", nil) forState:UIControlStateNormal];
    btnAddPhoto.backgroundColor = kAppTintColor;
    btnAddPhoto.layer.cornerRadius = 4.0;
    [btnAddPhoto addTarget:self action:@selector(btnAddPhotoClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    rect = [[AppDelegate sharedInstance] getWidth:btnAddPhoto.titleLabel.text font:btnAddPhoto.titleLabel.font height:btnAddPhoto.titleLabel.frame.size.height];
    
    [btnAddPhoto setFrame:CGRectMake(0, 3, rect.size.width + 10,38)];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:btnAddPhoto]];
    hasChangedImage = NO;
    
    if (self.imageAddPhoto != nil) {
        CGSize sizeforImg = CGSizeMake(492,504);
        self.imageAddPhoto = [ self.imageAddPhoto imageByScalingProportionallyToSize:sizeforImg];
        imageViewAddPhoto.image = self.imageAddPhoto;
    }
}

//-----------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated{
    START_METHOD
    [super viewWillAppear:animated];
    
    MESSAGE(@"lblImageTitle: %@",lblImageTitle.text);
    
    
    lblImageTitle.text = NSLocalizedString(lblImageTitle.text, nil);
    
    MESSAGE(@"lblImageTitle11: %@",lblImageTitle.text);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblImageTitle.text font:lblImageTitle.font height:lblImageTitle.frame.size.height];
    
    [lblImageTitle setFrame:CGRectMake(lblImageTitle.frame.origin.x, lblImageTitle.frame.origin.y, rect.size.width + 10, lblImageTitle.frame.size.height)];
    
    txtImageTitle.placeholder= NSLocalizedString(txtImageTitle.placeholder, nil);
    
    [txtImageTitle setFrame:CGRectMake(lblImageTitle.frame.origin.x + lblImageTitle.frame.size.width + 10 , txtImageTitle.frame.origin.y, 320, txtImageTitle.frame.size.height)];
    
    if (self.strImageTitle.length >0 && ![self.strImageTitle isEqual:(id)[NSNull null]]) {
        txtImageTitle.text = self.strImageTitle;
    }
    
}
@end
