//
//  SheetNameController.m
//  T-Notebook
//
//  Created by WLI on 23/06/12.
//  Copyright (c) 2012 WLI. All rights reserved.
//
// Purpose : Show popover for add template sheet

//For make json file and upload ti server
#import "TextFileManager.h"

#import "AddTemplateSheet.h"
#import "KeyboardControls.h"

@interface AddTemplateSheet(){
    
    __weak IBOutlet UIButton *btnDone;
    __weak IBOutlet UILabel *lblSheet1;
    __weak IBOutlet UILabel *lblSheet2;
    __weak IBOutlet UILabel *lblSheet3;
    __weak IBOutlet UITextField *txtSheet1;
    __weak IBOutlet UITextField *txtSheet2;
    __weak IBOutlet UITextField *txtSheet3;
    __weak IBOutlet UITextField *txtWorkOutName;
    __weak IBOutlet UILabel *lblWorkoutName;
    BOOL                    _boolAddedSheetData;
}
@property (nonatomic, strong) KeyboardControls      *keyboardControls;
@end


@implementation AddTemplateSheet

@synthesize  intsheetcount; 


@synthesize  delegateSheetName;
//----------------------------------------------------
- (IBAction)btnDoneClicked:(id)sender {

    
    START_METHOD
    int intTemp = 0;
    int intSheetname = 0;
    
    if(intsheetcount == 3){
        
        
        if([txtSheet1.text isEqualToString:txtSheet2.text] || [txtSheet2.text isEqualToString:txtSheet3.text] || [txtSheet3.text isEqualToString:txtSheet1.text]){
            intSheetname = 1;
        }
    }else if(intsheetcount == 2){
        
        if([txtSheet1.text isEqualToString:txtSheet2.text]){
            intSheetname = 1;
        }
    }
    if (intsheetcount == 1) {
        if (txtSheet1.text.length==0) {
            intTemp = 1;
        }
    }
    else if (intsheetcount ==2) {
        if (txtSheet1.text.length==0 ||txtSheet2.text.length==0) {
            intTemp = 2;
        }
    }
    else if (intsheetcount == 3) {
        if (txtSheet1.text.length==0||txtSheet2.text.length==0 || txtSheet1.text.length==0) {
            intTemp = 2;
        }
    }
    else if (txtWorkOutName.text.length == 0) {
        intTemp = 4;
    }

    if(intTemp == 0){
        
        if(intSheetname == 0){
            [self insertTemplateData];
            
            
            //Get Upgarde Status
            NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            if (![strUpgradeStatus isEqualToString:@"YES"]) {
            
            [self performSelector:@selector(saveDataAndDismiss:) withObject:nil afterDelay:0.7];
            }
            
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
            }];
        }else{
            DisplayAlertWithTitle(NSLocalizedString(@"Please Enter Unique Name For Each Sheet", nil),kAppName);
        }
        
    }else if(intTemp == 1){
        DisplayAlertWithTitle(NSLocalizedString(@"Please Enter Sheet Name", nil),kAppName);
    }
    else if(intTemp == 4){
        DisplayAlertWithTitle(NSLocalizedString(@"Please Enter Workout Name", nil), kAppName);
    }else{
        DisplayAlertWithTitle(NSLocalizedString(@"Please Enter Sheets Name", ni),kAppName);
    }

    
}

//-----------------------------------------------------------------------

-(void)saveDataAndDismiss:(id)sender {
    
    START_METHOD
    [delegateSheetName callSetArrayMethod:self.intCurrentSheetId :self.intTemplateid :self.intsheetcount :sender];
}

//-----------------------------------------------------------------------

- (IBAction)btnCancelClicked:(id)sender {
    [delegateSheetName didCancelSheetEntry];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
                   }];
}

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning{
  
    [super didReceiveMemoryWarning];
}

//-----------------------------------------------------------------------
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.keyboardControls setActiveField:textField];
    return YES;
}
//-----------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

//-----------------------------------------------------------------------

#pragma mark - Custom Method

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    
    lblSheet1.text = NSLocalizedString(lblSheet1.text, nil);
    lblSheet2.text = NSLocalizedString(lblSheet2.text, nil);
    lblSheet3.text = NSLocalizedString(lblSheet3.text, nil);
    lblWorkoutName.text = NSLocalizedString(lblWorkoutName.text, nil);
    
//    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(lblSheet1.text, nil) font:lblSheet1.font height:21];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblSheet1.text font:lblSheet1.font height:21];
    CGRect rect1 = [[AppDelegate sharedInstance] getWidth:lblSheet2.text font:lblSheet2.font height:21];
    CGRect rect2 = [[AppDelegate sharedInstance] getWidth:lblSheet3.text font:lblSheet3.font height:21];
    CGRect rect3 = [[AppDelegate sharedInstance] getWidth:lblWorkoutName.text font:lblWorkoutName.font height:21];
    
    
    CGFloat width = rect.size.width;
    if (rect1.size.width > width) {
        width = rect1.size.width;
    }
    if (rect2.size.width > width) {
        width = rect2.size.width;
    }
    if (rect3.size.width > width) {
        width = rect3.size.width;
    }
    
    if(width > 135) {
        [lblSheet1 setFrame:CGRectMake(lblSheet1.frame.origin.x - 25, lblSheet1.frame.origin.y, width + 5, lblSheet1.frame.size.height)];
        [lblSheet2 setFrame:CGRectMake(lblSheet2.frame.origin.x - 25, lblSheet2.frame.origin.y, width + 5, lblSheet2.frame.size.height)];
        [lblSheet3 setFrame:CGRectMake(lblSheet3.frame.origin.x - 25, lblSheet3.frame.origin.y, width + 5, lblSheet3.frame.size.height)];
        [lblWorkoutName setFrame:CGRectMake(lblWorkoutName.frame.origin.x - 25, lblWorkoutName.frame.origin.y, width + 5, lblWorkoutName.frame.size.height)];
    }
    
    NSString *str = btnDone.titleLabel.text;
}


//----------------------------------------------------
#pragma mark -
#pragma mark - View lifecycle
//----------------------------------------------------
-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self localizeControls];
    UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
    lblTitle.text = NSLocalizedString(@"Add Program",nil);
    lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
    lblTitle.textColor = kAppTintColor;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.adjustsFontSizeToFitWidth=YES;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=lblTitle;
    
    
    btnDone.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnDone.layer.cornerRadius = 4.0;
    btnDone.backgroundColor = kAppTintColor;
    
    
    
    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(btnCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -15;
    UIBarButtonItem *leftBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, leftBtn, nil] animated:NO];
    
    
    txtSheet1.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    txtSheet2.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    txtSheet3.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    txtWorkOutName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    txtSheet1.delegate  =(id)self;
    txtSheet2.delegate  =(id)self;
    txtSheet3.delegate  =(id)self;
    txtWorkOutName.delegate =(id)self;
    
   NSArray *fields;
    
    if (self.intsheetcount == 1) {
        txtSheet2.hidden = true;
        lblSheet2.hidden = true;
        txtSheet3.hidden = true;
        lblSheet3.hidden = true;
        if (iOS_7) {
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 150+44+46);
            self.navigationController.view.superview.center = self.view.center;
        }
        btnDone.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        CGRect btnAddFrame = self.btnAdd.frame;
        btnAddFrame.origin.y = btnAddFrame.origin.y - 100;
        self.btnAdd.frame = btnAddFrame;
        
        CGRect txtWorkoutFrame = txtWorkOutName.frame;
        txtWorkoutFrame.origin.y = txtWorkoutFrame.origin.y - 100;
        txtWorkOutName.frame = txtWorkoutFrame;
        
        CGRect lblWorkoutmname  = lblWorkoutName.frame;
        lblWorkoutmname.origin.y = lblWorkoutmname.origin.y - 100;
        lblWorkoutName.frame = lblWorkoutmname;
        
        fields = @[txtSheet1,txtWorkOutName];
    }
    else if (self.intsheetcount == 2){
            txtSheet3.hidden = true;
            lblSheet3.hidden = true;
        if (iOS_7) {
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 200+98);
            self.navigationController.view.superview.center = self.view.center;
        }
        
        CGRect btnAddFrame = self.btnAdd.frame;
        btnAddFrame.origin.y = btnAddFrame.origin.y -50;
        self.btnAdd.frame = btnAddFrame;
        
        CGRect txtWorkoutFrame = txtWorkOutName.frame;
        txtWorkoutFrame.origin.y = txtWorkoutFrame.origin.y - 60;
        txtWorkOutName.frame = txtWorkoutFrame;
        
        CGRect lblWorkoutmname  = lblWorkoutName.frame;
        lblWorkoutmname.origin.y = lblWorkoutmname.origin.y - 60;
        lblWorkoutName.frame = lblWorkoutmname;

        
        
        btnDone.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        fields = @[txtSheet1,txtSheet2,txtWorkOutName];
    }
    else{
        fields = @[txtSheet1,txtSheet2,txtSheet3,txtWorkOutName];
        if (iOS_7) {
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 270+80);
            self.navigationController.view.superview.center = self.view.center;
        }
    }

    
    NSString *str = NSLocalizedString(@"DONE",nil);
    [btnDone setTitle:str forState:UIControlStateNormal];
    
    [self setKeyboardControls:[[KeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:(id)self];
    fields = nil;
    
}


//-----------------------------------------------------------------------

#pragma mark - Keyboard Controls Delegate Methods

//-----------------------------------------------------------------------

- (void)keyboardControlsDonePressed:(KeyboardControls *)keyboardControls{
    [keyboardControls.activeField resignFirstResponder];
}
//-----------------------------------------------------------------------

- (void)keyboardControls:(KeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(KeyboardControlsDirection)direction{
    
}
//#pragma mark -
//#pragma mark - IBAction Methods.
//
-(IBAction)btnDone_Clicked:(id)sender{
    
   }

//#pragma mark -
//#pragma mark - Method for insert Template
//



-(void)insertTemplateData{
    START_METHOD
    
    NSDictionary * dictemp = [NSDictionary dictionaryWithObjectsAndKeys:self.strTemplateName,@"templateName",[NSNumber numberWithInteger:self.intsheetcount],@"sheetContained", nil];
    
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if ([strUpgradeStatus isEqualToString:@"YES"]) {

        //Invoke method for craete dictionry for Template
        NSDictionary *dictTemplateForPost= [self createDictionary];
        //TODO: Post request for ADD NEW TEMPLATE
        
        //Invoke method for adad the new template
        [self postRequestForTemplate:dictTemplateForPost];
        
        
    }else{
      
        int intReturnValue = [[DataManager initDB]addTemplate:dictemp];
        
        if(intReturnValue == 1){
            self.intTemplateid = [[DataManager initDB] getLastTemplateId];
            
            [self insertTemplateSheetData];
        }
    }
    
}
////----------------------------------------------------
-(void)insertTemplateSheetData{
    
    START_METHOD
    
    _boolAddedSheetData =   YES;
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
  
        for(int i=1;i<= self.intsheetcount ;i++){
            
            UITextField * textfield = (UITextField *)[self.view viewWithTag:i];
            
            NSDictionary * dicSheetData = [NSDictionary dictionaryWithObjectsAndKeys:textfield.text,@"sSheetName",[NSNumber numberWithInt:0],@"updateStatus",[NSNumber numberWithInteger:self.intTemplateid],@"templateid", nil];
            
            
            if ([strUpgradeStatus isEqualToString:@"YES"]) {
                
                
                //TODO: Post request for ADD NEW TEMPLATE
//                [self postRequestForTemplatePrograms:dicSheetData];
                
            }else{
                
                [[DataManager initDB] addSheetDetail:dicSheetData];
                
                dicSheetData = Nil;
                textfield = nil;
                
            }
        }
    
    if (![strUpgradeStatus isEqualToString:@"YES"]) {
        
        [self insertTemplateSheetBlockData];
    }

}
////----------------------------------------------------
-(void)insertTemplateSheetBlockData{
    
    START_METHOD
    
        NSArray * arrsheetBlocks = [NSArray arrayWithArray:[[DataManager initDB]getSheetsOfTemplate:self.intTemplateid]];
        
        NSDictionary * dicsheetData = [NSDictionary dictionaryWithDictionary:[arrsheetBlocks objectAtIndex:0]];
        
        self.intCurrentSheetId = [[dicsheetData valueForKey:@"SheetId"] intValue];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
       // [formatter setDateFormat:@"MM/dd/yyyy"];
       [formatter setDateFormat:kAppDateFormat];
       NSString *strDate = [formatter stringFromDate:[NSDate date]];
        for(int i=1;i<=arrsheetBlocks.count;i++){
            
           
            NSDictionary * dicBlockData = [NSDictionary dictionaryWithObjectsAndKeys:[[arrsheetBlocks objectAtIndex:i-1]valueForKey:@"SheetId"],@"sheetID",[NSNumber numberWithInt:1],@"blockNo",txtWorkOutName.text,@"sBlockTitle",strDate,@"dBlockDate", nil];
            
            
            //Get Upgarde Status
            NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            if ([strUpgradeStatus isEqualToString:@"YES"]) {
                
                
                //TODO: SUNIL-> Post Request for Add template workout
                
                //Invoke method for post request for add template workout
//                [self postRequestForTemplateWorkouts:dicBlockData];
                
            }else{
                
                [[DataManager initDB] insertTemplateSheet_Blocks:dicBlockData];
                
            }            
            dicBlockData = nil;
            
        }
        arrsheetBlocks = Nil;
        dicsheetData = Nil;
    
}
////----------------------------------------------------


#pragma mark - SUNIL Post Request
//TODO: ADD/ EDIT Template
//Method for post request for Add new workout
-(void)postRequestForTemplate:(NSDictionary *)dictTemplate{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictTemplate,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictTemplate
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                         
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictTemplateTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictTemplate,dictTemplateTemp);
                                   
                                   NSString *strAction =   [dictTemplateTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addTemplates"]){
                                      
                                       //Invoke method for add temoplate in local db
                                       [self saveTemplateInlocalDB:dictTemplateTemp];
                                       
                                   }
                    
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                               
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){

                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               //Hide Indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                               
                               [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
//                               [commonUtility alertMessage:@"Error Occur, Please Try Again"];
                               
                               
                           }];
    END_METHOD
}

//Save After get responce from server
-(void)saveTemplateInlocalDB:(NSDictionary *)dictemp{
    
    START_METHOD
    int intReturnValue = [[DataManager initDB]addTemplate:dictemp];
    
    if(intReturnValue == 1){
        self.intTemplateid = [[DataManager initDB] getLastTemplateId];
    }
    
    

    
    
    NSArray *arrayProgramWithWorkouts   = [dictemp objectForKey:@"program"];
    
    
    if(arrayProgramWithWorkouts && arrayProgramWithWorkouts .count>0){
        
        
        for (int i=0; i<arrayProgramWithWorkouts.count; i++) {
            
            
            //get the dictionry for the programs
            NSMutableDictionary *dictProgramsWorkouts =    [[arrayProgramWithWorkouts objectAtIndex:i] mutableCopy];
            
            //Set value for programs
            [dictProgramsWorkouts setValue:[dictProgramsWorkouts objectForKey:@"nSheetId"] forKey:ksheetID];
            [dictProgramsWorkouts setValue:[dictemp objectForKey:@"nId"] forKey:@"templateid"];
            
            //Set Workouts
            [dictProgramsWorkouts setValue:[dictProgramsWorkouts objectForKey:@"nSheetId"] forKey:@"sheetID"];
            [dictProgramsWorkouts setValue:[dictProgramsWorkouts objectForKey:@"nBlockNO"] forKey:@"blockNo"];
            [dictProgramsWorkouts setValue:[dictProgramsWorkouts objectForKey:@"nBlock_ID"] forKey:kBlockId];
            
            MESSAGE(@"for loop dictProgramsWorkouts: %@ ",dictProgramsWorkouts);

            
            //Invokem method for save the programin local db
            [[DataManager initDB] addSheetDetail:dictProgramsWorkouts];
            
            //Invoke method for save the workouts in local db
            [[DataManager initDB] insertTemplateSheet_Blocks:dictProgramsWorkouts];
            
        }
    }
 
    NSArray * arrsheetBlocks = [NSArray arrayWithArray:[[DataManager initDB]getSheetsOfTemplate:self.intTemplateid]];
    
    NSDictionary * dicsheetData = [NSDictionary dictionaryWithDictionary:[arrsheetBlocks objectAtIndex:0]];
    
    self.intCurrentSheetId = [[dicsheetData valueForKey:@"SheetId"] intValue];
    
    
        [self performSelector:@selector(saveDataAndDismiss:) withObject:nil afterDelay:0.2];
    END_METHOD
}


//Method for craete dictionry for post on server to add Template
-(NSDictionary *)createDictionary{
    
    //Dict for template
    NSMutableDictionary * dictTemplate = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.strTemplateName,@"templateName",[NSNumber numberWithInteger:self.intsheetcount],@"sheetContained", @"addTemplates",ACTION,[commonUtility retrieveValue:KEY_TRAINER_ID],USER_ID,  nil];
    
    
    //get DAte
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // [formatter setDateFormat:@"MM/dd/yyyy"];
    [formatter setDateFormat:kAppDateFormat];
    NSString *strDate = [formatter stringFromDate:[NSDate date]];
    
    //get all Program in current Template
    NSMutableArray *arrayProgramWithWorkouts    =   [[NSMutableArray alloc]init];
                                         
    for(int i=1;i<= self.intsheetcount ;i++){
        
        UITextField * textfield = (UITextField *)[self.view viewWithTag:i];
    
        //Dictionry with Program and its workouts
        NSMutableDictionary * dictTemplatePrograms  = [NSMutableDictionary dictionaryWithObjectsAndKeys:textfield.text,@"sSheetName", txtWorkOutName.text,@"sBlockTitle",strDate,@"dBlockDate", nil];
        

        //Add programs and  workouts in Array
        [arrayProgramWithWorkouts addObject: dictTemplatePrograms];
    }
    
    //Add Programs with template in Template Templates
    [dictTemplate setValue:arrayProgramWithWorkouts forKey:@"program"];
    
    MESSAGE(@"dictTemplate-----> craete: %@",dictTemplate);
    return dictTemplate;
    
}



@end
