//
//  AudioPlayer.m
//  IPH_3in1Timer
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "AudioPlayer.h"

@implementation AudioPlayer

@synthesize myaudioplayer,myaudioRecorder;
@synthesize apDelegate;
@synthesize isPlaying = _isPlaying;
@synthesize isPause = _isPause;

#pragma mark -
#pragma mark Custom methods


-(void) startPlaying {
    if (myaudioplayer) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient  withOptions:AVAudioSessionCategoryOptionMixWithOthers | AVAudioSessionCategoryOptionAllowBluetooth error:nil];
            [[AVAudioSession sharedInstance] setActive:YES error:nil];
        });
        
        [self.myaudioplayer play];
        self.isPlaying = TRUE;
    }
}


-(void) setAudioFromPathUrl:(NSURL *) audiourl {
    
    
    NSError* err;
    if (myaudioplayer) {
        myaudioplayer = nil;
    }
    myaudioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audiourl error:&err];
    [myaudioplayer setVolume:1.0];
    myaudioplayer.delegate=self;
    if( err ){
        self.isPlaying=FALSE;
        
        [self audioPlayerDidFinishPlaying:myaudioplayer successfully:NO];
    }
    else{
    }
    self.isPause = NO;
}


-(void) pausePlaying {
    if (myaudioplayer) {
        self.isPause = YES;
        [myaudioplayer pause];
        self.isPlaying=FALSE;
    }
}


-(void) stopPlaying
{
    if (myaudioplayer) {
        self.isPause = NO;
        [myaudioplayer stop];
        self.isPlaying=FALSE;
    }
    
}
-(void) recordAndSaveAudioAtPathUrl:(NSURL *) audiourl forDuration:(int) duration {
    
    recordtime = duration;
    recordEncoding = ENC_ALAC;
    myaudioRecorder = nil;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];
    
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] initWithCapacity:10];
    if(recordEncoding == ENC_PCM)
    {
        [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatLinearPCM] forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    }
    else
    {
        NSNumber *formatObject;
        
        switch (recordEncoding) {
            case (ENC_AAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
                break;
            case (ENC_ALAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleLossless];
                break;
            case (ENC_IMA4):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
                break;
            case (ENC_ILBC):
                formatObject = [NSNumber numberWithInt: kAudioFormatiLBC];
                break;
            case (ENC_ULAW):
                formatObject = [NSNumber numberWithInt: kAudioFormatULaw];
                break;
            default:
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
        }
        
        [recordSettings setObject:formatObject forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:12800] forKey:AVEncoderBitRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithInt: AVAudioQualityHigh] forKey: AVEncoderAudioQualityKey];
    }
    
    
    
    NSError *error = nil;
    myaudioRecorder = [[ AVAudioRecorder alloc] initWithURL:audiourl settings:recordSettings error:&error];
    
    if ([myaudioRecorder prepareToRecord] == YES){
        [myaudioRecorder record];
        currTime = CACurrentMediaTime();
        [self checkTime];
    }else {
        NSInteger errorCode = CFSwapInt64BigToHost([error code]);
        
    }
    
    
    
}
-(void) checkTime{
    
    double CurrentTime = CACurrentMediaTime();
    
    if (CurrentTime > currTime + recordtime) {
        
        [myaudioRecorder stop];
        
        self.myaudioRecorder = nil;
        
    }else{
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkTime) userInfo:nil repeats:NO];
    }
}

#pragma mark-
#pragma mark- AudioPlayer delegate methods
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (flag) {
        self.isPlaying=FALSE;
        [apDelegate stopPlayingAudio];
    }
    else {
        myaudioplayer = nil;
    }
}
@end
