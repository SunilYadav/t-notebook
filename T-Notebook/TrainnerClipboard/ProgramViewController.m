//
//  ProgramViewController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Show listing of all program and all workouts

#import "TextFileManager.h"

#import "ProgramViewController.h"
#import "ProgramGetWorkoutVC.h"
#import "ProgramCustomCell.h"
#import "ViewAllProgramVC.h"
#import "UpdateNotesVC.h"
#import "CStoreKit.h"


#define kcopyBtnTag          3000
#define keditBtnTag            9000


#define  kQuatcolor  [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0];
static ProgramViewController *sharedInstance = nil;
@interface ProgramViewController ()<UITableViewDelegate,UITableViewDataSource,CStoreKitDelegate>
{
    ViewAllProgramVC                        *viewAllProgOBJ;
    UIPopoverController                     *popover;
    NSString                                * strClientId;
    NSString                                * strClientName;
    NSString                                * selectedSheet;
    NSMutableArray                          *copyWorkoutArray;
     NSMutableArray                         *copyLineArray;
    PopoverController                       *PopoverViewController;
    
    NSInteger                               No_OfSheet;
    BOOL                                    isFromPasteNewLineClick;
    NSInteger                               rowValueForExercise;
    
    __weak IBOutlet UIView                  *viewheaderView;
   __weak IBOutlet UIView                   *viewTopToTakeScreenShot;
    
    BOOL isCopiedWorkOut;
    IBOutlet UIView *viewTbl;
    IBOutlet UILabel *lblDate;
    IBOutlet UIButton *btnNext;
    IBOutlet UILabel *lblWorkout;
    __weak IBOutlet UIButton *btnPrevious;
    
   
    __weak IBOutlet UILabel *lblPrevWorkout;
    IBOutlet UILabel *lblCopy;
    IBOutlet UILabel *lblExercise;
    IBOutlet UILabel *lblWeight;
    IBOutlet UILabel *lblSets;
    IBOutlet UILabel *lblReps;
    IBOutlet UILabel *lblRestTime;
    IBOutlet UILabel *lblDelete;
    IBOutlet UIImageView *imgvFirstLine;
    IBOutlet UIImageView *imgvSecondLine;
    IBOutlet UIImageView *imgvThirdLine;
    IBOutlet UIImageView *imgvFourthLine;
    IBOutlet UIImageView *imgvFifthLine;
    IBOutlet UIImageView *imgvSixthLine;
    IBOutlet UIView *mainView;
    

}

@property (weak, nonatomic) IBOutlet UIView                    *headerLabelview;
@property (weak, nonatomic) IBOutlet UIButton                  *btnDropDown;
@property (weak, nonatomic) IBOutlet UIButton                  *btnNotes;
@property (weak, nonatomic) IBOutlet UIView                   *dropdownview;
@property (weak, nonatomic) IBOutlet UIButton                 *btnBack;
@property (weak, nonatomic) IBOutlet UIView                   *prevNextView;
@property (weak, nonatomic) IBOutlet UIButton                 *btnViewAll;
@property (weak, nonatomic) IBOutlet UILabel                  *lblInstruction;
@property (weak, nonatomic) IBOutlet UIButton                *btnAddProgram;
@property (weak, nonatomic) IBOutlet UILabel                 *lblProgramName;
@property (weak, nonatomic) IBOutlet UIButton               *btnMail;
@property (weak, nonatomic) IBOutlet UIButton              *btnPhone;
@property (weak, nonatomic) IBOutlet UIImageView      *profileImage;
@property (weak, nonatomic) IBOutlet UILabel              *lblFirstName;
@property (weak, nonatomic) IBOutlet UILabel              *lblProgramPurpose;
@property (weak, nonatomic) IBOutlet UILabel              *lblWorkoutName;

@property (weak, nonatomic) IBOutlet UILabel             *lblMail;
@property (weak, nonatomic) IBOutlet UILabel            *lblPhone;
@property (weak, nonatomic) IBOutlet UIButton          *btnDate;
@property (weak, nonatomic) IBOutlet UIButton          *btnEditProgram;
@property (weak, nonatomic) IBOutlet UIButton         *btnEditWorkout;


@property (nonatomic, strong) NSMutableArray		      *arrSheetsList;
@property (nonatomic, strong) NSString				      * strSheetName;
@property (nonatomic, strong) NSMutableArray		      * arrayProgramData;
@property (nonatomic, strong) NSArray                     * arrayBlockDates;
@property (nonatomic,strong) DatePickerController		  * datePicker;
@property (nonatomic,strong) UIPopoverController		  * pop;
@property (nonatomic , strong)   OptionsViewVC            *optionsVCOBJ;
@property int noOfWorkout;
@property int buttonHeight;
@property BOOL isDropDownShow;

@property (nonatomic) NSInteger         totalBlocks;
@property (nonatomic) BOOL   isFromPreviousNext;


- (IBAction)mailBtnAction:(id)sender;
- (IBAction)btnDateAction:(id)sender;
- (IBAction)btnEditProgramAction:(id)sender;
- (IBAction)btnEditWorkoutAction:(id)sender;

@end

@implementation ProgramViewController

@synthesize delegate;
@synthesize currentSheet;
@synthesize currentBlockNo;
@synthesize No_OfSheet,strClientId,selectedSheet,strSelectedSheet,strClientName;

static UIPopoverController *popoverController = nil;
static UIDatePicker *datePicker = nil;
int countForOption1 = 0;


#pragma mark - Shared Method
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
+ (ProgramViewController*)sharedInstance
{

    static dispatch_once_t once;
    static ProgramViewController *program ;
    dispatch_once(&once, ^{
      program = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"program"];
    });
    return program;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - custome option delegate Methods

-(void)selectedOptionsTask:(NSInteger)selTask forEvent:(NSString*)evnt
{
    START_METHOD
    MESSAGE(@"strtask and event: %ld and envt: %@",(long)selTask,evnt);
    if (selTask == 11)
    {
         // Email Snapshot
    }
    else if (selTask == 12)
    {
        if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
            DisplayAlertWithYesNo(kliteMsg, kAppName, self);
            return;
        }
        [self docopyBtnClicked];
        
    }
    else if (selTask == 13)
    {
       //Clear Clipboard
        [self clearClipBoardBtnClciked];
    }
    else if (selTask == 14)
    {
        [self btnFrom_TemplateClicked];
        //Paste from template
    }
    else if (selTask == 15)
    {

        MESSAGE(@" else if (selTask == 15)");
        
        ProgramNameViewController *ProgramNameVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getProgram"];
        UINavigationController* programNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramNameVCOBJ];
        [programNavigationController.navigationBar setTranslucent:NO];
        programNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        programNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        
        ProgramNameVCOBJ.strSheetName =  [[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:ksheetName];
        ProgramNameVCOBJ.delegate = self;
        if (iOS_8) {
            CGPoint frameSize = CGPointZero;
            if (programNavigationController == nil) {
                frameSize = CGPointMake(500, 250);
            }
            else{
                frameSize = CGPointMake(500, 170);
            }
            programNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        [self.view.window.rootViewController presentViewController:programNavigationController animated:YES completion:nil];
    }
    else if (selTask == 16)
    {
    }
    else if (selTask == 10)
    {
        MESSAGE(@"prom add block");
        // Add Program
        [self AddProgramTap:nil];
    } 
    else if (selTask == 17)
    {
         [self copyNewLines];
        // copy new line
      
    }
    else if (selTask == 20)
    {
        if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
            DisplayAlertWithYesNo(kliteMsg, kAppName, self);
            return;
        }
        
        
        [self doCopyWorkOut];

        
    }
    else if (selTask == 19)
    {
        // Edit Workout
        ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
        UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
        ProgramGetWorkoutVCOBJ.strSheetName = [[self.arrayBlockDates objectAtIndex:_blockSheetNo-1] valueForKey:@"sBlockTitle"];
        ProgramGetWorkoutVCOBJ.delegate = self;
        [getWorkoutNavigationController.navigationBar setTranslucent:NO];
        getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        if (iOS_8) {
            CGPoint frameSize = CGPointMake(500, 116);
            getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
        
    }
  
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark DatePickerDelegate methods

-(void) getDateFormPicker:(NSString *)date1 :(int)tag1 {
    self.btnDate.titleLabel.text =date1;
}

- (void) doneWithDate {
    
    _blockSheetNo = [[[self.arrayBlockDates objectAtIndex:self.currentBlockNo]valueForKey:@"nBlockNo"]intValue];
    NSInteger success = [[DataManager initDB] updateDate:self.btnDate.titleLabel.text blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
    
    [self.pop dismissPopoverAnimated:YES];
    [_datePicker setDelegate:nil];
    _datePicker = nil;
}

-(void)closeDatePopover {
    [self.pop dismissPopoverAnimated:NO];
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Alert View Delegate Methods

//---------------------------------------------------------------------------------------------------------------

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    START_METHOD
    int t = (int)alertView.tag;
    
    if (buttonIndex == 0 && t == 1001) {

        MESSAGE(@"Delete block-->");
        
//        [commonUtility alertMessage:@"Under Development!"];
//        return;
        //Get User ID
        NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            

            //Create dictionaryb for send param in api for Trainer's Profile
            NSDictionary *params = @{
                                     kClientId              :   self.strClientId,
                                     ACTION                 :   @"deleteExercise",
                                     @"program_exercise_id"               :[NSString stringWithFormat:@"%ld",(long)selectedBtn.tag]
                                     };
            
            
            //Invoke Method for Post Request To delete Exercise
            [self postRequestForDeleteExercise: params];
            
        }else{
            
            //Invoke for delete from Local
            [self deleteExerciseFromLocal];
        }
        
    }
    
    else if (buttonIndex == 0 && t == 5000) {
        [[AppDelegate sharedInstance] showProgressAlert:NSLocalizedString(@"Purchase in progress", nil)];
        [[CStoreKit sharedInstance]buy:kFullAccessProductId delegate:(id)self];
    }
    
    //For PUrchase a new subcription plan
    if (t == 903) {
        
        if(buttonIndex==0){
            
            //Open the web app of T-notebook for purchase new subcription
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://52.26.135.139/index.php/welcome/"]];
            
            return;
            
            
        }
    }
    
 
}

#pragma mark
#pragma mark  MFMailComposer Delegate Method

//---------------------------------------------------------------------------------------------------------------
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultSent:
            DisplayLocalizedAlertWithTitle(NSLocalizedString(@"EmailSent", nil), kAppName);
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultSaved:
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultCancelled:
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        case MFMailComposeResultFailed:
            DisplayLocalizedAlertWithTitle(NSLocalizedString(@"EmailSendError", nil), kAppName);
            break;
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark ReloadSheetsDelegate methods

-(void)reloadSheets:(int)sheetID{
    [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
    
    self.isFromPreviousNext = FALSE;
    [self fillArrayWithData];
    [self setInitialLayout];
    [self.tableview reloadData];
    
}

//-----------------------------------------------------------------------

#pragma mark - EditNotes Delegate Methods

//-----------------------------------------------------------------------

-(void)updatedNote:(NSString *)updateString
{
    MESSAGE(@"ADD new Notes-> %@",updateString);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    NSString *strDate = [formatter stringFromDate:[NSDate date]];
    
    //Check condition for empty string
    updateString = [updateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    // SUNIL
    NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];
    
    if(updateString.length>0){
        
        //Get Upgarde Status
         
        //Get User ID
        NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            
            //TODO: POST REQUEST FOR ADDD NOTES
            
            //Craete dict
            NSDictionary *dictNote =@{
                                      kBlockNotes : updateString,
                                      ksheetID:sId,
                                      PROGRAM_ID:sId,
                                      kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                                      kClientId :self.strClientId,
                                      ACTION     :@"addNotes",
                                      kDate     :strDate,
                                      };
            
            //Invoke method for post request for Add notes
            [self postRequestForNotes:[dictNote mutableCopy]];
            
            
        }else{
            
          
            
            [[DataManager initDB]insertSheetBlocksForSheet:sId date:strDate blockNo:[NSString stringWithFormat:@"%ld", (long)_blockSheetNo] clientId:self.strClientId blockTitle:@"" blockNotes:@"" andBlockId:@""];
            
            
            NSInteger success = [[DataManager initDB] updateNotes:updateString blockTitle:sId ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId andexistingNotes:@""];
            
          
            
            //NSInteger success = [[DataManager initDB] updateNotes:updateString blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
            
            
            [self fillArrayWithData];
        }
        
        
    }else{
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text", nil), kAppName);

    }
    
}


#pragma mark - Add Program Delegate Methods

//-----------------------------------------------------------------------

-(void) addNewProgram:(NSString *)ProgramName : (NSString *)ProgramPurpose : (NSString *)workoutName
{
    
    
    //Get Upgarde Status
     
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
        
        
        
        //Crete Dictionary for Program
        NSMutableDictionary *dictProgram = [[NSMutableDictionary alloc]init];
        
        [dictProgram setValue:ProgramName forKey:ksheetName];
        [dictProgram setValue:self.strClientId forKey:kClientId];
        [dictProgram setValue:ProgramPurpose forKey:ksProgramPurpose];
        [dictProgram setValue:ProgramPurpose forKey:@"goal_name"];
        [dictProgram setValue:workoutName forKey:kBlockTitle];
        [dictProgram setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
        
        if ([ProgramPurpose isEqualToString:@"isForDelete"])
        {
            if ([ProgramName length]!=0) {
                
                [dictProgram setValue:@"updateProgram" forKey:ACTION];
                
                [dictProgram setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:kSheetId];
                
                //TODO: SUNIL-> UPDATE PROGRAM
                MESSAGE(@"TODO: SUNIL-> UPDATE PROGRAM");
                
                //Invoke method for post add program
                [self postRequestForProgram:dictProgram];
                
                
            }
        }
        else
        {
            
           
            rowValueForExercise = 0;
            if ([ProgramName length]!=0) {
                
                [dictProgram setValue:@"addProgram" forKey:ACTION];
                
                
                //Invoke method for Add program on server
                [self postRequestForProgram:dictProgram];
                
            }
            else {
                DisplayAlertWithTitle(NSLocalizedString(@"Please enter Sheet name.", nil),kAppName);
            }
        }

        
    }else{
        
        if ([ProgramPurpose isEqualToString:@"isForDelete"])
        {
            if ([ProgramName length]!=0) {
                
                
                NSInteger  success = [[DataManager initDB] updateSheetName:ProgramName forClient:self.strClientId sheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                
                
                [self.lblProgramName setText:ProgramName];
                [self addUnderlineInprogramName];
                
                [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
                if (success==0) {
                    DisplayAlertWithTitle(NSLocalizedString(@"Sheet name updated successfully.", nil),kAppName);
                }
            }
        }
        else
        {
            
            _blockSheetNo = 1;
            rowValueForExercise = 0;
            if ([ProgramName length]!=0) {
                
                int i = [[DataManager initDB] addNewSheet:ProgramName ForClient:self.strClientId ProgramPurpose:ProgramPurpose withWorkoutName: workoutName ForBlockNo:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]];
                
                self.strProgramSheetName = ProgramName;
                [self.delegate removePopOverForAddProgram];
                
                
                if (i == 0) {
                    NSString * sheetId = [[DataManager initDB] getsheetId:ProgramName forClient:self.strClientId];
                    DebugSTRLOG(@"SheetId:",sheetId);
                    
                    
                    
                    self.currentSheet = [sheetId intValue];
                    [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
                    self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
                    self.currentSheet = [self.arrSheetsList count];
                    DebugSTRLOG(@"self.arrayProgramData",self.arrayProgramData);
                    No_OfSheet = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
                    
                    self.isFromPreviousNext = FALSE;
                    
                    [self fillArrayWithData];
                    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:_blockSheetNo-1] valueForKey:@"sBlockTitle"]];
                    [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sPurpose"]];
                    [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetName"]];
                    [self addUnderlineInprogramName];
                }
                
                [self.tableview reloadData];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                if ([self.arrayProgramData count]==0)
                {
                    self.lblInstruction.hidden = NO;
                }
                else
                {
                    self.lblInstruction.hidden = YES;
                    
                }
            }
            else {
                DisplayAlertWithTitle(NSLocalizedString(@"Please enter Sheet name.", nil),kAppName);
            }
        }
    }

}
//-----------------------------------------------------------------------

#pragma mark - Add Workout Delegate Methods

//-----------------------------------------------------------------------

-(void) addNewWorkout:(NSString *)WorkoutSheet :(NSString *)isFromWhere {
    
    //Get Upgarde Status
     
    MESSAGE(@":self.currentBlockNo: %ld",(long)self.currentBlockNo);

    
    if ([isFromWhere isEqualToString:@"FromEdit"])
    {
        if ([isFromWhere length]!=0) {
            
            //Get User ID
            NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            if([strUpgradeStatus isEqualToString:@"YES"]){
                
                //TODO:SUNIL-> UPDATE  WORKOUT NAME
                
                MESSAGE(@"SUNIL-> Update Nmame WORKOUT: %@",WorkoutSheet);
                
                //Craete dict
                NSDictionary *dictWorkout =@{
                                             kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                             kBlockNo :[commonUtility retrieveValue:@"workoutIdBlockNo"],
                                             kClientId :self.strClientId,
                                             kBlockTitle:WorkoutSheet,
                                             ACTION:@"updateWorkout",
                                             };
                
                //Invoke method for poast new workout to server
                [self postRequestForwWorkout:[dictWorkout mutableCopy]];
                
            }else{
                
                NSInteger success =   [[DataManager initDB] updateBlockName:WorkoutSheet forClient:self.strClientId sheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] blockId:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]];
                self.isFromPreviousNext = FALSE;
                
                
                [self fillArrayWithData];
                [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                if (success==0) {
                    DisplayAlertWithTitle(NSLocalizedString(@"Sheet name updated successfully.", nil),kAppName);
                }
            }
            
            
        }
    }
    
    else
    {
        
        NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];
        
        
        //Get User ID
        NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
           if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            
            //TODO:SUNIL-> ADD NEW WORKOUT
            
            MESSAGE(@"SUNIL-> ADD NEW WORKOUT");
            
            //Craete dict
            NSDictionary *dictWorkout =@{
                                         kSheetId : sId,
                                         kDate :[self.btnDate titleForState:UIControlStateNormal],
                                         kBlockNo :[NSString stringWithFormat:@"%ld", (long)_blockSheetNo],
                                         kClientId :self.strClientId,
                                         kBlockTitle:WorkoutSheet,
                                         kBlockNotes : @"",
                                         ACTION      : @"addWorkout",
                                         @"user_id"  :[commonUtility retrieveValue:KEY_TRAINER_ID]
                                      
                                         };
               
            //Invoke method for poast new workout to server
            [self postRequestForwWorkout:[dictWorkout mutableCopy]];
            
        }else{
            
            _blockSheetNo = [[[DataManager initDB]getBlockNumberForNewWorkoutForClient:self.strClientId forProgramSheet:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]] integerValue] + 1;
            rowValueForExercise =0;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MM/dd/yyyy"];
            NSString *strDate = [formatter stringFromDate:[NSDate date]];
            
            
            [[DataManager initDB]insertSheetBlocksForSheet:sId date:strDate blockNo:[NSString stringWithFormat:@"%ld", (long)_blockSheetNo] clientId:self.strClientId blockTitle:WorkoutSheet blockNotes:@"" andBlockId:@""];
            
            
            NSInteger success1 = [[DataManager initDB] updateDate:[self.btnDate titleForState:UIControlStateNormal] blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
            
      
      
            
            [self fillArrayWithData];
            [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
            
            [self.tableview reloadData];
            self.tableview.delegate = self;
            self.tableview.dataSource = self;
        }
        
    }
    
    if ([self.arrayProgramData count]>0)
    {
        self.lblInstruction.hidden = YES;
    }
    else
    {
        self.lblInstruction.hidden = NO;
    }
}

#pragma mark - Add Newline Delegate Methods

//-----------------------------------------------------------------------

-(void) addNewSheet:(NSDictionary *)detailsExercise {
    START_METHOD
    rowValueForExercise = rowValueForExercise + 1;
    NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                         kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                         kExercise :[detailsExercise valueForKey:@"exercise"],
                         kLBValue :[detailsExercise valueForKey:@"weight"],
                         kRepValue :[detailsExercise valueForKey:@"rep"],
                         kSetValue : [detailsExercise valueForKey:@"set"],
                         kEXDate :[detailsExercise valueForKey:@"restTime"],
                         kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                         kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValueForExercise],
                         kColor : [detailsExercise valueForKey:@"selectedColor"]
                         };
    
    
    //Get Upgarde Status
     
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
 //TODO: SUNIL-> ADD EXERCISE
        
        NSMutableDictionary *dictExercise   =   [[NSMutableDictionary alloc]initWithDictionary:[dic mutableCopy]];
        [dictExercise setValue:@"addExercise" forKey:ACTION];
        [dictExercise setValue:[dic objectForKey:kEXDate] forKey:kDate];
        
        //Invoke method for Save Exercise
        [self postRequestForExercise:dictExercise];
        
        
    }else{
   

    NSInteger success = [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
    

    
    
    
    dic = nil;
    [self fillArrayWithData];
    [self.tableview reloadData];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    }
    
}

#pragma mark - Edit Newline Delegate Methods

//-----------------------------------------------------------------------

-(void) editNewLine:(NSDictionary *)detailsExercise{
    START_METHOD
    
    NSDictionary *dic =@{kBlockId : [detailsExercise valueForKey:@"nBlockID"],
                         kBlockDataId : [detailsExercise valueForKey:kBlockDataId],
                         kSheetId : [detailsExercise valueForKey:@"nSheetID"],
                         kExercise :[detailsExercise valueForKey:@"sProgram"],
                         kLBValue :[detailsExercise valueForKey:@"sLBValue"],
                         kRepValue :[detailsExercise valueForKey:@"sRepValue"],
                         kSetValue : [detailsExercise valueForKey:@"sSetValue"],
                         kEXDate :[detailsExercise valueForKey:@"dEXDate"],
                         kBlockNo :[detailsExercise valueForKey:@"nBlockNo"],
                         kRowNo :[detailsExercise valueForKey:@"nRowNo"],
                         kColor : [detailsExercise valueForKey:@"selectedColor"],
                         @"RowId" : [detailsExercise valueForKey:@"RowId"]
                         };
    
    
    
    //Get Upgarde Status
     
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
        
        NSMutableDictionary *dictExercise   =   [[NSMutableDictionary alloc]initWithDictionary:[dic mutableCopy]];
        [dictExercise setValue:@"updateExercise" forKey:ACTION];
        [dictExercise setValue:[dic objectForKey:kEXDate] forKey:kDate];
        [dictExercise setValue:[dic objectForKey:kBlockDataId] forKey:PROGRAM_EXERCISE_ID];

        
        //Invoke method for uodate the Exercise on server
        [self postRequestForExercise:dictExercise];
        
    }else{
     
        [[DataManager initDB] updateProgramData:dic forClient:self.strClientId];
        
        
        [self fillArrayWithData];
        
        NSInteger rowIdReload = [[detailsExercise valueForKey:@"RowId"]integerValue];
        [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIdReload inSection:0]] withRowAnimation:UITableViewRowAnimationNone];

    }
    
}

//-----------------------------------------------------------------------




#pragma mark -
#pragma mark IBAction methods

- (IBAction)btnEditProgramAction:(id)sender
{
    START_METHOD
    ProgramNameViewController *ProgramNameVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getProgram"];
    UINavigationController* programNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramNameVCOBJ];
    [programNavigationController.navigationBar setTranslucent:NO];
    programNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    programNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    
    ProgramNameVCOBJ.strSheetName =  [[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:ksheetName];
    ProgramNameVCOBJ.delegate = self;
    ProgramNameVCOBJ.isSetFrame = true;
    if (iOS_8) {
        CGPoint frameSize = CGPointZero;
            frameSize = CGPointMake(500, 150);
        programNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    [self.view.window.rootViewController presentViewController:programNavigationController animated:YES completion:nil];
}

//---------------------------------------------------------------------------------------------------------------


- (IBAction)btnEditWorkoutAction:(id)sender
{
    START_METHOD
    
    
    // Edit Workout
    ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
    
    
    UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
    ProgramGetWorkoutVCOBJ.strSheetName = [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"];
    
    //Set for update wokrout
    [commonUtility saveValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:kBlockNo] andKey:@"workoutIdBlockNo"];
    
    
    ProgramGetWorkoutVCOBJ.delegate = self;
    [getWorkoutNavigationController.navigationBar setTranslucent:NO];
    getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(500, 116);
        getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
}

//---------------------------------------------------------------------------------------------------------------

- (IBAction)btnEditLineClicked:(UIButton*)sender {
    
    START_METHOD
     [self checkForOptionsViewAndHide];
    ProgramGetDetailVC *ProgramGetDataVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"progDetail"];
    UINavigationController* programGetDetailNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetDataVCOBJ];
    ProgramGetDataVCOBJ.delegate = self;
    ProgramGetDataVCOBJ.dictLineToShow = [self.arrayProgramData objectAtIndex:sender.tag];
    ProgramGetDataVCOBJ.strRowIdtoReload = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    [programGetDetailNavigationController.navigationBar setTranslucent:NO];
    programGetDetailNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    programGetDetailNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    
    CGPoint frameSize = CGPointMake(500, 500);
    if (iOS_8) {
        programGetDetailNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
       
    }
    else{
    }
    
    [self.view.window.rootViewController presentViewController:programGetDetailNavigationController animated:YES completion:^{
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

- (IBAction)mailBtnAction:(id)sender
{
    START_METHOD
    [self checkForOptionsViewAndHide];
    NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientId];
    [self.delegate openMail:[clientInfo objectForKey:kEmail1]];
}

//-----------------------------------------------------------------------

- (IBAction)btnDateAction:(id)sender {
    
    START_METHOD
    [self checkForOptionsViewAndHide];
    if (popoverController == nil) {
        datePicker = nil;
        UIViewController* popoverContent = [[UIViewController alloc] init];
        UIView*  popoverView = [[UIView alloc] init];
        popoverView.backgroundColor = [UIColor whiteColor];
        
        datePicker=[[UIDatePicker alloc]init];
        datePicker.frame=CGRectMake(0,44,320, 216);
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setMinuteInterval:5];
        [datePicker setTag:10];
        [popoverView addSubview:datePicker];
        
        UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
        [btnDone addTarget:self action:@selector(seDateFromDatePicker) forControlEvents:UIControlEventTouchUpInside];
        
        
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDone.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
        
        CGRect rect = [[AppDelegate sharedInstance] getWidth:btnDone.titleLabel.text font:btnDone.titleLabel.font height:btnDone.titleLabel.frame.size.height];
        btnDone.frame = CGRectMake(5, 7, rect.size.width + 10, 30);
        btnDone.layer.cornerRadius = 4.0;
        btnDone.backgroundColor = kAppTintColor;
        [popoverView addSubview:btnDone];
        
        
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(btnDone.frame.origin.x + btnDone.frame.size.width + 5, 4, 320 - btnDone.frame.size.width + 10, 36)];
        [lblTitle setText:NSLocalizedString(@"Select Date", nil)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [lblTitle setTextColor:[UIColor grayColor]];
        lblTitle.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
        [popoverView addSubview:lblTitle];
        
        UILabel *lblLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 42, 320, 1)];
        [lblLine setBackgroundColor:kAppTintColor];
        [popoverView addSubview:lblLine];
        
        popoverContent.view = popoverView;
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
        popoverController.delegate=(id)self;
        [popoverController setPopoverContentSize:CGSizeMake(320, 264) animated:NO];
        
        popoverContent = nil;
        btnDone = nil;
        lblTitle = nil;
        lblLine = nil;
        popoverView = nil;
        
    }
    [popoverController presentPopoverFromRect:self.btnDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

//-----------------------------------------------------------------------

- (IBAction)BtnNotesTap:(id)sender {
    
    START_METHOD
    
    [self checkForOptionsViewAndHide];
 
    UpdateNotesVC *UpdateNotesVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"updatenote"];
    UINavigationController* updateNotesNavigationController = [[UINavigationController alloc] initWithRootViewController:UpdateNotesVCOBJ];
    [updateNotesNavigationController.navigationBar setTranslucent:NO];
    updateNotesNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    updateNotesNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
   
  //SUNIL
   // UpdateNotesVCOBJ.notes =   [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockNotes"];
    
    
    UpdateNotesVCOBJ.delegate = self;
    
    if(iOS_8){
        updateNotesNavigationController.preferredContentSize = CGSizeMake(500, 320);
    }
    
    [self.view.window.rootViewController presentViewController:updateNotesNavigationController animated:YES completion:nil];
}

//-----------------------------------------------------------------------

- (IBAction)viewAllTap:(id)sender {
    START_METHOD
    
    [[self.tableview layer] removeAnimationForKey:@"UITableViewReloadDataAnimationKey"];
    [self.view setHidden:YES];
    [[NSUserDefaults standardUserDefaults]setValue:self.strClientId forKey:@"clientID"];
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:@"currentSheetId"];
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forKey:@"blockSheetNo"];
    [self.delegate viewAllDelegate:self.strClientId];
}

//-----------------------------------------------------------------------

-(void)test : (NSInteger)currentSheetID : (NSInteger)blockID
{
     isCopiedWorkOut = NO;
    _blockSheetNo = blockID;
    
    self.currentSheet = currentSheetID;
    self.currentBlockNo =  blockID-1;
    self.isFromPreviousNext = TRUE;
    
     self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue] ;
    
       self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];

    _blockSheetNo = [[[ self.arrayBlockDates objectAtIndex:self.currentBlockNo]objectForKey:@"nBlockNo"]intValue];
    self.isFromPreviousNext = TRUE;
    [self fillArrayWithData];
    [self setInitialLayout];
    [self.tableview reloadData];
   
    [popover dismissPopoverAnimated:YES];
    
    PopoverViewController.delegate = nil;
    PopoverViewController = nil;
    popover = nil;

}

//-----------------------------------------------------------------------

- (IBAction)dropDownBtnTap:(id)sender {

    START_METHOD
    [self checkForOptionsViewAndHide];
   
    UIStoryboard*  storybBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     PopoverViewController = [storybBoard instantiateViewControllerWithIdentifier:@"popover"];
    
    popover = [[UIPopoverController alloc] initWithContentViewController:PopoverViewController];
    popover.delegate = self;
    PopoverViewController.arrayProgramSheet = self.arrSheetsList;
    PopoverViewController.strClientID = self.strClientId;
    [PopoverViewController setDelegate:self];
    if ([popover isPopoverVisible]) {
        [popover dismissPopoverAnimated:YES];
    }
    else{
        popover.popoverContentSize = CGSizeMake(350,200);
        [popover presentPopoverFromRect:self.btnDropDown.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionUp animated:YES];
    }
        
}

//-----------------------------------------------------------------------

-(void)WorkOutTap:(UIButton *)sender {
}

//-----------------------------------------------------------------------

- (void)reloadData:(BOOL)animated with :(NSString *)string
{
    [self.tableview reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        if ([string isEqualToString:@"Next"]) {
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [animation setSubtype:kCATransitionFromRight];
        } else {
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
            [animation setSubtype:kCATransitionFromLeft];
        }
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.3];
        [[self.tableview layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}


//-----------------------------------------------------------------------


- (IBAction)PrevNextBtnTap:(id)sender {
    
    START_METHOD
    
    ////-----> New Work 29 Dec
    int intTag =   (int)[sender tag];

    
    /*
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];

    if(strUpgradeStatus && strUpgradeStatus.length>0 && [strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke method for get exercise from server
        NSMutableDictionary *dictObjWorkout    =   [[NSMutableDictionary alloc]init];
        [dictObjWorkout setValue:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forKey:@"workout_id"];
        [dictObjWorkout setValue:[NSString stringWithFormat:@"%d",intTag] forKey:@"buttonTag"];
        
        [self getExerciseFromServer:dictObjWorkout];
        
    }else{
        
        //Invoke method for show exercise list
        [self showExerciseList:intTag];
    
    }
     */
    
    
    
    
    //Invoke method for show exercise list
    [self showExerciseList:intTag];
  
    
    //////<--------
    
    /*
    
    MESSAGE(@"self.currentSheet: %ld %ld %ld %ld %ld",(long)self.currentSheet,(long)_blockSheetNo,(long)currentBlockNo,(long)self.currentSheet, (long)self.currentSheetId);
    
    self.isFromPreviousNext = TRUE;
    isCopiedWorkOut = NO;
    [self checkForOptionsViewAndHide];
    [copyLineArray removeAllObjects];
    
    if([sender tag] == 501) {
        
    if (self.currentBlockNo>0) {
        
    
        self.currentBlockNo = self.currentBlockNo -1;
        _blockSheetNo = _blockSheetNo -1;
        
        _blockSheetNo =  [[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockNo"] intValue];
    
        
        [self fillArrayWithData];
        
            if (self.arrayBlockDates.count) {
                [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Prev"];
            }
            else{
            }
        
        
        
        } else {
            
            if (self.currentSheet > 1)
            {
                
                self.currentSheet = self.currentSheet-1;
                
                
                self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue];
                
                
                
                self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                
         
                _blockSheetNo =  [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] intValue];
                self.totalBlocks = [self.arrayBlockDates count];
                self.currentBlockNo = [self.arrayBlockDates count] -1;
           
                [self fillArrayWithData];
                if (self.arrayBlockDates.count) {
                    
                    
                    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                }
                
                [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet -1] valueForKey:@"sPurpose"]];
                [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet -1] valueForKey:@"sheetName"]];
                [self addUnderlineInprogramName];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Prev"];
            }
            
        }

    } else{
        
        if (self.currentBlockNo < [self.arrayBlockDates count]-1) {
            
            _blockSheetNo = _blockSheetNo +1;
            self.currentBlockNo = self.currentBlockNo + 1;
            
            _blockSheetNo =  [[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockNo"] intValue];
            self.isFromPreviousNext = TRUE;
            
            
            [self fillArrayWithData];
            if (self.arrayBlockDates.count) {
                [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Next"];
            }
            else{
            }
            
        } else {
            
            if (self.currentSheet < [self.arrSheetsList count])
            {
                self.currentSheet = self.currentSheet+1;
                self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue];
                
                _blockSheetNo =  [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] intValue];
                self.totalBlocks = [self.arrayBlockDates count];
                self.currentBlockNo = 0;

                [self fillArrayWithData];
                
                if (self.arrayBlockDates.count) {
                    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                }
                
                [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sPurpose"]];
                [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetName"]];
                [self addUnderlineInprogramName];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Next"];
            }
        }
    }
    
    */
}

//-----------------------------------------------------------------------

- (IBAction)AddProgramTap:(id)sender {
   
    START_METHOD
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
        DisplayAlertWithYesNo(kliteMsg, kAppName, self);
        return;
    }
    else{
        ProgramNameViewController *ProgramNameVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getProgram"];
        UINavigationController*  programNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramNameVCOBJ];
        [programNavigationController.navigationBar setTranslucent:NO];
        programNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        programNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        ProgramNameVCOBJ.strSheetName = @"";
        ProgramNameVCOBJ.delegate = self;
        if (iOS_8) {
            CGPoint frameSize = CGPointMake(500, 200+50);
            programNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        [self.view.window.rootViewController presentViewController:programNavigationController animated:YES completion:nil];
    
    }
}

//-----------------------------------------------------------------------

- (IBAction)AddWorkoutTap:(id)sender {
    
    START_METHOD
    [self checkForOptionsViewAndHide];
    self.isFromPreviousNext = FALSE;
    
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
        DisplayAlertWithYesNo(kliteMsg, kAppName, self);
        return;
    }
    else{
      ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
       UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
        ProgramGetWorkoutVCOBJ.delegate = self;
        [getWorkoutNavigationController.navigationBar setTranslucent:NO];
        getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        if (iOS_8) {
            CGPoint frameSize = CGPointMake(500, 116);
            getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];

    }
}

//-----------------------------------------------------------------------

- (IBAction)backBtnTap:(id)sender {
    
    START_METHOD
    self.isBackTap = YES;
    [self.delegate viewAllDelegate:self.strClientId];
}

//-----------------------------------------------------------------------

- (IBAction)AddNewLineTap:(id)sender {
    
    START_METHOD
    // add new line
    [self checkForOptionsViewAndHide];
    
    if (self.btnAddNewLine.tag == 1) {
        [self copyNewLines];
    }
    else{
        if (self.arrayProgramData.count >4 && (![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"])) {
            DisplayAlertWithYesNo(kliteMsg, kAppName, self);
            return;
        }
        else{
            ProgramGetDetailVC *ProgramGetDataVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"progDetail"];
            UINavigationController* programGetDetailNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetDataVCOBJ];
            ProgramGetDataVCOBJ.delegate = self;
            [programGetDetailNavigationController.navigationBar setTranslucent:NO];
            programGetDetailNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
            programGetDetailNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        
            CGPoint frameSize = CGPointMake(500, 500);

            [self.view.window.rootViewController presentViewController:programGetDetailNavigationController animated:YES completion:^{
               
            }];
        }
    }
    
}

//-----------------------------------------------------------------------

- (IBAction)DeleteBtnTap:(id)sender {
    START_METHOD
    
//    [commonUtility alertMessage:@"Under Development!"];
//    return;
    
     [self checkForOptionsViewAndHide];
    selectedBtn = (UIButton*)sender;
    UIAlertView * alertShow = [[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Are you sure you want to delete this Exercise?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes", nil),NSLocalizedString(@"No", nil),nil];
    [alertShow setTag:1001];
    [alertShow show];
}

//-----------------------------------------------------------------------

-(IBAction)btnCopyLineClicked:(UIButton*)sender{
    START_METHOD
    
    [self checkForOptionsViewAndHide];
    
    NSInteger selectedTag =  sender.tag-(self.blockSheetNo+self.currentSheetId);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([sender.currentImage isEqual:[UIImage imageNamed:@"copy"]]) {
        
        [sender setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
        NSDictionary *dictLine =  [self.arrayProgramData objectAtIndex:selectedTag];
        if ([copyLineArray containsObject:[self.arrayProgramData objectAtIndex:selectedTag]]){
            }
        else{
            [copyLineArray addObject:[self.arrayProgramData objectAtIndex:selectedTag]];
            [defaults setObject:copyLineArray forKey:kIsCopyLine];
        }
        
    }
    else if ([sender.currentImage isEqual:[UIImage imageNamed:@"copy-secelted"]]){
        
        if ([copyLineArray containsObject:[self.arrayProgramData objectAtIndex:selectedTag]]) {
            [copyLineArray removeObject:[self.arrayProgramData objectAtIndex:selectedTag]];
        }
        [sender setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
        [defaults setObject:copyLineArray forKey:kIsCopyLine];
    }
    [defaults synchronize];
}

//-----------------------------------------------------------------------

- (IBAction)copyBtnTap:(UIButton *)sender {
    START_METHOD
    [self checkForOptionsViewAndHide];

    UIButton *selectedBtn1 = (UIButton*)sender;
    NSInteger selectedTag =  selectedBtn1.tag;

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([sender.currentImage isEqual:[UIImage imageNamed:@"copy"]]) {
        
        [sender setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
        NSDictionary *dictLine =  [self.arrayProgramData objectAtIndex:selectedTag];
        if ([copyLineArray containsObject:[self.arrayProgramData objectAtIndex:selectedTag]]){
        }
        else{
            [copyLineArray addObject:[self.arrayProgramData objectAtIndex:selectedTag]];
            [defaults setObject:copyLineArray forKey:kIsCopyLine];
        }
        
        
    }
    else if ([sender.currentImage isEqual:[UIImage imageNamed:@"copy-secelted"]]){
        
        if ([copyLineArray containsObject:[self.arrayProgramData objectAtIndex:selectedTag]]) {
            [copyLineArray removeObject:[self.arrayProgramData objectAtIndex:selectedTag]];
        }
        [sender setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
        [defaults setObject:copyLineArray forKey:kIsCopyLine];
    }
    
    if (copyLineArray.count) {
        self.btnAddNewLine.tag = 1;
        [self.btnAddNewLine setTitle:NSLocalizedString(@"Paste New Exercise", nil) forState:0];
    }
    else{
        self.btnAddNewLine.tag = 0;
        [self.btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:UIControlStateNormal];
    }
    
    [defaults synchronize];
    
}

//-----------------------------------------------------------------------

#pragma mark - TableView Methods

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

//-----------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    return self.arrayProgramData.count;
}

//-----------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    START_METHOD
    ProgramCustomCell *cell = (ProgramCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[ProgramCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        cell.btnCopy.frame = CGRectMake(0,10 ,45 , 100);
        cell.btnDelete.frame = CGRectMake(883,61 ,32 , 29);
        cell.btnEditLine.frame = CGRectMake(883,15 ,32 , 29);

    }
    else
    {
        cell.btnCopy.frame = CGRectMake(0,10 ,45 , 100);
        cell.btnDelete.frame = CGRectMake(684,61 ,32 , 29);
        cell.btnEditLine.frame = CGRectMake(683,15 ,32 , 29);
    }
    [cell.btnCopy setTag:indexPath.row];
    [cell.btnEditLine setTag:indexPath.row];
    
    [self.view bringSubviewToFront:cell.btnEditLine];
    
    [cell.btnDelete setTag: [[[self.arrayProgramData objectAtIndex:indexPath.row] objectForKey:@"nRowNo"] integerValue]];
    if (isCopiedWorkOut) {
       [cell.btnCopy setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
    }
    else{
    [cell.btnCopy setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
    }

    for (int i = 0; i<[copyLineArray count]; i++)
    {
        if (indexPath.row+1 == [[[copyLineArray objectAtIndex:i] valueForKey:@"nRowNo"] integerValue]) {
            [cell.btnCopy setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
        }
        else{
        }
    }
    
    if ([self.arrayProgramData count]>0)
    {
        self.lblInstruction.hidden = YES;
        [cell configCell:[self.arrayProgramData objectAtIndex:indexPath.row]];
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        }
        else{
        [self.tableview setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"tableHorizontalLine"]]];
        }
    }
    else
    {
        self.lblInstruction.hidden = NO;
    }
    
    if(self.setAsLandscape) {
        
        cell.setLandscapeMode = TRUE;
    }else{
        cell.setLandscapeMode = FALSE;
    }
    [cell setupLayoutForCell];

    
    //Invoke method for set color
    [self setFontColor:cell];
    return cell;
}

//-----------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
      [self checkForOptionsViewAndHide];
}
//-----------------------------------------------------------------------



#pragma mark - Custom Methods

//-----------------------------------------------------------------------

-(void)checkForOptionsViewAndHide{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(hideOptionsViewOnTap)]) {
        [self.delegate hideOptionsViewOnTap];
    }
}

//-----------------------------------------------------------------------
-(void)btnFrom_TemplateClicked{

    START_METHOD
    NSArray * arrTemplates = [[DataManager initDB]getAllTemplatesDetail];
    
    if([arrTemplates count] > 0){
        
         TemplateSheetNameController * templatesheetnamecontrollerOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"templateSheet"];
        templatesheetnamecontrollerOBJ.strClientId =self.strClientId;
        templatesheetnamecontrollerOBJ.delegateReloadSheet =self;
        
        [templatesheetnamecontrollerOBJ setModalPresentationStyle:UIModalPresentationFormSheet];
        [templatesheetnamecontrollerOBJ setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        CGPoint frameSize = CGPointMake(500, 400);
        if (iOS_8) {
            templatesheetnamecontrollerOBJ.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        else{
        }
        [self presentViewController:templatesheetnamecontrollerOBJ animated:YES completion:^{
        }];
        templatesheetnamecontrollerOBJ = nil;
    }else{
        DisplayAlertWithTitle(NSLocalizedString(NSLocalizedString(@"No Templates Available.", nil), nil), kAppName);
    }
    END_METHOD
 }

//-----------------------------------------------------------------------

-(void)setBlockData{
    START_METHOD
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strBlockName = [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:kBlockTitle];
    NSMutableDictionary *dictBlock = [[NSMutableDictionary alloc]init];
    [dictBlock setObject:strBlockName forKey:@"sBlockTitle"];
    [dictBlock setObject:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:kBlockDate] forKey:kBlockDate];
    [dictBlock setObject:[NSNumber numberWithInt:1] forKey:@"blockNo"];
    [dictBlock setObject:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] forKey:@"sheetID"];
    [dictBlock setObject:self.arrayProgramData forKey:kCopiedLinesForBlock];
    [defaults setObject:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:kBlockCurrentCopiedSheetID];
    [defaults setObject:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forKey:kCopiedBlockIDSheetID];
    [defaults setObject:self.strClientId forKey:kCopiedClientID];
    isCopiedWorkOut = true;
    [defaults setObject:dictBlock forKey:kCopyProgramBlock];
    [defaults setObject:nil forKey:kCopyBlock];
    [defaults synchronize];
    [self.tableview reloadData];
    END_METHOD
}

//-----------------------------------------------------------------------

-(void)doCopyWorkOut {

    START_METHOD
//TODO: SUNIL COPY PASTE WORKOUT
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];
     NSString *strCurrentCopiedSheetID = [defaults valueForKey:kBlockCurrentCopiedSheetID];
     NSString *strCurrentCopiedClientID = [defaults valueForKey:kCopiedClientID];
    
    
    
    
    if ([strCurrentCopiedBlockId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.blockSheetNo]] && [strCurrentCopiedSheetID isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]] && [strCurrentCopiedClientID isEqualToString:self.strClientId]){
        
        MESSAGE(@"if block ------>");
        
        [self setBlockData];
        
    } else if ([defaults valueForKey:kCopyProgramBlock]) {
        
        
        
        //Invoke MEthod for Copy paste the current Workouts
        [self copyPasteCurrentWorkouts];
        
        
    }else if ([defaults valueForKey:kCopyBlock]) {
        
        
        //Invoke method for paste template from workoute
        [self pasteWorkoutFromTemplate];
        
    }
    else{
        [self setBlockData];
    }
    
    [defaults synchronize];
    END_METHOD

}

//-----------------------------------------------------------------------


-(void)docopyBtnClicked{
    START_METHOD

    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
//TODO: SUNIL PASTE ENTIRE PROGRAM
    NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
    
    NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
    NSString *strProgramClientID = [[defauts valueForKey:kProgramData] valueForKey:kClientId];
    NSString *strProgramSheetID = [[defauts valueForKey:kProgramData] valueForKey:ksheetID];

    if (((strProgramClientID == nil || strProgramSheetID == nil) && strTemplateSheetId == nil) || ([strProgramSheetID isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]] && [strProgramClientID isEqualToString:self.strClientId])) {
        
         NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientId,kClientId,[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],ksheetID, nil];
        [defauts setValue:tempDict forKey:kProgramData];
        [defauts setValue:nil forKey:kTemplateSheetData];
        [defauts synchronize];
        
    }
    
    else{
        
        if(strTemplateSheetId == nil){
            
          

            MESSAGE(@"ENTIRE PROGRAM COPY PASTE strUpgradeStatus--> %@",strUpgradeStatus);
            if([strUpgradeStatus isEqualToString:@"YES"]){
                
//TODO: ENTIRE PROGRAM COPY PASTE                //Invoke MEthod for Paste on server
                [self pasteEntireProgramInServer];
                
            }else{
                
                //Invoke method for paste the entire the program
                [self pasteEntireProgram];
            }
           
            
        }else{//For Entire Template PRogram
            
            MESSAGE(@"ENTIRE PROGRAM COPY PASTE strUpgradeStatus--> %@",strUpgradeStatus);
            if([strUpgradeStatus isEqualToString:@"YES"]){
            
                
                // For get data
                [self getEntireTemplateProgramForPaste];
                
            }else{
                
                //Invoke method for paste entire program from Template
                [self pasteEntireProgramFromTemplate];
            }
            
            
            
        }
    }
}

//TODO: PASTE Entore Program

-(void)pasteEntireProgram{
    START_METHOD
    //TODO: SUNIL PASTE ENTIRE PROGRAM
    NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
    
    NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
    NSString *strProgramClientID = [[defauts valueForKey:kProgramData] valueForKey:kClientId];
    NSString *strProgramSheetID = [[defauts valueForKey:kProgramData] valueForKey:ksheetID];

    NSArray *tempArraBlocks = [[DataManager initDB] getBlockDataForClient:strProgramClientID SheetId:strProgramSheetID];
    
    MESSAGE(@"tempArraBlocks->: %@",tempArraBlocks);
    
    for(int i=0;i<[tempArraBlocks count];i++){
        
        NSDictionary *dictBlock = [tempArraBlocks objectAtIndex:i];
        _blockSheetNo = _blockSheetNo + 1;
        rowValueForExercise = 0;
        
        self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
        
        MESSAGE(@"self.arrayBlockDates->Workouts: %@",self.arrayBlockDates);
        
        
        NSInteger intLastBlockNum = [[[self.arrayBlockDates objectAtIndex:self.arrayBlockDates.count-1] valueForKey:@"nBlockNo"] integerValue];
        
        NSMutableDictionary *dictTempBlock = [[NSMutableDictionary alloc]initWithDictionary:dictBlock];
        [dictTempBlock setObject:[NSString stringWithFormat:@"%ld",(long)intLastBlockNum+1] forKey:@"nBlockNo"];
        
        
        //Insert Into Workouts
        [[DataManager initDB] insertSheetBlocksForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] :self.strClientId :dictTempBlock];
        
        NSArray * temparrayBlocks = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]]];
        
        MESSAGE(@"temparrayBlocks-> %@",temparrayBlocks);
        
        NSInteger tempCurrentBlockSheetNum = temparrayBlocks.count;
        
        NSArray *arrayTempPrograms = [[DataManager initDB] selectProgramDataForClient:strProgramClientID sheet:strProgramSheetID blockNo:[NSString stringWithFormat:@"%ld",(long)[[dictBlock valueForKey:kBlockNo]integerValue]]];
        
        MESSAGE(@"arrayTempPrograms-> %@",arrayTempPrograms);
        
        NSInteger rowValueForExercise1 = 0;
        for (NSDictionary *dictCopiedLine in arrayTempPrograms) {
            
            rowValueForExercise1 = rowValueForExercise1 + 1;
            NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)tempCurrentBlockSheetNum],
                                 kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                 kExercise :[dictCopiedLine valueForKey:kExercise],
                                 kLBValue :[dictCopiedLine valueForKey:kLBValue],
                                 kRepValue :[dictCopiedLine valueForKey:kRepValue],
                                 kSetValue : [dictCopiedLine valueForKey:@"sSetValue"],
                                 kEXDate :[dictCopiedLine valueForKey:kEXDate],
                                 kBlockNo :[NSString stringWithFormat:@"%ld",(long)tempCurrentBlockSheetNum],
                                 kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValueForExercise1],
                                 kColor : [dictCopiedLine valueForKey:kColor],
                                 };
            MESSAGE(@"dictCopiedLine loop-> %@",dic);
            //Insert Exercse
            [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
            
        }
        
        
    }
    
  //Invoke Method for Realod view
    [self reloadViewAfterPasterEntireProgram];
    
 END_METHOD
}
//-----------------------------------------------------------------------


- (void) copyBtnClicked{
    START_METHOD
    NSString *sClientID = [[[NSUserDefaults standardUserDefaults] valueForKey:kProgramData] valueForKey:kClientId];
    NSString *sSheetID = [[[NSUserDefaults standardUserDefaults] valueForKey:kProgramData] valueForKey:kSheetId];
    NSString * strTemplatesheetId = [[NSUserDefaults standardUserDefaults]valueForKey:kTemplateSheetData];
    
    if(sClientID == nil ){
        NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientId,kClientId,[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],kSheetId, nil];
        [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else{
        
        if(sClientID == nil){

            NSInteger isDatadeleted =  [[DataManager initDB] deleteBlockData:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
            
            if(isDatadeleted == 0){
                
                NSArray * arrProgramData = [[DataManager initDB]selectProgramDataForSheet_Template:[strTemplatesheetId intValue]];
                
                [arrProgramData enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL * stop){
                    NSDictionary * dic = (NSDictionary *)obj;
                    
                    NSInteger blockid = [[DataManager initDB]getBlockIdForClient: [NSString stringWithFormat:@"%ld",(long)self.currentSheetId] :[dic valueForKey:kBlockNo] :self.strClientId];
                    
                    
                    NSDictionary * dictemp = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)blockid],kBlockId,
                                              [dic valueForKey:kExercise],kExercise,
                                              [dic valueForKey:kLBValue],kLBValue,
                                              [dic valueForKey:kRepValue],kRepValue,
                                              [dic valueForKey:kSetValue],kSetValue,
                                              [dic valueForKey:kColor],kColor,
                                              [dic valueForKey:kBlockNo],kBlockNo,
                                              [dic valueForKey:kRowNo],kRowNo,
                                              [dic valueForKey:kEXDate],kEXDate,nil];
                    NSInteger success = [[DataManager initDB]insertProgramData:dictemp forClient:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                    if(success == 0){
                        dictemp = nil;
                        dic = nil;
                    }
                    
                }];
                
                [[NSUserDefaults standardUserDefaults]setValue:nil forKey:kTemplateSheetData];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self fillArrayWithData];
            }
            
        }
        else{
            
            if([sClientID isEqualToString:self.strClientId]){
                
                if([sSheetID isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]]){
                    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientId,kClientId,[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],kSheetId, nil];
                    [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else{
                    NSArray *tempArray = [[DataManager initDB] getBlockDataForClient:sClientID SheetId:sSheetID];
                    NSArray *tempArray1 = [[DataManager initDB] selectProgramDataForClient:sClientID sheet:sSheetID];
                    [[DataManager initDB] deleteBlockData:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                    BOOL success = FALSE;
                    for(int i=0;i<[tempArray1 count];i++){
                        success = [[DataManager initDB] insertProgramData:[tempArray1 objectAtIndex:i] forClient:self.strClientId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                    }
                    if(success){
                    }
                    
                    [[DataManager initDB] deleteBlocks:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                    for(int i=0;i<[tempArray count];i++){
                        success = [[DataManager initDB] insertSheetBlocksForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] :self.strClientId :[tempArray objectAtIndex:i]];
                        if(success){
                        }
                    }
                    
                    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
                    [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self fillArrayWithData];
                }
            } else{
                NSArray *tempArray = [[DataManager initDB] getBlockDataForClient:sClientID SheetId:sSheetID];
                NSArray *tempArray1 = [[DataManager initDB] selectProgramDataForClient:sClientID sheet:sSheetID];
                [[DataManager initDB] deleteBlockData:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                BOOL success = FALSE;
                for(int i=0;i<[tempArray1 count];i++){
                    success = [[DataManager initDB] insertProgramData:[tempArray1 objectAtIndex:i] forClient:self.strClientId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                }
                if(success){
                }
                
                [[DataManager initDB] deleteBlocks:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]];
                for(int i=0;i<[tempArray count];i++){
                    success = [[DataManager initDB] insertSheetBlocksForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] :self.strClientId :[tempArray objectAtIndex:i]];
                    if(success){
                    }
                }
                NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
                [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self fillArrayWithData];
                
            }
            
        }
    }
    
    [self.tableview reloadData];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
}

//-----------------------------------------------------------------------

-(void)copyNewLines
{
//    
//    [commonUtility alertMessage:@"Under Development!"];
//    return;
    
    //Get Upgarde Status
     
         
    if (self.arrayProgramData.count >4 && (![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"])) {
        DisplayAlertWithYesNo(kliteMsg, kAppName, self);
        return;
    }
    else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrayCopiedLines = (NSArray*)[defaults valueForKey:kIsCopyLine];
        if (arrayCopiedLines.count) {
            [arrayCopiedLines enumerateObjectsUsingBlock:^(NSDictionary *dictLine, NSUInteger idx, BOOL *stop) {

                
                if (![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]){
                    
                    [self.arrayProgramData setArray:[[DataManager initDB] selectProgramDataForClient:self.strClientId sheet:[NSString stringWithFormat:@"%ld",(long)_currentSheetId] blockNo:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]]];
                    
                    if (self.arrayProgramData.count >4){
                        *stop = YES;
                        return;
                    }
                }
           
                rowValueForExercise =  rowValueForExercise+1;
                NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSString stringWithFormat:@"%ld",(long)_blockSheetNo],kBlockId,
                                      [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],kSheetId,
                                      [dictLine valueForKey:kExercise],kExercise,
                                      [dictLine valueForKey:kLBValue],kLBValue,
                                      [dictLine valueForKey:kRepValue],kRepValue,
                                      [dictLine valueForKey:kEXDate],kEXDate,
                                      [dictLine valueForKey:@"sSetValue"],@"sSetValue",
                                      [NSString stringWithFormat:@"%ld",(long)_blockSheetNo],kBlockNo,
                                      [NSString stringWithFormat:@"%ld",(long)rowValueForExercise],kRowNo,
                                      [dictLine valueForKey:@"sColor"],kColor,nil];
//TODO: SUNIL Copy Paste exercise
          
                //Get User ID
                NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
                
                if ([strUpgradeStatus isEqualToString:@"YES"]) {
                    
                    NSMutableDictionary *dictExercise   =   [[NSMutableDictionary alloc]initWithDictionary:[dic mutableCopy]];
                    [dictExercise setValue:@"addExercise" forKey:ACTION];
                    [dictExercise setValue:[dic objectForKey:kEXDate] forKey:kDate];
                    
                    //Invoke method for paste/Add exercise fon server
                    [self postRequestForExercise:[dictExercise mutableCopy]];
                    
                }else{
                    
                    //Invoke method for paste exercise on server for client
                    [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
                }

            }];
    
            //Get User ID
            NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            if (![strUpgradeStatus isEqualToString:@"YES"]) {
                
                if (copyLineArray.count) {
                    [copyLineArray removeAllObjects];
                }
                self.btnAddNewLine.tag = 0;
                [self.btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:UIControlStateNormal];
                
                
                [defaults setObject:nil forKey:kIsCopyLine];
                [self fillArrayWithData];
                [self.tableview reloadData];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
            }
        }
        else{
            DisplayAlertWithTitle(NSLocalizedString(@"Please select exercise to paste.", nil), kAppName);
        }
    }
}


//-----------------------------------------------------------------------

- (void) copyWorkoutClicked{
    
    NSString *sClientID = [[[NSUserDefaults standardUserDefaults] valueForKey:kProgramData] valueForKey:kClientId];
    NSString *sSheetID = [[[NSUserDefaults standardUserDefaults] valueForKey:kProgramData] valueForKey:kSheetId];
    NSString *sBlockID = [[[NSUserDefaults standardUserDefaults] valueForKey:kProgramData] valueForKey:kBlockId];
    
    if(sClientID == nil ){
        NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientId,kClientId,[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],kSheetId,[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],kBlockId, nil];
        [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else{
        
        
        if(sClientID == nil){
            
        }
        else{
            
            
            if([sClientID isEqualToString:self.strClientId]){
                
                if([sSheetID isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]]){
                    
                   if([sBlockID isEqualToString:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]]){
                        NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:self.strClientId,kClientId,[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],kSheetId,[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],kBlockId, nil];
                        [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                   else{
                       NSArray *tempArray =  [[DataManager initDB] getBlockDataForClient:sClientID SheetId:sSheetID :sBlockID];
                       NSArray *tempArray1 = [[DataManager initDB] selectProgramDataForClient:sClientID sheet:sSheetID blockNo:sBlockID];
                       NSInteger blockno = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue] +1 ;
                       
                       NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];
                       NSInteger success = [[DataManager initDB]insertSheetBlocksForSheet:sId date:[[tempArray objectAtIndex:0] valueForKey:@"dBlockdate"] blockNo:[NSString stringWithFormat:@"%ld", (long)blockno] clientId:self.strClientId blockTitle:[[tempArray objectAtIndex:0] valueForKey:@"sBlockTitle"] blockNotes:[[tempArray objectAtIndex:0] valueForKey:@"sBlockNotes"] andBlockId:@""];
                       
                       
                       for(int i=0;i<[tempArray1 count];i++){
                           NSInteger rowValue = 1;
                           NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)blockno],
                                                kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                                kExercise :[[tempArray1 objectAtIndex:i] valueForKey:@"sProgram"],
                                                kLBValue :[[tempArray1 objectAtIndex:i] valueForKey:@"sLBValue"],
                                                kRepValue :[[tempArray1 objectAtIndex:i] valueForKey:@"sRepValue"],
                                                kSetValue : [[tempArray1 objectAtIndex:i] valueForKey:@"sSetValue"],
                                                kEXDate :[[tempArray1 objectAtIndex:i] valueForKey:@"dEXDate"],
                                                kBlockNo :[NSString stringWithFormat:@"%ld",(long)blockno],
                                                kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValue]
                                                };
                           NSInteger success = [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
                           rowValue ++;
                           
                       }
                       if(success){
                       }
                       NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
                       [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       [self fillArrayWithData];
                   }
                }
                else{
                  NSArray *tempArray =  [[DataManager initDB] getBlockDataForClient:sClientID SheetId:sSheetID :sBlockID];
                   NSArray *tempArray1 = [[DataManager initDB] selectProgramDataForClient:sClientID sheet:sSheetID blockNo:sBlockID];
                    NSInteger blockno = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue] +1 ;

                    NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];
                    NSInteger success = [[DataManager initDB]insertSheetBlocksForSheet:sId date:[[tempArray objectAtIndex:0] valueForKey:@"dBlockdate"] blockNo:[NSString stringWithFormat:@"%ld", (long)blockno] clientId:self.strClientId blockTitle:[[tempArray objectAtIndex:0] valueForKey:@"sBlockTitle"] blockNotes:[[tempArray objectAtIndex:0] valueForKey:@"sBlockNotes"] andBlockId:@""];
                    
                    for(int i=0;i<[tempArray1 count];i++){
                        NSInteger rowValue = 1;
                        NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)blockno],
                                             kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                             kExercise :[[tempArray1 objectAtIndex:i] valueForKey:@"sProgram"],
                                             kLBValue :[[tempArray1 objectAtIndex:i] valueForKey:@"sLBValue"],
                                             kRepValue :[[tempArray1 objectAtIndex:i] valueForKey:@"sRepValue"],
                                             kSetValue : [[tempArray1 objectAtIndex:i] valueForKey:@"sSetValue"],
                                             kEXDate :[[tempArray1 objectAtIndex:i] valueForKey:@"dEXDate"],
                                             kBlockNo :[NSString stringWithFormat:@"%ld",(long)blockno],
                                             kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValue]
                                             };
                        NSInteger success = [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
                        rowValue ++;

                    }
                    if(success){
                    }
                    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
                    [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self fillArrayWithData];
                }
            }
            
            
            else{
                NSArray *tempArray =  [[DataManager initDB] getBlockDataForClient:sClientID SheetId:sSheetID :sBlockID];
                NSArray *tempArray1 = [[DataManager initDB] selectProgramDataForClient:sClientID sheet:sSheetID blockNo:sBlockID];
                NSInteger blockno = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue] +1 ;
                
                NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];
                NSInteger success = [[DataManager initDB]insertSheetBlocksForSheet:sId date:[[tempArray objectAtIndex:0] valueForKey:@"dBlockdate"] blockNo:[NSString stringWithFormat:@"%ld", (long)blockno] clientId:self.strClientId blockTitle:[[tempArray objectAtIndex:0] valueForKey:@"sBlockTitle"] blockNotes:[[tempArray objectAtIndex:0] valueForKey:@"sBlockNotes"] andBlockId:@""];
                
                
                for(int i=0;i<[tempArray1 count];i++){
                    NSInteger rowValue = 1;
                    NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)blockno],
                                         kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                         kExercise :[[tempArray1 objectAtIndex:i] valueForKey:@"sProgram"],
                                         kLBValue :[[tempArray1 objectAtIndex:i] valueForKey:@"sLBValue"],
                                         kRepValue :[[tempArray1 objectAtIndex:i] valueForKey:@"sRepValue"],
                                         kSetValue : [[tempArray1 objectAtIndex:i] valueForKey:@"sSetValue"],
                                         kEXDate :[[tempArray1 objectAtIndex:i] valueForKey:@"dEXDate"],
                                         kBlockNo :[NSString stringWithFormat:@"%ld",(long)blockno],
                                         kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValue]
                                         };
                    NSInteger success = [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
                    rowValue ++;
                    
                }
                if(success){
                }
                NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
                [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self fillArrayWithData];
            }
            
        }
    }
    
    [self.tableview reloadData];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
}

//-----------------------------------------------------------------------

- (void) clearClipBoardBtnClciked{
    START_METHOD
    [copyWorkoutArray removeAllObjects];
    [copyLineArray removeAllObjects];
    

    NSUserDefaults *defaluts = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
    [defaluts setValue:tempDict forKey:kProgramData];
    
    NSDictionary *tempDict1 = [[NSDictionary alloc] initWithObjectsAndKeys:nil,@"clientID",nil,@"sheetID",nil,@"blockID", nil];
    [defaluts setValue:tempDict1 forKey:kProgramBlockData];
    
    [defaluts setObject:nil forKey:kIsCopyLine];
    
    [defaluts setValue:nil forKey:kTemplateSheetData];
    [defaluts synchronize];
    
    [defaluts setObject:nil forKey:kBlockCurrentCopiedSheetID];
    [defaluts setObject:nil forKey:kCopiedBlockIDSheetID];
    [defaluts setObject:nil forKey:kCopyBlock];
    [defaluts setObject:nil forKey:kCopyProgramBlock];
    
    tempDict = nil;
    
    self.btnAddNewLine.tag = 0;
    [self.btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:UIControlStateNormal];
    isCopiedWorkOut = false;
    [self.tableview reloadData];
    END_METHOD
}

//-----------------------------------------------------------------------

-(void)deleteSheet {
    
    
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    //If not Upgrade then show alert for upgrade
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        
        
        //Create dictionaryb for send param in api for Trainer's Profile
        NSDictionary *params = @{
                                 kClientId              :   self.strClientId,
                                 ACTION                 :   @"deleteWorkout",
                                 @"client_workout_id"               : [NSString stringWithFormat:@"%ld",(long)_blockSheetNo]
                                 };
        
        //Invoke method for post request to delete client
        [self postRequestForDeleteWorkout:params];
        
    }else{
        
        //Invoke method for delete ASSESSMENT SHEET detail
        [self deleteWorkoutFromWorkOut];
        
    }
    
}

//-----------------------------------------------------------------------

-(void)seDateFromDatePicker{
    START_METHOD
    NSDate * date = datePicker.date;
    NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
    [dateformatter setDateFormat:kAppDateFormat];
    [self.btnDate setTitle:[dateformatter stringFromDate:date] forState:UIControlStateNormal];
    self.btnDate.titleLabel.font = [UIFont systemFontOfSize:14];
    [popoverController dismissPopoverAnimated:YES];
    dateformatter = nil;
    NSInteger success = [[DataManager initDB] updateDate:[self.btnDate titleForState:UIControlStateNormal] blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];

    
    //TODO:SUNIL-> ADD  WORKOUT NAME
    self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
    
    
    //--->
    MESSAGE(@"fillArrayWithData-> %@ and new self.currentBlockNo: %ld",self.arrayBlockDates,(long)self.currentBlockNo);
  
    NSString *strWorkName;
    
    if (self.arrayBlockDates && self.arrayBlockDates.count>self.currentBlockNo) {
        
        NSDictionary *objDict   =   [self.arrayBlockDates objectAtIndex:self.currentBlockNo];
        strWorkName = [NSString stringWithFormat:@"%@",[objDict objectForKey:@"sBlockTitle"]];
        
    }
    
    
    if(strWorkName && strWorkName.length>0){
        
    }else{
        strWorkName = @"workout-1";
        
    }
    
    MESSAGE(@"SUNIL-> Update Nmame WORKOUTbtnEditWorkoutAction : %@",_btnEditWorkout.titleLabel.text);
    
    //Craete dict
    NSDictionary *dictWorkout =@{
                                 kUpdatedDate:[NSString stringWithFormat:@"%@",[self.btnDate titleForState:UIControlStateNormal]],
                                 kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                 kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                                 kClientId :self.strClientId,
                                 ACTION     :@"updateWorkout",
                                 @"sBlockTitle": strWorkName,
                                 
                                 };
    
    //Invoke method for poast new workout to server
    [self postRequestForwWorkout:[dictWorkout mutableCopy]];
    
    
    [self fillArrayWithData];
}

//-----------------------------------------------------------------------

-(void)hideDropDown{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.dropdownview.frame = CGRectMake(295 , 200, 138, 0);
    for (UIView *view in self.dropdownview.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            [btn setFrame:CGRectMake(btn.frame.origin.x, btn.frame.origin.y, 0, 0)];
            [view removeFromSuperview];
        }
    }
    [UIView commitAnimations];
}

//-----------------------------------------------------------------------

-(void)DropDownItemTap:(UIButton *)sender {
    self.isDropDownShow = NO;
    [self.tableview reloadData];
    [self hideDropDown];
}



//-----------------------------------------------------------------------

- (void) fillArrayWithData {
    START_METHOD
    
      self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
    
    
    //--->
    MESSAGE(@"fillArrayWithData-> %@ and self.currentBlockNo: %ld",self.arrayBlockDates,(long)self.currentBlockNo);
    
    //19 sept 2016
    //For add new program or if count-1 is greater then current block no.
    
    if(self.currentBlockNo>[self.arrayBlockDates count]-1){
     
        MESSAGE(@"if(self.currentBlockNo>[self.arrayBlockDates count]-1)");
        
        self.currentBlockNo = [self.arrayBlockDates count]-1;
    }
    ///<-----
    
    
    MESSAGE(@"fillArrayWithData-> %@ and self.currentBlockNo: %ld",self.arrayBlockDates,(long)self.currentBlockNo);
    
    //14 sept 2016
    //Sunil Not getting right _blockSheetNo when tap net in exercise when add from premade template workout
    
    
    
     _blockSheetNo = [[[ self.arrayBlockDates objectAtIndex:self.currentBlockNo]objectForKey:@"nBlockNo"]intValue];
    
    //Get array of
     [self.arrayProgramData setArray:[[DataManager initDB] selectProgramDataForClient:self.strClientId sheet:[NSString stringWithFormat:@"%ld",(long)_currentSheetId] blockNo:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]]];
    
    self.totalBlocks = [self.arrayBlockDates count];
    
    if(!self.isFromPreviousNext) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromNotes"])
        {

        }
        else
        {
            
            //Get User ID
            NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            if(strUpgradeStatus && [strUpgradeStatus isEqualToString:@"YES"]){
                
            }else{
            
            //Commented for Update Workout SUNIL
                self.currentBlockNo = [self.arrayBlockDates count]-1;
            }
        }
    }

    if ([self.arrayProgramData count]==0)
    {
        self.lblInstruction.hidden = NO;
    }
    else
    {
        self.lblInstruction.hidden = YES;
        
    }
    
    MESSAGE(@"self.arrayBlockDates> %@ and self.currentBlockNo: %ld",self.arrayBlockDates,(long)self.currentBlockNo);
    
    
    if(self.arrayBlockDates.count>0){
        
        
        NSDateFormatter  *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:kAppDateFormat];
        
        if ([[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"dBlockdate"] isEqualToString:@""]) {
            
            [self.btnDate setTitle:[NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]] forState:UIControlStateNormal];
        }
        else
        {
            
            
            [self.btnDate setTitle:[NSString stringWithFormat:@"%@", [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"dBlockdate"]] forState:UIControlStateNormal];
        }
        formatter = nil;
    }
    

    
    rowValueForExercise = [[[self.arrayProgramData lastObject] valueForKey:kRowNo] intValue];
    [copyLineArray removeAllObjects];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];
    NSString *strCurrentSheetID =[defaults valueForKey:kBlockCurrentCopiedSheetID] ;
    NSString *strCurrentCopiedClientID = [defaults valueForKey:kCopiedClientID];
    if ([strCurrentCopiedBlockId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.blockSheetNo]] && [strCurrentSheetID isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]] &&  [strCurrentCopiedClientID isEqualToString:self.strClientId]) {
        isCopiedWorkOut = true;
    }
  
   END_METHOD
}
//-----------------------------------------------------------------------

- (NSString*)stringVisibleInRect:(CGRect)rect1 withFont:(UIFont*)font string:(NSString *)str
{
    NSString *visibleString = @"";
    for (int i = 1; i <= str.length; i++)
    {
        NSString *testString = [str substringToIndex:i];
        CGRect rect2 = [testString boundingRectWithSize:rect1.size options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil];
        if (rect2.size.width > rect1.size.width)
            break;
        visibleString = testString;
    }
    return visibleString;
}

//-----------------------------------------------------------------------

-(void)setLabelValue :(NSString *)ProgramName
{
    START_METHOD
    [self.lblWorkoutName setAttributedText:nil];
    [self.lblWorkoutName setText:nil];
    NSString *str = ProgramName;
    str = str.uppercaseString;
    str = [NSString stringWithFormat:@"\"%@\"",str];
    
    NSString *str2 = [self stringVisibleInRect:self.lblWorkoutName.frame withFont:[UIFont fontWithName:kproximanova_semibold size:26] string:str];
    
    if(![str isEqualToString:str2]){
        
        NSRange range = NSMakeRange(0, str2.length-3);
        str2 =  [str2 substringWithRange:range];
        str2 = [NSString stringWithFormat:@"%@...\"",str2];
    }
    
    UIColor *color;//Sunil for set "" color according to skin
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    switch (skin) {
        case FIRST_TYPE:
        case  SECOND_TYPE:
            color = [UIColor whiteColor];
            break;
            
        case THIRD_TYPE:
            color = kThirdSkin;
            break;
            
        default:
            color = kOldSkin;
            break;
    }
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:str2];
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,1)];
    
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(str2.length-1,1)];
    
    [self.lblWorkoutName setAttributedText:string];
    string = nil;
    
}

//-----------------------------------------------------------------------

-(void)addUnderlineInprogramName {
    
    CGRect expectedLabelSize = [self.lblProgramName.text boundingRectWithSize:self.lblProgramName.frame.size options:NSStringDrawingUsesFontLeading attributes:@{ NSFontAttributeName:[UIFont fontWithName:kproximanova_semibold size:20]} context:nil];
    
    CGFloat xOrigin=0;
    
    switch (self.lblProgramName.textAlignment) {
        case NSTextAlignmentCenter:
            xOrigin=(self.lblProgramName.frame.size.width - expectedLabelSize.size.width)/2;
            break;
        case NSTextAlignmentLeft:
            xOrigin=0;
            break;
        case NSTextAlignmentRight:
            if (expectedLabelSize.size.width>self.lblProgramName.frame.size.width)
            {
                xOrigin=315 ;
            }
            else
            {
               xOrigin=self.lblProgramName.frame.size.width - expectedLabelSize.size.width;
            }
            break;
        default:
            break;
    }
    
    for (UIView *view in self.lblProgramName.subviews){
        if([view isKindOfClass:[UIView class]])
            [view removeFromSuperview];
    }
    
    UIView *viewUnderline=[[UIView alloc] init];
    viewUnderline.backgroundColor = nil;
    if (expectedLabelSize.size.width>self.lblProgramName.frame.size.width)
    {
       viewUnderline.frame= CGRectMake(0,
                   expectedLabelSize.size.height,
                   315,
                   2);
    }
    else
    {
        viewUnderline.frame=CGRectMake(xOrigin-2,
                                       expectedLabelSize.size.height,
                                       expectedLabelSize.size.width+3,
                                       2);
    }
  
    viewUnderline.backgroundColor= [UIColor colorWithRed:61.0/255.0 green:180.0/255.0 blue:170.0/255.0 alpha:1.0];
    [self.lblProgramName addSubview:viewUnderline];
    viewUnderline = nil;
    
}

//-----------------------------------------------------------------------

-(void)setInitialLayout
{
    
    @try {
        
        self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB]
                                                        getBlockDataForClient:self.strClientId
                                                        SheetId:[NSString stringWithFormat:@"%ld",
                                                                 (long)self.currentSheetId]]];
        
        
        NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientId];
        
        if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates]) {
            self.totalBlocks = [self.arrayBlockDates count];
        } else {
            self.totalBlocks = 0;
        }
        
        if(!self.isFromPreviousNext) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromNotes"])
            {
                [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isFromNotes"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates]) {
                    self.currentBlockNo = [self.arrayBlockDates count]-1;
                } else {
                    self.currentBlockNo = 0;
                }
            }
        }
        
        if (clientInfo != nil &&
            ![clientInfo isKindOfClass:[NSNull class]] &&
            [clientInfo count] > 0) {
            self.lblFirstName.text = [NSString stringWithFormat:@"%@ %@", [clientInfo objectForKey:kFirstName],[clientInfo objectForKey:kLastName]];
            //Invoke method for set color of text sunil
            [self setColorForText];
        }
        
        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates]) {
            [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
        }
        [self.lblProgramPurpose setText:nil];
        
        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrSheetsList]){
            [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet -1] valueForKey:@"sPurpose"]];
            [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet - 1] valueForKey:@"sheetName"]];
        }
        
        [self addUnderlineInprogramName];
        
        if (clientInfo != nil &&
            ![clientInfo isKindOfClass:[NSNull class]] &&
            [clientInfo count] > 0) {
            if ([[clientInfo objectForKey:kPday] isEqualToString:@""] || [[clientInfo objectForKey:kPday] isEqualToString:@" "])
            {
                [self.lblPhone setText:NSLocalizedString(@"Not Available", nil)];
                
            }
            else
            {
                [self.lblPhone setText:[clientInfo objectForKey:kPday]];
            }
            
            if (![[clientInfo objectForKey:kEmail1] isEqualToString:@""])
            {
                [self.lblMail setText: [clientInfo objectForKey:kEmail1]];
            }
            else
            {
                [self.lblMail setText: NSLocalizedString(@"Not Available", nil)];
            }
        }
        
        
        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates]) {
            NSDateFormatter  *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:kAppDateFormat];
            if ([[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"dBlockdate"] isEqualToString:@""])
            {
                [self.btnDate setTitle:[NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]] forState:UIControlStateNormal];
            }
            else
            {
                [self.btnDate setTitle:[NSString stringWithFormat:@"%@", [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"dBlockdate"]] forState:UIControlStateNormal];
            }
            formatter = nil;
        }
        
        UIImageView *imgview = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellimage"]];
        [self.headerLabelview addSubview:imgview];
        [self.headerLabelview sendSubviewToBack:imgview];
        
        self.tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        
        UIRectCorner corner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
        [self setRoundRectForView:self.programView withCorner:corner];
        
        NSString * imgPath = [NSString stringWithFormat:@"%@/ClientsProfileImage/ProfileImage%@.png",[FileUtility basePath],self.strClientId];
        UIImage * img = [UIImage imageWithContentsOfFile:imgPath];
        if(img == nil) {
            img =  [UIImage imageNamed:@"ClientPlaceHolderBig"];
        }
        
        self.profileImage.image =  img;
        
        CALayer *layer = [self.profileImage layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:41.0];
        [layer setBorderWidth:1.0];
        [layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [self.tableview reloadData];
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
    }
    @catch (NSException *exception) {
        
    }
    
}

//-----------------------------------------------------------------------

-(void)setRoundRectForView :(UIView *) view withCorner :(UIRectCorner )rectCorner {
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                     byRoundingCorners:rectCorner
                                           cornerRadii:CGSizeMake(8.0,8.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    view.layer.masksToBounds = YES;
}

//-----------------------------------------------------------------------

-(UIImage*)screenShotForView:(UIView*)viewToTakeScreenShot{
    START_METHOD
    CGRect  croppingRect = viewToTakeScreenShot.bounds;
    
    UIGraphicsBeginImageContextWithOptions(croppingRect.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();

    //SUNIL SET IMAGE BACK GROUND COLR
    int intSkin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (intSkin) {
        case FIRST_TYPE:
            SECOND_TYPE:
            [[UIColor colorWithRed:25/255.f green:33/255.f blue:39/255.f alpha:1.0f] set];
            CGContextFillRect(context, croppingRect);
            
            break;

        case THIRD_TYPE:
            [[UIColor colorWithRed:247/255.f green:245/255.f blue:242/255.f alpha:1.0f] set];
            CGContextFillRect(context, croppingRect);
            
            break;
        default:
            [[UIColor colorWithRed:191/255.f green:196/255.f blue:200/255.f alpha:1.0f] set];
            CGContextFillRect(context, croppingRect);
            
            break;
    }
    
    
    if (context == NULL) return nil;
    CGContextTranslateCTM(context, -croppingRect.origin.x, - croppingRect.origin.y);
    
    [viewToTakeScreenShot layoutIfNeeded];
    [viewToTakeScreenShot.layer renderInContext:context];
    
    UIImage *screenshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenshotImage;
    
}

//-----------------------------------------------------------------------

-(void)takeScreenShot{
    
    viewheaderView.backgroundColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    UIImage *imageScreenShort =  [self screenShotAtTableViewAllIndexPaths];
    viewheaderView.backgroundColor = [UIColor whiteColor];
    [self.delegate openMailForScreenShotImage:imageScreenShort];
}

//-----------------------------------------------------------------------
- (void) takeScreenShotAndShareToFB {
    
    viewheaderView.backgroundColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    UIImage *imageScreenShort =  [self screenShotAtTableViewAllIndexPaths];
    viewheaderView.backgroundColor = [UIColor whiteColor];
    [self.delegate openFbForScreenShotImage:imageScreenShort];
    
}


//-----------------------------------------------------------------------

- (UIImage *)screenShotAtTableViewAllIndexPaths
{
    NSMutableArray *screenshots = [NSMutableArray array];
    
    UIImage *topViewScreenshot = [self screenShotForView:viewTopToTakeScreenShot];
    if (topViewScreenshot) [screenshots addObject:topViewScreenshot];
    
    NSInteger numberOfrows = [self.tableview numberOfRowsInSection:0];
    
    for (int row=0; row<numberOfrows; row++) {
        NSIndexPath *cellIndexPath = [NSIndexPath indexPathForRow:row inSection:0];
        UIImage *cellScreenshot = [self screenshotOfCellAtIndexPath:cellIndexPath];
        if (cellScreenshot) [screenshots addObject:cellScreenshot];
    }
    if (numberOfrows == 0) {
        UIImage *lblViewScreenshot = [self screenShotForView:self.lblInstruction];
        if (lblViewScreenshot) [screenshots addObject:lblViewScreenshot];
    }
    
    return [self verticalImageFromArray:screenshots];
}

//-----------------------------------------------------------------------

- (CGSize)verticalAppendedTotalImageSizeFromImagesArray:(NSArray *)imagesArray
{
    CGSize totalSize = CGSizeZero;
    for (UIImage *im in imagesArray) {
        CGSize imSize = [im size];
        totalSize.height += imSize.height;
        totalSize.width = MAX(totalSize.width, imSize.width);
    }
    return totalSize;
}

//-----------------------------------------------------------------------

-(UIImage *)verticalImageFromArray:(NSArray *)imagesArray
{
    UIImage *unifiedImage = nil;
    CGSize totalImageSize = [self verticalAppendedTotalImageSizeFromImagesArray:imagesArray];
    UIGraphicsBeginImageContextWithOptions(totalImageSize, NO, 0.f);
    int imageOffsetFactor = 0;
    for (UIImage *img in imagesArray) {
        [img drawAtPoint:CGPointMake(0, imageOffsetFactor)];
        imageOffsetFactor += img.size.height;
    }
    
    unifiedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    
    MESSAGE(@"SET COLOR --->");
    UIGraphicsEndImageContext();
    return unifiedImage;
}

//-----------------------------------------------------------------------

- (UIImage *)screenshotOfCellAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *cellScreenshot = nil;
    
    CGPoint currTableViewOffset = self.tableview.contentOffset;
    
    [self.tableview scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:NO];
    
    // Take the screenshot
    UIView *viewCell = [self.tableview cellForRowAtIndexPath:indexPath];
    
    cellScreenshot = [self screenShotForView:viewCell];
    
    // scroll back to the original offset
    [self.tableview setContentOffset:currTableViewOffset animated:NO];
    
    return cellScreenshot;
}

//---------------------------------------------------------------------------------------------------------------

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    [self checkForOptionsViewAndHide];
}

//---------------------------------------------------------------------------------------------------------------

-(void)setLastVisitedProgramScreen {
    START_METHOD
    NSString *editedScreenInfo = [[DataManager initDB] getLastEditedScreenForClient:self.strClientId];
    
    editedScreenInfo = [[AppDelegate sharedInstance] checkForNull:editedScreenInfo];
    if (![editedScreenInfo isEqualToString:@""]) {
         NSArray *ary = [editedScreenInfo componentsSeparatedByString:@"/##/"];
        
        if([[AppDelegate sharedInstance] isNotEmptyArray:ary]) {
            NSString *programPage = [ary objectAtIndex:0];
            programPage = [[AppDelegate sharedInstance] checkForNull:programPage];
            if(![programPage isEqualToString:@""] && [programPage isEqualToString:@"2"]) {
                NSString *str = [ary objectAtIndex:1];
                NSArray *ary1 = [str componentsSeparatedByString:@","];
                
                if([[AppDelegate sharedInstance] isNotEmptyArray:ary1] && [ary1 count] == 2) {
                    currentSheet = [[ary1 objectAtIndex:0] integerValue];
                    currentBlockNo = [[ary1 objectAtIndex:1] integerValue];
                    
                    self.isFromPreviousNext = TRUE;
                    if(currentSheet > 0) {
                        
                        if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrSheetsList]) {
                            self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue];
                        }
                        
                        self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                        
                        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates] && self.currentBlockNo <= [self.arrayBlockDates count]) {
                            _blockSheetNo = [[[ self.arrayBlockDates objectAtIndex:self.currentBlockNo]objectForKey:@"nBlockNo"]intValue];
                            self.isFromPreviousNext = TRUE;
                            [self fillArrayWithData];
                            [self setInitialLayout];
                            [self.tableview reloadData];
                        } else {
                            self.isFromPreviousNext = FALSE;
                            currentSheet = [self.arrSheetsList count];
                            self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
                            
                            self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                            
                            if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates]) {
                                _blockSheetNo = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue];
                                [self fillArrayWithData];
                                [self setInitialLayout];
                                [self.tableview reloadData];
                            }
                        }
                    }
                }
            } else {
            }
            
        } else {
            NSArray *ary = @[@"2",@"1,0"];
            NSString *programPage = [ary objectAtIndex:0];
            programPage = [[AppDelegate sharedInstance] checkForNull:programPage];
            if(![programPage isEqualToString:@""] && [programPage isEqualToString:@"2"]) {
                NSString *str = [ary objectAtIndex:1];
                NSArray *ary1 = [str componentsSeparatedByString:@","];
                
                if([[AppDelegate sharedInstance] isNotEmptyArray:ary1] && [ary1 count] == 2) {
                    currentSheet = [[ary1 objectAtIndex:0] integerValue];
                    currentBlockNo = [[ary1 objectAtIndex:1] integerValue];
                    
                    self.isFromPreviousNext = TRUE;
                    if(currentSheet > 0) {
                        
                        if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrSheetsList]) {
                            self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue];
                        }
                        
                        self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                        
                        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates] && self.currentBlockNo <= [self.arrayBlockDates count]) {
                            _blockSheetNo = [[[ self.arrayBlockDates objectAtIndex:self.currentBlockNo]objectForKey:@"nBlockNo"]intValue];
                            self.isFromPreviousNext = TRUE;
                            [self fillArrayWithData];
                            [self setInitialLayout];
                            [self.tableview reloadData];
                        } else {
                            self.isFromPreviousNext = FALSE;
                            currentSheet = [self.arrSheetsList count];

                            self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
                            
                            self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                            
                            if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrayBlockDates]) {
                                _blockSheetNo = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue];
                                [self fillArrayWithData];
                                [self setInitialLayout];
                                [self.tableview reloadData];
                            }
                        }
                    }
                }
            } else {
            }
        }

    }
    
    END_METHOD
}

//-----------------------------------------------------------------------
- (void) localizeControls {
    
    lblDate.text = NSLocalizedString(lblDate.text, nil);
    [self.btnDropDown setTitle:NSLocalizedString(self.btnDropDown.titleLabel.text, nil) forState:0];
    lblPrevWorkout.text = NSLocalizedString(lblPrevWorkout.text, nil);

    [btnPrevious setTitle:NSLocalizedString(btnPrevious.titleLabel.text, nil) forState:0];
    
    lblWorkout.text = NSLocalizedString(lblWorkout.text, nil);
    [btnNext setTitle:NSLocalizedString(btnNext.titleLabel.text, nil) forState:0];
    lblCopy.text = NSLocalizedString(lblCopy.text, nil);
    lblExercise.text = NSLocalizedString(lblExercise.text, nil);
    lblWeight.text = NSLocalizedString(lblWeight.text, nil);
    lblSets.text = NSLocalizedString(lblSets.text, nil);
    lblReps.text = NSLocalizedString(lblReps.text, nil);
    lblRestTime.text = NSLocalizedString(lblRestTime.text, nil);
    lblDelete.text = NSLocalizedString(lblDelete.text, nil);
    self.lblInstruction.text = NSLocalizedString(self.lblInstruction.text, nil);
    
    [self.btnAddNewLine setTitle:NSLocalizedString(self.btnAddNewLine.titleLabel.text, nil) forState:0];
    [self.btnAddWorkout setTitle:NSLocalizedString(self.btnAddWorkout.titleLabel.text, nil) forState:0];
   [self.btnNotes setTitle:NSLocalizedString(self.btnNotes.titleLabel.text, nil) forState:0];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.btnNotes.titleLabel.text font:self.btnNotes.titleLabel.font height:self.btnNotes.titleLabel.frame.size.height];
    
    [self.btnNotes setFrame:CGRectMake(self.btnNotes.frame.origin.x, self.btnNotes.frame.origin.y, rect.size.width + 30, self.btnNotes.frame.size.height)];
    
    [self.btnViewAll setTitle:NSLocalizedString(self.btnViewAll.titleLabel.text, nil) forState:0];
    self.lblPhone.text = NSLocalizedString(self.lblPhone.text, nil);
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : Storekit Delegate Methods 

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[NSNotificationCenter defaultCenter] postNotificationName:kupdateVersionNotification object:nil];
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseFailed:(NSString*)productId error:(NSString *)error {
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction failed, Please try again", nil)];
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseCancelled:(NSString*)productId {
    
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction Cancelled, Please try again", nil)];
    
    
}

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


////-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
    
    
    mainView.frame =CGRectMake(0,0 ,1000 , 610);
    self.programView.frame = CGRectMake(0, 0,922,560);
//    [self.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg_iPad"]]];
   [self setInitialLayout];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnAddNewLine.titleLabel.text, nil) font:self.btnAddNewLine.titleLabel.font height:self.btnAddNewLine.titleLabel.frame.size.height];
    
    self.btnAddNewLine.frame = CGRectMake(10,565 ,rect.size.width + 30 , 35);
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnAddWorkout.titleLabel.text, nil) font:self.btnAddWorkout.titleLabel.font height:self.btnAddWorkout.titleLabel.frame.size.height];
    
    self.btnAddWorkout.frame =CGRectMake(763,565 ,rect.size.width + 30 , 35);
    self.viewWithTbl.frame =CGRectMake(0,286 ,922 , 280);

    self.profileImage.frame = CGRectMake(430,20,83,83);
    self.lblProgramName.frame = CGRectMake(600,25,315,26);
    self.btnEditProgram.frame = CGRectMake(600,25,315,26);

    self.lblFirstName.frame = CGRectMake(0,112,915,28);
    self.btnViewAll.frame = CGRectMake(757,176,140,30);
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnDropDown.titleLabel.text, nil) font:self.btnDropDown.titleLabel.font height:self.btnDropDown.titleLabel.frame.size.height];
    
    self.btnDropDown.frame = CGRectMake(380,176,rect.size.width + 50,30);
    lblDate.frame = CGRectMake(779,57,53,21);
    self.btnDate.frame = CGRectMake(837,57,82,20);
    btnNext.frame = CGRectMake(815,10,108,24);
    lblWorkout.frame = CGRectMake(824,20,50,21);
    self.prevNextView.frame = CGRectMake(0,208,915,40);
    self.lblProgramPurpose.frame = CGRectMake(0,138,915,26);
    self.btnEditWorkout.frame = CGRectMake(352,5,396,30);
    self.lblWorkoutName.frame = CGRectMake(270,213,390,26);
    
    lblCopy.frame = CGRectMake(6,8 ,45 , 21);
    imgvFirstLine.frame = CGRectMake(42,0 ,1 , 32);
    lblExercise.frame = CGRectMake(50,8 ,490 , 21);
    imgvSecondLine.frame = CGRectMake(494,0 ,1 , 32);
    lblWeight.frame = CGRectMake(498,8 ,95 , 21);
    imgvThirdLine.frame = CGRectMake(591,0 ,1 , 32);
    lblSets.frame = CGRectMake(596,8 ,94 , 21);
    imgvFourthLine.frame = CGRectMake(685,0 ,1 , 32);
    lblReps.frame = CGRectMake(686,8 ,95 , 21);
    imgvFifthLine.frame = CGRectMake(780,0 ,1 , 32);
    lblRestTime.frame = CGRectMake(781,8 ,82 , 21);
    imgvSixthLine.frame = CGRectMake(866,0 ,1 , 32);
    lblDelete.frame = CGRectMake(871,8 ,45 , 21);
    viewheaderView.frame =CGRectMake(3,254 ,920 , 32);
    self.tableview.frame =CGRectMake(0,1 ,920 , 280);
    [self.lblInstruction setFrame:CGRectMake(0, 280, 728 + 200, 443 - 165)];
    viewTopToTakeScreenShot.frame =CGRectMake(0,0 ,999 , 287);

    [self.tableview reloadData];
    
  
  
}

//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
 
    mainView.frame =CGRectMake(0,0 ,730 , 850);

    self.programView.frame = CGRectMake(0, 0,730,800);
    [self.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg"]]];
   
    [self setInitialLayout];
    self.profileImage.frame = CGRectMake(322,20,83,83);
    self.lblProgramName.frame = CGRectMake(399,25,315,26);
    self.btnEditProgram.frame = CGRectMake(399,25,315,26);

    self.lblFirstName.frame = CGRectMake(0,112,730,28);
    self.btnViewAll.frame = CGRectMake(572,176,140,30);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnDropDown.titleLabel.text, nil) font:self.btnDropDown.titleLabel.font height:self.btnDropDown.titleLabel.frame.size.height];
    
    self.btnDropDown.frame = CGRectMake(290,176,rect.size.width + 50,30);
    lblDate.frame = CGRectMake(579,57,53,21);
    self.btnDate.frame = CGRectMake(637,57,82,20);
    btnNext.frame = CGRectMake(630,10,108,24);
    lblWorkout.frame = CGRectMake(639,20,50,21);
    self.prevNextView.frame = CGRectMake(0,208,730,40);
    self.lblProgramPurpose.frame = CGRectMake(0,138,730,26);
    self.btnEditWorkout.frame = CGRectMake(167,5,396,30);
    self.lblWorkoutName.frame = CGRectMake(167,213,390,26);
    
    lblCopy.frame = CGRectMake(8,8 ,45 , 21);
    imgvFirstLine.frame = CGRectMake(45,0 ,1 , 32);
    lblExercise.frame = CGRectMake(50,8 ,250 , 21);
    imgvSecondLine.frame = CGRectMake(307,0 ,1 , 32);
    lblWeight.frame = CGRectMake(307,8 ,95 , 21);
    imgvThirdLine.frame = CGRectMake(402,0 ,1 , 32);
    lblSets.frame = CGRectMake(404,8 ,94 , 21);
    imgvFourthLine.frame = CGRectMake(498,0 ,1 , 32);
    lblReps.frame = CGRectMake(498,8 ,95 , 21);
    imgvFifthLine.frame = CGRectMake(592,0 ,1 , 32);
    lblRestTime.frame = CGRectMake(592,8 ,76 , 21);
    imgvSixthLine.frame = CGRectMake(667,0 ,1 , 32);
    lblDelete.frame = CGRectMake(679,8 ,45 , 21);
    viewheaderView.frame =CGRectMake(3,254 ,725 , 32);
    
    self.tableview.frame =CGRectMake(0,1 ,730 , 520);
    [self.lblInstruction setFrame:CGRectMake(0, 280, 728, 443)];
    [self.tableview reloadData];
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnAddNewLine.titleLabel.text, nil) font:self.btnAddNewLine.titleLabel.font height:self.btnAddNewLine.titleLabel.frame.size.height];
    
    self.btnAddNewLine.frame =CGRectMake(25,815 ,rect.size.width + 30 , 35);
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.btnAddWorkout.titleLabel.text, nil) font:self.btnAddWorkout.titleLabel.font height:self.btnAddWorkout.titleLabel.frame.size.height];

    self.btnAddWorkout.frame =CGRectMake(553,815 ,rect.size.width + 30 , 35);
    self.viewWithTbl.frame =CGRectMake(0,286 ,730 , 520);
viewTopToTakeScreenShot.frame =CGRectMake(0,0 ,729 , 287);
    
}



//-----------------------------------------------------------------------

#pragma mark - Memory management Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self localizeControls];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
    
    
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //Get Upgarde Status
    strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    MESSAGE(@"viewDidLoad--> strUpgradeStatus: %@",strUpgradeStatus);
    
    
  }
//-----------------------------------------------------------------------


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

//-----------------------------------------------------------------------
-(void)viewWillAppear:(BOOL)animated
{START_METHOD
    
    [super viewWillAppear:animated];
    if (self.strClientId) {
        [[DataManager initDB] addNewFieldsToTablesIfnotAddedForClientId:self.strClientId];
    }
    
    isFromPasteNewLineClick = NO;
    [self.btnBack setHidden:YES];
    [self prefersStatusBarHidden];
    
    _arrayProgramData = [[NSMutableArray alloc] init];
    _arrSheetsList = [[NSMutableArray alloc] init];
    copyWorkoutArray = [[NSMutableArray alloc] init];
    copyLineArray  = [[NSMutableArray alloc] init];

    NSArray *alredyCopiedLines = (NSArray*)[[NSUserDefaults standardUserDefaults] valueForKey:kIsCopyLine];
    if (alredyCopiedLines.count) {
        [copyLineArray addObjectsFromArray:alredyCopiedLines];
    }
    if (copyLineArray.count) {
        self.btnAddNewLine.tag = 1;
        [self.btnAddNewLine setTitle:NSLocalizedString(@"Paste New Exercise", nil) forState:0];
        
    }
    
    if (!self.strClientId) {
        _blockSheetNo = [[[NSUserDefaults standardUserDefaults] valueForKey:@"blockSheetNo"] integerValue];
        self.strClientId = [[NSUserDefaults standardUserDefaults] valueForKey:@"clientID"] ;
        self.currentSheetId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"currentSheetId"] integerValue];
        self.currentSheet = [[[NSUserDefaults standardUserDefaults] valueForKey:@"currentSheetId"] integerValue]; 
        [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
    } else  {
    
        rowValueForExercise = 0;
        [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
       self.isFromPreviousNext = FALSE;
        if ([self.arrSheetsList count]!=0) {
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromNotes"])
            {
                MESSAGE(@"isFromNotes->");
                self.isFromPreviousNext = TRUE;
                currentSheet = [self.selectedSheet intValue];
                self.currentSheetId = [self.selectedSheet intValue];
                
                self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                
                
                for (int i = 0 ; i <[self.arrayBlockDates count]; i++)
                {
                    MESSAGE(@"[self.arrayBlockDates objectAtIndex:i]-> %@",[self.arrayBlockDates objectAtIndex:i]);
                    
                    if (self.blockSheetNo == [[[self.arrayBlockDates objectAtIndex:i] valueForKey:@"nBlockNo"] integerValue] ) {
                        
                        MESSAGE(@"[self.arrayBlockDates objectAtIndex:i]->value of i: %d",i);

                        self.currentBlockNo = i;
                    }
                }

            }

            else  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromViewAllTable"])
            {
                MESSAGE(@"isFromViewAllTable->: %ld",(long)currentSheet);
                self.isFromPreviousNext = TRUE;
                currentSheet = [self.selectedSheet intValue];
                
                self.currentSheetId = [self.selectedSheet intValue];
                
                self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
               
                
                _blockSheetNo = self.blockSheetNo;
                self.currentBlockNo = self.currentBlockNo;
                
                 currentSheet = [self.strSelectedSheet intValue];
                MESSAGE(@"isFromViewAllTable->next: : %ld",(long)currentSheet);

                
            }
            else
            {
                                MESSAGE(@"else isFromViewAllTable->");
                
                
                
                
                currentSheet = [self.arrSheetsList count];
                self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
                
                self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                _blockSheetNo = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue];
                
                
                //--->
                MESSAGE(@"fillArrayWithData-> %@ and self.currentBlockNo: %ld",self.arrayBlockDates,(long)self.currentBlockNo);
                
                //15 sept 2016 when came from par-Q TO program
                self.currentBlockNo = [self.arrayBlockDates count]-1;
                
                ///<-----
                
            }
        }
       
    }
   
    [self fillArrayWithData];
    
    
    rowValueForExercise = [[[self.arrayProgramData lastObject] valueForKey:kRowNo] intValue];
    
    [self setInitialLayout];
    
     if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromViewAllTable"])
     {
         [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isFromViewAllTable"];
         [[NSUserDefaults standardUserDefaults] synchronize];

     } else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromNotes"]) {
         
         [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isFromNotes"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
     } else {
         
         [self setLastVisitedProgramScreen];

     }
    
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        self.setAsLandscape = TRUE;
        [self setUpLandscapeOrientation];
    } else {
        self.setAsLandscape = FALSE;
        [self setupPortraitOrientation];
    }

    END_METHOD
    
}

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Sunil
-(void)setColorForText{
    START_METHOD
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
           
             self.lblFirstName.textColor =kFirstSkin;
            //Invoke method for set color
            [self setColorText:[UIColor whiteColor]];
        
            [self.btnPhone setImage:[UIImage imageNamed:@"green_phone.png"] forState:UIControlStateNormal];
            
            [self.btnMail setImage:[UIImage imageNamed:@"green_email.png"] forState:UIControlStateNormal];
            
            [self.btnViewAll setBackgroundImage:[UIImage imageNamed:@"green_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnDropDown setBackgroundImage:[UIImage imageNamed:@"green_select_button.png"] forState:UIControlStateNormal];
            
            
            [self.btnNotes setBackgroundImage:[UIImage imageNamed:@"green_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnAddNewLine setBackgroundImage:[UIImage imageNamed:@"green_add_new_line_button.png"] forState:UIControlStateNormal];
            
             [self.btnAddWorkout setBackgroundImage:[UIImage imageNamed:@"green_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [btnPrevious setImage:[UIImage imageNamed:@"prevbtn_green.png"] forState:UIControlStateNormal];
            [btnNext setImage:[UIImage imageNamed:@"nextbtn_green.png"] forState:UIControlStateNormal];
            
            break;
            
        case SECOND_TYPE:
            
            self.lblFirstName.textColor =ksecondSkin;
            //Invoke method for set color
            [self setColorText:[UIColor whiteColor]];
            
            [self.btnPhone setImage:[UIImage imageNamed:@"red_phone.png"] forState:UIControlStateNormal];
            
            [self.btnMail setImage:[UIImage imageNamed:@"red_email.png"] forState:UIControlStateNormal];
            
            [self.btnViewAll setBackgroundImage:[UIImage imageNamed:@"red_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnDropDown setBackgroundImage:[UIImage imageNamed:@"red_select_button.png"] forState:UIControlStateNormal];
            
            [self.btnNotes setBackgroundImage:[UIImage imageNamed:@"red_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnAddNewLine setBackgroundImage:[UIImage imageNamed:@"red_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnAddWorkout setBackgroundImage:[UIImage imageNamed:@"red_add_new_line_button.png"] forState:UIControlStateNormal];
            
             [btnPrevious setImage:[UIImage imageNamed:@"prevbtn_red.png"] forState:UIControlStateNormal];
            [btnNext setImage:[UIImage imageNamed:@"nextbtn_red.png"] forState:UIControlStateNormal];
            
            break;
            
        case THIRD_TYPE:
           
            
            self.lblFirstName.textColor =kThirdSkin;
            //Invoke method for set color
            [self setColorText:[UIColor grayColor]];
            
            [self.btnPhone setImage:[UIImage imageNamed:@"pink_phone.png"] forState:UIControlStateNormal];
            
            [self.btnMail setImage:[UIImage imageNamed:@"pink_email.png"] forState:UIControlStateNormal];
            
            [self.btnViewAll setBackgroundImage:[UIImage imageNamed:@"pink_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnDropDown setBackgroundImage:[UIImage imageNamed:@"pink_add_new_line_button1.png"] forState:UIControlStateNormal];
            
            [self.btnNotes setBackgroundImage:[UIImage imageNamed:@"pink_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnAddNewLine setBackgroundImage:[UIImage imageNamed:@"pink_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [self.btnAddWorkout setBackgroundImage:[UIImage imageNamed:@"pink_add_new_line_button.png"] forState:UIControlStateNormal];
            
            
             [btnPrevious setImage:[UIImage imageNamed:@"prevbtn_pink.png"] forState:UIControlStateNormal];
            
            [btnNext setImage:[UIImage imageNamed:@"nextbtn_pink.png"] forState:UIControlStateNormal];
            
            break;
    
        default:
            
            self.lblFirstName.textColor = kOldSkin;
            //Invoke method for set color
            [self setColorText:[UIColor grayColor]];
            
            [self.btnPhone setImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
            
           
            [self.btnMail setImage:[UIImage imageNamed:@"email.png"] forState:UIControlStateNormal];
            
            [self.btnViewAll setBackgroundImage:[UIImage imageNamed:@"buttonNewNote.png"] forState:UIControlStateNormal];
            
            [self.btnDropDown setBackgroundImage:[UIImage imageNamed:@"selectedbtn.png"] forState:UIControlStateNormal];
            
            [self.btnNotes setBackgroundImage:[UIImage imageNamed:@"buttonNewNote.png"] forState:UIControlStateNormal];
            
            
            [self.btnAddNewLine setBackgroundImage:[UIImage imageNamed:@"buttonNewNote.png"] forState:UIControlStateNormal];
            
            [self.btnAddWorkout setBackgroundImage:[UIImage imageNamed:@"buttonNewNote.png"] forState:UIControlStateNormal];
            
            
            [btnPrevious setImage:[UIImage imageNamed:@"prevbtn"] forState:UIControlStateNormal];
            [btnNext setImage:[UIImage imageNamed:@"nextbtn"] forState:UIControlStateNormal];
           
            
            break;
    }
    
    END_METHOD
}

//Sunil
-(void)setColorText:(UIColor *)color{
    
    START_METHOD
   
    self.lblInstruction.textColor = color;
    self.lblMail.textColor = color;
    
    self.lblPhone.textColor =color;
    self.lblProgramName.textColor = color;
    self.lblProgramPurpose.textColor = color;
    
    self.lblWorkoutName.textColor =color;
    
    lblDate.textColor = color;
    
    //No Need
    
    
//    lblCopy.textColor = color;
//    lblDelete.textColor = color;
//    lblExercise.textColor = color;
//    
//    lblPrevWorkout.textColor = color;
//    lblReps.textColor = color;
//    lblRestTime.textColor = color;
//    lblSets.textColor = color;
//    
//    

    
    [btnPrevious setTitleColor:color forState:UIControlStateNormal];
    [btnNext setTitleColor:color forState:UIControlStateNormal];
    [self.btnDate setTitleColor:color forState:UIControlStateNormal];
    lblPrevWorkout.textColor = color;
    lblWorkout.textColor    = color;
    

    END_METHOD
}

-(void)setFontColor:(ProgramCustomCell *)cell{
    START_METHOD

    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            cell.lblWeight.textColor = [UIColor whiteColor];
            cell.lblExercise.textColor = [UIColor whiteColor];
            cell.lblSet.textColor = [UIColor whiteColor];
            cell.lblRestTime.textColor = [UIColor whiteColor];
            cell.lblReps.textColor = [UIColor whiteColor];
            [cell.btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
            [cell.btnEditLine setImage:[UIImage imageNamed:@"gray_edit1.png"] forState:UIControlStateNormal];
            break;
            
        case SECOND_TYPE:
            cell.lblExercise.textColor = [UIColor whiteColor];
            cell.lblWeight.textColor = [UIColor whiteColor];
            cell.lblSet.textColor = [UIColor whiteColor];
            cell.lblRestTime.textColor = [UIColor whiteColor];
            cell.lblReps.textColor = [UIColor whiteColor];
             [cell.btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
              [cell.btnEditLine setImage:[UIImage imageNamed:@"gray_edit1.png"] forState:UIControlStateNormal];
            break;
            
        case THIRD_TYPE:
            cell.lblExercise.textColor = [UIColor grayColor];
            cell.lblWeight.textColor = [UIColor grayColor];
            cell.lblSet.textColor = [UIColor grayColor];
            cell.lblRestTime.textColor = [UIColor grayColor];
            cell.lblReps.textColor = [UIColor grayColor];
            [cell.btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
            [cell.btnEditLine setImage:[UIImage imageNamed:@"gray_edit1.png"] forState:UIControlStateNormal];
            break;
   
            
        default:
            cell.lblExercise.textColor = [UIColor grayColor];
            cell.lblWeight.textColor = [UIColor grayColor];
            cell.lblSet.textColor = [UIColor grayColor];
            cell.lblRestTime.textColor = [UIColor grayColor];
            cell.lblReps.textColor = [UIColor grayColor];
            [cell.btnDelete setImage:[UIImage imageNamed:@"notesDeleteBtn.png"] forState:UIControlStateNormal];
            break;
    }
    
    END_METHOD
}


//TODO: SERVER WORK

//Method for make request for Add new Program
-(void)postRequestForAddNewProgram:(NSString *) sheetName ForClient:(NSString *)cId ProgramPurpose:(NSString *)programPurpose withWorkoutName:(NSString *)workoutName ForBlockNo:(NSString *)blockNO andSheetId:(NSString *)sheetId{

    START_METHOD
    
    //Crete Dictionary for Program
    NSMutableDictionary *dictProgram = [[NSMutableDictionary alloc]init];
    
    [dictProgram setValue:sheetName forKey:ksheetName];
    [dictProgram setValue:cId forKey:kClientId];
    [dictProgram setValue:programPurpose forKey:ksProgramPurpose];
    [dictProgram setValue:workoutName forKey:ksheetName];
    [dictProgram setValue:blockNO forKey:kBlockNo];
    [dictProgram setObject:sheetId forKey:ksheetID];
    
    
    //Create dict for Post to server
    NSMutableDictionary *dictForPost    =   [[NSMutableDictionary alloc]init];
    [dictForPost setValue:dictProgram forKey:@"programs"];
    
    
    //TODO: Write Text in text file
    
    //Carete objet of TextFileManager for write text
    TextFileManager *obj    =   [TextFileManager getSharedInstance];
    
    //Invoke method for write JSON in text file
    NSString *strFilePath   =   [obj writeTextToFile:dictForPost];
    
    //Invoke method For request for Post data
    [commonUtility postRequest:strFilePath];
    
    END_METHOD
}


//Method for make request for Update Program
-(void)postRequestForProgram:(NSMutableDictionary *)dictProgram{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params addProgram=: %@ and Url : %@",dictProgram,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictProgram
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictProgramTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData add program fi=rst : dictProgram: %@",dictProgramTemp);
                                   
                                   NSString *strAction =   [dictProgramTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addProgram"]){
                                       
                                       //Invoke method for save in loacl db
                                       [self addPrograminLocal:dictProgramTemp];
                                       
                                   }else{
                                       
                                       [self updateProgramInLocal:dictProgramTemp];
                                       
                                   }
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:STATUS_LIST]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:STATUS_LIST];
                                           if(objDict && [[objDict objectForKey:@"message_id"] isEqualToString:@"7"]){
                                               
                                               //Remove Program POpup
                                               [self.delegate removePopOverForAddProgram];
                                               
                                               //Invoke method for Show alert for Free user
                                               [self checkFreeUserAccesForAddWorkout:[dictError objectForKey:POPUPTEXT]];
                                               
                                           }
                                       }
                                       else{
                                       
                                       
                                       
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}

-(void)addPrograminLocal:(NSDictionary *)dictProgram{
    
    START_METHOD
    MESSAGE(@"addPrograminLocal-> dictProgram: %@",dictProgram);
    
    _blockSheetNo =1;
    int i = [[DataManager initDB] insertProgram:dictProgram];
 
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:kAppDateFormat];
    NSString *result = [formater stringFromDate:[NSDate date]];
    
    [[DataManager initDB]insertSheetBlocksForSheet:[dictProgram objectForKey:kSheetId] date:result blockNo:[dictProgram objectForKey:kBlockNo] clientId:[dictProgram objectForKey:kClientId] blockTitle:[dictProgram objectForKey:kBlockTitle] blockNotes:@"" andBlockId:[dictProgram objectForKey:kBlockNo]];
    
    
    self.strProgramSheetName = [dictProgram objectForKey:ksheetName];
    [self.delegate removePopOverForAddProgram];
    
    
    if (i == 0) {
        NSString * sheetId = [[DataManager initDB] getsheetId:[dictProgram objectForKey:ksheetName] forClient:self.strClientId];
        MESSAGE(@"SheetId->:%@",sheetId);
        
        self.currentSheet = [sheetId intValue];
        
        [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
        self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
        
        
        self.currentSheet = [self.arrSheetsList count];
        MESSAGE(@"self.arrayProgramData: %@",self.arrayProgramData);
        
        No_OfSheet = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
        
        self.isFromPreviousNext = FALSE;
        
        [self fillArrayWithData];
        

        
        //15 Sept 2016 Sunil for add new program block sheet -->
        
        _blockSheetNo   = self.arrayBlockDates.count;
        //<--------
        
        
        MESSAGE(@"self.arrayProgramData: %@ \n\narrSheetsList:%@ \n\nand_blockSheetNo: %ld  \n\n\n and currentSheet: %ld",self.arrayBlockDates,_arrSheetsList,(long)_blockSheetNo,(long)currentSheet);
        
        
        [self setLabelValue:[[self.arrayBlockDates objectAtIndex:_blockSheetNo-1] valueForKey:@"sBlockTitle"]];
        [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sPurpose"]];
        [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetName"]];

        [self addUnderlineInprogramName];
    }
    
    [self.tableview reloadData];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    if ([self.arrayProgramData count]==0)
    {
        self.lblInstruction.hidden = NO;
    }
    else
    {
        self.lblInstruction.hidden = YES;
        
    }
}


-(void)updateProgramInLocal:(NSDictionary *)dictPropgram{
    START_METHOD
   
    NSInteger  success = [[DataManager initDB] updateSheetName:[dictPropgram objectForKey:ksheetName] forClient:self.strClientId sheetId:[dictPropgram objectForKey:kSheetId]];
    
    [self.lblProgramName setText:[dictPropgram objectForKey:ksheetName]];
    [self addUnderlineInprogramName];
    
    [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
    if (success==0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Sheet name updated successfully.", nil),kAppName);
    }
    
    END_METHOD
}
//Method for post new exercise
-(void)postRequestForExercise:(NSMutableDictionary *)dictExercise {
    START_METHOD
    
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params postRequestForExercise=: %@ and Url : %@",dictExercise,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictExercise
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictExerciseTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData add postRequestForExercise fi=rst : addExercise: %@",dictExerciseTemp);
                                   
                                   NSString *strAction =   [dictExerciseTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addExercise"]){
                                       
                                       //Set ID
                                       [dictExercise setValue:[dictExerciseTemp objectForKey:PROGRAM_EXERCISE_ID] forKey:kBlockDataId];
                                       
                                       //Invoke method for save in loacl db
                                       [self addExerciseInLocal:dictExercise];
                                       
                                       
                                   }else   {//For Update
                                       
                                       [self updateExercise:dictExercise];
                                       
                                   }
                                   
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}

-(void)addExerciseInLocal:(NSDictionary *)dictExercise{
    
    
    NSInteger success = [[DataManager initDB] insertProgramData:dictExercise forClient:self.strClientId];
   
 
    //--->For COPY PASTE
    if (copyLineArray.count) {
        [copyLineArray removeAllObjects];
    }
    self.btnAddNewLine.tag = 0;
    [self.btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:UIControlStateNormal];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:nil forKey:kIsCopyLine];
    //For COPY PASTE <-------
    
    

    
    //Block For Paste Reload view After Paste entire
    if ([dictExercise objectForKey:@"reloadTeable"]){
        
        
        //Block For Paste Reload view After Paste entire
        if ([[dictExercise objectForKey:@"reloadTeable"] isEqualToString:@"YES"]){
            //Invoke Method At time Copy Paste Entire Program
            [self reloadViewAfterPasterEntireProgram];
        }
        
        
    }else{
        //TODO: SUNIL-> ADD EXERCISE
        dictExercise = nil;
        [self fillArrayWithData];
        [self.tableview reloadData];
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
    }
}


-(void)updateExercise:(NSDictionary *)dictExercise{
    START_METHOD
    [[DataManager initDB] updateProgramData:dictExercise forClient:self.strClientId];
        
        
        [self fillArrayWithData];
        
        NSInteger rowIdReload = [[dictExercise valueForKey:@"RowId"]integerValue];
        [self.tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIdReload inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    
    END_METHOD
}

//Method for post request for Add new workout
-(void)postRequestForwWorkout:(NSMutableDictionary *)dictWorkout{
    START_METHOD
    
   
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictWorkout=: %@ and Url : %@",dictWorkout,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictWorkout
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictWorkoutTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send Dictonary : %@ \n and response from server dictWorkout: %@",dictWorkout,dictWorkoutTemp);
                                   
                                   NSString *strAction =   [dictWorkoutTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addWorkout"]){
                                       
                                       //Invoke method for save in loacl db
                                       [self addNewWorkoutInLocal:dictWorkoutTemp];
                                       
                                   }else{
                                       
                                       [self updateWorkout:dictWorkoutTemp];
                                       
                                   }
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:STATUS_LIST]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:STATUS_LIST];
                                           if(objDict && [[objDict objectForKey:@"message_id"] isEqualToString:@"6"]){
                                               //Invoke method for Show alert for Free user
                                               [self checkFreeUserAccesForAddWorkout:[dictError objectForKey:POPUPTEXT]];
                                               
                                           }
                                       }
                                       else{
                                       
                                       
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}

-(void)addNewWorkoutInLocal:(NSDictionary *)dictWorkOuts{
    
    MESSAGE(@"addNewWorkoutInLocal-->dictWorkOuts: %@",dictWorkOuts);
  
    if(![dictWorkOuts objectForKey:@"copyPasteWorkoutEntire"]){//For Add
        
        rowValueForExercise =0;
        
        [[DataManager initDB]insertSheetBlocksForSheet:[dictWorkOuts objectForKey:kSheetId] date:[dictWorkOuts objectForKey:kDate] blockNo:[dictWorkOuts objectForKey:kBlockNo] clientId:self.strClientId blockTitle:[dictWorkOuts objectForKey:ksBlockTitle] blockNotes:@"" andBlockId:[dictWorkOuts objectForKey:kBlockNo]];
        
        
        NSInteger success1 = [[DataManager initDB] updateDate:[self.btnDate titleForState:UIControlStateNormal] blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
   
        
        [self fillArrayWithData];
        [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
        
        [self.tableview reloadData];
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        
        
    }else{//For Paste From Entire Program
        
        [[DataManager initDB]insertSheetBlocksForSheet:[dictWorkOuts objectForKey:kSheetId] date:[dictWorkOuts objectForKey:kDate] blockNo:[dictWorkOuts objectForKey:kBlockNo] clientId:self.strClientId blockTitle:[dictWorkOuts objectForKey:ksBlockTitle] blockNotes:@"" andBlockId:[dictWorkOuts objectForKey:kBlockNo]];
        
        
        NSInteger success1 = [[DataManager initDB] updateDate:[self.btnDate titleForState:UIControlStateNormal] blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
        
    }
    
}


-(void)updateWorkout:(NSDictionary *)dictWorkOuts{

    /*
     "nSheetID": "10614",
     "sBlockTitle": "vika111sh",
     "action": "updateWorkout",
     "nBlockNo": "3215"
     */
    NSInteger success =   [[DataManager initDB] updateBlockName:[dictWorkOuts objectForKey:ksBlockTitle] forClient:self.strClientId sheetId:[dictWorkOuts objectForKey:kSheetId] blockId:[dictWorkOuts objectForKey:kBlockNo]];
    self.isFromPreviousNext = FALSE;
    
    
    MESSAGE(@"self.currentBlockNo--> %ld",self.currentBlockNo);
    
    
    [self fillArrayWithData];
    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];

    
    
//    if (success==0) {
//        DisplayAlertWithTitle(NSLocalizedString(@"Sheet name updated successfully.", nil),kAppName);
//    }
}



//Method for post request for Add new workout
-(void)postRequestForNotes:(NSMutableDictionary *)dictNotes{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictNotes,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictNotes
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictNoteTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData addNotes send Dictonary : %@ \n and response from server addNotes: %@",dictNotes,dictNoteTemp);
                                   
                                   NSString *strAction =   [dictNoteTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addNotes"]){
                                       
                                       //Invoke method for save in loacl db
                                       [self addNewNoteInLocal:dictNoteTemp];
                                       
                                   }
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}


//MEthod for post the request to update the notes
-(void)addNewNoteInLocal:(NSDictionary *)dictNote{
    /*
    "sBlockNotes": "vikas",
    "nBlockNo": "2503",
    "ClientID": "1580",
    "program_id": "10621",
    "action": "addNotes",
    "nBlockID": 1404
    */
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    NSString *strDate = [formatter stringFromDate:[NSDate date]];
    
    [[DataManager initDB]insertSheetBlocksForSheet:[dictNote objectForKey: PROGRAM_ID] date:[dictNote objectForKey:kDate] blockNo:[dictNote objectForKey:kBlockNo] clientId:self.strClientId blockTitle:@"" blockNotes:@"" andBlockId:[dictNote objectForKey:kBlockId]];
    
    
    NSInteger success = [[DataManager initDB] updateNotes:[dictNote objectForKey:kBlockNotes] blockTitle:[dictNote objectForKey:PROGRAM_ID] ForBlock:[dictNote objectForKey:kBlockNo]forClient:self.strClientId andexistingNotes:@""];
    
    
    
    //NSInteger success = [[DataManager initDB] updateNotes:updateString blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
    
    
    [self fillArrayWithData];
}

//TODO: DELETE EXERCISE REQUEST
-(void)postRequestForDeleteExercise:(NSDictionary *)params{
    START_METHOD
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@deleteData/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"postRequestForDeleteExercise-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestForDeleteExercise->responce: %@", responseDataDictionary);
                               
                               
                               //Invoke method for delete Exercise
                               [self deleteExerciseFromLocal];
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                               
                               
                           }];
    END_METHOD
    
}


//TODO: DELETE exercise from local
-(void)deleteExerciseFromLocal{
    START_METHOD
    
    int success =[[DataManager initDB] deleteBlockData:self.strClientId :[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]  :[NSString stringWithFormat:@"%ld",(long)selectedBtn.tag]];
    [self fillArrayWithData];
    [self.tableview reloadData];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    END_METHOD
}

//TODO: PASTE Entire Program
-(void)pasteEntireProgramInServer{
    START_METHOD
    
    
    //TODO: SUNIL PASTE ENTIRE PROGRAM
    NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
    
    NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
    NSString *strProgramClientID = [[defauts valueForKey:kProgramData] valueForKey:kClientId];
    NSString *strProgramSheetID = [[defauts valueForKey:kProgramData] valueForKey:ksheetID];
    
    NSArray *tempArraBlocks = [[DataManager initDB] getBlockDataForClient:strProgramClientID SheetId:strProgramSheetID];
    
    //Array of total workouts ids
    NSMutableArray *arrayWorkoutsIds    =   [[NSMutableArray alloc]init];
    MESSAGE(@"tempArraBlocks->: %@",tempArraBlocks);
    
    for(int i=0;i<[tempArraBlocks count];i++){
        
        NSDictionary *dictBlock = [tempArraBlocks objectAtIndex:i];
       
        //Add workouts ids in array
        [arrayWorkoutsIds addObject:[dictBlock objectForKey:kBlockNo]];
    
    }
    
    MESSAGE(@"[commonUtility retrieveValue:KEY_USER_ID]: %@",[commonUtility retrieveValue:KEY_TRAINER_ID]);
    
    //Craete the dictionry for send to copy paste
    NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
     [objdict setValue:@"copyPasteProgram" forKey:ACTION];
    [objdict setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:@"program_id"];
     [objdict setValue:arrayWorkoutsIds forKey:@"workout_id"];
     [objdict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
    

    //Invoke method for Copy paste exercsie exercise
    [self postRequestForCopyEntireProgram:objdict];

    END_METHOD
}




//TODO: POST REQUSET FOR ENTIRE PROGRAM COPY PASTE
-(void)postRequestForCopyEntireProgram:(NSMutableDictionary *)dictCopyProgram{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictCopyProgram,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictCopyProgram
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSMutableDictionary *dictCopyProgramTemp =  [[[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] mutableCopy];
                                   
                                   MESSAGE(@"arrayUserData delete dictClientExercsie send Dictonary : %@ \n and response from server addNotes: %@",dictCopyProgram,dictCopyProgramTemp);
                                   
                                   [dictCopyProgramTemp setValue:[dictCopyProgram objectForKey:@"actioncopyPasteWorkouts"] forKey:@"actioncopyPasteWorkouts"];
                                   
                                   
                                       //Invoke method for save in loacl db
                                       [self copyPasteInLocal:dictCopyProgramTemp];

                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}


-(void)copyPasteInLocal:(NSDictionary *)responseDictionary{
    START_METHOD
    
    
    MESSAGE(@"_blockSheetNo for copy paste: %ld",(long)_blockSheetNo);
    
    
    //Get Dictionary
    NSDictionary   *responseDataDictionary    =   [commonUtility dictionaryByReplacingNullsWithStrings:responseDictionary];
        if([responseDataDictionary objectForKey:@"workout_list"]){
            
            //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
            NSArray *arrayclientsWorkoutsAndNotes =  [responseDataDictionary objectForKey:@"workout_list"];
            
            MESSAGE(@"arrayUserData -> arrayclientsWorkoutsAndNotes: %@",arrayclientsWorkoutsAndNotes);
            
            //Save Clients workouts and notes
            [self saveClientsWorkoutsAndNotes:arrayclientsWorkoutsAndNotes];
        }
    
    //Save Client's exercise
    [self saveClientsExercise:responseDataDictionary];
    
    END_METHOD
}



//Save client's workouts and notes in local DB
-(void)saveClientsWorkoutsAndNotes:(NSArray *)arrayclientsWorkoutAndNotes{
    
    START_METHOD
    
    
    //Delete the current workout
//    [[DataManager initDB] deleteWorkoutForCopyPaste:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forClient:self.strClientId     andWorkOutBlockId:[NSString stringWithFormat:@"%ld",_blockSheetNo]];
    
    //If array has data
    if (arrayclientsWorkoutAndNotes && arrayclientsWorkoutAndNotes.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsWorkoutAndNotes.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictWorkoutAndNotes = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsWorkoutAndNotes objectAtIndex:i]]mutableCopy];
            
            
            
            MESSAGE(@"cleints dictWorkoutAndNotes: %@",dictWorkoutAndNotes);
            
            [[DataManager initDB]insertSheetBlocksForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] date:[dictWorkoutAndNotes objectForKey:@"dBlockdate"] blockNo:[dictWorkoutAndNotes objectForKey:kBlockNo] clientId:self.strClientId blockTitle:[dictWorkoutAndNotes objectForKey:kBlockTitle] blockNotes:[dictWorkoutAndNotes objectForKey:kBlockNotes] andBlockId:[dictWorkoutAndNotes objectForKey:kBlockNo]];
            //sunil-> date: 02 sept 2016
            
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsExercise:(NSDictionary *)responseDataDictionary{
    
    START_METHOD
    if([responseDataDictionary objectForKey:@"exercise_list"]){
        
    //************  GET CLEINT'S Exercise LIST AND SAVE IN LOCAL DB
    NSArray *arrayclientsExercise =  [responseDataDictionary objectForKey:@"exercise_list"];
    
    MESSAGE(@"arrayUserData -> arrayclientsExercise: %@",arrayclientsExercise);
    
    
    //If array has data
    if ([arrayclientsExercise isKindOfClass:[NSArray class]] && arrayclientsExercise.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsExercise.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictExercise = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsExercise objectAtIndex:i]]mutableCopy];
            
            
            [dictExercise setValue:[dictExercise objectForKey:@"sDate"] forKey:kEXDate];
            [dictExercise setValue:[dictExercise objectForKey:@"program_exercise_id"] forKey:kBlockDataId];
            [dictExercise setValue:self.strClientId forKey:kClientId];
            [dictExercise setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:kSheetId];

            
            MESSAGE(@"cleints dictExercise: %@",dictExercise);
            
            //Invoke method for insert exercise
            NSInteger success = [[DataManager initDB] insertProgramData:dictExercise forClient:[dictExercise objectForKey:kClientId]];
            
        }
        
    }
    
    //For Paste the current workouts
    if ([responseDataDictionary objectForKey:@"actioncopyPasteWorkouts"]) {
        
        MESSAGE(@"For Paste entire actioncopyPasteWorkouts ");
        //Invoke method for realod the table after paste the curent workout
        [self reloadWorkoutLists];
        
        
    }else{//For Paste entire Program
    
        MESSAGE(@"For Paste entire Program");
        
        
    //Invoke Method for laod table
    [self reloadViewAfterPasterEntireProgram];
    
        /* //Comment for crash at time paste entire program
    
//For Set Workout and exercise to display after paste entire program -->
        self.currentBlockNo = self.currentBlockNo -1;
        _blockSheetNo = _blockSheetNo -1;
        
        
        
        _blockSheetNo =  [[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockNo"] intValue];
        
        
        [self fillArrayWithData];
        
        if (self.arrayBlockDates.count) {
            
            MESSAGE(@"if (self.arrayBlockDates.count) {");
            
           [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
            
            self.tableview.delegate = self;
            self.tableview.dataSource = self;
            [self reloadData:YES with:@"Prev"];
        }
         */
    }
       //For Set Workout and exercise to display after paste entire program <------------
        
    }
}



-(void)reloadViewAfterPasterEntireProgram{
    START_METHOD
    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
    [[NSUserDefaults standardUserDefaults] setValue:tempDict forKey:kProgramData];
    
    //20 sept for Template entire program copy
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:kTemplateSheetData];
    
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
    _blockSheetNo = self.arrayBlockDates.count;
    
    MESSAGE(@"self.arrayBlockDates 121 array-> %@",self.arrayBlockDates);
    
    [self fillArrayWithData];
    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
    
    
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview reloadData];
}

//TODO: COPY PASTE CURRENT WORKOUTS
-(void)copyPasteCurrentWorkouts{
    START_METHOD
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];
    NSString *strCurrentCopiedSheetID = [defaults valueForKey:kBlockCurrentCopiedSheetID];
    NSString *strCurrentCopiedClientID = [defaults valueForKey:kCopiedClientID];
    
    

    MESSAGE(@"else kCopyProgramBlock  block ------>strCurrentCopiedClientID: %@  strCurrentCopiedSheetID:%@  strCurrentCopiedBlockId:%@",strCurrentCopiedClientID,strCurrentCopiedSheetID,strCurrentCopiedBlockId);
    
    NSDictionary *dictCopiedBlock = [defaults valueForKey:kCopyProgramBlock];
    
    NSString *strBlockID = [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockID"];
    
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    MESSAGE(@"copyPasteCurrentWorkouts--> strUpgradeStatus: %@",strUpgradeStatus);
    //For old version app
    if(![strUpgradeStatus isEqualToString:@"YES"]){
        
        
        
        [[DataManager initDB] updateBlockNameForWorkout:[dictCopiedBlock valueForKey:@"sBlockTitle"] forClient:self.strClientId sheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] blockId:strBlockID];
        
        NSArray *arrayCopiedLines = (NSArray*)[dictCopiedBlock valueForKey:kCopiedLinesForBlock];
        MESSAGE(@"else kCopyProgramBlock  block ------>arrayCopiedLines:%@  and dictCopiedBlock:%@",arrayCopiedLines,dictCopiedBlock);
        
    for (NSDictionary *dictCopiedLine in arrayCopiedLines) {
        
        rowValueForExercise = rowValueForExercise + 1;
        NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                             kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                             kExercise :[dictCopiedLine valueForKey:kExercise],
                             kLBValue :[dictCopiedLine valueForKey:kLBValue],
                             kRepValue :[dictCopiedLine valueForKey:kRepValue],
                             kSetValue : [dictCopiedLine valueForKey:@"sSetValue"],
                             kEXDate :[dictCopiedLine valueForKey:kEXDate],
                             kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                             kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValueForExercise],
                             kColor : [dictCopiedLine valueForKey:kColor],
                             };
        MESSAGE(@" kCopyProgramBlock  block dictCopiedLine loop------dic: %@",dic);
  
            [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
        
    }
        //Reaload table
         [self reloadWorkoutLists];
        
    }else{//For Updated Version
        
        //Array of total workouts ids
        NSMutableArray *arrayWorkoutsIds    =   [[NSMutableArray alloc]init];
         [arrayWorkoutsIds addObject:strCurrentCopiedBlockId];
        
        //Craete the dictionry for send to copy paste
        NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
        [objdict setValue:@"copyPasteProgram" forKey:ACTION];
        [objdict setValue:@"copyPasteWorkouts" forKey:@"actioncopyPasteWorkouts"];
        [objdict setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:@"program_id"];
        [objdict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
        [objdict setValue:arrayWorkoutsIds forKey:@"workout_id"];
        
        //In which workout paste
        [objdict setValue:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forKey:@"workout_id_in"];

        /*
        
        //Invoke methods for update  workouts name
        MESSAGE(@"SUNIL-> Update Nmame WORKOUT");
        //Craete dict
        NSDictionary *dictWorkout =@{
                                     kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                     kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                                     kClientId :self.strClientId,
                                     kBlockTitle:[dictCopiedBlock valueForKey:@"sBlockTitle"],
                                     ACTION:@"updateWorkout",
                                     };
        
        //Invoke method for poast new workout to server
        [self postRequestForwWorkout:[dictWorkout mutableCopy]];
         */
        
        //Invoke method for paste workouts
        [self postRequestForCopyEntireProgram:objdict];

    }
  
    END_METHOD
}

//Method for reaload table after the Paste curent workout
-(void)reloadWorkoutLists{
    START_METHOD
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:nil forKey:kCopyProgramBlock];
    [defaults setObject:nil forKey:kCopyBlock];
    [defaults setObject:nil forKey:kCopiedBlockIDSheetID];
    [defaults setObject:nil forKey:kBlockCurrentCopiedSheetID];
    [defaults setObject:nil forKey:kCopiedClientID];
    
    [self fillArrayWithData];
    
    
    if(self.arrayBlockDates.count>self.currentBlockNo){
        //SUNIL
        [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
    }
    
    
    [self.tableview reloadData];
    
    
    
    END_METHOD
}

//20 Sept
//TODO : PASTE workout from template
-(void)pasteWorkoutFromTemplate{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];

    
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    MESSAGE(@"copyPasteCurrentWorkouts--> strUpgradeStatus: %@",strUpgradeStatus);

    
    //For old version app
    if(![strUpgradeStatus isEqualToString:@"YES"]){
        MESSAGE(@"else kCopyBlock  block ------>");
        
        NSDictionary *dictCopiedBlock = [defaults valueForKey:kCopyBlock];
        NSString *strBlockID = [[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockID"];
        [[DataManager initDB] updateBlockNameForWorkout:[dictCopiedBlock valueForKey:@"sBlockTitle"] forClient:self.strClientId sheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] blockId:strBlockID];
        
        NSArray *arrayCopiedLines = (NSArray*)[dictCopiedBlock valueForKey:kCopiedLinesForBlock];
        
        for (NSDictionary *dictCopiedLine in arrayCopiedLines) {
            
            rowValueForExercise = rowValueForExercise + 1;
            NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                                 kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                 kExercise :[dictCopiedLine valueForKey:kExercise],
                                 kLBValue :[dictCopiedLine valueForKey:kLBValue],
                                 kRepValue :[dictCopiedLine valueForKey:kRepValue],
                                 kSetValue : [dictCopiedLine valueForKey:@"sSetValue"],
                                 kColor : [dictCopiedLine valueForKey:kColor],
                                 kEXDate :[dictCopiedLine valueForKey:kEXDate],
                                 kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
                                 kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValueForExercise],
                                 
                                 };
            MESSAGE(@" kCopyBlock  block dictCopiedLine loop------dic: %@",dic);
            
            [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
            
        }
        
        
        //Reaload table
        [self reloadWorkoutLists];
        
        
        
        //For Pgradeded user
    }else{//For Updated Version
        
        //Array of total workouts ids
        NSMutableArray *arrayWorkoutsIds    =   [[NSMutableArray alloc]init];
        [arrayWorkoutsIds addObject:strCurrentCopiedBlockId];
        
        //Craete the dictionry for send to copy paste
        NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
        [objdict setValue:@"copyPasteProgram" forKey:ACTION];
        [objdict setValue:@"copyPasteWorkouts" forKey:@"actioncopyPasteWorkouts"];
        [objdict setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:@"program_id"];
        [objdict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
        [objdict setValue:arrayWorkoutsIds forKey:@"workout_id"];
        
        /*
         
         //Invoke methods for update  workouts name
         MESSAGE(@"SUNIL-> Update Nmame WORKOUT");
         //Craete dict
         NSDictionary *dictWorkout =@{
         kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
         kBlockNo :[NSString stringWithFormat:@"%ld",(long)_blockSheetNo],
         kClientId :self.strClientId,
         kBlockTitle:[dictCopiedBlock valueForKey:@"sBlockTitle"],
         ACTION:@"updateWorkout",
         };
         
         //Invoke method for poast new workout to server
         [self postRequestForwWorkout:[dictWorkout mutableCopy]];
         */
        
        //Invoke method for paste workouts
        [self postRequestForCopyEntireProgram:objdict];
        
    }
    
    
}


//TODO: Paste Entire program from Template
-(void)pasteEntireProgramFromTemplate{
    START_METHOD
    
    NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
    
    NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
    
    NSArray * arrSheetBlocks = [[DataManager initDB]getBlockDataFor_TemplateSheet:[strTemplateSheetId integerValue]];
    MESSAGE(@"arrSheetBlocks -> %@",arrSheetBlocks);
    
    
    
    for (NSDictionary *dic in arrSheetBlocks) {
        
        NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];
        NSString *strDate = [dic valueForKey:@"dBlockDate"];
        _blockSheetNo = _blockSheetNo + 1;
        rowValueForExercise =0;
        
        self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
        MESSAGE(@"self.arrayBlockDates -> %@",self.arrayBlockDates);
        
        
        NSInteger intLastBlockNum = [[[self.arrayBlockDates objectAtIndex:self.arrayBlockDates.count-1] valueForKey:@"nBlockNo"] integerValue];
        
        NSMutableDictionary *dictTempBlock = [[NSMutableDictionary alloc]initWithDictionary:dic];
        [dictTempBlock setObject:[NSString stringWithFormat:@"%ld",(long)intLastBlockNum+1] forKey:@"nBlockNo"];
        
        [[DataManager initDB]insertSheetBlocksForSheet:sId date:strDate blockNo:[NSString stringWithFormat:@"%ld",(long)intLastBlockNum+1] clientId:self.strClientId blockTitle:[dictTempBlock valueForKey:kBlockTitle] blockNotes:@"" andBlockId:@""];
        
        
        [[DataManager initDB] updateDate:[self.btnDate titleForState:UIControlStateNormal] blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo] forClient:self.strClientId];
        dictTempBlock = nil;
        
        NSArray * temparrayBlocks = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId]]];
        
        MESSAGE(@"self.arrayBlockDates temparrayBlocks -> %@",temparrayBlocks);
        
        NSInteger tempCurrentBlockSheetNum = temparrayBlocks.count;
        
        NSArray *arrayTempPrograms = [[DataManager initDB]selectProgramDataForBlockNo:[[dic valueForKey:@"nBlockID"]integerValue]];
        NSInteger rowValueForExercise1 = 0;
        MESSAGE(@"self.arrayBlockDates temparrayBlocks arrayTempPrograms-> %@",arrayTempPrograms);
        
        for (NSDictionary *dictCopiedLine in arrayTempPrograms) {
            
            rowValueForExercise1 = rowValueForExercise1 + 1;
            
            NSDictionary *dic =@{kBlockId : [NSString stringWithFormat:@"%ld",(long)tempCurrentBlockSheetNum],
                                 kSheetId : [NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                 kExercise :[dictCopiedLine valueForKey:kExercise],
                                 kLBValue :[dictCopiedLine valueForKey:kLBValue],
                                 kRepValue :[dictCopiedLine valueForKey:kRepValue],
                                 kSetValue : [dictCopiedLine valueForKey:@"sSetValue"],
                                 kEXDate :[dictCopiedLine valueForKey:kEXDate],
                                 kBlockNo :[NSString stringWithFormat:@"%ld",(long)tempCurrentBlockSheetNum],
                                 kRowNo :[NSString stringWithFormat:@"%ld",(long)rowValueForExercise1],
                                 kColor : [dictCopiedLine valueForKey:kColor],
                                 };
            MESSAGE(@"dictCopiedLine  Loop-> %@",dic);
            
            [[DataManager initDB] insertProgramData:dic forClient:self.strClientId];
            
        }
        
    }
    
    
    //Invoke method for realod
    [self reloadViewAfterPasterEntireProgram];
    
    //Get Blocks
   
    MESSAGE(@"dictCopiedLine  self.arrayBlockDates -> %@",self.arrayBlockDates );
    
    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:kTemplateSheetData];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
    
    END_METHOD
}




//TODO: PASTE Entire Program
-(void)getEntireTemplateProgramForPaste{
    START_METHOD
    
    
    NSUserDefaults *defauts = [NSUserDefaults standardUserDefaults];
    
    NSString        *strProgramSheetID;
    NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
    
    NSArray * arrSheetBlocks = [[DataManager initDB]getBlockDataFor_TemplateSheet:[strTemplateSheetId integerValue]];
    MESSAGE(@"getEntireTemplateProgramForPaste->arrSheetBlocks -> %@",arrSheetBlocks);
    

    NSMutableArray *arrayWorkoutsIds    =   [[NSMutableArray alloc]init];
    MESSAGE(@"arrayWorkoutsIds -> %@",arrayWorkoutsIds);
    
    for (NSDictionary *dic in arrSheetBlocks) {
       
        
        strProgramSheetID   =   [[NSString alloc]initWithFormat:@"%@",[dic objectForKey:kSheetId]];
        //Add workouts ids in array
        [arrayWorkoutsIds addObject:[dic objectForKey:kBlockNo]];
        
    }
    
    MESSAGE(@"[commonUtility retrieveValue:KEY_USER_ID]: %@",[commonUtility retrieveValue:KEY_TRAINER_ID]);
    
    //Craete the dictionry for send to copy paste
    NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
    [objdict setValue:@"copyPasteProgram" forKey:ACTION];
    [objdict setValue:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forKey:@"program_id"];
    [objdict setValue:arrayWorkoutsIds forKey:@"workout_id"];
    [objdict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
    
    
    //Invoke method for Copy paste exercsie exercise
    [self postRequestForCopyEntireProgram:objdict];
    
    END_METHOD
}



//TODO: DELET THE current workout

-(void)postRequestForDeleteWorkout:(NSDictionary *)params{
    START_METHOD
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@deleteData/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"postRequestForDeleteWorkout-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                                       success:^(NSDictionary *responseDataDictionary) {
                                           
                                           MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                                           
                                           //Invoke method for delete Workout detail
                                           [self deleteWorkoutFromServer];
                                           
                                           //Hide the indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                       }failure:^(NSError *error) {
                                           MESSAGE(@"Eror : %@",error);
                                           
                                           //Hide the indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                           //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                                           
                                           
                                           
                                           
                                       }];
    END_METHOD
    
}


//TODO: DELETE workout fropm local
-(void)deleteWorkoutFromWorkOut{
    
    NSInteger success= 1;
    if([self.arrayBlockDates count] == 1) {
        success = [[DataManager initDB]deleteWorkOutBlockDataForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forClient:self.strClientId andWorkoutBlockId:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]];
        NSInteger success1 =   [[DataManager initDB] updateBlockName:@"Workout-1" forClient:self.strClientId sheetId:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] blockId:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]];
        if(success1 == 0) {
        }
        
    } else {
        success = [[DataManager initDB]deleteWorkoutForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forClient:self.strClientId andWorkOutBlockId:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]];
    }
    
    
    if (success == 0) {
        
        No_OfSheet = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
        if (No_OfSheet>0) {
            
            [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
            self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
            self.currentSheet = [self.arrSheetsList count];
            
            self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
            _blockSheetNo = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue];
            self.isFromPreviousNext = FALSE;
            
            [self fillArrayWithData];
            [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
            [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sPurpose"]];
            [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetName"]];
            [self.tableview reloadData];
        }
    }
    
    
}




//TODO: DELETE workout fropm local
-(void)deleteWorkoutFromServer{
    
    NSInteger success= 1;
    if([self.arrayBlockDates count] == 1) {
        
        
        //Post Request for add defult workout
        //TODO:SUNIL-> ADD NEW WORKOUT
        
        MESSAGE(@"SUNIL-> ADD NEW WORKOUT");
        NSString * sId =   [[DataManager initDB]getsheetId:self.lblProgramName.text forClient:self.strClientId];

        //Craete dict
        NSDictionary *dictWorkout =@{
                                     kSheetId : sId,
                                     kDate :[self.btnDate titleForState:UIControlStateNormal],
                                     kBlockNo :[NSString stringWithFormat:@"%ld", (long)_blockSheetNo],
                                     kClientId :self.strClientId,
                                     kBlockTitle:@"Workout-1",
                                     kBlockNotes : @"",
                                     ACTION      : @"addWorkout",
                                     @"user_id"  :[commonUtility retrieveValue:KEY_TRAINER_ID]

                                     };
        
        //Invoke method for poast new workout to server
        [self postRequestForwWorkout:[dictWorkout mutableCopy]];
        
        
    }
    
    //Delete from local
    success = [[DataManager initDB]deleteWorkoutForSheet:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] forClient:self.strClientId andWorkOutBlockId:[NSString stringWithFormat:@"%ld",(long)_blockSheetNo]];
    
    MESSAGE(@"success-----> %ld",(long)success);
    
    //[self.arrayBlockDates count] != 1     FOR last workout deleted
    if (success == 0 && [self.arrayBlockDates count] != 1) {
        
        No_OfSheet = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
        if (No_OfSheet>0) {
            
            
            [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
            self.currentSheetId = [[[self.arrSheetsList lastObject] valueForKey:ksheetID] intValue];
            self.currentSheet = [self.arrSheetsList count];
            
            self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
            _blockSheetNo = [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] integerValue];
            self.isFromPreviousNext = FALSE;
            
            [self fillArrayWithData];
            
            [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
            [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sPurpose"]];
            [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetName"]];
            [self.tableview reloadData];
        }
    }
    
    
}





-(void)checkFreeUserAccesForAddWorkout:(NSString *)strPopText{
    START_METHOD
    //Show Alert for Purchase Subscription
    UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(strPopText,@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
    
    
    //Set the Alert view tag
    [alertView setTag:903];
    
    //Show the alert view for confiramtion
    [alertView show];
    
    END_METHOD
}



//TODO: GET exercise from server
-(void)getExerciseFromServer:(NSDictionary*)dictObjWorkout{
    START_METHOD
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@getCLientExcercise/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"postRequestForDeleteExercise-> params=: %@ and Url : %@",dictObjWorkout,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:dictObjWorkout
                                       success:^(NSDictionary *responseDataDictionary) {
                                           
                                           MESSAGE(@"postRequestForDeleteExercise->responce: %@", responseDataDictionary);
                                           
                                           //************  GET CLEINT'S Exercise LIST AND SAVE IN LOCAL DB
                                           NSArray *arrayclientsExercise   =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"exercise_list"];
                                           
                                           MESSAGE(@"arrayUserData -> arrayclientsExercise: %@",arrayclientsExercise);
                                           
                                           NSString *strButtonTag = [dictObjWorkout objectForKey:@"buttonTag"];

                                           //Save Client's exercise
                                           [self requestClientsExercise:arrayclientsExercise andButtonTag:strButtonTag.intValue];
                                           
                                           
                                       }failure:^(NSError *error) {
                                           MESSAGE(@"Eror : %@",error);
                                           
                                           //Hide the indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                           //Show Alert For error
                                           [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                                           
                                           
                                           
                                       }];
    END_METHOD
    
}



//Save clients in local DB
-(void)requestClientsExercise:(NSArray *)arrayclientsExercise andButtonTag: (int)buttonTag{
    
    START_METHOD
    
    //If array has data
    if (arrayclientsExercise && arrayclientsExercise.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsExercise.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictExercise = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsExercise objectAtIndex:i]]mutableCopy];
            
            
            [dictExercise setValue:[dictExercise objectForKey:@"sDate"] forKey:kEXDate];
            [dictExercise setValue:[dictExercise objectForKey:@"program_exercise_id"] forKey:kBlockDataId];
            
            MESSAGE(@"cleints dictExercise: %@",dictExercise);
            
            //Invoke method for insert exercise
            NSInteger success = [[DataManager initDB] insertProgramData:dictExercise forClient:[dictExercise objectForKey:kClientId]];
            
            if(success){
                [self showExerciseList:buttonTag];
                
                
                
                //Hide the indicator
                [TheAppController hideHUDAfterDelay:0];
            }
        }
        
    }
}



-(void)showExerciseList:(int)intTag{
    
    START_METHOD
    
    
    MESSAGE(@"self.currentSheet: %ld %ld %ld %ld %ld",(long)self.currentSheet,(long)_blockSheetNo,(long)currentBlockNo,(long)self.currentSheet, (long)self.currentSheetId);
    
    self.isFromPreviousNext = TRUE;
    isCopiedWorkOut = NO;
    [self checkForOptionsViewAndHide];
    [copyLineArray removeAllObjects];
    
    if(intTag == 501) {
        
        if (self.currentBlockNo>0) {
            
            
            self.currentBlockNo = self.currentBlockNo -1;
            _blockSheetNo = _blockSheetNo -1;
            
            _blockSheetNo =  [[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockNo"] intValue];
            
            
            [self fillArrayWithData];
            
            if (self.arrayBlockDates.count) {
                [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Prev"];
            }
            else{
            }
            
            
            
        } else {
            
            if (self.currentSheet > 1)
            {
                
                self.currentSheet = self.currentSheet-1;
                
                
                self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue];
                
                
                
                self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
                
                
                _blockSheetNo =  [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] intValue];
                self.totalBlocks = [self.arrayBlockDates count];
                self.currentBlockNo = [self.arrayBlockDates count] -1;
                
                [self fillArrayWithData];
                if (self.arrayBlockDates.count) {
                    
                    
                    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                }
                
                [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet -1] valueForKey:@"sPurpose"]];
                [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet -1] valueForKey:@"sheetName"]];
                [self addUnderlineInprogramName];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Prev"];
            }
            
        }
        
    } else{
        
        if (self.currentBlockNo < [self.arrayBlockDates count]-1) {
            
            _blockSheetNo = _blockSheetNo +1;
            self.currentBlockNo = self.currentBlockNo + 1;
            
            _blockSheetNo =  [[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"nBlockNo"] intValue];
            self.isFromPreviousNext = TRUE;
            
            
            [self fillArrayWithData];
            if (self.arrayBlockDates.count) {
                [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Next"];
            }
            else{
            }
            
        } else {
            
            if (self.currentSheet < [self.arrSheetsList count])
            {
                self.currentSheet = self.currentSheet+1;
                self.currentSheetId = [[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetID"] integerValue];
                
                _blockSheetNo =  [[[self.arrayBlockDates lastObject] valueForKey:@"nBlockNo"] intValue];
                self.totalBlocks = [self.arrayBlockDates count];
                self.currentBlockNo = 0;
                
                [self fillArrayWithData];
                
                if (self.arrayBlockDates.count) {
                    [self setLabelValue:[[self.arrayBlockDates objectAtIndex:self.currentBlockNo] valueForKey:@"sBlockTitle"]];
                }
                
                [self.lblProgramPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sPurpose"]];
                [self.lblProgramName setText:[[self.arrSheetsList objectAtIndex:self.currentSheet-1] valueForKey:@"sheetName"]];
                [self addUnderlineInprogramName];
                self.tableview.delegate = self;
                self.tableview.dataSource = self;
                [self reloadData:YES with:@"Next"];
            }
        }
    }
}

@end

