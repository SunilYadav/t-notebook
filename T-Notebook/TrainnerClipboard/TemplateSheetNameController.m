//
//  TemplateSheetNameController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "TemplateSheetNameController.h"
#import "ProgramViewController.h"

@implementation TemplateSheetNameController{
    
}

@synthesize tblTemplates = _tblTemplates;
@synthesize arrTemplateNames = _arrTemplateNames;
@synthesize arrTemplateSheet = _arrTemplateSheet;
@synthesize btnSave = btnSave;
@synthesize strClientId = _strClientId;
@synthesize strText1 = _strText1;
@synthesize strText2 = _strText2;
@synthesize strText3 = _strText3;
@synthesize intlastSheetidOfClient;
@synthesize delegateReloadSheet;

//----------------------------------------------------
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
//----------------------------------------------------
#pragma mark -
#pragma mark - View lifecycle

- (void)viewDidLoad{
    
    [self.tblTemplates setDelegate:self];
    [self.tblTemplates setDataSource:self];
    
    [super viewDidLoad];
}
//----------------------------------------------------

-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    [super viewWillAppear:YES];
    self.view.superview.frame = CGRectMake(0, 0, 500, 400);
    self.view.superview.center = self.view.center;
    if (iOS_7) {
        
    }
    self.strText1 = @"";
    self.strText2 = @"";
    self.strText3 = @"";
    
    [self.btnSave setEnabled:NO];

    self.arrTemplateNames = [NSArray arrayWithArray:[[DataManager initDB]getAllTemplatesDetail]];
}

//----------------------------------------------------

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
     return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

#if defined(__IPHONE_6_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0

-(BOOL)shouldAutorotate {
    return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

-(NSUInteger)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskAll;
}

#endif

//-----------------------------------------------------------------------------------------------------------------------------


#pragma mark -
#pragma mark - IBAction Methods

-(IBAction)btnSave_Clicked:(id)sender{
    
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
	
  
        BOOL changed = YES;
        int displayed = 0;
        UITextField * textfield1= (UITextField *)[self.view viewWithTag:(1)];
        
        UILabel * label = (UILabel *)[self.view viewWithTag:(101)];
        UILabel * label1 = (UILabel *)[self.view viewWithTag:(102)];
        UILabel * label2 = (UILabel *)[self.view viewWithTag:(103)];
        
        [label setHidden:YES];
        [label1 setHidden:YES];
        [label2 setHidden:YES];
        
        if([[textfield1 text] isEqualToString:@""]){
            displayed = 1;
            [label setText:@"Please Enter Sheet Name"];
            [label setHidden:NO];
        }
        
        if([self.arrTemplateSheet count] > 1){
            UITextField * textfield = (UITextField *)[self.view viewWithTag:(2)];
            UITextField * textfield2= (UITextField *)[self.view viewWithTag:(3)];
            
            if([[textfield text] isEqualToString:@""]){
                displayed = 1;
                [label1 setText:@"Please Enter Sheet Name"];
                [label1 setHidden:NO];
            }
            if([[textfield text] isEqualToString:[textfield1 text] ]){
                displayed = 1;
                [label1 setText:@"Sheet1 and Sheet2 Name is Same,Please use different name."];
                [label2 setHidden:NO];
            }
            
            if([self.arrTemplateSheet count] > 2){
                
                if([[textfield2 text] isEqualToString:@""]){
                    displayed = 1;
                    [label2 setText:@"Please Enter Sheet Name."];
                    [label2 setHidden:NO];
                }
                if([[textfield1 text] isEqualToString:[textfield2 text] ]){
                    displayed = 1;
                    [label2 setText:@"Sheet2 and Sheet3 Name is Same,Please use different name."];
                    [label2 setHidden:NO];
                }
                if([[textfield2 text] isEqualToString:[textfield text] ]){
                    changed = NO;
                    displayed = 1;
                    [label setText:@"Sheet1 and Sheet3 Name is Same,Please use different name."];
                    [label setHidden:NO];
                }
                
            }
        }
        
        if(displayed != 1){
            
            NSArray * arrclientSheetList = [NSArray arrayWithArray: [[DataManager initDB] getProgramSheetList:self.strClientId]];
            for(int i = 0; i<[self.arrTemplateSheet count]; i++){
                UITextField * textfield = (UITextField *)[self.view viewWithTag:(i+1)];
                UILabel * labeln = (UILabel *)[self.view viewWithTag:(i+101)];
                
                for(int j=0; j<[arrclientSheetList count]; j++){
                    
                    if([[textfield text] isEqualToString:[[arrclientSheetList objectAtIndex:j]valueForKey:ksheetName]]){
                        [labeln setHidden:NO];
                        [labeln  setText:@"This sheet name already exists, Please use different name."];
                        changed = NO;
                        break;
                    }
                }
            }
        }
        
        if(changed && displayed != 1){
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            
            
            
            //If Upgarede version
            if(strUpgradeStatus && [strUpgradeStatus isEqualToString:@"YES"]){
                
                //Invoke method for paste request from pre made template
                [self getRecordFromPreMadeTemplate];
                
            }else{
                
                [self copyTemplate_SheetData];
            }
            
            
        }else if(displayed != 1){
            
        }
    
    
}
//----------------------------------------------------

-(IBAction)btnCancel_Clicked:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
//----------------------------------------------------

#pragma mark -
#pragma mark - UITableViewDelegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.arrTemplateNames count];
}
//----------------------------------------------------

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}
//----------------------------------------------------
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld", (long)indexPath.row] ;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.backgroundColor = [UIColor clearColor];
    
    [cell.textLabel setText:[[self.arrTemplateNames objectAtIndex:indexPath.row] valueForKey:@"templateName"]];
    return cell;
}
//----------------------------------------------------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    int intSelectedTemplateId = [[[self.arrTemplateNames objectAtIndex:indexPath.row]valueForKey:@"template_Id"] intValue];
    
    [commonUtility saveValue:[NSString stringWithFormat:@"%d",intSelectedTemplateId] andKey:KEY_TEMPALET_ID_PASTE];
    
    self.arrTemplateSheet = [NSArray arrayWithArray:[[DataManager initDB]getSheetsOfTemplate:intSelectedTemplateId]];
    
    [self setLableAndTextField];
    
    [self.tblTemplates setHidden:YES];
    [self.btnSave setEnabled:YES];
    
}
//----------------------------------------------------

#pragma mark -
#pragma mark - Custome Methods

-(void)setLableAndTextField{
    
        NSArray * arrclientSheetList = [NSArray arrayWithArray: [[DataManager initDB] getProgramSheetList:self.strClientId]];
        
        self.intlastSheetidOfClient = [[[arrclientSheetList objectAtIndex:([arrclientSheetList count]-1)]valueForKey:ksheetID] intValue];
        
        int originOfX = 60;
        int originOfY = 100;
        
        for(int i=0; i < [self.arrTemplateSheet count]; i++){
            
            __autoreleasing UITextField * textfiled = [[UITextField alloc]initWithFrame:CGRectMake((originOfX + 150), (originOfY - 5), 175.0, 31.0)];
            [textfiled setTag:(i+1)];
            [textfiled setFont:[UIFont fontWithName:@"Helvetica" size:17]];
            [textfiled setBorderStyle:UITextBorderStyleRoundedRect];
            [textfiled setText:[[self.arrTemplateSheet objectAtIndex:i]valueForKey:@"SheetName"]];
            [self.view addSubview:textfiled];
            textfiled = nil;
            
            __autoreleasing UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(originOfX, originOfY, 125, 21)];
            [label setBackgroundColor:[UIColor clearColor]];
            [label setFont:[UIFont fontWithName:@"Helvetica" size:19]];
            NSString * strlabeltext = [NSString stringWithFormat:@"Sheet%d Name",(i+1)];
            [label setText:strlabeltext];
            [self.view addSubview:label];
            label = nil;
            
            __autoreleasing UILabel * lblWarning = [[UILabel alloc]initWithFrame:CGRectMake(originOfX + 150, (originOfY + 33), 300, 42)];
            [lblWarning setTextColor:[UIColor redColor]];
            [lblWarning setText:@"This sheet name already exists, Please use different name."];
            [lblWarning setNumberOfLines:2];
            [lblWarning setTag:(i+101)];
            [lblWarning setHidden:YES];
            [lblWarning setFont:[UIFont fontWithName:@"Helvetica" size:15]];
            [self.view addSubview:lblWarning];
            lblWarning = nil;
            
            
            for(int j=0; j<[arrclientSheetList count]; j++){
                
                if([[[self.arrTemplateSheet objectAtIndex:i]valueForKey:@"SheetName"] isEqualToString:[[arrclientSheetList objectAtIndex:j]valueForKey:ksheetName]]){
                    [lblWarning setHidden:NO];
                    if(i == 0){
                        self.strText1 = [[self.arrTemplateSheet objectAtIndex:i]valueForKey:@"SheetName"];
                    }else if(i == 1){
                        self.strText2 = [[self.arrTemplateSheet objectAtIndex:i]valueForKey:@"SheetName"];
                    }else if(i == 2){
                        self.strText3 = [[self.arrTemplateSheet objectAtIndex:i]valueForKey:@"SheetName"];
                    }
                }
                
            }
            originOfY = originOfY + 95;
            
            textfiled = nil;
            label = nil;
            lblWarning = nil;
        }
        arrclientSheetList = nil;
   
    
}

//----------------------------------------------------

-(void)copyTemplate_SheetData{
    START_METHOD
//TODO: COPY PASTE FROM TEMPLATE
    sheetNo = [[DataManager initDB] getCountofProgramSheetForClient:self.strClientId];
    for(int i = 1; i <= [self.arrTemplateSheet count]; i++){
        UITextField * textfield = (UITextField *)[self.view viewWithTag:i];
        [[DataManager initDB] addNewSheet1:[textfield text] ForClient:self.strClientId];
    }
    
    [self copyTemplateSheet_BlocksAndData];
}
//----------------------------------------------------

-(void)copyTemplateSheet_BlocksAndData{
    START_METHOD
    NSArray *arrSheetsList = [[DataManager initDB] getProgramSheetList:self.strClientId];

        for(int i=0; i < [self.arrTemplateSheet count]; i++){
            NSInteger sheetID = [[[arrSheetsList objectAtIndex:[arrSheetsList count]-[self.arrTemplateSheet count]+i] valueForKey:@"sheetID"] integerValue];
            
            MESSAGE(@"sheetID-> template: %ld",(long)sheetID);
            
            sheetNo ++;
            blockid = 0;
            NSArray * arrSheetBlockdata = [[DataManager initDB] getBlockDataFor_TemplateSheet:[[[self.arrTemplateSheet objectAtIndex:i]valueForKey:@"SheetId"]intValue]];
            
             MESSAGE(@"sheetID-> arrSheetBlockdata: %@",arrSheetBlockdata);
            
            if([arrSheetBlockdata count] > 0){
                
                for(int j = 0; j<[arrSheetBlockdata count]; j++){
                    rowNo = 0;
                    __autoreleasing NSDictionary * dictemp1 = [[NSDictionary alloc]initWithDictionary:[arrSheetBlockdata objectAtIndex:j]];
                    
                    int temp = [[DataManager initDB]getBlockIdForClient:[NSString stringWithFormat:@"%d",(self.intlastSheetidOfClient + i + 1)] :[NSString stringWithFormat:@"%d",(j+1)] :self.strClientId];
                    DebugINTLOG(@"temp string : ", temp);
                    
                    blockid ++;
                    
                    [[DataManager initDB] updateDate1:[dictemp1 valueForKey:@"dBlockdate"] blockTitle:[dictemp1 valueForKey:@"sBlockTitle"] ForBlock:[NSString stringWithFormat:@"%d",blockid] forClient:self.strClientId sheetID:[NSString stringWithFormat:@"%ld",(long)sheetID]];
                    
                    if( [[dictemp1 valueForKey:@"nBlockNo"] length] > 0){
                        
                        NSArray * arrblockdata = [NSArray arrayWithArray:[[DataManager initDB]selectProgramDataForBlockNo:[[dictemp1 valueForKey:@"nBlockID"] intValue]]];
                        NSArray *arrayBlockDates;
                        int blocksheetID;
                        int blocksheetNO;

                        arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)sheetID]]];
                        blocksheetID =  [[[arrayBlockDates lastObject] valueForKey:@"nBlockID"] intValue];
                        blocksheetNO =  [[[arrayBlockDates lastObject] valueForKey:@"nBlockNo"] intValue];

                        
                        for(int k = 0; k<[arrblockdata count]; k++){
                            
                            __autoreleasing NSDictionary * dictemp = [[NSDictionary alloc]initWithDictionary:[arrblockdata objectAtIndex:k]];
                            
                            UITextField * textfield = (UITextField *)[self.view viewWithTag:(i+1)];
                            
                            NSString * strsheetid = [[DataManager initDB]getsheetId:[textfield text] forClient:self.strClientId];
                             rowNo ++;
                  
                            NSDictionary *dicInsertData =@{kEXDate : [dictemp valueForKey:@"dEXDate"],
                                                 kBlockId :[NSString stringWithFormat:@"%d",blocksheetID],
                                                 kBlockNo :[NSString stringWithFormat:@"%d",blocksheetNO],
                                                 kRowNo :[NSString stringWithFormat:@"%d",rowNo],
                                                 kLBValue :[dictemp valueForKey:@"sLBValue"],
                                                 kExercise :[dictemp valueForKey:@"sProgram"],
                                                 kRepValue :[dictemp valueForKey:@"sRepValue"],
                                                 kSetValue :[dictemp valueForKey:@"sSetValue"],
                                                 kColor : [dictemp valueForKey:@"sColor"]
                                                 };
                            
                            
                            [[DataManager initDB] insertProgramData:dicInsertData forClient:self.strClientId :[NSString stringWithFormat:@"%ld",(long)sheetID]];
                            dicInsertData = nil;
                        }
                        arrblockdata = nil;
                    }
                    
                }
            }
            arrSheetBlockdata = nil;
            
        }
    
    [delegateReloadSheet reloadSheets:self.intlastSheetidOfClient];

END_METHOD
}

//----------------------------------------------------

#pragma mark -
#pragma mark - Memory Management Methods

-(void)dealloc{
    
    [self.tblTemplates setDelegate:nil];
}
//----------------------------------------------------

-(void)viewDidUnload{
    self.arrTemplateNames = nil;
    self.tblTemplates = nil;
    self.btnSave = nil;
    self.strClientId = nil;
    self.arrTemplateSheet = nil;
    self.strText1 = nil;
    self.strText2 = nil;
    self.strText3 = nil;
    [super viewDidUnload];
}
//----------------------------------------------------


//TODO: Get record from pre made template

-(void)getRecordFromPreMadeTemplate{
        START_METHOD
    
    
    NSMutableArray *arrayTemplateProgram =   [[NSMutableArray alloc]init];

    NSArray *arrSheetsList = [[DataManager initDB] getProgramSheetList:self.strClientId];

    MESSAGE(@"arrSheetsList--> arrSheetsList: %@ and self.arrTemplateSheet: %@",arrSheetsList,self.arrTemplateSheet);
    
    for(int i=0; i < [self.arrTemplateSheet count]; i++){
        

        //Crash at time new client paste from pre made
        
//        NSInteger sheetID = [[[arrSheetsList objectAtIndex:[arrSheetsList count]-[self.arrTemplateSheet count]+i] valueForKey:@"sheetID"] integerValue];
//        
        
        UITextField * textfield = (UITextField *)[self.view viewWithTag:(i+1)];
        
        NSMutableDictionary *objDict    =   [[NSMutableDictionary alloc]init];
        [objDict setValue:[[self.arrTemplateSheet objectAtIndex:i] objectForKey:@"SheetId"] forKey:kSheetId];
        [objDict setValue:textfield.text                                forKey:ksheetName];

        //Add program id in array
        [arrayTemplateProgram addObject:objDict];
        
    }
  
    MESSAGE(@"[commonUtility retrieveValue:KEY_USER_ID]: %@",[commonUtility retrieveValue:KEY_TRAINER_ID]);
    
    //Craete the dictionry for send to copy paste
    NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
    
    [objdict setValue:@"copyPasteTemplate"              forKey:ACTION];
    [objdict setValue:self.strClientId                  forKey:@"client_id"];
     [objdict setValue:arrayTemplateProgram                forKey:@"programs"];
    [objdict setValue:[commonUtility retrieveValue:KEY_TEMPALET_ID_PASTE]      forKey:@"template_id"];
    
    
    //Invoke method for Copy paste exercsie exercise
   [self postRequestForPasteTemplatePrograms:objdict];
    END_METHOD
}


//TODO: POST REQUSET FOR ENTIRE PROGRAM COPY PASTE
-(void)postRequestForPasteTemplatePrograms:(NSMutableDictionary *)dictCopyProgram{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictCopyProgram,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictCopyProgram
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictCopyProgramTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData delete dictClientExercsie send Dictonary : %@ \n and response from server addNotes: %@",dictCopyProgram,dictCopyProgramTemp);
                                   
                                   //Invoke method for save in loacl db
                                   [self copyPasteInLocal:dictCopyProgramTemp];
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                        
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                       
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                               
                               
                           }];
    END_METHOD
}


-(void)copyPasteInLocal:(NSDictionary *)responseDataDictionary{
    START_METHOD
   
    //Save All the programs
    if([responseDataDictionary objectForKey:@"program_list"]){
        
        //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsprogram =  [responseDataDictionary objectForKey:@"program_list"];
        
        MESSAGE(@"arrayUserData -> program_list %@",arrayclientsprogram);
        
        
        for (int i= 0; i<arrayclientsprogram.count;i++) {
            
            NSMutableDictionary *objDict   =   [[arrayclientsprogram objectAtIndex:i] mutableCopy];
            
            [objDict setValue:[objDict objectForKey:@"sSheetName"] forKey:ksheetName];
            
            //Save Clients program
            
            //Invoke method for Insert the programs for insert only program
            [[DataManager initDB] insertProgram:[commonUtility dictionaryByReplacingNullsWithStrings:objDict]];
        }
        
       
    }
    
    
    //Save all the workouts
    
    if([responseDataDictionary objectForKey:@"workout_list"]){
        
        //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsWorkoutsAndNotes =  [responseDataDictionary objectForKey:@"workout_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsWorkoutsAndNotes: %@",arrayclientsWorkoutsAndNotes);
        
        //Save Clients workouts and notes
        [self saveClientsWorkoutsAndNotes:arrayclientsWorkoutsAndNotes];
    }
    
    //Save Client's exercise
    [self saveClientsExercise:responseDataDictionary];
    
    END_METHOD
}



-(void)addPrograminLocal:(NSDictionary *)dictProgram{
    
    START_METHOD
    MESSAGE(@"addPrograminLocal-> dictProgram: %@",dictProgram);
    
    //Invoke method for Insert the programs
    [[DataManager initDB] insertProgram:dictProgram];
    
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:kAppDateFormat];
    NSString *result = [formater stringFromDate:[NSDate date]];
    
    [[DataManager initDB]insertSheetBlocksForSheet:[dictProgram objectForKey:kSheetId] date:result blockNo:[dictProgram objectForKey:kBlockNo] clientId:[dictProgram objectForKey:kClientId] blockTitle:[dictProgram objectForKey:kBlockTitle] blockNotes:@"" andBlockId:[dictProgram objectForKey:kBlockNo]];
    
    END_METHOD
}


//Save client's workouts and notes in local DB
-(void)saveClientsWorkoutsAndNotes:(NSArray *)arrayclientsWorkoutAndNotes{
    
    START_METHOD
    //If array has data
    if (arrayclientsWorkoutAndNotes && arrayclientsWorkoutAndNotes.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsWorkoutAndNotes.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictWorkoutAndNotes = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsWorkoutAndNotes objectAtIndex:i]]mutableCopy];
            
            
            
            MESSAGE(@"cleints dictWorkoutAndNotes: %@",dictWorkoutAndNotes);
            
            [[DataManager initDB]insertSheetBlocksForSheet:[dictWorkoutAndNotes objectForKey:kSheetId] date:[dictWorkoutAndNotes objectForKey:@"dBlockdate"] blockNo:[dictWorkoutAndNotes objectForKey:kBlockNo] clientId:self.strClientId blockTitle:[dictWorkoutAndNotes objectForKey:kBlockTitle] blockNotes:[dictWorkoutAndNotes objectForKey:kBlockNotes] andBlockId:[dictWorkoutAndNotes objectForKey:kBlockNo]];
            //sunil-> date: 02 sept 2016
            
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsExercise:(NSDictionary *)responseDataDictionary{
    
    START_METHOD
    if([responseDataDictionary objectForKey:@"exercise_list"]){
        
        //************  GET CLEINT'S Exercise LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsExercise =  [responseDataDictionary objectForKey:@"exercise_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsExercise: %@",arrayclientsExercise);
        
        
        //If array has data
        if ([arrayclientsExercise isKindOfClass:[NSArray class]] && arrayclientsExercise.count>0) {
            
            //Iterate all the clients
            for (int i=0; i<arrayclientsExercise.count; i++) {
                
                //Get clint dict info
                NSMutableDictionary *dictExercise = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsExercise objectAtIndex:i]]mutableCopy];
                
                
                [dictExercise setValue:[dictExercise objectForKey:@"sDate"] forKey:kEXDate];
                [dictExercise setValue:[dictExercise objectForKey:@"program_exercise_id"] forKey:kBlockDataId];
                [dictExercise setValue:self.strClientId forKey:kClientId];
                
                MESSAGE(@"cleints dictExercise: %@",dictExercise);
                
                //Invoke method for insert exercise
                NSInteger success = [[DataManager initDB] insertProgramData:dictExercise forClient:[dictExercise objectForKey:kClientId]];
                
            }
            
        }
    }
    
    //Realod table view
        [delegateReloadSheet reloadSheets:self.intlastSheetidOfClient];
}

@end
