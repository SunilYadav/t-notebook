//
//  NotesCustomCell.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "NotesCustomCell.h"
#define kProximaRegular @"ProximaNova-Regular"
#define kFont [UIFont fontWithName:@"ProximaNova-Regular" size:16]
@implementation NotesCustomCell


- (void)awakeFromNib {

}

//-----------------------------------------------------------------------

-(void)configCell :(NSString *)str  {
    
    if(self.setLandscapeCell) {
        [self setFrame:CGRectMake(0, 0, kLandscapeWidth, self.frame.size.height)];
        [self setLandscapeOrientation];
    } else {
        [self setFrame:CGRectMake(0, 0, kPotraitWidth, self.frame.size.height)];
        [self setPotraitOrientation];
    }
    

    [self.lblDetails setNumberOfLines:0];

    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc]initWithString:str attributes:@{NSFontAttributeName:kFont}];

    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    
    if(self.setLandscapeCell) {
        [style setLineSpacing:19];
        [style setMaximumLineHeight:28];
    } else {
        [style setLineSpacing:19];
        [style setMaximumLineHeight:20];
    }
    
   
    
    NSDictionary *attributes = @{NSFontAttributeName :kFont,
                                 NSParagraphStyleAttributeName : style,
                                 };
    
    [attrString addAttributes:attributes range:NSMakeRange(0, attrString.length)];
    
    [self.lblDetails setLineBreakMode:NSLineBreakByCharWrapping];
    [[self lblDetails] setNumberOfLines:0];
    [self.lblDetails setTextColor:[UIColor colorWithRed:114.0f/255 green:117.0f/255 blue:118.0f/255 alpha:1.0]];
    
    self.lblDetails.attributedText = attrString;

    self.lblDetails.backgroundColor = [UIColor clearColor];
    CGFloat rect1 = [self getHeightForText:self.lblDetails.attributedText];
    
    [self.lblDetails setFrame:CGRectMake(self.lblDetails.frame.origin.x + 3, self.lblDetails.frame.origin.y, self.lblDetails.frame.size.width, rect1)];
   
   

    
}

//-----------------------------------------------------------------------

- (CGFloat)getHeightForText:(NSAttributedString *)strText {
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
   
    CGRect paragraphRect;
    if(self.setLandscapeCell) {
        [style setLineSpacing:19];
        [style setMaximumLineHeight:28];

        paragraphRect =
        [strText boundingRectWithSize:CGSizeMake(kLandscapeWidth - 310, CGFLOAT_MAX)
                              options:(NSStringDrawingUsesFontLeading| NSStringDrawingUsesLineFragmentOrigin)
                              context:nil];
    } else {
        [style setLineSpacing:19];
        [style setMaximumLineHeight:20];

        paragraphRect =
        [strText boundingRectWithSize:CGSizeMake(600, CGFLOAT_MAX)
                              options:(NSStringDrawingUsesFontLeading| NSStringDrawingUsesLineFragmentOrigin)
                              context:nil];
  
    }
    
    
    return paragraphRect.size.height;
}


//-----------------------------------------------------------------------

-(void)setLandscapeOrientation {
    
    [self.lblDate setFrame:CGRectMake(8+12, 5, 84, 44)];
    [self.btnDate setFrame:CGRectMake(8+12, 5, 84, 44)];
    [self.lblDetails setFrame:CGRectMake(95 +35, 10, kLandscapeWidth - 330, 44)];
    [self.lblDetails setBackgroundColor:[UIColor clearColor]];
    [self.btnEditNotes setFrame:CGRectMake(642 + 190, 5, 45, 45)]; //200

}


//-----------------------------------------------------------------------

-(void) setPotraitOrientation {
    [self.lblDate setFrame:CGRectMake(8, 0, 84, 44)];
    [self.btnDate setFrame:CGRectMake(8, 0, 84, 44)];
    [self.lblDetails setFrame:CGRectMake(95, 5, 552, 44)];
    [self.btnEditNotes setFrame:CGRectMake(642, 0, 45, 45)];
}

//-----------------------------------------------------------------------

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
