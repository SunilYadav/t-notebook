//
//  HomeCusetomCell.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2011 WLI All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeCustomCell : UITableViewCell {
    	UILabel		* _lblCName;
	UILabel		* _lblCNameWorkout;
    	UILabel		* _lblCNameWorkoutDate;
}

@property (nonatomic,strong) UILabel		*   lblCName;
@property (nonatomic,strong) UILabel		*   lblCNameWorkout;
@property (nonatomic,strong) UILabel		*   lblCNameWorkoutDate;
@property (nonatomic,strong) UIImageView		* ImgTable;
@property (nonatomic,strong)  UISwitch      *   mySwitch;

@end
