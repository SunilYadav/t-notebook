//
//  Logger.h
//  
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>

#define DebugSTRLOG(string,obj) [Logger displayLog:obj withString:string];
#define DebugLOG(string) [Logger debugLog:string];
#define DebugINTLOG(string,int) [Logger logInt:int withString:string];
#define DebugFLOATOG(string,float) [Logger logFloat:float withString:string];
#define DebugBOOLLOG(string,bool) [Logger logBool:bool withString:string];

#define DebugCGRect(str, rect) // NSLog(@"%@:- %.02f, %.02f, %.02f, %.02f", str, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height)
#define DebugCGPoint(str, point) // NSLog(@"%@:- %.02f, %.02f", str, point.x, point.y)

@interface Logger : NSObject {

}

+ (void) displayLog:(id)object withString:(NSString *)String;
+ (void) debugLog:(NSString *)string;
+ (void) logInt:(int)integer withString:(NSString *)andString;
+ (void) logFloat:(float)Float withString:(NSString *)andString;
+ (void) logBool:(BOOL)boolean withString:(NSString *)andString;

@end
