//
//  UIImage-Extensions.h
//  version 1.0.2
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>
#include <QuartzCore/QuartzCore.h>

@interface UIImage (CS_Extensions)

- (UIImage *)imageAtRect:(CGRect)rect;
- (UIImage *)imageByScalingProportionallyToMinimumSize:(CGSize)targetSize;
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
- (UIImage *)imageByScalingToSize:(CGSize)targetSize;
- (UIImage *)imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;

/*
// These three functions come from
// http://www.icab.de/blog/2010/10/01/scaling-images-and-creating-thumbnails-from-uiviews/
*/
+ (UIImage*)imageFromView:(UIView*)view;
+ (UIImage*)imageFromView:(UIView*)view scaledToSize:(CGSize)newSize;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;

/*
 // This function came from
 // http://stackoverflow.com/questions/5427656/ios-uiimagepickercontroller-result-image-orientation-after-upload
 */
- (UIImage *)fixOrientation;

+ (UIImage *)maskImage:(UIImage *)image 
              withMask:(UIImage *)maskingImage;

@end;


CGImageRef createMaskWithImage(CGImageRef image);