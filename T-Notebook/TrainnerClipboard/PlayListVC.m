//
//  PlayListVC.m
//  T-Notebook
//
//  Created by WLIi on 13/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "PlayListVC.h"

@interface PlayListVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic , weak) IBOutlet          UISwitch             *switchMusic;
@property (nonatomic , weak) IBOutlet          UISwitch             *switchShuffle;
@property (nonatomic , weak) IBOutlet          UISwitch             *switchRepeat;
@property (nonatomic , weak) IBOutlet          UITableView       *tblPlaylist;
@property (nonatomic , weak) IBOutlet           UIView               *containerView;

@property (nonatomic , strong) NSArray                                    *arrayPlayList;
@property (nonatomic , strong) NSIndexPath                           *selectedRow;
@property (weak, nonatomic) IBOutlet UILabel *lblMusicMode;
@property (weak, nonatomic) IBOutlet UILabel *lblShuffleMode;
@property (weak, nonatomic) IBOutlet UILabel *lblRepeatMode;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectPlayList;



@end

@implementation PlayListVC

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Memory Management methods 

//---------------------------------------------------------------------------------------------------------------

-(void)dealloc {
    
    self.arrayPlayList = nil;
    self.tblPlaylist.delegate = nil;
    self.tblPlaylist.dataSource = nil;
    self.switchMusic = nil;
    self.switchRepeat = nil;
    self.switchShuffle = nil;
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Custom Methods 

//---------------------------------------------------------------------------------------------------------------

-(void)setupLayout {
    
    UIButton * btnhome = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnhome setFrame:CGRectMake(10, 10, 80, 30)];
    [btnhome setBackgroundColor:[UIColor colorWithRed:73.0f/255 green:197.0f/255 blue:186.0f/255 alpha:1.0f]];
    [btnhome setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnhome setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
    [btnhome addTarget:self action:@selector(homeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtn = [[UIBarButtonItem alloc] initWithCustomView:btnhome];
    
    UILabel * lblPlayList = [[UILabel alloc]initWithFrame:CGRectMake(125, 10, 150, 29)];
    [lblPlayList setText:NSLocalizedString(@"PLAYLIST", nil)];
    [lblPlayList setFont:[UIFont fontWithName:kProximaNovaAltBold_Italic size:24]];
    UIBarButtonItem *lblTitle = [[UIBarButtonItem alloc] initWithCustomView:lblPlayList];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    NSArray * toolItems = [NSArray arrayWithObjects:leftBtn,space,lblTitle,space, nil];
    
    UIToolbar *signToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,600, 48)];
    [signToolBar setBackgroundColor:[UIColor grayColor]];
    signToolBar.items = toolItems;
    
    [self.view addSubview:signToolBar];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundImage"]]];
    [self.tblPlaylist setBackgroundColor:[UIColor clearColor]];
    
    
    [self setupData];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)setupData {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kMusicMode]) {
        [self.switchMusic setOn:YES];
    }
    else {
        [self.switchMusic setOn:NO];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kSuffleMode]) {
        [self.switchShuffle setOn:YES];
    }
    else {
        [self.switchShuffle setOn:NO];
    }
    
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:kRepeatMode]) {
        [self.switchRepeat setOn:YES];
    }
    else {
        [self.switchRepeat setOn:NO];
    }	

    MPMediaQuery * query = [MPMediaQuery playlistsQuery];
    NSArray * temp = [NSArray arrayWithArray:[query collections]];
    NSMutableArray *musicArray = [[NSMutableArray alloc]init];
    
    if ([temp count]!=0) {
        for (int i = 0; i<[temp count]; i++) {
            MPMediaPlaylist * playList = [temp objectAtIndex:i];
            if ([playList count]!=0) {
                [musicArray addObject:playList];
            }
        }
        self.arrayPlayList = [NSArray arrayWithArray:musicArray];
    }
    
    
    self.tblPlaylist.delegate = self;
    self.tblPlaylist.dataSource = self;
   

}
//---------------------------------------------------------------------------------------------------------------

-(void)homeButtonTapped:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) localizedControls {
    
    self.lblMusicMode.text =  NSLocalizedString(self.lblMusicMode.text, nil);
    self.lblRepeatMode.text =  NSLocalizedString(self.lblRepeatMode.text, nil);
    self.lblSelectPlayList.text =  NSLocalizedString(self.lblSelectPlayList.text, nil);
    self.lblShuffleMode.text =  NSLocalizedString(self.lblShuffleMode.text, nil);
}

//-----------------------------------------------------------------------


#pragma mark : Action Methods

//---------------------------------------------------------------------------------------------------------------

-(IBAction)musicModeSwitchChanged:(id)sender {
    
    if ([self.switchMusic isOn]) {
        NSString * isTimerOn = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kIsTimerPlaying]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kMusicMode];
        [AppDelegate sharedInstance].isAppPlayerPlaying = @"";
           [appDelegate playMusic];
            [AppDelegate sharedInstance].isFromPlayList = @"YES";
    } else {

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kMusicMode];
        [appDelegate pauseMusic];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)shuffleModeSwitchChanged:(id)sender {
    
    if ([self.switchShuffle isOn]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kSuffleMode];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kRepeatMode];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];

}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)repeatModeSwitchChanged:(id)sender {

    if ([self.switchRepeat isOn]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRepeatMode];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kRepeatMode];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : TableView Delegate 

//---------------------------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.arrayPlayList count]!=0) {
        return [self.arrayPlayList count];
    } else {
        return 1;
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.arrayPlayList count]!=0) {
        if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark) {
            self.selectedRow = indexPath;
        }
        else {
            [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
            [[tableView cellForRowAtIndexPath:self.selectedRow] setAccessoryType:UITableViewCellAccessoryNone];
            NSString * strPlaylist = [[self.arrayPlayList objectAtIndex:indexPath.row] valueForProperty:MPMediaPlaylistPropertyName];
            [[NSUserDefaults standardUserDefaults] setValue:strPlaylist forKey:kPlayListName];
            [[NSUserDefaults standardUserDefaults] synchronize];
             appDelegate.isPause = NO;
            self.selectedRow = indexPath;
        }		
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:self.selectedRow.row] forKey:@"SelectedSong"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([self.switchMusic isOn]) {
        NSString * isTimerOn = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kIsTimerPlaying]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kMusicMode];
        [AppDelegate sharedInstance].isAppPlayerPlaying = @"";
           [appDelegate playMusic];
    }
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : TableView Datasource Method

//---------------------------------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld",(long)indexPath.section,(long)indexPath.row];
    
    UITableViewCell * cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    NSString * str = [[NSUserDefaults standardUserDefaults] valueForKey:kPlayListName];
    
    
    if ([self.arrayPlayList count]!=0) {
        if ([str isEqualToString:[[self.arrayPlayList objectAtIndex:indexPath.row] valueForProperty:MPMediaPlaylistPropertyName]]) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            self.selectedRow = indexPath;
        }
        cell.textLabel.text = [[self.arrayPlayList objectAtIndex:indexPath.row] valueForProperty:MPMediaPlaylistPropertyName];
    }
    else {
        cell.textLabel.text = NSLocalizedString(@"No Playlist found with traks.", nil);
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    }	
    
    return cell;
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : View Lifecycle Method

//---------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupLayout];
    
}

//---------------------------------------------------------------------------------------------------------------
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.tblPlaylist reloadData];
    [self localizedControls];
}


//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
