//
//  DatePickerVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DateParQDelegate

@optional
-(void) doneWithDate;
-(void) getDateFormPicker:(NSString *)date1 :(int)tag1;
-(void) closeDatePopover;

@end

@interface DatePickerVC : UIViewController {
    
}

@property (nonatomic , strong) id <DateParQDelegate>                datePickDelegate;

@property (nonatomic , strong) NSString                                         *dateString;
@property (nonatomic , strong) NSString                                         * date1;
@property(nonatomic)int                                                                   dateflag;
@property (nonatomic )  int                                                               tag1;
@property (nonatomic) BOOL                                                           isFromParQ;
@property (nonatomic) BOOL                                                           flag;

@end
