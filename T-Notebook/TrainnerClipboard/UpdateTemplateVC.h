//
//  UpdateTemplateVC.h
//  T-Notebook
//
//  Created by WLI on 29/06/15.
//  Copyright (c) 2015 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UpdateTemplateNameDelegate
-(void) updateTemplateName:(NSString *)templateName;

@end

@interface UpdateTemplateVC : BaseViewController

@property (nonatomic, unsafe_unretained) id<UpdateTemplateNameDelegate>	delegate;
@property (nonatomic, strong) NSString *oldTemplateName;

@end
