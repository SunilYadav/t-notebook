//
//  ViewAllProgramVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Show all program in one screen


#import "ViewAllProgramVC.h"
#import "ViewProgramCell.h"
#import "UpdateNotesVC.h"
#import "ClientViewController.h"

static ViewAllProgramVC *sharedInstance = nil;

@interface ViewAllProgramVC () <UITableViewDataSource,UITableViewDelegate,updateNoteDelegate>
{
    ProgramViewController *programVCOBJ;
    ClientViewController *ClientVCOBJ;
    __weak IBOutlet UIView * viewheaderView;
    __weak IBOutlet UIView * viewTopToTakeScreenShot;
    IBOutlet UILabel *lblProName;
    IBOutlet UIImageView *imgvFirstLine;
    IBOutlet UILabel *lblDay;
    IBOutlet UIImageView *imgvSecondLine;
    IBOutlet UILabel *lblNotes;
    IBOutlet UIImageView *imgvThirdLine;
    IBOutlet UIView *mainView;
    NSString *_strExistingNotes;
    
    int intIndex;

}
@property (nonatomic) NSMutableArray *arrSheetsList;
@property (nonatomic) NSArray *arrWorkoutList,*viewAllArray;
@property (nonatomic, strong) NSArray                     * arrayBlockDates;
@property (nonatomic) UIGestureRecognizer                 *tapGesture;
@property (nonatomic) NSInteger                           currentSheetId;
@property (nonatomic) NSInteger							  currentSheet;
@property (nonatomic) NSInteger							  blockID;
@property (nonatomic) NSInteger							  blockNo;

@property (weak, nonatomic) IBOutlet UITableView *viewallTable;
@property (weak, nonatomic) IBOutlet UIView *viewAllProgView;
@property (weak, nonatomic) IBOutlet UILabel *lblProgram;
@property (weak, nonatomic) IBOutlet UIImageView *clientImg;
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (weak, nonatomic) IBOutlet UILabel *lblProPurpose;
@property (weak, nonatomic) IBOutlet UIButton *btnPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnMail;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *btnShowAllDetails;


@property NSInteger selectedLabelTag;
@end

@implementation ViewAllProgramVC



//-----------------------------------------------------------------------

#pragma mark - Memory Management Methods

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-----------------------------------------------------------------------

#pragma mark - Class Methods

//-----------------------------------------------------------------------

+ (ViewAllProgramVC *)sharedInstance {

    static dispatch_once_t once;
    static ViewAllProgramVC *program;
    dispatch_once(&once, ^{
        program = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"viewall"];
    });
    return program;

}

//-----------------------------------------------------------------------

#pragma mark - Tableview Methods

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

//-----------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    START_METHOD
    intIndex = 0;
return  self.arrSheetsList.count;
}


//-----------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *currentSheetID = [[self.arrSheetsList objectAtIndex:section] valueForKey:@"sheetID"] ;
    
   // NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:currentSheetID]];
    
     //Sunil
    NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID]];
    
    MESSAGE(@"arrayRowCount-> %@",arrayRowCount);
    self.arrayBlockDates = [NSArray arrayWithArray:arrayRowCount];
    return arrayRowCount.count;
}

//-----------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    intIndex    =   intIndex+1;
    
    MESSAGE(@"intIndex---> %d",intIndex);
    
    
    ViewProgramCell *cell = (ViewProgramCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell.btnShowDetail setTitle:NSLocalizedString(cell.btnShowDetail.titleLabel.text, nil) forState:0];
    [cell setBackgroundColor:[UIColor clearColor]];
    [tableView setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"tableHorizontalLine"]]];
    
    NSString *currentSheetID = [[self.arrSheetsList objectAtIndex:indexPath.section] valueForKey:@"sheetID"] ;
  //  NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:currentSheetID]];
    //Sunil
    NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID]];
    
    NSMutableDictionary *objDictNotes = [[arrayRowCount objectAtIndex:indexPath.row] mutableCopy];
    
    NSArray *arrayWorkoutName = [NSArray arrayWithArray:[[DataManager initDB] getWorkoutNameForNotes:self.strClientId andBlockNo:[objDictNotes valueForKey:@"nBlockNo"] andSheetId:currentSheetID]];
    
    NSDictionary *objDictWorkoutName = [arrayWorkoutName objectAtIndex:0];
    MESSAGE(@"arrayRowCount==>: %@ and arrayWorkoutName:%@ objDictWorkoutName: %@ objDictWorkoutName: %@",arrayRowCount,arrayWorkoutName,[objDictWorkoutName valueForKey:@"sBlockTitle"],objDictWorkoutName);
    
    [objDictNotes setValue:[objDictWorkoutName valueForKey:@"sBlockTitle"] forKey:@"sBlockTitle"];
    
      if(self.setAsLandscape) {
          cell.lblProgramName.frame = CGRectMake(38,12 ,170 , 44);
          cell.imgvFirstLine.frame = CGRectMake(250,0 ,1 , 60);
          cell.lblDayDate.frame = CGRectMake(280,12 ,100 , 44);
          cell.imgvSecondLine.frame = CGRectMake(422,0 ,1 , 60);
          cell.lblNotes.frame = CGRectMake(430,12 ,300 , 44);
          cell.imgvThirdLine.frame = CGRectMake(730,0 ,1 , 60);
          cell.btnShowDetail.frame = CGRectMake(770,12 ,109 , 35);
          
        
      }
    else
    {
        cell.lblProgramName.frame = CGRectMake(22,12 ,180 , 44);
        cell.imgvFirstLine.frame = CGRectMake(200,0 ,1 , 60);
        cell.lblDayDate.frame = CGRectMake(230,12 ,100 , 44);
        cell.imgvSecondLine.frame = CGRectMake(349,0 ,1 , 60);
        cell.lblNotes.frame = CGRectMake(366,12 ,207 , 44);
        cell.imgvThirdLine.frame = CGRectMake(589,0 ,1 , 60);
        cell.btnShowDetail.frame = CGRectMake(605,12 ,109 , 35);
    }
    
    if (arrayRowCount.count) {
        [cell configCell:objDictNotes];
        
    }
    
    
    [cell.lblNotes setTag:indexPath.row];
    [cell.lblNotes setUserInteractionEnabled:YES];
    
    
    //For updated version app
    if (strUpgradeStatus && [strUpgradeStatus isEqualToString:@"YES"]) {
    
    //sunil for use in Show button deatil
    NSString *strBlockNo = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
//    [cell.btnShowDetail setTag:strBlockNo.intValue];
        
        
//        NSArray *arrayRowCountTag = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID andBlock:[objDictNotes valueForKey:@"nBlockNo"]]];
        
        //SUNIl: 25 Oct for Show deatil button tag
        NSArray *arrayRowCountTag = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:currentSheetID]];
        
                MESSAGE(@"arrayRowCountTag--> %@",arrayRowCountTag);
                                     
        
        for(int i= 0  ; i< arrayRowCountTag.count;i++){
            NSDictionary *objDictTag = [arrayRowCountTag objectAtIndex:i];
            
            if([[objDictNotes objectForKey:@"sBlockTitle"] isEqualToString:[objDictTag objectForKey:@"sBlockTitle"]]  &&   [[objDictNotes valueForKey:@"nBlockNo"] isEqualToString: [objDictTag valueForKey:@"nBlockNo"]]  ){
                
                [cell.btnShowDetail setTag:i];
                break;
                
            }
        }
            
        
        
    //    [cell.btnShowDetail setTag:indexPath.section];
    
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPopoverForNotes:)];
    [cell.lblNotes addGestureRecognizer:self.tapGesture];
    
    //sunil
    self.tapGesture.view.tag = strBlockNo.intValue ;
    }else{
        
        //sunil for use in Show button deatil
        NSString *strBlockNo = [objDictNotes valueForKey:@"nBlockNo"];
        [cell.btnShowDetail setTag:strBlockNo.intValue -1];
        //    [cell.btnShowDetail setTag:indexPath.section];
        
        
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openPopoverForNotes:)];
        [cell.lblNotes addGestureRecognizer:self.tapGesture];
        
        //sunil
        self.tapGesture.view.tag = strBlockNo.intValue - 1;
        
    }
    return cell;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

//-----------------------------------------------------------------------

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 30)];
    
    [view setBackgroundColor:[UIColor grayColor]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 4, 340, 20)];
    [label setText:[[self.arrSheetsList objectAtIndex:section] valueForKey:@"sheetName"]];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    [view addSubview:label];
    return view;
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc]initWithFrame:CGRectZero];
}


#pragma mark - Custom Methods


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)setRoundRectForView :(UIView *) view withCorner :(UIRectCorner )rectCorner {
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                     byRoundingCorners:rectCorner
                                           cornerRadii:CGSizeMake(8.0,8.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    view.layer.masksToBounds = YES;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)prepareArray
{
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //TODO: SUNIL CONDTION
    if(strUpgradeStatus && [strUpgradeStatus isEqualToString:@"YES"]){
    self.blockID = 1;
    _currentSheet = 1;
    _currentSheetId = 1;
    }
    
    
    if (_arrSheetsList == nil) {
        _arrSheetsList = [[NSMutableArray alloc] init];
    }
    [_arrSheetsList removeAllObjects];
    
    [self.arrSheetsList setArray:[[DataManager initDB] getProgramSheetList:self.strClientId]];
      self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
   
    /*
    //Sunil
    self.arrayBlockDates = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:[NSString stringWithFormat:@"%ld",(long)_currentSheetId]]];
    */
    
    //TODO : Sunil 9 sept 2016
    NSString *strProgramName = [[self.arrSheetsList objectAtIndex:_currentSheetId-1] valueForKey:@"sheetName"];
    
    NSDictionary * clientInfo = [[DataManager initDB] getClientsDetail:self.strClientId];

    self.lblProgram.text = [[self.arrSheetsList objectAtIndex:_currentSheetId-1] valueForKey:@"sheetName"];
    self.clientName.text = [NSString stringWithFormat:@"%@ %@", [clientInfo objectForKey:kFirstName],[clientInfo objectForKey:kLastName]];
    [self.lblProPurpose setText:[[self.arrSheetsList objectAtIndex:self.currentSheetId-1] valueForKey:@"sPurpose"]];
    if ([[clientInfo objectForKey:kPday] isEqualToString:@""] || [[clientInfo objectForKey:kPday] isEqualToString:@" "])
    {
        [self.btnPhone setTitle:NSLocalizedString(@"Not Available", nil) forState:UIControlStateNormal];

    }
    else
    {
        [self.btnPhone setTitle:[clientInfo objectForKey:kPday] forState:UIControlStateNormal];

    }
    
    if (![[clientInfo objectForKey:kEmail1] isEqualToString:@" "])
    {
        [self.btnMail setTitle:[clientInfo objectForKey:kEmail1] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnMail setTitle:NSLocalizedString(@"Not Available", nil) forState:UIControlStateNormal];

    }

    
    NSString * imgPath = [NSString stringWithFormat:@"%@/ClientsProfileImage/ProfileImage%@.png",[FileUtility basePath],self.strClientId];
    UIImage * img = [UIImage imageWithContentsOfFile:imgPath];
    if(img == nil) {
        img =  [UIImage imageNamed:@"ClientPlaceHolderBig"];
    }
    self.clientImg.image =  img;
    CALayer *layer = [self.clientImg layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:41.0];
    
    [layer setBorderWidth:1.0];
    [layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
}

//-----------------------------------------------------------------------

-(void)openPopoverForNotes :(UITapGestureRecognizer* )gesture {
   
    START_METHOD
//    [commonUtility alertMessage:@"Under Development!"];
//    return;
    
    CGPoint location = [gesture locationInView:self.viewallTable];
    NSIndexPath *selectedIndexPath = [self.viewallTable indexPathForRowAtPoint:location];
    self.blockID = gesture.view.tag +1;
    
    NSString *currentSheetID = [[self.arrSheetsList objectAtIndex:selectedIndexPath.section] valueForKey:@"sheetID"] ;
    self.currentSheetId =[currentSheetID integerValue];
    
//    NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:currentSheetID]];
     NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID]];
    /*
    //Sunil
        NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID]];
    */
    self.arrayBlockDates = [NSArray arrayWithArray:arrayRowCount];
    
    MESSAGE("self.arrayBlockDates-> %@ and gesture.view.tag: %ld",self.arrayBlockDates,gesture.view.tag);
    
    
    self.blockID = [[[self.arrayBlockDates objectAtIndex:gesture.view.tag]valueForKey:@"nBlockID"] integerValue];
        self.blockNo = [[[self.arrayBlockDates objectAtIndex:gesture.view.tag]valueForKey:@"nBlockNo"] integerValue];


   UpdateNotesVC *UpdateNotesVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"updatenote"];
   UpdateNotesVCOBJ.delegate = self;
   UINavigationController* updateNotesNavigationController = [[UINavigationController alloc] initWithRootViewController:UpdateNotesVCOBJ];
    [updateNotesNavigationController.navigationBar setTranslucent:NO];
    updateNotesNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    updateNotesNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    if (iOS_8) {
     CGPoint frameSize =  CGPointMake(500, 320);
     updateNotesNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
  
//    UpdateNotesVCOBJ.notes = [[self.arrayBlockDates objectAtIndex:selectedIndexPath.row] valueForKey:@"sBlockNotes"];
        UpdateNotesVCOBJ.notes = [[self.arrayBlockDates objectAtIndex:selectedIndexPath.row] valueForKey:@"sBlockNotes"];

    _strExistingNotes      =UpdateNotesVCOBJ.notes;
    
    MESSAGE(@"self.arrayBlockDates -> %@",self.arrayBlockDates);
    self.selectedLabelTag = gesture.view.tag;
    
    //For If note not already exist
    if(_strExistingNotes.length>0){
        
        [self.view.window.rootViewController presentViewController:updateNotesNavigationController animated:YES completion:nil];
    }else{
        return;
    }
    
}

//-----------------------------------------------------------------------
- (void) localizedControl {
    
    [self.btnBack setTitle:NSLocalizedString(self.btnBack.titleLabel.text, nil) forState:0];
   
    lblProName.text = NSLocalizedString(lblProName.text, nil);
    lblDay.text = NSLocalizedString(lblDay.text, nil);
    lblNotes.text =NSLocalizedString(lblNotes.text, nil);
    
}

//-----------------------------------------------------------------------

#pragma mark - Action Methods
- (IBAction)btnShowDetailClick:(id)sender {
    
    START_METHOD
    
    
//    [commonUtility alertMessage:@"Under Development!"];
//    return;
    
    UIButton *btn = (UIButton *) sender;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.viewallTable];
    NSIndexPath *indexPath = [self.viewallTable indexPathForRowAtPoint:buttonPosition];

    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"isFromViewAllTable"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[self.viewallTable layer] removeAnimationForKey:@"UITableViewReloadDataAnimationKey"];
    [self.delegate changeOptionScreen];
    
    programVCOBJ = [ProgramViewController sharedInstance];
    ClientVCOBJ = [ClientViewController sharedInstance];
    
    if ([ClientVCOBJ.backgroundAddView.subviews containsObject:programVCOBJ.view]) {
        
    } else{
        
        [ClientVCOBJ.backgroundAddView addSubview:programVCOBJ.view];
    }
    

    
    [self.view.layer removeAnimationForKey:@"ViewAllAnimation"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.50;
    transition.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    
    [programVCOBJ.view.layer addAnimation:transition forKey:@"ProgramVCAnimation"];
    programVCOBJ.isViewAllVisible = NO;
    [self.view setHidden:YES];
    
    NSString *currentSheetID = [[self.arrSheetsList objectAtIndex:indexPath.section] valueForKey:@"sheetID"] ;
 NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:currentSheetID]];
    //Sunil
    //NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID]];
    
    self.arrayBlockDates = [NSArray arrayWithArray:arrayRowCount];
    
    MESSAGE(@"arrayRowCount-> %@ and btn.tag: %ld",arrayRowCount,(long)btn.tag);
    
    for (int i =0; i<self.arrayBlockDates.count; i++) {
        MESSAGE(@"self.arrayBlockDates dict> %@",[self.arrayBlockDates objectAtIndex:i]);

    }
    
    
    
    for (int i =0; i<self.arrSheetsList.count; i++) {
        MESSAGE(@"arrSheetsList dict> %@",[self.arrSheetsList objectAtIndex:i]);
        
    }
    
  
    
   int intRowNumber     =   ([self getIndexOfarray:(int)indexPath.section] + (int)btn.tag + 1);
    
    MESSAGE(@"intRowNumber----> %d",intRowNumber);
    
    
     programVCOBJ.selectedSheet = [[self.arrayBlockDates objectAtIndex:btn.tag] valueForKey:@"nSheetID"];
     programVCOBJ.strSelectedSheet = [NSString stringWithFormat:@"%d",intRowNumber];
    programVCOBJ.blockSheetNo = [[[self.arrayBlockDates objectAtIndex:btn.tag] valueForKey:@"nBlockNo"] integerValue] ;
    programVCOBJ.currentBlockNo = (int)btn.tag;
    [programVCOBJ.view setHidden:NO];
    [programVCOBJ viewWillAppear:YES];
    [programVCOBJ.view.layer removeAnimationForKey:@"ProgramVCAnimation"];
    
    
    //SUNIL change skin 7 Apr
    [self setBackgroundImageOfLastView];
    
    END_METHOD
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)reloadData:(BOOL)animated with:(NSString *)string
{
    [self.viewallTable reloadData];
    
    if (animated) {
        
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        if ([string isEqualToString:@"Next"]) {
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [animation setSubtype:kCATransitionFromRight];
        } else {
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
            [animation setSubtype:kCATransitionFromLeft];
        }
        
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:0.4];
        [[self.viewallTable layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction)PrevNextBtnTap:(id)sender {
    if([sender tag] == 501) {
        if (self.currentSheetId> 1)
        {
           self.currentSheet = self.currentSheet-1;
           self.currentSheetId = self.currentSheetId-1;
            [self prepareArray];
            self.viewallTable.delegate = self;
            self.viewallTable.dataSource = self;
            [self reloadData:YES with:@"Prev"];

        }
        
    } else{
        if (self.currentSheet < [self.arrSheetsList count])
        {
            self.currentSheet = self.currentSheet+1;
            self.currentSheetId = self.currentSheetId+1;
            [self prepareArray];
            self.viewallTable.delegate = self;
            self.viewallTable.dataSource = self;
            [self reloadData:YES with:@"Next"];

        }
        
    }
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction)btnViewAllTap:(id)sender {
    
       [[self.viewallTable layer] removeAnimationForKey:@"UITableViewReloadDataAnimationKey"];
       [self.delegate changeOptionScreen];
    
        programVCOBJ = [ProgramViewController sharedInstance];
        ClientVCOBJ = [ClientViewController sharedInstance];
    
  
    
       if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
           programVCOBJ.setAsLandscape = TRUE;
           [programVCOBJ setUpLandscapeOrientation];
           
           
       } else {
           programVCOBJ.setAsLandscape = FALSE;
           [programVCOBJ setupPortraitOrientation];
          
       }
    
        if ([ClientVCOBJ.backgroundAddView.subviews containsObject:programVCOBJ.view]) {
            
        }
        else{
            
            CGRect backgroundRect = ClientVCOBJ.backgroundAddView.frame;
            backgroundRect.size.height = 690;
            
            [ClientVCOBJ.backgroundAddView setFrame:backgroundRect]; //programBack_Landscape
            
            UIRectCorner rectCorner2 = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            [self setRoundRectForView:ClientVCOBJ.backgroundAddView withCorner:rectCorner2];

            [ClientVCOBJ.backgroundAddView addSubview:programVCOBJ.view];
            [ClientVCOBJ.backgroundAddView setBackgroundColor:[UIColor clearColor]];

        }
    
        [self.view.layer removeAnimationForKey:@"ViewAllAnimation"];
        CATransition *transition = [CATransition animation];
        transition.duration = 0.50;
        transition.timingFunction =
        [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        transition.type = kCATransitionMoveIn;
        transition.subtype = kCATransitionFromLeft;
    
      [programVCOBJ.view.layer addAnimation:transition forKey:@"ProgramVCAnimation"];
      programVCOBJ.isViewAllVisible = NO;
      [self.view setHidden:YES];
      [programVCOBJ.view setHidden:NO];
      [programVCOBJ.view.layer removeAnimationForKey:@"ProgramVCAnimation"];
    
    
    //Sunil 7 Apr
    
    [self setBackgroundImageOfLastView];
    }


//-----------------------------------------------------------------------

- (IBAction)copyBtnTap:(id)sender {
    
    UIButton *selectedBtn = (UIButton*)sender;
    if ([selectedBtn.currentImage isEqual:[UIImage imageNamed:@"copy"]]) {
        [selectedBtn setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
    }
    else{
        [selectedBtn setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
    }
}

//-----------------------------------------------------------------------
- (IBAction)DeleteBtnTap:(id)sender {
}

//-----------------------------------------------------------------------

-(UIImage*)screenShotForView:(UIView*)viewToTakeScreenShot{
    
    CGRect  croppingRect = viewToTakeScreenShot.bounds;
    
    UIGraphicsBeginImageContextWithOptions(croppingRect.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //SUNIL SET IMAGE BACK GROUND COLR
    int intSkin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (intSkin) {
        case FIRST_TYPE:
        SECOND_TYPE:
            [[UIColor colorWithRed:25/255.f green:33/255.f blue:39/255.f alpha:1.0f] set];
            CGContextFillRect(context, croppingRect);
            
            break;
            
        case THIRD_TYPE:
            [[UIColor colorWithRed:247/255.f green:245/255.f blue:242/255.f alpha:1.0f] set];
            CGContextFillRect(context, croppingRect);
            
            break;
        default:
            [[UIColor colorWithRed:191/255.f green:196/255.f blue:200/255.f alpha:1.0f] set];
            CGContextFillRect(context, croppingRect);
            
            break;
    }
    
    
    if (context == NULL) return nil;
    CGContextTranslateCTM(context, -croppingRect.origin.x, -croppingRect.origin.y);
    
    [viewToTakeScreenShot layoutIfNeeded];
    [viewToTakeScreenShot.layer renderInContext:context];
    
    UIImage *screenshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenshotImage;
    
}

//-----------------------------------------------------------------------

-(void)takeScreenShot{
    
    viewheaderView.backgroundColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    UIImage *imageScreenShort =  [self screenShotAtTableViewAllIndexPaths];
    viewheaderView.backgroundColor = [UIColor whiteColor];
    [self.delegate openMailForScreenShotImage:imageScreenShort];
}

//-----------------------------------------------------------------------

- (UIImage *)screenShotAtTableViewAllIndexPaths
{
    NSMutableArray *screenshots = [NSMutableArray array];
    
    UIImage *topViewScreenshot = [self screenShotForView:viewTopToTakeScreenShot];
    if (topViewScreenshot) [screenshots addObject:topViewScreenshot];
    
    NSInteger numberOfrows = [self.viewallTable numberOfRowsInSection:0];
    
    for (int row=0; row<numberOfrows; row++) {
        NSIndexPath *cellIndexPath = [NSIndexPath indexPathForRow:row inSection:0];
        UIImage *cellScreenshot = [self screenshotOfCellAtIndexPath:cellIndexPath];
        if (cellScreenshot) [screenshots addObject:cellScreenshot];
    }

    return [self verticalImageFromArray:screenshots];
}

//-----------------------------------------------------------------------

- (CGSize)verticalAppendedTotalImageSizeFromImagesArray:(NSArray *)imagesArray
{
    CGSize totalSize = CGSizeZero;
    for (UIImage *im in imagesArray) {
        CGSize imSize = [im size];
        totalSize.height += imSize.height;
        totalSize.width = MAX(totalSize.width, imSize.width);
    }
    return totalSize;
}

//-----------------------------------------------------------------------

-(UIImage *)verticalImageFromArray:(NSArray *)imagesArray
{
    UIImage *unifiedImage = nil;
    CGSize totalImageSize = [self verticalAppendedTotalImageSizeFromImagesArray:imagesArray];
    UIGraphicsBeginImageContextWithOptions(totalImageSize, NO, 0.f);
    int imageOffsetFactor = 0;
    for (UIImage *img in imagesArray) {
        [img drawAtPoint:CGPointMake(0, imageOffsetFactor)];
        imageOffsetFactor += img.size.height;
    }
    
    unifiedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return unifiedImage;
}

//-----------------------------------------------------------------------

- (UIImage *)screenshotOfCellAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *cellScreenshot = nil;
    
    CGPoint currTableViewOffset = self.viewallTable.contentOffset;
    
    [self.viewallTable scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:NO];
    
    // Take the screenshot
    UIView *viewCell = [self.viewallTable cellForRowAtIndexPath:indexPath];
    
    cellScreenshot = [self screenShotForView:viewCell];
    
    // scroll back to the original offset
    [self.viewallTable setContentOffset:currTableViewOffset animated:NO];
    
    return cellScreenshot;
}

//-----------------------------------------------------------------------

-(void)reloadViewAllTable{
    self.blockID = 1;
    _currentSheet = 1;
    _currentSheetId = 1;
    [self prepareArray];
    [self.viewallTable reloadData];
}

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        // landscape orientation
        [self setUpLandscapeOrientation];
        
        
    } else {
        // portrait orientation
        [self setupPortraitOrientation];
        
    }
}

//-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
    
    self.viewAllProgView.frame = CGRectMake(0, 0,1000,560);
    
    UIRectCorner corner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    [self setRoundRectForView:self.viewAllProgView withCorner:corner];
    
    [self.viewAllProgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg_iPad"]]];
    mainView.frame =CGRectMake(0, 0, 1000, 560);
    self.view.frame =CGRectMake(0, 0, 1000, 560);
    self.clientName.frame = CGRectMake(0, 111, 915, 28);
    self.lblProPurpose.frame = CGRectMake(0, 137, 915, 26);
    self.clientImg.frame = CGRectMake(420, 20, 83, 83);
    
    viewheaderView.frame = CGRectMake(1, 170, 920, 32);
    self.viewallTable.frame = CGRectMake(1, 203, 923, 360);
    
    lblProName.frame = CGRectMake(38, 8, 170, 21);
    imgvFirstLine.frame = CGRectMake(250, 0, 1, 32);
    lblDay.frame = CGRectMake(251, 8, 170, 21);
    imgvSecondLine.frame = CGRectMake(422, 0, 1, 32);
    lblNotes.frame = CGRectMake(423, 8, 307, 21);
    imgvThirdLine.frame = CGRectMake(730, 0, 1, 32);
    viewTopToTakeScreenShot.frame = CGRectMake(0, 0, 999, 261);

    [self.viewallTable reloadData];
    


    
}

//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
    
    
    
    self.viewAllProgView.frame = CGRectMake(0, 0, 730, 800);
    UIRectCorner corner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    [self setRoundRectForView:self.viewAllProgView withCorner:corner];

    
    [self.viewAllProgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg"]]];

    mainView.frame =CGRectMake(0, 0, 730, 800);
    self.view.frame =CGRectMake(0, 0, 730, 800);

    self.clientName.frame = CGRectMake(0, 111, 730, 28);
    self.lblProPurpose.frame = CGRectMake(0, 137, 730, 26);
    self.clientImg.frame = CGRectMake(323, 20, 83, 83);
    
    viewheaderView.frame = CGRectMake(1, 165, 730, 32);
    self.viewallTable.frame = CGRectMake(2, 198, 730, 595);
    
   lblProName.frame = CGRectMake(38, 8, 120, 21);
   imgvFirstLine.frame = CGRectMake(200, 0, 1, 32);
   lblDay.frame = CGRectMake(207, 8, 140, 21);
    imgvSecondLine.frame = CGRectMake(350, 0, 1, 32);
    lblNotes.frame = CGRectMake(365, 8, 207, 21);
    imgvThirdLine.frame = CGRectMake(590, 0, 1, 32);
    viewTopToTakeScreenShot.frame = CGRectMake(0, 0, 729, 261);
    [self.viewallTable reloadData];

   }


//-----------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.viewallTable.backgroundColor = [UIColor clearColor];
    [self.viewallTable setDelegate:self];
    [self.viewallTable setDataSource:self];
    self.viewallTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    intIndex = 0;
    
    
    // Do any additional setup after loading the view.
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    
    intIndex = 0;
    
    self.blockID = 1;
    _currentSheet = 1;
    _currentSheetId = 1;
    [self prepareArray];
    [self.viewallTable reloadData];
    [self localizedControl];
    [super viewWillAppear:animated];
   
  
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [self.viewAllProgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg_iPad"]]];
        [self setUpLandscapeOrientation];
    } else {
        [self.viewAllProgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg"]]];
        [self setupPortraitOrientation];
    }

    
}


//-----------------------------------------------------------------------
//Sunil commented 

-(void)updatedNote:(NSString *)updateString {
    
    START_METHOD
    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
//    NSInteger success = [[DataManager initDB] updateNotes:updateString blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)self.blockID] forClient:self.strClientId];
//  

    updateString= [updateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (updateString.length>0) {
        
        
        if(strUpgradeStatus &&  [strUpgradeStatus  isEqualToString:@"YES"]){
            
            //TODO: SUNIL-> Update Notes
            //Craete dict
            NSDictionary *dictNote =@{
                                      kBlockNotes : updateString,
                                      ksheetID:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                      PROGRAM_ID:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId],
                                      kBlockNo :[NSString stringWithFormat:@"%ld",(long)self.blockNo],
                                      kClientId :self.strClientId,
                                      kBlockId:[NSString stringWithFormat:@"%ld",(long)self.blockID],
                                      @"existingNote":_strExistingNotes,
                                      ACTION:@"updateNotes"
                                      };
            
            //Invoke method for post request for Add notes
            [self postRequestForNotes:[dictNote mutableCopy]];
            
        }else{
            
            NSInteger success = [[DataManager initDB] updateNotes:updateString blockTitle:[NSString stringWithFormat:@"%ld",(long)self.currentSheetId] ForBlock:[NSString stringWithFormat:@"%ld",(long)self.blockID]   forClient:self.strClientId andexistingNotes: _strExistingNotes];
            
            
            [self prepareArray];
            [self.viewallTable reloadData];
            
        }
        
        
    }else{
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text", nil), kAppName);

    }
  

}



-(void)optionsButtonClicked:(id)sender {
}


//For set Lst view background image after go from here
-(void)setBackgroundImageOfLastView{
    START_METHOD
    //Sunil ->
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    
    switch (skin) {
        case FIRST_TYPE:

            [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg_green.png"]]];
            break;
            
        case SECOND_TYPE:
            [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg_green.png"]]];
            break;
            
        case THIRD_TYPE:
            [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"white_assessmentbg-1.png"]]];
            break;
            
        default:{
         
            if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)){
                [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg1_iPad.png"]]];

            }else{
            [ programVCOBJ.programView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"programbg"]]];
            }
            
        }
            
            break;
    }
    END_METHOD
}


//TODO: SUNIL-> POST REQUEST TO SERVER



//Method for post request for Add new workout
-(void)postRequestForNotes:(NSMutableDictionary *)dictNotes{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictNotes,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictNotes
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictNoteTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictNotes,dictNoteTemp);
                                   
                                   NSString *strAction =   [dictNoteTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"updateNotes"]){
                                       
                                       [self updateNote:dictNotes];
                                   }
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                               
                               
                           }];
    END_METHOD
}


//MEthod for post the request to update the notes
-(void)updateNote:(NSDictionary *)dictNote{
    
    MESSAGE(@"updateNote-> dict: %@",dictNote);
    
    NSInteger success = [[DataManager initDB] updateNotes:[dictNote objectForKey:kBlockNotes] blockTitle:[dictNote objectForKey:PROGRAM_ID] ForBlock:[dictNote objectForKey:kBlockNo] forClient:self.strClientId andexistingNotes: _strExistingNotes];
    
    
    [self prepareArray];
    [self.viewallTable reloadData];
    
}

//Count total number of rows in till section which tap button

-(int)getIndexOfarray:(int)intSection{
    
    int intRowNumber=   0;
    
    for (int i=0; i<intSection; i++) {
     
        NSString *currentSheetID = [[self.arrSheetsList objectAtIndex:intSection] valueForKey:@"sheetID"] ;
        
        // NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientId SheetId:currentSheetID]];
        
        //Sunil
        NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getNotesForClient:self.strClientId andSheets:currentSheetID]];
        
        
        if(arrayRowCount && arrayRowCount.count>0){
        NSDictionary *dictTemp  =   [arrayRowCount objectAtIndex:0];
        NSString *str   =   [dictTemp objectForKey:@"sBlockNotes"];
        
            if(str.length>1){
                    intRowNumber    =   intRowNumber+ (int)arrayRowCount.count;
            }
        }
        
    }
    return intRowNumber;
    
}


@end
