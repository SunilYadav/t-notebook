//
//  HelperClass.h
//  
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>


#ifdef DEBUG

#define  DLOG(fmt, ...)  NSLog(@"%s: " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#define DCGRect(str, rect)  NSLog(@"%@:- %.02f, %.02f, %.02f, %.02f", str, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height)
#define DCGPoint(str, point)  NSLog(@"%@:- %.02f, %.02f", str, point.x, point.y)
#else
#define  DLOG(...)

#endif

// Localization Macro
#define LocalizedString(str) NSLocalizedString(str, @"")

#define IOS7 (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)?NO:YES
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

// UIAlertView methods

//alert with only message
#define DisplayAlert(msg) { UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(msg,nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil]; [alertView show]; }

//alert with only message
#define DisplayAlertWithYesNo(msg,title,self) { UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:NSLocalizedString(msg,nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Yes",nil) otherButtonTitles:NSLocalizedString(@"No",nil),nil]; [alertView show];[alertView setTag:5000]; }

//alert with message and title
#define DisplayAlertWithTitle(msg,title){UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:NSLocalizedString(msg,nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil]; [alertView show];}
//alert with only localized message
#define DisplayLocalizedAlert(msg){UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(msg,@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil]; [alertView show]; }
//alert with localized message and title
#define DisplayLocalizedAlertWithTitle(msg,title){UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title,@"") message:NSLocalizedString(msg,@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil]; [alertView show]; }

// Firmware methods

#define DeviceID [[UIDevice currentDevice] uniqueIdentifier]
#define OSVersion [[[UIDevice currentDevice] systemVersion] intValue]
#define DeviceName [UIDevice currentDevice].name

#define RANDOM(minNumber, maxNumber) random() % (maxNumber-minNumber+1) + minNumber