//
//  ViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "FileUtility.h"
#import "AddNewTemplateVC.h"

@interface ViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,AddNewTemplateDelegate,UIPopoverControllerDelegate>
{
    UIPopoverController     * _popOverForTemplate;

}
-(IBAction)btnAddNewClientClick;
- (IBAction) dailyBtnClicked;
- (IBAction) weeklyBtnClicked;
- (IBAction) timerBtnClicked;

+(ViewController *)sharedInstance;

@end

