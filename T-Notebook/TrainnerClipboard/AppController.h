//
//  AppController.h
//  
//
//  Created by Sunil on 21/05/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

#define TheAppController ((AppController *)[[UIApplication sharedApplication] delegate])
#define TheAppDelegate   ((AppDelegate *)[[UIApplication sharedApplication] delegate])


@interface AppController : UIResponder

/*
 [TheAppController showHUDonView:nil];
 [TheAppController hideHUDAfterDelay:0];
 */

// Class Helper Methods.------------------------------------------------
/*!         For getting application window.
 @param     nil
 @return    UIWindow
 */
-(UIWindow *)mainWindow;
//----------------------------------------------------------------------
// ProgessHud.----------------------------------------------------------
@property (strong) MBProgressHUD         *sharedHud;

- (MBProgressHUD *)showHUDonView:(UIView *)view;
- (MBProgressHUD *)showHUDonView:(UIView *)view andTheLabel:(NSString *)strTitle;


- (void)hideHUDAfterDelay:(NSTimeInterval)delay;
//----------------------------------------------------------------------

@end
