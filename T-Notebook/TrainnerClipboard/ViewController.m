//
//  ViewController.m
//  T-Notebook
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Display client listing , Program template name , and timer / Home screen


#import "TextFileManager.h"
//For Update traniner Profile
#import "TrainerProfileViewController.h"
#import "SignViewController.h"

//SUNIL
NSString * const VERSION_KEY_FOR_DB = @"version";


#import "ViewController.h"
#import "TemplateCustomeCell.h"
#import "HomeCustomCell.h"
#import "EventNotesCustomCell.h"
#import "TimerViewController.h"
#import "ClientViewController.h"
#import "HelpViewController.h"
#import "AddNameVC.h"
#import "AddNewTemplateVC.h"
#import "PurchaseVC.h"
#import "CStoreKit.h"

#import "ProgramTemplateViewController.h"


@import GoogleMobileAds;


#define kVewBGColor [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]

static ViewController   *vewCntroller;

@interface ViewController ()<CStoreKitDelegate,UIDocumentInteractionControllerDelegate>{
    
    UIDocumentInteractionController *documentInteractionController;
    
    __weak IBOutlet UIButton *btnTimer;
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIView *viewTemplateBackground;
    __weak IBOutlet UILabel *lblNoTemplateFound;
    __weak IBOutlet UIView *viewRecentClients;
    

    __weak IBOutlet UILabel *lblAddNewProgramTemplate;
    IBOutlet UIImageView *imgvBg;
    IBOutlet UIView *viewClient;
    IBOutlet UIButton *btnSetting;
    IBOutlet UILabel *lblNotebook;
    IBOutlet UILabel *lblTraining;
    IBOutlet UIImageView *imgvTopBar;
    IBOutlet UIView *viewCalender;
    IBOutlet UIView *viewMain;
    IBOutlet UIImageView *clientImgv;
    IBOutlet UIView *viewTemplate;
    IBOutlet UIView *viewCalenderTable;
    IBOutlet UIView *viewTimer;
    
    __weak IBOutlet UILabel *lblHelp;
    __weak IBOutlet UILabel *lblClients;
    __weak IBOutlet UILabel *lblLastTrainedClients;
    __weak IBOutlet UILabel *lblDaily;
    __weak IBOutlet UILabel *lblWeekly;
    __weak IBOutlet UILabel *lblCalendar;
    
    __weak IBOutlet GADBannerView *bannerView;
    __weak IBOutlet UIButton *btnRemoveAdd;
    
    
    PurchaseVC     *purchaseCntrol;
    
    //sunil
    
    __weak IBOutlet UIImageView *addClientImageView;
    
    __weak IBOutlet UIImageView *searchIconImageView;
    
    __weak IBOutlet UIImageView *calenderBgImageView;
    
    __weak IBOutlet UIButton *buttonWeekly;
    
    __weak IBOutlet UIButton *buttondaily;


    //TODO: New features propeerty
      
    __weak IBOutlet UIButton *buttonActiveClintes;
    __weak IBOutlet UIButton *buttonInactiveClients;
    __weak IBOutlet UIButton *buttonAllClients;
    
    //For Show List
    int currentList;
}
@property (nonatomic,strong) IBOutlet UITableView				* tblClientDetails;
@property (nonatomic , strong)IBOutlet UITableView              *tblTemplates;
@property (nonatomic,strong) IBOutlet UITableView			    * tblEventManager;
@property (nonatomic,strong) IBOutlet UIButton					* btnAddNewClient;
@property (nonatomic,weak) IBOutlet UIButton					* btnAddTemplate;
@property (nonatomic,strong) NSMutableArray						* arrClientsDetail;
@property (nonatomic,strong) NSMutableArray						* arrSearchData;
@property (nonatomic,strong) NSArray                            * arrTemplateData;
@property (nonatomic,strong) NSMutableArray						* eventsList;
@property (nonatomic,strong) IBOutlet UITextField				* srcClient;
@property (nonatomic,strong) EKEventStore                         * eventStore;
@property (nonatomic,strong) EKCalendar							* defaultCalendar;
@property (nonatomic,strong) NSMutableArray						* arrRecentClientsData;
@property (nonatomic,strong) IBOutlet UIButton					* btnWeekly;
@property (nonatomic,strong) IBOutlet UIButton					* btnDaily;
@property (nonatomic)    int                                                    intForDeleteTemplate;
@property (nonatomic,strong) IBOutlet UIScrollView				* scrlviewEventManger;
@property (nonatomic,strong) IBOutlet UIImageView               * imgvCalender;
@property (nonatomic , weak) IBOutlet    UILabel                *lblTimerLabel;
@property (nonatomic , weak) IBOutlet    UILabel                *lblPhase;
@property (nonatomic , weak) IBOutlet    UIImageView            *imgvewPhase;
@property (nonatomic)    NSInteger                                                     intSheetContainedByTemplate;
@property (nonatomic , strong) UIView                                   *settingsView;
@property (nonatomic , weak) IBOutlet   UIButton                  *btnInfo;
@property (nonatomic , weak) IBOutlet   UILabel                    *lblInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogOut;


@property (nonatomic , strong)  UIPopoverController            *popover;
@property (nonatomic) BOOL                                                  isAdmobAddFailedToLoad;
@property (nonatomic , strong) UIView                                  *dataUpdateLoadView;
@property (nonatomic) BOOL                                          isLandScapeMode;
@property (nonatomic , strong) PurchaseVC                         *purchaseCntrol;



@end

@implementation ViewController

static BOOL isSearch = NO;
static BOOL isEditing = NO;
static int tag = 100;
static NSString * delClientID = @"";
static NSString * currentClientID = @"";


#pragma mark -
#pragma mark Action Method
- (IBAction)btnRemoveAddClicked:(id)sender {
    
    START_METHOD
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        bannerView.frame = CGRectMake(bannerView.frame.origin.x, bannerView.frame.origin.y+bannerView.frame.size.height+10, bannerView.frame.size.width, bannerView.frame.size.height);
    }completion:^(BOOL finished) {
        btnRemoveAdd.hidden = TRUE;
    }];
    END_METHOD
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction) timerBtnClicked
{
    START_METHOD
    //===== check for light and full version to show the timer Functionality =======//
//    
//    
//    if( [[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"] || [[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"]){
//        
//        //Create the objet of the timerview controller for move to that view
//        TimerViewController *timerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TimerViewController"];
//        
//        //Push the view
//        [self.navigationController pushViewController:timerVC animated:YES];
//        
//        return;
//    }
//    
//    
    
    
    //GEt the number of clintes in the current plan user have
    int intNumberOfClinetPlan = [commonUtility retrieveValue:TOTAL_CLIENT].intValue;
    
    
    //App is Not Full version OR Cureent plan is FRee
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"] || intNumberOfClinetPlan==3){
        
        //Invoke method for check free
        [self checkFreePlan];
        
        
    }else{
        
        //Create the objet of the timerview controller for move to that view
        TimerViewController *timerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TimerViewController"];
        
        //Push the view
        [self.navigationController pushViewController:timerVC animated:YES];
        
    }    
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(IBAction)btnAddNewClientClick{
    START_METHOD
    
    MESSAGE(@"Version of app: %@",[commonUtility retrieveValue:kisFullVersion]);
    
    //Check the status of the APP  ful verison or Lite version
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        
        //Check the total cliets >=3
        if ([self.arrClientsDetail count] >= 3) {
            
            //Check the user has upraded
            if([[commonUtility retrieveValue:KEY_UPGRADE_STATUS] isEqualToString:@"YES"]){
                
                //Show Alert for Purchase Subscription
                UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(@"You have limited access to add clients features. In order to add more clients, please purchase a new subscription plan. Would you like to purchase?",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
                
                
                //Set the Tag of alert view
                [alertView setTag:903];
                
                //Show the Alert
                [alertView show];
                
                 return;
            }else{
                
                ///Alert for make payment from the itune store
                //            DisplayAlertWithYesNo(kliteMsg,kAppName,self);  // SUNIL Comment for Update the Profile and migrate the data v_5.0
                
                //Invoke method for move to edit Traner profile
                [self moveToTrainerEditProfileView];
                
                return;
                
            }
            
        }
       

    }
    
    
    //Check the status for add the new clients, If can't add then return   AND in Login API also
    if([[commonUtility retrieveValue:KEY_STATUS_ADD_CLIENT] isEqualToString:@"NO"] && [[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"NO"]){
        
        //-Invoke method for Check Status for Add new client
        [self checkAddClientStatus];
        
        return;
    }
    
    
    //Move to Add clients view
    ClientViewController  *clientVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientView"];
    clientVC.strClientId = @"0";
    clientVC.selectedScreen = kisParq;
    clientVC.isFromClientTable = FALSE;
    [self.navigationController pushViewController:clientVC animated:YES];
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction) dailyBtnClicked
{START_METHOD
    [[NSUserDefaults standardUserDefaults ] setValue:@"Daily" forKey:@"SelectedCalenderEvent"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //Sunil
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            _imgvCalender.image = [UIImage imageNamed:@"green_calendar_tab_daily.png"];
            break;
            
        case SECOND_TYPE:
            _imgvCalender.image = [UIImage imageNamed:@"red_calendar_tab_daily.png"];
            break;
        case THIRD_TYPE:
            _imgvCalender.image = [UIImage imageNamed:@"pinl_calendar_tab_daily.png"];
            break;
   
            
        default:
            [self.imgvCalender setImage:[UIImage imageNamed:@"calender-tab-daily"]];
            break;
    }
    
    
    
    self.tblEventManager.hidden = YES;
    self.scrlviewEventManger.hidden = NO;
    
    [self loadDailyEvents];
    
    
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (IBAction) weeklyBtnClicked
{
    START_METHOD
   
    [self actionOnweeklyButton];
    
}

-(void)actionOnweeklyButton{
    [[NSUserDefaults standardUserDefaults ] setValue:@"Weekly" forKey:@"SelectedCalenderEvent"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            _imgvCalender.image = [UIImage imageNamed:@"green_calendar_tab_weekly.png"];
            break;
            
        case SECOND_TYPE:
            _imgvCalender.image = [UIImage imageNamed:@"red_calendar_tab_weekly.png"];
            break;
        case THIRD_TYPE:
            _imgvCalender.image = [UIImage imageNamed:@"pink_calendar_tab_weekly.png"];
            break;
            
        default:
            [self.imgvCalender setImage:[UIImage imageNamed:@"calender-tab-weekly"]];
            break;
    }
    
    
    self.tblEventManager.hidden = NO;
    self.scrlviewEventManger.hidden = YES;
    [self loadRecentEventsSetType:0];
}
//---------------------------------------------------------------------------------------------------------------

- (IBAction) btnAddNewTemplateClicked:(id)sender{
    
    START_METHOD
    AddNewTemplateVC *addNewTemplateObj = (AddNewTemplateVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddNewTemplateView"];
    addNewTemplateObj.delegateTemplate = (id)self;
    
    UINavigationController *modalController = [[UINavigationController alloc]initWithRootViewController:addNewTemplateObj];
    modalController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    modalController.modalPresentationStyle =  UIModalPresentationFormSheet;
    
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(500, 250);
        modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    
    [self.navigationController presentViewController:modalController animated:YES completion:^{
        
    }];
    
    
}

//---------------------------------------------------------------------------------------------------------------

-(IBAction)btnHelpClicked:(id)sender {
    START_METHOD
    HelpViewController *helpVC = (HelpViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
    [self.navigationController pushViewController:helpVC animated:YES];
}

//---------------------------------------------------------------------------------------------------------------

- (IBAction)btnSettingPageClicked:(id)sender {
    START_METHOD
    if(self.settingsView) {
        [self hideSettingsView];
    } else {
        [self showSettingsView];
    }
}

//---------------------------------------------------------------------------------------------------------------
-(IBAction)infoBtnTapped:(id)sender {
    START_METHOD
    if (purchaseCntrol == nil)
    {
        purchaseCntrol = (PurchaseVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseView"];
        _popover = [[UIPopoverController alloc] initWithContentViewController:purchaseCntrol];
    }
    
    [self.popover setDelegate:(id)self];
    [self.popover setPopoverContentSize:CGSizeMake(600, 400) animated:NO];
    
    CGRect rect =CGRectZero;
    
    if (self.isLandScapeMode) {
        rect = CGRectMake(self.view.center.y +150,self.view.center.x - 220, 50, 50);
    }
    else {
        rect = CGRectMake(self.view.center.x +220,self.view.center.y -150, 50, 50);
    }
    
    
    [self.popover presentPopoverFromRect:rect inView:self.view permittedArrowDirections:0 animated:YES];
    
}


//---------------------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Custom Method

//--------------------------------------------------------------------------------------------------------------------------

-(void) buildNewArray:(NSString *) matchString {
    START_METHOD
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (![[AppDelegate sharedInstance] isNotEmptyArray:self.arrSearchData]) {
            self.arrSearchData=[[NSMutableArray alloc] init];
        }
        else {
            [self.arrSearchData removeAllObjects];
        }
        
        if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrClientsDetail]) {
            for (int i=0; i<[self.arrClientsDetail count]; i++) {
                NSDictionary * dict = [self.arrClientsDetail objectAtIndex:i];
                NSString * nameword =[NSString stringWithFormat:@"%@ %@",[dict objectForKey:kFirstName],[dict objectForKey:kLastName]];
                NSRange range=[nameword rangeOfString:matchString options:NSCaseInsensitiveSearch];
                
                if(range.location != NSNotFound) {
                    if (dict !=nil && [dict count] > 0) {
                        [self.arrSearchData addObject:dict];
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tblClientDetails reloadData];
        });
    });
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//TODO: SUNIL add parameter for check active inactive

- (void) loadAllClientData:(int) intTypeLlist {
    START_METHOD
    // Weblineindia //
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        int intNumberOfClinetPlan = [commonUtility retrieveValue:TOTAL_CLIENT].intValue;

        //App is Not Full version OR Cureent plan is FRee
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"] || intNumberOfClinetPlan==3){
            
            
            MESSAGE(@" getClientsList: 22222 22 ]]");
            self.arrRecentClientsData = [NSMutableArray arrayWithArray:[[DataManager initDB] getClientsList:@"2"]];
            self.tblTemplates.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
            self.tblTemplates.layer.cornerRadius = 2.0;
            self.tblTemplates.tag = 2001;
            self.tblTemplates.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            self.tblTemplates.separatorColor = [UIColor grayColor];
            self.arrTemplateData = nil;
            viewRecentClients.hidden = false;
            self.btnAddTemplate.hidden = true;
            lblAddNewProgramTemplate.hidden = true;
            self.tblTemplates.scrollEnabled = NO;
        } else {
            
            MESSAGE(@" getClientsList: 22222 22  else ]]");

            
            
            self.tblTemplates.tag = 1001;
            
            NSArray *aryTemplete = [[DataManager initDB] getAllTemplatesDetail];
            if([[AppDelegate sharedInstance] isNotEmptyArray:aryTemplete]) {
                self.arrTemplateData = [NSArray arrayWithArray:aryTemplete];
                
            } else {
                self.arrTemplateData = nil;
            }
            
            self.tblTemplates.backgroundColor = [UIColor clearColor];
            self.tblTemplates.separatorStyle = UITableViewCellSeparatorStyleNone;
            
            viewRecentClients.hidden = true;
            self.btnAddTemplate.hidden = false;
            lblAddNewProgramTemplate.hidden = false;
            self.tblTemplates.scrollEnabled = true;
        }
        
        self.arrClientsDetail = nil;
        self.arrClientsDetail = [[NSMutableArray alloc] init];
        
        
        //Chek the Upgrade Status of app
        if ([[commonUtility retrieveValue:KEY_UPGRADE_STATUS] isEqualToString:@"YES"]){
            
            //Send the type of list
            self.arrClientsDetail = [[DataManager initDB] getClientsListForShow:[NSString stringWithFormat:@"%d",intTypeLlist]];
            
            
        }else{
            //Send the type of list
           self.arrClientsDetail = [[DataManager initDB] getClientsList:@"1"];
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isSearch) {
                [self buildNewArray:self.srcClient.text];
            }
            else {
                if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrClientsDetail]) {
                }
                else {
                    [self.tblClientDetails setEditing:NO animated:YES];
                    isEditing = NO;
                    if (isEditing) {

                    }
                }
                [self.tblClientDetails reloadData];
            }
            
            if (self.arrTemplateData != nil && ![self.arrTemplateData isKindOfClass:[NSNull class]] && self.arrTemplateData.count == 0 && self.tblTemplates.tag == 1001) {
                self.tblTemplates.hidden = true;
                viewTemplateBackground.alpha = 0.7;
                lblNoTemplateFound.hidden = NO;
            }
            else{
                viewTemplateBackground.alpha = 0.0;
                self.tblTemplates.hidden = false;
                lblNoTemplateFound.hidden =YES;
                
            }
            [self.tblTemplates reloadData];
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]){
            }else{
                
            }
            
        });
    });
}

//-----------------------------------------------------------------------------------------------------------------------------

- (void) removeScrollviewContent {
    START_METHOD
    @try {
        for (int i = 1500; i<tag; i++) {
            [[self.view viewWithTag:i] removeFromSuperview];
        }
    }
    @catch (NSException *exception) {
        return;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (void) loadDailyEvents {
    
    START_METHOD
    [self removeScrollviewContent];
    tag = 1500;
    
    if (_eventStore == nil) {
        _eventStore = [[EKEventStore alloc] init];
    }
    
    if (_eventsList == nil) {
        _eventsList = [[NSMutableArray alloc] init];
    }
    
    [self.eventsList removeAllObjects];
    
    // Get the default calendar from store.
    self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
    
    // Fetch today's event on selected calendar and put them into the eventsList array
    [self.eventsList addObjectsFromArray:[self fetchEventsForToday:1]];   // TODO : Check For event End date Appstore crash.
    
    int hh = 12;
    int yX = 0;
    int incYx = 28;
    NSString * a = NSLocalizedString(@"AM",@"");
    
    
    
    //Default 12
    UILabel * hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, yX+20, 30, 30)];
    [hourLabel setTextAlignment:NSTextAlignmentRight];
    [hourLabel setFont:[UIFont fontWithName:kproximanova_semibold size:22]];
    [hourLabel setTextColor:[UIColor colorWithRed:(110/255.f) green:(117/255.f) blue:(119/255.f) alpha:1.0]];
    [hourLabel setText:[NSString stringWithFormat:@"%d",hh]];
    hourLabel.tag = tag;
    tag++;
    [self.scrlviewEventManger addSubview:hourLabel];
    
    UILabel * amLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, yX+30, 30, 15)];
    [amLabel setTextAlignment:NSTextAlignmentRight];
    [amLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    [amLabel setTextColor:[UIColor colorWithRed:(110/255.f) green:(117/255.f) blue:(119/255.f) alpha:1.0]];
    [amLabel setText:a];
    amLabel.tag = tag;
    tag++;
    [self.scrlviewEventManger addSubview:amLabel];
    
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(48, yX+13, 285, 2)];
    imgView.image = [UIImage imageNamed:@"eventLine.png"];
    imgView.tag = tag;
    tag++;
    [self.scrlviewEventManger addSubview:imgView];
    //calender-time
    hh = 1;
    int index = 0;
    BOOL isGetValue = NO;
    int chk = 0;
    yX += 25;
    
    NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
    [tmpdt setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    while (chk == 0 && index < [self.eventsList count]) {
        
        NSDate * evedate;
        [tmpdt setDateFormat:@"h a"];
        
        if(index < [self.eventsList count])
            evedate = [[self.eventsList objectAtIndex:index] startDate];
        
        if(evedate){
            
            NSString * hour = [tmpdt stringFromDate:evedate];
            
            if ([hour isEqualToString:[NSString stringWithFormat:@"12 %@",a]]) {
                
                isGetValue = YES;
                NSString * eveTitle = [[self.eventsList objectAtIndex:index] title];
                NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
                [tmpdt setDateFormat:@"hh:mm a"];
                hour = [tmpdt stringFromDate:evedate];
                
                hour = [hour stringByReplacingOccurrencesOfString:@" PM" withString:NSLocalizedString(@"PM",@"")];
                hour = [hour stringByReplacingOccurrencesOfString:@" AM" withString:NSLocalizedString(@"AM",@"")];
                
                UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(104, yX-10, 250, 41)];
                
                //Sunil - cell image for daily events
                
                //imgView.image = [UIImage imageNamed:@"calendertime.png"];
                
                //Incoke m ethod for set cell image on daily events
                [self setDailyEventsCellImage:imgView];
                
                imgView.tag = tag;
                tag++;
                [self.scrlviewEventManger addSubview:imgView];
                
                UILabel * evenmLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, yX-3, 130, 28)];     // 220 width
                [evenmLabel setTextAlignment:NSTextAlignmentLeft];
                [evenmLabel setFont:[UIFont fontWithName:kProximaNova_Regular size:18]];
                [evenmLabel setText:eveTitle];
                [evenmLabel setLineBreakMode:NSLineBreakByTruncatingTail];
                evenmLabel.backgroundColor = [UIColor clearColor];
                evenmLabel.tag = tag;
                evenmLabel.textColor = [UIColor whiteColor];
                tag++;
                [self.scrlviewEventManger addSubview:evenmLabel];
                
                
                UILabel * eveTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(252, yX-3, 80, 28)];
                [eveTimeLabel setTextAlignment:NSTextAlignmentRight];
                [eveTimeLabel setFont:[UIFont fontWithName:kProximaNova_Regular size:18]];
                [eveTimeLabel setText:hour];
                eveTimeLabel.backgroundColor = [UIColor clearColor];
                eveTimeLabel.tag = tag;
                tag++;
                eveTimeLabel.textColor = [UIColor whiteColor];
                [self.scrlviewEventManger addSubview:eveTimeLabel];
                
                yX+=incYx;
                index++;
            }
            else {
                chk++;
            }
        }
    }
    
    tmpdt = nil;
    
    
    if (isGetValue) {
        yX-=10;
    }
    else {
        yX+=10;
        yX+=5;
    }
    
    
    
    for (int i = 0; i<23; i++) {
        
        hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, yX+20, 30, 30)];
        [hourLabel setTextAlignment:NSTextAlignmentRight];
        [hourLabel setFont:[UIFont fontWithName:kproximanova_semibold size:22]];
        [hourLabel setTextColor:[UIColor colorWithRed:(110/255.f) green:(117/255.f) blue:(119/255.f) alpha:1.0]];
        
        amLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, yX+30, 30, 15)];
        [amLabel setTextAlignment:NSTextAlignmentRight];
        [amLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [amLabel setTextColor:[UIColor colorWithRed:(110/255.f) green:(117/255.f) blue:(119/255.f) alpha:1.0]];
        [amLabel setText:a];
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(48, yX+13, 285, 2)];
        imgView.image = [UIImage imageNamed:@"eventLine.png"];
        imgView.tag = tag;
        tag++;
        [self.scrlviewEventManger addSubview:imgView];
        
        amLabel.tag = tag;
        tag++;
        hourLabel.tag = tag;
        tag++;
        [self.scrlviewEventManger addSubview:amLabel];
        [self.scrlviewEventManger addSubview:hourLabel];
        
        if (hh == 12) {
            yX+=8;
            hourLabel.frame = CGRectMake(0, yX+20, 80, 20);
            imgView.frame = CGRectMake(55, yX+7, 350, 2);
            [amLabel setText:@""];
            [hourLabel setText:NSLocalizedString(@"Noon", @"")];
            a = NSLocalizedString(NSLocalizedString(@"PM",@""),@"");
            
        }
        else {
            [hourLabel setText:[NSString stringWithFormat:@"%d",hh]];
        }
        yX+=25;
        chk = 0;
        isGetValue = NO;
        //
        while (chk == 0 && index < [self.eventsList count]) {
            
            NSDate * evedate;
            if(index < [self.eventsList count])
                evedate = [[self.eventsList objectAtIndex:index] startDate];
            
            if(evedate){
                NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
                [tmpdt setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                [tmpdt setDateFormat:@"h a"];
                NSString * hour = [tmpdt stringFromDate:evedate];
                
                if ([hour isEqualToString:[NSString stringWithFormat:@"%d %@",hh,a]]) {
                    
                    isGetValue = YES;
                    NSString * eveTitle = [[self.eventsList objectAtIndex:index] title];
                    NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
                    [tmpdt setDateFormat:@"hh:mm a"];
                    hour = [tmpdt stringFromDate:evedate];
                    
                    hour = [hour stringByReplacingOccurrencesOfString:@" PM" withString:NSLocalizedString(@"PM",@"")];
                    hour = [hour stringByReplacingOccurrencesOfString:@" AM" withString:NSLocalizedString(@"AM",@"")];
                    
                    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(104, yX-10, 250, 41)];
                    
                    
                    //Sunil ->
                   // imgView.image = [UIImage imageNamed:@"calendertime.png"];
                    
                    
                    //Incoke m ethod for set cell image on daily events
                    [self setDailyEventsCellImage:imgView];
                    
                    imgView.tag = tag;
                    tag++;
                    [self.scrlviewEventManger addSubview:imgView];
                    
                    UILabel * evenmLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, yX-3, 130, 28)];  // 220
                    [evenmLabel setTextAlignment:NSTextAlignmentLeft];
                    [evenmLabel setFont:[UIFont fontWithName:kProximaNova_Regular size:18]];
                    [evenmLabel setLineBreakMode:NSLineBreakByTruncatingTail];
                    [evenmLabel setText:eveTitle];
                    evenmLabel.backgroundColor = [UIColor clearColor];
                    evenmLabel.tag = tag;
                    tag++;
                    evenmLabel.textColor = [UIColor whiteColor];
                    [self.scrlviewEventManger addSubview:evenmLabel];
                    
                    UILabel * eveTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(267, yX-3, 80, 28)];
                    [eveTimeLabel setTextAlignment:NSTextAlignmentLeft];
                    [eveTimeLabel setFont:[UIFont fontWithName:kProximaNova_Regular size:18]];
                    [eveTimeLabel setText:hour];
                    eveTimeLabel.backgroundColor = [UIColor clearColor];
                    eveTimeLabel.tag = tag;
                    tag++;
                    eveTimeLabel.textColor = [UIColor whiteColor];
                    [self.scrlviewEventManger addSubview:eveTimeLabel];
                    
                    yX+=incYx;
                    index++;
                }
                else {
                    chk++;
                }
            }
        }
        
        if (isGetValue) {
            yX-=10;
        }
        else {
            
            
            yX+=10;
            yX+=5;
        }
        
        hh++;
        if (hh>12) {
            hh = 1;
        }
    }
    
    hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, yX+20, 30, 30)];
    [hourLabel setTextAlignment:NSTextAlignmentRight];
    [hourLabel setFont:[UIFont fontWithName:kproximanova_semibold size:22]];
    [hourLabel setTextColor:[UIColor colorWithRed:(110/255.f) green:(117/255.f) blue:(119/255.f) alpha:1.0]];
    [hourLabel setText:@"12"];
    hourLabel.tag = tag;
    tag++;
    [self.scrlviewEventManger addSubview:hourLabel];
    
    amLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, yX+30, 30, 15)];
    [amLabel setTextAlignment:NSTextAlignmentRight];
    [amLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    [amLabel setTextColor:[UIColor colorWithRed:(110/255.f) green:(117/255.f) blue:(119/255.f) alpha:1.0]];
    [amLabel setText:NSLocalizedString(@"PM",@"")];
    amLabel.tag = tag;
    tag++;
    [self.scrlviewEventManger addSubview:amLabel];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(48, yX+13, 285, 2)];
    imgView.image = [UIImage imageNamed:@"eventLine.png"];
    imgView.tag = tag;
    tag++;
    [self.scrlviewEventManger addSubview:imgView];
    
    UIImageView *imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(48, yX+50, 285, 2)];
    imgView1.image = [UIImage imageNamed:@"eventLine.png"];
    imgView1.tag = tag;
    [self.scrlviewEventManger addSubview:imgView1];
    
    [self.scrlviewEventManger setContentSize:CGSizeMake(self.scrlviewEventManger.frame.size.width, yX+54)];
    
    
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) loadRecentEventsSetType:(int)type {
    START_METHOD
    // Initialize an event store object with the init method. Initilize the array for events.
    if (_eventStore == nil) {
        _eventStore = [[EKEventStore alloc] init];
    }
    
    if (_eventsList == nil) {
        _eventsList = [[NSMutableArray alloc] init];
    }
    [self.eventsList removeAllObjects];
    
    
    if([[[UIDevice currentDevice] systemVersion] intValue] >= 6.0){
        // Get the default calendar from store.
        [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            
            if (granted) {
                self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
                // Fetch today's event on selected calendar and put them into the eventsList array
                [self.eventsList addObjectsFromArray:[self fetchEventsForToday:type]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tblEventManager reloadData];
                });
                
            }
            
        }];
    }else{
        self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
        // Fetch today's event on selected calendar and put them into the eventsList array
        [self.eventsList addObjectsFromArray:[self fetchEventsForToday:type]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tblEventManager reloadData];
        });
    }
    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSArray *)fetchEventsForToday:(int)type {
    
    START_METHOD
    NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
    [tmpdt setDateFormat:kAppDateFormat];
    
    NSDateFormatter * tmpdt1 = [[NSDateFormatter alloc] init];
    [tmpdt1 setDateFormat:@"MMM dd, yyyy"];
    
    NSDate * startDate = [NSDate date];
    startDate = [startDate dateByAddingTimeInterval:3600];
    
    NSDate * endDate ;
    if (type == 0) {
        endDate = [NSDate dateWithTimeIntervalSinceNow:604800];//7 days
    } else {
        // updated
        endDate = [NSDate dateWithTimeInterval:86400 sinceDate:startDate]; // Todays Events
        
    }
    
    // Create the predicate. Pass it the default calendar.
    NSArray  *events;
    if(startDate) {
        if(endDate) {
            NSArray *calendarArray = [self.eventStore calendarsForEntityType:EKEntityTypeEvent];
            NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:calendarArray];
            events = [self.eventStore eventsMatchingPredicate:predicate];
        } else {
            events = nil;
        }
    }
    
    return events;
}

//---------------------------------------------------------------------------------------------------------------

-(void)showSettingsView {
    START_METHOD
    if(!self.settingsView) {
        [self addSettingView];
    }
    
    CGRect actualFrame = self.settingsView.frame;
    CGRect modifiedFrame = actualFrame;
    modifiedFrame.origin.y =actualFrame.origin.y ;
    
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        modifiedFrame.size.height = 110;
    } else {
        modifiedFrame.size.height = 260;
    }
    
    self.settingsView.alpha = 0.0;
    self.settingsView.frame = modifiedFrame;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.settingsView.frame = modifiedFrame;
        self.settingsView.alpha = 0.7;
    } completion:^(BOOL finished) {
        self.settingsView.alpha = 1.0;
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)hideSettingsView {
    START_METHOD
    CGRect actualFrame = self.settingsView.frame;
    [UIView animateWithDuration:0.5 animations:^{
        CGRect modifiedFrame = actualFrame;
        modifiedFrame.origin.y =actualFrame.origin.y ;
        modifiedFrame.size.height = 0;
        self.settingsView.frame = modifiedFrame;
        self.settingsView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.settingsView removeFromSuperview];
        self.settingsView = nil;
    }];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)addSettingView {
    START_METHOD
    _settingsView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 250, 71, 250, 100)];
    [self.settingsView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ParQ-Background"]]];
//    _settingsView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.settingsView];
    
    if(![[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        UIButton *btn1 =[self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Restore", @"")];
        [btn1 addTarget:self action:@selector(restoreTheProduct) forControlEvents:UIControlEventTouchUpInside];
        
        [self setSkinnOptionButton:btn1];
        [self.settingsView addSubview:btn1];
        
        UIButton *btn2 =[self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:NSLocalizedString(@"Trainer's Profile",@"")];
        [btn2 addTarget:self action:@selector(addTrainerName) forControlEvents:UIControlEventTouchUpInside];
        [self setSkinnOptionButton:btn2];
        [self.settingsView addSubview:btn2];
        
        [self.settingsView setClipsToBounds:TRUE];
        
    } else {
        
        UIButton *btn2 =[self btnWithFrame:CGRectMake(15, 10, 225, 40) andTitle:NSLocalizedString(@"Trainer's Profile",@"")];
        [btn2 addTarget:self action:@selector(addTrainerName) forControlEvents:UIControlEventTouchUpInside];
        
        //Sunil
        [self setSkinnOptionButton:btn2];
        
        
        [self.settingsView addSubview:btn2];

        
        //Add button for change Skin Sunil
        UIButton *buttonSkin1 =[self btnWithFrame:CGRectMake(15, 60, 225, 40) andTitle:@"Skin Turquoise/White"];
        [buttonSkin1 setBackgroundColor:kOldSkin];
        [buttonSkin1 addTarget:self action:@selector(changeSkin:) forControlEvents:UIControlEventTouchUpInside];
        buttonSkin1.tag = 0;
        
        [self.settingsView addSubview:buttonSkin1];
        
        //Add button for change Skin Sunil
        UIButton *buttonSkin2 =[self btnWithFrame:CGRectMake(15, 110, 225, 40) andTitle:@"Skin  Green/Black"];
        [buttonSkin2 addTarget:self action:@selector(changeSkin:) forControlEvents:UIControlEventTouchUpInside];
        buttonSkin2.tag = 1;
        [buttonSkin2 setBackgroundColor:kFirstSkin];
        [self.settingsView addSubview:buttonSkin2];
        
        //Add button for change Skin Sunil
        UIButton *buttonSkin3 =[self btnWithFrame:CGRectMake(15, 160, 225, 40) andTitle:@"Skin Red/Black"];
        [buttonSkin3 addTarget:self action:@selector(changeSkin:) forControlEvents:UIControlEventTouchUpInside];
         [buttonSkin3 setBackgroundColor:ksecondSkin];
        buttonSkin3.tag = 2;
        
        [self.settingsView addSubview:buttonSkin3];
        
        //Add button for change Skin Sunil
        UIButton *buttonSkin4 =[self btnWithFrame:CGRectMake(15, 210, 225, 40) andTitle:@"Skin Pink/White"];
        [buttonSkin4 addTarget:self action:@selector(changeSkin:) forControlEvents:UIControlEventTouchUpInside];
        buttonSkin4.tag = 3;
         [buttonSkin4 setBackgroundColor:kThirdSkin];
        
        [self.settingsView addSubview:buttonSkin4];
        
    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(UIButton *)btnWithFrame:(CGRect)frame andTitle:(NSString *)title {
    START_METHOD
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:frame];
    [btn.titleLabel setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:16]];
    [btn.titleLabel setTextColor:[UIColor whiteColor]];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.layer.cornerRadius = 8.0f;
    btn.layer.borderColor = kVewBGColor.CGColor;
    btn.layer.borderWidth = 1.0f;
    [btn setBackgroundColor:kVewBGColor];
    return btn;
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)restoreTheProduct {
    START_METHOD
    [self hideSettingsView];
    [[AppDelegate sharedInstance]showProgressAlert:NSLocalizedString(@"Restoring", @"")];
    [[CStoreKit sharedInstance]restore:(id)self];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)addTrainerName {
    START_METHOD
    [self hideSettingsView];
    
    //Invoke method for move to Edit profile view
    [self moveToTrainerEditProfileView];
    
    
    /*
    AddNameVC *objAddNameVC = (AddNameVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddNameView"];
    
    objAddNameVC.strScreenName = @"EnterTrainerName";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kTrainerName])
    {
        objAddNameVC.strFiledContent = [[NSUserDefaults standardUserDefaults] objectForKey:kTrainerName];
    }
    else
    {
        objAddNameVC.strFiledContent =nil;
    }
    
    UINavigationController *modalController = [[UINavigationController alloc]initWithRootViewController:objAddNameVC];
    modalController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    modalController.modalPresentationStyle =  UIModalPresentationFormSheet;
    
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(350, 160);
        modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    
    [self.navigationController presentViewController:modalController animated:YES completion:^{
        
    }];
     */
}


-(void)changeSkin:(UIButton *)button{
    START_METHOD
     [self hideSettingsView];
    int tag = (int) button.tag;
    
    //Save TAg
    [commonUtility saveValue:[NSString stringWithFormat:@"%d",tag] andKey:KEY_SKIN];
    
    MESSAGE(@"changeSkin : -> tag of button : %d",tag);
     //Invoke methos for chnage skin
    [self actionChnageSkin:[commonUtility retrieveValue:KEY_MODE].intValue];
    
    END_METHOD
}
-(void)actionChnageSkin:(int )tag{
    MESSAGE(@"tag of button : %d",tag);
    //Sunil Background image dynamicaly -->
    
     [imgvBg setImage:[UIImage imageNamed:@"BackgroundImage"]];
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    [self actionOnweeklyButton];
    
    [_tblTemplates reloadData];
    switch (tag) {
        case PORTRAIT:
            
            //<---
            
            [self setViewChnagesOnColorPortraitScape:skin];
            
            
            
            break;
            
        case LANDSCAPE:
            
            //<---
            
            [self setViewChnagesOnColorLandScape:skin];
            
            
            break;
            
            
        default:
            break;
    }
}

-(void)setPortraitImaes{
    START_METHOD
}
-(void)setLandscapeImaes{
    START_METHOD
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(UIImage *) profileImageOfClient:(NSString *) clientId andIndexPath:(NSIndexPath *)inedxPath{
    START_METHOD
    
    
    NSString * imgPath =[NSString stringWithFormat:@"%@/ClientsProfileImage/ProfileImage%@.png",[FileUtility basePath],clientId];
    UIImage * img = [UIImage imageWithContentsOfFile:imgPath];
    
    if(img == nil) {
        
        //invoke method for get image from server
//        [self getClientProfileImage:clientId andIndexPath:inedxPath];
        
        
        UIImage * img = [UIImage imageNamed:@"ClientPlaceholder"];
        return img;
    }
    
    
    return img;
}

//---------------------------------------------------------------------------------------------------------------


-(IBAction)btnAddTemplate_Clicke:(id)sender{
    START_METHOD
    AddNewTemplateVC * addnewTemplatecontrollersOBJ = [[AddNewTemplateVC alloc]initWithNibName:@"AddNewTemplateView" bundle:Nil];
    addnewTemplatecontrollersOBJ.delegateTemplate = (id)self;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:addnewTemplatecontrollersOBJ];
    _popOverForTemplate = [[UIPopoverController alloc] initWithContentViewController:navigationController];
    [_popOverForTemplate setDelegate:(id)self];
    
    CGRect rect;
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        rect =CGRectMake(450,300, 50, 50);
    } else {
        rect =CGRectMake(350,300, 50, 50);
    }
    
    [_popOverForTemplate presentPopoverFromRect:rect inView:self.view permittedArrowDirections:0 animated:YES];
}

//---------------------------------------------------------------------------------------------------------------

-(void)setLoadingViewForVersionUpgrade {
    
    START_METHOD
    self.dataUpdateLoadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.dataUpdateLoadView setBackgroundColor:[UIColor blackColor]];
    [self.dataUpdateLoadView setAlpha:0.6];
    [self.view addSubview:self.dataUpdateLoadView];
    
    UILabel *lblInfo1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 270,self.view.frame.size.width, 150)];
    lblInfo1.textAlignment = NSTextAlignmentCenter;
    [lblInfo1 setText:NSLocalizedString(@"Application Updating to Version", @"")];
    [lblInfo1 setTextColor:[UIColor whiteColor]];
    [lblInfo1 setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:35]];
    [self.dataUpdateLoadView addSubview:lblInfo1];
    
    UILabel *lblInfo2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 340,self.view.frame.size.width, 150)];
    lblInfo2.textAlignment = NSTextAlignmentCenter;
    [lblInfo2 setText:[NSString stringWithFormat:@"%@",[self versionNumberString]]];
    [lblInfo2 setTextColor:[UIColor whiteColor]];
    [lblInfo2 setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:35]];
    [self.dataUpdateLoadView addSubview:lblInfo2];
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityView setFrame:CGRectMake(320, 460, 100, 100)];
    [self.dataUpdateLoadView addSubview:activityView];
    [activityView startAnimating];
    
    UILabel *lblInfo3 = [[UILabel alloc]initWithFrame:CGRectMake(0, 855,self.view.frame.size.width, 150)];
    lblInfo3.textAlignment = NSTextAlignmentCenter;
    [lblInfo3 setText:NSLocalizedString(@"Important : Please do not close the application", @"")];
    [lblInfo3 setTextColor:[UIColor whiteColor]];
    [lblInfo3 setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:35]];
    [self.dataUpdateLoadView addSubview:lblInfo3];
    
}

//---------------------------------------------------------------------------------------------------------------

- (NSString *)versionNumberString {
    
    START_METHOD
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return majorVersion;
}

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    START_METHOD
    lblHelp.text = NSLocalizedString(lblHelp.text, nil);
    self.lblInfo.text = NSLocalizedString(self.lblInfo.text, @"");
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblHelp.text font:lblHelp.font height:lblHelp.frame.size.height];
    [btnHelp setFrame:CGRectMake(0, 20, rect.size.width + 30, 51)];
    
    
    rect = [[AppDelegate sharedInstance] getWidth:self.lblInfo.text font:self.lblInfo.font height:self.lblInfo.frame.size.height];
    [self.btnInfo setFrame:CGRectMake(btnHelp.frame.origin.x + btnHelp.frame.size.width + 1, 20, rect.size.width + 30, 51)];
    
    [lblHelp setFrame:CGRectMake(btnHelp.frame.origin.x, 51, btnHelp.frame.size.width, lblHelp.frame.size.height)];
    [self.lblInfo setFrame:CGRectMake(self.btnInfo.frame.origin.x, 51, self.btnInfo.frame.size.width, self.lblInfo.frame.size.height)];
    
  
    lblClients.text = NSLocalizedString(lblClients.text, @"");
    lblCalendar.text = NSLocalizedString(lblCalendar.text, @"");
    lblWeekly.text = NSLocalizedString(lblWeekly.text, @"");
    lblDaily.text = NSLocalizedString(lblDaily.text, @"");
    
    
    
    self.lblPhase.text = NSLocalizedString(self.lblPhase.text, @"");
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(self.lblPhase.text, nil) font:self.lblPhase.font height:21];
    
    if ([[AppDelegate sharedInstance].isFirstTime isEqualToString:@"Yes"] && rect.size.width > 150) {
        [self.lblPhase setFrame:CGRectMake(self.lblPhase.frame.origin.x, self.lblPhase.frame.origin.y, rect.size.width + 5, self.lblPhase.frame.size.height)];
        
        [btnTimer setFrame:CGRectMake(btnTimer.frame.origin.x - 10, btnTimer.frame.origin.y, rect.size.width + 60, btnTimer.frame.size.height)];
        
        [self.imgvewPhase setFrame:CGRectMake(self.imgvewPhase.frame.origin.x - 10, self.imgvewPhase.frame.origin.y, rect.size.width + 60, self.imgvewPhase.frame.size.height)];
        
        [viewTimer setFrame:CGRectMake(viewTimer.frame.origin.x - 10, viewTimer.frame.origin.y, rect.size.width + 60, viewTimer.frame.size.height)];
    }
    
    self.srcClient.placeholder = NSLocalizedString(@"Search", @"");
    
    lblAddNewProgramTemplate.text = NSLocalizedString(kAddTemplateMessage, nil);
    lblNoTemplateFound.text = NSLocalizedString(lblNoTemplateFound.text,@"");
    
    lblLastTrainedClients.text = NSLocalizedString(lblLastTrainedClients.text, @"");
    
    UIFontDescriptor * fontD = [lblTraining.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold
                                ];
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language isEqualToString:@"en"] || [language isEqualToString:@"zh-Hans"] || [language isEqualToString:@"nb"]) {
        [lblTraining setFont:[UIFont fontWithDescriptor:fontD size:24.0]];
        [lblNotebook setFont:[UIFont fontWithName:@"Proxima Nova Alt" size:24.0]];
    }
    
    
    lblTraining.text = NSLocalizedString(lblTraining.text, nil);
    lblNotebook.text = NSLocalizedString(lblNotebook.text, nil);
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : Timer Notification  Methods

//---------------------------------------------------------------------------------------------------------------

- (void)changePhaseTimerValue:(NSNotification *) timeNotification {
    
    START_METHOD
    NSDictionary * dicData = [timeNotification userInfo];
    NSString *time = [dicData objectForKey:@"Time"];
    time = [time substringToIndex:([time length] - 1)];
    [self.lblTimerLabel setText:time];
    
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)changePhaseImage:(NSNotification *) imageNotification {
    
    START_METHOD
    NSDictionary * dicData = [imageNotification userInfo];
    NSString *phaseName = [dicData objectForKey:@"selector"];
    
    if([phaseName isEqualToString:kisWork]) {
        [self.lblPhase setText:@"WORK"];
        [self.imgvewPhase setImage:[UIImage imageNamed:@"Work"]];
    } else if ([phaseName isEqualToString:kisRest]) {
        [self.lblPhase setText:@"REST"];
        [self.imgvewPhase setImage:[UIImage imageNamed:@"Rest"]];
    } else if ([phaseName isEqualToString:kisSetRest]) {
        [self.lblPhase setText:@"SET REST"];
        [self.imgvewPhase setImage:[UIImage imageNamed:@"Set-Reset"]];
    } else {
        [self.lblPhase setText: @"TRAINING TIMER"];
        [self.imgvewPhase setImage:[UIImage imageNamed:@"Work"]];
        [self.lblPhase setTextColor:[UIColor darkGrayColor]];
        [self.lblTimerLabel setTextColor:[UIColor darkGrayColor]];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)updateToFullVersion:(NSNotification *) imageNotification {
    
    START_METHOD
    [[AppDelegate sharedInstance]hideProgressAlert];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [viewRecentClients setHidden:TRUE];
    [self.btnAddTemplate setHidden:FALSE];
    [lblAddNewProgramTemplate setHidden:FALSE];
    
    [self.btnInfo setHidden:TRUE];
    [self.lblInfo setHidden:TRUE];
}


//---------------------------------------------------------------------------------------------------------------
-(void)blinkImage:(NSNotification *)blinkNotification {
    
    START_METHOD
    NSDictionary * dicData = [blinkNotification userInfo];
    NSString *phaseName = [dicData objectForKey:@"selector"];
    BOOL isForBlink = [[dicData objectForKey:kisForBlink] boolValue];
    
    if([phaseName isEqualToString:kisWork]) {
        [self.lblPhase setText:@"WORK"];
        if(isForBlink) {
            [self.imgvewPhase setImage:[UIImage imageNamed:@"BlinkWorkSmall"]];
            [self.lblPhase setTextColor:[UIColor whiteColor]];
            [self.lblTimerLabel setTextColor:[UIColor whiteColor]];
        } else {
            [self.imgvewPhase setImage:[UIImage imageNamed:@"Work"]];
            [self.lblPhase setTextColor:[UIColor darkGrayColor]];
            [self.lblTimerLabel setTextColor:[UIColor darkGrayColor]];
        }
        
    } else if ([phaseName isEqualToString:kisRest]) {
        [self.lblPhase setText:@"REST"];
        if(isForBlink) {
            [self.imgvewPhase setImage:[UIImage imageNamed:@"BlinkRestSmall"]];
            [self.lblPhase setTextColor:[UIColor whiteColor]];
            [self.lblTimerLabel setTextColor:[UIColor whiteColor]];
        } else {
            [self.imgvewPhase setImage:[UIImage imageNamed:@"Rest"]];
            [self.lblPhase setTextColor:[UIColor darkGrayColor]];
            [self.lblTimerLabel setTextColor:[UIColor darkGrayColor]];
        }
        
    } else if ([phaseName isEqualToString:kisSetRest]) {
        [self.lblPhase setText:@"SET REST"];
        if(isForBlink) {
            [self.imgvewPhase setImage:[UIImage imageNamed:@"BlinkSetRestSmall"]];
            [self.lblPhase setTextColor:[UIColor whiteColor]];
            [self.lblTimerLabel setTextColor:[UIColor whiteColor]];
        } else {
            [self.imgvewPhase setImage:[UIImage imageNamed:@"Set-Reset"]];
            [self.lblPhase setTextColor:[UIColor darkGrayColor]];
            [self.lblTimerLabel setTextColor:[UIColor darkGrayColor]];
            
        }
        
    } else {
        [self.lblPhase setText:@"TRAINING TIMER"];
        [self.imgvewPhase setImage:[UIImage imageNamed:@"Work"]];
        
    }
    
}

//---------------------------------------------------------------------------------------------------------------
-(void)hideUpdatingVersionView:(NSNotification *)updateNotification {
    
    START_METHOD
    if(self.dataUpdateLoadView) {
        [self.dataUpdateLoadView removeFromSuperview];
        self.dataUpdateLoadView = nil;
        
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : StoreKit Delegate MEthods

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseDone:(NSString*)productId purchase:(NSDictionary *)purchase {
    
    START_METHOD
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[NSNotificationCenter defaultCenter] postNotificationName:kupdateVersionNotification object:nil];
    
    [viewRecentClients setHidden:TRUE];
    [self.btnAddTemplate setHidden:FALSE];
    [lblAddNewProgramTemplate setHidden:FALSE];
    
    [self.btnInfo setHidden:TRUE];
    [self.lblInfo setHidden:TRUE];

    
    //SUNIL -> 14 oct for the active and all clients
    //Check user status old or new
    if ([[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"]){
        
        
        //Get all the clients for the old user
        [self loadAllClientData:2];
        
    }else{
        
        //Get the Active clients only
        [self loadAllClientData:currentList];
    }
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseFailed:(NSString*)productId error:(NSString *)error {
    
    START_METHOD
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction failed, Please try again", @"")];
   
}

//---------------------------------------------------------------------------------------------------------------

- (void)purchaseCancelled:(NSString*)productId {
    START_METHOD
    [[AppDelegate sharedInstance] hideProgressAlert];
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Transaction Cancelled, Please try again", @"")];
 
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : Restore Delegate Method

//---------------------------------------------------------------------------------------------------------------

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    
    START_METHOD
    NSMutableArray * arrTransactionproductID = [NSMutableArray array];
    for (SKPaymentTransaction *transaction in queue.transactions) {
        NSString *productID = transaction.payment.productIdentifier;
        [arrTransactionproductID addObject:productID];
    };
    
    if([arrTransactionproductID count] >0) {
        if([arrTransactionproductID containsObject:kFullAccessProductId]) {
            [[AppDelegate sharedInstance]hideProgressAlert];
            [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Application Restored", @"")];
            [self updateToFullVersion:nil];
        } else {
            [[AppDelegate sharedInstance]hideProgressAlert];
            [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Application is not purchased", @"")];
        }
    } else {
        [[AppDelegate sharedInstance]hideProgressAlert];
        [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"No Products Purchased", @"")];
    }
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    
    START_METHOD
    
    [[AppDelegate sharedInstance]showAlert:NSLocalizedString(@"Restoration Failed", @"")];
    return;
    
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark Tableview DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    START_METHOD
    if (tableView.tag == 500 || tableView.tag == 700 || tableView.tag == 1001) {
        return 1;
    }
    else {
        
        return 1;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    START_METHOD
    if (tableView.tag == 2001) {
        if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrRecentClientsData]) {
            return  [self.arrRecentClientsData count];
        }
        return 0;
    }
    else if(tableView.tag == 1001){
        if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrTemplateData]) {
            return  [self.arrTemplateData count];
        }
        return 0;
        
    }else if (tableView.tag == 500) {
        if (self.eventsList.count > 7) {
            return self.eventsList.count;
        }
        return 7;
    }else {
        if (isSearch) {
            if (![[AppDelegate sharedInstance] isNotEmptyArray:self.arrSearchData]) {
                return 1;
            }
            return [self.arrSearchData count];
        }
        
        if (![[AppDelegate sharedInstance] isNotEmptyArray:self.arrClientsDetail]) {
            return 1;
            
        }
        return [self.arrClientsDetail count];
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    START_METHOD
    return @"";
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    START_METHOD
    if (aTableView.tag == 500) {
        return UITableViewCellEditingStyleNone;
    }
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        return UITableViewCellEditingStyleNone;
    }
    if ([self.arrClientsDetail count] == 0) {
        [self.tblClientDetails setEditing:NO animated:YES];
        isEditing = NO;
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    START_METHOD
    if (tableView.tag == 2001) {
        
        static NSString * CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *lblDate,*lblName;
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            lblDate = [[UILabel alloc]initWithFrame:CGRectMake(205, 25, 140, 17)];
            lblDate.font = [UIFont fontWithName:kproximanova_semibold size:15];
            lblDate.backgroundColor = [UIColor clearColor];
            lblDate.textColor = [UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0];
            lblDate.tag = 10;
            [cell.contentView addSubview:lblDate];
            
            lblName = [[UILabel alloc]initWithFrame:CGRectMake(19, 22, 188, 22)];
            lblName.font = [UIFont fontWithName:kproximanova_semibold size:16];
            lblName.backgroundColor = [UIColor clearColor];
            lblName.textColor = [UIColor colorWithRed:(90/255.f) green:(94/255.f) blue:(96/255.f) alpha:1.0];
            lblName.tag = 11;
            [cell.contentView addSubview:lblName];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (lblDate == nil) {
            lblDate = (UILabel *)[cell.contentView viewWithTag:10];
        }
        if (lblName == nil) {
            lblName = (UILabel *)[cell.contentView viewWithTag:11];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        
        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrRecentClientsData]){
            
            NSDictionary * dictemp = [self.arrRecentClientsData objectAtIndex:indexPath.row];
            [lblName setText:[NSString stringWithFormat:@"%@ %@",[dictemp objectForKey:kFirstName],[dictemp objectForKey:kLastName]]];
            NSDateFormatter * dt = [[NSDateFormatter alloc] init];
            [dt setDateFormat:@"MM-dd-yyyy HH:mm:ss +0000"];
            NSDate * date = [dt dateFromString:[dictemp objectForKey:kLastReviewdDate]];
            [dt setDateFormat:@"MM/dd/yy @ hh:mm a"];
            NSString * strDate = [dt stringFromDate:date];
            if (indexPath.row == 0) {
                lblDate.text =strDate;
            }
            else{
                lblDate.text = @"";
            }
        }
        
        return cell;
        
    }
    else if (tableView.tag == 1001) {
        static NSString * CellIdentifier = @"Cell";
        
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]){
            
        }
        else{
            
            TemplateCustomeCell * cell = (TemplateCustomeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[TemplateCustomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            self.tblTemplates .backgroundColor = [UIColor clearColor];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            
            UIImageView *av = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 353, 67)];
            av.backgroundColor = [UIColor clearColor];
            av.opaque = NO;
            
            //Sunil
            //av.image = [UIImage imageNamed:@"list.png"];
            
            //invoke method for set image
            [self cellImageForTemplate:av];
            
            
            cell.backgroundView = av;
            
            if ([[AppDelegate sharedInstance] isNotEmptyArray:self.arrTemplateData] && indexPath.row < [self.arrTemplateData count]) {
                NSDictionary * dictemp = [NSDictionary dictionaryWithDictionary:[self.arrTemplateData objectAtIndex:indexPath.row]];
                [cell displayTableData:dictemp];
            }
            else{
                cell.lblSheetContained.text = @"";
                cell.lblTemplateName.text = @"";
            }
            //  }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //Sunil ->
            [self setCellTextColor:cell];
            return cell;
        }
    }
    
    else if (tableView.tag == 500) {
        
        static NSString *CellIdentifier = @"Cell";
        
        EventNotesCustomCell * cell = (EventNotesCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[EventNotesCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if (indexPath.row < [self.eventsList count]) {
            NSDate * date = [[self.eventsList objectAtIndex:indexPath.row] startDate];
            NSString * eveTitle = [[self.eventsList objectAtIndex:indexPath.row] title];
            NSString * notes = [[self.eventsList objectAtIndex:indexPath.row] notes];
            if (notes == nil) {
                notes = @"";
            }
            NSString * ch = @"0";
            if ([self.btnWeekly backgroundImageForState:UIControlStateNormal] == [UIImage imageNamed:@"calBtn_deselected_bg.png"]) {
                ch = @"1";
            }
            

            NSDateFormatter * tmpdt = [[NSDateFormatter alloc] init];
            [tmpdt setDateFormat:@"MM-dd-yyyy hh:mm:ss a"];
            NSString * updatedDate = [tmpdt stringFromDate:date];
            
            NSArray *dateArray = [updatedDate componentsSeparatedByString:@" "];
            NSString *date1 = [dateArray objectAtIndex:0];
            
            //===================================
            
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:date,date1,eveTitle,notes,@"1",ch,nil] forKeys:[NSArray arrayWithObjects:@"date",@"evntDate",@"title",@"notes",@"type",@"isDaily",nil]];
            [cell displayTableData:dictInfo];
            
        }
        else {
            cell.lblCName.text = @"";
            cell.lblDate.text = @"";
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
         cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    else if(tableView.tag == 600){
        
        NSString *strBool;

        NSString * CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld",(long)indexPath.section,(long)indexPath.row];
        
        HomeCustomCell * cell1 = (HomeCustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell1 == nil) {
            cell1 = [[HomeCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        self.tblClientDetails.backgroundColor = [UIColor clearColor];
        cell1.backgroundColor = [UIColor clearColor];
        cell1.contentView.backgroundColor = [UIColor clearColor];
        
        if (isSearch) {
            
            if([self.arrSearchData count] != 0) {
                
                //HIde the toggle button
                cell1.mySwitch.hidden   =   NO;
                
                if(indexPath.row < [self.arrSearchData count]) {
                    
                    strBool   =   [[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:STATUS];
                    
                    cell1.lblCName.text = [NSString stringWithFormat:@"%@ %@",[[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kFirstName],[[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kLastName]];
                   
                    
                    cell1.ImgTable.image = [self profileImageOfClient:[[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kClientId]  andIndexPath:indexPath];
                    
                    
                } else {
                    cell1.lblCName.text = NSLocalizedString(@"No Data Found", @"");
                    
                }
            }
            else {
                cell1.lblCName.text = NSLocalizedString(@"No Data Found",@"");
                cell1.ImgTable.image = [UIImage imageNamed:@"ClientPlaceholder"];
               
                //HIde the toggle button
                cell1.mySwitch.hidden   =   YES;
            }
            
        } else {
            
            
            if([self.arrClientsDetail count] != 0 && [self.arrClientsDetail isKindOfClass:[NSMutableArray class]]) {
                
                //HIde the toggle button
                cell1.mySwitch.hidden   =   NO;
                
                
                if(indexPath.row < [self.arrClientsDetail count]) {
                    
                    strBool   =   [[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:STATUS];
                    
                    cell1.lblCName.text = [NSString stringWithFormat:@"%@ %@",[[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kFirstName],[[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kLastName]];

                    cell1.ImgTable.image = [self profileImageOfClient:[[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kClientId]  andIndexPath:indexPath];
                    
                } else {
                    cell1.lblCName.text = NSLocalizedString(@"No Data Found", @"");
                    
                }
            }
            
            else {
                cell1.lblCName.text = NSLocalizedString(@"No Data Found", @"");;
                cell1.ImgTable.image = [UIImage imageNamed:@"ClientPlaceholder"];
               
                //HIde the toggle button
                cell1.mySwitch.hidden   =   YES;
            }
            
        }
        
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        cell1.backgroundColor = [UIColor clearColor];
        
        
        
        //Set the swict on cell
        
        //GEt Dictionery for perticluar cell

        if([strBool isEqualToString: @"YES"]){
            [cell1.mySwitch setOn:YES animated:YES];
        }else{
            [cell1.mySwitch setOn:NO animated:YES];
        }
        
        
        //Set ther swicth tag for access
        cell1.mySwitch.tag   = (int)indexPath.row;

        
        //Set Selector
        [ cell1.mySwitch addTarget:self
                               action:@selector(changeSwitch:)
                     forControlEvents:UIControlEventValueChanged];
    
        
        
        NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        
        //Check user status old or new
        if ([[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"] || ![strUpgradeStatus isEqualToString:@"YES"]){
            
            
            //HIde the toggle button
            cell1.mySwitch.hidden   =   YES;
        }
        
        
        return cell1;
    }
    return nil;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    START_METHOD
    return [NSArray arrayWithObjects:nil];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    START_METHOD
    
    if(tableView.tag == 1001){
        
        NSDictionary * dictemp = [NSDictionary dictionaryWithDictionary:[self.arrTemplateData objectAtIndex:indexPath.row]];
        
        self.intForDeleteTemplate = [[dictemp valueForKey:@"template_Id"]intValue];
        __autoreleasing UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(@"Are you sure you want to delete this Template",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
        [alertView setTag:101];
        [alertView show];
        
    }
    else if (tableView.tag == 500 || tableView.tag == 700) {
        
    }else {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            if (isSearch) {
                delClientID = [[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kClientId];
            }
            else {
                delClientID = [[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kClientId];
            }
            
            UIAlertView * alertShow = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Training Notebook", @"") message:NSLocalizedString(@"Are you sure you want to delete this client's details?",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""),nil];
            [alertShow setCancelButtonIndex:1];
            [alertShow setTag:100001];
            [alertShow show];
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


#pragma mark -
#pragma mark Tableview Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    START_METHOD
    if (tableView.tag == 500) {
        return 40;
    }
    else {
        return 70;
    }
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    START_METHOD
    
    if(tableView.tag == 2001){
        
        
        NSString * clientID = @"";
        NSString * clientName = @"";
        
        if([[AppDelegate sharedInstance] isNotEmptyArray:self.arrRecentClientsData]){
            clientID = [[self.arrRecentClientsData objectAtIndex:indexPath.row] objectForKey:kClientId];
            clientName = [[self.arrRecentClientsData objectAtIndex:indexPath.row] objectForKey:kFirstName];
            clientName = [clientName stringByAppendingFormat:@" %@",[[self.arrRecentClientsData objectAtIndex:indexPath.row] objectForKey:kLastName]];
        }
        
        
        if ([[DataManager initDB] upDateLastReviewedDate:clientID] == 0) {
            ClientViewController * clientDetailsViewOBJ = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientView"];
            clientDetailsViewOBJ.strClientId = clientID;
            clientDetailsViewOBJ.strClientName = clientName;
            clientDetailsViewOBJ.isFromClientTable = TRUE;
            clientDetailsViewOBJ.selectedScreen = kisParq;
            [self.navigationController pushViewController:clientDetailsViewOBJ animated:YES];
        }
    }
    else if(tableView.tag == 1001){
        
        
        MESSAGE(@"self.arrTemplateData.count : %lu , and index: %ld",self.arrTemplateData.count,(long)indexPath.row);
          if(self.arrTemplateData && self.arrTemplateData.count>(int)indexPath.row){
              
        ProgramTemplateViewController *programTemplateViewObj = (ProgramTemplateViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramTemplateView"];
        programTemplateViewObj.strLableText = [[self.arrTemplateData objectAtIndex:indexPath.row] valueForKey:@"templateName"];
        programTemplateViewObj.isNewTemplate = NO;
        programTemplateViewObj.intTemplateSheets = self.intSheetContainedByTemplate;
        
      
            
         
        programTemplateViewObj.dictTemplateInfo = [self.arrTemplateData objectAtIndex:indexPath.row];
        
        MESSAGE(@"cell-> dictemplate: %@ and programTemplateViewObj.intTemplateSheets: %ld",programTemplateViewObj.dictTemplateInfo,(long)programTemplateViewObj.intTemplateSheets);
        
        [self.navigationController pushViewController:programTemplateViewObj animated:YES];
        
        }
        
    }else if (tableView.tag == 500) { // Events Table cell
        
        
    }  else {
        
        //Clients Status
        NSString *   strBoolStatus;
        
        if (isSearch) {

            
            if ([self.arrSearchData count] == 0) {
                return;
            
            }else {
                strBoolStatus   =   [[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:STATUS];
                //Check user status old or new
                if([strBoolStatus isEqualToString: @"NO"] && [[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"NO"]){
                    
                    
                    //SDhow alert for tel that clinetios active
                    [commonUtility alertMessage:@"The client is currently in the Inactive state"];
                    return;
                }
            }
            
        }
        else {
            if ([self.arrClientsDetail count] == 0) {
                return;

            }else {
                strBoolStatus   =   [[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:STATUS];
                
                if([strBoolStatus isEqualToString: @"NO"] && [[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"NO"]){
                    
                    //SDhow alert for tel that clinetios active
                    [commonUtility alertMessage:@"The client is currently in the Inactive state"];
                    return;
                }
            }
            
        }
        
        if (!isEditing) {
            NSString * clientID = @"";
            NSString * clientName = @"";
            if (isSearch) {
                if(indexPath.row < [self.arrSearchData count]){
                    clientID = [[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kClientId];
                    clientName = [[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kFirstName];
                    clientName = [clientName stringByAppendingFormat:@" %@",[[self.arrSearchData objectAtIndex:indexPath.row] objectForKey:kLastName]];
                }
            } else {
                if(indexPath.row < [self.arrClientsDetail count]){
                    clientID = [[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kClientId];
                    clientName = [[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kFirstName];
                    clientName = [clientName stringByAppendingFormat:@" %@",[[self.arrClientsDetail objectAtIndex:indexPath.row] objectForKey:kLastName]];
                }
                
            }
            
            if ([[DataManager initDB] upDateLastReviewedDate:clientID] == 0) {
                
                
                MESSAGE(@"upDateLastReviewedDate---> 1123");
                
                NSMutableDictionary *dictObj    =   [[NSMutableDictionary alloc]init];
                [dictObj setValue:clientID forKey:kClientId];
                [dictObj setValue:clientName forKey:@"clientName"];
                
                
                
                
                NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
                //If not Upgrade then show alert for upgrade
                if([strUpgradeStatus isEqualToString:@"YES"]){
                    
                    NSString *strStaus = [[DataManager initDB]getClientsInfoStaus:clientID];

                    
                    if(strStaus && strStaus.length>0 && [strStaus isEqualToString:@"YES"]){
                        
                        ClientViewController * clientDetailsViewOBJ = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientView"];
                        clientDetailsViewOBJ.strClientId = clientID;
                        clientDetailsViewOBJ.strClientName = clientName;
                        clientDetailsViewOBJ.isFromClientTable = TRUE;
                        clientDetailsViewOBJ.selectedScreen = kisParq;
                        [self.navigationController pushViewController:clientDetailsViewOBJ animated:YES];
                        
                    }else{
                        
                        //Invoke method for get the client info
                        [self getClientInfoFromServer:dictObj];
                    }
                }else{
                    
                    ClientViewController * clientDetailsViewOBJ = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientView"];
                    clientDetailsViewOBJ.strClientId = clientID;
                    clientDetailsViewOBJ.strClientName = clientName;
                    clientDetailsViewOBJ.isFromClientTable = TRUE;
                    clientDetailsViewOBJ.selectedScreen = kisParq;
                    [self.navigationController pushViewController:clientDetailsViewOBJ animated:YES];
                    
                    
                }
                
                
                /*
                ClientViewController * clientDetailsViewOBJ = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientView"];
                clientDetailsViewOBJ.strClientId = clientID;
                clientDetailsViewOBJ.strClientName = clientName;
                clientDetailsViewOBJ.isFromClientTable = TRUE;
                clientDetailsViewOBJ.selectedScreen = kisParq;
                [self.navigationController pushViewController:clientDetailsViewOBJ animated:YES];
                 */
            }
        }
        
        
        
    }
}

//-----------------------------------------------------------------------

- (NSString *) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    START_METHOD
    
    return NSLocalizedString(@"Delete", nil);
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark AlertView Delegatet Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    START_METHOD
    int t = (int)alertView.tag;
    if (buttonIndex == 0 && t == 5000) {
        [[AppDelegate sharedInstance] showProgressAlert:NSLocalizedString(@"Purchase in progress", nil)];
        [[CStoreKit sharedInstance]buy:kFullAccessProductId delegate:(id)self];
    }
    
    if( [alertView tag] == 101 ){
        
        if(buttonIndex == 0){
            
            //Get Upgrade status
            NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            //If not Upgrade then show alert for upgrade
            if([strUpgradeStatus isEqualToString:@"YES"]){
                
                //Create dictionaryb for send param in api for Trainer's Profile
                NSDictionary *params = @{
                                         kClientId              :   [NSString stringWithFormat:@"%d",self.intForDeleteTemplate],
                                         ACTION                 :   @"deleteClient",
                                         kTemplateIdForTemplate :  [NSString stringWithFormat:@"%d",self.intForDeleteTemplate]
                                         };
                
                //Invoke method for post request to delete client
                [self postRequestForDeleteClientDetail:params];
                
            }else{
                
                //Invoke method for delete client detail
                [self deleteTemplate];
                
            }
        }
        
    }
    else if( [alertView tag] == 100001 ){

//TODO: Delete Client
        MESSAGE(@"Delete client detail");
        
        if (buttonIndex == 0) {
            
            //Get Upgrade status
            NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            //If not Upgrade then show alert for upgrade
            if([strUpgradeStatus isEqualToString:@"YES"]){
                
                
                //Create dictionaryb for send param in api for Trainer's Profile
                NSDictionary *params = @{
                                         kClientId              :   delClientID,
                                         ACTION                 :   @"deleteClient"
                                         };
                
                //Invoke method for post request to delete client
                [self postRequestForDeleteClientDetail:params];
                
            }else{
        
               //Invoke method for delete client detail
                [self deleteClientDetail];
                
            }
        }
    }
    
    //Sunil->
    //--> Sunil
    //Handle the Upgradation
    if (buttonIndex == 0 && t == 921) {
        
        MESSAGE(@"alert atg : %d",t);
        
        //Invoke method for move to edit Traner profile
        [self moveToTrainerEditProfileView];
    }
    
    
    //Handle the Upgradation
    if (buttonIndex == 0 && t == 129) {
        
        MESSAGE(@"alert atg : %d",t);
        
        //Invoke method for Log out
        [self logOut];
    }
    
    
    //----> FOR TEST
    
    
 
    
    ///<-----
    
    //For  Inactive Action
    if (t == 901) {
 
        if(buttonIndex==0){
   
            //invoke method for take action for In active the current Client
            [self  takeActionForInActiveClient : t];
    }else{
            [self.tblClientDetails reloadData];

    }
}

    //For Active  Action

    //For  Inactive Action
    if (t == 902) {
        
        if(buttonIndex==0){
            
            //invoke method for take action for In active the current Client
            [self  takeActionForInActiveClient : t];
            
        }else{
            [self.tblClientDetails reloadData];
            
        }
    }
    
    //For PUrchase a new subcription plan
    if (t == 903) {
        
        if(buttonIndex==0){
    
            //Open the web app of T-notebook for purchase new subcription
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://52.26.135.139/index.php/welcome/"]];
            
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:WEB_HOST]];
            
            return;
            //Invoke method for send A mail to client also
            [self sendMailToUserForPurchseSubscriptionPlan];
            
        }
    }
    
}



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - UITextFiled Delegate Methods

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    START_METHOD
    [textField resignFirstResponder];
    return true;
}

//-----------------------------------------------------------------------

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return true;
}

//-----------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    START_METHOD
    
    NSString *strToSearch = nil;
    if (range.length > 0)
    {
        if (textField.text.length>0) {
            strToSearch = [textField.text substringToIndex:[textField.text length]-1];
            if ([strToSearch length] != 0) {
                isSearch = YES;
                [self buildNewArray:strToSearch];
            }
            else {
                isSearch = NO;
                [self.tblClientDetails reloadData];
            }
            
        }
        else
        {
            isSearch = NO;
            [self.tblClientDetails reloadData];
            [textField resignFirstResponder];
        }
    }
    else{
        strToSearch = [textField.text stringByAppendingString:string];
        isSearch = YES;
        [self buildNewArray:strToSearch];
        
    }
    
    
    return true;
}

#pragma mark
#pragma mark UIPopoverController Delegate Method


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark - AddNewTemplate Delegate Methods.

-(void)addNewTemplate:(NSString *)Template :(NSInteger)sheetContained{
    
    ProgramTemplateViewController *programTemplateViewObj = (ProgramTemplateViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramTemplateView"];
    
    BOOL boolAvailableOrNot = NO;
    self.intSheetContainedByTemplate = sheetContained;
    NSArray * arrtemp = [NSArray arrayWithArray:[[DataManager initDB]getAllTemplatesDetail]];
    
    
    MESSAGE(@"arrtemp----> %@ %@",arrtemp,programTemplateViewObj);
    for(int i = 0; i< [arrtemp count]; i++){
        
        if([[[arrtemp objectAtIndex:i]valueForKey:@"templateName"] isEqualToString:Template]){
            boolAvailableOrNot = YES;
        }
    }
    arrtemp = Nil;
    
    if(boolAvailableOrNot){
        DisplayAlertWithTitle(NSLocalizedString(@"Template Name Exists!", nil), kAppName);
        
    }else{
        programTemplateViewObj.strLableText = Template;
        programTemplateViewObj.isNewTemplate = YES;
        programTemplateViewObj.intTemplateSheets = self.intSheetContainedByTemplate;
        
        MESSAGE(@"programTemplateViewObj.strLableText: %@ %d %ld",programTemplateViewObj.strLableText,programTemplateViewObj.isNewTemplate,(long)programTemplateViewObj.intTemplateSheets);
        
      [self.navigationController pushViewController:programTemplateViewObj animated:YES];
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-(void) closePopOver{
    
    START_METHOD
    [_popOverForTemplate dismissPopoverAnimated:YES];
    _popOverForTemplate = nil;
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark-
#pragma mark AddMob Delegate Methods

//---------------------------------------------------------------------------------------------------------------
#ifdef INCLUDEAD



-(void)adViewDidReceiveAd:(GADBannerView *)view {
    START_METHOD
    if(bannerView.hidden) {
        [bannerView bringSubviewToFront:btnRemoveAdd];
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.isAdmobAddFailedToLoad = NO;
            [bannerView setHidden:FALSE];
            
            if(self.isLandScapeMode) {
                bannerView.frame = CGRectMake(0, 768, 1024, 95);
                [btnRemoveAdd setFrame:CGRectMake(1024-30, 5, 25, 25)];
                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                    bannerView.frame = CGRectMake(0, 768-95, 1024, 95);
                }completion:^(BOOL finished) {
                    [btnRemoveAdd setHidden:FALSE];
                }];
            } else {
                [btnRemoveAdd setFrame:CGRectMake(768-30, 5, 25, 25)];
                bannerView.frame = CGRectMake(0, 1024, 768, 80);
                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                    bannerView.frame = CGRectMake(0, 1024-80, 768, 80);
                }completion:^(BOOL finished) {
                    [btnRemoveAdd setHidden:FALSE];
                }];
            }
            
        });
        
    } else {
        btnRemoveAdd.hidden = FALSE;
    }
    END_METHOD
}

//-----------------------------------------------------------------------

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    START_METHOD
    self.isAdmobAddFailedToLoad = YES;
    END_METHOD
}

//-----------------------------------------------------------------------


#endif

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    START_METHOD
    return YES;
}


//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    START_METHOD
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        // landscape orientation
        [self setUpLandscapeOrientation];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];

        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
        
    } else {
        // portrait orientation
        [self setupPortraitOrientation];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];

        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    }
    
#ifdef INCLUDEAD
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:kisFullVersion] isEqualToString:@"N"]) {
        
        START_METHOD
        if(bannerView) {
            [bannerView setHidden:TRUE];
            [btnRemoveAdd setHidden:TRUE];
        }
        
        
        if(bannerView) {
            if(self.isLandScapeMode) {
                bannerView.frame = CGRectMake(0, 768, 1024, 95);
                [btnRemoveAdd setFrame:CGRectMake(1024-30, 5, 25, 25)];
                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                    bannerView.frame = CGRectMake(0, 768-95, 1024, 95);
                }completion:^(BOOL finished) {
                    [btnRemoveAdd setHidden:FALSE];
                    [bannerView bringSubviewToFront:btnRemoveAdd];
                }];
                

            } else {
                [btnRemoveAdd setFrame:CGRectMake(768-30, 5, 25, 25)];
                bannerView.frame = CGRectMake(0, 1024, 768, 80);
                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                    bannerView.frame = CGRectMake(0, 1024-80, 768, 80);
                }completion:^(BOOL finished) {
                    [btnRemoveAdd setHidden:FALSE];
                    [bannerView bringSubviewToFront:btnRemoveAdd];
                }];

            }
        }
    }
    
#endif
    
}


//-----------------------------------------------------------------------

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    START_METHOD
    if(UIInterfaceOrientationIsLandscape(fromInterfaceOrientation)) {
        //set potrait
        if([self.popover isPopoverVisible]) {
            
            
            CGRect  popoverRect = CGRectMake(self.view.center.x +150,self.view.center.y-150, 50, 50);
            [self.popover presentPopoverFromRect:popoverRect inView:self.view permittedArrowDirections:0 animated:YES];
        }
        
    } else {
        // set landscape
        if([self.popover isPopoverVisible]) {
            CGRect  popoverRect =  CGRectMake(self.view.center.y-50, self.view.center.x- 150, 50, 50);
            [self.popover presentPopoverFromRect:popoverRect inView:self.view permittedArrowDirections:0 animated:YES];
        }
    }
    
    
}

//-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
    START_METHOD
    self.isLandScapeMode = TRUE;
    [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight)];
    viewClient.frame = CGRectMake(21,83,330 ,680- 20 );
    
    [self.tblClientDetails setFrame:CGRectMake(19,91,287,560)];
    clientImgv.frame =CGRectMake(0, 42,330 ,635 -10);
    
    [imgvBg setFrame:CGRectMake(0, 20, 1024, 773)];
    btnSetting.frame = CGRectMake(953, 20,66 ,52 );
    
    
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblTraining.text font:lblTraining.font height:lblTraining.frame.size.height];
    lblTraining.frame = CGRectMake(380, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:lblNotebook.text font:lblNotebook.font height:lblNotebook.frame.size.height];
    lblNotebook.frame = CGRectMake(lblTraining.frame.origin.x + lblTraining.frame.size.width , 23,rect.size.width + 5 ,48 );
    
    imgvTopBar.frame = CGRectMake(0,20,1024 ,54 );
    [imgvTopBar setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];
    
    [viewCalender setFrame:CGRectMake(380, 70, 620, 679)];
    viewCalenderTable.frame = CGRectMake(620-352, 13,352 ,316 );
    viewTimer.frame = CGRectMake(26, 240+15,185 ,188 );
    viewTemplate.frame = CGRectMake(620-353, 375,353 ,305 );
    
    if([self.popover isPopoverVisible]) {
        [self infoBtnTapped:nil];
    }
    
    [self hideSettingsView];
    
}

//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
    START_METHOD
    self.isLandScapeMode = FALSE;
    [self.view setFrame:CGRectMake(0, 0, kPotraitWidth, kPotraitHeight)];
    viewMain.frame =CGRectMake(0, 0,768 ,1024 );
    viewClient.frame = CGRectMake(21, 112,330 ,870 );
    clientImgv.frame =CGRectMake(0, 42,330 ,826 );
    self.tblClientDetails.frame =CGRectMake(19, 91,287 ,766 );
    
    [imgvBg setFrame:CGRectMake(0,20, 773, 1024)];
    
   
   
   btnSetting.frame = CGRectMake(702, 20,66 ,52 );
    
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblTraining.text font:lblTraining.font height:lblTraining.frame.size.height];
    lblTraining.frame = CGRectMake(255 + 40, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:lblNotebook.text font:lblNotebook.font height:lblNotebook.frame.size.height];
    lblNotebook.frame = CGRectMake(lblTraining.frame.origin.x + lblTraining.frame.size.width , 23,rect.size.width + 5 ,48 );
    
    imgvTopBar.frame = CGRectMake(0, 20,768 ,54 );
    [imgvTopBar setImage:[UIImage imageNamed:@"Top-Bar"]];
    
    viewCalender.frame = CGRectMake(373, 106,352 ,876 );
    viewCalenderTable.frame = CGRectMake(0, 236,352 ,316 );
    viewTimer.frame = CGRectMake(94, 13,185 ,188 );
    viewTemplate.frame = CGRectMake(0, 571,353 ,305 );
    

    [self hideSettingsView];
    
}



//---------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark View Life Cycle Methods
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    START_METHOD
    
    [TheAppController hideHUDAfterDelay:0];
    
  
    NSLog(@"viewDidLoad--> dashboard");
    //For migration
    [[DataManager initDB] addNewFieldToClientTable];
    
    //Invoke for keep the back up file
    [self copyDatabaseIfNeededForBackup];
    
    
    int intNumberOfClinetPlan = [commonUtility retrieveValue:TOTAL_CLIENT].intValue;

    NSLog(@"[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion]: %@ and intNumberOfClinetPlan: %d",[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion],intNumberOfClinetPlan);
    
    //App is Not Full version OR Cureent plan is FRee
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"] || intNumberOfClinetPlan==3){
        
        //Invoke method for check free
        [self checkFreePlan];
        
    }
    
    //Set By default
    buttonActiveClintes.backgroundColor        =    kOptionButtonColor;
    currentList                                 =   1;

    //Get all Aseesments
    NSArray *arrayAssesmentSheetFullData =  [NSArray arrayWithArray:[[DataManager initDB] getAllAssessmentSheetFullData:@"2" ]];
    
    NSLog(@"arrayAssesmentSheetFullData: %@",arrayAssesmentSheetFullData);
    
    
    [AppDelegate sharedInstance].isFirstTime = @"Yes";
    
    [super viewDidLoad];
    [self.tblEventManager setHidden:NO];
    
    vewCntroller = self;
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kDashboardNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changePhaseTimerValue:) name:kDashboardNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kphaseImageNotification  object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changePhaseImage:) name:kphaseImageNotification  object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kupdateVersionNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateToFullVersion:) name:kupdateVersionNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kblinkTimerNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(blinkImage:) name:kblinkTimerNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kAppUpdated object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideUpdatingVersionView:) name:kAppUpdated object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadClientTableViewForImages:) name:kReloadTable object:nil];
    
    

    
//    [self setNavigationBarButton];
    [self prefersStatusBarHidden];
    
    
    if (_arrRecentClientsData == nil) {
        _arrRecentClientsData = [[NSMutableArray alloc] init];
    }
    delClientID = @"";
    
    //Sunil Var for the store current client id for active or inactive
    currentClientID         = @"";
    
    if (_eventStore == nil) {
        _eventStore = [[EKEventStore alloc] init];
    }
    
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:viewRecentClients.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(3.0, 3.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = viewRecentClients.bounds;
    maskLayer.path = maskPath.CGPath;
    viewRecentClients.layer.mask = maskLayer;
    
    bannerView.hidden = true;
    
    
    //Sunil
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //If not Upgrade then show alert for upgrade
    if(![strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke method for show alert  for upgrade
        [self showAlertForUpgrade];
        
        [commonUtility saveValue:@"NO" andKey:KEY_UPGRADE_STATUS];
    }
   
    
    //SUNIL : Hide the Option for the old user (Active, Inactive , All)
    //Check user status old or new
    if ([[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"] || ![strUpgradeStatus isEqualToString:@"YES"]){
        
        buttonInactiveClients.hidden    =   YES;
        buttonActiveClintes.hidden      =   YES;
        buttonAllClients.hidden         =   YES;
        
    }else{
        
        buttonInactiveClients.hidden    =   NO;
        buttonActiveClintes.hidden      =   NO;
        buttonAllClients.hidden         =   NO;
        
    }
    
    
    NSLog(@"Upgrade : %@  and User Type OLD: %@",[commonUtility retrieveValue:KEY_UPGRADE_STATUS],[commonUtility retrieveValue:KEY_OLD_USER]);
    NSLog(@"viewDidLoad--> dashboard->end dashbord load");

    
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    START_METHOD
     NSLog(@"bannerView-> fame:01 2818:");
    
    //Check User Authorization, If not then Log out
    if([[commonUtility retrieveValue:KEY_LOGIN_STATUS ] isEqualToString:@"NO"]){
        [self logOut];
    }
    
    
    //Sunil
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //If not Upgrade then show alert for upgrade
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        //Show button
        self.buttonLogOut.hidden    =   NO;
    }else{
        
        //Hide button
        self.buttonLogOut.hidden    =   YES;
    }
    
    //Set Hiddeen YES
     self.navigationController.navigationBarHidden = YES;
    
    [super viewWillAppear:animated];
    
    lblAddNewProgramTemplate.text = NSLocalizedString(kAddTemplateMessage, nil);
    
    viewTemplateBackground.alpha = 0.0;
    lblNoTemplateFound.hidden =YES;
    self.tblTemplates.hidden = true;
    
    [self localizeControls];
    [lblHelp setText:LocalizedString(@"Help")];
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
        [self.btnInfo setHidden:TRUE];
        [self.lblInfo setHidden:TRUE];
    } else {
        [self.btnInfo setHidden:FALSE];
        [self.lblInfo setHidden:FALSE];
    }
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:kisFullVersion]isEqualToString:@"Y"]) {
        [viewRecentClients setHidden:TRUE];
        [self.btnAddTemplate setHidden:FALSE];
        [lblAddNewProgramTemplate setHidden:FALSE];
    } else {
        [viewRecentClients setHidden:FALSE];
        [self.btnAddTemplate setHidden:TRUE];
        [lblAddNewProgramTemplate setHidden:TRUE];
    }
    
    
    [self.scrlviewEventManger setContentSize:CGSizeMake(self.scrlviewEventManger.frame.size.width, self.scrlviewEventManger.contentSize.height)];
    
//SUNIL -> 14 oct for the active and all clients
    //Check user status old or new
    if ([[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"]){
        
        
        //Get all the clients for the old user
        [self loadAllClientData:2];
        
    }else{
        
        //Get the Active clients only
        [self loadAllClientData:currentList];
    }
    
    if ([self.eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
         {
             if ( granted )
             {
                 
                 if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedCalenderEvent"] isEqualToString:@"Daily"])
                 {
                     
                     [self.imgvCalender setImage:[UIImage imageNamed:@"calender-tab-daily"]];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self loadDailyEvents];
                     });
                     
                 }
                 else
                 {
                     [self.imgvCalender setImage:[UIImage imageNamed:@"calender-tab-weekly"]];
                     [self loadRecentEventsSetType:0];
                     
                 }
                 
             }
             else
             {
             }
         }];
    }
    
    
    [self.tblEventManager setEditing:NO animated:YES];
    
    if (![[AppDelegate sharedInstance] IsPreviousVersion])
    {
        [self setLoadingViewForVersionUpgrade];
        [AppDelegate sharedInstance].IsPreviousVersion = TRUE;
    }
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        // landscape orientation
        [self setUpLandscapeOrientation];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
    }
    else {
        [self setupPortraitOrientation];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
        
        
    }
    
     NSLog(@"bannerView-> fame:01 2950 :");
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)viewDidAppear:(BOOL)animated {
    START_METHOD
    [super viewDidAppear:animated];
    
     NSLog(@"bannerView-> fame:01 :");
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        // landscape orientation
        [self setUpLandscapeOrientation];
        
        //Set 
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",LANDSCAPE] andKey:KEY_MODE];
        
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:LANDSCAPE];
    } else {
        [self setupPortraitOrientation];
        
        [commonUtility saveValue:[NSString stringWithFormat:@"%d",PORTRAIT] andKey:KEY_MODE];
        
        //Invoke methos for chnage skin
        [self actionChnageSkin:PORTRAIT];
    }
    NSLog(@"bannerView-> fame:1 :");
#ifdef INCLUDEAD
    MESSAGE(@"bannerView-> fame:21 :");
    bannerView.hidden = true;
    if([[[NSUserDefaults standardUserDefaults] valueForKey:kisFullVersion] isEqualToString:@"N"]) {
        MESSAGE(@"bannerView-> fame:31 :");
        CGRect frame = bannerView.frame;
        
        MESSAGE(@"bannerView-> fame: %@",NSStringFromCGRect(frame));
        bannerView.adUnitID = @"ca-app-pub-3833158715595946/3705586713";
        
        GADRequest *request = [GADRequest request];
        
//request.testDevices = @[ @"ac5b36db1d1b1a76842a187f57ba538c" ];
        bannerView.rootViewController = self;
        bannerView.delegate = (id)self;
        [bannerView loadRequest:request];
        [btnRemoveAdd setHidden:true];
        [btnRemoveAdd setAutoresizingMask:UIViewAutoresizingNone];
        
    }
    MESSAGE(@"bannerView-> fame:51 :");
    
#endif
    NSLog(@"bannerView-> fame:61 :");
    
}

//---------------------------------------------------------------------------------------------------------------

-(void) viewDidDisappear:(BOOL)animated {
    START_METHOD
    [super viewDidDisappear:animated];
    
    [AppDelegate sharedInstance].isFirstTime = @"No";
    
#ifdef INCLUDEAD
    if([[[NSUserDefaults standardUserDefaults] valueForKey:kisFullVersion] isEqualToString:@"N"]) {
        
        if(bannerView) {
            [bannerView removeFromSuperview];
            bannerView.delegate = nil;
            bannerView = nil;
        }
    }
#endif
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    START_METHOD
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//TODO--> Sunil

/*!
 @Description : Methods for Set View base on bg color
 @Param       :
 @Return      :
 */
-(void)setViewChnagesOnColorLandScape:(int)skin{
    START_METHOD
    MESSAGE(@"setViewChnagesOnColorLandScape--> %d",skin);
    switch (skin) {
        case FIRST_TYPE:
              [imgvBg setImage:[UIImage imageNamed:@"Default-Landscape-ipad.png"]];
            
            lblNotebook.textColor = kFirstSkin;
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor whiteColor];
            _lblTimerLabel.textColor    = [UIColor whiteColor];
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"green_training_timer_ellipse.png"]];
            //Set Timer color text
            _lblPhase.textColor         = [UIColor whiteColor];
            _lblTimerLabel.textColor    = [UIColor whiteColor];
            
            addClientImageView.image = [UIImage imageNamed:@"green_clients_Title_Bar.png"];
            
            
            _imgvCalender.image = [UIImage imageNamed:@"green_calendar_tab_weekly.png"];
            
            
            
//            [btnTimer setImage:[UIImage imageNamed:@"green_training_timer_ellipse.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor blackColor];
            lblClients.textColor = [UIColor blackColor];
            
            [_btnAddTemplate setImage:[UIImage imageNamed:@"green_add_new_assessment_button1.png"] forState:UIControlStateNormal];
            clientImgv.image  = [UIImage imageNamed:@"clients_transparent_box.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calendar_transparent_box.png"];
            
            imgvTopBar.image = [UIImage imageNamed:@"nav-bg1.png"];
            
            //[self.btnInfo setImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            [btnSetting setImage:[UIImage imageNamed:@"settings1.png"] forState:UIControlStateNormal];
            
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor darkGrayColor] forState:UIControlStateNormal];
            break;
            
        case SECOND_TYPE:
          
            [imgvBg setImage:[UIImage imageNamed:@"Default-Landscape-ipad.png"]];
            lblNotebook.textColor = ksecondSkin;
            //Set Timer color text
            _lblPhase.textColor         = [UIColor whiteColor];
            _lblTimerLabel.textColor    = [UIColor whiteColor];
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor whiteColor];
            _lblTimerLabel.textColor    = [UIColor whiteColor];
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"red_training_timer_ellipse.png"]];
            
            
            
            addClientImageView.image = [UIImage imageNamed:@"red_clients_Title_Bar.png"];
            
            
            _imgvCalender.image = [UIImage imageNamed:@"red_calendar_tab_weekly.png"];
            
            
            
//            [btnTimer setImage:[UIImage imageNamed:@"red_training_timer_ellipse.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor blackColor];
            lblClients.textColor = [UIColor blackColor];
            
            [_btnAddTemplate setImage:[UIImage imageNamed:@"red_add_new_assessment_button1.png"] forState:UIControlStateNormal];
            
            clientImgv.image  = [UIImage imageNamed:@"clients_transparent_box.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calendar_transparent_box.png"];
             imgvTopBar.image = [UIImage imageNamed:@"nav-bg1.png"];
            
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            
            [btnSetting setImage:[UIImage imageNamed:@"settings1.png"] forState:UIControlStateNormal];
            
            
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor darkGrayColor] forState:UIControlStateNormal];
            
            break;
            
        case  THIRD_TYPE:
            
            [imgvBg setImage:[UIImage imageNamed:@"Default4-Landscape-ipad.png"]];
            
            //Set Colr of Text
            lblNotebook.textColor = kThirdSkin;
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor grayColor];
            _lblTimerLabel.textColor    = [UIColor grayColor];
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"pink_training_timer_ellipse.png"]];
            //Set Timer color text
            _lblPhase.textColor         = [UIColor grayColor];
            _lblTimerLabel.textColor    = [UIColor grayColor];
            addClientImageView.image = [UIImage imageNamed:@"pink_clients_Title_Bar.png"];
            
            
            _imgvCalender.image = [UIImage imageNamed:@"pink_calendar_tab_weekly.png"];
            
            
            
//            [btnTimer setImage:[UIImage imageNamed:@"pink_training_timer_ellipse.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor grayColor];
            lblClients.textColor = [UIColor grayColor];
            
            [_btnAddTemplate setImage:[UIImage imageNamed:@"pink_add_new_assessment_button1.png"] forState:UIControlStateNormal];
            
            clientImgv.image  = [UIImage imageNamed:@"clients-bg.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calender-bg.png"];
             imgvTopBar.image = [UIImage imageNamed:@"nav-bg1.png"];
            
            
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            
            [btnSetting setImage:[UIImage imageNamed:@"settings1.png"] forState:UIControlStateNormal];
            
            
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor darkGrayColor] forState:UIControlStateNormal];
            
            break;
            
        default:
        
            //Set Colr of Text
            lblNotebook.textColor = kOldSkin;
            
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"Work"]];
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor grayColor];
            _lblTimerLabel.textColor    = [UIColor grayColor];
            
            addClientImageView.image = [UIImage imageNamed:@"clients-Title-Bar.png"];
            
            //Sunil have to change image
            [_btnAddTemplate setImage:[UIImage imageNamed:@"skycolor_add_new_assessment_button.png"] forState:UIControlStateNormal];
            
            clientImgv.image  = [UIImage imageNamed:@"clients-bg.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calender-bg.png"];
            
            
            //           _imgvCalender.image = [UIImage imageNamed:@"calendar-tab-daily.png"];
            //
            //
            //
            //            [btnTimer setImage:[UIImage imageNamed:@"calendar-tab-daily.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor grayColor];
            lblClients.textColor = [UIColor grayColor];
            
            imgvTopBar.image = [UIImage imageNamed:@"Top_Bar_Landscape"];
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn.png"] forState:UIControlStateNormal];
        
        //Sunil 5 May
//            [btnSetting setImage:[UIImage imageNamed:@"settingsOld"] forState:UIControlStateNormal];
        
            
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
            break;
    }
}

-(void)setViewChnagesOnColorPortraitScape:(int)skin{
    imgvTopBar.image = [UIImage imageNamed:@"Top_Bar_Landscape"];
    MESSAGE(@"setViewChnagesOnColorPortraitScape--> %d",skin);
    switch (skin) {
        case FIRST_TYPE:
            
            [imgvBg setImage:[UIImage imageNamed:@"Default-Portrait.png"]];
            
            lblNotebook.textColor =kFirstSkin;
            
            
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor whiteColor];
            _lblTimerLabel.textColor    = [UIColor whiteColor];
            
            addClientImageView.image = [UIImage imageNamed:@"green_clients_Title_Bar.png"];
            
            
            _imgvCalender.image = [UIImage imageNamed:@"green_calendar_tab_weekly.png"];
            
           
         //Comment Sunil 6 apr
//            [btnTimer setImage:[UIImage imageNamed:@"green_training_timer_ellipse.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor blackColor];
            lblClients.textColor = [UIColor blackColor];
            
              [_btnAddTemplate setImage:[UIImage imageNamed:@"green_add_new_assessment_button1.png"] forState:UIControlStateNormal];
            
            clientImgv.image  = [UIImage imageNamed:@"clients_transparent_box.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calendar_transparent_box.png"];

             imgvTopBar.image = [UIImage imageNamed:@"nav-bg1.png"];
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            
            [btnSetting setImage:[UIImage imageNamed:@"settings1.png"] forState:UIControlStateNormal];
            
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"green_training_timer_ellipse.png"]];
           
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor darkGrayColor] forState:UIControlStateNormal];
            
            break;
            
        case SECOND_TYPE:
            
            [imgvBg setImage:[UIImage imageNamed:@"Default-Portrait.png"]];
            
            //Label Notebook Tile color
            lblNotebook.textColor = ksecondSkin;
            //Set Timer color text
            _lblPhase.textColor         = [UIColor whiteColor];
            _lblTimerLabel.textColor    = [UIColor whiteColor];
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"red_training_timer_ellipse.png"]];
            
            
            
            addClientImageView.image = [UIImage imageNamed:@"red_clients_Title_Bar.png"];
            
            
            _imgvCalender.image = [UIImage imageNamed:@"red_calendar_tab_weekly.png"];
            
            
            
//            [btnTimer setImage:[UIImage imageNamed:@"red_training_timer_ellipse.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor blackColor];
            lblClients.textColor = [UIColor blackColor];
            
            [_btnAddTemplate setImage:[UIImage imageNamed:@"red_add_new_assessment_button1.png"] forState:UIControlStateNormal];
             clientImgv.image  = [UIImage imageNamed:@"clients_transparent_box.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calendar_transparent_box.png"];

             imgvTopBar.image = [UIImage imageNamed:@"nav-bg1.png"];
            
            
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            
            [btnSetting setImage:[UIImage imageNamed:@"settings1.png"] forState:UIControlStateNormal];
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor darkGrayColor] forState:UIControlStateNormal];
            break;
            
        case THIRD_TYPE:
           
            [imgvBg setImage:[UIImage imageNamed:@"Default4-Portrait.png"]];
            
            //Set Colr of Text
            lblNotebook.textColor = kThirdSkin;
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"pink_training_timer_ellipse.png"]];
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor grayColor];
            _lblTimerLabel.textColor    = [UIColor grayColor];
            addClientImageView.image = [UIImage imageNamed:@"pink_clients_Title_Bar.png"];
            
            
            _imgvCalender.image = [UIImage imageNamed:@"pink_calendar_tab_weekly.png"];
            
            
            
//            [btnTimer setImage:[UIImage imageNamed:@"pink_training_timer_ellipse.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor grayColor];
            lblClients.textColor = [UIColor grayColor];
            
            [_btnAddTemplate setImage:[UIImage imageNamed:@"pink_add_new_assessment_button1.png"] forState:UIControlStateNormal];
            clientImgv.image  = [UIImage imageNamed:@"clients-bg.png"];
            calenderBgImageView.image = [UIImage imageNamed:@"calender-bg.png"];
            
             imgvTopBar.image = [UIImage imageNamed:@"nav-bg1.png"];
            
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn1.png"] forState:UIControlStateNormal];
            
            [btnSetting setImage:[UIImage imageNamed:@"settings1.png"] forState:UIControlStateNormal];
            
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor darkGrayColor] forState:UIControlStateNormal];
            break;
            
            
        default:
            //Set Colr of Text
            lblNotebook.textColor = kOldSkin;
            
            //Set Image
            [self.imgvewPhase setImage:[UIImage imageNamed:@"Work"]];
            
            //Set Timer color text
            _lblPhase.textColor         = [UIColor grayColor];
            _lblTimerLabel.textColor    = [UIColor grayColor];
            
            addClientImageView.image = [UIImage imageNamed:@"clients-Title-Bar.png"];
          
            //Sunil have to change image
            [_btnAddTemplate setImage:[UIImage imageNamed:@"skycolor_add_new_assessment_button.png"] forState:UIControlStateNormal];
            clientImgv.image  = [UIImage imageNamed:@"clients-bg.png"];  
            calenderBgImageView.image = [UIImage imageNamed:@"calender-bg.png"];
            
//           _imgvCalender.image = [UIImage imageNamed:@"calendar-tab-daily.png"];
//            
//            
//            
//            [btnTimer setImage:[UIImage imageNamed:@"calendar-tab-daily.png"] forState:UIControlStateNormal];
            
            lblCalendar.textColor = [UIColor grayColor];
            lblClients.textColor = [UIColor grayColor];
            
              imgvTopBar.image = [UIImage imageNamed:@"Top_Bar_Landscape"];
         
            
            [btnHelp setBackgroundImage:[UIImage imageNamed:@"Help-btn.png"] forState:UIControlStateNormal];
        
        //Sunil 5  Apr
//            [btnSetting setImage:[UIImage imageNamed:@"settingsOld.png"] forState:UIControlStateNormal];
        
            //Set Option buton txt color
            [buttonActiveClintes    setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [buttonAllClients       setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [buttonInactiveClients  setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
            break;
    }
    
  
 
    END_METHOD
}

-(void)setCellTextColor:(TemplateCustomeCell*)cell{
    START_METHOD
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    
    
    MESSAGE(@"setCellTextColor------> %d",skin);
    //Set White
    if (skin == FIRST_TYPE || skin == SECOND_TYPE) {
    cell.lblSheetContained.textColor =[UIColor whiteColor];
    cell.lblTemplateName.textColor = [UIColor whiteColor];

        /*calenderBgImageView.image = nil;
        clientImgv.image = nil;
         */
    }else{
        cell.lblSheetContained.textColor =[UIColor grayColor];
        cell.lblTemplateName.textColor = [UIColor grayColor];
    }
    
    
    END_METHOD
}
//---------------------------------------------------------------------------------------------------------------

+(ViewController *)sharedInstance {
    
    START_METHOD
    if(vewCntroller == nil) {
        vewCntroller = [[ViewController alloc]init];
    }
    return vewCntroller;
}


-(void)setDailyEventsCellImage:(UIImageView *)imgView{
    START_METHOD
    //Sunil ->
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    
    switch (skin) {
        case FIRST_TYPE:
            imgView.image = [UIImage imageNamed:@"clock_button.png"];
            break;

        case SECOND_TYPE:
            imgView.image = [UIImage imageNamed:@"red_clock_button.png"];
            break;

        case THIRD_TYPE:
            imgView.image = [UIImage imageNamed:@"pink_clock_button.png"];
            break;
       
        default:
            imgView.image = [UIImage imageNamed:@"calendertime.png"];

            break;
    }
    END_METHOD
}

/*!
 @Description : Methods for set cell image sunil
 @Param       :
 @Return      :
 */
-(void)cellImageForTemplate:(UIImageView *)imgView{
    START_METHOD
    
    //Sunil ->
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    
    switch (skin) {
        case FIRST_TYPE:
            imgView.image = [UIImage imageNamed:@"green_list_button.png"];
            break;
            
        case SECOND_TYPE:
            imgView.image = [UIImage imageNamed:@"red_list_button.png"];
            break;
            
        case THIRD_TYPE:
            imgView.image = [UIImage imageNamed:@"pink_list_button.png"];
            break;
            
        default:
            imgView.image = [UIImage imageNamed:@"list.png"];
            
            break;
    }
    END_METHOD
}

//sunil
-(void)setSkinnOptionButton:(UIButton *)btn2{
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            [btn2 setBackgroundColor:kFirstSkin];
            
            break;
            
        case SECOND_TYPE:
            [btn2 setBackgroundColor:ksecondSkin];
            
            break;
        case THIRD_TYPE:
            [btn2 setBackgroundColor:kThirdSkin];
            
            break;
        default:
            [btn2 setBackgroundColor:kOldSkin];
            
            break;
    }
}

//method for show alert for upgrade
-(void)showAlertForUpgrade{
    //Craete alert message
  
    NSString *strTrainerName;
    
    if([commonUtility retrieveValue:kTrainerName]){
        strTrainerName =[commonUtility retrieveValue:kTrainerName];
    }else{
        strTrainerName =@"";
    }
    
    NSArray *arrayAllClients                    = [NSArray arrayWithArray:[[DataManager initDB] getTotalClientsCount]];
    NSArray *arrayAllWithoutMigrationClients    = [NSArray arrayWithArray:[[DataManager initDB] getClientsCountWithoutMigration]];

    
    NSLog(@"arrayAllClientsarrayAllClients : %@ and %@",arrayAllClients,arrayAllWithoutMigrationClients);
    NSString *strCount;
    
    if (arrayAllClients && arrayAllClients.count>0 && arrayAllWithoutMigrationClients && arrayAllWithoutMigrationClients.count>0) {
        
        
        NSDictionary *dictTotalClient       = [arrayAllClients objectAtIndex:0];
        NSDictionary *dictNotMigratedClient = [arrayAllWithoutMigrationClients objectAtIndex:0];

        NSString  *strTotal = [dictTotalClient objectForKey:@"count"] ;
        NSString  *strNotMigrated = [dictNotMigratedClient objectForKey:@"count"] ;
        
        strCount = [NSString stringWithFormat:@"%d / %d ",strNotMigrated.intValue,  strTotal.intValue];
    }
    
    
    
    NSString *strTempMessage        =   [NSString stringWithFormat:@"Hey %@, All your client's info will now be saved online. You can access your account from any iPad with just your username and password. The Internet is now required. For this you have to update your profile.\n\n %@ needs to be migrated. \n\n Click Update to proceed.",strTrainerName,strCount];
    
    NSString *alertMessage  =   NSLocalizedString(strTempMessage, nil);
    
    //Create alert for upgrade
    UIAlertView *alertView      = [[UIAlertView alloc]initWithTitle:@"Congratulations" message:alertMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Update", nil) otherButtonTitles:NSLocalizedString(@"Cancel", nil), nil];
    
    //Set Random alert vi
        alertView.tag      = 921;
        
        [alertView show];
}

//Move to Edit Profile view
-(void)moveToTrainerEditProfileView{
   
    START_METHOD
    //Create object of trainer profile view
    TrainerProfileViewController *objMenuView  = (TrainerProfileViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"TrainerProfileViewController"];
    
    //Push view
    [self.navigationController pushViewController:objMenuView animated:NO];
    
    END_METHOD
}

#pragma mark - Action methods
- (IBAction)actionLogOut:(id)sender {
    START_METHOD
    
    
    //Craete alert message
    NSString *alertMessage  =   NSLocalizedString(@"Are you sure want to Log Out!", nil);
    
    //Create alert for upgrade
    UIAlertView *alertView      = [[UIAlertView alloc]initWithTitle:kAppName message:alertMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Yes", nil) otherButtonTitles:NSLocalizedString(@"Cancel", nil), nil];
    
    //Set Random alert vi
    alertView.tag      = 129;
    
    [alertView show];
    
    END_METHOD
}

//MEthod for lOg out
-(void)logOut{
    START_METHOD
    
    //Invoke method for delete the images of clients
    [self deleteAllImagesOfAllClients];
    
    //Invoke method 
    [self copyDatabaseIfNeeded];
    
    //==========Add Last edited screen to client table ================//
    
    [[DataManager initDB] addNewFieldToClientTableIfDoesNotExist];
    
    //=====================================================//
    
    [self performSelector:@selector(CreateTableIfNotExist) withObject:nil afterDelay:0];
    [self performSelector:@selector(CreateTableIfNotExist_OLD) withObject:nil afterDelay:0];
    
    
    //Save lout Key
    [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
    [commonUtility saveValue:nil andKey:EMAIL];
    [commonUtility saveValue:nil andKey:PASSWORD];
    [commonUtility saveValue:nil andKey:KEY_TRAINER_ID];
    
    //Create object of trainer profile view
    SignViewController *objMenuView  = (SignViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SignViewController"];
    
    //Push view
    [self.navigationController pushViewController:objMenuView animated:NO];
}


#pragma mark -
#pragma mark prior to database work methods

- (NSString *) getDBPath {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString * documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"TrainerClipboardDatabase.sqlite"];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) copyDatabaseIfNeeded {
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSError * error;
    NSString * dbPath = [self getDBPath];
    
    
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if (success){
        success = [fileManager removeItemAtPath:dbPath error:&error];
    }
    
    
    if(success) {
        NSString * defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"TrainerClipboardDatabase.sqlite"];
        
        MESSAGE(@"defaultDBPath: %@",defaultDBPath);
        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
}
- (void) CreateTableIfNotExist_OLD{
    NSArray *array = [[DataManager initDB]getClientsID];
    int i;
    for(i=0;i<[array count];i++){
        [[DataManager initDB] createClientTablesIfNotExist:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void) CreateTableIfNotExist{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *previousVersion=[defaults objectForKey:VERSION_KEY_FOR_DB];
    NSString *currentVersion=[self versionNumberString];
    
    if(previousVersion != nil) {
        return;
    }
    
    if(previousVersion == nil || [previousVersion compare:currentVersion options:NSNumericSearch] == NSOrderedAscending) {
        
        
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        
        
        NSArray *array = [[DataManager initDB]getClientsID];
        int i;
        
        @try {
            
            for (i = 0; i< [array count]; i++) {
                [[DataManager initDB] createClientTablesIfNotExist:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
                [[DataManager initDB] addNewFieldsToTablesIfnotAddedForClientId:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
            }
            [defaults setObject:currentVersion forKey:VERSION_KEY_FOR_DB];
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:kAppUpdated object:nil];
            
        }
        @catch (NSException *exception) {
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:kAppUpdated object:nil];
            return;
        }
        
    }
    
    
}


//TODO: POST Request for delete client Detail
-(void)postRequestForDeleteClientDetail:(NSDictionary *)params{
    START_METHOD
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@deleteData/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"requestForPassword-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               
                               if([params objectForKey:kTemplateIdForTemplate]){
                                   
                                   //Invoke method for del;ete the temppalte data
                                   [self deleteTemplate];
                               }else{
                                   
                                   
                                   //Delete from Local
                                   [self deleteClientDetail];

                               }
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                           }];
    END_METHOD
    
}

//TODO: Delete client detail
-(void)deleteClientDetail{
    if ([[DataManager initDB] deleteClientDetail:delClientID] == 0) {
        NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data",delClientID];
        NSString * dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
        [FileUtility deleteFile:dirPath];
        
        imageFolder = [NSString stringWithFormat:@"ClientsSignature/Signature%@.png",delClientID];
        dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
        [FileUtility deleteFile:dirPath];
        
        
        
        imageFolder = [NSString stringWithFormat:@"ClientsProfileImage/ProfileImage%@.png",delClientID];
        dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
        [FileUtility deleteFile:dirPath];
        
        
        DisplayAlertWithTitle(NSLocalizedString(@"Client's details deleted successfully.", nil) ,kAppName);

        //SUNIL -> 14 oct for the active and all clients
        //Check user status old or new
        if ([[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"]){
            
            
            //Get all the clients for the old user
            [self loadAllClientData:2];
            
        }else{
            
            //Get the Active clients only
            [self loadAllClientData:currentList];
        }
    }
    else {
        DisplayAlertWithTitle(NSLocalizedString(@"Client's details not deleted successfully.", nil) ,kAppName);
    }
}
//---------------------------------------------------------------------------------------------------------------




//TODO: GEt client Profile image  from server
-(void)getClientProfileImage:(NSString *)strClientId andIndexPath:(NSIndexPath *)indexPath{
    START_METHOD
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{

                    HomeCustomCell *updateCell = (id)[self.tblClientDetails cellForRowAtIndexPath:indexPath];
                    if (updateCell){
                        
                       updateCell.ImgTable.image = image;
                        
                        //Invoke method for save in local
                        [self saveClientsProfileImage:strClientId andImage:image];
                    }
                });
            }
        }
    }];
    [task resume];
    
    END_METHOD
}



//Method for save signature in local
-(void)saveClientsProfileImage :(NSString *) clientId andImage:(UIImage *)img{
    
    START_METHOD
    if(![clientId isEqualToString:@"0"]) {
        
        [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsProfileImage",[FileUtility basePath]]];
        NSString * imgNm = [NSString stringWithFormat:@"ProfileImage%@",clientId];
        NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
        [FileUtility createFileInFolder:@"ClientsProfileImage" withName:imgNm withData:imageData];
    }
    END_METHOD
}


//Method for delete all images of client
-(void)deleteAllImagesOfAllClients{
    
    
    //All Clients with Deatils
    NSArray *arrayAllClientsID = [NSArray arrayWithArray:[[DataManager initDB] getAllClientsID]];
    
    MESSAGE(@"All clients ID: %@",arrayAllClientsID);
    
    for (int intClientId = 0; intClientId<arrayAllClientsID.count; intClientId++) {
        
        NSDictionary *objDictClient = [arrayAllClientsID objectAtIndex:intClientId];

            NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data",[objDictClient objectForKey:kClientId]];
            NSString * dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
            [FileUtility deleteFile:dirPath];
            
        imageFolder = [NSString stringWithFormat:@"ClientsSignature/Signature%@.png",[objDictClient objectForKey:kClientId]];

            dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
            [FileUtility deleteFile:dirPath];
            
            
            
        imageFolder = [NSString stringWithFormat:@"ClientsProfileImage/ProfileImage%@.png",[objDictClient objectForKey:kClientId]];

            dirPath = [NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder];
            [FileUtility deleteFile:dirPath];
            
       
    }
}


//TODO: REload tbale for images of clients at time login
-(void)reloadClientTableViewForImages:(NSNotification *)noti{
    START_METHOD
    
    [self.tblClientDetails reloadData];
    
    END_METHOD
}

//TODO: Delete Tempalte

-(void)deleteTemplate{
    
    NSArray * arrtemp = [NSArray arrayWithArray:[[DataManager initDB]getSheetsOfTemplate:self.intForDeleteTemplate]];
    
    for(int i = 0; i < [arrtemp count]; i++){
        NSDictionary * tempdic = [NSDictionary dictionaryWithDictionary:[arrtemp objectAtIndex:i]];
        int sheetid = [[tempdic valueForKey:@"SheetId"] intValue];
        [[DataManager initDB] deleteSheetBlock_BlockData:sheetid];
        tempdic = Nil;
    }
    int temp = [[DataManager initDB] deleteTemplate_TemplateSheet:self.intForDeleteTemplate];
    [self.tblTemplates setBackgroundColor:[UIColor clearColor]];
    if(temp == 1){
        NSArray *arytemp = [[DataManager initDB] getAllTemplatesDetail];
        if([[AppDelegate sharedInstance] isNotEmptyArray:arytemp]) {
            self.arrTemplateData = [NSArray arrayWithArray:arytemp];
        } else {
            self.arrTemplateData = nil;
        }
        
        if (self.arrTemplateData != nil && ![self.arrTemplateData isKindOfClass:[NSNull class]] && self.arrTemplateData.count == 0 && self.tblTemplates.tag == 1001) {
            viewTemplateBackground.alpha    = 0.7;
            lblNoTemplateFound.hidden       = NO;
            self.tblTemplates.hidden        = true;
        }
        else{
            viewTemplateBackground.alpha    = 0.0;
            lblNoTemplateFound.hidden       = YES;
            self.tblTemplates.hidden        = false;
        }
        [self.tblTemplates reloadData];
        DisplayAlertWithTitle(NSLocalizedString(@"Template details deleted successfully.", @"")  ,kAppName);
    }
    else{
        
        DisplayAlertWithTitle(NSLocalizedString(@"Template details Not deleted successfully.", @"")  ,kAppName);
        
    }
}


//TODO: NEW Features


//TODO: Active clients
- (IBAction)actionShowActiveClients:(id)sender {
    
    START_METHOD
    
    buttonActiveClintes.backgroundColor     =   kOptionButtonColor;
    buttonInactiveClients.backgroundColor   =   [UIColor clearColor];;
    buttonAllClients.backgroundColor        =    [UIColor clearColor];
    
    currentList =   1;
    
    //Get the Active clients only
    [self loadAllClientData:currentList];
    
    
    END_METHOD
}



//TODO: Inactive Clients

- (IBAction)actionInactiveClients:(id)sender {
    
    START_METHOD
    
    //Change the background color
    buttonActiveClintes.backgroundColor     =   [UIColor clearColor];;
    buttonInactiveClients.backgroundColor   =   kOptionButtonColor;
    buttonAllClients.backgroundColor        =    [UIColor clearColor];;
    
//    [buttonActiveClintes setBackgroundImage:[UIImage imageNamed:@"tabSection.png"] forState:UIControlStateNormal];
    
    //Set current list - 0
    currentList =   0;
    
    //Get the In Active clients only
    [self loadAllClientData:currentList];
    
    END_METHOD
}


//TODO: All Clients

- (IBAction)actionShowAllClients:(id)sender {
    
    START_METHOD
    
    
    buttonActiveClintes.backgroundColor     =   [UIColor clearColor];;
    buttonInactiveClients.backgroundColor   =   [UIColor clearColor];
    buttonAllClients.backgroundColor        =   kOptionButtonColor;
    
    currentList = 2;
    
    //Get the ALL clients only
    [self loadAllClientData:currentList];
    END_METHOD
}



//TODO: Action method for Swict / Toggle button

//Switch method for take actoin on on off
- (void)changeSwitch:(id)sender{
    START_METHOD
    //GEt Switch button
    UISwitch *buttonSwitchOff   =   (UISwitch *)sender;
    
    ///get Tag og button
    int    intButtonTag         =  (int) buttonSwitchOff.tag;
    
    
    //Get the client Dictionary
    NSDictionary *dictCleint    =   [self.arrClientsDetail objectAtIndex:intButtonTag];
    
    
    NSString *strName;
    
    //If change switch in search
    if (isSearch) {
        
        
        //Get the cleint Name
        strName  = [NSString stringWithFormat:@"%@ %@",[[self.arrSearchData objectAtIndex:intButtonTag] objectForKey:kFirstName],[[self.arrSearchData objectAtIndex:intButtonTag] objectForKey:kLastName]];
        
  currentClientID = [NSString stringWithFormat:@"%@,%@",[[self.arrSearchData objectAtIndex:intButtonTag] objectForKey:kClientId],[[self.arrSearchData objectAtIndex:intButtonTag] objectForKey:STATUS]];
        
        
        
        NSString *strStatus = [[self.arrSearchData objectAtIndex:intButtonTag] objectForKey:STATUS];
        MESSAGE(@"strStatus=====> switch: %@",strStatus);
        
        
        
        //If switch in ON
        if([sender isOn] && [strStatus isEqualToString:@"YES"]){
            
            return;
        }
        
        //If switch in ON
        if(![sender isOn] && [strStatus isEqualToString:@"NO"]){
            
            return;
        }
        
        
    
    }else{
                
                //Get the cleint Name
                strName  = [NSString stringWithFormat:@"%@ %@",[[self.arrClientsDetail objectAtIndex:intButtonTag] objectForKey:kFirstName],[[self.arrClientsDetail objectAtIndex:intButtonTag] objectForKey:kLastName]];
                
                
                currentClientID = [NSString stringWithFormat:@"%@,%@",[[self.arrClientsDetail objectAtIndex:intButtonTag] objectForKey:kClientId],[[self.arrClientsDetail objectAtIndex:intButtonTag] objectForKey:STATUS]];
        
        NSString *strStatus = [[self.arrClientsDetail objectAtIndex:intButtonTag] objectForKey:STATUS];
        MESSAGE(@"strStatus=====> switch: %@",strStatus);
        
        //If switch in ON
        if([sender isOn] && [strStatus isEqualToString:@"YES"]){
        
            return;
        }
        
        //If switch in ON
        if(![sender isOn] && [strStatus isEqualToString:@"NO"]){
            
            return;
        }
        
            }
    


    
    //If switch in ON
    if([sender isOn]){
    
        
        int intNumberOfClinetPlan = [commonUtility retrieveValue:TOTAL_CLIENT].intValue;
        
        NSString *strAlert;
        
        //Alert Show
        UIAlertView * alertView;
        
        //Get the all clinet number
        NSArray *arrayOfTotalClient = [[DataManager initDB] getClientsListForShow:@"1"];

        if(intNumberOfClinetPlan>arrayOfTotalClient.count){
            
            //If unlimited
            if (intNumberOfClinetPlan>100) {
                
                    strAlert  =   [NSString stringWithFormat:@"Are you sure, you want to Active '%@'.",strName];
                
            }else{
            
            strAlert  =   [NSString stringWithFormat:@"In the current plan, You can have only %d Active clients. \n\nAre you sure, you want to Active '%@'.",intNumberOfClinetPlan,strName];
            
            }
            
            
            alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(strAlert,@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
            
            [alertView setTag:902];
            
        }else{
            
            //YOU can not active more  accordding to plan
            strAlert  =   [NSString stringWithFormat:@"You cann't active more client. Because, In the current plan you have access for only  %d  active client.",intNumberOfClinetPlan];
            
            alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(strAlert,@"") delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alertView setTag:9002];
            
            [self.tblClientDetails reloadData];
        }
         [alertView show];
        
    } else{
        
        int intNumberOfClinetPlan = [commonUtility retrieveValue:TOTAL_CLIENT].intValue;
        
        NSString *strAlert;
        
        //Alert Show
        UIAlertView * alertView;
        
        
        //Get the all clinet number
        NSArray *arrayOfTotalClient = [[DataManager initDB] getClientsListForShow:@"2"];
        if(intNumberOfClinetPlan>=arrayOfTotalClient.count){
            
            strAlert  =   [NSString stringWithFormat:@"Are you sure, You want to In-Active '%@'.",strName];
            
             alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(strAlert,@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
            
            [alertView setTag:901];

        }else{
            
            
            strAlert  =   [NSString stringWithFormat:@"By making this client inactive, You will not able to Active again this client until one month completed. \n\nAre you sure, you want to In-Active '%@'.",strName];
            
            
            
          alertView  = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(strAlert,@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
            
            //Set the alert view tag to identify the alert
            [alertView setTag:901];

        }
      
        [alertView show];
        
        
    }
    
    //After take action
//    [self.tblClientDetails reloadData];
    
    MESSAGE(@"client Deatil : %@",dictCleint);
    END_METHOD
    
}


//TODO: : SERVER HANDLER Active / In Active

//Method for Post request for active and inactive clients
-(void)takeActionForInActiveClient: (int)intActionType{
    START_METHOD
    MESSAGE(@"currentClientID -> %@",currentClientID);

    NSArray *arrayOfString  = [currentClientID componentsSeparatedByString:@","];
    
    if (!arrayOfString && arrayOfString.count<2) {
     
        //Invoke method for log out user
        [self logOut];
        
        return;
    }
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             kClientId          :   [arrayOfString objectAtIndex:0],
                             @"client_status"   :   [arrayOfString objectAtIndex:1],
                             @"user_id"         :   [commonUtility retrieveValue:KEY_TRAINER_ID]
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@updateClientStatus/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce: %@", responseDataDictionary);
                               
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   //************  GET Trainer's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictClientStatus =  [ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] ;
                                   MESSAGE(@"arrayUserData : dictTrainerProfile: %@",dictClientStatus);
                                   
                                   NSMutableDictionary *clientDict  = [[commonUtility dictionaryByReplacingNullsWithStrings:dictClientStatus] mutableCopy];
                                   
                                   [clientDict setObject:[params objectForKey:kClientId] forKey:kClientId];
                                   
                                   //Invoke method for updat the client status in local Db
                                   [[DataManager initDB] updateClientStatus:clientDict  ];
                                   
                                   
                                   //For Update table after update databse (Update instantly than crash) //SUNIL 24 oct
                                   [self performSelector:@selector(updateTableAfterSatusChanged) withObject:nil afterDelay:2];

                                   
                               }else if([responseDataDictionary objectForKey:METADATA]){                               //Check if get error for Expire token

                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   NSString *strErrorMessage = [dictError objectForKey:POPUPTEXT];
                                   
                                   
                                   if(strErrorMessage.length>0){
                                   //Show Error Alert
                                       
                                       //create alertView
                                       UIAlertView *alertMessage = [[UIAlertView alloc]
                                                                    initWithTitle:@"" message:[NSString stringWithString:strErrorMessage]
                                                                    delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                       //Show alertView
                                       [alertMessage show];
                                       
                                       
                                       
                                       if([strErrorMessage isEqualToString:@"You are not Authorized. Please Login Again."]){
                                       
                                       //Log Out
                                       [self logOut];
                                           
                                           
                                       }else{
                                           
                                           //Get  clients after update and realod table
                                           [self loadAllClientData:currentList];
                                           
                                           //Hide the indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                       }
                                       
                                       
                                       //Hide the indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                   
                                   }else{
                                   
                                   //Get  clients after update and realod table
                                   [self loadAllClientData:currentList];
                                   
                                   //Hide the indicator
                                   [TheAppController hideHUDAfterDelay:0];
                                   }
                                   
                               }
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                           }];
    END_METHOD
}




//TODO: Send mail to the User for Purchase new subcription
-(void)checkAddClientStatus{
    START_METHOD
    MESSAGE(@"checkAddClientStatus--> ");
    
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             @"user_id"          :   [commonUtility retrieveValue:KEY_TRAINER_ID],
                             
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@trainerAddClientStatus/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce: %@", responseDataDictionary);
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   //************  GET Trainer's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictTrainerProfile =  [ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] ;
                                   MESSAGE(@"arrayUserData : dictTrainerProfile: %@",dictTrainerProfile);
                                   
                                   //For Old User, He can Add any number of clients for full version
                                   [commonUtility saveValue:[dictTrainerProfile objectForKey:@"status"] andKey:KEY_STATUS_ADD_CLIENT];
                                   
                                   
                                   if([[commonUtility retrieveValue:KEY_STATUS_ADD_CLIENT] isEqualToString:@"NO"]){

                                   //Show Alert for Purchase Subscription
                                   UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(@"You have limited access to add client features. In order to add more clients, please purchase a new subscription plan. Would you like to purchase?",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
                                   
                                   
                                   [alertView setTag:903];
                                   [alertView show];
                                   }
                                   
                                   //Hide
                                   [TheAppController hideHUDAfterDelay:0];
                                   
                               }
                               
                               //Check if get error for Expire token
                               if([responseDataDictionary objectForKey:METADATA]){
                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   
                                   NSString *strErrorMessage = [dictError objectForKey:POPUPTEXT];
                                   
                                   
                                   if(strErrorMessage.length>0){
                                   //Show Error Alert
                                   [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       
                                       //Log out User
                                       [self logOut];
                                   }
                                   
                                   //Hide the indicator
                                   [TheAppController hideHUDAfterDelay:0];
                                   
                                   
                               }
                               
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide indicator
                               [TheAppController hideHUDAfterDelay:0];
                           }];
    END_METHOD
}



//TODO: Send mail to the User for Purchase new subcription
-(void)sendMailToUserForPurchseSubscriptionPlan{
    START_METHOD
    MESSAGE(@"sendMailToUserForPurchseSubscriptionPlan ");
    
    
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             @"userID"          :   [commonUtility retrieveValue:USER_ID],
                             
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce: %@", responseDataDictionary);

                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               
                           }];
    END_METHOD
}


//Method for update table after status changed
-(void)updateTableAfterSatusChanged{
    START_METHOD
    [TheAppController showHUDonView:nil];
    
    //Get In Active clients after update and realod table
    [commonUtility alertMessage:@"Client Status Updated!"];
    
    
    //Get  clients after update and realod table
    [self loadAllClientData:currentList];
    
    //Hide the indicator
    [TheAppController hideHUDAfterDelay:0];
    
    END_METHOD
}


//TODO: CHECK for free plan
-(void)checkFreePlan{
    
        START_METHOD
        // Check the status for the Upgrade  status version of ther app
        if([[commonUtility retrieveValue:KEY_UPGRADE_STATUS] isEqualToString:@"YES"]){
            
            //Show Alert for Purchase Subscription
            UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"")  message:NSLocalizedString(@"You have limited access. In order to full access, please purchase a new subscription plan. Would you like to purchase?",@"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",@""),NSLocalizedString(@"No",@""), nil];
            
            
            //Set the Alert view tag
            [alertView setTag:903];
            
            //Show the alert view for confiramtion
            [alertView show];
        }else{
            
            ///Alert for make payment from the itune store
            //            DisplayAlertWithYesNo(kliteMsg,kAppName,self);  // SUNIL Comment for Update the Profile and migrate the data v_5.0
            
            //Invoke method for move to edit Traner profile
//            [self moveToTrainerEditProfileView];
            
            return;
            
        }
        return;
    
    END_METHOD
}



///TODO: Get clienty info from server
-(void)getClientInfoFromServer:(NSMutableDictionary *)objDictClient{
    START_METHOD
    
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             USER_ID              :   [commonUtility retrieveValue:KEY_TRAINER_ID],
                             kClientId            :   [objDictClient objectForKey:kClientId],
                             @"access_key"        :   USER_ACCESS_TOKEN,
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@clientInfo",HOST_URL];
    
    
    MESSAGE(@"postRequestToServerForTrainerProfile-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                               
                               
                               //Invoke method for handle responce
                               [self saveClientInfoInLocal:responseDataDictionary andClientId: objDictClient];
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                               [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                                    
                           }];
    END_METHOD
}



#pragma mark - Handle Response fromServer

//Method for Take action on update profile
-(void)saveClientInfoInLocal: (NSDictionary *)responseDataDictionary andClientId:(NSMutableDictionary *)objDictClient{
    
    MESSAGE(@"saveClientInfoInLocal: -> responce: %@", responseDataDictionary);
    
    //If Success
    if([responseDataDictionary objectForKey:PAYLOAD]){
        
//        
//        //************  GET Trainer's Profile AND SAVE IN LOCAL DB
//        NSString  *strAccessKey         =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:KEY_ACCESS];
//        
//        
//        //Save Access key
//        [commonUtility saveValue:strAccessKey andKey:KEY_ACCESS];
//        
        
//        //************  GET CLEINTS LIST AND SAVE IN LOCAL DB
//        NSArray *arrayclients           =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"client_list"];
//        MESSAGE(@"arrayUserData : %@",arrayclients);
//        
//        
//        //Save Clients
//        [self saveClients:arrayclients];
        
        
        //************  GET CLEINTS ANSWERS AND SAVE IN LOCAL DB
        NSArray *arrayclientsAnswers    =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"answer_list"];
        MESSAGE(@"arrayUserData arrayclientsAnswers: %@",arrayclientsAnswers);
        
        //Save Client's Answers
        [self saveClientsAnswers:arrayclientsAnswers];
        
        
        
        //************  GET CLEINT'S PROGRAMS LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsprogram    =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"program_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsprogram: %@",arrayclientsprogram);
        
        //Save Client's program
        [self saveClientsProgram:arrayclientsprogram];
        
        
        
        //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsWorkoutsAndNotes =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"workout_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsWorkoutsAndNotes: %@",arrayclientsWorkoutsAndNotes);
        
        //Save Clients workouts and notes
        [self saveClientsWorkoutsAndNotes:arrayclientsWorkoutsAndNotes];
        
        
        
        //************  GET CLEINT'S Exercise LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsExercise   =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"exercise_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsExercise: %@",arrayclientsExercise);
        
        //Save Client's exercise
        [self saveClientsExercise:arrayclientsExercise];
        
        
        //#************  GET CLEINT'S Assessment and SAVE IN LOCAL DB
        NSArray *arrayclientAssessment  =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"clientAsseessment"];
        
        MESSAGE(@"arrayUserData -> arrayclientAssessment: %@",arrayclientAssessment);
        
        //Save Client's asseessments
        [self saveClientsAssessment:arrayclientAssessment];
        
        
        
        //#************  GET CLEINT'S Assessment DATA and SAVE IN LOCAL DB
        NSArray *arrayclientAssessmentData  =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"assessmentFullData"];
        
        MESSAGE(@"arrayUserData -> arrayclientAssessmentData: %@",arrayclientAssessmentData);
        
        //Save Client's asseessments
        [self saveClientsAssessmentData:arrayclientAssessmentData];
        
        
        
        
//        //#************  GET CLEINT'S TEMPLATES DATA and SAVE IN LOCAL DB
//        NSArray *arrayTemplate              =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"templates"];
//        
//        MESSAGE(@"arrayUserData -> templates: %@",arrayTemplate);
//        
//        //Save Client's asseessments
//        [self saveClientsTemplates:arrayTemplate];
        
        
        
//        //#************  GET CLEINT'S TEMPLATE's EXERCISE DATA and SAVE IN LOCAL DB
//        NSArray *arrayTemplateExercise      =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"exerciseTemplates"];
//        
//        MESSAGE(@"arrayUserData -> arrayTemplateExercise: %@",arrayTemplateExercise);
//        
//        //Save Client's asseessments
//        [self saveClientsTemplatesExercise:arrayTemplateExercise];
        
        
        
//        //#************  GET CLEINT'S TEMPLATE's PROGRAM DATA and SAVE IN LOCAL DB
//        NSArray *arrayTemplatePrograms      =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"templatePrograms"];
//        
//        MESSAGE(@"arrayUserData -> arrayTemplatePrograms: %@",arrayTemplatePrograms);
//        
//        //Save Client's asseessments
//        [self saveClientsTemplatesProgram:arrayTemplatePrograms];
        
        
        
        
//        
//        //#************  GET CLEINT'S TEMPLATE's Workouts DATA and SAVE IN LOCAL DB
//        NSArray *arrayTemplateWorkouts      =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"workoutsTemplates"];
//        
//        MESSAGE(@"arrayUserData -> arrayTemplateWorkouts: %@",arrayTemplateWorkouts);
//        
//        //Save Client's asseessments
//        [self saveClientsTemplatesWorkouts:arrayTemplateWorkouts];
        
      
        
//        
//        //Get Dictionary
//        NSDictionary   *dictProfileData         =   [commonUtility dictionaryByReplacingNullsWithStrings:dictTrainerProfile];
//        
//        if([dictProfileData objectForKey:kisFullVersion] && [[dictProfileData objectForKey:kisFullVersion] isEqualToString:@"YES"]){
//            
//            [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            
//        }
//
        
        //Invoke method for move to Home view
        [self moveToParQScreen:objDictClient];
        
        
        //Invoke method for update the client status Migrated YES
        [[DataManager initDB] updateClientInfoStatus:objDictClient ];
        
        
        
        //Hide the indicator
        [TheAppController hideHUDAfterDelay:0];
        
    }else{//If Server respose a Error
        
        //Check
        if([responseDataDictionary objectForKey:METADATA]){
            
            //Get Dictionary
            NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
            
            //Show Error Alert
            [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
            
            //Hide Indicator
            [TheAppController hideHUDAfterDelay:0];
            
            
            //For Unauthorized user --->
            if( [dictError objectForKey:LIST_KEY]){
                
                NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                
                if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                    
                    //Show Error Alert
                    [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                    
                    
                    //Move to dashbord for login
                    [commonUtility  logOut];
                }
                
            }
            
            //<-----
        }
    }
    
    
}



//Save client's Answers in local DB
-(void)saveClientsAnswers:(NSArray *)arrayclientAnswers{
    
    START_METHOD
    
    //If array has data
    if (arrayclientAnswers && arrayclientAnswers.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientAnswers.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictAnswers = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientAnswers objectAtIndex:i]]mutableCopy];
            
            [dictAnswers setValue:[dictAnswers objectForKey:@"sSheetName"] forKey:ksheetName];
            MESSAGE(@"cleints dictAnswers: %@",dictAnswers);
            
            //Invoke method for insert programs
            [[DataManager initDB] insertAnswerFor:dictAnswers];
            
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsProgram:(NSArray *)arrayclientsPrograms{
    
    START_METHOD
    
    //If array has data
    if (arrayclientsPrograms && arrayclientsPrograms.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsPrograms.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictProgram = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsPrograms objectAtIndex:i]]mutableCopy];
            
            [dictProgram setValue:[dictProgram objectForKey:@"sSheetName"] forKey:ksheetName];
            MESSAGE(@"cleints dictProgram: %@",dictProgram);
            
            //Invoke method for insert programs
            [[DataManager initDB] insertProgram:dictProgram];
            
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsExercise:(NSArray *)arrayclientsExercise{
    
    START_METHOD
    
    //If array has data
    if (arrayclientsExercise && arrayclientsExercise.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsExercise.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictExercise = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsExercise objectAtIndex:i]]mutableCopy];
            
            
            [dictExercise setValue:[dictExercise objectForKey:@"sDate"] forKey:kEXDate];
            [dictExercise setValue:[dictExercise objectForKey:@"program_exercise_id"] forKey:kBlockDataId];
            
            MESSAGE(@"cleints dictExercise: %@",dictExercise);
            
            //Invoke method for insert exercise
            NSInteger success = [[DataManager initDB] insertProgramData:dictExercise forClient:[dictExercise objectForKey:kClientId]];
            
        }
        
    }
}

//Save client's workouts and notes in local DB
-(void)saveClientsWorkoutsAndNotes:(NSArray *)arrayclientsWorkoutAndNotes{
    
    START_METHOD
    //If array has data
    if (arrayclientsWorkoutAndNotes && arrayclientsWorkoutAndNotes.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsWorkoutAndNotes.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictWorkoutAndNotes = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsWorkoutAndNotes objectAtIndex:i]]mutableCopy];
            
            MESSAGE(@"cleints dictWorkoutAndNotes: %@",dictWorkoutAndNotes);
            
            [[DataManager initDB]insertSheetBlocksForSheet:[dictWorkoutAndNotes objectForKey:kSheetId] date:[dictWorkoutAndNotes objectForKey:@"dBlockdate"] blockNo:[dictWorkoutAndNotes objectForKey:kBlockNo] clientId:[dictWorkoutAndNotes objectForKey:kClientId] blockTitle:[dictWorkoutAndNotes objectForKey:kBlockTitle] blockNotes:[dictWorkoutAndNotes objectForKey:kBlockNotes] andBlockId:[dictWorkoutAndNotes objectForKey:kBlockId]];
            //sunil-> date: 02 sept 2016
            
        }
        
    }
}


//Save client's asseessments and notes in local DB
-(void)saveClientsAssessment:(NSArray *)arrayclientsAssessments{
    
    START_METHOD
    
    MESSAGE(@"arrayclientsAssessments->00: %@",arrayclientsAssessments);
    //If array has data
    if (arrayclientsAssessments && arrayclientsAssessments.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsAssessments.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictAssessment = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsAssessments objectAtIndex:i]]mutableCopy];
            
            
            MESSAGE(@"cleints dictAssessment: %@",dictAssessment);
            [[DataManager initDB] insertNewAssessmentSheetCleintID:[dictAssessment objectForKey:kClientId] sheetName:[dictAssessment objectForKey:ksheetName] andSheetID:[dictAssessment objectForKey:kSheetId]];
            
            //
            //            //Invoke methdo for get Assessment Image
            //            [self getAssessmentImage:[dictAssessment objectForKey:@"assessmentImage"] andClientId:[dictAssessment objectForKey:kClientId]];
            //
        }
        
    }
}


//Save client's asseessments's data and notes in local DB
-(void)saveClientsAssessmentData:(NSArray *)arrayclientsAssessmentsData{
    
    START_METHOD
    //If array has data
    if (arrayclientsAssessmentsData && arrayclientsAssessmentsData.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsAssessmentsData.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictAssessmentData = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsAssessmentsData objectAtIndex:i]]mutableCopy];
            
            //Set Values
            
            NSString *strGender =   [dictAssessmentData objectForKey:kgender];
            [[NSUserDefaults standardUserDefaults] setObject:strGender forKey:kgender];
            //            if([strGender isEqualToString:@"M"]){
            
            [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Male_Chest" ] forKey:kThree_Data1];
            [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Male_Abdomen" ] forKey:kThree_Data2];
            [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Male_Thigh" ] forKey:kThree_Data3];
            
            //            }else{
            //
            //                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Female_Triceps" ] forKey:kThree_Data1];
            //                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Female_Suprailiac" ] forKey:kThree_Data2];
            //                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Female_Thigh" ] forKey:kThree_Data3];
            //
            //
            //
            //            }
            
            MESSAGE(@"cleints dictWorkoutAndNotes: %@",dictAssessmentData);
            
            [[DataManager initDB] updateAssessmentSheetFor:dictAssessmentData];
            
            
            
            //Invoke methdo for get Assessment Image
            [self getAssessmentImage:[dictAssessmentData objectForKey:@"assessmentImage"] andClientId:[dictAssessmentData objectForKey:kClientId]];
            
            
            
        }
        
    }
}


//TODO: GEt client Profile image  from server
-(void)getAssessmentImage:(NSArray *)arryaAssessmentImages andClientId:(NSString *)strClientId{
    START_METHOD
    
    for (int i =0; i<arryaAssessmentImages.count; i++) {
        
        [TheAppController showHUDonView:nil];
        
        NSDictionary *dictAssessment    =   [[arryaAssessmentImages objectAtIndex:i] mutableCopy];
        [dictAssessment setValue:strClientId forKey:kClientId];
        
        
        MESSAGE(@"dictAssessment---> asimage: %@",dictAssessment);
        
        //Invoke Method for Update the Tilte of image
        NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:strClientId,kClientId,
                                   [dictAssessment objectForKey:@"assessment_id"],ksheetID,
                                   [dictAssessment objectForKey:@"type_id"],khasImage,
                                   [NSString stringWithFormat:@"%d",(i+1)],kImageID,
                                   [dictAssessment objectForKey:@"image_name"],kImageName,nil];
        
        [[DataManager initDB] updateAssessmentImageTitlefor:dictInfo];
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictAssessment objectForKey:@"url_name"]]];
        
        MESSAGE(@"assessment url image: %@",url);
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                
                MESSAGE(@"assessment url image: success image: %@",image);
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        MESSAGE(@"assdispatch_get_main_queue");
                        
                        
                        //Invoke method for save in local
                        [self saveAssessmentImage:dictAssessment andImage:image];
                        
                        
                    });
                }
            }
        }];
        [task resume];
    }
    
    END_METHOD
}

-(void)saveAssessmentImage:(NSDictionary *)objDict andImage:(UIImage *) image{
    
    START_METHOD
    //
    //    "assessment_image_id": "111",
    //    "assessment_id": "788",
    //    "type_id": "2",
    //    "url_name": "http://52.26.135.139/assets/images/1320335643090171787.jpg",
    //    "image_name": "text"
    //
    //
    
    MESSAGE(@"arrya assessment images: %@ and image: %@",objDict , image);
    
    
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%@",[objDict objectForKey:kClientId],[objDict objectForKey:@"assessment_id"]];
    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder]];
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [FileUtility createFileInFolder:imageFolder withName:[objDict objectForKey:@"type_id"] withData:imageData];
    
    [TheAppController hideHUDAfterDelay:0.0];
    END_METHOD
}



-(void )moveToParQScreen:(NSMutableDictionary *)objDictClient{
    START_METHOD
    
    MESSAGE(@"moveToParQScreen objDictClient--> %@",objDictClient);
    
    ClientViewController * clientDetailsViewOBJ = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientView"];
    clientDetailsViewOBJ.strClientId = [objDictClient objectForKey:kClientId];
    clientDetailsViewOBJ.strClientName = [objDictClient objectForKey:@"clientName"];
    clientDetailsViewOBJ.isFromClientTable = TRUE;
    clientDetailsViewOBJ.selectedScreen = kisParq;
    [self.navigationController pushViewController:clientDetailsViewOBJ animated:YES];
    
    END_METHOD
}



//TODO: KEEP The copy//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Create a copy for back up file to  use in future
- (void) copyDatabaseIfNeededForBackup{
    START_METHOD
        NSLog(@"viewDidLoad--> dashboard->copyDatabaseIfNeededForBackup");
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSError * error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dbPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"TrainerClipboardDatabaseBackUp.sqlite"];

    
    
    NSLog(@"dbPath: copyDatabaseIfNeededForBackup: %@",dbPath);
    
    
    BOOL ifNewVersion = [commonUtility isUpdatedVersionApp];
    if(!ifNewVersion) {
        
        
        BOOL success = [fileManager fileExistsAtPath:dbPath];
        
        if (success){
            success = [fileManager removeItemAtPath:dbPath error:&error];
        }
    
        
        NSString * defaultDBPath = [self getDBPath];
        
        MESSAGE(@"main file  old: %@",defaultDBPath);

        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
}


//TODO:SAVE IN LOCAL





@end
