//
//  SignatureVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "SignatureVC.h"

@interface SignatureVC (){
    __weak IBOutlet UIButton *btnDone;
}

@end

@implementation SignatureVC

@synthesize signImage;
static BOOL mouseSwiped = NO;
CGPoint lastPoint;
int mouseMoved = 0;

//-----------------------------------------------------------------------

- (IBAction)btnDoneClicked:(id)sender {
    
    START_METHOD
    [self.signatureDelegate DoneImage:self.drawImage.image];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark View touches Methods

//---------------------------------------------------------------------------------------------------------------

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    START_METHOD
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    
    if ([touch tapCount] == 2) {
        self.drawImage.image = nil;
        return;
    }
    
    lastPoint = [touch locationInView:self.view];
    lastPoint.y -= 20;
    
}

//-----------------------------------------------------------------------------------------------------------------------------

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    START_METHOD
    mouseSwiped = YES;
    
    UITouch *touch          = [touches anyObject];
    CGPoint currentPoint    = [touch locationInView:self.view];
    currentPoint.y          -= 20;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [self.drawImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 12.0);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    lastPoint = currentPoint;
    
}

//-----------------------------------------------------------------------------------------------------------------------------

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    START_METHOD
    
    UITouch *touch = [touches anyObject];
    
    if ([touch tapCount] == 2) {
        self.drawImage.image = nil;
        return;
    }
    if(!mouseSwiped) {
        UIGraphicsBeginImageContext(self.view.frame.size);
        [self.drawImage.image drawInRect:CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 12.0);  // 15.0
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        self.drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    
    START_METHOD
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _drawImage = [[UIImageView alloc] initWithImage:self.signImage];
    self.drawImage.frame = CGRectMake(0, 49,550, 502);
    self.drawImage.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.drawImage];
    mouseMoved = 0;
    
    [btnDone setTitle:NSLocalizedString(btnDone.titleLabel.text, nil) forState:0];
    
    
    btnDone.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnDone.layer.cornerRadius = 4.0;
    btnDone.backgroundColor = kAppTintColor;

    
    self.navigationController.navigationBarHidden = NO;
    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(cancelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setTag:201];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    


    UIBarButtonItem *rightBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UILabel *notelbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 350, 48)];
    [notelbl setFont:[UIFont fontWithName:@"TradeGothic" size:18]];
    [notelbl setTextColor:kAppTintColor];
    [notelbl setBackgroundColor:[UIColor clearColor]];
    [notelbl setTextAlignment:NSTextAlignmentCenter];
    [notelbl setText:NSLocalizedString(@"Note : Double tap to erase", nil)];
    
    UIBarButtonItem *lblbtn = [[UIBarButtonItem alloc]initWithCustomView:notelbl];
    
    NSArray * toolItems = [NSArray arrayWithObjects:lblbtn,space,rightBtn, nil];
    
    UIToolbar *signToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,550, 48)];
    [signToolBar setBackgroundColor:[UIColor grayColor]];
    signToolBar.items = toolItems;
    
    [self.view addSubview:signToolBar];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)doneBtnClicked:(id)sender {
    
    START_METHOD
    [self.signatureDelegate DoneImage:self.drawImage.image];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
    END_METHOD
}

//---------------------------------------------------------------------------------------------------------------

-(void)cancelBtnClicked:(id)sender {
 
    START_METHOD
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}


//---------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    
    START_METHOD
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
