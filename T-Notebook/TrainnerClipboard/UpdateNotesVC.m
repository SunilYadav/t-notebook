//
//  UpdateNotesVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "UpdateNotesVC.h"

@interface UpdateNotesVC () <UITextViewDelegate>{
    
    __weak IBOutlet UIButton *btnDone;
}

@property (strong, nonatomic) IBOutlet UITextView *notesTextview;

@end

@implementation UpdateNotesVC

//-----------------------------------------------------------------------

#pragma mark - Action methods

//-----------------------------------------------------------------------

- (IBAction)btnDoneClicked:(id)sender {
    /*
    NSString *strNotes =  self.notesTextview.text;
    if (allTrim(strNotes) == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text.", nil),kAppName);
        return;
    }
    [self.delegate updatedNote:self.notesTextview.text];
    [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
     */
    
    //Sunil ->
    NSString *updateString =  self.notesTextview.text;
    
    //Sunil Check condition
    updateString= [updateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(updateString.length<1){
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text.", nil),kAppName);
    }else{
        
        [self.delegate updatedNote:self.notesTextview.text];
        
        [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
    }
    //<--
}


#pragma mark - ViewLifeCycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect rectOfMainScreen = [[UIScreen mainScreen] bounds];
    CGFloat xPos = 0.0, yPos = 0.0;
    xPos = (rectOfMainScreen.size.height / 2);
    yPos = (rectOfMainScreen.size.width / 2);
    
    if (iOS_7) {
        
        self.navigationController.view.superview.frame = CGRectMake(0, 0, 500, 365);
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            
            self.navigationController.view.superview.center = CGPointMake(xPos, yPos);
        }
        else{
            self.navigationController.view.superview.center = CGPointMake(yPos, xPos);
        }
    }
    
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated{
START_METHOD
    [super viewWillAppear:animated];
    [self addNavBarButtons];
    
    if (self.isFromHelpAction) {
        for (UIView *subView in self.view.subviews) {
            [subView removeFromSuperview];
        }
        UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
        lblTitle.text = NSLocalizedString(@"Body Fat Norms", nil);
        lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
        lblTitle.textColor = kAppTintColor;
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.adjustsFontSizeToFitWidth=YES;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        self.navigationItem.titleView=lblTitle;
        
        
        UIWebView *webView ;
        if(iOS_8) {
            webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 500, 400)];
        } else {
              webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 500, 400)];
        }
        
        webView.delegate = (id)self;
        webView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:webView];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"BodyChart_Full" ofType:@"html"];
        DebugLOG(path);
        NSURL *baseURL = [NSURL fileURLWithPath:path];
        [webView loadRequest:[NSURLRequest requestWithURL:baseURL]];
        
    } else {
        
        btnDone.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
        btnDone.layer.cornerRadius = 4.0;
        btnDone.backgroundColor = kAppTintColor;
        
        [self.notesTextview setDelegate:self];
        [self.notesTextview setText:self.notes];
        self.notesTextview.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.notesTextview.layer.borderWidth = 1.0;
        self.notesTextview.layer.cornerRadius = 8.0;
        
        UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
        lblTitle.text = NSLocalizedString(@"Notes", nil);
        lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
        lblTitle.textColor = kAppTintColor;
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.adjustsFontSizeToFitWidth=YES;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        
        self.navigationItem.titleView=lblTitle;
        
    }
    [btnDone setTitle:NSLocalizedString(btnDone.titleLabel.text, nil) forState:0];

    END_METHOD
}
-(void)openKeyboard{
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    [self performSelector:@selector(openKeyboard) withObject:nil afterDelay:0.3];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


//-----------------------------------------------------------------------

- (IBAction) btnBackClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------------

-(void)NavBarBtnTap :(UIBarButtonItem *)sender {
    if ([sender tag] == 201) {
        // back button
        [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
    } else if ([sender tag] == 202) {
        NSString *strNotes =  self.notesTextview.text;
        if (allTrim(strNotes) == 0) {
            DisplayAlertWithTitle(NSLocalizedString(@"Please enter text.", nil),kAppName);
            return;
        }
        // done button
        [self.delegate updatedNote:self.notesTextview.text];
        [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView{
   
}
//-----------------------------------------------------------------------

@end
