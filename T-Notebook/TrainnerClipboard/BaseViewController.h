//
//  BaseViewController.h
//  
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface BaseViewController : UIViewController <UINavigationControllerDelegate> {
	AppDelegate		* appDelegate;
}

-(BOOL)prefersStatusBarHidden;
-(void)setNavigationBarButton;
-(void)addNavBarButtons;



@end
