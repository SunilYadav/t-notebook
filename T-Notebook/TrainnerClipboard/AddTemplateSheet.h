//
//  SheetNameController.h
//  T-Notebook
//
//  Created by WLI on 23/06/12.
//  Copyright (c) 2012 WLI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SheetNameDelegate
- (void) callSetArrayMethod:(NSInteger)sheetid :(NSInteger)intTemplateID :(NSInteger)intSheetCount :(id)sender;
-(void)didCancelSheetEntry;

@end

@interface AddTemplateSheet : UIViewController<UITextFieldDelegate>{
    
    id<SheetNameDelegate>		__unsafe_unretained delegateSheetName;
  
    
}
@property (nonatomic, unsafe_unretained) id<SheetNameDelegate>	delegateSheetName;
@property (nonatomic , strong) NSString    * strTemplateName;
@property (nonatomic)NSInteger   intsheetcount;
@property (nonatomic)NSInteger   intTemplateid;
@property (nonatomic)NSInteger   intCurrentSheetId;
@property (weak , nonatomic)  IBOutlet   UIButton *btnAdd;

@end
