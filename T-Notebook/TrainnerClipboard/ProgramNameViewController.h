//
//  ProgramNameViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AddNewProgramSheetDelegate<NSObject>
-(void) addNewProgram:(NSString *)ProgramName : (NSString *)ProgramPurpose : (NSString *)workoutName;

@optional
-(void)cancelProgramAndGoToParq;
@end

@interface ProgramNameViewController : BaseViewController
@property (nonatomic, unsafe_unretained) id<AddNewProgramSheetDelegate>			delegate;
@property (nonatomic, strong) NSString			*strSheetName;
@property (nonatomic) BOOL			isSetFrame;

@end
