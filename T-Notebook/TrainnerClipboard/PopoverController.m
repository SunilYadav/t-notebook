
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "HomeCustomCell.h"
#import "PopoverController.h"

@interface PopoverController () <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic)  NSArray *programArray;
@property (strong, nonatomic)  NSArray *workOutArray;
@property (strong, nonatomic)  NSArray *arrayBlockDates,*programMainArray;
@end

@implementation PopoverController

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated
{START_METHOD
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
    [self.tableview setDataSource:self];
    [self.tableview setDelegate:self];
    [self.tableview reloadData];
    
}

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//-----------------------------------------------------------------------

#pragma mark - TableView mMethods

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

//-----------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *currentSheetID = [[self.arrayProgramSheet objectAtIndex:section] valueForKey:@"sheetID"] ;
    
    NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientID SheetId:currentSheetID]];
    self.workOutArray = [NSArray arrayWithArray:arrayRowCount];
    return arrayRowCount.count;
}

//-----------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.arrayProgramSheet.count;
}
//-----------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
        NSString * CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];
    
    HomeCustomCell * cell1 = (HomeCustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell1 == nil) {
        cell1 = [[HomeCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell1.lblCNameWorkout.hidden        = NO;
    cell1.lblCNameWorkoutDate.hidden    = NO;
    
    cell1.lblCName.hidden = YES;
    cell1.ImgTable.hidden = YES;
    cell1.mySwitch.hidden = YES;

    
    NSString *currentSheetID = [[self.arrayProgramSheet objectAtIndex:indexPath.section] valueForKey:@"sheetID"] ;
    NSArray *arrayRowCount = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForClient:self.strClientID SheetId:currentSheetID]];
    
    if (arrayRowCount.count) {
        
        cell1.lblCNameWorkout.text = [[arrayRowCount objectAtIndex:indexPath.row] valueForKey:@"sBlockTitle"];
        cell1.lblCNameWorkoutDate.text = [[arrayRowCount objectAtIndex:indexPath.row] valueForKey:@"dBlockdate"];
        
    }
    
    return cell1;
}

//-----------------------------------------------------------------------

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate test:indexPath.section+1 :indexPath.row+1];
}
//-----------------------------------------------------------------------

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 30)];
    
    [view setBackgroundColor:[UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 3, 340, 20)];
    [label setText:[[self.arrayProgramSheet objectAtIndex:section] valueForKey:@"sheetName"]];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    [view addSubview:label];
    return view;
}

//-----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc]initWithFrame:CGRectZero];
}
@end
