//
//  FMDatabaseAdditions.h
//  fmkit
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>
@interface FMDatabase (FMDatabaseAdditions)


- (int) intForQuery:(NSString*)objs, ...;
- (long) longForQuery:(NSString*)objs, ...; 
- (BOOL) boolForQuery:(NSString*)objs, ...;
- (double) doubleForQuery:(NSString*)objs, ...;
- (NSData*) dataForQuery:(NSString*)objs, ...;
- (NSString*) stringForQuery:(NSString*)objs, ...; 

- (BOOL)tableExists:(NSString*)tableName;
- (FMResultSet*)getSchema;
- (FMResultSet*)getTableSchema:(NSString*)tableName;
- (BOOL)columnExists:(NSString*)tableName columnName:(NSString*)columnName;
@end
