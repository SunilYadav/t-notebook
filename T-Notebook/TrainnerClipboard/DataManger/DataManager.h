//
//  DataManager.h
//  MyRadio
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "FMDatabase.h"
#import "AppDelegate.h"

@interface DataManager : NSObject {
	FMDatabase			* db;
	AppDelegate			* appDelegate;
    
    BOOL shouldUpdateBlockData;
    BOOL shouldUpdateBlocks;
    BOOL shouldUpdateTemplate;
}


+ (DataManager *)initDB;

- (int) checkForTableInDatabaseForClientID:(NSString *)clientID inview: (int) inView;

//===================Parq VC Methods===================//

//Insert
- (NSString *) insertNewClient:(NSMutableDictionary *) dictInfo;
- (NSString *) createClientTable;
- (int) insertAnswerFor:(NSDictionary *)dictInfo;

//Update
- (NSString *) updateClientDetail:(NSMutableDictionary *) dictInfo;
- (NSInteger) updateLastViewedScreen : (NSInteger) screen forClient: (NSString *) clientID;
- (int) upDateLastReviewedDate : (NSString *) clientID;
- (int) updateAnswerFor:(NSDictionary *)dictInfo;
- (int) upDateClientPQRData : (NSDictionary *) dictInfo;
- (int) updateParqField :(NSDictionary *) dictInfo;

//Delete
- (int) deleteClientDetail : (NSString *) clientID;


//Select
- (NSMutableArray *) getClientsList :(NSString *) scope;
- (NSDictionary	*) getClientsDetail :(NSString *) clientID;
- (NSString *) getClientsGender :(NSString *) clientID;
- (NSString *) getClientsAge :(NSString *) clientID;
- (int) getLastViewdScreenForClient:(NSString *)clientID;
- (NSArray *) selectQuestionCategory;
- (NSArray *) selectQuestionForCategory:(NSString *) categoryID;
- (int) checkAnswerForClient:(NSString *)clientId;
- (NSDictionary *) answerForQuestion:(NSString *) questionId :(NSString *)clientID;

//===================Assessment VC Methods===================//

//Insert
- (NSInteger) insertNewAssessmentSheetFor :(NSString *) clientID sheetName:(NSString *) sheetName;
- (int) insertAssessmentSheetDataFor :(NSString *) clientID sheetID:(int) sheetID;
- (int) insertAssessmentSheetImagesDataFor :(NSString *) clientID sheetID:(int) sheetID;
//Update
- (NSInteger) updateAssessmentSheetNameFor:(NSString *) clientID ForSheetID:(NSInteger) sheetID toSheetName:(NSString *)sheetName;
- (int) updateAssessmentSheetFor : (NSDictionary *) dictInfo;
- (int) updateField :(NSDictionary *) dictInfo; 
- (int) updateAssessmentImageTitlefor :(NSDictionary *) dictInfo;

//Select
- (int) getCountofAssessmentSheetForClient:(NSString *)clientID;
- (NSMutableArray *) getAllAssessmentSheetsFor:(NSString *)clientID;
- (NSMutableArray *) getAssessmentSheetsDataForSheet:(NSString *)sheetID ofClientID:(NSString *)clientID;
- (NSArray *) getAllAssessmentImagesForSheet:(NSString *) sheetID ofClientID:(NSString *)clientID;

//Charting Methods
- (NSArray *) getClientsWeightHeight :(NSString *) clientID;
- (NSArray	*) getClientsGeneralBodyFatPercentage :(NSString *) clientID;
- (NSArray *) getClientsBMIResult :(NSString *) clientID;
- (NSArray *) getClientsBodyFatPercentage :(NSString *) clientID;
- (NSArray *) getClientscircumferenceMeasurement :(NSString *) clientID;

//Delete
-(NSInteger)deleteAssessmentSheet:(NSInteger)sheetId forClientid:(NSString *)clientId;
-(NSInteger)deleteAssessmentSheetDataForSheetId:(NSInteger)sheetId forClientid:(NSString*)clientId;
-(NSInteger)deleteAssessmentImagesforSheetId:(NSInteger)sheetId forClient:(NSString*)clientId;

//Check and alterTable if id not update

-(void)addNewFieldsInAssessmentTableForClient:(NSString *) clientId;
-(void)addNewFieldToClientTableIfDoesNotExist;
-(NSString *)getLastEditedScreenForClient:(NSString *) clientID;
- (int)updateLastEditedScreen:(NSString*)editedScreen forClientId:(NSString *)clientID;

//===================Program VC Methods===================//

//Insert
- (NSInteger) insertSheetBlocksForSheet:(NSString *)sheetId date:(NSString *)date blockNo:(NSString *) bNo clientId:(NSString *) cId blockTitle:(NSString *) title blockNotes:(NSString *) notes andBlockId:(NSString *)strBlockId;

- (int) addNewSheet:(NSString *) sheetName ForClient:(NSString *)cId ProgramPurpose:(NSString *)programPurpose withWorkoutName:(NSString *)workoutName ForBlockNo:(NSString *)blockNO;
- (NSInteger) insertProgramData:(NSDictionary *)dic forClient:(NSString *) cId;
- (NSInteger) insertProgramData:(NSDictionary *)dic forClient:(NSString *) cId :(NSString *)sheetId;
- (int) insertSheetBlocksForSheet:(NSString *)sheetId :(NSString *)clientId :(NSDictionary *)dataDict;
- (BOOL) insertProgramDataWith_Block:(NSDictionary *)dic forClient:(NSString *) cId :(NSString *)sheetId :(NSString *)blockID :(int)blockNo;
- (int) addNewSheet:(NSString *) sheetName ForClient:(NSString *)cId;
- (int) insertSheetBlocksForSheet:(NSString *)sheetId date:(NSString *)date blockNo:(NSString *) bNo clientId:(NSString *) cId blockTitle:(NSString *) title;
//Update
- (NSInteger) updateSheetName:(NSString *) sheetname forClient:(NSString *)cID sheetId:(NSString *) sId;
- (int) updateProgramData:(NSDictionary *)dic forClient:(NSString *) cId;
- (NSInteger) updateDate:(NSString *)date blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId ;
- (int) updateSheetsStatusForSheetID:(NSString *)sheetId to:(int)status forClientID: (NSString *) clientID;
- (int) updateProgramData:(NSDictionary *)dic forClient:(NSString *)cId :(NSString *)sheetId;

//Select
- (NSArray *) selectProgramBlocksForClient:(NSString *)clientID :(NSString *)sheetId;
- (NSArray *) selectProgramDataForClient:(NSString *)clientID :(NSString *)sheetId;
- (NSArray *) selectProgramDataForClient:(NSString *)cId sheet:(NSString *)sheet blockNo:(NSString *)bNo;
- (NSArray *) getBlockDataForClient:(NSString *)cId SheetId:(NSString *)sheetId;
- (int) getCountofProgramSheetForClient:(NSString *)cId;
- (NSString *) getsheetId:(NSString *) sheetName forClient:(NSString *)cId;
- (NSArray *) getProgramSheetList:(NSString *)cId;
- (NSDictionary *) getEmailAddressesofClient : (NSString *) clientID;
- (int) getSheetsStatusForSheetID:(NSString *)sheetId forClientID: (NSString *) clientID;
-(int)getBlockIdForClient:(NSString *)sheetId :(NSString *)blockNo :(NSString *)clientID;
//- (NSArray *) getBlocksForClient:(NSString *)cId SheetId:(NSString *)sheetId;
//- (NSArray *) getBlockDataForClient:(NSString *)cId SheetId:(NSString *)sheetId;
- (NSArray *) selectProgramDataForClient:(NSString *)cId sheet:(NSString *)sheet;
-(NSString*)getBlockNumberForNewWorkoutForClient:(NSString*)clientId forProgramSheet:(NSString*)strSheetId;

//Delete
- (NSInteger) deleteBlockData : (NSString *)clientID :(NSString *)sheetID;
- (int) deleteBlocks : (NSString *)clientID :(NSString *)sheetID;
- (int) deleteSheet:(NSString *)sheetId forClient:(NSString *)clientId;
 -(int)deleteWorkoutForSheet:(NSString*)sheetId forClient:(NSString *)clientId andWorkOutBlockId:(NSString*)blockId;
-(int)deleteWorkOutBlockDataForSheet:(NSString*)sheetId forClient:(NSString *)clientId andWorkoutBlockId:(NSString*) blockId;


//// Block Copy Paste Methods

- (NSArray *) getBlock:(NSString *)cId SheetId:(NSString *)sheetId :(NSString *)blockID;
- (NSArray *) selectProgramDataForClient:(NSString *)cId sheet:(NSString *)sheet :(NSString *)blockID;
- (int) deleteBlockData : (NSString *)clientID :(NSString *)sheetID :(NSString *)blockNO;
- (int) insertProgramData:(NSDictionary *)dic forClient:(NSString *)cId :(NSString *)sheetId :(NSString *)blockID;
- (int) deleteBlocks : (NSString *)clientID :(NSString *)sheetID :(NSString *)blockID;
- (int) insertSheetBlocksForSheet:(NSString *)sheetId :(NSString *)clientId :(NSDictionary *)dataDict :(NSString *)nBlockID;



//-----------------------Notes View Methods ------------------------------------------------------------------
-(int) insertNotes:(NSDictionary *)dic forClient:(NSString *)cid;
-(int) updateNotes:(NSDictionary *)dic forClient:(NSString *)cid;
-(NSDictionary *) selectNotes:(NSString *)cid;
- (NSString *) createClientTablesIfNotExist:(NSString *)cid;
- (NSMutableArray *) getClientsID ;


-(NSArray*)getNotesForClient:(NSString*)clientId andSheets:(NSString *)sheetId;
-(NSArray *)getTotalSheetsForClient:(NSString *)clientId;

//===================Template Methods===================//

- (int)getLastTemplateId;
- (int)insertTemplate_ProgramedData:(NSDictionary *)dic;
- (int)updateTemplateProgrammedData:(NSDictionary *)dic;
- (int)deleteTemplate_TemplateSheet:(int)templateId;
- (NSInteger)updateBlockDate_Title:(NSInteger)intSheetID :(NSInteger)intBlockNo :(NSString *)date :(NSString *)strBlock_Title;
- (int)addTemplate:(NSDictionary *)dic;
- (int)deleteTemplateSheetData:(int)sheetId;

- (void)addSheetDetail:(NSDictionary *)dic;
- (void)insertTemplateSheet_Blocks:(NSDictionary *)dic;
- (void)deleteSheetBlock_BlockData:(int)sheetId;

- (int )updateTemplateName:(NSString *)templateName oldTemplate:(NSInteger)intTemplateID;// PRashnt Change

- (NSArray *)selectProgramDataForBlockNo:(NSInteger)intBlockno;
- (NSArray *)getAllTemplatesDetail;
- (NSArray *)getSheetsOfTemplate:(NSInteger)intTemplateID;
- (NSArray *)getBlockDataFor_TemplateSheet:(NSInteger)intSheetId;
- (NSArray *)selectProgramDataForTemplate:(int)intSheet :(int)intBlockNo;
- (NSArray *)getTemplateSheetList:(NSInteger)intTemplateID;
- (NSArray *)selectProgramDataForSheet_Template:(int)intSheet;
- (NSArray *)getBlock_Template:(NSString *)sheetId :(NSString *)blockNo;

- (NSString *)getSheetName:(int)SheetId;
- (NSString *)getnBlockID_Template:(NSString *)sheetID :(NSString *)BlockNo;



- (NSString *) escapeString:(NSString *)string;
- (NSString *)escapeHtmlCharaters:(NSString *)str;

//===================BlockHavingData====================//
- (BOOL) isBlockHavingData:(NSString *)cId sheet:(NSString *)sheet blockNo:(NSString *)bNo;

//===================for insert Notes====================//

- (NSInteger) updateNotes:(NSString *)Notes blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId;

- (NSInteger) updateNotes:(NSString *)Notes blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId andexistingNotes: (NSString *)str ;

-(void)addNewFieldsToTablesIfnotAddedForClientId:(NSString*)clientID;
- (NSInteger) updateBlockName:(NSString *) blockTitle forClient:(NSString *)cID sheetId:(NSString *) sId blockId:(NSString *)blockID;
- (int) updateBlockNameForWorkout:(NSString *) blockTitle forClient:(NSString *)cID sheetId:(NSString *) sId blockId:(NSString *)blockID;

- (int) deleteBlockData : (NSString *)clientID :(NSString *)sheetID :(NSString *)blockNO :(NSString *)rowNO;
- (NSArray *) getBlockDataForClient:(NSString *)cId SheetId:(NSString *)sheetId :(NSString *)blockno;
- (int) updateDate1:(NSString *)date blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId sheetID:(NSString *)sheedID;
- (int) addNewSheet1:(NSString *) sheetName ForClient:(NSString *)cId;


-(int) updateTemplateSheetName: (NSString *)sheetName forSheetID: (NSInteger)sheetId;
-(void)deleteLineForLineID:(NSInteger)lineID;

//Sunil Addeed method for get perticular workout anme
-(NSArray*)getWorkoutNameForNotes:(NSString*)clientId andBlockNo:(NSString *)strBlockNo  andSheetId:(NSString *)strSheetId;


//SUNIL for get data on view aall notes for shoe detail option
- (NSArray *) getNotesDataForClient:(NSString *)cId SheetId:(NSString *)sheetId ;

//TODO: MIGRATION
//GEt all workouts from all template
-(NSArray *)getBlockDataForTemplates;

//Get all exercise from all template
-(NSArray *)getProgramTemplateLines;

//For All Clients Deatil
- (NSArray	*) getClientsDetail;

//For All Client's Program
- (NSArray	*)getProgramSheets:(NSString *)strClientId;

//For All Client's Notes
- (NSArray	*)getAllNotes:(NSString *)strClientId andBlockNo:(NSString *)strBlockNo;

//For All Client's Exercises
- (NSArray	*)getAllExercises:(NSString *)strClientId andProgramId:(NSString *)strProgramID;

//For All Client's Asseessment Data full sheet
- (NSArray	*)getAllAssessmentSheetFullData:(NSString *)strClientId;

//All Template Program
- (NSMutableArray *) getAllTemplatePrograms;

//For All Clients Name
- (NSArray	*) getClients;

//Method for Insert the Trainer Prifle
- (int) insertTrainerProfiles:(NSDictionary *)dictTrainerProfile ;

//Get Trainer Profile
- (NSArray	*)getTrainerProfile;

//Get Assessment
-(NSMutableArray *) getAssessmentSheetsFor:(NSString *)clientID andSheetId:(NSString *)sheetID;

//Method for get all questions
- (NSArray *) getAllQuestions;

//Method for get all answers
- (NSArray *) getAllAnswersByClientID:(NSString *)strClientId;

//Insert Program SUNIL
-(int)insertProgram:(NSDictionary *)dictProgram;

- (void) insertNewClientUpgraded:(NSMutableDictionary *) dictInfo;

//Method for get all Client's ID
- (NSArray *) getAllClientsID;

//TODO: SUNIL Save assessment sheet for upgraded user

- (NSInteger) insertNewAssessmentSheetCleintID :(NSString *) clientID sheetName:(NSString *) sheetName andSheetID:(NSString *)strSheetId;

- (NSArray	*)getAllNotesUnderProgram:(NSString *)strClientId andSheetId:(NSString *)strSheetId;

//TODO: get All Asseessment Images
- (NSArray *) getAllAssessmentImages:(NSString *)strClientId;

//TODO: Active inavtive clients
- (NSMutableArray *) getClientsListForShow :(NSString *) scope;

//TODO: Update the clients Staus
- (int ) updateClientStatus:(NSMutableDictionary *) dictInfo;

//For Tag of show detail button
-(NSArray*)getNotesForClient:(NSString*)clientId andSheets:(NSString *)sheetId  andBlock:(NSString *)block ;

//Delete current workout for copy paste
-(int)deleteWorkoutForCopyPaste:(NSString*)sheetId forClient:(NSString *)clientId andWorkOutBlockId:(NSString*)blockId;



//TODO: For migration
-(void)addNewFieldToClientTable;

- (int ) updateClientMigrationStatus:(NSMutableDictionary *) dictInfo;

- (NSArray	*) getTotalClientsCount;
- (NSArray	*) getClientsCountWithoutMigration;

- (NSString *) getClientsInfoStaus :(NSString *) clientID ;

- (int ) updateClientInfoStatus:(NSMutableDictionary *) dictInfo;

@end

