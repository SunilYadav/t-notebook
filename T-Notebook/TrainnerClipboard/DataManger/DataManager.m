
//
//  DataManager.m
//  MyRadio
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "DataManager.h"
#import "FMDatabaseAdditions.h"


static DataManager *sharedDB;

@implementation DataManager


+ (DataManager *)initDB
{
	@synchronized(self)
	{
		if (!sharedDB)
			sharedDB = [[DataManager alloc] init];		
	}
	return sharedDB;
}


- (void) initFMDB{
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
	[db close];
	if(db == nil){
		db = [[FMDatabase alloc]initWithPath:[appDelegate getDBPath]];
	}
	[db setLogsErrors:TRUE];
}


- (int) addNewTableForQuery:(NSString *) query {
	[self  initFMDB];
	
	if([db open])
	{
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 0;
		}
	}
	return 1;
}

- (int) checkForTableInDatabaseForClientID:(NSString *)clientID inview: (int) inView {
	[self  initFMDB];
	
	if([db open])
	{
        //db
		NSString * query = @"";
		if (inView == 1) {
			query = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' and name = 'FT%@_AssessmentSheet'",clientID];
			
			FMResultSet * rs = [db executeQuery:query];
			int count = 0;
			while ([rs next]) {
				count = 1;
			}
			
			if (count == 0) {
				query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_AssessmentSheet\" (\"nSheetID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"sSheetName\" VARCHAR)",clientID];
				if ([self addNewTableForQuery:query] == 0) {
					return 0;
				}				
			}
			[self checkForTableInDatabaseForClientID:clientID inview:2];
		}
		else if (inView == 2) {
			query = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' and name = 'FT%@_AssessmentSheet_Data'",clientID];
			
			FMResultSet * rs = [db executeQuery:query];
			int count = 0;
			while ([rs next]) {
				count = 1;
			}
			
			if (count == 0) {
				query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_AssessmentSheet_Data\" (\"nDetailID\" INTEGER PRIMARY KEY  NOT NULL ,\"nSheetID\" INTEGER,\"nAssessmentText\" VARCHAR,\"fCM_RightUpperArm\" VARCHAR,\"fCM_RightForeArm\" VARCHAR,\"fCM_Chest\" VARCHAR,\"fCM_Abdominal\" VARCHAR,\"fCM_HipButtocks\" VARCHAR,\"fCM_RightThing\" VARCHAR,\"fCM_RightCalf\" VARCHAR,\"fCM_Result\" VARCHAR,\"fBMI_Pound_Weight\" VARCHAR,\"fBMI_Pound_Height\" VARCHAR,\"fBMI_Pound_Result\" VARCHAR,\"fHeight\" VARCHAR,\"fWeight\" VARCHAR,\"nRestingHeartBeat\" INTEGER,\"sBloodPressure\" VARCHAR,\"fThree_Male_Chest\" VARCHAR,\"fThree_Male_Abdomen\" VARCHAR,\"fThree_Male_Thigh\" VARCHAR,\"fThree_Female_Triceps\" VARCHAR,\"fThree_Female_Suprailiac\" VARCHAR,\"fThree_Female_Thigh\" VARCHAR,\"fFour_Abdomen\" VARCHAR,\"fFour_Suprailiac\" VARCHAR,\"fFour_Triceps\" VARCHAR,\"fFour_Thigh\" VARCHAR,\"fSeven_Chest\" VARCHAR,\"fSeven_Midaxillary\" VARCHAR,\"fSeven_Triceps\" VARCHAR,\"fSeven_subscapular\" VARCHAR,\"fSeven_Abdomen\" VARCHAR,\"fSeven_Suprailiac\" VARCHAR,\"fSeven_Thigh\" VARCHAR,\"fThree_Result\" VARCHAR, \"fFour_Result\" VARCHAR, \"fSeven_Result\" VARCHAR,\"fBMI_Metrics_Weight\" VARCHAR,\"fBMI_Metrics_Height\" VARCHAR,\"fBMI_Metrics_Result\" VARCHAR,\"dUpdated_date\" DATETIME, \"sAssessment_Goal\" VARCHAR,\"fGenereal_FatPercent\" VARCHAR,\"fExtra_Value\" VARCHAR,\"sExtra_FieldName\" VARCHAR)",clientID];
				if ([self addNewTableForQuery:query] == 0) {
					return 0;
				}				
			}
			[self checkForTableInDatabaseForClientID:clientID inview:3];
		}
		else if (inView == 3) {
			query = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' and name = 'FT%@_AssessmentSheet_ImagesData'",clientID];
			
			FMResultSet * rs = [db executeQuery:query];
			int count = 0;
			while ([rs next]) {
				count = 1;
			}
			
			if (count == 0) {
				query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_AssessmentSheet_ImagesData\" (\"nImageID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"nSheetID\" INTEGER, \"sImageLabel\" VARCHAR, \"hasImage\" INTEGER)",clientID];
				
				if ([self addNewTableForQuery:query] == 0) {
					return 0;
				}				
			}
			if ([self getCountofAssessmentSheetForClient:clientID] == 0) {
                
                
                NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];

                if([strUpgradeStatus isEqualToString:@"YES"]){
                    
                }else{
                    if ([self insertNewAssessmentSheetFor:clientID sheetName:@"Assessment-1"] == 1) {
                        return 0;
                    }
                }
                
			}					
		}
	}
	return 1;
}

//========================================ParqVS Methods Methods=================================================//

#pragma mark -
#pragma mark PARQView Insert Methods

- (NSString *) insertNewClient:(NSMutableDictionary *) dictInfo{
    [self  initFMDB];
    
    if([db open])
    {
        NSString * strName = [self escapeString:[dictInfo objectForKey:kFirstName]];
        NSString * strLastName = [self escapeString:[dictInfo objectForKey:kLastName]];
        NSString * strEmergencyContactName1 = [self escapeString:[dictInfo objectForKey:kEmergencyConName1]];
        NSString * strEmergencyContactRelationship = [self escapeString:[dictInfo objectForKey:kEmergencyRel1]];
        NSString * strEmergencyContactName2 = [self escapeString:[dictInfo objectForKey:kEmergencyConName2]];
        NSString * strEmergencyContactRelationship2 = [self escapeString:[dictInfo objectForKey:kEmergencyRel2]];
        
        NSString * age = [dictInfo objectForKey:kAge];
        NSString * bDate = [dictInfo objectForKey:kBirthDate];
        
        //BOOL   acceptedTerms = [[dictInfo objectForKey:kTermsAccepted]boolValue];
        
        NSDateFormatter * tmpdt = [[[NSDateFormatter alloc] init] autorelease];
        [tmpdt setDateFormat:@"MM/dd/yy hh:mm:ss a"];
        NSString * updatedDate = [tmpdt stringFromDate:[NSDate date]];
        // [tmpdt setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        [tmpdt setDateFormat:@"MM-dd-yyyy HH:mm:ss +0000"];
        NSString * updatedDate1 = [tmpdt stringFromDate:[NSDate date]];
        NSString * query = @" insert into FT_Client (dLastReviewedDate,dUpdatedDate,sFirstName,sLastName,dBirthDate,nAge,";
        query = [query stringByAppendingString:@"sPhone_Day,sPhone_Evening,sPhone_cell,sEmailAddress,sEmailAddress2,"];
        query = [query stringByAppendingString:@"sEmergencyContactName1,sEmergencyContact1_Phone_Work,sEmergencyContact1_Phone_Home,sEmergencyContact1_Phone_Cell,sEmergencyContact1_Relationship,"];
        query = [query stringByAppendingString:@"sEmergencyContactName2,sEmergencyContact2_Phone_Work,sEmergencyContact2_Phone_Home,sEmergencyContact2_Phone_cell,sEmergencyContact2_Relationship,sGender,"];
        query = [query stringByAppendingString:@"nLastViewedScreen,sPhysicianName,sPhysicianPhone,sHeight,sWeight) values("];
        query = [query stringByAppendingFormat:@"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',0,' ',' ',' ',' ')",updatedDate1,updatedDate,
                 strName,
                 strLastName,
                 bDate,
                 age,
                 [dictInfo objectForKey:kPday],
                 [dictInfo objectForKey:kPeve],
                 [dictInfo objectForKey:kPcell],
                 [dictInfo objectForKey:kEmail1],
                 [dictInfo objectForKey:kEmail2],
                 strEmergencyContactName1,
                 [dictInfo objectForKey:kEmergencyConWNo1],
                 [dictInfo objectForKey:kEmergencyConHNo1],
                 [dictInfo objectForKey:kEmergencyConCNo1],
                 strEmergencyContactRelationship,
                 strEmergencyContactName2,
                 [dictInfo objectForKey:kEmergencyConWNo2],
                 [dictInfo objectForKey:kEmergencyConHNo2],
                 [dictInfo objectForKey:kEmergencyConCNo2],
                 strEmergencyContactRelationship2,
                 [dictInfo objectForKey:kgender]
                 ];
        [db executeUpdate:query];
    }
    NSString * str= [db lastErrorMessage];
    if([str isEqualToString:@"not an error"]) {
        
        
        return [self createClientTable:@""];
    }
    return @"";
}
//--------------------------------------------------------------------------

- (NSString *) createClientTable:(NSString *)client_id {
    START_METHOD
	[self  initFMDB];
    
//	NSString * client_id = @"";// Comment Sunil
	if([db open])
	{
         //TODO: by  SUNIL
        
        NSString *strStatus     =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString *query ;
        
        if([strStatus  isEqualToString:@"YES"]){
            
        }else{
            
            client_id = @"";//-------------------------------->
            query = @"SELECT  max(nclientid) from ft_client";
            FMResultSet * rs = [db executeQuery:query];
            while ([rs next]) {
                client_id = [rs stringForColumn:@"max(nclientid)"];
            }
            
        }
        
	
		// Program Sheet Table
        
		query  = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_ProgramSheet\" (\"nSheetID\" INTEGER PRIMARY KEY  NOT NULL , \"sSheetName\" VARCHAR,\"sPurpose\" VARCHAR, \"nUpdateStatus\" INTEGER DEFAULT 0)",client_id];
		[db executeUpdate:query];
		
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return client_id;
		}
		
		// Program Sheet Block Table
		query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_ProgramSheet_Blocks\" (\"nBlockID\" INTEGER PRIMARY KEY  NOT NULL , \"nSheetID\" INTEGER, \"dBlockdate\" DATETIME, \"nBlockNo\" INTEGER, \"sBlockTitle\" VARCHAR,\"sBlockNotes\" VARCHAR)",client_id];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return client_id;
		}
		
		// Program Sheet BlockData Table
		query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_ProgramSheet_BlockData\" (\"nBlockDataID\" INTEGER PRIMARY KEY  NOT NULL , \"nBlockID\" INTEGER, \"nSheetID\" INTEGER, \"sProgram\" VARCHAR, \"sLBValue\" VARCHAR, \"sRepValue\" VARCHAR,\"sSetValue\" VARCHAR,\"nBlockNo\" INTEGER,\"nRowNo\" INTEGER,\"sDate\" VARCHAR, \"sColor\" INTEGER)",client_id];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return client_id;
		}
		
		query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_AssessmentSheet\" (\"nSheetID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"sSheetName\" VARCHAR)",client_id];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return client_id;
		}
		
		query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_AssessmentSheet_Data\" (\"nDetailID\" INTEGER PRIMARY KEY  NOT NULL ,\"nSheetID\" INTEGER,\"nAssessmentText\" VARCHAR,\"fCM_RightUpperArm\" VARCHAR,\"fCM_RightForeArm\" VARCHAR,\"fCM_Chest\" VARCHAR,\"fCM_Abdominal\" VARCHAR,\"fCM_HipButtocks\" VARCHAR,\"fCM_RightThing\" VARCHAR,\"fCM_RightCalf\" VARCHAR,\"fCM_Result\" VARCHAR,\"fBMI_Pound_Weight\" VARCHAR,\"fBMI_Pound_Height\" VARCHAR,\"fBMI_Pound_Result\" VARCHAR,\"fHeight\" VARCHAR,\"fWeight\" VARCHAR,\"nRestingHeartBeat\" INTEGER,\"sBloodPressure\" VARCHAR,\"fThree_Male_Chest\" VARCHAR,\"fThree_Male_Abdomen\" VARCHAR,\"fThree_Male_Thigh\" VARCHAR,\"fThree_Female_Triceps\" VARCHAR,\"fThree_Female_Suprailiac\" VARCHAR,\"fThree_Female_Thigh\" VARCHAR,\"fFour_Abdomen\" VARCHAR,\"fFour_Suprailiac\" VARCHAR,\"fFour_Triceps\" VARCHAR,\"fFour_Thigh\" VARCHAR,\"fSeven_Chest\" VARCHAR,\"fSeven_Midaxillary\" VARCHAR,\"fSeven_Triceps\" VARCHAR,\"fSeven_subscapular\" VARCHAR,\"fSeven_Abdomen\" VARCHAR,\"fSeven_Suprailiac\" VARCHAR,\"fSeven_Thigh\" VARCHAR,\"fThree_Result\" VARCHAR, \"fFour_Result\" VARCHAR, \"fSeven_Result\" VARCHAR,\"fBMI_Metrics_Weight\" VARCHAR,\"fBMI_Metrics_Height\" VARCHAR,\"fBMI_Metrics_Result\" VARCHAR,\"dUpdated_date\" DATETIME, \"sAssessment_Goal\" VARCHAR,\"fGenereal_FatPercent\" VARCHAR,\"fExtra_Value\" VARCHAR,\"sExtra_FieldName\" VARCHAR)",client_id];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return client_id;
            
        } else {
        }
        
		
		query = [NSString stringWithFormat:@"CREATE TABLE \"FT%@_AssessmentSheet_ImagesData\" (\"nImageID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"nSheetID\" INTEGER, \"sImageLabel\" VARCHAR, \"hasImage\" INTEGER)",client_id];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return client_id;
		}
        
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        if([strUpgradeStatus isEqualToString:@"YES"]){
            
        }else{
            [self insertNewAssessmentSheetFor:client_id sheetName:@"Assessment-1"];
        }
		
		
	}	
	return client_id;
}
//--------------------------------------------------------------------------

- (int) insertAnswerFor:(NSDictionary *)dictInfo {
	[self  initFMDB];
	
	if([db open])
	{
        NSString * strTextAnswer = [self escapeString:[dictInfo objectForKey:kTextAnswer]];
		NSString * query = [NSString stringWithFormat:@"insert into FT_PARQ_Answer (nClientID,bAnswer,sAnswer,nQuestionID) values(%d,'%@','%@',%d)",
							[[dictInfo objectForKey:kClientId] intValue],
							[dictInfo objectForKey:kBoolAnswer],
							strTextAnswer,
							[[dictInfo objectForKey:kQuestionId] intValue]
							];
		
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;
}

//--------------------------------------------------------------------------

#pragma mark -
#pragma mark PARQView Update Methods

- (NSString *) updateClientDetail:(NSMutableDictionary *) dictInfo{
	
    MESSAGE(@"updateClientDetail=> dict: %@",dictInfo);
	[self  initFMDB];
	if([db open])
	{
        
        NSString * strName = [self escapeString:[dictInfo objectForKey:kFirstName]];
        NSString * strLastName = [self escapeString:[dictInfo objectForKey:kLastName]];
        NSString * strEmergencyContactName1 = [self escapeString:[dictInfo objectForKey:kEmergencyConName1]];
        NSString * strEmergencyContactRelationship = [self escapeString:[dictInfo objectForKey:kEmergencyRel1]];
        NSString * strEmergencyContactName2 = [self escapeString:[dictInfo objectForKey:kEmergencyConName2]];
        NSString * strEmergencyContactRelationship2 = [self escapeString:[dictInfo objectForKey:kEmergencyRel2]];
        
        
		NSString * age = [dictInfo objectForKey:kAge];
		NSString * bDate = [dictInfo objectForKey:kBirthDate];
		
		NSDateFormatter * tmpdt = [[[NSDateFormatter alloc] init] autorelease];
		[tmpdt setDateFormat:@"MM/dd/yy hh:mm:ss a"];
		NSString * updatedDate = [tmpdt stringFromDate:[NSDate date]];
        
		
		NSString * query = [NSString stringWithFormat:@"update FT_Client set dUpdatedDate = '%@', sFirstName = '%@', sLastName = '%@', dBirthDate = '%@',nAge = '%@',",updatedDate,strName,strLastName,bDate,age];
        
		query = [query stringByAppendingFormat:@"sPhone_Day = '%@',sPhone_Evening = '%@',sPhone_cell = '%@',sEmailAddress = '%@',sEmailAddress2 = '%@',",[dictInfo objectForKey:kPday],[dictInfo objectForKey:kPeve],[dictInfo objectForKey:kPcell],[dictInfo objectForKey:kEmail1],[dictInfo objectForKey:kEmail2]];
        
		query = [query stringByAppendingFormat:@"sEmergencyContactName1 = '%@',sEmergencyContact1_Phone_Work = '%@',sEmergencyContact1_Phone_Home = '%@',sEmergencyContact1_Phone_Cell = '%@',sEmergencyContact1_Relationship = '%@',",strEmergencyContactName1,[dictInfo objectForKey:kEmergencyConWNo1],[dictInfo objectForKey:kEmergencyConHNo1],[dictInfo objectForKey:kEmergencyConCNo1],strEmergencyContactRelationship];
        
		query = [query stringByAppendingFormat:@"sEmergencyContactName2 = '%@',sEmergencyContact2_Phone_Work = '%@',sEmergencyContact2_Phone_Home = '%@',sEmergencyContact2_Phone_cell = '%@',sEmergencyContact2_Relationship = '%@',sGender = '%@',",strEmergencyContactName2,[dictInfo objectForKey:kEmergencyConWNo2],[dictInfo objectForKey:kEmergencyConHNo2],[dictInfo objectForKey:kEmergencyConCNo2],strEmergencyContactRelationship2,[dictInfo objectForKey:kgender]];
        
		query = [query stringByAppendingFormat:@"nLastViewedScreen = '0',sPhysicianName = ' ',sPhysicianPhone = ' ',sHeight = ' ',sWeight = ' ' where nclientid = '%@'",[dictInfo objectForKey:kClientId]];
        
		
		[db executeUpdate:query];
	}
	NSString * str= [db lastErrorMessage];
	if([str isEqualToString:@"not an error"]) {
		NSString * gender = [self getClientsGender:[dictInfo objectForKey:kClientId]];
		[[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
		[[NSUserDefaults standardUserDefaults] synchronize];
		
		NSString * age = [self getClientsAge:[dictInfo objectForKey:kClientId]];
		[[NSUserDefaults standardUserDefaults] setObject:age forKey:kAge];
		[[NSUserDefaults standardUserDefaults] synchronize];
		
		return [dictInfo objectForKey:kClientId];
	}	
	
	return @"";
}

//--------------------------------------------------------------------------

- (NSInteger) updateLastViewedScreen : (NSInteger) screen forClient: (NSString *) clientID {
	[self  initFMDB];
	
	if([db open])
	{

		
		NSString * query = [NSString stringWithFormat:@"update FT_Client set nLastViewedScreen = %ld where nclientid = %@",(long)screen,clientID];
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;
}

//---------------------------------------------------------------------------------------------------------------

- (int)updateLastEditedScreen:(NSString*)editedScreen forClientId:(NSString *)clientID {
  
    [self  initFMDB];
    if([db open]) {
        NSString * query = [NSString stringWithFormat:@"update FT_Client set sLastViewedPage = '%@' where nclientid = %@",editedScreen,clientID];
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 0;
        }
    }
    return 1;
}


//---------------------------------------------------------------------------------------------------------------

- (int) updateTemplateSheetName: (NSString *)sheetName forSheetID: (NSInteger)sheetId {
    [self  initFMDB];
    START_METHOD
    if([db open])
    {
        NSString * query = [NSString stringWithFormat:@"update FT_Template_Sheet set sSheetName = '%@' where nSheetId = %ld",sheetName,(long)sheetId];
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}
//--------------------------------------------------------------------------

- (int) upDateLastReviewedDate : (NSString *) clientID {
	[self  initFMDB];
	
	if([db open])
	{

		NSDateFormatter * tmpdt = [[[NSDateFormatter alloc] init] autorelease];
        [tmpdt setDateFormat:@"MM-dd-yyyy HH:mm:ss +0000"];
		NSString * updatedDate = [tmpdt stringFromDate:[NSDate date]];
		
		NSString * query = [NSString stringWithFormat:@"update FT_Client set dLastReviewedDate = '%@' where nclientid = %@",updatedDate,clientID];
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;
}

//--------------------------------------------------------------------------

- (int) updateAnswerFor:(NSDictionary *)dictInfo {
	
	[self  initFMDB];
	
	if([db open])
	{
        NSString * strTextAnswer = [self escapeString:[dictInfo objectForKey:kTextAnswer]];

		NSString * query = [NSString stringWithFormat:@"update FT_PARQ_Answer set bAnswer = %d, sAnswer = '%@' where nAnswerID = %d",
							[[dictInfo objectForKey:kBoolAnswer] intValue],
							strTextAnswer,
							[[dictInfo objectForKey:kAnswerId] intValue]
							];
		
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;	
}

//--------------------------------------------------------------------------

- (int) upDateClientPQRData : (NSDictionary *) dictInfo {
	[self  initFMDB];
	
	if([db open])
	{
        NSString * strPhysicianName = [self escapeString:[dictInfo objectForKey:kPhysicianname]];
		NSString * query = [NSString stringWithFormat:@"update FT_Client set sHeight = '%@', sWeight = '%@',dUpdatedDate = '%@',sPhysicianName = '%@',sPhysicianPhone = '%@' where nclientid = %@",[dictInfo objectForKey:kHeight],[dictInfo objectForKey:kWeight],[dictInfo objectForKey:kUpdatedDate],strPhysicianName,[dictInfo objectForKey:kPhysicianphone],[dictInfo objectForKey:kClientId]];
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;
}

//--------------------------------------------------------------------------

- (int) updateParqField :(NSDictionary *) dictInfo {
	[self initFMDB];	
	
	if (![db open]) {		
	}	
	else {
		NSString * query = @"";
		if ([[dictInfo objectForKey:kDictType] isEqualToString:@"0"]) {
			query = [NSString stringWithFormat:@"update FT_Client set %@ = '%@' where nclientid = '%@'",[dictInfo objectForKey:kfieldName],[dictInfo objectForKey:kfieldValue],[dictInfo objectForKey:kClientId]];		
		}
		else if([[dictInfo objectForKey:kDictType] isEqualToString:@"1"]){
			query = [NSString stringWithFormat:@"update FT_Client set %@ = '%@',%@ = '%@' where nclientid = '%@'",[dictInfo objectForKey:kfieldName],[dictInfo objectForKey:kfieldValue],[dictInfo objectForKey:kfieldName2],[dictInfo objectForKey:kfieldValue2],[dictInfo objectForKey:kClientId]];		
		}
		else {
			query = [NSString stringWithFormat:@"update FT_PARQ_Answer set %@ = '%@' where nAnswerID = %d",[dictInfo objectForKey:kfieldName],[dictInfo objectForKey:kfieldValue],[[dictInfo objectForKey:kAnswerId] intValue]];
		}

		DebugLOG(query);
		
		[db executeUpdate:query];
	}
	NSString * str= [db lastErrorMessage];
	DebugLOG(str);
	if([str isEqualToString:@"not an error"]) {
		return 0;
	}	
	
	return 1;
}

//--------------------------------------------------------------------------

#pragma mark -
#pragma mark PARQView Delete Methods

- (int) deleteClientDetail : (NSString *) clientID {
    START_METHOD
	[self initFMDB];	
	if (![db open]) {		
	}	
	else {	
		[db executeUpdate:[NSString stringWithFormat:@"drop table FT%@_ProgramSheet",clientID]];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		[db executeUpdate:[NSString stringWithFormat:@"drop table FT%@_ProgramSheet_Blocks",clientID]];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		[db executeUpdate:[NSString stringWithFormat:@"drop table FT%@_ProgramSheet_BlockData",clientID]];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		[db executeUpdate:[NSString stringWithFormat:@"drop table FT%@_AssessmentSheet",clientID]];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		[db executeUpdate:[NSString stringWithFormat:@"drop table FT%@_AssessmentSheet_Data",clientID]];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		[db executeUpdate:[NSString stringWithFormat:@"delete from FT_PARQ_Answer where nClientID = %@",clientID]];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		[db executeUpdate:[NSString stringWithFormat:@"delete from FT_Client where nClientID = %@",clientID]];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;
}

//--------------------------------------------------------------------------

#pragma mark -
#pragma mark PARQView Select Methods


//--------------------------------------------------------------------------

- (NSMutableArray *) getClientsList :(NSString *) scope {
	START_METHOD
	[self initFMDB];	
	NSMutableArray * arrClientsDetail = [[NSMutableArray alloc] init];
	if (![db open]) {		
	}	
	else {		
		NSString * query = @"SELECT nclientid,sFirstName,sLastName FROM FT_Client ";
		
		if ([scope isEqualToString:@"0"]) {
			query = [query stringByAppendingFormat:@"%@",@"order by sFirstName"];
		}
		else if ([scope isEqualToString:@"1"]) {
			query = [query stringByAppendingFormat:@"%@",@"order by sFirstName,sLastName"];
		}
		else {
			query = @"SELECT nclientid,sFirstName,sLastName,dLastReviewedDate FROM FT_Client order by dLastReviewedDate desc LIMIT 0,10";			
		}
		
		
		FMResultSet * rs = [db executeQuery:query];
        
        MESSAGE(@"getClientsList;> %@",query);
		
		while ([rs next]) {
			
			NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
			
			[dict setObject:[rs stringForColumn:@"nclientid"] forKey:kClientId];
			[dict setObject:[rs stringForColumn:@"sFirstName"] forKey:kFirstName];
			[dict setObject:[rs stringForColumn:@"sLastName"] forKey:kLastName];
			
			if ([scope isEqualToString:@"2"]) {
				if ([[rs stringForColumn:@"dLastReviewedDate"] length] != 0) {
					[dict setObject:[rs stringForColumn:@"dLastReviewedDate"] forKey:kLastReviewdDate];
                    
                    if (dict != nil && [dict count] > 0) {
                        [arrClientsDetail addObject:dict];
                    }
                }
			}
			else {
                if (dict != nil && [dict count] > 0) {
                    [arrClientsDetail addObject:dict];
                }
			}
			[dict release];			
		}			
	}	
	return [arrClientsDetail autorelease];
}

//--------------------------------------------------------------------------

- (NSDictionary	*) getClientsDetail :(NSString *) clientID {
    [self initFMDB];
    NSMutableDictionary * dictClientsInfo = [[NSMutableDictionary alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT sFirstName,sLastName,sGender,dBirthDate,nAge,sPhone_Day,sPhone_Evening,sPhone_Cell,sEmailAddress,sEmailAddress2,sEmergencyContactName1,sEmergencyContact1_Relationship,sEmergencyContact1_Phone_Work,sEmergencyContact1_Phone_Home,sEmergencyContact1_Phone_Cell,sEmergencyContactName2,sEmergencyContact2_Relationship,sEmergencyContact2_Phone_Work,sEmergencyContact2_Phone_Home,sEmergencyContact2_Phone_Cell,dUpdatedDate FROM FT_Client where nclientid = %d",[clientID intValue]];
        
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            [dictClientsInfo setObject:[rs stringForColumn:@"sFirstName"] forKey:kFirstName];
            [dictClientsInfo setObject:[rs stringForColumn:@"sLastName"] forKey:kLastName];
            [dictClientsInfo setObject:[rs stringForColumn:@"sGender"] forKey:kgender];
            [dictClientsInfo setObject:[rs stringForColumn:@"dBirthDate"] forKey:kBirthDate];
            [dictClientsInfo setObject:[rs stringForColumn:@"nAge"] forKey:kAge];
            [dictClientsInfo setObject:[rs stringForColumn:@"sPhone_Day"] forKey:kPday];
            [dictClientsInfo setObject:[rs stringForColumn:@"sPhone_Evening"] forKey:kPeve];
            [dictClientsInfo setObject:[rs stringForColumn:@"sPhone_Cell"] forKey:kPcell];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmailAddress"] forKey:kEmail1];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmailAddress2"] forKey:kEmail2];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContactName1"] forKey:kEmergencyConName1];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact1_Relationship"] forKey:kEmergencyRel1];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact1_Phone_Work"] forKey:kEmergencyConWNo1];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact1_Phone_Home"] forKey:kEmergencyConHNo1];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact1_Phone_Cell"] forKey:kEmergencyConCNo1];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContactName2"] forKey:kEmergencyConName2];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact2_Relationship"] forKey:kEmergencyRel2];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact2_Phone_Work"] forKey:kEmergencyConWNo2];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact2_Phone_Home"] forKey:kEmergencyConHNo2];
            [dictClientsInfo setObject:[rs stringForColumn:@"sEmergencyContact2_Phone_Cell"] forKey:kEmergencyConCNo2];
            [dictClientsInfo setObject:[rs stringForColumn:@"dUpdatedDate"] forKey:kUpdatedDate];
        }	
        
        DebugLOG([db lastErrorMessage]);
        
        
    }
    return (NSDictionary *) [dictClientsInfo autorelease];
}


//--------------------------------------------------------------------------

- (NSString *) getClientsGender :(NSString *) clientID {
	[self initFMDB];	
	NSString * gender = @"";
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT sGender FROM FT_Client where nclientid = %d",[clientID intValue]];
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			gender = [rs stringForColumn:@"sGender"];
		}
	}
	return gender;
}

//--------------------------------------------------------------------------

- (NSString *) getClientsAge :(NSString *) clientID {
	[self initFMDB];	
	NSString * age = @"";
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT nAge FROM FT_Client where nclientid = %d",[clientID intValue]];
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			age = [rs stringForColumn:@"nAge"];
		}
	}
	return age;
}

//--------------------------------------------------------------------------

- (int) getLastViewdScreenForClient:(NSString *)clientID {
	[self initFMDB];	
	NSString * ansDict = @"0";
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT nLastViewedScreen FROM FT_Client where nClientID=%d",[clientID intValue]];
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			ansDict = [rs stringForColumn:@"nLastViewedScreen"];
		}			
	}	
	return [ansDict intValue];
}


-(NSString *)getLastEditedScreenForClient:(NSString *) clientID {
    
     [self initFMDB];
    NSString *editedScreen = nil;
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT nLastViewedScreen,sLastViewedPage FROM FT_Client where nClientID=%d",[clientID intValue]];
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            editedScreen =[rs stringForColumn:@"nLastViewedScreen"];
            NSString *lastEditedPage =  [rs stringForColumn:@"sLastViewedPage"];
            if(lastEditedPage.length == 0) {
                lastEditedPage = @"0";
            }
            editedScreen = [editedScreen stringByAppendingString:[NSString stringWithFormat:@"/##/%@",lastEditedPage]];

        }
    }
    return editedScreen;
    
}

//--------------------------------------------------------------------------

- (NSArray *) selectQuestionCategory {
	[self initFMDB];	
	NSMutableArray * arrayCategory = [[NSMutableArray alloc] init];
	if (![db open]) {		
	}	
	else {		
		NSString * query = @"SELECT * FROM FT_PARQ_Category ";		
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
								   [rs stringForColumn:@"nCategoryID"],kCategoryId,
								   [rs stringForColumn:@"sCategoryName"],kCategoryName,nil];
			
			[arrayCategory addObject:dict];
			
		}			
	}	
	return (NSArray *) [arrayCategory autorelease];
}

//--------------------------------------------------------------------------

- (NSArray *) selectQuestionForCategory:(NSString *) categoryID {
	[self initFMDB];	
	NSMutableArray * arrayQuestion = [[NSMutableArray alloc] init];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_PARQ_Questions where nCategoryID = %d",[categoryID intValue]];		
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
								   [rs stringForColumn:@"nQuestionID"],kQuestionId,
								   [rs stringForColumn:@"nCategoryID"],kQuestionCateId,
								   [rs stringForColumn:@"sQuestion"],kQuestion,
								   [rs stringForColumn:@"bBooleanQuestion"],kQuestionBool,
								   [rs stringForColumn:@"bTextualQuestion"],kQuestionText,nil];
			
			[arrayQuestion addObject:dict];
			
		}			
	}	
	return (NSArray *) [arrayQuestion autorelease];
}

//--------------------------------------------------------------------------

- (int) checkAnswerForClient:(NSString *)clientId {
	int exist = 0;
	[self initFMDB];
	if(![db open]) {
	}
	else {
		NSString *qry=[NSString stringWithFormat:@"select count(*) as count from FT_PARQ_Answer where nClientID=%d",[clientId intValue]];
		FMResultSet *rs=[db executeQuery:qry];
		while ([rs next]) 
		{
			exist=[[rs stringForColumn:@"count"] intValue];
		}
	}
	return exist;
}

//--------------------------------------------------------------------------

- (NSDictionary *) answerForQuestion:(NSString *) questionId :(NSString *)clientID {
	[self initFMDB];	
	NSDictionary * ansDict = [[[NSDictionary alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_PARQ_Answer where nQuestionID = %d and nClientID=%d",[questionId intValue],[clientID intValue]];
        MESSAGE(@"query for answer: %@",query);
        
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			ansDict = [NSDictionary dictionaryWithObjectsAndKeys:
					   [rs stringForColumn:@"nAnswerID"],kAnswerId,
					   [rs stringForColumn:@"nClientID"],kClientId,
					   [rs stringForColumn:@"bAnswer"],kBoolAnswer,
					   [rs stringForColumn:@"sAnswer"],kTextAnswer,
					   [rs stringForColumn:@"nQuestionID"],kQuestionId,nil];
		}			
	}	
	return ansDict;
}

//======================================== Notes VC Methods =============================================================//

-(NSArray*)getNotesForClient:(NSString*)clientId andSheets:(NSString *)sheetId {
    
    [self initFMDB];
    NSMutableArray * arrSheetDetail = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        
        // SUNIL add :  group by sBlockNotes  ORDER BY nBlockNo  04 May 2016
         NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %@   group by sBlockNotes  ORDER BY nBlockNo",clientId,sheetId];
      //  NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %@  and sBlockTitle='' ORDER BY nBlockNo",clientId,sheetId];
        
        MESSAGE(@"query-> Get anotes : %@",query);
        
        
        FMResultSet * rs = [db executeQuery:query];
        while ([rs next]) {
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            NSString *strNotes = [rs stringForColumn:@"sBlockNotes"];
            if([strNotes isKindOfClass:[NSNull class]] || [strNotes isEqual:nil] || !strNotes.length > 0 ) {
                strNotes = @"";
            }
            
            [dict setObject:[rs stringForColumn:@"dBlockDate"] forKey:kBlockDate];
            [dict setObject:strNotes forKey:kBlockNotes];
            [dict setObject:[rs stringForColumn:@"nBlockID"] forKey:kBlockId];
            [dict setObject:[rs stringForColumn:@"nSheetID"] forKey:kSheetId];
            [dict setObject:[rs stringForColumn:@"nBlockNo"] forKey:kBlockNo];
            [arrSheetDetail addObject:dict];
            [dict release];
        }
    }
    return [arrSheetDetail autorelease];
}

//Sunil Addeed method for get perticular workout anme
-(NSArray*)getWorkoutNameForNotes:(NSString*)clientId andBlockNo:(NSString *)strBlockNo  andSheetId:(NSString *)strSheetId{
    
    [self initFMDB];
    NSMutableArray * arrSheetDetail = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        
        // SUNIL add :  and sBlockTitle=''
        NSString * query = [NSString stringWithFormat:@"SELECT sBlockTitle FROM FT%@_ProgramSheet_Blocks where nBlockNo = %@  and sBlockTitle !='' and nSheetID=%@",clientId,strBlockNo,strSheetId];
        
        MESSAGE(@"getWorkoutNameForNotes ->query-> Get anotes : %@",query);
        
        
        FMResultSet * rs = [db executeQuery:query];
        while ([rs next]) {
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            NSString *strNotes = [rs stringForColumn:@"sBlockTitle"];
            if([strNotes isKindOfClass:[NSNull class]] || [strNotes isEqual:nil] || !strNotes.length > 0 ) {
                strNotes = @"";
            }
            
            [dict setObject:[rs stringForColumn:@"sBlockTitle"] forKey:@"sBlockTitle"];
            
            [arrSheetDetail addObject:dict];
            [dict release];
        }
    }
    return [arrSheetDetail autorelease];
}


-(NSArray *)getTotalSheetsForClient:(NSString *)clientId {
    
    [self initFMDB];
    NSMutableArray * arrSheetDetail = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT nSheetID,sSheetName  FROM FT%@_ProgramSheet",clientId];
        
        FMResultSet * rs = [db executeQuery:query];
        while ([rs next]) {
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            [dict setObject:[rs stringForColumn:@"nSheetID"] forKey:kSheetId];
            [dict setObject:[rs stringForColumn:@"sSheetName"] forKey:ksheetName];
            [arrSheetDetail addObject:dict];
            [dict release];
        }			
    }	
    return [arrSheetDetail autorelease];
    
}


//========================================Assessment VC Methods Methods=================================================//

#pragma mark -
#pragma mark AssessmentView Insert Methods

- (NSInteger) insertNewAssessmentSheetFor :(NSString *) clientID sheetName:(NSString *) sheetName {
	[self  initFMDB];
	
	if([db open])
	{
        sheetName = [self escapeString:sheetName];
		NSString * query = [NSString stringWithFormat:@"insert into FT%@_AssessmentSheet (sSheetName) values ('%@')",clientID,sheetName];
		
		[db executeUpdate:query];
        
        MESSAGE(@"Query for insert Assessment : %@",query);
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			NSString * gender = [self getClientsGender:clientID];
			[[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
			[[NSUserDefaults standardUserDefaults] synchronize];
			
			NSString * age = [self getClientsAge:clientID];
			[[NSUserDefaults standardUserDefaults] setObject:age forKey:kAge];
			[[NSUserDefaults standardUserDefaults] synchronize];
			
			NSString * sheetID = @"0";
			NSString * query = [NSString stringWithFormat:@"SELECT  max(nSheetID) from FT%@_AssessmentSheet",clientID];
			FMResultSet * rs = [db executeQuery:query];
			while ([rs next]) {
				sheetID = [rs stringForColumn:@"max(nSheetID)"];
			}
			[self insertAssessmentSheetDataFor:clientID sheetID:[sheetID intValue]];
			[self insertAssessmentSheetImagesDataFor:clientID sheetID:[sheetID intValue]];
		}
		
	}
	return 0;
}

- (int) insertAssessmentSheetDataFor :(NSString *) clientID sheetID:(int) sheetID {
	[self  initFMDB];
	
	if([db open])
	{
		
		NSDateFormatter * tmpdt = [[[NSDateFormatter alloc] init] autorelease];
		[tmpdt setDateFormat:kAppDateFormat];
		NSString * updatedDate = [tmpdt stringFromDate:[NSDate date]];
		
		NSString * query = [NSString stringWithFormat:@" insert into FT%d_AssessmentSheet_Data (nSheetID,fHeight,fWeight,nRestingHeartBeat,sBloodPressure,",[clientID intValue]];
		query = [query stringByAppendingString:@"fThree_Male_Chest,fThree_Male_Abdomen,fThree_Male_Thigh,"];
		query = [query stringByAppendingString:@"fThree_Female_Triceps,fThree_Female_Suprailiac,fThree_Female_Thigh,"];
		query = [query stringByAppendingString:@"fFour_Abdomen,fFour_Suprailiac,fFour_Triceps,fFour_Thigh,"];
		query = [query stringByAppendingString:@"fSeven_Chest,fSeven_Midaxillary,fSeven_Triceps,fSeven_subscapular,fSeven_Abdomen,fSeven_Suprailiac,fSeven_Thigh,"];
		query = [query stringByAppendingString:@"fCM_RightUpperArm,fCM_RightForeArm,fCM_Chest,fCM_Abdominal,fCM_HipButtocks,fCM_RightThing,fCM_RightCalf,"];
		query = [query stringByAppendingString:@"fBMI_Metrics_Weight,fBMI_Metrics_Height,fBMI_Metrics_Result,fBMI_Pound_Weight,fBMI_Pound_Height,fBMI_Pound_Result,fCM_Result,fThree_Result,fFour_Result,fSeven_Result,dUpdated_date,sAssessment_Goal,fGenereal_FatPercent,fExtra_Value,sExtra_FieldName) values("];
		query = [query stringByAppendingFormat:@"%d,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'%@','',0,0,'')",sheetID,updatedDate];
		DebugLOG(query);
		[db executeUpdate:query];
	}
	NSString * str= [db lastErrorMessage];
	if([str isEqualToString:@"not an error"]) {
		return 1;
	}
	return 0;
}

- (int) insertAssessmentSheetImagesDataFor :(NSString *) clientID sheetID:(int) sheetID {
	[self  initFMDB];
	
	if([db open])
	{
		NSString * query = @"";
		NSString * str = @"";
		for (int i = 0; i<5; i++) {
			query =[NSString stringWithFormat:@"insert into FT%d_AssessmentSheet_ImagesData (nSheetID,sImageLabel,hasImage) values (%d,'',0)",[clientID intValue],sheetID];
			DebugLOG(query);
			[db executeUpdate:query];
			str= [db lastErrorMessage];
			if(![str isEqualToString:@"not an error"]) {
				return 0;
			}
		}		
	}	
	return 1;
}

#pragma mark -
#pragma mark AssessmentView Update Methods

- (NSInteger) updateAssessmentSheetNameFor:(NSString *) clientID ForSheetID:(NSInteger) sheetID toSheetName:(NSString *)sheetName  {
	[self  initFMDB];
	
	if([db open])
	{
        sheetName = [self escapeString:sheetName];
		NSString * query = [NSString stringWithFormat:@"update FT%d_AssessmentSheet set sSheetName = '%@' where nSheetID = %ld",[clientID intValue],sheetName,(long)sheetID];
		DebugLOG(query);
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
	}
	return 0;
}


- (int) updateAssessmentSheetFor : (NSMutableDictionary *) dictInfo {
    [self  initFMDB];
    
    if([db open])
    {
        NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
        
        NSString * query = [NSString stringWithFormat:@" update FT%d_AssessmentSheet_Data set fHeight = %f, fWeight = %f, nRestingHeartBeat = %d, sBloodPressure = '%@', ",
                            [[dictInfo objectForKey:kClientId] intValue],
                            [[dictInfo objectForKey:kHeight] doubleValue],
                            [[dictInfo objectForKey:kWeight] doubleValue],
                            [[dictInfo objectForKey:kRestingHeartBeat] intValue],
                            [dictInfo objectForKey:kBloodPressure]];
        if ([gender isEqualToString:@"M"]) {
            query = [query stringByAppendingFormat:@"fThree_Male_Chest = '%@', fThree_Male_Abdomen ='%@', fThree_Male_Thigh = '%@', fThree_Result = '%@', ",
                     [dictInfo objectForKey:kThree_Data1] ,
                     [dictInfo objectForKey:kThree_Data2] ,
                     [dictInfo objectForKey:kThree_Data3] ,
                     [dictInfo objectForKey:kfThree_Result] ];
        }
        else {
            query = [query stringByAppendingFormat:@"fThree_Female_Triceps = '%@', fThree_Female_Suprailiac = '%@', fThree_Female_Thigh = '%@', fThree_Result ='%@', ",
                     [dictInfo objectForKey:kThree_Data1] ,
                     [dictInfo objectForKey:kThree_Data2] ,
                     [dictInfo objectForKey:kThree_Data3] ,
                     [dictInfo objectForKey:kfThree_Result] ];
        }
        
        query = [query stringByAppendingFormat:@"fFour_Abdomen = %f, fFour_Suprailiac = %f, fFour_Triceps = %f, fFour_Thigh = %f, fFour_Result = %f, ",
                 [[dictInfo objectForKey:kFour_Abdomen] doubleValue],
                 [[dictInfo objectForKey:kFour_Suprailiac] doubleValue],
                 [[dictInfo objectForKey:kFour_Triceps] doubleValue],
                 [[dictInfo objectForKey:kFour_Thigh] doubleValue],
                 [[dictInfo objectForKey:kFour_Result] doubleValue]];
        
        query = [query stringByAppendingFormat:@"fSeven_Chest = %f, fSeven_Midaxillary = %f, fSeven_Triceps = %f, fSeven_subscapular = %f, fSeven_Abdomen = %f, fSeven_Suprailiac = %f, fSeven_Thigh = %f, fSeven_Result = %f, ",
                 [[dictInfo objectForKey:kSeven_Chest] doubleValue],
                 [[dictInfo objectForKey:kSeven_Midaxillary] doubleValue],
                 [[dictInfo objectForKey:kSeven_Triceps] doubleValue],
                 [[dictInfo objectForKey:kSeven_subscapular] doubleValue],
                 [[dictInfo objectForKey:kSeven_Abdomen] doubleValue],
                 [[dictInfo objectForKey:kSeven_Suprailiac] doubleValue],
                 [[dictInfo objectForKey:kSeven_Thigh] doubleValue],
                 [[dictInfo objectForKey:kfSeven_Result] doubleValue]];
        
        query = [query stringByAppendingFormat:@"fCM_RightUpperArm = '%@', fCM_RightForeArm = %f, fCM_Chest = %f, fCM_Abdominal = %f, fCM_HipButtocks = %f, fCM_RightThing = %f, fCM_RightCalf = %f,fCM_Result = %f, ",
                 [dictInfo objectForKey:kCM_RightUpperArm] ,
                 [[dictInfo objectForKey:kCM_RightForeArm] doubleValue],
                 [[dictInfo objectForKey:kCM_Chest] doubleValue],
                 [[dictInfo objectForKey:kCM_Abdominal] doubleValue],
                 [[dictInfo objectForKey:kCM_HipButtocks] doubleValue],
                 [[dictInfo objectForKey:kCM_RightThing] doubleValue],
                 [[dictInfo objectForKey:kCM_RightCalf] doubleValue],
                 [[dictInfo objectForKey:kCM_Result] doubleValue]];
        
        query = [query stringByAppendingFormat:@"fBMI_Pound_Weight = %f, fBMI_Pound_Height = %f, fBMI_Pound_Result = %f, ",
                 [[dictInfo objectForKey:kBMI_Pound_Weight] doubleValue],
                 [[dictInfo objectForKey:kBMI_Pound_Height] doubleValue],
                 [[dictInfo objectForKey:kBMI_Pound_Result] doubleValue]];
        
        query = [query stringByAppendingFormat:@"fBMI_Metrics_Weight = %f, fBMI_Metrics_Height = %f, fBMI_Metrics_Result = %f, ",
                 [[dictInfo objectForKey:kBMI_Metrics_Weight] doubleValue],
                 [[dictInfo objectForKey:kBMI_Metrics_Height] doubleValue],
                 [[dictInfo objectForKey:kBMI_Metrics_Result] doubleValue]];
        
        query = [query stringByAppendingFormat:@"dUpdated_date = '%@', sAssessment_Goal = '%@',",
                 [dictInfo objectForKey:kAssUpdatedDate],
                 [dictInfo objectForKey:ksAssessment_Goal]];
        
        query = [query stringByAppendingFormat:@"fGenereal_FatPercent = '%@', fExtra_Value = '%@', sExtra_FieldName = '%@'",
                 [dictInfo objectForKey:kGereralFatPercent],
                 [dictInfo objectForKey:kExtraValue],[dictInfo objectForKey:kExtraFieldName]];
        
        query = [query stringByAppendingFormat:@" where nSheetID = %d", [[dictInfo objectForKey:kSheetId] intValue]];
        MESSAGE(@"Query for insert Assessment daata: %@",query);
        [db executeUpdate:query];
    }
    NSString * str= [db lastErrorMessage];
    DebugLOG(str);
    if([str isEqualToString:@"not an error"]) {
        return 0;
    }
    
    return 1;
}


- (int) updateField :(NSDictionary *) dictInfo {
    [self initFMDB];
    
    if (![db open]) {
    }
    else {
        NSString * query = @"";
        if ([[dictInfo objectForKey:kDictType] isEqualToString:@"0"]) {
            query = [NSString stringWithFormat:@"update FT%@_AssessmentSheet_Data set %@ = '%@' where nSheetID = %d",[dictInfo objectForKey:kClientId],[dictInfo objectForKey:kfieldName],[dictInfo objectForKey:kfieldValue],[[dictInfo objectForKey:ksheetID] intValue]];
        }
        else if([[dictInfo objectForKey:kDictType] isEqualToString:@"2"]){
            query = [NSString stringWithFormat:@"update FT%@_AssessmentSheet_Data set %@ = '%@' where nSheetID = %d",[dictInfo objectForKey:kClientId],[dictInfo objectForKey:kfieldName],[dictInfo objectForKey:kfieldValue],[[dictInfo objectForKey:ksheetID] intValue]];
        }
        else {
            query = [NSString stringWithFormat:@"update FT%@_AssessmentSheet_Data set %@ = '%@',%@ = '%@' where nSheetID = %d",[dictInfo objectForKey:kClientId],[dictInfo objectForKey:kfieldName],[dictInfo objectForKey:kfieldValue],[dictInfo objectForKey:kfieldName2],[dictInfo objectForKey:kfieldValue2] ,[[dictInfo objectForKey:ksheetID] intValue]];
        }
        [db executeUpdate:query];
        MESSAGE(@"Assessemnt query:\n\n %@\n\n",query);
    }
    
    
    NSString * str= [db lastErrorMessage];
    DebugLOG(str);
    if([str isEqualToString:@"not an error"]) {
        return 0;
    }	
    
    return 1;
}

- (int) updateAssessmentImageTitlefor :(NSDictionary *) dictInfo {
    START_METHOD
	[self initFMDB];	
	
	if (![db open]) {		
	}	
	else {
		NSString * query = @"";
		
        NSString * strImageName = [self escapeString:[dictInfo objectForKey:kImageName]];
		query = [NSString stringWithFormat:@"update FT%@_AssessmentSheet_ImagesData set sImageLabel = '%@',hasImage = %d where nSheetID = %d and nImageID = %d",[dictInfo objectForKey:kClientId],strImageName,[[dictInfo objectForKey:khasImage] intValue],[[dictInfo objectForKey:ksheetID] intValue],[[dictInfo objectForKey:kImageID] intValue]];
		DebugLOG(query);
        
        
        MESSAGE(@"updateAssessmentImageTitlefor--> %@",query);
        
		[db executeUpdate:query];
	}
	NSString * str= [db lastErrorMessage];
	DebugLOG(str);
	if([str isEqualToString:@"not an error"]) {
		return 0;
	}	
	
	return 1;
	
}

#pragma mark -
#pragma mark AssessmentView Select Methods



- (int) getCountofAssessmentSheetForClient:(NSString *)clientID {
	[self initFMDB];	
	int count = 0;
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_AssessmentSheet",clientID];		
		FMResultSet * rs = [db executeQuery:query];
		DebugLOG(query);
		while ([rs next]) {
			count++;
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return count;
}

- (NSMutableArray *) getAllAssessmentSheetsFor:(NSString *)clientID {
	[self initFMDB];	
	NSMutableArray * assessmentList = [[NSMutableArray alloc] init];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_AssessmentSheet",clientID];		
		FMResultSet * rs = [db executeQuery:query];
		DebugLOG(query);
		while ([rs next]) {
			NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
									   [rs stringForColumn:@"nSheetID"],ksheetID,
									   [rs stringForColumn:@"sSheetName"],ksheetName,nil];
			[assessmentList addObject:dictInfo];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return [assessmentList autorelease];
}

- (NSMutableArray *) getAssessmentSheetsDataForSheet:(NSString *)sheetID ofClientID:(NSString *)clientID {
    [self initFMDB];
    NSMutableArray * assessmentDataList = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%d_AssessmentSheet_Data where nSheetID = %d",[clientID intValue],[sheetID intValue]];
        DebugLOG(query);
        FMResultSet * rs = [db executeQuery:query];
        NSString * gender = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kgender]];
        while ([rs next]) {
            NSMutableDictionary * dictInfo = [[NSMutableDictionary alloc] init];
            [dictInfo setObject:[rs stringForColumn:@"nDetailID"] forKey:kdetailID];
            [dictInfo setObject:[rs stringForColumn:@"fHeight"]	forKey:	kHeight];
            [dictInfo setObject:[rs stringForColumn:@"fWeight"]	forKey:	kWeight];
            [dictInfo setObject:[rs stringForColumn:@"nRestingHeartBeat"]	forKey:	kRestingHeartBeat];
            [dictInfo setObject:[rs stringForColumn:@"sBloodPressure"]	forKey:	kBloodPressure];
            if ([gender isEqualToString:@"M"]) {
                [dictInfo setObject:[rs stringForColumn:@"fThree_Male_Chest"]	forKey:	kThree_Data1];
                [dictInfo setObject:[rs stringForColumn:@"fThree_Male_Abdomen"]	forKey:	kThree_Data2];
                [dictInfo setObject:[rs stringForColumn:@"fThree_Male_Thigh"]	forKey:	kThree_Data3];
            }
            else {
                [dictInfo setObject:[rs stringForColumn:@"fThree_Female_Triceps"]	forKey:	kThree_Data1];
                [dictInfo setObject:[rs stringForColumn:@"fThree_Female_Suprailiac"]	forKey:	kThree_Data2];
                [dictInfo setObject:[rs stringForColumn:@"fThree_Female_Thigh"]	forKey:	kThree_Data3];
            }
            DebugLOG([rs stringForColumn:@"fFour_Triceps"]);
            [dictInfo setObject:[rs stringForColumn:@"fFour_Abdomen"]	forKey:	kFour_Abdomen];
            [dictInfo setObject:[rs stringForColumn:@"fFour_Suprailiac"]	forKey:	kFour_Suprailiac];
            [dictInfo setObject:[rs stringForColumn:@"fFour_Triceps"]	forKey:	kFour_Triceps];
            [dictInfo setObject:[rs stringForColumn:@"fFour_Thigh"]	forKey:	kFour_Thigh];
            
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Chest"]	forKey:	kSeven_Chest];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Midaxillary"]	forKey:	kSeven_Midaxillary];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Triceps"]	forKey:	kSeven_Triceps];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_subscapular"]	forKey:	kSeven_subscapular];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Abdomen"]	forKey:	kSeven_Abdomen];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Suprailiac"]	forKey:	kSeven_Suprailiac];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Thigh"]	forKey:	kSeven_Thigh];
            
            [dictInfo setObject:[rs stringForColumn:@"fCM_RightUpperArm"]	forKey:	kCM_RightUpperArm];
            [dictInfo setObject:[rs stringForColumn:@"fCM_RightForeArm"]	forKey:	kCM_RightForeArm];
            [dictInfo setObject:[rs stringForColumn:@"fCM_Chest"]	forKey:	kCM_Chest];
            [dictInfo setObject:[rs stringForColumn:@"fCM_Abdominal"]	forKey:	kCM_Abdominal];
            [dictInfo setObject:[rs stringForColumn:@"fCM_HipButtocks"]	forKey:	kCM_HipButtocks];
            [dictInfo setObject:[rs stringForColumn:@"fCM_RightThing"]	forKey:	kCM_RightThing];
            [dictInfo setObject:[rs stringForColumn:@"fCM_RightCalf"]	forKey:	kCM_RightCalf];
            
            [dictInfo setObject:[rs stringForColumn:@"fBMI_Pound_Weight"]	forKey:	kBMI_Pound_Weight];
            [dictInfo setObject:[rs stringForColumn:@"fBMI_Pound_Height"]	forKey:	kBMI_Pound_Height];
            [dictInfo setObject:[rs stringForColumn:@"fBMI_Pound_Result"]	forKey:	kBMI_Pound_Result];
            
            [dictInfo setObject:[rs stringForColumn:@"fBMI_Metrics_Weight"]	forKey:	kBMI_Metrics_Weight];
            [dictInfo setObject:[rs stringForColumn:@"fBMI_Metrics_Height"]	forKey:	kBMI_Metrics_Height];
            [dictInfo setObject:[rs stringForColumn:@"fBMI_Metrics_Result"]	forKey:	kBMI_Metrics_Result];
            
            [dictInfo setObject:[rs stringForColumn:@"fCM_Result"]	forKey:	kCM_Result];
            
            [dictInfo setObject:[rs stringForColumn:@"dUpdated_date"]	forKey:	kAssUpdatedDate];
            [dictInfo setObject:[rs stringForColumn:@"sAssessment_Goal"]	forKey:	ksAssessment_Goal];
            
            [dictInfo setObject:[rs stringForColumn:@"fGenereal_FatPercent"]	forKey:	kGereralFatPercent];
            [dictInfo setObject:[rs stringForColumn:@"fExtra_Value"]	forKey:	kExtraValue];
            [dictInfo setObject:[rs stringForColumn:@"sExtra_FieldName"]	forKey:	kExtraFieldName];
            
            [dictInfo setObject:[rs stringForColumn:@"fThree_Result"]	forKey:	kfThree_Result];
            [dictInfo setObject:[rs stringForColumn:@"fFour_Result"]	forKey:	kFour_Result];
            [dictInfo setObject:[rs stringForColumn:@"fSeven_Result"]	forKey:	kfSeven_Result];
           
            
            [assessmentDataList addObject:dictInfo];
            [dictInfo release];
        }			
    }	
    return [assessmentDataList autorelease];
}

- (NSArray *) getAllAssessmentImagesForSheet:(NSString *) sheetID ofClientID:(NSString *)clientID {
	NSMutableArray * cliensImagesArr = [[[NSMutableArray alloc] init] autorelease];
	[self initFMDB];
	if(![db open]) {
	}
	else { 
		NSString *qry=[NSString stringWithFormat:@"select * from FT%d_AssessmentSheet_ImagesData where nSheetID = %d",[clientID intValue],[sheetID intValue]];
		FMResultSet *rs=[db executeQuery:qry];
		while ([rs next]) {
			NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:[rs stringForColumn:@"nImageID"],kImageID,
									   [rs stringForColumn:@"sImageLabel"],kImageName,
									   [rs stringForColumn:@"hasImage"],khasImage,nil];
			[cliensImagesArr addObject:dictInfo];
		}
	}
	DebugLOG([cliensImagesArr description]);
	DebugLOG([db lastErrorMessage]);
	return (NSArray *) cliensImagesArr;
}

#pragma mark -
#pragma mark AssessmentView Charting Methods

- (NSArray	*) getClientsWeightHeight :(NSString *) clientID {
	
	NSMutableArray * cliensArr = [[[NSMutableArray alloc] init] autorelease];
    BOOL isNoData = YES;
	[self initFMDB];
	if(![db open]) {
	}
	else {
		NSString *qry=[NSString stringWithFormat:@"select fWeight from FT%d_AssessmentSheet_Data",[clientID intValue]];
		FMResultSet *rs=[db executeQuery:qry];
		while ([rs next]) 
		{
            if ([[rs stringForColumn:@"fWeight"] floatValue] != 0) {
                isNoData = NO;
            }
            [cliensArr addObject:[rs stringForColumn:@"fWeight"]];
		}
	}
    if (isNoData) {
        [cliensArr removeAllObjects];
    }
    
	DebugLOG([cliensArr description]);
	DebugLOG([db lastErrorMessage]);
	return (NSArray *) cliensArr;
}

- (NSArray	*) getClientsGeneralBodyFatPercentage :(NSString *) clientID {
    
    NSMutableArray * cliensArr = [[[NSMutableArray alloc] init] autorelease];
    BOOL isNoData = YES;
    [self initFMDB];
    if(![db open]) {
    }
    else {
        NSString *qry=[NSString stringWithFormat:@"select fGenereal_FatPercent from FT%d_AssessmentSheet_Data",[clientID intValue]];
        FMResultSet *rs=[db executeQuery:qry];
        while ([rs next])
        {
            if ([[rs stringForColumn:@"fGenereal_FatPercent"] floatValue] != 0) {
                isNoData = NO;
            }
            [cliensArr addObject:[rs stringForColumn:@"fGenereal_FatPercent"]];
        }
    }
    if (isNoData) {
        [cliensArr removeAllObjects];
    }
    
    DebugLOG([cliensArr description]);
    DebugLOG([db lastErrorMessage]);
    return (NSArray *) cliensArr;
}

- (NSArray	*) getClientsBMIResult :(NSString *) clientID {
	
	NSMutableArray * poundsArr = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * metricsArr = [[[NSMutableArray alloc] init] autorelease];
    BOOL haspoundArrData = NO;
    BOOL hasmetricsArrData = NO;
    
	[self initFMDB];
	if(![db open]) {
	}
	else {
		NSString *qry=[NSString stringWithFormat:@"select fBMI_Pound_Result,fBMI_Metrics_Result from FT%d_AssessmentSheet_Data",[clientID intValue]];
		FMResultSet *rs=[db executeQuery:qry];
		while ([rs next]) 
		{
            if ([[rs stringForColumn:@"fBMI_Pound_Result"] floatValue] != 0) {
                haspoundArrData = YES;
            }
			[poundsArr addObject:[rs stringForColumn:@"fBMI_Pound_Result"]];
            
            if ([[rs stringForColumn:@"fBMI_Metrics_Result"] floatValue] != 0) {
                hasmetricsArrData = YES;
            }
			[metricsArr addObject:[rs stringForColumn:@"fBMI_Metrics_Result"]];
		}
	}
    if (!haspoundArrData) {
        [poundsArr removeAllObjects];
    }
	DebugLOG([poundsArr description]);
    
    if (!hasmetricsArrData) {
        [metricsArr removeAllObjects];
    }
	DebugLOG([metricsArr description]);
	
	DebugLOG([db lastErrorMessage]);
	return [NSArray arrayWithObjects:poundsArr,metricsArr,nil];
}

- (NSArray	*) getClientsBodyFatPercentage :(NSString *) clientID {
	
	NSMutableArray * arr3sRes = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arr4sRes = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arr7sRes = [[[NSMutableArray alloc] init] autorelease];
	BOOL hasarr3sResData = NO;
    BOOL hasarr4sResData = NO;
    BOOL hasarr7sResData = NO;
    
	[self initFMDB];
	if(![db open]) {
	}
	else {
		NSString *qry=[NSString stringWithFormat:@"select fThree_Result,fFour_Result,fSeven_Result from FT%d_AssessmentSheet_Data",[clientID intValue]];
		FMResultSet *rs=[db executeQuery:qry];
		while ([rs next]) 
		{
            if ([[rs stringForColumn:@"fThree_Result"] floatValue] != 0) {
                hasarr3sResData = YES;
            }
			[arr3sRes addObject:[rs stringForColumn:@"fThree_Result"]];
            
            if ([[rs stringForColumn:@"fFour_Result"] floatValue] != 0) {
                hasarr4sResData = YES;
            }
			[arr4sRes addObject:[rs stringForColumn:@"fFour_Result"]];
            
            if ([[rs stringForColumn:@"fSeven_Result"] floatValue] != 0) {
                hasarr7sResData = YES;
            }
			[arr7sRes addObject:[rs stringForColumn:@"fSeven_Result"]];
		}
	}
    if (!hasarr3sResData) {
        [arr3sRes removeAllObjects];
    }
	DebugLOG([arr3sRes description]);
    
    if (!hasarr4sResData) {
        [arr4sRes removeAllObjects];
    }
	DebugLOG([arr4sRes description]);
    
    if (!hasarr7sResData) {
        [arr7sRes removeAllObjects];
    }
	DebugLOG([arr7sRes description]);
	DebugLOG([db lastErrorMessage]);
	return [NSArray arrayWithObjects:arr3sRes,arr4sRes,arr7sRes,nil];
}

- (NSArray	*) getClientscircumferenceMeasurement :(NSString *) clientID {
	NSMutableArray * arrRightUpperArm = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arrForeArm = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arrChest = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arrAbdominal = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arrHipButtocks = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arrRightThigh = [[[NSMutableArray alloc] init] autorelease];
	NSMutableArray * arrRightCalf = [[[NSMutableArray alloc] init] autorelease];
    
    BOOL hasarrRightUpperArmData = NO;
    BOOL hasarrForeArmData = NO;
    BOOL hasarrChestData = NO;
    BOOL hasarrAbdominalData = NO;
    BOOL hasarrHipButtocksData = NO;
    BOOL hasarrRightThighData = NO;
    BOOL hasarrRightCalfData = NO;
	
	[self initFMDB];
	if(![db open]) {
	}
	else {
		NSString *qry=[NSString stringWithFormat:@"select fCM_RightUpperArm,fCM_RightForeArm,fCM_Chest,fCM_Chest,fCM_Abdominal,fCM_HipButtocks,fCM_RightThing,fCM_RightCalf,fCM_Result from FT%d_AssessmentSheet_Data",[clientID intValue]];
		FMResultSet *rs=[db executeQuery:qry];
		while ([rs next]) 
		{	
            if ([[rs stringForColumn:@"fCM_RightUpperArm"] floatValue] != 0) {
                hasarrRightUpperArmData = YES;
            }
			[arrRightUpperArm addObject:[rs stringForColumn:@"fCM_RightUpperArm"]];
            
            if ([[rs stringForColumn:@"fCM_RightForeArm"] floatValue] != 0) {
                hasarrForeArmData = YES;
            }
			[arrForeArm addObject:[rs stringForColumn:@"fCM_RightForeArm"]]; 
            
            if ([[rs stringForColumn:@"fCM_Chest"] floatValue] != 0) {
                hasarrChestData = YES;
            }
			[arrChest addObject:[rs stringForColumn:@"fCM_Chest"]];
			
            if ([[rs stringForColumn:@"fCM_Abdominal"] floatValue] != 0) {
                hasarrAbdominalData = YES;
            }
            [arrAbdominal addObject:[rs stringForColumn:@"fCM_Abdominal"]];
			
            if ([[rs stringForColumn:@"fCM_HipButtocks"] floatValue] != 0) {
                hasarrHipButtocksData = YES;
            }
            [arrHipButtocks addObject:[rs stringForColumn:@"fCM_HipButtocks"]];
			
            if ([[rs stringForColumn:@"fCM_RightThing"] floatValue] != 0) {
                hasarrRightThighData = YES;
            }
            [arrRightThigh addObject:[rs stringForColumn:@"fCM_RightThing"]];
			
            if ([[rs stringForColumn:@"fCM_RightCalf"] floatValue] != 0) {
                hasarrRightCalfData = YES;
            }
            [arrRightCalf addObject:[rs stringForColumn:@"fCM_RightCalf"]];
		}
	}
    
    if (!hasarrRightUpperArmData) {
        [arrRightUpperArm removeAllObjects];
    }    
	DebugLOG([arrRightUpperArm description]);
    
    if (!hasarrForeArmData) {
        [arrForeArm removeAllObjects];
    } 
	DebugLOG([arrForeArm description]);
    
    if (!hasarrChestData) {
        [arrChest removeAllObjects];
    } 
	DebugLOG([arrChest description]);
    
    if (!hasarrAbdominalData) {
        [arrAbdominal removeAllObjects];
    }
	DebugLOG([arrAbdominal description]);
    
    if (!hasarrHipButtocksData) {
        [arrHipButtocks removeAllObjects];
    }
	DebugLOG([arrHipButtocks description]);
	
    if (!hasarrRightThighData) {
        [arrRightThigh removeAllObjects];
    }
    DebugLOG([arrRightThigh description]);
	
    if (!hasarrRightCalfData) {
        [arrRightCalf removeAllObjects];
    }
    DebugLOG([arrRightCalf description]);
	
	DebugLOG([db lastErrorMessage]);
	return [NSArray arrayWithObjects:arrRightUpperArm,arrForeArm,arrChest,arrAbdominal,arrHipButtocks,arrRightThigh,arrRightCalf,nil];
}


#pragma mark -
#pragma mark AssessmentView Deleting Methods

-(NSInteger)deleteAssessmentSheet:(NSInteger)sheetId forClientid:(NSString *)clientId {
    
    [self initFMDB];
    
    if ([db open]) {
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_AssessmentSheet where nSheetID = %ld",clientId,(long)sheetId];
        [db executeUpdate:query];
        str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}

//---------------------------------------------------------------------------------------------------------------

-(NSInteger)deleteAssessmentSheetDataForSheetId:(NSInteger)sheetId forClientid:(NSString*)clientId {
    
    [self initFMDB];
    if ([db open]) {
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_AssessmentSheet_Data where nSheetID = %ld",clientId,(long)sheetId];
        [db executeUpdate:query];
        str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
    
}

//---------------------------------------------------------------------------------------------------------------

-(NSInteger)deleteAssessmentImagesforSheetId:(NSInteger)sheetId forClient:(NSString*)clientId {
    [self initFMDB];
    if ([db open]) {
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_AssessmentSheet_ImagesData where nSheetID = %ld",clientId,(long)sheetId];
        [db executeUpdate:query];
        str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}
//---------------------------------------------------------------------------------------------------------------

#pragma mark : Update if Table Didnot update

//---------------------------------------------------------------------------------------------------------------

-(void)addNewFieldToClientTableIfDoesNotExist {
    
     [self initFMDB];
    if([db open]) {
        
        BOOL  isAdded = [db columnExists:@"FT_Client" columnName:@"sLastViewedPage"];
        NSString * query;
        if(!isAdded) {
            query = [NSString stringWithFormat:@"ALTER TABLE \"FT_Client\"  ADD COLUMN \"sLastViewedPage\" VARCHAR DEFAULT ''"];
            [db executeUpdate:query];
            NSString * str= [db lastErrorMessage];
            if(![str isEqualToString:@"not an error"]) {
            }
            else {
            }
    
        }
        BOOL isAddedColoumn  = [db columnExists:@"FT_TemplateSheet_BlockData" columnName:@"sColor"];
        
        if (!isAddedColoumn) {
            shouldUpdateTemplate = TRUE;
            query = [NSString stringWithFormat:@"ALTER TABLE \"FT_TemplateSheet_BlockData\" ADD COLUMN \"sColor\" INTEGER DEFAULT 0"];
            [db executeUpdate:query];
            
        }
        


    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)addNewFieldsInAssessmentTableForClient:(NSString *) clientId {
    
    
    BOOL  isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_AssessmentSheet_Data",clientId] columnName:@"fGenereal_FatPercent"];
     NSString * query;
    
    if (!isAdded) {
        query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_AssessmentSheet_Data\" ADD COLUMN \"fGenereal_FatPercent\" FLOAT DEFAULT 0",clientId];
        [db executeUpdate:query];
        
    }
    
    isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_AssessmentSheet_Data",clientId] columnName:@"fExtra_Value"];
    
    if (!isAdded) {
        query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_AssessmentSheet_Data\" ADD COLUMN \"fExtra_Value\" FLOAT DEFAULT 0",clientId];
        [db executeUpdate:query];
        
    }

    isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_AssessmentSheet_Data",clientId] columnName:@"sExtra_FieldName"];
    
    if (!isAdded) {
        query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_AssessmentSheet_Data\" ADD COLUMN \"sExtra_FieldName\" VARCHAR DEFAULT ''",clientId];
        [db executeUpdate:query];
        
    }
    
}


//========================================Program VC Methods Methods=================================================//

#pragma mark -
#pragma mark ProgramView Insert Methods

- (NSInteger) insertSheetBlocksForSheet:(NSString *)sheetId date:(NSString *)date blockNo:(NSString *) bNo clientId:(NSString *) cId blockTitle:(NSString *) title blockNotes:(NSString *) notes andBlockId:(NSString *)strBlockId {
    
    
    START_METHOD
	[self  initFMDB];
	
	if([db open])
	{
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString * query;
        
        if([strUpgradeStatus isEqualToString:@"YES"]){
            
             query = [NSString stringWithFormat:@"INSERT INTO FT%@_ProgramSheet_Blocks (nBlockID,nSheetID,dBlockdate,nBlockNo,sBlockTitle,sBlockNotes) values(%d,%d,'%@',%d,'%@','%@')",cId,[strBlockId intValue],[sheetId intValue],date,[bNo intValue],[self escapeString:title],notes];
            
        }else{
            query = [NSString stringWithFormat:@"INSERT INTO FT%@_ProgramSheet_Blocks (nSheetID,dBlockdate,nBlockNo,sBlockTitle,sBlockNotes) values(%d,'%@',%d,'%@','%@')",cId,[sheetId intValue],date,[bNo intValue],[self escapeString:title],notes];
        }
		
		DebugLOG(query);
		[db executeUpdate:query];
        
        
        MESSAGE(@"workout query : %@",query);
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
    
    END_METHOD
}

//--------------------------------------------------------------------------

- (int) insertSheetBlocksForSheet:(NSString *)sheetId date:(NSString *)date blockNo:(NSString *) bNo clientId:(NSString *) cId blockTitle:(NSString *) title {
    [self  initFMDB];
    
    if([db open])
    {
        title = [self escapeString:title];
        NSString * query = [NSString stringWithFormat:@"INSERT INTO FT%@_ProgramSheet_Blocks (nSheetID,dBlockdate,nBlockNo,sBlockTitle) values(%d,'%@',%d,'%@')",cId,[sheetId intValue],date,[bNo intValue],title];
        DebugLOG(query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }	
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (int) addNewSheet1:(NSString *) sheetName ForClient:(NSString *)cId {
    
    START_METHOD
    [self initFMDB];
    
    if (![db open]) {
        return 1;
    }
    else {
        sheetName = [self escapeString:sheetName];
        NSString * query = [NSString stringWithFormat:@"Insert into FT%@_ProgramSheet (sSheetName) values ('%@')",cId,sheetName];
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            NSString * sId = [self getsheetId:sheetName forClient:cId];
            return 0;
        }
    }
    END_METHOD
}


//--------------------------------------------------------------------------

- (int) addNewSheet:(NSString *) sheetName ForClient:(NSString *)cId {
    [self initFMDB];
    
    if (![db open]) {
        return 1;
    }
    else {
        sheetName = [self escapeString:sheetName];
        NSString * query = [NSString stringWithFormat:@"Insert into FT%@_ProgramSheet (sSheetName) values ('%@')",cId,sheetName];
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            NSString * sId = [self getsheetId:sheetName forClient:cId];
            for (int i = 0; i<1; i++) {
                int success = [self insertSheetBlocksForSheet:sId date:@"" blockNo:[NSString stringWithFormat:@"%d",i+1] clientId:cId blockTitle:@""];
                DebugINTLOG(@"success",success);
            }
            return 0;
        }	
    }
}

//--------------------------------------------------------------------------

- (int) insertSheetBlocksForSheet:(NSString *)sheetId :(NSString *)clientId :(NSDictionary *)dataDict {
	[self  initFMDB];
	START_METHOD
	if([db open])
	{
        NSString * strTitle = [self escapeString:[dataDict valueForKey:@"sBlockTitle"]];
		NSString * query = [NSString stringWithFormat:@"INSERT INTO FT%@_ProgramSheet_Blocks (nSheetID,dBlockdate,nBlockNo,sBlockTitle,sBlockNotes) values(%d,'%@',%d,'%@','%@')",clientId,[sheetId intValue],[dataDict valueForKey:@"dBlockdate"],[[dataDict valueForKey:@"nBlockNo"] intValue],strTitle,[dataDict valueForKey:@"sBlockNotes"]];
		DebugLOG(query);
        
                                    MESSAGE(@"insertSheetBlocksForSheet-> %@",query);
        
        
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}	
}

//--------------------------------------------------------------------------

- (int) addNewSheet:(NSString *) sheetName ForClient:(NSString *)cId ProgramPurpose:(NSString *)programPurpose withWorkoutName:(NSString *)workoutName ForBlockNo:(NSString *)blockNO {
    START_METHOD
    
    NSDateFormatter *formater = [[[NSDateFormatter alloc] init]autorelease];
    [formater setDateFormat:kAppDateFormat];
    NSString *result = [formater stringFromDate:[NSDate date]];
    
	[self initFMDB];
	if (![db open]) {	
        return 1;
	}	
	else {

        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString * query;
        
        if([strUpgradeStatus isEqualToString:@"YES"]){
            
            
            query = [NSString stringWithFormat:@"Insert into FT%@_ProgramSheet (nSheetID,sSheetName,sPurpose) values (%d,'%@','%@')",cId,blockNO.intValue, [self escapeString:sheetName],[self escapeString:programPurpose]];
        }else{
            	query = [NSString stringWithFormat:@"Insert into FT%@_ProgramSheet (sSheetName,sPurpose) values ('%@','%@')",cId, [self escapeString:sheetName],[self escapeString:programPurpose]];
        }
        
        MESSAGE(@"addNewSheet-> query: %@",query);
        
        
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		} else {
            
            
			NSString * sId = [self getsheetId:sheetName forClient:cId];
            
            
			for (int i = 0; i<1; i++) {
                
                  if(![strUpgradeStatus isEqualToString:@"YES"]){
                NSInteger success = [self insertSheetBlocksForSheet:sId date:result blockNo:blockNO clientId:cId blockTitle:workoutName blockNotes:@"" andBlockId:blockNO];
                  }
			}
			return 0;
		}	
	}
}

//--------------------------------------------------------------------------

- (NSInteger) insertProgramData:(NSDictionary *)dic forClient:(NSString *) cId {
    START_METHOD
	[self  initFMDB];
	
	if([db open])
	{
        NSString * strExcercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        NSString * strSetValue = [self escapeString:[dic objectForKey:kSetValue]];

        //Sunil
        
        NSString * strRestTimeValue = [self escapeString:[dic objectForKey:kEXDate]];
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString * query;
        
        if([strUpgradeStatus isEqualToString:@"YES"]){
            
            //
            query = [NSString stringWithFormat:@"insert into FT%@_ProgramSheet_BlockData (nBlockDataID,nBlockID,nSheetID,sProgram,sLBValue,sRepValue,sSetValue,nBlockNo,nRowNo,sDate,sColor) values(%d, %d,%d,'%@','%@','%@','%@',%d,%d,'%@',%d)",cId,
                     [[dic objectForKey:kBlockDataId] intValue],
                     [[dic objectForKey:kBlockId] intValue],
                     [[dic objectForKey:kSheetId] intValue],
                     strExcercise,strLbValue,strRepValue,strSetValue,
                     [[dic objectForKey:kBlockNo] intValue],
                     [[dic objectForKey:kRowNo] intValue],
                     strRestTimeValue,
                     [[dic objectForKey:kColor] intValue]
                     ];
            
        }else{
		 query = [NSString stringWithFormat:@"insert into FT%@_ProgramSheet_BlockData (nBlockID,nSheetID,sProgram,sLBValue,sRepValue,sSetValue,nBlockNo,nRowNo,sDate,sColor) values(%d,%d,'%@','%@','%@','%@',%d,%d,'%@',%d)",cId,
							[[dic objectForKey:kBlockId] intValue],
							[[dic objectForKey:kSheetId] intValue],
							 strExcercise,strLbValue,strRepValue,strSetValue,
							[[dic objectForKey:kBlockNo] intValue],
							[[dic objectForKey:kRowNo] intValue],
							strRestTimeValue,
                            [[dic objectForKey:kColor] intValue]
							];
        }
		[db executeUpdate:query];
        
        MESSAGE(@"insert exercise query: %@",query);
        
		NSString * str= [db lastErrorMessage];
		DebugLOG(query);
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	return 1;
}

//--------------------------------------------------------------------------

- (NSInteger) insertProgramData:(NSDictionary *)dic forClient:(NSString *) cId :(NSString *)sheetId{
	[self  initFMDB];
    if([db open])
	{
        NSString * strExcercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        NSString * strSetValue = [self escapeString:[dic objectForKey:kSetValue]];

		NSString * query = [NSString stringWithFormat:@"insert into FT%@_ProgramSheet_BlockData (nBlockID,nSheetID,sProgram,sLBValue,sRepValue,sSetValue,nBlockNo,nRowNo,sDate,sColor) values(%d,%d,'%@','%@','%@','%@',%d,%d,'%@',%d)",cId,
							[[dic objectForKey:kBlockId] intValue],
							[sheetId intValue],
							 strExcercise,strLbValue,strRepValue,strSetValue,
							[[dic objectForKey:kBlockNo] intValue],
							[[dic objectForKey:kRowNo] intValue],
							[dic objectForKey:kEXDate],
                            [[dic objectForKey:kColor] intValue]
							];
		
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		DebugLOG(query);
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	return 1;
}

//--------------------------------------------------------------------------

#pragma mark -
#pragma mark ProgramView Update Methods

- (NSInteger) updateSheetName:(NSString *) sheetname forClient:(NSString *)cID sheetId:(NSString *) sId {
	[self  initFMDB];
	
	if([db open])
	{
        sheetname = [self escapeString:sheetname];
		NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet set sSheetName = '%@' where nSheetID = %d",cID,sheetname,[sId intValue]];
		
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger) updateBlockName:(NSString *) blockTitle forClient:(NSString *)cID sheetId:(NSString *) sId blockId:(NSString *)blockID {
    [self  initFMDB];
    
    if([db open])
    {
        blockTitle = [self escapeString:blockTitle];
        
        //Sunil ->
//        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set sBlockTitle = '%@' , sBlockNotes = '  where nBlockNo = %d and nSheetID = %d and sBlockNotes=''" ,cID,blockTitle,@"",[blockID intValue],[sId intValue]];
        
        //Sunil Add and sBlockTitle!='' remove : , sBlockNotes = '%@' No neeed to set notes
        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set sBlockTitle = '%@' where nBlockNo = %d and nSheetID = %d  and sBlockTitle!=''" ,cID,blockTitle,[blockID intValue],[sId intValue]];
        
        
        MESSAGE(@"Query update  workout name: %@",query);
        ///Sunil <-
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (int) updateBlockNameForWorkout:(NSString *) blockTitle forClient:(NSString *)cID sheetId:(NSString *) sId blockId:(NSString *)blockID {
    START_METHOD
    [self  initFMDB];
    
    if([db open])
    {
        blockTitle = [self escapeString:blockTitle];
        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set sBlockTitle = '%@' where nBlockID = %d and nSheetID = %d",cID,blockTitle,[blockID intValue],[sId intValue]];
        
        MESSAGE(@"query---> updateBlockNameForWorkout: %@",query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}
//--------------------------------------------------------------------------

- (int) updateProgramData:(NSDictionary *)dic forClient:(NSString *) cId {
    
    START_METHOD
    MESSAGE(@"dctionry for dab -> %@",dic);
	[self  initFMDB];
	
	if([db open])
	{
        
        NSString * strExercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        NSString * strSetValue = [self escapeString:[dic objectForKey:@"sSetValue"]];
        
        //Sunil
         NSString * strRestTimeValue = [self escapeString:[dic objectForKey:kEXDate]];
        
        
		NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_BlockData set nBlockID = %d,nSheetID = %d,sProgram = '%@',sLBValue = '%@',sRepValue = '%@',nBlockNo = %d,nRowNo = %d,sDate = '%@',sSetValue = '%@',sColor = %d  Where nBlockNo = %d and nRowNo = %d and nSheetID = %d",cId,[[dic objectForKey:kBlockId] intValue],[[dic objectForKey:kSheetId] intValue],strExercise,strLbValue,strRepValue,[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],   strRestTimeValue  ,strSetValue,[[dic valueForKey:kColor]intValue ],[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],[[dic objectForKey:kSheetId] intValue]];
		DebugLOG(query);
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
        strExercise = nil;
        strLbValue = nil;
        strRepValue = nil;
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
	
}

//--------------------------------------------------------------------------

- (int) updateProgramData:(NSDictionary *)dic forClient:(NSString *)cId :(NSString *)sheetId {
	[self  initFMDB];
	if([db open])
	{
        NSString * strExcercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        
		NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_BlockData set nBlockID = %d,nSheetID = %d,sProgram = '%@',sLBValue = '%@',sRepValue = '%@',nBlockNo = %d,nRowNo = %d,sDate = '%@',sColor = %d Where nBlockNo = %d and nRowNo = %d and nSheetID = %d",cId,[[dic objectForKey:kBlockId] intValue],[sheetId intValue],strExcercise,strLbValue,strRepValue,[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],[dic objectForKey:kEXDate],[[dic valueForKey:kColor]intValue ],[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],[[dic objectForKey:kSheetId] intValue]];
		DebugLOG(query);
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (NSInteger) updateNotes:(NSString *)Notes blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId {
    [self  initFMDB];
    
    if([db open])
    {
        if(Notes == NULL){
            Notes = @"";
        }
        if(title == NULL){
            title = @"";
        }
        
        title = [self escapeString:title];
        
        
        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set sBlockNotes = '%@' where nBlockNo = %d and nSheetID = %d" ,cId,Notes,[blockId intValue],[title intValue]];
        DebugSTRLOG(@"query:",query);
        
        MESSAGE(@"updateNotes->query %@",query);
        
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        DebugSTRLOG(@"Error:",str);
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}



- (NSInteger) updateNotes:(NSString *)Notes blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId andexistingNotes: (NSString *)str {
    [self  initFMDB];
    
    if([db open])
    {
        if(Notes == NULL){
            Notes = @"";
        }
        if(title == NULL){
            title = @"";
        }
        
        title = [self escapeString:title];
        
        
        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set sBlockNotes = '%@' where nBlockNo = %d and nSheetID = %d and sBlockNotes='%@' " ,cId,Notes,[blockId intValue],[title intValue],str];
        DebugSTRLOG(@"query:",query);
        
        MESSAGE(@"updateNotes->query %@",query);
        
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        DebugSTRLOG(@"Error:",str);
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}

//--------------------------------------------------------------------------

- (int) updateDate1:(NSString *)date blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId sheetID:(NSString *)sheedID {
    [self  initFMDB];
    
    if([db open])
    {
        if(date == NULL){
            date = @"";
        }
        if(title == NULL){
            title = @"";
        }
        
        title = [self escapeString:title];
        
        
            NSString * query = [NSString stringWithFormat:@"INSERT INTO FT%@_ProgramSheet_Blocks (nSheetID,dBlockdate,nBlockNo,sBlockTitle) values(%d,'%@',%d,'%@')",cId,[sheedID intValue],date,[blockId  intValue],title];
        
        DebugSTRLOG(@"query:",query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        DebugSTRLOG(@"Error:",str);
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}

//--------------------------------------------------------------------------

- (NSInteger) updateDate:(NSString *)date blockTitle:(NSString *) title ForBlock:(NSString *)blockId forClient:(NSString *)cId {
	[self  initFMDB];
	
	if([db open])
	{
        if(date == NULL){
            date = @"";
        }
        if(title == NULL){
            title = @"";
        }
        
        title = [self escapeString:title];

        
       NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set dBlockdate = '%@' where nBlockNo = %d and nSheetID = %d" ,cId,date,[blockId intValue],[title intValue]];
		DebugSTRLOG(@"query:",query);
        
        MESSAGE(@"query - insert workout: %@",query);
        
        
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		DebugSTRLOG(@"Error:",str);
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
}

//--------------------------------------------------------------------------

- (int) updateSheetsStatusForSheetID:(NSString *)sheetId to:(int)status forClientID: (NSString *) clientID {
	[self  initFMDB];
	
	if([db open])
	{
		NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet set nUpdateStatus = %d where nSheetID=%d",clientID,status,[sheetId intValue]];
		DebugSTRLOG(@"query:",query); 
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		DebugSTRLOG(@"Error:",str);
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
}

//--------------------------------------------------------------------------

#pragma mark -
#pragma mark ProgramView Select Methods


- (NSArray *) selectProgramDataForClient:(NSString *)clientID :(NSString *)sheetId{
	NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_BlockData where nSheetID = %d ORDER BY nBlockNo,nRowNo",clientID,[sheetId intValue]];		
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
                                   [rs stringForColumn:@"sSetValue"],kSetValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
                                  [rs stringForColumn:@"sColor"],kColor,
								  [rs stringForColumn:@"sDate"],kEXDate,nil];
			[programData addObject:dic];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)programData;	
}

//--------------------------------------------------------------------------

- (NSArray *) selectProgramBlocksForClient:(NSString *)clientID :(NSString *)sheetId {
	NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT DISTINCT nBlockNo FROM FT%@_ProgramSheet_BlockData where nSheetID = %d ORDER BY nBlockNo,nRowNo",clientID,[sheetId intValue]];		
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			[programData addObject:[rs stringForColumn:@"nBlockNo"]];
		}			
	}	
    DebugSTRLOG(@"programData:- ", [programData description]);
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)programData;
}

//--------------------------------------------------------------------------

- (NSArray *) selectProgramDataForClient:(NSString *)cId sheet:(NSString *)sheet blockNo:(NSString *)bNo {
	NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_BlockData where nSheetID = %d AND nBlockNo = %d ORDER BY nRowNo",cId,[sheet intValue],[bNo intValue]];
		MESSAGE(@"selectProgramDataForClient query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
                                  [rs stringForColumn:@"sSetValue"],kSetValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
								  [rs stringForColumn:@"sDate"],kEXDate,
                                  [rs stringForColumn:@"sColor"],kColor,nil];
			[programData addObject:dic];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)programData;
}

//--------------------------------------------------------------------------

- (NSArray *) getBlockDataForClient:(NSString *)cId SheetId:(NSString *)sheetId :(NSString *)blockno{
    NSMutableArray * blockData = [[[NSMutableArray alloc] init] autorelease];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %d and nBlockNo = %d ORDER BY nBlockNo",cId,[sheetId intValue],[blockno intValue]];
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSString *strBlobkNotes = [rs stringForColumn:@"sBlockNotes"];
            NSString *strBlobkTitle = [rs stringForColumn:@"sBlockTitle"];
            
            if ([[rs stringForColumn:@"sBlockNotes"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkNotes = @"";
            }
            if ([[rs stringForColumn:@"sBlockTitle"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkTitle = @"";
            }
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [rs stringForColumn:@"nBlockID"],kBlockId,
                                  [rs stringForColumn:@"nSheetID"],kSheetId,
                                  [rs stringForColumn:@"dBlockdate"],kBlockDate,
                                  [rs stringForColumn:@"nBlockNo"],kBlockNo,
                                  strBlobkNotes,kBlockNotes,
                                  strBlobkTitle,kBlockTitle,nil];
            [blockData addObject:dic];
        }
    }	
    
    DebugLOG([db lastErrorMessage]);
    return (NSArray *)blockData;
}

//--------------------------------------------------------------------------


- (NSArray *) getBlockDataForClient:(NSString *)cId SheetId:(NSString *)sheetId {
    START_METHOD
	NSMutableArray * blockData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {
        // SUNIL add :  and sBlockTitle!='' in query
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %d and sBlockTitle!='' ORDER BY nBlockNo",cId,[sheetId intValue]];
		FMResultSet * rs = [db executeQuery:query];
		
        
        MESSAGE(@"query select workout: %@",query);
        
        
		while ([rs next]) {
            NSString *strBlobkNotes = [rs stringForColumn:@"sBlockNotes"];
            NSString *strBlobkTitle = [rs stringForColumn:@"sBlockTitle"];

            if ([[rs stringForColumn:@"sBlockNotes"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                 strBlobkNotes = @"";
            }
            if ([[rs stringForColumn:@"sBlockTitle"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkTitle = @"";
            }
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"dBlockdate"],kBlockDate,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
                                  strBlobkNotes,kBlockNotes,
								  strBlobkTitle,kBlockTitle,nil];
			[blockData addObject:dic];
		}
	}	
	
	DebugLOG([db lastErrorMessage]);
      MESSAGE(@"query select workout (NSArray *)blockData: %@",(NSArray *)blockData);
	return (NSArray *)blockData;
}



//SUNIL for get data on view aall notes for shoe detail option
- (NSArray *) getNotesDataForClient:(NSString *)cId SheetId:(NSString *)sheetId {
    NSMutableArray * blockData = [[[NSMutableArray alloc] init] autorelease];
    if (![db open]) {
    }
    else {
        // SUNIL add :  and sBlockTitle!='' in query
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %d ORDER BY nBlockNo",cId,[sheetId intValue]];
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSString *strBlobkNotes = [rs stringForColumn:@"sBlockNotes"];
            NSString *strBlobkTitle = [rs stringForColumn:@"sBlockTitle"];
            
            if ([[rs stringForColumn:@"sBlockNotes"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkNotes = @"";
            }
            if ([[rs stringForColumn:@"sBlockTitle"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkTitle = @"";
            }
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [rs stringForColumn:@"nBlockID"],kBlockId,
                                  [rs stringForColumn:@"nSheetID"],kSheetId,
                                  [rs stringForColumn:@"dBlockdate"],kBlockDate,
                                  [rs stringForColumn:@"nBlockNo"],kBlockNo,
                                  strBlobkNotes,kBlockNotes,
                                  strBlobkTitle,kBlockTitle,nil];
            [blockData addObject:dic];
        }
    }	
    
    DebugLOG([db lastErrorMessage]);
    return (NSArray *)blockData;
}


//--------------------------------------------------------------------------




//--------------------------------------------------------------------------

- (NSArray *) selectProgramDataForClient:(NSString *)cId sheet:(NSString *)sheet{
	NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_BlockData where nSheetID = %d ",cId,[sheet intValue]];
		DebugSTRLOG(@"Query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
                                   [rs stringForColumn:@"sSetValue"],kSetValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
								  [rs stringForColumn:@"sDate"],kEXDate,
                                  [rs stringForColumn:@"sColor"],kColor,nil];
            
			[programData addObject:dic];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)programData;
}

//--------------------------------------------------------------------------

- (int) getCountofProgramSheetForClient:(NSString *)cId {
	int i = 0;
	[self initFMDB];	
	if (![db open]) {		
	}	
	else {
		NSString * query = [NSString stringWithFormat:@"SELECT count(*) as count from FT%@_ProgramSheet",cId];
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			i = [rs intForColumn:@"count"];
		}
	}
	return i;
}
//--------------------------------------------------------------------------

- (NSString *) getsheetId:(NSString *) sheetName forClient:(NSString *)cId {
	[self initFMDB];
	NSString * sheetId = @"";
	if (![db open]) {		
	}	
	else {
     //   sheetName = [self escapeString:sheetName];
		NSString * query = [NSString stringWithFormat:@"SELECT * from FT%@_ProgramSheet where sSheetName= '%@'",cId,[self escapeString:sheetName]];
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			sheetId = [NSString stringWithFormat:@"%d",[rs intForColumn:@"nSheetID"]];
		}
	}
	return [NSString stringWithFormat:@"%@",sheetId];
}

//-----------------------------------------------------------------------

- (NSArray *) getProgramSheetList:(NSString *)cId {
	NSMutableArray * sheetList = [[[NSMutableArray alloc] init] autorelease];
    if([cId length] > 0){
        if (![db open]) {
        }
        else {
            NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet",cId];
            FMResultSet * rs = [db executeQuery:query];
            
            while ([rs next]) {
                NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [rs stringForColumn:@"nSheetID"],ksheetID,
                                      [rs stringForColumn:@"sSheetName"],ksheetName,
                                      [rs stringForColumn:@"sPurpose"],ksProgramPurpose,nil];
                [sheetList addObject:dic];
            }			
        }	
        DebugLOG([db lastErrorMessage]);
    }
	
	return (NSArray *)sheetList;
}
//--------------------------------------------------------------------------

- (NSDictionary *) getEmailAddressesofClient : (NSString *) clientID {
	NSDictionary * dictInfo = [[[NSDictionary alloc] init] autorelease];
	
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT SEmailAddress,sEmailAddress2 FROM FT_Client where nClientID=%d",[clientID intValue]];	
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"SEmailAddress"],kEmail1,
								  [rs stringForColumn:@"sEmailAddress2"],kEmail2,nil];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return dictInfo;
}
//--------------------------------------------------------------------------

-(int)getBlockIdForClient:(NSString *)sheetId :(NSString *)blockNo :(NSString *)clientID{
    int temp = 0;
	
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT nBlockID FROM FT%@_ProgramSheet_Blocks where nSheetID=%d AND nBlockNo = %d",clientID,[sheetId intValue],[blockNo intValue]];	
		FMResultSet * rs = [db executeQuery:query]; 
		
		while ([rs next]) {
			temp = [[rs stringForColumn:@"nBlockID"] intValue];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return temp;
}
//--------------------------------------------------------------------------

- (int) getSheetsStatusForSheetID:(NSString *)sheetId forClientID: (NSString *) clientID {
	
	int status = 0;
	
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT nUpdateStatus FROM FT%@_ProgramSheet where nSheetID=%d",clientID,[sheetId intValue]];	
		FMResultSet * rs = [db executeQuery:query]; 
		
		while ([rs next]) {
			status = [[rs stringForColumn:@"nUpdateStatus"] intValue];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return status;
}
//--------------------------------------------------------------------------

- (BOOL) isBlockHavingData:(NSString *)cId sheet:(NSString *)sheet blockNo:(NSString *)bNo {
    
	NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
        
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_BlockData where nSheetID = %d AND nBlockNo = %d ORDER BY nRowNo",cId,[sheet intValue],[bNo intValue]];
		DebugSTRLOG(@"Query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
								  [rs stringForColumn:@"sDate"],kEXDate,nil];
			[programData addObject:dic];
		}			
	}	
    if([programData count] > 0){
        return YES;
    }else{
        return NO;
    }
}

//--------------------------------------------------------------------------

#pragma mark -
#pragma mark ProgramView Delete Methods

- (int) deleteSheet:(NSString *)sheetId forClient:(NSString *)clientId {
    [self initFMDB];	
	if (![db open]) {		
	}	
	else {	
		NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d",clientId,[sheetId intValue]];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		} else {
            query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_Blocks where nSheetID = %d",clientId,[sheetId intValue]];
            [db executeUpdate:query];
            str= [db lastErrorMessage];
            if (![str isEqualToString:@"not an error"]) {
                return 1;
            } else {
                query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet where nSheetID = %d",clientId,[sheetId intValue]];
                [db executeUpdate:query];
                str= [db lastErrorMessage];
                if (![str isEqualToString:@"not an error"]) {
                    return 1;
                }
            }
        }
    }
	return 0;
}

//---------------------------------------------------------------------------------------------------------------

-(int)deleteWorkoutForSheet:(NSString*)sheetId forClient:(NSString *)clientId andWorkOutBlockId:(NSString*)blockId {
    [self initFMDB];
    if ([db open]) {
        
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d and nBlockId = %d",clientId,[sheetId intValue],[blockId intValue]];
        // DLOG(@"query To delete block data %@",query);
        
        [db executeUpdate:query];
        str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        } else {
            query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_Blocks where nSheetID = %d and nBlockNo = %d",clientId,[sheetId intValue],[blockId intValue]];
            [db executeUpdate:query];
            str= [db lastErrorMessage];
            if (![str isEqualToString:@"not an error"]) {
                return 1;
            }
      }
    }
    return 0;
}


//---------------------------------------------------------------------------------------------------------------

-(int)deleteWorkOutBlockDataForSheet:(NSString*)sheetId forClient:(NSString *)clientId andWorkoutBlockId:(NSString*) blockId {
    
    [self initFMDB];
    if([db open]) {
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d and nBlockId = %d",clientId,[sheetId intValue],[blockId intValue]];
        // DLOG(@"query To delete block data %@",query);
        
        [db executeUpdate:query];
        str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;

}

//---------------------------------------------------------------------------------------------------------------

- (NSInteger) deleteBlockData : (NSString *)clientID :(NSString *)sheetID {

	[self initFMDB];	
	if (![db open]) {		
	}	
	else {	
		NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d",clientID,[sheetID intValue]];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
			}
	return 0;
}

//--------------------------------------------------------------------------


- (int) deleteBlocks : (NSString *)clientID :(NSString *)sheetID {

	[self initFMDB];	
	if (![db open]) {		
	}	
	else {	
		NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_Blocks where nSheetID = %d",clientID,[sheetID intValue]];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
    }
	return 0;
}

//--------------------------------------------------------------------------


- (NSArray *) getBlock:(NSString *)cId SheetId:(NSString *)sheetId :(NSString *)blockID{
	NSMutableArray * blockData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %d and nBlockNo = %d ORDER BY nBlockNo",cId,[sheetId intValue],[blockID intValue]];	
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"dBlockdate"],kBlockDate,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"sBlockTitle"],kBlockTitle,nil];
			[blockData addObject:dic];
		}
	}	
	
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)blockData;
}

//--------------------------------------------------------------------------

- (NSArray *) selectProgramDataForClient:(NSString *)cId sheet:(NSString *)sheet :(NSString *)blockID{
	NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
        DebugINTLOG(@"%d",[blockID intValue]);
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_BlockData where nSheetID = %d and nBlockNo=%d  ORDER BY nRowNo",cId,[sheet intValue],[blockID intValue]];
		DebugSTRLOG(@"Query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
								  [rs stringForColumn:@"sDate"],kEXDate,nil];
			[programData addObject:dic];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)programData;
}

//--------------------------------------------------------------------------

- (int) deleteBlockData : (NSString *)clientID :(NSString *)sheetID :(NSString *)blockNO :(NSString *)rowNO {

    [self initFMDB];
    if (![db open]) {
    }
    else {
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d and nBlockNo = %d and nRowNo = %d",clientID,[sheetID intValue],[blockNO intValue],[rowNO intValue]];
        [db executeUpdate:query];
        str= [db lastErrorMessage];
        
        
        MESSAGE(@"delte query for exercise: %@",query);
        
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}

//--------------------------------------------------------------------------

- (int) deleteBlockData : (NSString *)clientID :(NSString *)sheetID :(NSString *)blockNO {

	[self initFMDB];	
	if (![db open]) {		
	}	
	else {	
		NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d and nBlockNo = %d",clientID,[sheetID intValue],[blockNO intValue]];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
    }
	return 0;
}

//--------------------------------------------------------------------------

- (int) insertProgramData:(NSDictionary *)dic forClient:(NSString *)cId :(NSString *)sheetId :(NSString *)blockID{
	[self  initFMDB];
    
	if([db open]){
        
        NSString * strExercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        NSString * strSetValue = [self escapeString:[dic objectForKey:kSetValue]];
        
		NSString * query = [NSString stringWithFormat:@"insert into FT%@_ProgramSheet_BlockData (nBlockID,nSheetID,sProgram,sLBValue,sRepValue,sSetValue,nBlockNo,nRowNo,sDate,sColor) values(%d,%d,'%@','%@','%@','%@',%d,%d,'%@','%d')",cId,
							[blockID intValue],
							[sheetId intValue],
							strExercise,strLbValue,strRepValue,strSetValue,
							[blockID intValue],
							[[dic objectForKey:kRowNo] intValue],
							[dic objectForKey:kEXDate],
                            [[dic objectForKey:kColor] intValue]
							];
		
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		DebugLOG(query);
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	return 1;
}

//--------------------------------------------------------------------------

- (BOOL) insertProgramDataWith_Block:(NSDictionary *)dic forClient:(NSString *)cId :(NSString *)sheetId :(NSString *)blockID :(int)blockNo{
    
	[self  initFMDB];
	
	if([db open]){
        
        NSString * strExercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        NSString * strSetValue = [self escapeString:[dic objectForKey:kSetValue]];

        
		NSString * query = [NSString stringWithFormat:@"insert into FT%@_ProgramSheet_BlockData (nBlockID,nSheetID,sProgram,sLBValue,sRepValue,sSetValue,nBlockNo,nRowNo,sDate,sColor) values(%d,%d,'%@','%@','%@','%@',%d,%d,'%@','%d')",cId,
							[blockID intValue],
							[sheetId intValue],
							strExercise,strLbValue,strRepValue,strSetValue,
							blockNo,
							[[dic objectForKey:kRowNo] intValue],
							[dic objectForKey:kEXDate],
                            [[dic objectForKey:kColor] intValue]
							];
		
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		DebugLOG(query);
		if(![str isEqualToString:@"not an error"]) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	return TRUE;
}

//--------------------------------------------------------------------------

- (int) deleteBlocks : (NSString *)clientID :(NSString *)sheetID :(NSString *)blockID {

	[self initFMDB];	
	if (![db open]) {		
	}	
	else {	
		NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_Blocks where nSheetID = %d and nBlockNo= %d",clientID,[sheetID intValue],[blockID intValue]];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
    }
	return 0;
}
//--------------------------------------------------------------------------

- (int) insertSheetBlocksForSheet:(NSString *)sheetId :(NSString *)clientId :(NSDictionary *)dataDict :(NSString *)nBlockID {
	[self  initFMDB];
	
	if([db open])
	{
        NSString * strBlockTitle = [self escapeString:[dataDict valueForKey:@"sBlockTitle"]];
		NSString * query = [NSString stringWithFormat:@"INSERT INTO FT%@_ProgramSheet_Blocks (nSheetID,dBlockdate,nBlockNo,sBlockTitle) values(%d,'%@',%d,'%@')",clientId,[sheetId intValue],[dataDict valueForKey:@"dBlockdate"],[nBlockID  intValue],strBlockTitle];
		DebugLOG(query);
        MESSAGE(@"insertSheetBlocksForSheet: query-----> %@",query);
        
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}	
}


// ---------------------Notes View methods--------------------------------------------------------------------
-(int) updateNotes:(NSDictionary *)dic forClient:(NSString *)cid{
    [self  initFMDB];
	
	if([db open])
	{

        NSString * query = [NSString stringWithFormat:@"Update FT%@_Notes set %@ = '%@', %@ = '%@', %@ = '%@', %@ = '%@', %@ = '%@'",cid,kSectionToAddNotes,[dic valueForKey:kSectionToAddNotes],kMedicalInformation,[dic valueForKey:kMedicalInformation],kExerciseHistory,[dic valueForKey:kExerciseHistory],kClientGoals,[dic valueForKey:kClientGoals],kPersonalRecordsAndMore,[dic valueForKey:kPersonalRecordsAndMore]];
		DebugLOG(query);
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}	 
}

//--------------------------------------------------------------------------

-(int) insertNotes:(NSDictionary *)dic forClient:(NSString *)cid{
    [self  initFMDB];
	
	if([db open])
	{
        NSString * query = [NSString stringWithFormat:@"insert into FT%@_Notes (%@,%@,%@,%@,%@) values('%@','%@','%@','%@','%@')",cid,kSectionToAddNotes,kMedicalInformation,kExerciseHistory,kClientGoals,kPersonalRecordsAndMore,[dic valueForKey:kSectionToAddNotes],[dic valueForKey:kMedicalInformation],[dic valueForKey:kExerciseHistory],[dic valueForKey:kClientGoals],[dic valueForKey:kPersonalRecordsAndMore]];
		DebugLOG(query);
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}	 
}
//--------------------------------------------------------------------------

-(NSDictionary *) selectNotes:(NSString *)cid{
    NSDictionary * dic = nil;
    if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_Notes",cid];	
		FMResultSet * rs = [db executeQuery:query];
		while ([rs next]) {
			dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:kSectionToAddNotes],kSectionToAddNotes,
								  [rs stringForColumn:kMedicalInformation],kMedicalInformation,
								  [rs stringForColumn:kExerciseHistory],kExerciseHistory,
								  [rs stringForColumn:kClientGoals],kClientGoals,
								  [rs stringForColumn:kPersonalRecordsAndMore],kPersonalRecordsAndMore,nil];
			
		}			
	}	
	
	DebugLOG([db lastErrorMessage]);
	return dic;
}

//--------------------Create tables if not exists-------------------------------------------------------------
//create table if not exists TableName
-(void)addNewFieldsToTablesIfnotAddedForClientId:(NSString*)clientID{
START_METHOD
    [self  initFMDB];
    if([db open])
    {
        NSString * query;
        if (clientID.length == 0) {

            
            BOOL isAdded = [db columnExists:@"FT_TemplateSheet_BlockData" columnName:@"sSetValue"];
           
            if (!isAdded) {
                shouldUpdateTemplate = TRUE;
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT_TemplateSheet_BlockData\" ADD COLUMN \"sSetValue\" VARCHAR DEFAULT ''"];
                [db executeUpdate:query];
    
            }
                      
            if( shouldUpdateTemplate) {
              [self updateTemplateDefaultData];
            }
            
            BOOL isAdded1 = [db columnExists:@"FT_TemplateSheet_BlockData" columnName:@"sColor"];
            
            if (!isAdded1) {
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT_TemplateSheet_BlockData\" ADD COLUMN \"sColor\" INTEGER DEFAULT 0"];
                [db executeUpdate:query];
                 [self updateTemplateDefaultData];
                         
                
            }

            
            
        }
        else{
            
            BOOL  isAdded = [db columnExists:@"FT_Client" columnName:@"sLastViewedPage"];
            if(!isAdded) {
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT_Client\"  ADD COLUMN \"sLastViewedPage\" VARCHAR DEFAULT ''"];
                [db executeUpdate:query];
            }
            
            
             isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_ProgramSheet",clientID] columnName:@"sPurpose"];
             shouldUpdateBlockData = FALSE;
             shouldUpdateBlocks = FALSE;
 
            if (!isAdded) {
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_ProgramSheet\" ADD COLUMN \"sPurpose\" VARCHAR DEFAULT ''",clientID];
                [db executeUpdate:query];
                
            }
            
            isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_ProgramSheet_BlockData",clientID] columnName:@"sSetValue"];
            
            if (!isAdded) {
                shouldUpdateBlockData = TRUE;
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_ProgramSheet_BlockData\" ADD COLUMN \"sSetValue\" VARCHAR DEFAULT ''",clientID];
                [db executeUpdate:query];
                
            }
            
            
            isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_ProgramSheet_BlockData",clientID] columnName:@"sColor"];
            
            if (!isAdded) {
                shouldUpdateBlockData = TRUE;
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_ProgramSheet_BlockData\" ADD COLUMN \"sColor\" INTEGER DEFAULT 0 ",clientID];
                [db executeUpdate:query];
                
            }
            
             isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_ProgramSheet_Blocks",clientID] columnName:@"sBlockNotes"];
            
             if (!isAdded) {
                 shouldUpdateBlocks = TRUE;
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_ProgramSheet_Blocks\" ADD COLUMN \"sBlockNotes\" VARCHAR DEFAULT ''",clientID];
                [db executeUpdate:query];
                
             }
            
             isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_AssessmentSheet_Data",clientID] columnName:@"fGenereal_FatPercent"];
            
                if (!isAdded) {
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_AssessmentSheet_Data\" ADD COLUMN \"fGenereal_FatPercent\" FLOAT DEFAULT 0",clientID];
                [db executeUpdate:query];
                
                 }
            
            isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_AssessmentSheet_Data",clientID] columnName:@"fExtra_Value"];
            
            if (!isAdded) {
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_AssessmentSheet_Data\" ADD COLUMN \"fExtra_Value\" FLOAT DEFAULT 0",clientID];
                [db executeUpdate:query];
                
                }
            
            isAdded = [db columnExists:[NSString stringWithFormat:@"FT%@_AssessmentSheet_Data",clientID] columnName:@"sExtra_FieldName"];
            
            if (!isAdded) {
                query = [NSString stringWithFormat:@"ALTER TABLE \"FT%@_AssessmentSheet_Data\" ADD COLUMN \"sExtra_FieldName\" VARCHAR DEFAULT ''",clientID];
                [db executeUpdate:query];
                
              }
            
            if(shouldUpdateBlocks || shouldUpdateBlockData ) {
                 [self updateDefaultDataIfColumnValueDoesnotExistsForClientID:clientID];
            }
          

        }
    }

}
//-----------------------------------------------------------------------

-(void)updateDefaultDataIfColumnValueDoesnotExistsForClientID:(NSString*)clientID{
    
    if(shouldUpdateBlocks) {
        NSArray *arrayBlocksToUpdate  = [self getBlockDataForClientID:clientID];
        
        for (NSDictionary *dictBlockToUpdate in arrayBlocksToUpdate) {
            
            NSString *strBlockTitle = [dictBlockToUpdate valueForKey:kBlockTitle];
            NSString *strDate = [dictBlockToUpdate valueForKey:kBlockDate];
            
            BOOL hasChaged = NO;
            if (strBlockTitle.length == 0) {
                hasChaged = true;
                strBlockTitle = [NSString stringWithFormat:@"Workout-%@",[dictBlockToUpdate valueForKey:kBlockNo]];
            }
            if (strDate.length == 0) {
                hasChaged = true;
                NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
                [formatter setDateFormat:kAppDateFormat];
                strDate = [formatter stringFromDate:[NSDate date]];
            }
            
            if (hasChaged) {
                NSMutableDictionary *dictToUpdate = [[[NSMutableDictionary alloc]initWithDictionary:dictBlockToUpdate] autorelease];
                [dictToUpdate setObject:strBlockTitle forKey:kBlockTitle];
                [dictToUpdate setObject:strDate forKey:kBlockDate];
                [self updateBlockNameForBlock:dictToUpdate atClientID:clientID];
                dictToUpdate = nil;
            }
            
        }

    }
   
    
    if(shouldUpdateBlockData) {
        NSArray *arrayLinesToUpdate  = [self getLinesDataForClientID:clientID];
        
        for (NSDictionary *dictLineToUpdate in arrayLinesToUpdate) {
            
            NSString *strProgram = [dictLineToUpdate valueForKey:kExercise];
            NSString *strLBValue = [dictLineToUpdate valueForKey:kLBValue];
            NSString *strRepValue = [dictLineToUpdate valueForKey:kRepValue];
            NSString *strDate = [dictLineToUpdate valueForKey:kEXDate];
            NSString *setValue = [dictLineToUpdate valueForKey:kSetValue];
            
            
            BOOL hasChaged = NO;
            if (strProgram != nil && strProgram.length == 0) {
                hasChaged = true;
                strProgram = @" ";
              
            }
            if (strLBValue!= nil && strLBValue.length == 0) {
                
                hasChaged = true;
                strLBValue  = @" ";
              
            }
            if (strRepValue != nil && strRepValue.length == 0) {
                hasChaged = true;
                strRepValue = @" ";
              
            }
            
            if (strDate != nil && strDate.length == 0) {
                hasChaged = true;
                strDate  = @" ";
            }
            if (setValue!= nil && setValue.length == 0) {
                hasChaged = true;
                setValue  = @" ";
            }
            
            
            
            if (strProgram == nil ) {
                hasChaged = true;
                strProgram = @" ";
                
            }
            if (strLBValue == nil ) {
                
                hasChaged = true;
                strLBValue  = @" ";
                
            }
            if (strRepValue == nil) {
                hasChaged = true;
                strRepValue = @" ";
                
            }
            
            if (strDate == nil) {
                hasChaged = true;
                strDate  = @" ";
            }
            if (setValue == nil ) {
                hasChaged = true;
                setValue  = @" ";
            }

            
            if (hasChaged) {
                
                NSMutableDictionary *dictToUpdate = [[[NSMutableDictionary alloc]initWithDictionary:dictLineToUpdate] autorelease];
                [dictToUpdate setObject:strProgram forKey:kExercise];
                [dictToUpdate setObject:strLBValue forKey:kLBValue];
                [dictToUpdate setObject:strRepValue forKey:kRepValue];
                [dictToUpdate setObject:strDate forKey:kEXDate];
                [dictToUpdate setObject:setValue forKey:kSetValue];
                
                [self updateBlockLineDataForDict:dictToUpdate andForClientID:clientID];
                dictToUpdate = nil;
            }
        }
 
    }
    
    
}

//-----------------------------------------------------------------------

-(void)updateTemplateDefaultData{
    START_METHOD
    NSArray *arrayTemplateBlocks = [self getBlockDataForTemplates];
    NSArray *arraySheets = [self getSheetsOfTemplate];
    
    for (NSDictionary *dictBlock in arraySheets ) {
        
      NSPredicate*  predicate = [NSPredicate predicateWithFormat:@"nSheetID == %@",[dictBlock valueForKey:@"SheetId"]];
      NSArray * filteredarrayforFeedArray  = [arrayTemplateBlocks filteredArrayUsingPredicate:predicate];
        if (filteredarrayforFeedArray.count == 0) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:kAppDateFormat];
            NSString* strDate = [formatter stringFromDate:[NSDate date]];
            
            NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc]init];
            [dictTemp setObject:@"1" forKey:@"blockNo"];
            [dictTemp setObject:strDate forKey:@"dBlockDate"];
            [dictTemp setObject:[dictBlock valueForKey:@"SheetId"] forKey:@"sheetID"];
            [dictTemp setObject:@"Workout-1" forKey:@"sBlockTitle"];
            
            [self insertTemplateSheet_Blocks:dictTemp];
            dictTemp = nil;
        }
    }
    
    arrayTemplateBlocks = [self getBlockDataForTemplates];
    
    for (NSDictionary *dictTemplateBlock in arrayTemplateBlocks) {
        
        NSString *strBlockTitle = [dictTemplateBlock valueForKey:kBlockTitle];
        NSString *strDate = [dictTemplateBlock valueForKey:kBlockDate];
        
        BOOL hasChaged = NO;
        if (strBlockTitle.length == 0) {
            hasChaged = true;
            strBlockTitle = [NSString stringWithFormat:@"Workout-%@",[dictTemplateBlock valueForKey:kBlockNo]];
        }
        if (strDate.length == 0) {
            hasChaged = true;
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:kAppDateFormat];
            strDate = [formatter stringFromDate:[NSDate date]];
        }
        if (hasChaged) {
            
            NSMutableDictionary *dictToUpdate = [[[NSMutableDictionary alloc]initWithDictionary:dictTemplateBlock] autorelease];
            [dictToUpdate setObject:strBlockTitle forKey:kBlockTitle];
            [dictToUpdate setObject:strDate forKey:kBlockDate];
            [self updateTemplateBlockDataForDict:dictToUpdate];
            dictToUpdate = nil;
        }

    }
    
    NSArray *arrayLinesToUpdate  = [self getProgramTemplateLines];
    for (NSDictionary *dictLineToUpdate in arrayLinesToUpdate) {
        
        NSString *strProgram = [dictLineToUpdate valueForKey:kExercise];
        NSString *strLBValue = [dictLineToUpdate valueForKey:kLBValue];
        NSString *strRepValue = [dictLineToUpdate valueForKey:kRepValue];
        NSString *strDate = [dictLineToUpdate valueForKey:kEXDate];
        NSString *setValue = [dictLineToUpdate valueForKey:kSetValue];
        
        
        BOOL hasChaged = NO;
        if (strProgram!= nil && strProgram.length == 0) {
            hasChaged = true;
            strProgram = @" ";
        }
        if (strLBValue!= nil && strLBValue.length == 0) {
            hasChaged = true;
            strLBValue  = @" ";
        }
        if (strRepValue != nil && strRepValue.length == 0) {
            hasChaged = true;
            strRepValue = @" ";
        }
        if (strDate!= nil && strDate.length == 0) {
            hasChaged = true;
            strDate  = @" ";
        }
        if (setValue!= nil && setValue.length == 0) {
            hasChaged = true;
            setValue  = @" ";
        }
        
        
        if (strProgram == nil) {
            hasChaged = true;
            strProgram = @" ";
        }
        if (strLBValue == nil) {
            hasChaged = true;
            strLBValue  = @" ";
        }
        if (strRepValue == nil ) {
            hasChaged = true;
            strRepValue = @" ";
        }
        if (strDate == nil) {
            hasChaged = true;
            strDate  = @" ";
        }
        if (setValue == nil) {
            hasChaged = true;
            setValue  = @" ";
        }

        
        
        if (hasChaged) {
            
            NSMutableDictionary *dictToUpdate = [[[NSMutableDictionary alloc]initWithDictionary:dictLineToUpdate] autorelease];
            [dictToUpdate setObject:strProgram forKey:kExercise];
            [dictToUpdate setObject:strLBValue forKey:kLBValue];
            [dictToUpdate setObject:strRepValue forKey:kRepValue];
            [dictToUpdate setObject:strDate forKey:kEXDate];
            [dictToUpdate setObject:setValue forKey:kSetValue];
            [self updateTemplateProgrammedData:dictToUpdate];
            dictToUpdate = nil;
        }
    }

}

//-----------------------------------------------------------------------

-(NSArray *)getSheetsOfTemplate{
    
    __autoreleasing NSMutableArray * arrSheets = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * From FT_Template_Sheet"];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:[rs stringForColumn:@"nSheetId"],@"SheetId",[rs stringForColumn:@"sSheetName"],@"SheetName",[rs stringForColumn:@"nUpdateStatus"],@"UpdateStatus", nil];
            
            [arrSheets addObject:dicTemp];
            dicTemp = Nil;
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * array = [NSArray arrayWithArray:arrSheets];
    [arrSheets release];
    return array;
}


-(NSArray *)getProgramTemplateLines{
    
    __autoreleasing NSMutableArray * programData = [[NSMutableArray alloc] init] ;
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_TemplateSheet_BlockData"];
        DebugSTRLOG(@"Query:%@",query);
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
                                  [rs stringForColumn:@"nBlockID"],kBlockId,
                                  [rs stringForColumn:@"nSheetID"],kSheetId,
                                  [rs stringForColumn:@"sProgram"],kExercise,
                                  [rs stringForColumn:@"sLBValue"],kLBValue,
                                  [rs stringForColumn:@"sRepValue"],kRepValue,
                                  [rs stringForColumn:@"nBlockNo"],kBlockNo,
                                  [rs stringForColumn:@"nRowNo"],kRowNo,
                                  [rs stringForColumn:kSetValue],kSetValue,
                                  [rs stringForColumn:@"sDate"],kEXDate,
                                   [rs stringForColumn:@"sColor"],kColor,
                                  
                                  
                                  nil];
            [programData addObject:dic];
            dic = nil;
        }			
    }	
    DebugLOG([db lastErrorMessage]);
    NSArray * arrtemp = [NSArray arrayWithArray:programData];
    [programData release];
    return arrtemp;
}


-(int)updateTemplateBlockDataForDict:(NSDictionary*)dictTemplateBlockInfo{
    START_METHOD
    [self  initFMDB];
    if([db open])
    {
        NSString *steBlockTitle = [dictTemplateBlockInfo valueForKey:kBlockTitle];
        NSString *strBlockDate = [dictTemplateBlockInfo valueForKey:kBlockDate];
        NSInteger intSheetID = [[dictTemplateBlockInfo valueForKey:kSheetId] integerValue];
        NSInteger intBlockNo = [[dictTemplateBlockInfo valueForKey:@"nBlock_ID"] integerValue];
        
        NSString * query = [NSString stringWithFormat:@"Update FT_TemplateSheet_Blocks set dBlockDate = '%@',sBlockTitle = '%@' where nSheetID = %ld And nBlock_ID = %ld",strBlockDate,steBlockTitle,(long)intSheetID,(long)intBlockNo];
        DebugSTRLOG(@"query:",query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        DebugSTRLOG(@"Error:",str);
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}

//-----------------------------------------------------------------------

-(NSArray *)getBlockDataForTemplates{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * From FT_TemplateSheet_Blocks"];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys: [rs stringForColumn:@"nBlock_ID"],kBlockId,
                                      [rs stringForColumn:@"nSheetID"],kSheetId,
                                      [rs stringForColumn:@"dBlockDate"],kBlockDate,
                                      [rs stringForColumn:@"nBlockNO"],kBlockNo,
                                      [rs stringForColumn:@"sBlockTitle"],kBlockTitle,[rs stringForColumn:@"nBlock_ID"],@"nBlock_ID",nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}

//-----------------------------------------------------------------------

- (int) updateBlockLineDataForDict:(NSDictionary *)dic andForClientID:(NSString *) cId {
    [self  initFMDB];
    
    if([db open])
    {
        NSString * strExercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLbValue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        NSString * strSetValue = [self escapeString:[dic objectForKey:@"sSetValue"]];
        
        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_BlockData set nBlockID = %d,sColor = %d,nSheetID = %d,sProgram = '%@',sLBValue = '%@',sRepValue = '%@',nBlockNo = %d,nRowNo = %d,sDate = '%@',sSetValue = '%@' Where nBlockNo = %d and nRowNo = %d and nSheetID = %d",cId,[[dic objectForKey:kBlockId] intValue],[[dic objectForKey:kColor] intValue],[[dic objectForKey:kSheetId] intValue],strExercise,strLbValue,strRepValue,[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],[dic objectForKey:kEXDate],strSetValue,[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],[[dic objectForKey:kSheetId] intValue]];
        DebugLOG(query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        strExercise = nil;
        strLbValue = nil;
        strRepValue = nil;
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
    
}

//-----------------------------------------------------------------------

- (NSArray *) getLinesDataForClientID:(NSString *)clientID{
    
    NSMutableArray * programData = [[[NSMutableArray alloc] init] autorelease];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_BlockData",clientID];
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
                                  [rs stringForColumn:@"nBlockID"],kBlockId,
                                  [rs stringForColumn:@"nSheetID"],kSheetId,
                                  [rs stringForColumn:@"sProgram"],kExercise,
                                  [rs stringForColumn:@"sLBValue"],kLBValue,
                                  [rs stringForColumn:@"sRepValue"],kRepValue,
                                  [rs stringForColumn:@"sSetValue"],kSetValue,
                                  [rs stringForColumn:@"sColor"],kColor,
                                  [rs stringForColumn:@"nBlockNo"],kBlockNo,
                                  [rs stringForColumn:@"nRowNo"],kRowNo,
                                  [rs stringForColumn:@"sDate"],kEXDate,nil];
            [programData addObject:dic];
        }			
    }	
    DebugLOG([db lastErrorMessage]);
    return (NSArray *)programData;	
}



- (int) updateBlockNameForBlock:(NSDictionary*)dictBlockInfo atClientID:(NSString*)clientID {
    START_METHOD
    [self  initFMDB];
    if([db open])
    {
        NSString*  blockTitle = [dictBlockInfo valueForKey:kBlockTitle];
        NSInteger blockID = [[dictBlockInfo valueForKey:kBlockId] integerValue];
        NSString* blockDate = [dictBlockInfo valueForKey:kBlockDate];
        NSString * query = [NSString stringWithFormat:@"Update FT%@_ProgramSheet_Blocks set sBlockTitle = '%@',dBlockdate = '%@' where nBlockID = %ld",clientID,blockTitle,blockDate,(long)blockID];
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
}

//-----------------------------------------------------------------------

- (NSArray *) getBlockDataForClientID:(NSString *)cId{
    
    NSMutableArray * blockData = [[[NSMutableArray alloc] init] autorelease];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks",cId];
        
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSString *strBlobkNotes = [rs stringForColumn:@"sBlockNotes"];
            NSString *strBlobkTitle = [rs stringForColumn:@"sBlockTitle"];
            
            if ([[rs stringForColumn:@"sBlockNotes"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkNotes = @"";
            }
            if ([[rs stringForColumn:@"sBlockTitle"] isEqual:(id)[NSNull null]] || strBlobkNotes == nil)
            {
                strBlobkTitle = @"";
            }
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [rs stringForColumn:@"nBlockID"],kBlockId,
                                  [rs stringForColumn:@"nSheetID"],kSheetId,
                                  [rs stringForColumn:@"dBlockdate"],kBlockDate,
                                  [rs stringForColumn:@"nBlockNo"],kBlockNo,
                                  strBlobkNotes,kBlockNotes,
                                  strBlobkTitle,kBlockTitle,nil];
            [blockData addObject:dic];
        }
    }
    
    DebugLOG([db lastErrorMessage]);
    return (NSArray *)blockData;
}


//-----------------------------------------------------------------------

- (NSString *) createClientTablesIfNotExist:(NSString *)cid {
    START_METHOD
	[self  initFMDB];
	NSString * client_id = @"";
	if([db open])
	{
		
		NSString * query;
		// Program Sheet Table
		query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT%@_ProgramSheet\" (\"nSheetID\" INTEGER PRIMARY KEY  NOT NULL , \"sSheetName\" VARCHAR,\"sPurpose\" VARCHAR,\"nUpdateStatus\" INTEGER DEFAULT 0)",cid];
		[db executeUpdate:query];
		
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
		
		query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT%@_ProgramSheet_Blocks\" (\"nBlockID\" INTEGER PRIMARY KEY  NOT NULL , \"nSheetID\" INTEGER, \"dBlockdate\" DATETIME, \"nBlockNo\" INTEGER, \"sBlockTitle\" VARCHAR,\"sBlockNotes\" VARCHAR)",cid];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
		
		query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT%@_ProgramSheet_BlockData\" (\"nBlockDataID\" INTEGER PRIMARY KEY  NOT NULL , \"nBlockID\" INTEGER, \"nSheetID\" INTEGER, \"sProgram\" VARCHAR, \"sLBValue\" VARCHAR, \"sRepValue\" VARCHAR,\"sSetValue\" VARCHAR,\"nBlockNo\" INTEGER,\"nRowNo\" INTEGER,\"sDate\" VARCHAR, \"sColor\" INTEGER)",cid];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
		
		query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT%@_AssessmentSheet\" (\"nSheetID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"sSheetName\" VARCHAR)",cid];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
		
		query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT%@_AssessmentSheet_Data\" (\"nDetailID\" INTEGER PRIMARY KEY  NOT NULL ,\"nSheetID\" INTEGER,\"nAssessmentText\" VARCHAR,\"fCM_RightUpperArm\" VARCHAR,\"fCM_RightForeArm\" VARCHAR,\"fCM_Chest\" VARCHAR,\"fCM_Abdominal\" VARCHAR,\"fCM_HipButtocks\" VARCHAR,\"fCM_RightThing\" VARCHAR,\"fCM_RightCalf\" VARCHAR,\"fCM_Result\" VARCHAR,\"fBMI_Pound_Weight\" VARCHAR,\"fBMI_Pound_Height\" VARCHAR,\"fBMI_Pound_Result\" VARCHAR,\"fHeight\" VARCHAR,\"fWeight\" VARCHAR,\"nRestingHeartBeat\" INTEGER,\"sBloodPressure\" VARCHAR,\"fThree_Male_Chest\" VARCHAR,\"fThree_Male_Abdomen\" VARCHAR,\"fThree_Male_Thigh\" VARCHAR,\"fThree_Female_Triceps\" VARCHAR,\"fThree_Female_Suprailiac\" VARCHAR,\"fThree_Female_Thigh\" VARCHAR,\"fFour_Abdomen\" VARCHAR,\"fFour_Suprailiac\" VARCHAR,\"fFour_Triceps\" VARCHAR,\"fFour_Thigh\" VARCHAR,\"fSeven_Chest\" VARCHAR,\"fSeven_Midaxillary\" VARCHAR,\"fSeven_Triceps\" VARCHAR,\"fSeven_subscapular\" VARCHAR,\"fSeven_Abdomen\" VARCHAR,\"fSeven_Suprailiac\" VARCHAR,\"fSeven_Thigh\" VARCHAR,\"fThree_Result\" VARCHAR, \"fFour_Result\" VARCHAR, \"fSeven_Result\" VARCHAR,\"fBMI_Metrics_Weight\" VARCHAR,\"fBMI_Metrics_Height\" VARCHAR,\"fBMI_Metrics_Result\" VARCHAR,\"dUpdated_date\" DATETIME, \"sAssessment_Goal\" VARCHAR,\"fGenereal_FatPercent\" VARCHAR,\"fExtra_Value\" VARCHAR,\"sExtra_FieldName\" VARCHAR)",cid];
		[db executeUpdate:query];
        
          MESSAGE(@"Query Asseessment Craete-> query:%@",query);
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
        
		
		query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT%@_AssessmentSheet_ImagesData\" (\"nImageID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"nSheetID\" INTEGER, \"sImageLabel\" VARCHAR, \"hasImage\" INTEGER)",cid];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}		
        
        
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT_Template\" (\"nId\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"sTemplate_Name\" VARCHAR, \"nSheet_Contained\" INTEGER)"];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
        
        
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT_Template_Sheet\" (\"nSheetId\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"sSheetName\" VARCHAR, \"nUpdateStatus\" INTEGER DEFAULT 0, \"nTemplateId\" INTEGER)"];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
        
        
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT_TemplateSheet_Blocks\" (\"nBlock_ID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"nSheetID\" INTEGER, \"dBlockDate\" DATETIME, \"nBlockNO\" INTEGER, \"sBlockTitle\" VARCHAR)"];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
               
        query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS \"FT_TemplateSheet_BlockData\" (\"nBlockDataID\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"nBlockID\" INTEGER, \"nSheetID\" INTEGER, \"sProgram\" VARCHAR, \"sLBValue\" VARCHAR, \"sRepValue\" VARCHAR, \"nBlockNO\" INTEGER, \"nRowNO\" INTEGER, \"sDate\" VARCHAR,\"sColor\" INTEGER)"];
		[db executeUpdate:query];
		str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return cid;
		}
        MESSAGE(@"Query Asseessment Craete-> query:%@",query);


		
	}
    
	return client_id;
}

//--------------------------------------------------------------------------

- (NSMutableArray *) getClientsID {
	[self initFMDB];	
	NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT nClientID from FT_Client"];
		FMResultSet * rs = [db executeQuery:query];
		while ([rs next]) {
			[array addObject:[rs stringForColumn:@"nClientID"]];
		}	
		
		DebugLOG([db lastErrorMessage]);
	}
	return array;
}


//-----------------------------------------------------------------------

-(NSString*)getBlockNumberForNewWorkoutForClient:(NSString*)clientId forProgramSheet:(NSString*)strSheetId {
    START_METHOD
    [self initFMDB];
    NSString *strBlockNumber = 0;
    if([db open]) {
    
        NSString *query = [NSString stringWithFormat:@"SELECT  max(nBlockNo) from FT%@_ProgramSheet_Blocks where nSheetID = %@",clientId,strSheetId];
        FMResultSet * rs = [db executeQuery:query];
        while ([rs next]) {
          strBlockNumber = [rs stringForColumn:@"max(nBlockNo)"];
        }
    }
    
    NSString * str= [db lastErrorMessage];
    if(![str isEqualToString:@"not an error"]) {
        return 0;
    }
    
    return strBlockNumber;
}




//--------------------------------------------------------------------------

#pragma mark -
#pragma mark - insert Method of Template.

-(int)addTemplate:(NSDictionary *)dic{
    MESSAGE(@"addTemplate template dic: %@",dic);

    [self initFMDB];
    if (![db open]) {
        return 0;
    }	
    else{
        NSString * strTemplateName=[self escapeString: [dic objectForKey:@"templateName"]];
        int inttemp = [[dic objectForKey:@"sheetContained"] intValue];
        int inttempId = [[dic objectForKey:@"nId"] intValue];

        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString *query;
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            query=[NSString stringWithFormat:@"Insert into FT_Template(nId,sTemplate_Name,nSheet_Contained) values(%d,'%@',%d)",inttempId,strTemplateName,inttemp];
            
            
        }else{
              query=[NSString stringWithFormat:@"Insert into FT_Template(sTemplate_Name,nSheet_Contained) values('%@',%d)",strTemplateName,inttemp];
        }
      
          MESSAGE(@"cleints template query: %@",query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];

        if(![str isEqualToString:@"not an error"]) {
            return 0;
        }
        else {
            query = Nil;
            strTemplateName = Nil;
            return 1;
        }
    }
    return 0;
}

//--------------------------------------------------------------------------

-(void)addSheetDetail:(NSDictionary *)dic{
    
    MESSAGE(@"addSheetDetail-->dictProgramsWorkouts:%@",dic);
    
    int intUpdateStatus = [[dic objectForKey:@"updateStatus"] intValue];
    int intTemplateID = [[dic objectForKey:@"templateid"] intValue];
    
    [self initFMDB];
    if (![db open]) {		
    }	
    else{
        
        NSString * strSheetName = [self escapeString:[dic objectForKey:@"sSheetName"]];
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString *query;
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            query=[NSString stringWithFormat:@"Insert into FT_Template_Sheet (nSheetId,sSheetName,nUpdateStatus,nTemplateId)values(%d,'%@',%d,%d)",[[dic objectForKey:ksheetID] intValue],strSheetName,intUpdateStatus,intTemplateID];

            
        }else{
            query=[NSString stringWithFormat:@"Insert into FT_Template_Sheet (sSheetName,nUpdateStatus,nTemplateId)values('%@',%d,%d)",strSheetName,intUpdateStatus,intTemplateID];

        }
        
        MESSAGE(@"query addSheetDetail dictProgramsWorkouts:: %@",query);
        
        [db executeUpdate:query];
        query = Nil;
        strSheetName = Nil;
    }
}
//--------------------------------------------------------------------------

-(void)insertTemplateSheet_Blocks:(NSDictionary *)objDict{
    
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:objDict];
    
    if([dic objectForKey:@"strDate"]){
        
        MESSAGE(@"already date : ");
        
    }else{
        
        MESSAGE(@"Add adtege ke d idr ");
        
        
        [dic setValue:@"" forKey:@"strDate"];
    }
    
    MESSAGE("insertTemplateSheet_Blocks: dictWorkout: %@",dic);
    
    [self initFMDB];
    if (![db open]) {		
    }
    
    else{
        int intblockNo = [[dic objectForKey:@"blockNo"] intValue];
        int intsheetID = [[dic objectForKey:@"sheetID"] intValue];
        NSString * strblockDate = [dic objectForKey:@"dBlockDate"];
        
        MESSAGE(@"strblockDate---> %@",[dic objectForKey:@"strDate"]);
        
        
        
        NSString * strBlockTitle = [self escapeString:[dic objectForKey:@"sBlockTitle"]];

        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString *query;
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            
            if(strblockDate && strblockDate.length>0){
                
            }else{
                strblockDate = [dic objectForKey:@"strDate"];
            }
            
            query=[NSString stringWithFormat:@"Insert into FT_TemplateSheet_Blocks (nBlock_ID,nSheetID,nBlockNO,dBlockDate,sBlockTitle)values(%d,%d,%d,'%@','%@')",[[dic objectForKey:kBlockId] intValue],intsheetID,intblockNo,strblockDate,strBlockTitle];
        
        }else{
           query=[NSString stringWithFormat:@"Insert into FT_TemplateSheet_Blocks (nSheetID,nBlockNO,dBlockDate,sBlockTitle)values(%d,%d,'%@','%@')",intsheetID,intblockNo,strblockDate,strBlockTitle];
            
        }
        MESSAGE(@"query  dictWorkout:: %@",query);

     
        [db executeUpdate:query];
        query = Nil;
    }
}
//--------------------------------------------------------------------------

- (int) insertTemplate_ProgramedData:(NSDictionary *)dic{
	[self  initFMDB];   
    MESSAGE(@"dictionary - .> %@",dic);
	if([db open])
	{
        NSString * strExcercise = [self escapeString:[dic objectForKey:kExercise]];
        NSString * strLBvalue = [self escapeString:[dic objectForKey:kLBValue]];
        NSString * strRepValue = [self escapeString:[dic objectForKey:kRepValue]];
        
        
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        NSString *query;
        if([strUpgradeStatus isEqualToString:@"YES"]){
            
    query     = [NSString stringWithFormat:@"insert into FT_TemplateSheet_BlockData (nBlockDataID,nBlockID,nSheetID,sProgram,sLBValue,sRepValue,nBlockNO,nRowNO,sDate,sSetValue,sColor) values(%d,%d,%d,'%@','%@','%@',%d,%d,'%@','%@',%d)",
                                [[dic objectForKey:kBlockDataId] intValue],
                                [[dic objectForKey:kBlockId] intValue],
                                [[dic objectForKey:kSheetId] intValue],
                                strExcercise,strLBvalue,strRepValue,
                                [[dic objectForKey:kBlockNo] intValue],
                                [[dic objectForKey:kRowNo] intValue],
                                [dic objectForKey:kEXDate],
                                [dic objectForKey:@"sSetValue"],
                                [[dic objectForKey:kColor] intValue]
                                ];
        }else{
            query = [NSString stringWithFormat:@"insert into FT_TemplateSheet_BlockData (nBlockID,nSheetID,sProgram,sLBValue,sRepValue,nBlockNO,nRowNO,sDate,sSetValue,sColor) values(%d,%d,'%@','%@','%@',%d,%d,'%@','%@',%d)",
                               [[dic objectForKey:kBlockId] intValue],
                               [[dic objectForKey:kSheetId] intValue],
                               strExcercise,strLBvalue,strRepValue,
                               [[dic objectForKey:kBlockNo] intValue],
                               [[dic objectForKey:kRowNo] intValue],
                               [dic objectForKey:kEXDate],
                               [dic objectForKey:@"sSetValue"],
                               [[dic objectForKey:kColor] intValue]
                               ];
        }
        
	
		[db executeUpdate:query];
        
        MESSAGE(@"insert query: %@",query);
        
		NSString * str= [db lastErrorMessage];
		DebugLOG(query);
		if(![str isEqualToString:@"not an error"]) {
            strExcercise = nil;
            strLBvalue = nil;
            strRepValue = nil;
			return 1;
		}
		else {
            strExcercise = nil;
            strLBvalue = nil;
            strRepValue = nil;
			return 0;
		}
	}
	return 0;
}
//--------------------------------------------------------------------------

#pragma mark -
#pragma mark - Update Method of Template.

-(NSInteger)updateBlockDate_Title:(NSInteger)intSheetID :(NSInteger)intBlockNo :(NSString *)date :(NSString *)strBlock_Title{
    
   
    [self  initFMDB];
	if([db open])
	{
        if([strBlock_Title length] <= 0){
            strBlock_Title = @"";
        }else{
         strBlock_Title = [self escapeString:strBlock_Title];   
        }
        
		NSString * query = [NSString stringWithFormat:@"Update FT_TemplateSheet_Blocks set dBlockDate = '%@',sBlockTitle = '%@' where nSheetID = %ld And nBlock_ID = %ld",date,strBlock_Title,(long)intSheetID,(long)intBlockNo];
		DebugSTRLOG(@"query:",query);
        
        MESSAGE(@"updateBlockDate_Title--> query: %@",query);
        
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		DebugSTRLOG(@"Error:",str);
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
}

//--------------------------------------------------------------------------

- (int) updateTemplateProgrammedData:(NSDictionary *)dic {
    START_METHOD
	NSString * strExcercise = @"";
    NSString * strLBvalue  = @"";
    NSString * strRepValue = @"";
    
    if(![[dic valueForKey:kExercise] isEqualToString:@""]){
        strExcercise = [self escapeHtmlCharaters:[dic valueForKey:kExercise]];
    }
        
    if(![[dic valueForKey:kLBValue] isEqualToString:@""]){
        strLBvalue = [self escapeHtmlCharaters:[dic valueForKey:kLBValue]];
    }
    
    if(![[dic valueForKey:kRepValue] isEqualToString:@""]){
        strRepValue = [self escapeHtmlCharaters:[dic valueForKey:kRepValue]];
    }
    [self  initFMDB];
	if([db open])
	{
		NSString * query = [NSString stringWithFormat:@"Update FT_TemplateSheet_BlockData set nBlockID = %d,nSheetID = %d,sProgram = '%@',sLBValue = '%@',sRepValue = \"%@\",nBlockNO = %d,nRowNO = %d,sDate = '%@',sSetValue = '%@', sColor = %d Where nBlockDataID = %d and nRowNo = %d and nSheetID = %d",[[dic objectForKey:kBlockId] intValue],[[dic objectForKey:kSheetId] intValue],strExcercise,strLBvalue,strRepValue,[[dic objectForKey:kBlockNo] intValue],[[dic objectForKey:kRowNo] intValue],[dic objectForKey:kEXDate],[dic objectForKey:@"sSetValue"],[[dic objectForKey:kColor] intValue],[[dic objectForKey:kBlockDataId] intValue],[[dic objectForKey:kRowNo] intValue],[[dic objectForKey:kSheetId] intValue]];
		DebugLOG(query);
		[db executeUpdate:query];
		NSString * str= [db lastErrorMessage];
		if(![str isEqualToString:@"not an error"]) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 1;
	}
}

//-----------------------------------------------------------------------

- (int )updateTemplateName:(NSString *)templateName oldTemplate:(NSInteger)intTemplateID {
    START_METHOD
    
//TODO : SUNIL-> Update Template Name
    
    [self  initFMDB];
    if([db open])
    {
        if([templateName length] <= 0){
            templateName = @"";
        }else{
            templateName = [self escapeString:templateName];
        }
        NSString * query = [NSString stringWithFormat:@"update FT_Template set sTemplate_Name = '%@' where nId = %ld",templateName,(long)intTemplateID];
        DebugSTRLOG(@"query:",query);
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        DebugSTRLOG(@"Error:",str);
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }

}

//--------------------------------------------------------------------------
#pragma mark -
#pragma mark - Select Method of Template.


-(NSArray *)getAllTemplatesDetail{
    
    [self initFMDB];
    __autoreleasing NSMutableArray * array = [[NSMutableArray alloc]init];
    if(![ db open]){
        
    }else{
        NSString * query = [NSString stringWithFormat:@"SELECT * from FT_Template"];
        FMResultSet *rs = [db executeQuery:query];;
        while([rs next]){

            
            NSDictionary * dicTemplateData = [NSDictionary dictionaryWithObjectsAndKeys:[rs stringForColumn:@"nId"],@"template_Id",[rs stringForColumn:@"sTemplate_Name"],@"templateName",[rs stringForColumn:@"nSheet_Contained"],@"SheetContained", nil];
            
            if (dicTemplateData != nil && dicTemplateData.count > 0) {
                [array addObject:dicTemplateData];
            }
        }
    }
    NSArray * arrtemp = [NSArray arrayWithArray:array];
    [array release];
    
    
    return arrtemp;
}
//--------------------------------------------------------------------------

-(int)getLastTemplateId{
    int intTemplateID = 0;
    [self initFMDB];
    if (![db open]) {		
	}	
    else{
        FMResultSet *rs=[db executeQuery:@"SELECT nId FROM FT_Template"];        
        while([rs next]){
            intTemplateID = [[rs stringForColumn:@"nId"] intValue];
        }
    }
    
    return intTemplateID;
}


//--------------------------------------------------------------------------

-(NSArray *)getSheetsOfTemplate:(NSInteger)intTemplateID{
    
    __autoreleasing NSMutableArray * arrSheets = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {		
	}	
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * From FT_Template_Sheet where nTemplateId = %ld ",(long)intTemplateID];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:[rs stringForColumn:@"nSheetId"],@"SheetId",[rs stringForColumn:@"sSheetName"],@"SheetName",[rs stringForColumn:@"nUpdateStatus"],@"UpdateStatus", nil];
            
            [arrSheets addObject:dicTemp];
            dicTemp = Nil;
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * array = [NSArray arrayWithArray:arrSheets];
    [arrSheets release];
    return array;   
}

//--------------------------------------------------------------------------

-(NSArray *)getBlockDataFor_TemplateSheet:(NSInteger)intSheetId{
    START_METHOD
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {		
	}	
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * From FT_TemplateSheet_Blocks where nSheetID = %ld ORDER BY nBlock_ID",(long)intSheetId];
        
                MESSAGE(@"selectProgramDataForBlockNo> %@ ",stringSQL);
        
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
        NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:[rs stringForColumn:@"nBlock_ID"],kBlockId,
            [rs stringForColumn:@"nSheetID"],kSheetId,
            [rs stringForColumn:@"dBlockDate"],kBlockDate,
            [rs stringForColumn:@"nBlockNO"],kBlockNo,
            [rs stringForColumn:@"sBlockTitle"],kBlockTitle,nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}
//--------------------------------------------------------------------------

-(NSArray *)selectProgramDataForTemplate:(int)intSheet :(int)intBlockNo{
    
    __autoreleasing NSMutableArray * programData = [[NSMutableArray alloc] init] ;
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_TemplateSheet_BlockData where nSheetID = %d AND nBlockNO = %d ORDER BY nRowNo",intSheet,intBlockNo];
		DebugSTRLOG(@"Query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
								  [rs stringForColumn:@"sDate"],kEXDate,
                                [rs stringForColumn:@"sColor"],kColor,nil];
			[programData addObject:dic];
            dic = nil;
		}			
	}	
	DebugLOG([db lastErrorMessage]);
    NSArray * arrtemp = [NSArray arrayWithArray:programData];
    [programData release];
	return arrtemp;
}
//--------------------------------------------------------------------------

- (NSArray *) getTemplateSheetList:(NSInteger)intTemplateID {
	  __autoreleasing NSMutableArray * sheetList = [[NSMutableArray alloc] init];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_Template_Sheet where nTemplateId = %ld",(long)intTemplateID];
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nSheetId"],ksheetID,
								  [rs stringForColumn:@"sSheetName"],ksheetName,nil];
			[sheetList addObject:dic];
            dic = nil;
		}			
	}	
	DebugLOG([db lastErrorMessage]);
    NSArray * array = [NSArray arrayWithArray:sheetList];
    [sheetList release];
	return array;
}
//--------------------------------------------------------------------------

-(NSArray *)selectProgramDataForSheet_Template:(int)intSheet{
    
    __autoreleasing NSMutableArray * programData = [[NSMutableArray alloc] init] ;
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_TemplateSheet_BlockData where nSheetID = %d",intSheet];
		DebugSTRLOG(@"Query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
								  [rs stringForColumn:@"sDate"],kEXDate,[rs stringForColumn:@"sColor"],kColor,nil];
			[programData addObject:dic];
            dic = nil;
		}			
	}	
	DebugLOG([db lastErrorMessage]);
    NSArray * array = [NSArray arrayWithArray:programData];
    [programData release];
	return array;
}
//--------------------------------------------------------------------------

-(NSString *)getSheetName:(int)SheetId{
    
    NSString * strTemp = @" ";
    if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT sSheetName FROM FT_Template_Sheet where nSheetId = %d",SheetId];		
		FMResultSet * rs = [db executeQuery:query];
		while ([rs next]) {
            strTemp = [NSString stringWithString:[rs stringForColumn:@"sSheetName"]];
		}			
	}	
	DebugLOG([db lastErrorMessage]); 
    return strTemp;
}
//--------------------------------------------------------------------------

-(NSArray *)selectProgramDataForBlockNo:(NSInteger)intBlockno{
    
    START_METHOD
    MESSAGE(@"intBlockno-> %ld",(long)intBlockno);
    __autoreleasing NSMutableArray * programData = [[NSMutableArray alloc] init] ;
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_TemplateSheet_BlockData where nBlockID = %ld",(long)intBlockno];
        MESSAGE(@"selectProgramDataForBlockNo> %@ ",query);

		DebugSTRLOG(@"Query:%@",query);
		FMResultSet * rs = [db executeQuery:query];
		
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlockDataID"],kBlockDataId,
								  [rs stringForColumn:@"nBlockID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"sProgram"],kExercise,
								  [rs stringForColumn:@"sLBValue"],kLBValue,
								  [rs stringForColumn:@"sRepValue"],kRepValue,
                                    [rs stringForColumn:@"sSetValue"],kSetValue,
								  [rs stringForColumn:@"nBlockNo"],kBlockNo,
								  [rs stringForColumn:@"nRowNo"],kRowNo,
                                  [rs stringForColumn:@"sSetValue"],@"sSetValue",
								  [rs stringForColumn:@"sDate"],kEXDate,
                                [rs stringForColumn:@"sColor"],kColor,nil];
			[programData addObject:dic];
            dic = nil;
		}		
	}	
	DebugLOG([db lastErrorMessage]);
    NSArray * arrtemp = [NSArray arrayWithArray:programData];
    [programData release];
    programData = nil;
	return arrtemp;
}
//--------------------------------------------------------------------------

-(NSString *)getnBlockID_Template:(NSString *)sheetID :(NSString *)BlockNo{
    
    NSString * strBlockID = @"";
    if(![db open]){
        
    }else{
        NSString * query = [NSString stringWithFormat:@"SELECT nBlock_ID FROM FT_TemplateSheet_Blocks where nSheetID = %d AND nBlockNO = %d",[sheetID intValue],[BlockNo intValue]];
       FMResultSet * rs = [db executeQuery:query];
        
        while([rs next]){
            strBlockID = [rs stringForColumn:@"nBlock_ID"];
        }
        
        DebugLOG([db lastErrorMessage]);
    }
    return strBlockID;
    
}
//--------------------------------------------------------------------------

- (NSArray *) getBlock_Template:(NSString *)sheetId :(NSString *)blockNo{
	NSMutableArray * blockData = [[[NSMutableArray alloc] init] autorelease];
	if (![db open]) {		
	}	
	else {		
		NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_TemplateSheet_Blocks where nSheetID = %d and nBlockNo = %d ORDER BY nBlockNo",[sheetId intValue],[blockNo intValue]];	
		FMResultSet * rs = [db executeQuery:query];
		while ([rs next]) {
			NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
								  [rs stringForColumn:@"nBlock_ID"],kBlockId,
								  [rs stringForColumn:@"nSheetID"],kSheetId,
								  [rs stringForColumn:@"dBlockDate"],kBlockDate,
								  [rs stringForColumn:@"nBlockNO"],kBlockNo,
								  [rs stringForColumn:@"sBlockTitle"],kBlockTitle,nil];
			[blockData addObject:dic];
		}			
	}	
	DebugLOG([db lastErrorMessage]);
	return (NSArray *)blockData;
}
//--------------------------------------------------------------------------

#pragma mark -
#pragma mark - Delete Method of Template.

-(void)deleteSheetBlock_BlockData:(int)sheetId{
 
    [self  initFMDB];
	if([db open])
	{
		NSString * query =[NSString stringWithFormat:@"delete from FT_TemplateSheet_BlockData where nSheetID = %d",sheetId];
		[db executeUpdate:query];
        
        NSString * query1 =[NSString stringWithFormat:@"delete from FT_TemplateSheet_Blocks where nSheetID = %d",sheetId];
		[db executeUpdate:query1];
        
    }
}

//-----------------------------------------------------------------------
-(void)deleteLineForLineID:(NSInteger)lineID{
    [self  initFMDB];
    if([db open])
    {
        NSString * query1 =[NSString stringWithFormat:@"delete from FT_TemplateSheet_BlockData where nBlockDataID == %ld",(long)lineID];
        [db executeUpdate:query1];
        
        MESSAGE(@"deleteLineForLineID-->query1: %@",query1);
        
        
    }
}

-(void)deleteSheetLineForSheetID:(NSInteger)sheetId andForBlockID:(NSInteger)blockID{
    
    [self  initFMDB];
    if([db open])
    {
//        NSString * query =[NSString stringWithFormat:@"delete from FT_TemplateSheet_BlockData where nSheetID = %d",sheetId];
//        [db executeUpdate:query];
        
        NSString * query1 =[NSString stringWithFormat:@"delete from FT_TemplateSheet_Blocks where nSheetID = %ld AND nBlockID == %ld",(long)sheetId,(long)blockID];
        [db executeUpdate:query1];
        
    }
}
//--------------------------------------------------------------------------

-(int)deleteTemplate_TemplateSheet:(int)templateId{
    
    int temp = 1;
    [self  initFMDB];
	if([db open])
	{
		NSString * query =[NSString stringWithFormat:@"delete from FT_Template_Sheet where nTemplateId = %d",templateId];
		
        if(![db executeUpdate:query]){
            temp = 0;
        }
        
        NSString * query1 =[NSString stringWithFormat:@"delete from FT_Template where nId = %d",templateId];
		if(![db executeUpdate:query1]){
            temp = 0;
        }
    }
    return temp;
}

//--------------------------------------------------------------------------

-(int)deleteTemplateSheetData:(int)sheetId{
    
    [self  initFMDB];
	if([db open])
	{
		NSString * query =[NSString stringWithFormat:@"delete from FT_TemplateSheet_BlockData where nSheetID = %d",sheetId];
		[db executeUpdate:query];
        
        NSString * str= [db lastErrorMessage];
        
        if([str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    return 0;
}    
//--------------------------------------------------------------------------
        
#pragma mark - 
#pragma mark - EscapeString Method.

- (NSString *) escapeString:(NSString *)string {
    START_METHOD
    
    MESSAGE(@"Message -> before:%@",string);
	if([string rangeOfString:@"'"].location == NSNotFound){
        
        MESSAGE(@"Message -> retunrn :%@",string);
        return string;
    }else{
        
        MESSAGE(@"Message -> retunrn :%@",string);
        NSRange range = NSMakeRange(0, [string length]);
        return [string stringByReplacingOccurrencesOfString:@"'" withString:@"''" options:NSCaseInsensitiveSearch range:range];
    }
    
    
}
//--------------------------------------------------------------------------

-(NSString *)escapeHtmlCharaters:(NSString *)str{
    
    NSString * strtemp = [str stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    strtemp =  [strtemp stringByReplacingOccurrencesOfString:@"/" withString:@"&#47;"];
    strtemp = [strtemp stringByReplacingOccurrencesOfString:@"\\" withString:@"&#92;"];
    strtemp = [strtemp stringByReplacingOccurrencesOfString:@";" withString:@"&#59;"];
     strtemp = [strtemp stringByReplacingOccurrencesOfString:@":" withString:@"&#58;"];
     strtemp = [strtemp stringByReplacingOccurrencesOfString:@"?" withString:@"&#63;"];
    strtemp = [self escapeString:strtemp];
    return strtemp;
}
//--------------------------------------------------------------------------

//SUNIl->

#pragma mark - MIGRATION

/*
 
 "FT_PARQ_Category"
 "FT_PARQ_Questions"
 "FT_PARQ_Answer"
 "FT_Template"
 "FT_Template_Sheet"
 "FT_TemplateSheet_Blocks"
 "FT_TemplateSheet_BlockData"
 "FT_Client" Get All clients
 for->
 get all tables
 
 {
 
 "FT1_AssessmentSheet"
 
 
 "FT1_ProgramSheet"
 "FT1_ProgramSheet_Blocks"
 "FT1_ProgramSheet_BlockData"
 
 "FT1_AssessmentSheet_Data"
 "FT1_AssessmentSheet_ImagesData"

 }
 */


//For All Clients Name
- (NSArray	*) getClients{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select sLastName, sFirstName From FT_Client"];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"sFirstName"]  ,kFirstName ,
                                      [rs stringForColumn:@"sLastName"]  ,kLastName , nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}




//For All Clients Deatil
- (NSArray	*) getClientsDetail{

    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        
        
//        NSString *stringSQL = [NSString stringWithFormat:@"select * From FT_Client"];

        NSString *stringSQL = [NSString stringWithFormat:@"select * From FT_Client where migrated !='YES'"];

        
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"sFirstName"]  ,kFirstName ,
                                      [rs stringForColumn:@"sLastName"]  ,kLastName ,
                                      [rs stringForColumn:@"sGender"]  ,kgender ,
                                      [rs stringForColumn:@"dBirthDate"]  ,kBirthDate ,
                                      [rs stringForColumn:@"nAge"]  ,kAge ,
                                      [rs stringForColumn:@"sPhone_Day"]  ,kPday ,
                                      [rs stringForColumn:@"sPhone_Evening"]  ,kPeve ,
                                      [rs stringForColumn:@"sPhone_Cell"]  ,kPcell ,
                                      [rs stringForColumn:@"sEmailAddress"]  ,kEmail1 ,
                                      [rs stringForColumn:@"sEmailAddress2"]  ,kEmail2 ,
                                      [rs stringForColumn:@"sEmergencyContactName1"]  ,kEmergencyConName1 ,
                                      [rs stringForColumn:@"sEmergencyContact1_Relationship"]  ,kEmergencyRel1 ,
                                      [rs stringForColumn:@"sEmergencyContact1_Phone_Work"]  ,kEmergencyConWNo1 ,
                                      [rs stringForColumn:@"sEmergencyContact1_Phone_Home"]  ,kEmergencyConHNo1 ,
                                      [rs stringForColumn:@"sEmergencyContact1_Phone_Cell"]  ,kEmergencyConCNo1 ,
                                      [rs stringForColumn:@"sEmergencyContactName2"]  ,kEmergencyConName2 ,
                                      [rs stringForColumn:@"sEmergencyContact2_Relationship"]  ,kEmergencyRel2 ,
                                      [rs stringForColumn:@"sEmergencyContact2_Phone_Work"]  ,kEmergencyConWNo2 ,
                                      [rs stringForColumn:@"sEmergencyContact2_Phone_Home"]  ,kEmergencyConHNo2 ,
                                      [rs stringForColumn:@"sEmergencyContact2_Phone_Cell"]  ,kEmergencyConCNo2 ,
                                      [rs stringForColumn:@"dUpdatedDate"]  ,kUpdatedDate ,
                                      [rs stringForColumn:@"migrated"]  ,kMigrated,
                                      [rs stringForColumn:@"nClientID"]  ,kClientId, nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}



//For All Client's Program
- (NSArray	*)getProgramSheets:(NSString *)strClientId{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * from FT%@_ProgramSheet",strClientId];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
 
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"nSheetID"]  ,kSheetId ,
                                      [rs stringForColumn:@"sSheetName"]  ,ksheetName ,
                                      [rs stringForColumn:@"sPurpose"]  ,ksProgramPurpose ,
                                      [rs stringForColumn:@"nUpdateStatus"]  ,kUpdateData , nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}


//For All Client's Notes
- (NSArray	*)getAllNotes:(NSString *)strClientId andBlockNo:(NSString *)strBlockNo{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * from FT%@_ProgramSheet_Blocks",strClientId];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"nSheetID"]  ,kSheetId ,
                                      [rs stringForColumn:@"nBlockID"]  ,kBlockId ,
                                      [rs stringForColumn:@"dBlockdate"]  ,kBlockDate ,
                                      [rs stringForColumn:@"nBlockNo"]  ,kBlockNo ,
                                      
                                      [rs stringForColumn:@"sBlockTitle"]  ,kBlockTitle ,
                                      [rs stringForColumn:@"sBlockNotes"]  ,kBlockNotes ,
                                      
                                      nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}






//For All Program's Notes
- (NSArray	*)getAllNotesUnderProgram:(NSString *)strClientId andSheetId:(NSString *)strSheetId{

    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * from FT%@_ProgramSheet_Blocks where nSheetID = '%@'",strClientId,strSheetId];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"nSheetID"]  ,kSheetId ,
                                      [rs stringForColumn:@"nBlockID"]  ,kBlockId ,
                                      [rs stringForColumn:@"dBlockdate"]  ,kBlockDate ,
                                      [rs stringForColumn:@"nBlockNo"]  ,kBlockNo ,
                                      
                                      [rs stringForColumn:@"sBlockTitle"]  ,kBlockTitle ,
                                      [rs stringForColumn:@"sBlockNotes"]  ,kBlockNotes ,
                                      
                                      nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}




//For All Client's Exercises
- (NSArray	*)getAllExercises:(NSString *)strClientId andProgramId:(NSString *)strProgramID{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * from FT%@_ProgramSheet_BlockData where nSheetID = '%@' ",strClientId,strProgramID];
        
        
        MESSAGE(@"query for get : %@",stringSQL);
        
        
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
        
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [rs stringForColumn:@"nBlockDataID"]  ,kBlockDataId ,
                                      [rs stringForColumn:@"nBlockID"]  ,kBlockId ,
                                      [rs stringForColumn:@"nSheetID"]  ,kSheetId ,
                                      [rs stringForColumn:@"sProgram"]  ,kExercise ,
                                      [rs stringForColumn:@"sLBValue"]  ,kLBValue ,
                                      [rs stringForColumn:@"sRepValue"]  ,kRepValue ,
                                      [rs stringForColumn:@"sSetValue"]  ,kSetValue ,
                                      [rs stringForColumn:@"nBlockNo"]  ,kBlockNo ,
                                      [rs stringForColumn:@"nRowNo"]  ,kRowNo ,
                                      [rs stringForColumn:@"sDate"]  ,kDate ,
                                      [rs stringForColumn:@"sColor"]  ,kColor ,
                                      
                                      nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}




//For All Client's Asseessment Data full sheet
- (NSArray	*)getAllAssessmentSheetFullData:(NSString *)strClientId{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * from FT%@_AssessmentSheet_Data",strClientId];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            MESSAGE(@" [rs stringForColumn: %@", [rs stringForColumn:@"nAssessmentText"]);
            
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                                      
                                      [rs stringForColumn:@"nDetailID"]  ,@"nDetailID" ,
                                      [rs stringForColumn:@"nSheetID"]  ,kSheetId ,
                                      
                                      [rs stringForColumn:@"fCM_RightUpperArm"]  ,kCM_RightUpperArm ,
                                      [rs stringForColumn:@"fCM_RightForeArm"]  ,kCM_RightForeArm ,
                                      [rs stringForColumn:@"fCM_Chest"]  ,kCM_Chest ,
                                      
                                      [rs stringForColumn:@"fCM_Abdominal"]  ,kCM_Abdominal ,
                                      [rs stringForColumn:@"fCM_HipButtocks"]  ,kCM_HipButtocks ,
                                      [rs stringForColumn:@"fCM_RightThing"]  ,kCM_RightThing ,
                                      [rs stringForColumn:@"fCM_RightCalf"]  ,kCM_RightCalf ,
                                      
                                      [rs stringForColumn:@"fCM_Result"]  ,kCM_Result ,
                                      [rs stringForColumn:@"fBMI_Pound_Weight"]  ,kBMI_Pound_Weight ,
                                      [rs stringForColumn:@"fBMI_Pound_Height"]  ,kBMI_Pound_Height ,
                                      [rs stringForColumn:@"fBMI_Pound_Result"]  ,kBMI_Pound_Result ,
                                      [rs stringForColumn:@"fHeight"]  ,kHeight ,
                                      [rs stringForColumn:@"fWeight"]  ,kWeight ,
                                      [rs stringForColumn:@"nRestingHeartBeat"]  ,kRestingHeartBeat ,
                                      [rs stringForColumn:@"sBloodPressure"]  ,kBloodPressure ,
                                      [rs stringForColumn:@"fThree_Male_Chest"]  ,@"fThree_Male_Chest" ,
                                      [rs stringForColumn:@"fThree_Male_Abdomen"]  ,@"fThree_Male_Abdomen" ,
                                      
                                      [rs stringForColumn:@"fThree_Male_Thigh"]  ,@"fThree_Male_Thigh" ,
                                      [rs stringForColumn:@"fThree_Female_Triceps"]  ,@"fThree_Female_Triceps" ,
                                      [rs stringForColumn:@"fThree_Female_Suprailiac"]  ,@"fThree_Female_Suprailiac" ,
                                      [rs stringForColumn:@"fThree_Female_Thigh"]  ,@"fThree_Female_Thigh" ,
                                      [rs stringForColumn:@"fFour_Abdomen"]  ,@"fFour_Abdomen" ,
                                      [rs stringForColumn:@"fFour_Suprailiac"]  ,kFour_Suprailiac ,
                                      [rs stringForColumn:@"fFour_Triceps"]  ,kFour_Triceps ,
                                      [rs stringForColumn:@"fFour_Thigh"]  ,kFour_Thigh ,
                                      
                                      [rs stringForColumn:@"fSeven_Chest"]  ,kSeven_Chest ,
                                      [rs stringForColumn:@"fSeven_Midaxillary"]  ,kSeven_Midaxillary ,
                                      [rs stringForColumn:@"fSeven_Triceps"]  ,kSeven_Triceps ,
                                      [rs stringForColumn:@"fSeven_subscapular"]  ,kSeven_subscapular ,
                                      [rs stringForColumn:@"fSeven_Abdomen"]  ,kSeven_Abdomen ,
                                      [rs stringForColumn:@"fSeven_Suprailiac"]  ,kSeven_Suprailiac ,
                                      [rs stringForColumn:@"fSeven_Thigh"]  ,kSeven_Thigh ,
                                      [rs stringForColumn:@"fThree_Result"]  ,kfThree_Result ,
                                      [rs stringForColumn:@"fFour_Result"]  ,kFour_Result ,
                                      [rs stringForColumn:@"fSeven_Result"]  ,kfSeven_Result ,
                                      [rs stringForColumn:@"fBMI_Metrics_Weight"]  ,kBMI_Metrics_Weight ,
                                      [rs stringForColumn:@"fBMI_Metrics_Height"]  ,kBMI_Metrics_Height ,
                                      [rs stringForColumn:@"fBMI_Metrics_Result"]  ,kBMI_Metrics_Result ,
                                      [rs stringForColumn:@"dUpdated_date"]  ,kUpdateData ,
                                      [rs stringForColumn:@"sAssessment_Goal"]  ,ksAssessment_Goal ,
                                      [rs stringForColumn:@"fGenereal_FatPercent"]  ,kGereralFatPercent ,
                                      [rs stringForColumn:@"fExtra_Value"]  ,kExtraValue ,
                                      [rs stringForColumn:@"sExtra_FieldName"],kExtraFieldName,
                                      
                                      nil];
            
            MESSAGE(@"dictAsseesment Full data : %@ and count: %lu",dicTemp,(unsigned long)dicTemp.count);
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}

//All Template Program
- (NSMutableArray *) getAllTemplatePrograms {
    [self initFMDB];
    NSMutableArray * assessmentList = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"select * from FT_Template_Sheet"];
        FMResultSet * rs = [db executeQuery:query];
        DebugLOG(query);
        while ([rs next]) {
            
            
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [rs stringForColumn:@"nSheetID"],ksheetID,
                                       [rs stringForColumn:@"sSheetName"],ksheetName,
                                       [rs stringForColumn:@"nUpdateStatus"],kUpdateStatus,
                                       [rs stringForColumn:@"nTemplateId"],kTemplateIdForTemplate,
                                       
                                       nil];
            [assessmentList addObject:dictInfo];
        }
    }
    DebugLOG([db lastErrorMessage]);
    return [assessmentList autorelease];
}


//TODO: SUNIL-> TRAINER valuse

//Method for Insert the Trainer Prifle
- (int) insertTrainerProfiles:(NSDictionary *)dictTrainerProfile {
    
    START_METHOD
    [self  initFMDB];
    
    if([db open]) {
        
        //Delete Existing Record
        NSString *queryDelete   =   @"delete from Trainer";
        [db executeUpdate:queryDelete];
        
        
        NSString * query = [NSString stringWithFormat:@"INSERT INTO Trainer (id,username,email,password,user_type_old,app_version,profile_image)values('%@','%@','%@','%@','%@','%@','%@')",[dictTrainerProfile objectForKey:ID], [dictTrainerProfile objectForKey:kFirstName],[dictTrainerProfile objectForKey:EMAIL],[dictTrainerProfile objectForKey:PASSWORD],[dictTrainerProfile objectForKey:USER_TYPE_OLD],[dictTrainerProfile objectForKey:KEY_APP_VERSION],[dictTrainerProfile objectForKey:@"profile_image"]];
        DebugLOG(query);
        
        MESSAGE(@"query--> Insert Traner: %@",query);
        
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        return 1;
    }
    
    END_METHOD
}


//For Trainer Profile
- (NSArray	*)getTrainerProfile{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        NSString *stringSQL = [NSString stringWithFormat:@"select * from Trainer"];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:ID]  ,ID ,
                                      [rs stringForColumn:USERNAME]  ,USERNAME ,
                                      [rs stringForColumn:EMAIL]  ,EMAIL ,
                                      [rs stringForColumn:PASSWORD]  ,PASSWORD ,
                                      [rs stringForColumn:USER_TYPE_OLD]  ,USER_TYPE_OLD ,
                                      [rs stringForColumn:@"profile_image"]  ,@"profile_image",
                                      [rs stringForColumn:KEY_APP_VERSION]  ,KEY_APP_VERSION ,
                                      
                                      nil];
            
            MESSAGE(@"dicTemp--> user imae: %@",dicTemp);
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}


//SUNIL get Perticular assesement
- (NSMutableArray *) getAssessmentSheetsFor:(NSString *)clientID andSheetId:(NSString *)sheetID{
    [self initFMDB];
    NSMutableArray * assessmentList = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_AssessmentSheet where nSheetID='%@'",clientID,sheetID];
        
        FMResultSet * rs = [db executeQuery:query];
        DebugLOG(query);
        while ([rs next]) {
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [rs stringForColumn:@"nSheetID"],ksheetID,
                                       [rs stringForColumn:@"sSheetName"],ksheetName,nil];
            [assessmentList addObject:dictInfo];
        }			
    }	
    DebugLOG([db lastErrorMessage]);
    return [assessmentList autorelease];
}

//Method for get all questions
- (NSArray *) getAllQuestions {
    [self initFMDB];
    NSMutableArray * arrayQuestion = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = @"SELECT * FROM FT_PARQ_Questions";
        
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [rs stringForColumn:@"nQuestionID"],kQuestionId,
                                   [rs stringForColumn:@"nCategoryID"],kQuestionCateId,
                                   [rs stringForColumn:@"sQuestion"],kQuestion,
                                   [rs stringForColumn:@"bBooleanQuestion"],kQuestionBool,
                                   [rs stringForColumn:@"bTextualQuestion"],kQuestionText,nil];
            
            [arrayQuestion addObject:dict];
            
        }			
    }	
    return (NSArray *) [arrayQuestion autorelease];
}

//Method for get all answers
- (NSArray *) getAllAnswersByClientID:(NSString *)strClientId{
    [self initFMDB];
    NSMutableArray * arrayQuestion = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT_PARQ_Answer where nclientId= '%@'",strClientId];
        
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [rs stringForColumn:@"nAnswerID"],kAnswerId,
                                   [rs stringForColumn:@"nClientID"],kClientId,
                                   [rs stringForColumn:@"bAnswer"],kBoolAnswer,
                                   [rs stringForColumn:@"sAnswer"],kTextAnswer,
                                   [rs stringForColumn:@"nQuestionID"],kQuestionId,nil];
            
            [arrayQuestion addObject:dict];
            
        }
    }
    return (NSArray *) [arrayQuestion autorelease];
}





//Method for get all Client's ID
- (NSArray *) getAllClientsID{
    [self initFMDB];
    NSMutableArray * arrayQuestion = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = @"SELECT nClientID FROM FT_CLIENT";
        
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [rs stringForColumn:@"nClientID"],kClientId,nil];
            
            [arrayQuestion addObject:dict];
            
        }
    }
    return (NSArray *) [arrayQuestion autorelease];
}



//Insert Program SUNIL
-(int)insertProgram:(NSDictionary *)dictProgram{
    START_METHOD
    
    [self initFMDB];
    if (![db open]) {
        return 1;
    }
    else {
        
        
        NSString * query = [NSString stringWithFormat:@"Insert into FT%@_ProgramSheet (nSheetID,sSheetName,sPurpose) values ('%@','%@','%@')",[dictProgram objectForKey:kClientId],[dictProgram objectForKey:kSheetId], [self escapeString:[dictProgram objectForKey:ksheetName]],[self escapeString:[dictProgram objectForKey:ksProgramPurpose]]];
        [db executeUpdate:query];
        
        MESSAGE(@"program insert quearty: %@",query);
        
        
        return 0;
    }
}

//Method for insert clients for upgrar=ded users
- (void) insertNewClientUpgraded:(NSMutableDictionary *) dictInfo{
    [self  initFMDB];
    
    if([db open])
    {
        NSString * strName = [self escapeString:[dictInfo objectForKey:kFirstName]];
        NSString * strLastName = [self escapeString:[dictInfo objectForKey:kLastName]];
        NSString * strEmergencyContactName1 = [self escapeString:[dictInfo objectForKey:kEmergencyConName1]];
        NSString * strEmergencyContactRelationship = [self escapeString:[dictInfo objectForKey:kEmergencyRel1]];
        NSString * strEmergencyContactName2 = [self escapeString:[dictInfo objectForKey:kEmergencyConName2]];
        NSString * strEmergencyContactRelationship2 = [self escapeString:[dictInfo objectForKey:kEmergencyRel2]];
        
        NSString * age = [dictInfo objectForKey:kAge];
        NSString * bDate = [dictInfo objectForKey:kBirthDate];
        
        //BOOL   acceptedTerms = [[dictInfo objectForKey:kTermsAccepted]boolValue];
        
        NSDateFormatter * tmpdt = [[[NSDateFormatter alloc] init] autorelease];
        [tmpdt setDateFormat:@"MM/dd/yy hh:mm:ss a"];
        NSString * updatedDate = [tmpdt stringFromDate:[NSDate date]];
        // [tmpdt setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
        [tmpdt setDateFormat:@"MM-dd-yyyy HH:mm:ss +0000"];
        NSString * updatedDate1 = [tmpdt stringFromDate:[NSDate date]];
        NSString * query = @" insert into FT_Client (nClientID,dLastReviewedDate,dUpdatedDate,sFirstName,sLastName,dBirthDate,nAge,";
        query = [query stringByAppendingString:@"sPhone_Day,sPhone_Evening,sPhone_cell,sEmailAddress,sEmailAddress2,"];
        query = [query stringByAppendingString:@"sEmergencyContactName1,sEmergencyContact1_Phone_Work,sEmergencyContact1_Phone_Home,sEmergencyContact1_Phone_Cell,sEmergencyContact1_Relationship,"];
        query = [query stringByAppendingString:@"sEmergencyContactName2,sEmergencyContact2_Phone_Work,sEmergencyContact2_Phone_Home,sEmergencyContact2_Phone_cell,sEmergencyContact2_Relationship,sGender,status,"];
        query = [query stringByAppendingString:@"nLastViewedScreen,sPhysicianName,sPhysicianPhone,sHeight,sWeight) values("];
        query = [query stringByAppendingFormat:@"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',0,' ',' ',' ',' ')",[dictInfo objectForKey:kClientId],updatedDate1,updatedDate,
                 strName,
                 strLastName,
                 bDate,
                 age,
                 [dictInfo objectForKey:kPday],
                 [dictInfo objectForKey:kPeve],
                 [dictInfo objectForKey:kPcell],
                 [dictInfo objectForKey:kEmail1],
                 [dictInfo objectForKey:kEmail2],
                 strEmergencyContactName1,
                 [dictInfo objectForKey:kEmergencyConWNo1],
                 [dictInfo objectForKey:kEmergencyConHNo1],
                 [dictInfo objectForKey:kEmergencyConCNo1],
                 strEmergencyContactRelationship,
                 strEmergencyContactName2,
                 [dictInfo objectForKey:kEmergencyConWNo2],
                 [dictInfo objectForKey:kEmergencyConHNo2],
                 [dictInfo objectForKey:kEmergencyConCNo2],
                 strEmergencyContactRelationship2,
                 [dictInfo objectForKey:kgender],
                  [dictInfo objectForKey:STATUS]
                 ];
        
        MESSAGE(@"Client info query: %@",query);
        [db executeUpdate:query];
        
        
    }
    NSString * str= [db lastErrorMessage];
    if([str isEqualToString:@"not an error"]) {
        
        
        [self createClientTable:[dictInfo objectForKey:kClientId]];
         [self deleteAssessmentSheetClientId:[dictInfo objectForKey:kClientId]];
    }
}


-(void)deleteAssessmentSheetClientId:(NSString *)strClientID{
    START_METHOD
        
        [self initFMDB];
        
        if ([db open]) {
            NSString *str;
            NSString * query = [NSString stringWithFormat:@"delete from FT%@_AssessmentSheet",strClientID];
            [db executeUpdate:query];
            str= [db lastErrorMessage];
            if(![str isEqualToString:@"not an error"]) {
            }
            
            
            
             query = [NSString stringWithFormat:@"delete from FT%@_AssessmentSheet_ImagesData",strClientID];
            [db executeUpdate:query];
            str= [db lastErrorMessage];
            if(![str isEqualToString:@"not an error"]) {
            }
            
            
            
            
            query = [NSString stringWithFormat:@"delete from FT%@_AssessmentSheet_Data",strClientID];
            [db executeUpdate:query];
            str= [db lastErrorMessage];
            if(![str isEqualToString:@"not an error"]) {
            }
            
            
            
        }
    END_METHOD
}

//TODO: SUNIL Save assessment sheet for upgraded user

- (NSInteger) insertNewAssessmentSheetCleintID :(NSString *) clientID sheetName:(NSString *) sheetName andSheetID:(NSString *)strSheetId{
    [self  initFMDB];
    
    if([db open])
    {
        sheetName = [self escapeString:sheetName];
        NSString * query = [NSString stringWithFormat:@"insert into FT%@_AssessmentSheet (nSheetID,sSheetName) values ('%@','%@')",clientID,strSheetId,sheetName];
        
        [db executeUpdate:query];
        
        MESSAGE(@"Query for insert Assessment : %@",query);
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
        else {
            NSString * gender = [self getClientsGender:clientID];
            [[NSUserDefaults standardUserDefaults] setObject:gender forKey:kgender];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString * age = [self getClientsAge:clientID];
            [[NSUserDefaults standardUserDefaults] setObject:age forKey:kAge];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString * sheetID = @"0";
            NSString * query = [NSString stringWithFormat:@"SELECT  max(nSheetID) from FT%@_AssessmentSheet",clientID];
            FMResultSet * rs = [db executeQuery:query];
            while ([rs next]) {
                sheetID = [rs stringForColumn:@"max(nSheetID)"];
            }
            [self insertAssessmentSheetDataFor:clientID sheetID:[sheetID intValue]];
            [self insertAssessmentSheetImagesDataFor:clientID sheetID:[sheetID intValue]];
        }
        
    }
    return 0;
}



//TODO: get All Asseessment Images
- (NSArray *) getAllAssessmentImages:(NSString *)strClientId{
    NSMutableArray * cliensImagesArr = [[[NSMutableArray alloc] init] autorelease];
    [self initFMDB];
    if(![db open]) {
    }
    else {
        NSString *qry=[NSString stringWithFormat:@"select * from FT%d_AssessmentSheet_ImagesData",[strClientId intValue]];
        FMResultSet *rs=[db executeQuery:qry];
        while ([rs next]) {
            NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:[rs stringForColumn:@"nImageID"],kImageID,
                                       [rs stringForColumn:@"sImageLabel"],kImageName,
                                       [rs stringForColumn:@"hasImage"],khasImage,
                                       [rs stringForColumn:@"nSheetID"],kSheetId,
                                       
                                       
                                       nil];
            [cliensImagesArr addObject:dictInfo];
        }
    }
    DebugLOG([cliensImagesArr description]);
    DebugLOG([db lastErrorMessage]);
    return (NSArray *) cliensImagesArr;
}


//TODO: Active inavtive clients SUNIL
- (NSMutableArray *) getClientsListForShow :(NSString *) scope {
    START_METHOD
    [self initFMDB];
    NSMutableArray * arrClientsDetail = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        NSString * query = @"SELECT nclientid,sFirstName,sLastName FROM FT_Client ";
        
        if ([scope isEqualToString:@"0"]) {

            //Set condition where status = NO (In active)
              query = @"SELECT nclientid,sFirstName,sLastName,dLastReviewedDate,status FROM FT_Client  WHERE status = 'NO'  order by sFirstName,sLastName ";
        }
        else if ([scope isEqualToString:@"1"]) {

                //Set condition where status = 1 (Active )
                query = @"SELECT nclientid,sFirstName,sLastName,dLastReviewedDate,status FROM FT_Client WHERE status = 'YES'   order by sFirstName,sLastName ";        }
        else {
            
              query = @"SELECT nclientid,sFirstName,sLastName,dLastReviewedDate,status FROM FT_Client   order by sFirstName,sLastName  ";
        }
        
        
        FMResultSet * rs = [db executeQuery:query];
                MESSAGE(@"getClientsListForShow;> %@",query);
        while ([rs next]) {
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            
            [dict setObject:[rs stringForColumn:@"nclientid"] forKey:kClientId];
            [dict setObject:[rs stringForColumn:@"sFirstName"] forKey:kFirstName];
            [dict setObject:[rs stringForColumn:@"sLastName"] forKey:kLastName];
            [dict setObject:[rs stringForColumn:STATUS] forKey:STATUS];
            
            if ([scope isEqualToString:@"2"]) {
                if ([[rs stringForColumn:@"dLastReviewedDate"] length] != 0) {
                    [dict setObject:[rs stringForColumn:@"dLastReviewedDate"] forKey:kLastReviewdDate];
                    
                    if (dict != nil && [dict count] > 0) {
                        [arrClientsDetail addObject:dict];
                    }
                }
            }
            else {
                if (dict != nil && [dict count] > 0) {
                    [arrClientsDetail addObject:dict];
                }
            }
            [dict release];			
        }			
    }	
    return [arrClientsDetail autorelease];
}


//TODO: Update the clients staus for Active and inactive

- (int ) updateClientStatus:(NSMutableDictionary *) dictInfo{
    [self  initFMDB];
    
    if([db open])
    {
        NSString * query = [NSString stringWithFormat:@"update FT_Client set status = '%@' where nclientid = %@",[dictInfo objectForKey:STATUS],[dictInfo objectForKey: kClientId]];
        
        MESSAGE(@"update status query: %@",query);
        
        
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}


//For ShowDeatil  button Tag

-(NSArray*)getNotesForClient:(NSString*)clientId andSheets:(NSString *)sheetId  andBlock:(NSString *)block {
    
    [self initFMDB];
    NSMutableArray * arrSheetDetail = [[NSMutableArray alloc] init];
    if (![db open]) {
    }
    else {
        
        // SUNIL add :  group by sBlockNotes  ORDER BY nBlockNo  04 May 2016
        NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %@ and nBlockNo = %@  group by sBlockNotes  ORDER BY nBlockNo",clientId,sheetId,block];
        //  NSString * query = [NSString stringWithFormat:@"SELECT * FROM FT%@_ProgramSheet_Blocks where nSheetID = %@  and sBlockTitle='' ORDER BY nBlockNo",clientId,sheetId];
        
        MESSAGE(@"query-> Get anotes : %@",query);
        
        
        FMResultSet * rs = [db executeQuery:query];
        while ([rs next]) {
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            NSString *strNotes = [rs stringForColumn:@"sBlockNotes"];
            if([strNotes isKindOfClass:[NSNull class]] || [strNotes isEqual:nil] || !strNotes.length > 0 ) {
                strNotes = @"";
            }
            
            [dict setObject:[rs stringForColumn:@"dBlockDate"] forKey:kBlockDate];
            [dict setObject:strNotes forKey:kBlockNotes];
            [dict setObject:[rs stringForColumn:@"nBlockID"] forKey:kBlockId];
            [dict setObject:[rs stringForColumn:@"nSheetID"] forKey:kSheetId];
            [dict setObject:[rs stringForColumn:@"nBlockNo"] forKey:kBlockNo];
            [arrSheetDetail addObject:dict];
            [dict release];
        }
    }
    return [arrSheetDetail autorelease];
}



//Sunil for the delete the curret workout for copy paste the current workouts

-(int)deleteWorkoutForCopyPaste:(NSString*)sheetId forClient:(NSString *)clientId andWorkOutBlockId:(NSString*)blockId {
    [self initFMDB];
    if ([db open]) {
        
        NSString *str;
        NSString * query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_BlockData where nSheetID = %d and nBlockId = %d",clientId,[sheetId intValue],[blockId intValue]];
        // DLOG(@"query To delete block data %@",query);
        
        [db executeUpdate:query];
        
        MESSAGE(@"deleteWorkoutForCopyPaste--> _ProgramSheet_BlockData: %@",query);
        
        
        str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        } else {
            query = [NSString stringWithFormat:@"delete  from FT%@_ProgramSheet_Blocks where nSheetID = %d and nBlockId = %d",clientId,[sheetId intValue],[blockId intValue]];
            [db executeUpdate:query];
            
            MESSAGE(@"deleteWorkoutForCopyPaste--> _ProgramSheet_Blocks: %@",query);
            
            str= [db lastErrorMessage];
            if (![str isEqualToString:@"not an error"]) {
                return 1;
            }
        }
    }
    return 0;
}



//TODO:  FOR MIGARTION

-(void)addNewFieldToClientTable{
    NSLog(@"addNewFieldToClientTable---> ");
    [self initFMDB];
    if([db open]) {
        
            NSLog(@"addNewFieldToClientTable--->if open ");
        
        BOOL  isAdded = [db columnExists:@"FT_Client" columnName:@"migrated"];
        
            NSLog(@"addNewFieldToClientTable---> isAdded: %d",isAdded);
        NSString * query;
        
        
        if(!isAdded) {
            
            
            query = [NSString stringWithFormat:@"ALTER TABLE \"FT_Client\"  ADD COLUMN \"migrated\" VARCHAR DEFAULT ''"];
            
            NSLog(@"addNewFieldToClientTable---> query: %@",query);

            
            [db executeUpdate:query];
            
            NSString * str= [db lastErrorMessage];
            
            if(![str isEqualToString:@"not an error"]) {
                NSLog(@"addNewFieldToClientTable---> last error NOT : %@",str);

            }
            else {
                
                NSLog(@"addNewFieldToClientTable---> last error : %@",str);

            }
            
        }
        
        
        
        BOOL  isAddedNew = [db columnExists:@"FT_Client" columnName:@"is_client_info"];
        NSLog(@"addNewFieldToClientTable--->isAddedNew : %d",isAddedNew);

        NSString * queryNew;
        
        if(!isAddedNew) {
                        
            queryNew = [NSString stringWithFormat:@"ALTER TABLE \"FT_Client\"  ADD COLUMN \"is_client_info\" VARCHAR DEFAULT ''"];
            
            NSLog(@"addNewFieldToClientTable--->isAddedNew queryNew : %@",queryNew);

            
            [db executeUpdate:queryNew];
            
            NSString * str= [db lastErrorMessage];
            
            if(![str isEqualToString:@"not an error"]) {
                NSLog(@"addNewFieldToClientTable---> last error NO client : %@",str);

            }
            else {
                
                NSLog(@"addNewFieldToClientTable---> last error  new client : %@",str);

            }
            
        }
        
        
        
    }
    
}



//TODO: Update the clients Migration status

- (int ) updateClientMigrationStatus:(NSMutableDictionary *) dictInfo{
    [self  initFMDB];
    
    if([db open])
    {
        NSString * query = [NSString stringWithFormat:@"update FT_Client set migrated = 'YES' where nclientid = %@",[dictInfo objectForKey: kClientId]];
        
        MESSAGE(@"update status query: %@",query);
        
        
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}



//For All Clients not migrated
- (NSArray	*) getClientsCountWithoutMigration{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        
        
        NSString *stringSQL = [NSString stringWithFormat:@"SELECT COUNT(*) as count  From FT_Client  where migrated !='YES'"];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"count"]  ,@"count" , nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}



//For All Clients Count
- (NSArray	*) getTotalClientsCount{
    
    __autoreleasing NSMutableArray * arrSheetData = [[NSMutableArray alloc]init];
    [self initFMDB];
    if (![db open]) {
    }
    else{
        
        
        NSString *stringSQL = [NSString stringWithFormat:@"SELECT COUNT(*) as count  From FT_Client"];
        FMResultSet *rs=[db executeQuery:stringSQL];
        while ([rs next]) {
            
            NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:  [rs stringForColumn:@"count"]  ,@"count" , nil];
            
            [arrSheetData addObject:dicTemp];
            
        }
        stringSQL = nil;
        rs = nil;
    }
    NSArray * arrtemp = [NSArray arrayWithArray:arrSheetData];
    [arrSheetData release];
    return arrtemp;
}




- (NSString *) getClientsInfoStaus :(NSString *) clientID {
    [self initFMDB];
    NSString * is_client_info = @"";
    if (![db open]) {
    }
    else {
        NSString * query = [NSString stringWithFormat:@"SELECT is_client_info FROM FT_Client where nclientid = %d",[clientID intValue]];
        FMResultSet * rs = [db executeQuery:query];
        
        while ([rs next]) {
            is_client_info = [rs stringForColumn:@"is_client_info"];
        }
    }
    return is_client_info;
}


//TODO: Update the clients Migration status

- (int ) updateClientInfoStatus:(NSMutableDictionary *) dictInfo{
    [self  initFMDB];
    
    if([db open])
    {
        NSString * query = [NSString stringWithFormat:@"update FT_Client set is_client_info = 'YES' where nclientid = %@",[dictInfo objectForKey: kClientId]];
        
        MESSAGE(@"update status query: %@",query);
        
        
        [db executeUpdate:query];
        NSString * str= [db lastErrorMessage];
        if(![str isEqualToString:@"not an error"]) {
            return 1;
        }
    }
    return 0;
}


@end




