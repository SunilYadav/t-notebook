//
//  ProgramTemplateViewController.m
//  T-Notebook
//
//  Created by WLI on 23/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//
// Purpose : Show listing of program templates

#import "ProgramTemplateViewController.h"

#import "DatePickerController.h"
#import "ProgramGetDetailVC.h"
#import "ProgramGetWorkoutVC.h"
#import "ProgramNameViewController.h"
#import "UpdateNotesVC.h"
#import "FileUtility.h"
#import "PopoverController.h"
#import "OptionsViewVC.h"


#import "AddTemplateSheet.h"
#import "ProgramCustomCell.h"
#import "SelectAllTemplatesVC.h"
#import "OptionsViewVC.h"
#import "UpdateTemplateVC.h"

@interface ProgramTemplateViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrayProgramData;
    NSMutableArray *arrSheetsList;
    NSArray *arrayBlocks;
    __weak IBOutlet UIButton *btnAddNewLine;
    __weak IBOutlet UILabel *lblTemplateName;
    __weak IBOutlet UILabel *lblWorkOutName;
    __weak IBOutlet UILabel *lblSheetName;
    __weak IBOutlet UITableView *tblProgramView;
    __weak IBOutlet UILabel *lblInstruction;
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UIButton *btnDate;
    
    NSInteger	currentSheetIndex;
    NSInteger	currentBlockIndex;
    NSString *strSheetName;
    SelectAllTemplatesVC *objSelectAllTemplatesVC;
    UIPopoverController* popoverController;
    OptionsViewVC *objOptionsVC;
    NSMutableArray *arrayCopyLines;
    BOOL isWokoutCopied;
    
    IBOutlet UIImageView *imgvNavBar;
    IBOutlet UIButton *btnHome;
    IBOutlet UIButton *btnOption;
    IBOutlet UIImageView *imgvBackground;
    IBOutlet UILabel *lblTraining;
    IBOutlet UILabel *lblNotebook;
    IBOutlet UIView *viewTbl;
    IBOutlet UIView *viewHeader;
    IBOutlet UIButton *btnAddWorkout;
    IBOutlet UILabel *lblDate;
    IBOutlet UIButton *btnSheetDate;
    IBOutlet UIView *viewMain;
    IBOutlet UIView *viewPrevNext;
    IBOutlet UILabel *lblWorkout;
    IBOutlet UIButton *btnNext;
    IBOutlet UILabel *lblNextWorkout;
    IBOutlet UIButton *btnWorkout;
    IBOutlet UIView *viewTable;
    IBOutlet UIButton *btnSheetName;
    __weak IBOutlet UILabel *lblPrevWorkout;
    

    __weak IBOutlet UIButton *btnPrevWorkout;
    
    IBOutlet UILabel *lblCopy;
    IBOutlet UIImageView *imgvFirstLine;
    IBOutlet UILabel *lblExercise;
    IBOutlet UIImageView *imgvSecondLine;
    IBOutlet UILabel *lblWeight;
    IBOutlet UIImageView *imgvThirdLine;
    IBOutlet UILabel *lblSet;
    IBOutlet UIImageView *imgvFourthLine;
    IBOutlet UILabel *lblRep;
    IBOutlet UIImageView *imgvFifthLine;
    IBOutlet UILabel *lblRestTime;
    IBOutlet UIImageView *imgvSixLine;
    IBOutlet UILabel *lblDelete;
}

@property (nonatomic , strong) SelectAllTemplatesVC *objSelectAllTemplatesVC;
@property (nonatomic) BOOL                                          isLandScapeMode;

- (IBAction)btnEditProgramAction:(id)sender;
- (IBAction)btnEditWorkoutAction:(id)sender;
@end

@implementation ProgramTemplateViewController

@synthesize objSelectAllTemplatesVC;
static UIPopoverController *popoverControllerDate = nil;
static UIDatePicker *datePicker = nil;

-(void)dealloc{
    objSelectAllTemplatesVC.delegate = nil;
    objSelectAllTemplatesVC = nil;
}
//-----------------------------------------------------------------------

#pragma mark - TableView mMethods

//-----------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
//-----------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrayProgramData.count;
}

//-----------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProgramCustomCell *cell = (ProgramCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setBackgroundColor:[UIColor clearColor]];
    if (cell == nil) {
        cell = [[ProgramCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
           cell.setLandscapeMode = TRUE;
        cell.btnCopy.frame = CGRectMake(0,10 ,45 , 100);
        cell.btnDelete.frame = CGRectMake(897,71 ,32 , 29);
        cell.btnEditLine.frame = CGRectMake(897,25 ,32 , 29);
        
        cell.lblExercise.frame = CGRectMake(50,3 ,500 , 101);
        cell.lblReps.frame = CGRectMake(698,3 ,95 , 101);
        cell.lblRestTime.frame = CGRectMake(796,3 ,82 , 101);
        cell.lblSet.frame = CGRectMake(604,3,94 , 101);
        cell.lblWeight.frame = CGRectMake(507,3 ,95 , 101);
        cell.imgvFirstLine.frame = CGRectMake(46,0 ,1 , 135);
        cell.imgvSecondLine.frame = CGRectMake(508,0 ,1 , 135);
        cell.imgvThirdLine.frame = CGRectMake(605,0 ,1 , 135);
        cell.imgvFourthLine.frame = CGRectMake(699,0 ,1 , 135);
        cell.imgvFifthLine.frame = CGRectMake(794,0 ,1 , 135);
        cell.imgvSixLine.frame = CGRectMake(879,0 ,1 , 135);
        cell.lblCellsepetator.frame = CGRectMake(0, 134, 1024, 1);
        
    }
    else
    {
        cell.setLandscapeMode = FALSE;
        cell.btnCopy.frame = CGRectMake(0,10 ,45 , 100);
        cell.btnDelete.frame = CGRectMake(684,71 ,32 , 29);
        cell.btnEditLine.frame = CGRectMake(683,25 ,32 , 29);
        
        cell.lblExercise.frame = CGRectMake(60,3 ,248 , 101);
        cell.lblReps.frame = CGRectMake(501,3 ,95 , 101);
        cell.lblRestTime.frame = CGRectMake(599,3 ,68 , 101);
        cell.lblSet.frame = CGRectMake(407,3,95 , 101);
        cell.lblWeight.frame = CGRectMake(311,3 ,95 , 101);
        cell.imgvFirstLine.frame = CGRectMake(46,0 ,1 , 135);
        cell.imgvSecondLine.frame = CGRectMake(308,0 ,1 , 135);
        cell.imgvThirdLine.frame = CGRectMake(403,0 ,1 , 135);
        cell.imgvFourthLine.frame = CGRectMake(499,0 ,1 , 135);
        cell.imgvFifthLine.frame = CGRectMake(593,0 ,1 , 135);
        cell.imgvSixLine.frame = CGRectMake(668,0 ,1 , 135);
        cell.lblCellsepetator.frame = CGRectMake(0, 134, 730, 1);
        
    }
    
    
    if (isWokoutCopied) {
        [cell.btnCopy setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
    }
    else{
       [cell.btnCopy setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
    }
   
    [cell.btnCopy setTag:indexPath.row+currentBlockIndex+currentSheetIndex];
    [cell.btnDelete setTag:indexPath.row];
    [cell.btnEditLine setTag:indexPath.row];
    
    
    
    NSInteger selectedTag =  cell.btnCopy.tag-(currentBlockIndex+currentSheetIndex);
    
    if ([cell.btnCopy.currentImage isEqual:[UIImage imageNamed:@"copy"]]) {
        
        if ([arrayCopyLines containsObject:[arrayProgramData objectAtIndex:selectedTag]]){
            [cell.btnCopy setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];

        }
        else{
            [cell.btnCopy setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];

        }
        
        
    }
    
    if ([arrayProgramData count]>0) {
        lblInstruction.hidden = YES;
        [cell configCell:[arrayProgramData objectAtIndex:indexPath.row]];
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        }
        else{
        [tblProgramView setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"tableHorizontalLine"]]];
        }
    } else {
       lblInstruction.hidden = NO;
    }
    
    return cell;
}

//-----------------------------------------------------------------------

#pragma mark - WorkOut && New Line && Update Template Delegate Methods

//-----------------------------------------------------------------------

-(void) editNewLine:(NSDictionary *)detailsExercise{
    START_METHOD
    MESSAGE("editNewLine--> %@",detailsExercise);
    
    NSDictionary *dic = @{kBlockId : [detailsExercise valueForKey:@"nBlockID"],
                         kSheetId : [detailsExercise valueForKey:@"nSheetID"],
                         kExercise :[detailsExercise valueForKey:@"sProgram"],
                         kLBValue :[detailsExercise valueForKey:@"sLBValue"],
                         kRepValue :[detailsExercise valueForKey:@"sRepValue"],
                         kSetValue : [detailsExercise valueForKey:@"sSetValue"],
                         kEXDate :[detailsExercise valueForKey:@"dEXDate"],
                         kBlockNo :[detailsExercise valueForKey:@"nBlockNo"],
                         kRowNo :[detailsExercise valueForKey:@"nRowNo"],
                         kBlockDataId:[detailsExercise valueForKey:kBlockDataId],
                          kColor:[detailsExercise valueForKey:@"selectedColor"],
                          @"RowId":[detailsExercise valueForKey:@"RowId"],
                          ACTION:@"updateExercise",
                          kDate:[detailsExercise  objectForKey:kEXDate],
                          PROGRAM_EXERCISE_ID:[detailsExercise  objectForKey:kBlockDataId],
                         };
    
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
//TODO: SUNIL -> EDIT EXERCISE
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
        
        //Post Request
        [self postRequestForTemplateExercise:dic];
        
    }else{

        [[DataManager initDB]  updateTemplateProgrammedData:dic];
        
        [self fillArrayWithData];
        NSInteger rowIdReload = [[detailsExercise valueForKey:@"RowId"]integerValue];
        [tblProgramView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIdReload inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
    }
   
}
//-----------------------------------------------------------------------

-(void)addNewSheet:(NSDictionary*)dictNewLineData{
    START_METHOD
    [self  fillArrayWithData];
    NSInteger  blockNum = 0;
    if (arrayBlocks.count) {
        blockNum = [[[arrayBlocks objectAtIndex:0] valueForKey:@"nBlockNo"] integerValue];
    }
    self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    
        
    NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID],kBlockId,
                          [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                          [dictNewLineData valueForKey:@"exercise"],kExercise,
                          [dictNewLineData valueForKey:@"weight"],kLBValue,
                          [dictNewLineData valueForKey:@"rep"],kRepValue,
                          [dictNewLineData valueForKey:@"restTime"],kEXDate,
                          [dictNewLineData valueForKey:@"selectedColor"],kColor,
                          [dictNewLineData valueForKey:@"set"],@"sSetValue",
                          [NSString stringWithFormat:@"%ld",(long)blockNum],kBlockNo,
                          [NSString stringWithFormat:@"%i",0],kRowNo,
                          [dictNewLineData valueForKey:@"restTime"],kDate,
                          @"addExercise",ACTION, nil];
    
    
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
  //TODO: SUNIL -> ADD EXERCISE
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
        
        NSDictionary * dicTemp = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID],kBlockId,
                              [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                              [dictNewLineData valueForKey:@"exercise"],kExercise,
                              [dictNewLineData valueForKey:@"weight"],kLBValue,
                              [dictNewLineData valueForKey:@"rep"],kRepValue,
                              [dictNewLineData valueForKey:@"restTime"],kEXDate,
                              [dictNewLineData valueForKey:@"selectedColor"],kColor,
                              [dictNewLineData valueForKey:@"set"],@"sSetValue",
                              [NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID],kBlockNo,
                              [NSString stringWithFormat:@"%i",0],kRowNo,
                              [dictNewLineData valueForKey:@"restTime"],kDate,
                              @"addExercise",ACTION, nil];
        
        
        //Invoke method for post request
        [self postRequestForTemplateExercise:dicTemp];
        
    }else{
        [[DataManager initDB] insertTemplate_ProgramedData:dic];
      
        
        [self fillArrayWithData];
        [self setSheetValues];
        [tblProgramView reloadData];
    }

    
}

//-----------------------------------------------------------------------

-(void) addNewWorkout:(NSString *)WorkoutSheet :(NSString*)strEdit{
    
    START_METHOD
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    if ([strEdit isEqualToString:@"FromEdit"]) {
        NSInteger blockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
        NSString *strDate = [[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"dBlockdate"];
        
//TODO: SUNIL-> Workoout EDIT
        
            if ([strUpgradeStatus isEqualToString:@"YES"]) {

                //Create Dict for POst on server
                NSMutableDictionary *dictWorkouts = [[NSMutableDictionary alloc]init];
                [dictWorkouts setValue:strDate forKey:kDate];
                [dictWorkouts setValue:[NSString stringWithFormat:@"%ld",(long)blockID] forKey:kBlockId];
                 [dictWorkouts setValue:[NSString stringWithFormat:@"%ld",(long)blockID] forKey:kBlockNo];
                [dictWorkouts setValue:[NSString stringWithFormat:@"%ld",self.intCurrentSheetID] forKey:kSheetId];
                [dictWorkouts setValue:WorkoutSheet forKey:ksheetName];
                [dictWorkouts setValue:WorkoutSheet forKey:ksBlockTitle];
                [dictWorkouts setValue:@"updateWorkout" forKey:ACTION];
                
                //Inoke for post server
                [self postRequestForTemplateWorkouts:dictWorkouts];
                
            }else{
                
                [[DataManager initDB] updateBlockDate_Title:self.intCurrentSheetID :blockID :strDate :WorkoutSheet];
            }
        
    }
    else if ([strEdit isEqualToString:@"EditSheet"]){
        

//TODO: SUNIL-> template Program EDIT
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            //Create Dict for POst on server
            NSMutableDictionary *dictPrograms = [[NSMutableDictionary alloc]init];
            [dictPrograms setValue:[NSString stringWithFormat:@"%ld",self.intCurrentSheetID] forKey:kSheetId];
            [dictPrograms setValue:WorkoutSheet forKey:ksheetName];
            [dictPrograms setValue:@"updateProgram" forKey:ACTION];
            
            //Inoke for post server
            [self postRequestForTemplatePrograms:dictPrograms];
            
        }else{
            
            
            [[DataManager initDB] updateTemplateSheetName:WorkoutSheet forSheetID:self.intCurrentSheetID];
            [arrSheetsList setArray:[[DataManager initDB]getTemplateSheetList:self.intCurrentTemplateID]];
        }
        
        
    }
    else{
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kAppDateFormat];
        NSString *strDate = [formatter stringFromDate:[NSDate date]];
        
        
   
        NSDictionary * dicBlockData = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"], @"sheetID",
                                       [[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"], kSheetId,
                                       [NSNumber numberWithInt:1],@"blockNo",
                                       [NSNumber numberWithInt:1],kBlockNo,
                                       WorkoutSheet,@"sBlockTitle",
                                       strDate,@"dBlockDate",
                                       strDate,kDate,
                                       @"addWorkout",ACTION,
                                       [commonUtility retrieveValue:KEY_TRAINER_ID], @"user_id"  ,
                                       nil];
        
//TODO: SUNIL-> template ADD WORKOUT
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            //Invoke for post on server
            [self postRequestForTemplateWorkouts:dicBlockData];
            
            
        }else{
            [[DataManager initDB] insertTemplateSheet_Blocks:dicBlockData];
            dicBlockData = nil;
            
            //Get Blocks
            arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
            currentBlockIndex = arrayBlocks.count-1;
            self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
        }
       
    }
    
    if (![strUpgradeStatus isEqualToString:@"YES"]) {
    
    [self fillArrayWithData];
    [self setSheetValues];
    [tblProgramView reloadData];
    }
    
}

//-----------------------------------------------------------------------


-(void) updateTemplateName:(NSString *)templateName {
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    BOOL boolAvailableOrNot = false;
    NSArray * arrtemp = [NSArray arrayWithArray:[[DataManager initDB]getAllTemplatesDetail]];
    
    for(int i = 0; i< [arrtemp count]; i++){
        if([[[arrtemp objectAtIndex:i]valueForKey:@"templateName"] isEqualToString:templateName]){
            boolAvailableOrNot = YES;
        }
    }
    arrtemp = Nil;
    if(boolAvailableOrNot){
        DisplayAlertWithTitle(NSLocalizedString(@"Template Name Exists!", nil), kAppName);
    }else{
        
        
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            //TODO: POST REQUEST FOR UPDTE TMAPLate NAME
            //Create Dict for POst on server
            NSMutableDictionary *dictNewTemplate = [[NSMutableDictionary alloc]init];
            [dictNewTemplate setValue:[NSString stringWithFormat:@"%ld",self.intCurrentTemplateID] forKey:@"templateId"];

            [dictNewTemplate setValue:[NSString stringWithFormat:@"%ld",self.intCurrentTemplateID] forKey:@"nId"];

            [dictNewTemplate setValue:templateName forKey:@"templateName"];
            [dictNewTemplate setValue:templateName forKey:@"sTemplate_Name"];
            [dictNewTemplate setValue:@"updateTemplateName" forKey:ACTION];
            
            //Invoke method for post Updated name of Template
            [self postRequestForTemplate:dictNewTemplate];
            
            
            
        }else{
            
            int fail = [[DataManager initDB] updateTemplateName:templateName oldTemplate:self.intCurrentTemplateID];
            if (!fail) {
                
                  //Alert view
                [[[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Template Updated Successfully", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
                lblTemplateName.text = templateName;
            } else {
                
                [[[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Template Updated Successfully", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                
            }

        }
        
       
    }
}

//-----------------------------------------------------------------------

#pragma mark - AlertView Delegate methods

//-----------------------------------------------------------------------


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{

    START_METHOD
        if (buttonIndex == 0) {
            
            
            
            
            
            NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            if ([strUpgradeStatus isEqualToString:@"YES"]) {
                
                
                
                //Create dictionaryb for send param in api for Trainer's Profile
                NSDictionary *params = @{
                                         ACTION                 :   @"deleteExercise",
                                         @"program_exercise_id"               :[[arrayProgramData objectAtIndex:(int)alertView.tag] valueForKey:@"nBlockDataID"],
                                         };
                
                
                //Invoke Method for Post Request To delete Exercise
                [self postRequestForDeleteExercise: params];
                
            }else{
                
                //Invoke for delete from Local
                [self deleteTemplateExercise: [[[arrayProgramData objectAtIndex:(int)alertView.tag] valueForKey:@"nBlockDataID"] intValue]];
            }
            
            
            
    }
}

//-----------------------------------------------------------------------

#pragma mark - Action Methods

//-----------------------------------------------------------------------

- (IBAction)btnEditProgramAction:(id)sender
{
    ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
    UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
    ProgramGetWorkoutVCOBJ.strSheetName = [[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetName"];
    ProgramGetWorkoutVCOBJ.delegate =(id) self;
    ProgramGetWorkoutVCOBJ.isEditTemplateSheet = true;
    [getWorkoutNavigationController.navigationBar setTranslucent:NO];
    getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(500, 116);
        getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
}


- (IBAction)btnEditWorkoutAction:(id)sender
{
    ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
    UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
    ProgramGetWorkoutVCOBJ.strSheetName =[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"sBlockTitle"];
    ProgramGetWorkoutVCOBJ.delegate =(id) self;
    [getWorkoutNavigationController.navigationBar setTranslucent:NO];
    getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(500, 116);
        getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
}


-(IBAction)btnDleteLineClicked:(id)sender{
    
    START_METHOD
//    [commonUtility alertMessage:@"Under Development!"];
//    return;
    
    
    [self hideOptionView];
    UIButton *selectedBtn = (UIButton*)sender;
    
    UIAlertView * alertShow = [[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Are you sure you want to delete this Exercise?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes",nil),NSLocalizedString(@"No",nil),nil];
    [alertShow setTag:selectedBtn.tag];
    [alertShow show];

}

//-----------------------------------------------------------------------

-(IBAction)btnCopyLineClicked:(UIButton*)sender{
    [self hideOptionView];
    NSInteger selectedTag =  sender.tag-(currentBlockIndex+currentSheetIndex);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([sender.currentImage isEqual:[UIImage imageNamed:@"copy"]]) {
        
        [sender setImage:[UIImage imageNamed:@"copy-secelted"] forState:UIControlStateNormal];
        if ([arrayCopyLines containsObject:[arrayProgramData objectAtIndex:selectedTag]]){
        }
        else{
            [arrayCopyLines addObject:[arrayProgramData objectAtIndex:selectedTag]];
            [defaults setObject:arrayCopyLines forKey:kIsCopyLine];
        }
        
        
    }
    else if ([sender.currentImage isEqual:[UIImage imageNamed:@"copy-secelted"]]){
        
        if ([arrayCopyLines containsObject:[arrayProgramData objectAtIndex:selectedTag]]) {
            [arrayCopyLines removeObject:[arrayProgramData objectAtIndex:selectedTag]];
        }
        [sender setImage:[UIImage imageNamed:@"copy"] forState:UIControlStateNormal];
        [defaults setObject:arrayCopyLines forKey:kIsCopyLine];
    }
    
    if (arrayCopyLines.count) {
        btnAddNewLine.tag = 1;
        [btnAddNewLine setTitle:NSLocalizedString(@"Paste New Exercise", nil) forState:0];
    }
    else{
        btnAddNewLine.tag = 0;
        [btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:0];
    }

    [defaults synchronize];
    
    
}

//-----------------------------------------------------------------------

- (IBAction)btnEditLineClicked:(UIButton*)sender {
     [self hideOptionView];
    ProgramGetDetailVC *ProgramGetDataVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"progDetail"];
    UINavigationController* programGetDetailNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetDataVCOBJ];
    ProgramGetDataVCOBJ.delegate = (id)self;
    ProgramGetDataVCOBJ.dictLineToShow = [arrayProgramData objectAtIndex:sender.tag];
    ProgramGetDataVCOBJ.strRowIdtoReload = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    [programGetDetailNavigationController.navigationBar setTranslucent:NO];
    programGetDetailNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    programGetDetailNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    
    CGPoint frameSize = CGPointMake(500, 500);
    if (iOS_8) {
        programGetDetailNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    else{
    }
    
    [self.view.window.rootViewController presentViewController:programGetDetailNavigationController animated:YES completion:^{
    }];
    
}

//-----------------------------------------------------------------------

-(void)seDateFromDatePicker{
    START_METHOD
    NSDate * date = datePicker.date;
    NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
    [dateformatter setDateFormat:kAppDateFormat];
    NSString *strDate = [dateformatter stringFromDate:date];
    [btnDate setTitle:strDate forState:UIControlStateNormal];
    
        [btnSheetDate setTitle:strDate forState:UIControlStateNormal];
    
    
    int blockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    NSString *strBlockTitle = [[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"sBlockTitle"];
    
    //Update BlockDate..
   [[DataManager initDB] updateBlockDate_Title:self.intCurrentSheetID :blockID :strDate :strBlockTitle];
    
    [popoverControllerDate dismissPopoverAnimated:YES];
    dateformatter = nil;
    END_METHOD
    
}

//-----------------------------------------------------------------------

- (IBAction)btnDateClicked:(id)sender {
    START_METHOD
    
    if (popoverControllerDate == nil) {
        
        datePicker = nil;
        UIViewController* popoverContent = [[UIViewController alloc] init];
        UIView*  popoverView = [[UIView alloc] init];
        popoverView.backgroundColor = [UIColor whiteColor];
        
        datePicker=[[UIDatePicker alloc]init];
        datePicker.frame=CGRectMake(0,44,320, 216);
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setMinuteInterval:5];
        [datePicker setTag:10];
        [popoverView addSubview:datePicker];
        
        UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
        [btnDone addTarget:self action:@selector(seDateFromDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDone.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
        
        
        CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnDone.titleLabel.text, nil) font:btnDone.titleLabel.font height:btnDone.titleLabel.frame.size.height];
        
        btnDone.frame = CGRectMake(5, 7, rect.size.width + 10, 30);
        btnDone.layer.cornerRadius = 4.0;
        btnDone.backgroundColor = kAppTintColor;
        [popoverView addSubview:btnDone];
        
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(btnDone.frame.origin.x + btnDone.frame.size.width + 5, 4, 320 - rect.size.width + 10, 36)];
        [lblTitle setText:NSLocalizedString(@"Select Date", nil)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [lblTitle setTextColor:kAppTintColor];
        lblTitle.font = [UIFont fontWithName:@"ProximaNova-Regular" size:19];
        [popoverView addSubview:lblTitle];
        
        UILabel *lblLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 42, 320, 1)];
        [lblLine setBackgroundColor:kAppTintColor];
        [popoverView addSubview:lblLine];
        
        popoverContent.view = popoverView;
        
        popoverControllerDate = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
        popoverControllerDate.delegate=(id)self;
        
        
        
    }
    

    [popoverControllerDate setPopoverContentSize:CGSizeMake(320, 264) animated:NO];
    
    if([popoverControllerDate isPopoverVisible]) {
        [popoverControllerDate dismissPopoverAnimated:YES];
         popoverControllerDate = nil;
    } else {
        [popoverControllerDate presentPopoverFromRect:btnSheetDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES]; //btnSheetDate.frame
        
    }
    
    

  END_METHOD
}

//-----------------------------------------------------------------------


- (void)reloadData:(BOOL)animated with :(NSString *)string
{
    [tblProgramView reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionPush];
        if ([string isEqualToString:@"Next"]) {
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [animation setSubtype:kCATransitionFromRight];
        } else {
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
            [animation setSubtype:kCATransitionFromLeft];
        }
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.2];
        [[tblProgramView layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}

//-----------------------------------------------------------------------

- (IBAction)PrevNextBtnTap:(id)sender {
    isWokoutCopied = NO;
    if([sender tag] == 501) {
         [self hideOptionView];
        
        if (currentBlockIndex>0) {
            
            currentBlockIndex = currentBlockIndex -1;
            self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
            [self fillArrayWithData];
            [self reloadData:YES with:@"Prev"];
            [self setSheetValues];
            [tblProgramView reloadData];
        }
        
        else if (currentSheetIndex> 0)
        {
            currentSheetIndex = currentSheetIndex -1;
            self.intCurrentSheetID = [[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"] intValue];
            
            arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
            currentBlockIndex = arrayBlocks.count-1;
            self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
            
            [self fillArrayWithData];
            [self reloadData:YES with:@"Prev"];
            [self setSheetValues];
            [tblProgramView reloadData];
        }
        
    } else{
        
        if (currentBlockIndex<[arrayBlocks count]-1) {
            
            currentBlockIndex = currentBlockIndex +1;
            self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
            [self fillArrayWithData];
            [self reloadData:YES with:@"Next"];
            [self setSheetValues];
            [tblProgramView reloadData];
        }
        
       else if (currentSheetIndex < [arrSheetsList count]-1)
        {
            currentSheetIndex = currentSheetIndex +1;
            self.intCurrentSheetID = [[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"] intValue];
            
            arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
            currentBlockIndex = 0;
            self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
            
            [self fillArrayWithData];
            [self reloadData:YES with:@"Next"];
            [self setSheetValues];
            [tblProgramView reloadData];
        }
    }
}

//-----------------------------------------------------------------------

- (IBAction)btnHomeClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}

//-----------------------------------------------------------------------

-(void)hideOptionView{
    
    if (objOptionsVC) {
        CGRect actualFrame = objOptionsVC.view.frame;
        [UIView animateWithDuration:0.5 animations:^{
            
            CGRect modifiedFrame = actualFrame;
            modifiedFrame.origin.y =actualFrame.origin.y ;
            modifiedFrame.size.height = 0;
            objOptionsVC.view.frame = modifiedFrame;
            objOptionsVC.view.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            [objOptionsVC.view removeFromSuperview];
            objOptionsVC = nil;
            
        }];
 
    }
    
}

//-----------------------------------------------------------------------

- (IBAction)btnOptionsClicked:(UIButton *)sender {
    
    if(objOptionsVC) {
        
        [self hideOptionView];
    }
    else  {
      //  kProgramTemplate
        objOptionsVC = [[OptionsViewVC alloc]initWIthFrame:CGRectMake(self.view.frame.size.width - 250,75, 250, 160) andcontroller:kProgramTemplate];
        
        objOptionsVC.strClientId = [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID];
        objOptionsVC.currentSheetId = [NSString stringWithFormat:@"%ld",(long)self.self.intCurrentBlockID] ;
        objOptionsVC.typeOfOptions = kProgramTemplate;
        objOptionsVC.currentSheetId = [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID];
        objOptionsVC.currentBlockId = [NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID];
        objOptionsVC.optionDelegate =(id) self;
        [self.view addSubview:objOptionsVC.view];
        
        CGRect actualFrame = objOptionsVC.view.frame;
        CGRect modifiedFrame = actualFrame;
        modifiedFrame.origin.y =actualFrame.origin.y ;
        modifiedFrame.size.height = 0;
        objOptionsVC.view.alpha = 0.0;
        objOptionsVC.view.frame = modifiedFrame;
        
        [UIView animateWithDuration:0.5 animations:^{
            objOptionsVC.view.frame = actualFrame;
            objOptionsVC.view.alpha = 0.7;
        } completion:^(BOOL finished) {
            objOptionsVC.view.alpha = 1.0;
            
        }];

        
    }
}

//-----------------------------------------------------------------------

- (IBAction)btnAddWorkOutClicked:(id)sender {
    
     [self hideOptionView];
    ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
    UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
    ProgramGetWorkoutVCOBJ.delegate = (id)self;
    [getWorkoutNavigationController.navigationBar setTranslucent:NO];
    getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    
    CGPoint frameSize = CGPointMake(500, 116);
    if (iOS_8) {
        getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);

    }
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
    }
    
    [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
    
}

//-----------------------------------------------------------------------

- (IBAction)btnAddNewLineClicked:(id)sender {

    
    MESSAGE(@"btnAddNewLineClicked-> %ld",(long)btnAddNewLine.tag);
    
    
    // add new line
    [self hideOptionView];
    
    if (btnAddNewLine.tag == 1) {

        //TODO: PASTE Exercise
//        [commonUtility alertMessage:@"Under Development!"];
//        return;
//        
        [self doPasteLines];
    }
    else{

        ProgramGetDetailVC *ProgramGetDataVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"progDetail"];
        UINavigationController* programGetDetailNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetDataVCOBJ];
        ProgramGetDataVCOBJ.delegate = (id)self;
        [programGetDetailNavigationController.navigationBar setTranslucent:NO];
        programGetDetailNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        programGetDetailNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        
        CGPoint frameSize = CGPointMake(500, 500);
        if (iOS_8) {
            programGetDetailNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        else{
        }
        
        [self.view.window.rootViewController presentViewController:programGetDetailNavigationController animated:YES completion:^{
        }];
        
    }
    
}

//-----------------------------------------------------------------------

- (IBAction)btnSelectFromAllClicked:(id)sender {
     [self hideOptionView];
    
    if (objSelectAllTemplatesVC == nil)
    {
        objSelectAllTemplatesVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SelectAllTemplatesView"];
        popoverController = [[UIPopoverController alloc] initWithContentViewController:self.objSelectAllTemplatesVC];
        popoverController.delegate = (id)self;
    }
  
    
     self.objSelectAllTemplatesVC.arrayProgramSheet = arrSheetsList;
     [self.objSelectAllTemplatesVC setDelegate:(id)self];
    
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
    else{
        popoverController.popoverContentSize = CGSizeMake(350,200);
        [popoverController presentPopoverFromRect:btnSelectFromAll.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionUp animated:YES];
    }
    
}

//-----------------------------------------------------------------------

- (IBAction)upateTemplateName:(id)sender {
      UpdateTemplateVC *UpdateTemplateObj = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"UpdateTemplate"];
    UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:UpdateTemplateObj];
    UpdateTemplateObj.oldTemplateName = lblTemplateName.text;
    UpdateTemplateObj.delegate =(id) self;
    
    [getWorkoutNavigationController.navigationBar setTranslucent:NO];
    getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
    if (iOS_8) {
        CGPoint frameSize = CGPointMake(500, 116);
        getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
    
}


//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (void) clearClipBoardBtnClciked{
    START_METHOD
    [self hideOptionView];
    
    NSUserDefaults *defaluts = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
    [defaluts setValue:tempDict forKey:kProgramData];
    
    NSDictionary *tempDict1 = [[NSDictionary alloc] initWithObjectsAndKeys:nil,@"clientID",nil,@"sheetID",nil,@"blockID", nil];
    [defaluts setValue:tempDict1 forKey:kProgramBlockData];
    
    [defaluts setObject:nil forKey:kIsCopyLine];
    
    [defaluts setValue:nil forKey:kTemplateSheetData];
    [defaluts synchronize];
    
    [defaluts setObject:nil forKey:kBlockCurrentCopiedSheetID];
    [defaluts setObject:nil forKey:kCopiedBlockIDSheetID];
    [defaluts setObject:nil forKey:kCopyBlock];
    [defaluts setObject:nil forKey:kCopyProgramBlock];
    
    tempDict = nil;
    
    btnAddNewLine.tag = 0;
    [btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:UIControlStateNormal];
    isWokoutCopied = false;
    [tblProgramView reloadData];
    
    
}

//-----------------------------------------------------------------------

-(void)copyBtnClicked{
    START_METHOD
    
//TODO: SUNIL -> COPY
    
    [self hideOptionView];
    
    NSUserDefaults*defauts = [NSUserDefaults standardUserDefaults];
    NSString * strTemplateSheetId = [defauts valueForKey:kTemplateSheetData];
    NSString *strProgramClientID = [[defauts valueForKey:kProgramData] valueForKey:kClientId];
    NSString *strProgramSheetID = [[defauts valueForKey:kProgramData] valueForKey:ksheetID];

    
    if((strTemplateSheetId == nil || [strTemplateSheetId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID]]) && strProgramClientID == nil){
        MESSAGE(@"if block------->");
        
        [defauts setValue:[NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID] forKey:kTemplateSheetData];
        [defauts synchronize];
        
        NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
        [defauts setValue:tempDict forKey:kProgramData];
        [defauts synchronize];
        
        
    }else{
                MESSAGE(@"else block--1111----->");
        if(strTemplateSheetId == nil){
            
            MESSAGE(@"else block-f(strTemplateSheetId == nil-->");

            
            NSArray *tempArraBlocks = [[DataManager initDB] getBlockDataForClient:strProgramClientID SheetId:strProgramSheetID];
            
            for(int i=0;i<[tempArraBlocks count];i++){
                
                NSDictionary *dictBlock = [tempArraBlocks objectAtIndex:i];
                
                
                NSDictionary * dicBlockData = [NSDictionary dictionaryWithObjectsAndKeys:[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"],@"sheetID",[NSNumber numberWithInt:1],@"blockNo",[dictBlock valueForKey:kBlockTitle],@"sBlockTitle", [dictBlock valueForKey:kBlockDate],@"dBlockDate", nil];
              
                //Insert
                [[DataManager initDB] insertTemplateSheet_Blocks:dicBlockData];
                
                dicBlockData = nil;
                
                NSArray * temparrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
                
                NSInteger tempCurrentBlockID = [[[temparrayBlocks objectAtIndex:temparrayBlocks.count-1] valueForKey:@"nBlockID"] intValue];
                
                NSArray *arrayTempPrograms = [[DataManager initDB] selectProgramDataForClient:strProgramClientID sheet:strProgramSheetID blockNo:[NSString stringWithFormat:@"%ld",(long)[[dictBlock valueForKey:kBlockNo]integerValue]]];
                
                
                for (NSDictionary *dictCopiedLine in arrayTempPrograms) {
                    
                    
                    NSDictionary * dicToAdd = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [NSString stringWithFormat:@"%ld",(long)tempCurrentBlockID],kBlockId,
                                               [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                                               [dictCopiedLine valueForKey:kExercise],kExercise,
                                               [dictCopiedLine valueForKey:kLBValue],kLBValue,
                                                [dictCopiedLine valueForKey:kColor], kColor,
                                               [dictCopiedLine valueForKey:kRepValue],kRepValue,
                                               [dictCopiedLine valueForKey:kEXDate],kEXDate,
                                               [dictCopiedLine valueForKey:@"sSetValue"],@"sSetValue",
                                               [dictCopiedLine valueForKey:kColor],kColor,
                                               [dictBlock valueForKey:@"kBlockNo"],kBlockNo,
                                               [NSString stringWithFormat:@"%i",0],kRowNo,nil];
                    MESSAGE(@"dictCopiedLine block---dicToAdd---->%@",dictCopiedLine);

                    [[DataManager initDB] insertTemplate_ProgramedData:dicToAdd];
                    
                }
                
            }
            
            NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
            [defauts setValue:tempDict forKey:kProgramData];
            [defauts synchronize];
            
            //Get Blocks
            arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
            currentBlockIndex = arrayBlocks.count-1;
            self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
            
            [defauts setValue:nil forKey:kTemplateSheetData];
            [defauts synchronize];
            [self updateSheetData:YES];
            
            
        }else{
            
            //Get Upgarde Status
            NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
            
            //TODO: SUNIL ->
            if ([strUpgradeStatus isEqualToString:@"YES"]) {
                
                
                //Invoke method for paste entire Template program
                [self careteDictForRequestForPasteEntireTemplateProgram:[strTemplateSheetId intValue]];
                
            }else{
                
                //Paste in local for Existing non upgradde user
                [self pasteTemplateEntireProgram:[strTemplateSheetId intValue]];
            }
        }
    }
    END_METHOD
}

-(void)pasteTemplateEntireProgram:(int )intTemplateSheetId{
    MESSAGE(@"else block-f(strTemplateSheetId == nil--> else again --->");
    
    NSUserDefaults*defauts = [NSUserDefaults standardUserDefaults];

    NSArray * arrSheetBlocks = [[DataManager initDB]getBlockDataFor_TemplateSheet:intTemplateSheetId];
    
    for (NSDictionary *dic in arrSheetBlocks) {
        
        NSDictionary * dicBlockData = [NSDictionary dictionaryWithObjectsAndKeys:[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"],@"sheetID",[NSNumber numberWithInt:1],@"blockNo",[dic valueForKey:kBlockTitle],@"sBlockTitle", [dic valueForKey:kBlockDate],@"dBlockDate", nil];
        
        [[DataManager initDB] insertTemplateSheet_Blocks:dicBlockData];
        dicBlockData = nil;
        
        NSArray * temparrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
        
        NSInteger tempCurrentBlockID = [[[temparrayBlocks objectAtIndex:temparrayBlocks.count-1] valueForKey:@"nBlockID"] intValue];
        NSArray *arrayTempPrograms = [[DataManager initDB]selectProgramDataForBlockNo:[[dic valueForKey:@"nBlockID"]integerValue]];
        
        for (NSDictionary *dictCopiedLine in arrayTempPrograms) {
            
            NSDictionary * dicToAdd = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSString stringWithFormat:@"%ld",(long)tempCurrentBlockID],kBlockId,
                                       [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                                       [dictCopiedLine valueForKey:kExercise],kExercise,
                                       [dictCopiedLine valueForKey:kLBValue],kLBValue,
                                       [dictCopiedLine valueForKey:kColor], kColor,
                                       [dictCopiedLine valueForKey:kRepValue],kRepValue,
                                       [dictCopiedLine valueForKey:kEXDate],kEXDate,
                                       [dictCopiedLine valueForKey:@"sSetValue"],@"sSetValue",[dictCopiedLine valueForKey:kColor],kColor,
                                       [dic valueForKey:@"kBlockNo"],kBlockNo,
                                       [NSString stringWithFormat:@"%i",0],kRowNo,nil];
            MESSAGE(@"dictCopiedLine block--else agian -dicToAdd---->%@",dictCopiedLine);
            
            [[DataManager initDB] insertTemplate_ProgramedData:dicToAdd];
        }
        
    }
    
    //Get Blocks
    arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
    currentBlockIndex = arrayBlocks.count-1;
    self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    
    NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
    [defauts setValue:tempDict forKey:kProgramData];
    [defauts synchronize];
    
    [defauts setValue:nil forKey:kTemplateSheetData];
    [defauts synchronize];
    [self updateSheetData:YES];
}

//-----------------------------------------------------------------------

-(void)updateSheetData:(BOOL)update{
    START_METHOD
    [self fillArrayWithData];
    [self setSheetValues];
    [tblProgramView reloadData];
    END_METHOD
}

//-----------------------------------------------------------------------

-(void)setSheetValues{
    
    MESSAGE(@"arrSheetsList------> %@ and : currentSheetIndex: %ld \n\n and arrayBlocksL:%@ and currentBlockIndex: %ld",arrSheetsList,(long)currentSheetIndex,arrayBlocks,(long)currentBlockIndex);
    
    
    lblSheetName.text =[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetName"];
    [self setWorkOutName:[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"sBlockTitle"]];
    [self addUnderlineToSheetName];
    
    
}

//-----------------------------------------------------------------------

- (NSString*)stringVisibleInRect:(CGRect)rect1 withFont:(UIFont*)font string:(NSString *)str
{
    NSString *visibleString = @"";
    for (int i = 1; i <= str.length; i++)
    {
        NSString *testString = [str substringToIndex:i];
        CGRect rect2 = [testString boundingRectWithSize:rect1.size options:NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil];
        if (rect2.size.width > rect1.size.width)
            break;
        visibleString = testString;
    }
    return visibleString;
}

//-----------------------------------------------------------------------

-(void)setWorkOutName :(NSString *)ProgramName
{
    [lblWorkOutName setAttributedText:nil];
    [lblWorkOutName setText:nil];
    NSString *str = ProgramName;
    str = str.uppercaseString;
    str = [NSString stringWithFormat:@"\"%@\"",str];
    
    NSString *str2 = [self stringVisibleInRect:lblWorkOutName.frame withFont:[UIFont fontWithName:kproximanova_semibold size:26] string:str];
    
    if(![str isEqualToString:str2]){
        
        NSRange range = NSMakeRange(0, str2.length-3);
        str2 =  [str2 substringWithRange:range];
        str2 = [NSString stringWithFormat:@"%@...\"",str2];
    }
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:str2];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0] range:NSMakeRange(0,1)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0] range:NSMakeRange(str2.length-1,1)];
    
    [lblWorkOutName setAttributedText:string];
    string = nil;
    
}

-(void)addUnderlineToSheetName {
    
    CGRect expectedLabelSize = [lblSheetName.text boundingRectWithSize:lblSheetName.frame.size options:NSStringDrawingUsesFontLeading attributes:@{ NSFontAttributeName:[UIFont fontWithName:kproximanova_semibold size:20]} context:nil];
    
    CGFloat xOrigin=0;
    
    switch (lblSheetName.textAlignment) {
        case NSTextAlignmentCenter:
            xOrigin=(lblSheetName.frame.size.width - expectedLabelSize.size.width)/2;
            break;
        case NSTextAlignmentLeft:
            xOrigin=0;
            break;
        case NSTextAlignmentRight:
            if (expectedLabelSize.size.width>lblSheetName.frame.size.width)
            {
                xOrigin=385 ;
            }
            else
            {
                xOrigin=lblSheetName.frame.size.width - expectedLabelSize.size.width;
            }
            break;
        default:
            break;
    }
    
    for (UIView *view in lblSheetName.subviews){
        if([view isKindOfClass:[UIView class]])
            [view removeFromSuperview];
    }
    
    UIView *viewUnderline=[[UIView alloc] init];
    viewUnderline.backgroundColor = nil;
    if (expectedLabelSize.size.width>lblSheetName.frame.size.width)
    {
        viewUnderline.frame= CGRectMake(0,
                                        expectedLabelSize.size.height-4,
                                        336,
                                        2);
    }
    else
    {
        viewUnderline.frame=CGRectMake(xOrigin-2,
                                       expectedLabelSize.size.height-4,
                                       expectedLabelSize.size.width+3,
                                       2);
    }
    
    viewUnderline.backgroundColor= [UIColor colorWithRed:61.0/255.0 green:180.0/255.0 blue:170.0/255.0 alpha:1.0];
    [lblSheetName addSubview:viewUnderline];
    viewUnderline = nil;
    
}

//-----------------------------------------------------------------------


- (void) fillArrayWithData {
    START_METHOD
    
    MESSAGE(@"self.intCurrentBlockID----> %ld",self.intCurrentBlockID);
    [arrayProgramData setArray:[[DataManager initDB]selectProgramDataForBlockNo:self.intCurrentBlockID]];
    arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
    
    [btnDate setTitle:[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"dBlockdate"] forState:UIControlStateNormal];
    
    NSString *strDate = [[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"dBlockdate"];
    
    if(strDate && strDate.length>6){
        [btnSheetDate setTitle:[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"dBlockdate"] forState:UIControlStateNormal];
    }
    [arrayCopyLines removeAllObjects];

    if (arrayProgramData.count>0) {
        lblInstruction.hidden = YES;
    }
    else{
        lblInstruction.hidden = NO;
        lblInstruction.autoresizingMask = UIViewAutoresizingNone;
        [self.view bringSubviewToFront:lblInstruction];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];
    
    
    if([strCurrentCopiedBlockId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID]]){
        isWokoutCopied = true;
    }
    END_METHOD
}

//-----------------------------------------------------------------------

-(void)OpenSheetNameView{
    
    AddTemplateSheet *addTemplateSheetObj = (AddTemplateSheet *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddTemplateSheetView"];
    addTemplateSheetObj.intsheetcount = self.intTemplateSheets;
    addTemplateSheetObj.delegateSheetName = (id)self;
    addTemplateSheetObj.intTemplateid = self.intCurrentTemplateID;
    addTemplateSheetObj.strTemplateName = self.strLableText;
    addTemplateSheetObj.intCurrentSheetId = self.intCurrentSheetID;
    
    UINavigationController *modalController = [[UINavigationController alloc]initWithRootViewController:addTemplateSheetObj];
    modalController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    modalController.modalPresentationStyle =  UIModalPresentationFormSheet;
    
    if (iOS_8) {
        CGPoint frameSize;
        if(self.intTemplateSheets == 1){
            frameSize = CGPointMake(500, 160+44);
        }
        else if(self.intTemplateSheets == 2){
            frameSize = CGPointMake(500, 210+40);
        }
        else{
            frameSize = CGPointMake(500, 270+36);
        }
        modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
    }
    
    [self presentViewController:modalController animated:YES completion:^{
        
    }];
    
}
//-----------------------------------------------------------------------

-(void)setBlockData{
    START_METHOD
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strBlockName = [[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:kBlockTitle];
    NSMutableDictionary *dictBlock = [[NSMutableDictionary alloc]init];
    [dictBlock setObject:strBlockName forKey:@"sBlockTitle"];
    [dictBlock setObject:[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:kBlockDate] forKey:kBlockDate];
    [dictBlock setObject:[NSNumber numberWithInt:1] forKey:@"blockNo"];
    [dictBlock setObject:[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"] forKey:@"sheetID"];
    [dictBlock setObject:arrayProgramData forKey:kCopiedLinesForBlock];
    [defaults setObject:[NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID] forKey:kBlockCurrentCopiedSheetID];
    [defaults setObject:[NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID] forKey:kCopiedBlockIDSheetID];
    [defaults setObject:dictBlock forKey:kCopyBlock];
    [defaults setObject:nil forKey:kCopyProgramBlock];
    isWokoutCopied = true;
    [tblProgramView reloadData];
    [defaults synchronize];
    END_METHOD
}

//-----------------------------------------------------------------------

-(void)doCopyWorkOut{

    START_METHOD
    
    
//TODO:  SUNIL = COPY WORK OUT
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strCurrentCopiedBlockId = [defaults valueForKey:kCopiedBlockIDSheetID];
    
    if ([strCurrentCopiedBlockId isEqualToString:[NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID]]) {
        [self setBlockData];
    }
    else if ([defaults valueForKey:kCopyProgramBlock]) {
    
        MESSAGE(@"[defaults valueForKey:kCopyProgramBlock]");
        NSDictionary *dictCopiedBlock = [defaults valueForKey:kCopyProgramBlock];
        NSInteger blockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
        
        [[DataManager initDB] updateBlockDate_Title:self.intCurrentSheetID :blockID :[dictCopiedBlock valueForKey:kBlockDate] :[dictCopiedBlock valueForKey:@"sBlockTitle"]];
        
        NSArray *arrayCopiedLines = (NSArray*)[dictCopiedBlock valueForKey:kCopiedLinesForBlock];
        
        for (NSDictionary *dictCopiedLine in arrayCopiedLines) {
            
            NSDictionary * dicToAdd = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSString stringWithFormat:@"%ld",(long)blockID],kBlockId,
                                       [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                                       [dictCopiedLine valueForKey:kExercise],kExercise,
                                       [dictCopiedLine valueForKey:kLBValue],kLBValue,
                                       [dictCopiedLine valueForKey:kRepValue],kRepValue,
                                       [dictCopiedLine valueForKey:kEXDate],kEXDate,
                                       [dictCopiedLine valueForKey:kColor],kColor,
                                       [dictCopiedLine valueForKey:@"sSetValue"],@"sSetValue",
                                       [dictCopiedBlock valueForKey:@"kBlockNo"],kBlockNo,
                                       [NSString stringWithFormat:@"%i",0],kRowNo,nil];
            [[DataManager initDB] insertTemplate_ProgramedData:dicToAdd];
            
            
        }
        
        [defaults setObject:nil forKey:kCopyBlock];
        [defaults setObject:nil forKey:kCopyProgramBlock];
        [defaults setObject:nil forKey:kCopiedBlockIDSheetID];
        [defaults setObject:nil forKey:kBlockCurrentCopiedSheetID];
        [self fillArrayWithData];
        [self setSheetValues];
        [tblProgramView reloadData];
    }
    else if ([defaults valueForKey:kCopyBlock]) {
        
        
        //Get Upgarde Status
        NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        //TODO: SUNIL -> EDIT EXERCISE
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            //Invoke method for create the dict for paste the workoutds on the server
            [self careteDictForRequestForPasteTemplateWorkouts];
            
        }
        else{
        
            
        //Invoke methodf or paste template workouts
        [self pasteTemplateWorkouts];
    }
    
    }
    else{
        
        MESSAGE(@"else docopyworkout");
        [self setBlockData];
    }
    
    [defaults synchronize];
    
}

-(void)pasteTemplateWorkouts{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    
    MESSAGE(@"[defaults valueForKey:kCopyBlock])");
    NSDictionary *dictCopiedBlock = [defaults valueForKey:kCopyBlock];
    
    int blockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    
    [[DataManager initDB] updateBlockDate_Title:self.intCurrentSheetID :blockID :[dictCopiedBlock valueForKey:kBlockDate] :[dictCopiedBlock valueForKey:@"sBlockTitle"]];
    
    self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    NSArray *arrayCopiedLines = (NSArray*)[dictCopiedBlock valueForKey:kCopiedLinesForBlock];
    
    //Insert all Copied Blocks..!!
    for (NSDictionary *dictCopiedLine in arrayCopiedLines) {
        
        NSDictionary * dicToAdd = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%d",blockID],kBlockId,
                                   [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                                   [dictCopiedLine valueForKey:kExercise],kExercise,
                                   [dictCopiedLine valueForKey:kLBValue],kLBValue,
                                   [dictCopiedLine valueForKey:kRepValue],kRepValue,
                                   [dictCopiedLine valueForKey:kEXDate],kEXDate,
                                   [dictCopiedLine valueForKey:kColor],kColor,
                                   [dictCopiedLine valueForKey:@"sSetValue"],@"sSetValue",
                                   [dictCopiedBlock valueForKey:@"kBlockNo"],kBlockNo,
                                   [NSString stringWithFormat:@"%i",0],kRowNo,nil];
        
        [[DataManager initDB] insertTemplate_ProgramedData:dicToAdd];
    }
    
    [defaults setObject:nil forKey:kCopyBlock];
    [defaults setObject:nil forKey:kCopyProgramBlock];
    [defaults setObject:nil forKey:kCopiedBlockIDSheetID];
    
    [self fillArrayWithData];
    [self setSheetValues];
    [tblProgramView reloadData];
}
//-----------------------------------------------------------------------

-(void)doPasteLines{
    START_METHOD
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrayCopiedLines = (NSArray*)[defaults valueForKey:kIsCopyLine];
    if (arrayCopiedLines.count) {
        [arrayCopiedLines enumerateObjectsUsingBlock:^(NSDictionary *dictLine, NSUInteger idx, BOOL *stop) {
            
            NSInteger  blockNum = 0;
            if (arrayBlocks.count) {
                blockNum = [[[arrayBlocks objectAtIndex:0] valueForKey:@"nBlockNo"] integerValue];
            }
            NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%ld",(long)self.intCurrentBlockID],kBlockId,
                                  [NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID],kSheetId,
                                  [dictLine valueForKey:kExercise],kExercise,
                                  [dictLine valueForKey:kColor],kColor,
                                  [dictLine valueForKey:kLBValue],kLBValue,
                                  [dictLine valueForKey:kRepValue],kRepValue,
                                  [dictLine valueForKey:kEXDate],kEXDate,
                                  [dictLine valueForKey:@"sSetValue"],@"sSetValue",
                                  [NSString stringWithFormat:@"%ld",(long)blockNum],kBlockNo,
                                  [NSString stringWithFormat:@"%i",0],kRowNo,
                                  nil];
            
            [[DataManager initDB] insertTemplate_ProgramedData:dic];
        }];
        [defaults setObject:nil forKey:kIsCopyLine];
        [arrayCopyLines removeAllObjects];
        btnAddNewLine.tag = 0;
        [btnAddNewLine setTitle:NSLocalizedString(@"Add New Exercise", nil) forState:UIControlStateNormal];
        
        [self fillArrayWithData];
        [self setSheetValues];
        [tblProgramView reloadData];
    }
    else{
        DisplayAlertWithTitle(NSLocalizedString(@"Please select Lines to paste.", nil), kAppName);
    }
    END_METHOD
}

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    [btnHome setTitle:NSLocalizedString(btnHome.titleLabel.text, nil) forState:0];
    [btnOption setTitle:NSLocalizedString(btnOption.titleLabel.text, nil) forState:0];
    lblDate.text = NSLocalizedString(lblDate.text, nil);
    [btnSelectFromAll setTitle:NSLocalizedString(btnSelectFromAll.titleLabel.text, nil) forState:0];
    lblPrevWorkout.text = NSLocalizedString(lblPrevWorkout.text, nil);
    [btnPrevWorkout setTitle:NSLocalizedString(btnPrevWorkout.titleLabel.text, nil) forState:0];
    
    lblNextWorkout.text = NSLocalizedString(lblNextWorkout.text, nil);
    [btnNext setTitle:NSLocalizedString(btnNext.titleLabel.text, nil) forState:0];
    lblCopy.text = NSLocalizedString(lblCopy.text, nil);
    lblExercise.text = NSLocalizedString(lblExercise.text, nil);
    lblWeight.text = NSLocalizedString(lblWeight.text, nil);
    lblSet.text = NSLocalizedString(lblSet.text, nil);
    lblRep.text = NSLocalizedString(lblRep.text, nil);
    lblRestTime.text = NSLocalizedString(lblRestTime.text, nil);
    lblDelete.text = NSLocalizedString(lblDelete.text, nil);
    lblInstruction.text = NSLocalizedString(lblInstruction.text, nil);

    [btnAddNewLine setTitle:NSLocalizedString(btnAddNewLine.titleLabel.text, nil) forState:0];
    [btnAddWorkout setTitle:NSLocalizedString(btnAddWorkout.titleLabel.text, nil) forState:0];


}



//-----------------------------------------------------------------------

#pragma mark - Touch Methods

//-----------------------------------------------------------------------

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideOptionView];
}

//-----------------------------------------------------------------------

#pragma mark - Options Delegate Method

//-----------------------------------------------------------------------

-(void)selectedOptionsTask:(NSInteger)selTask forEvent:(NSString*)evnt {
    
    MESSAGE(@"selectedOptionsTask-> %ld",(long)selTask);
    
    [self hideOptionView];
    
    if (selTask == 12) {  //TODO: Copy Sheet..!!
        
        
//         [commonUtility alertMessage:@"Under Development!"];
//        return;
        
        //Invoke for copy paste Entire Template Program
        [self copyBtnClicked];
    }
    
    else if (selTask == 41) {//TODO: copy WorkOut
        
        
//         [commonUtility alertMessage:@"Under Development!"];
//                return;
        
        //Invoke for copy paste workout
        [self doCopyWorkOut];
        
    }
    else if (selTask == 42) {//TODO: past NewLines
         [commonUtility alertMessage:@"Under Development!"];
                return;
        [self doPasteLines];
    }
    else if (selTask == 13){
        [self clearClipBoardBtnClciked];
    }
    else if (selTask == 43){// Edit Workout..
        START_METHOD
        ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
        UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
        ProgramGetWorkoutVCOBJ.strSheetName =[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"sBlockTitle"];
        ProgramGetWorkoutVCOBJ.delegate =(id) self;
        [getWorkoutNavigationController.navigationBar setTranslucent:NO];
        getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        if (iOS_8) {
            CGPoint frameSize = CGPointMake(500, 116);
            getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
    }
    
    else if (selTask == 15){//Edit Sheet..

        ProgramGetWorkoutVC *ProgramGetWorkoutVCOBJ = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"getworkout"];
        UINavigationController*  getWorkoutNavigationController = [[UINavigationController alloc] initWithRootViewController:ProgramGetWorkoutVCOBJ];
        ProgramGetWorkoutVCOBJ.strSheetName = [[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetName"];
        ProgramGetWorkoutVCOBJ.delegate =(id) self;
        ProgramGetWorkoutVCOBJ.isEditTemplateSheet = true;
        [getWorkoutNavigationController.navigationBar setTranslucent:NO];
        getWorkoutNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        getWorkoutNavigationController.modalTransitionStyle = kModalTransitionStyleCoverVertical;
        if (iOS_8) {
            CGPoint frameSize = CGPointMake(500, 116);
            getWorkoutNavigationController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
        }
        [self.view.window.rootViewController presentViewController:getWorkoutNavigationController animated:YES completion:nil];
    }
}


//-----------------------------------------------------------------------

#pragma mark - select All Delegate Methods

//-----------------------------------------------------------------------

-(void)selectItemAtBlockIndex:(NSInteger)blockIndex AtSheetIndex:(NSInteger)sheetIndex{
    
    isWokoutCopied = false;
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
    currentSheetIndex = sheetIndex;
    currentBlockIndex = blockIndex;
    
    self.intCurrentSheetID = [[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"] intValue];
    arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
    self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    
    [self fillArrayWithData];
    
    [self setSheetValues];
    [tblProgramView reloadData];
}

//----------------------------------------------------

#pragma mark - SheetName Delegate Method.

//-----------------------------------------------------------------------

-(void)callSetArrayMethod:(NSInteger)sheetid :(NSInteger)intTemplateID :(NSInteger)intSheetCount :(id)sender{

        self.intStoreFirstSheetId = sheetid;
        self.intCurrentSheetID = sheetid;
        self.intCurrentTemplateID = intTemplateID;
        self.intTemplateSheets = intSheetCount;
    
        [arrSheetsList setArray:[[DataManager initDB]getTemplateSheetList:self.intCurrentTemplateID]];
        arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
        self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:0] valueForKey:@"nBlockID"] integerValue];
    
        [self fillArrayWithData];
        currentSheetIndex = 0;
        currentBlockIndex = 0;
        [self setSheetValues];
    
    
}

//-----------------------------------------------------------------------

-(void)didCancelSheetEntry{
    [self.navigationController popViewControllerAnimated:true];
}

//-----------------------------------------------------------------------

#pragma mark - Orientation Methods

//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    if([self.navigationController.visibleViewController isKindOfClass:[AutoSuggestViewController class]]){
        AutoSuggestViewController *objProgramGetVC = (AutoSuggestViewController*)self.navigationController.visibleViewController;
        if ([objProgramGetVC respondsToSelector:@selector(didRotateFromInterfaceOrientation:)]) {
            [objProgramGetVC didRotateFromInterfaceOrientation:fromInterfaceOrientation];
        }
        
    }
}
//-----------------------------------------------------------------------

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
  
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        // landscape orientation
       
        [objOptionsVC.view removeFromSuperview];
        objOptionsVC = nil;
        [self setUpLandscapeOrientation];
        
    } else {
       //potrait orientation
        [objOptionsVC.view removeFromSuperview];
        objOptionsVC = nil;
        [self setupPortraitOrientation];
        
    }
}


//-----------------------------------------------------------------------

-(void)setUpLandscapeOrientation {
    
    self.isLandScapeMode = TRUE;
    viewMain.frame = CGRectMake(0, 0, 1024,768 );
    imgvBackground.frame = CGRectMake(0, 20, 1024,768);
    imgvNavBar.frame = CGRectMake(0, 20, 1024,57 );
    [imgvNavBar setImage:[UIImage imageNamed:@"Top_Bar_Landscape"]];
    [imgvBackground setImage:[UIImage imageNamed:@"BackgroundImage_iPad"]];
    
    
    lblTraining.text = NSLocalizedString(lblTraining.text, nil);
    lblNotebook.text = NSLocalizedString(lblNotebook.text, nil);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblTraining.text font:lblTraining.font height:lblTraining.frame.size.height];
    lblTraining.frame = CGRectMake(385, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:lblNotebook.text font:lblNotebook.font height:lblNotebook.frame.size.height];
    lblNotebook.frame = CGRectMake(lblTraining.frame.origin.x + lblTraining.frame.size.width , 23,rect.size.width + 5 ,48);
    

    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnOption.titleLabel.text, nil) font:btnOption.titleLabel.font height:btnOption.titleLabel.frame.size.height];
    [btnOption setFrame:CGRectMake(self.view.frame.size.width - (rect.size.width + 30), 20, rect.size.width + 30, 55)];
    
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnHome.titleLabel.text, nil) font:btnHome.titleLabel.font height:btnHome.titleLabel.frame.size.height];
    
    [btnHome setFrame:CGRectMake(0, 20, rect.size.width + 30, 55)];
    
    viewTbl.frame = CGRectMake(19, 85, 986,609);
    viewHeader.frame = CGRectMake(20, 246, 984,31 );
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnAddWorkout.titleLabel.text, nil) font:btnAddWorkout.titleLabel.font height:btnAddWorkout.titleLabel.frame.size.height];
    btnAddWorkout.frame = CGRectMake(816, 709, rect.size.width + 30 ,35);
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnSelectFromAll.titleLabel.text, nil) font:btnSelectFromAll.titleLabel.font height:btnSelectFromAll.titleLabel.frame.size.height];
    
    btnSelectFromAll.frame =CGRectMake(430, 179, rect.size.width + 50,30);
    lblDate.frame = CGRectMake(880, 110 + 10, 40,21 );
    btnSheetDate.frame = CGRectMake(918, 111 + 10, 85,20 );
    lblSheetName.frame = CGRectMake(640, 6, 336,18 );
    btnSheetName.frame = CGRectMake(640, 6, 336,18 );
    viewPrevNext.frame = CGRectMake(0, 120, 994,40 );
    lblTemplateName.frame = CGRectMake(0, 54, 976,28 );
    
    lblWorkout.frame = CGRectMake(300, 13, 390,26 );
    btnNext.frame = CGRectMake(886, 10, 108,24 );
    lblNextWorkout.frame = CGRectMake(895, 20, 50,21 );
    btnWorkout.frame = CGRectMake(300, 13, 390,26 );
    tblProgramView.frame =CGRectMake(0, 0, 982,407 );
    viewTable.frame =CGRectMake(0, 193, 986,407 );
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnAddNewLine.titleLabel.text, nil) font:btnAddNewLine.titleLabel.font height:btnAddNewLine.titleLabel.frame.size.height];
    
    btnAddNewLine.frame = CGRectMake(42, 709, rect.size.width + 30, 35);
    lblInstruction.frame = CGRectMake(150,450 ,768 , 21);
    
    lblCopy.frame = CGRectMake(8,8 ,45 , 21);
    imgvFirstLine.frame = CGRectMake(45,0 ,1 , 32);
    lblExercise.frame = CGRectMake(50,8 ,500 , 21);
    imgvSecondLine.frame = CGRectMake(507,0 ,1 , 32);
    lblWeight.frame = CGRectMake(507,8 ,95 , 21);
    imgvThirdLine.frame = CGRectMake(604,0 ,1 , 32);
    lblSet.frame = CGRectMake(604,8 ,94 , 21);
    imgvFourthLine.frame = CGRectMake(698,0 ,1 , 32);
    lblRep.frame = CGRectMake(698,8 ,95 , 21);
    imgvFifthLine.frame = CGRectMake(793,0 ,1 , 32);
    lblRestTime.frame = CGRectMake(796,8 ,82 , 21);
    imgvSixLine.frame = CGRectMake(878,0 ,1 , 32);
    lblDelete.frame = CGRectMake(888,8 ,45 , 21);
    [tblProgramView reloadData];
    
    if ([popoverController isPopoverVisible]) {
       [self btnSelectFromAllClicked:nil];
    }
    
    if([popoverControllerDate isPopoverVisible]) {
        [self btnDateClicked:nil];
    }
   
    
}

//-----------------------------------------------------------------------

-(void)setupPortraitOrientation {
    
    self.isLandScapeMode = FALSE;
    viewMain.frame = CGRectMake(0, 0, 768,1024 );
    imgvBackground.frame = CGRectMake(0, 20, 768,1024 );
    imgvNavBar.frame = CGRectMake(0, 20, 768,57 );
    [imgvBackground setImage:[UIImage imageNamed:@"BackgroundImage"]];
    [imgvNavBar setImage:[UIImage imageNamed:@"Top-Bar"]];
    
    lblTraining.text = NSLocalizedString(lblTraining.text, nil);
    lblNotebook.text = NSLocalizedString(lblNotebook.text, nil);
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblTraining.text font:lblTraining.font height:lblTraining.frame.size.height];
    lblTraining.frame = CGRectMake(255 + 45, 23,rect.size.width + 5 ,48 );
    
    rect = [[AppDelegate sharedInstance] getWidth:lblNotebook.text font:lblNotebook.font height:lblNotebook.frame.size.height];
    lblNotebook.frame = CGRectMake(lblTraining.frame.origin.x + lblTraining.frame.size.width , 23,rect.size.width + 5 ,48);
    

    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnOption.titleLabel.text, nil) font:btnOption.titleLabel.font height:btnOption.titleLabel.frame.size.height];
    [btnOption setFrame:CGRectMake(self.view.frame.size.width - (rect.size.width + 30), 20+1, rect.size.width + 30, 54)];
    
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnHome.titleLabel.text, nil) font:btnHome.titleLabel.font height:btnHome.titleLabel.frame.size.height];
    
    [btnHome setFrame:CGRectMake(0, 20+1, rect.size.width + 30, 55)];
    
    viewTbl.frame = CGRectMake(19, 85, 730,865 );
    viewHeader.frame = CGRectMake(20, 246, 728,31 );
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnAddWorkout.titleLabel.text, nil) font:btnAddWorkout.titleLabel.font height:btnAddWorkout.titleLabel.frame.size.height];
    btnAddWorkout.frame = CGRectMake(567, 965, rect.size.width + 30,35 );
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnSelectFromAll.titleLabel.text, nil) font:btnSelectFromAll.titleLabel.font height:btnSelectFromAll.titleLabel.frame.size.height];
    
    btnSelectFromAll.frame =CGRectMake(308, 179, rect.size.width + 50,30 );
    lblDate.frame = CGRectMake(624, 110 + 10, 40,21 );
    btnSheetDate.frame = CGRectMake(662, 111+ 10, 85,20 );
    lblTemplateName.frame = CGRectMake(0, 54, 730,28 );
    lblSheetName.frame = CGRectMake(385, 6, 336,18 );
    btnSheetName.frame= CGRectMake(385, 6, 336,18 );
    viewPrevNext.frame = CGRectMake(0, 120, 730,40 );
    lblWorkout.frame = CGRectMake(170, 13, 390,26 );
    btnNext.frame = CGRectMake(630, 10, 108,24 );
    lblNextWorkout.frame = CGRectMake(639, 20, 50,21 );
    btnWorkout.frame = CGRectMake(170, 13, 390,26 );
    tblProgramView.frame =CGRectMake(0, 0, 730,663 );
    viewTable.frame =CGRectMake(0, 193, 730,663 );
    
    rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(btnAddNewLine.titleLabel.text, nil) font:btnAddNewLine.titleLabel.font height:btnAddNewLine.titleLabel.frame.size.height];
    
    btnAddNewLine.frame = CGRectMake(42, 966, rect.size.width + 30, 35);
    lblInstruction.frame = CGRectMake(0,532 ,768 , 21);
    
    lblCopy.frame = CGRectMake(8,8 ,45 , 21);
    imgvFirstLine.frame = CGRectMake(45,0 ,1 , 32);
    lblExercise.frame = CGRectMake(50,8 ,250 , 21);
    imgvSecondLine.frame = CGRectMake(307,0 ,1 , 32);
    lblWeight.frame = CGRectMake(307,8 ,95 , 21);
    imgvThirdLine.frame = CGRectMake(402,0 ,1 , 32);
    lblSet.frame = CGRectMake(404,8 ,94 , 21);
    imgvFourthLine.frame = CGRectMake(498,0 ,1 , 32);
    lblRep.frame = CGRectMake(498,8 ,95 , 21);
    imgvFifthLine.frame = CGRectMake(592,0 ,1 , 32);
    lblRestTime.frame = CGRectMake(592,8 ,76 , 21);
    imgvSixLine.frame = CGRectMake(667,0 ,1 , 32);
    lblDelete.frame = CGRectMake(679,8 ,45 , 21);
    [tblProgramView reloadData];
    
    
    if ([popoverController isPopoverVisible]) {
        [self btnSelectFromAllClicked:nil];
    }
    
    if([popoverControllerDate isPopoverVisible]) {
        [self btnDateClicked:nil];
    }

}

//-----------------------------------------------------------------------

#pragma mark - View Life Cycle Methods

//-----------------------------------------------------------------------

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    arrayProgramData = [[NSMutableArray alloc] init];
    arrSheetsList = [[NSMutableArray alloc] init];
    arrayCopyLines = [[NSMutableArray alloc] init];
    lblTemplateName.text = self.strLableText;
    
    viewOptions.translatesAutoresizingMaskIntoConstraints = NO;
    viewOptions.frame = CGRectMake(601, 55, 167, 0);
    
    /*
    NSDate * date = datePicker.date;
    NSDateFormatter * dateformatter=[[NSDateFormatter alloc ] init];
    [dateformatter setDateFormat:kAppDateFormat];
    NSString *strDate = [dateformatter stringFromDate:date];
     */
    
    NSDate * date123 = [NSDate date];
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kAppDateFormat];
    
    NSString * currDate = [df stringFromDate:date123];
    
    
    [btnDate setTitle:currDate forState:UIControlStateNormal];
    
    [btnSheetDate setTitle:currDate forState:UIControlStateNormal];
    
    MESSAGE(@"btnDate : %@  and btnSheetDate : %@",currDate,currDate);
    
    
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated{
    
    START_METHOD
    [super viewWillAppear:animated];
    
    [self localizeControls];
    
    [[DataManager initDB] addNewFieldsToTablesIfnotAddedForClientId:@""];
    
    NSArray *alredyCopiedLines = (NSArray*)[[NSUserDefaults standardUserDefaults] valueForKey:kIsCopyLine];
    if (alredyCopiedLines.count) {
        [arrayCopyLines addObjectsFromArray:alredyCopiedLines];
    }
    
    if (arrayCopyLines.count) {
        btnAddNewLine.tag = 1;
        [btnAddNewLine setTitle:NSLocalizedString(@"Paste New Exercise", nil) forState:0];
        
    }
    
    if (self.isNewTemplate){
        
    }
    else{
        
        self.intCurrentTemplateID = [[self.dictTemplateInfo valueForKey:@"template_Id"] integerValue];
        
        [arrSheetsList setArray:[[DataManager initDB]getTemplateSheetList:self.intCurrentTemplateID]];
        
        if(arrSheetsList && arrSheetsList.count>0)  {
            
        self.intCurrentSheetID = [[[arrSheetsList objectAtIndex:0] valueForKey:@"sheetID"] integerValue];
     
        
        arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
      
        
      if(arrayBlocks && arrayBlocks.count>0){
        self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:0] valueForKey:@"nBlockID"] integerValue];
        
      
        [self fillArrayWithData];
        currentSheetIndex = 0;
        currentBlockIndex = 0;
        [self setSheetValues];
        [tblProgramView reloadData];
            
            }else{
                
                [commonUtility alertMessage:@"No Template info, Please Add new Template and delete this one"];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            
        
        }else{
            [commonUtility alertMessage:@"No Template info, Please Add new Template and delete this one"];
                [self.navigationController popViewControllerAnimated:YES];
        }
    
    }
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [self setUpLandscapeOrientation];
    } else {
        [self setupPortraitOrientation];
    }
    
    UIFontDescriptor * fontD = [lblTraining.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold
                                ];
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language isEqualToString:@"en"] || [language isEqualToString:@"zh-Hans"] || [language isEqualToString:@"nb"]) {
        [lblTraining setFont:[UIFont fontWithDescriptor:fontD size:24.0]];
        [lblNotebook setFont:[UIFont fontWithName:@"Proxima Nova Alt" size:24.0]];
    }
    
    lblTraining.text = NSLocalizedString(lblTraining.text, nil);
    lblNotebook.text = NSLocalizedString(lblNotebook.text, nil);
    
    //Invoke method for set View color  SUNIL
//    [self setViewOnBasisSkin];
    END_METHOD
}

//-----------------------------------------------------------------------


-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (self.isNewTemplate) {
        [self performSelector:@selector(OpenSheetNameView) withObject:Nil afterDelay:0.5];
    }
    
}
//sunil
-(void) setViewOnBasisSkin{
    START_METHOD
    
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    switch (skin) {
        case FIRST_TYPE:
            [updateTemplateName setBackgroundColor:kFirstSkin];
            break;
        
        case SECOND_TYPE:
            [updateTemplateName setBackgroundColor:ksecondSkin];
            break;
            
        case THIRD_TYPE:
            [updateTemplateName setBackgroundColor:kThirdSkin];
            break;
        
            
        default:
            [updateTemplateName setBackgroundColor:kOldSkin];
            break;

    }
    END_METHOD
}

#pragma mark - POST REQUESTS
//TODO: ADD/ EDIT Template
//Method for ppostRequest For Template
-(void)postRequestForTemplate:(NSDictionary *)dictTemplate{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictTemplate,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictTemplate
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictTemplateTemp =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictTemplate,dictTemplateTemp);
                                   
                                   NSString *strAction =   [dictTemplateTemp objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"updateTemplateName"]){
                                       
                                       //Invoke method for add temoplate in local db
                                       [self updateTemplateNameInlocalDB:dictTemplateTemp];
                                       
                                   }else{
                                       
                                   }
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                       
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}


-(void)updateTemplateNameInlocalDB:(NSDictionary *)dictemp{
    
    int fail = [[DataManager initDB] updateTemplateName:[dictemp objectForKey:@"templateName"] oldTemplate:[[dictemp objectForKey:@"templateId"] intValue ]];
    if (!fail) {
        
        //Alert view
        [[[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Template Updated Successfully", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        lblTemplateName.text = [dictemp objectForKey:@"templateName"];
    } else {
        
        [[[UIAlertView alloc] initWithTitle:kAppName message:NSLocalizedString(@"Template Updated Successfully", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
    }
    
}

//TODO: ADD / UPDATE TEMPLATE PROGRAMS
//Method for post request for Programs
-(void)postRequestForTemplatePrograms:(NSDictionary *)dictTemplatePrograms{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictTemplatePrograms,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictTemplatePrograms
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictTemplateProgram =  [[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA];
                                   
                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictTemplatePrograms,dictTemplateProgram);
                                   
                                   NSString *strAction =   [dictTemplateProgram objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addTemplatePrograms"]){
                                       
                                       //Invoke method for add temoplate in local db
                                       [self saveTemplateProgramInlocalDB:dictTemplateProgram];
                                       
                                   }else{
                              
                                       //Update in local DB
                                       [[DataManager initDB] updateTemplateSheetName:[dictTemplateProgram objectForKey:ksheetName] forSheetID:[[dictTemplateProgram objectForKey:kSheetId] intValue]];
                                       
                                       [arrSheetsList setArray:[[DataManager initDB]getTemplateSheetList:self.intCurrentTemplateID]];

                                       [self fillArrayWithData];
                                       [self setSheetValues];
                                       [tblProgramView reloadData];
                                       
                                   }
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                       
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}

-(void)saveTemplateProgramInlocalDB:(NSDictionary *)dictprogram{
    
}



//TODO: ADD / UPDATE TEMPLATE WORKOUTS
//Method for post request for Add new workout
-(void)postRequestForTemplateWorkouts:(NSDictionary *)dictTemplateWorkouts{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictTemplateWorkouts,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictTemplateWorkouts
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSMutableDictionary *dictTemplateWorkouts =  [[[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] mutableCopy];
                                   [dictTemplateWorkouts setValue:[dictTemplateWorkouts objectForKey:kBlockNo] forKey:kBlockId];
                                   
                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictTemplateWorkouts,dictTemplateWorkouts);
                                   
                                   NSString *strAction =   [dictTemplateWorkouts objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addWorkout"]){
                                       
                                       //Invoke method for add temoplate in local db
                                       [self saveTemplateWorkoutsInlocalDB:dictTemplateWorkouts];
                                       
                                   }else{
                                       
                                       //Update Workout in local
                                       [[DataManager initDB] updateBlockDate_Title:[[dictTemplateWorkouts objectForKey:kSheetId] integerValue] :[[dictTemplateWorkouts objectForKey:kBlockId] intValue]:[dictTemplateWorkouts objectForKey:kDate] :[dictTemplateWorkouts objectForKey:ksheetName]];
                                       
                                       [self fillArrayWithData];
                                       [self setSheetValues];
                                       [tblProgramView reloadData];

                                   }
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}

-(void)saveTemplateWorkoutsInlocalDB:(NSDictionary *)dictWorkouts{
    
    START_METHOD
    MESSAGE(@"saveTemplateWorkoutsInlocalDB-> %@",dictWorkouts);
    

    
    [[DataManager initDB] insertTemplateSheet_Blocks:dictWorkouts];
    
    //Get Blocks
    arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
    currentBlockIndex = arrayBlocks.count-1;
    self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
    
    
    
    [self fillArrayWithData];
    [self setSheetValues];
    [tblProgramView reloadData];
    
    END_METHOD
}



//TODO: ADD / UPDATE TEMPLATE WORKOUTS
//Method for post request for Add new workout
-(void)postRequestForTemplateExercise:(NSDictionary *)dictTemplateExercise{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictTemplateExercise,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictTemplateExercise
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSMutableDictionary *dictTemplateExercise =  [[[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] mutableCopy];
                                   [dictTemplateExercise setValue:[dictTemplateExercise objectForKey:PROGRAM_EXERCISE_ID] forKey:kBlockDataId];

                                   MESSAGE(@"arrayUserData send updateNotes Dictonary : %@ \n and response from server dictWorkout: %@",dictTemplateExercise,dictTemplateExercise);
                                   
                                   NSString *strAction =   [dictTemplateExercise objectForKey:ACTION];
                                   
                                   if([strAction isEqualToString:@"addExercise"]){
                                       
                                       //Invoke method for add temoplate in local db
                                       [self saveTemplateExerciseInlocalDB:[dictTemplateExercise mutableCopy]];
                                       
                                   }else{
                                       
                                       
                                       //Update Exercise in local
                                       [[DataManager initDB]  updateTemplateProgrammedData:dictTemplateExercise];
                                       
                                       [self fillArrayWithData];
                                       NSInteger rowIdReload = [[dictTemplateExercise valueForKey:@"RowId"]integerValue];
                                       [tblProgramView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIdReload inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                                   }
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       
                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}

-(void)saveTemplateExerciseInlocalDB:(NSMutableDictionary *)dictExercise{
    MESSAGE(@"saveTemplateExerciseInlocalDB-> %@",dictExercise);
    
    //Invoke for insert into Local DB
    [[DataManager initDB] insertTemplate_ProgramedData:dictExercise];
    
    [self fillArrayWithData];
    [self setSheetValues];
    [tblProgramView reloadData];
}

//TODO: Paste request for template entire program

-(void)careteDictForRequestForPasteEntireTemplateProgram:(int)intTemplateSheetId{
    START_METHOD
    
    MESSAGE(@"[commonUtility retrieveValue:KEY_USER_ID]: %@",[commonUtility retrieveValue:KEY_TRAINER_ID]);
    
    //Array for Add workout ids
    NSMutableArray *arrayWorkoutsIds    =   [[NSMutableArray alloc]init];

    NSArray * arrSheetBlocks = [[DataManager initDB]getBlockDataFor_TemplateSheet:intTemplateSheetId];
    
    for (NSDictionary *dic in arrSheetBlocks) {
        
//        NSDictionary * dicBlockData = [NSDictionary dictionaryWithObjectsAndKeys:[[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:@"sheetID"],@"sheetID",[NSNumber numberWithInt:1],@"blockNo",[dic valueForKey:kBlockTitle],@"sBlockTitle", [dic valueForKey:kBlockDate],@"dBlockDate", nil];
        
        
        //Add workout ids
        [arrayWorkoutsIds addObject:[dic objectForKey:kBlockNo]];
        
    }
    
    NSString *strTempPRogramId =   [[arrSheetsList objectAtIndex:currentSheetIndex] valueForKey:ksheetID];
    //Craete the dictionry for send to copy paste
    NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
    
    
    //Set the valuse for copy paste entire program of template
    [objdict setValue:@"copyPasteProgram"           forKey:ACTION];
    [objdict setValue:strTempPRogramId              forKey:@"program_id"];
    [objdict setValue:arrayWorkoutsIds              forKey:@"workout_id"];
    [objdict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
    
    
    
    //Invoke method for Copy paste exercsie exercise
    [self postRequestForCopyEntireTemplateProgram:objdict];
    END_METHOD
}


//TODO: POST REQUSET FOR ENTIRE PROGRAM COPY PASTE
-(void)postRequestForCopyEntireTemplateProgram:(NSMutableDictionary *)dictCopyProgram{
    START_METHOD
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@savedataInfo/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params dictNotes=: %@ and Url : %@",dictCopyProgram,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:dictCopyProgram
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   
                                   //************  GET Client's Profile AND SAVE IN LOCAL DB
                                   NSMutableDictionary *dictCopyProgramTemp =  [[[responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] mutableCopy];
                                   
                                   MESSAGE(@"arrayUserData delete dictClientExercsie send Dictonary : %@ \n and response from server addNotes: %@",dictCopyProgram,dictCopyProgramTemp);
                                   
                                   
                                   [dictCopyProgramTemp setValue:[dictCopyProgram objectForKey:@"copyPasteTempalteWorkouts"] forKey:@"copyPasteTempalteWorkouts"];
                                
                                   
                                   //Invoke method for save in loacl db
                                   [self copyPasteInLocal:dictCopyProgramTemp];
                                   
                               }else{//If Server respose a Error
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       //Hide Indicator
                                       [TheAppController hideHUDAfterDelay:0];

                                       //For Unauthorized user --->
                                       if( [dictError objectForKey:LIST_KEY]){
                                           
                                           NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                                           
                                           if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                                               
                                               //Show Error Alert
                                               [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                               
                                               //Move to dashbord for login
                                               [commonUtility  logOut];
                                           }
                                           
                                       }
                                       
                                       //<-----
                                   }
                               }
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                           }];
    END_METHOD
}


-(void)copyPasteInLocal:(NSDictionary *)responseDataDictionary{
    START_METHOD
    
    //Save all the workouts
    if([responseDataDictionary objectForKey:@"workout_list"]){
        
        //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsWorkoutsAndNotes =  [responseDataDictionary objectForKey:@"workout_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsWorkoutsAndNotes: %@",arrayclientsWorkoutsAndNotes);
        
        //Save Clients workouts and notes
        [self saveClientsTemplatesWorkouts:arrayclientsWorkoutsAndNotes];
    }
    
    
    if([responseDataDictionary objectForKey:@"exercise_list"]){
        
        //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsExewrcise =  [responseDataDictionary objectForKey:@"exercise_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsWorkoutsAndNotes: %@",arrayclientsExewrcise);
        
        //Save Client's exercise
        [self saveClientsTemplatesExercise:arrayclientsExewrcise];
    }
    
    //Invoke method for realod table
    [self reloadTableViewAfterPaste:responseDataDictionary];
   
    
    END_METHOD
}


-(void)reloadTableViewAfterPaste:(NSDictionary *)responseDataDictionary{
 
    START_METHOD
    
    
    MESSAGE(@"responseDataDictionary-> %@",responseDataDictionary);
    
    NSUserDefaults  *defauts = [NSUserDefaults standardUserDefaults];

    
    if([responseDataDictionary objectForKey:@"copyPasteTempalteWorkouts"]){
        
        [defauts setObject:nil forKey:kCopyBlock];
        [defauts setObject:nil forKey:kCopyProgramBlock];
        [defauts setObject:nil forKey:kCopiedBlockIDSheetID];
        
        [self fillArrayWithData];
        [self setSheetValues];
        [tblProgramView reloadData];
      
    }else{
    
        //Get Blocks
        arrayBlocks= [NSArray arrayWithArray:[[DataManager initDB]getBlockDataFor_TemplateSheet:self.intCurrentSheetID]];
        currentBlockIndex = arrayBlocks.count-1;
        self.intCurrentBlockID = [[[arrayBlocks objectAtIndex:currentBlockIndex] valueForKey:@"nBlockID"] intValue];
        
        NSDictionary *tempDict = [NSDictionary dictionaryWithObjectsAndKeys:nil,kClientId,nil,ksheetID, nil];
        
        [defauts setValue:tempDict forKey:kProgramData];
        [defauts synchronize];
        
        [defauts setValue:nil forKey:kTemplateSheetData];
        [defauts synchronize];
        [self updateSheetData:YES];
        
    }
    
    END_METHOD
}

//Save clients in local DB
-(void)saveClientsTemplatesWorkouts:(NSArray *)arrayTemplateWorkouts{
    
    START_METHOD
    
    //If array has data
    if (arrayTemplateWorkouts && arrayTemplateWorkouts.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayTemplateWorkouts.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictTemplateWorkouts = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayTemplateWorkouts objectAtIndex:i]] mutableCopy];
            
            [dictTemplateWorkouts setValue:[dictTemplateWorkouts objectForKey:@"nBlockNo"] forKey:@"blockNo"];
            [dictTemplateWorkouts setValue:[dictTemplateWorkouts objectForKey:@"nSheetID"] forKey:@"sheetID"];
            
            
            MESSAGE(@"cleints dictTemplateWorkouts: %@",dictTemplateWorkouts);
            
            //Invoke method for insert exercise
            [[DataManager initDB] insertTemplateSheet_Blocks:dictTemplateWorkouts];
        }
        
    }
}



//Save clients in local DB
-(void)saveClientsTemplatesExercise:(NSArray *)arrayTemplateExercise{
    
    START_METHOD
    
    //If array has data
    if ([arrayTemplateExercise isKindOfClass:[NSArray class]]&& arrayTemplateExercise.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayTemplateExercise.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictTemplateExercise =[ [commonUtility dictionaryByReplacingNullsWithStrings:[arrayTemplateExercise objectAtIndex:i]] mutableCopy];
            
            [dictTemplateExercise setValue:[dictTemplateExercise objectForKey:PROGRAM_EXERCISE_ID] forKey:kBlockDataId];
            [dictTemplateExercise setValue:[dictTemplateExercise objectForKey:kDate] forKey:kEXDate];
            
            
            
            MESSAGE(@"cleints dictExercise: %@",dictTemplateExercise);
            
            //Invoke method for insert exercise
            [[DataManager initDB] insertTemplate_ProgramedData:dictTemplateExercise];
        }
        
    }
    
}


//TODO: Craete dict for paste template workouts
-(void)careteDictForRequestForPasteTemplateWorkouts{//For Updated Version
    START_METHOD
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    

    NSDictionary *dictCopiedBlock = [defaults valueForKey:kCopyBlock];
        MESSAGE(@"[defaults valueForKey:kCopyBlock]) dictCopiedBlock: %@",dictCopiedBlock);

    //Array of total workouts ids
    NSMutableArray *arrayWorkoutsIds    =   [[NSMutableArray alloc]init];
    
    //Get the Workout Array
    NSArray             *arrayWorkouts   =   [dictCopiedBlock objectForKey:@"copiedLinesForBlock"];

    //Loop for Add the workout ids
    for ( int i=0; i<arrayWorkouts.count; i++) {
        
        
        //Add id in array
         [arrayWorkoutsIds addObject:[[arrayWorkouts objectAtIndex:i] objectForKey:kBlockId]];
    }
   
   
    
    //Craete the dictionry for send to copy paste
    NSMutableDictionary *objdict = [[NSMutableDictionary alloc]init];
    [objdict setValue:@"copyPasteProgram"                       forKey:ACTION];
    [objdict setValue:@"copyPasteTempalteWorkouts"              forKey:@"copyPasteTempalteWorkouts"];
    
    [objdict setValue:[NSString stringWithFormat:@"%ld",(long)self.intCurrentSheetID]   forKey:@"program_id"];
    [objdict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID]                      forKey:@"user_id"];
    [objdict setValue:arrayWorkoutsIds                                                  forKey:@"workout_id"];
    
    
    
    //Invoke method for paste workouts
    [self postRequestForCopyEntireTemplateProgram:objdict];
 
    END_METHOD
}





//TODO: DELETE EXERCISE REQUEST
-(void)postRequestForDeleteExercise:(NSDictionary *)params{
    START_METHOD
    
    
    NSString *strUrl =[NSString stringWithFormat:@"%@deleteData/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"postRequestForDeleteExercise-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                                       success:^(NSDictionary *responseDataDictionary) {
                                           
                                           MESSAGE(@"postRequestForDeleteExercise->responce: %@", responseDataDictionary);
                                           
                                           
                                           //Invoke method for delete Exercise
                                           [self deleteTemplateExercise:[[params objectForKey:@"program_exercise_id"] intValue]];
                                           
                                           //Hide the indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                       }failure:^(NSError *error) {
                                           MESSAGE(@"Eror : %@",error);
                                           
                                           //Hide the indicator
                                           [TheAppController hideHUDAfterDelay:0];
                                           
                                           //Show Alert For error
[commonUtility alertMessage:@"No internet detected, please connect to internet!"];                                           
                                           
                                           
                                           
                                       }];
    END_METHOD
    

}

//TODO: Delete the Template Exercise
-(void)deleteTemplateExercise:(int)intTemplateExerciseId{
    START_METHOD
    
    
    [[DataManager initDB] deleteLineForLineID:intTemplateExerciseId];
    [self fillArrayWithData];
    [tblProgramView reloadData];
    
    END_METHOD
}
@end