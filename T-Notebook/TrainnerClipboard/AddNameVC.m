//
//  AddNameVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "AddNameVC.h"

@interface AddNameVC ()<UITextFieldDelegate>{
    __weak IBOutlet UIButton *btnDone;
    CGPoint centerPoint;
}
@property(nonatomic,weak) IBOutlet UITextField  *txtEnterName;
@property(nonatomic,weak) IBOutlet UILabel      *lblTitle;

@end

@implementation AddNameVC
@synthesize delegate;

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (void)setFrameOfFormSheet {
    if (iOS_7) {
        [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth -300, kLandscapeHeight+200)];
        self.navigationController.view.superview.frame = CGRectMake(0, 0, 205, 350);
    
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (orientation == UIInterfaceOrientationLandscapeLeft) {
            self.navigationController.view.superview.center = CGPointMake(centerPoint.x - 120, centerPoint.y);
            MESSAGE(@"Left %@",NSStringFromCGPoint(self.navigationController.view.superview.center));
        } else if (orientation == UIInterfaceOrientationLandscapeRight) {
            self.navigationController.view.superview.center = CGPointMake(centerPoint.x + 175, centerPoint.y);
            MESSAGE(@"Right %@",NSStringFromCGPoint(self.navigationController.view.superview.center));
        
        } else {
            [self.view setFrame:CGRectMake(0, 0, kPotraitWidth, kPotraitHeight)];
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 350, 205);
            self.navigationController.view.superview.center = centerPoint;
        }
    }
}

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
     self.navigationController.navigationBarHidden = false;
    
    btnDone.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnDone.layer.cornerRadius = 4.0;
    btnDone.backgroundColor = kAppTintColor;
    
    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(btnCancelClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -15;
    UIBarButtonItem *leftBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, leftBtn, nil] animated:NO];
    
     self.txtEnterName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
}

//-----------------------------------------------------------------------

- (void)keyboardWillShow:(NSNotification *)notification {
    [self setFrameOfFormSheet];
}

//-----------------------------------------------------------------------

- (void)keyboardWillHide {
    self.navigationController.view.superview.center = centerPoint;
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    START_METHOD
    [super viewWillAppear:animated];
    
    // Weblineindia //
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
    
    [btnDone setTitle:NSLocalizedString(btnDone.titleLabel.text, @"") forState:UIControlStateNormal];
    
    self.txtEnterName.text = self.strFiledContent;
    
    if ([self.strScreenName isEqualToString:@"EnterTrainerName"]) {
        self.lblTitle.text = NSLocalizedString(@"Enter Trainer Name", @"");
        [self.txtEnterName setKeyboardType:UIKeyboardTypeDefault];
    }
    else if ([self.strScreenName isEqualToString:@"EnterSheetName"]){
        self.lblTitle.text = NSLocalizedString(@"Enter Sheet Name", nil);
          [self.txtEnterName setKeyboardType:UIKeyboardTypeDefault];
    }
//TODO: TItle Alert for Trainer's Email
    else if ([self.strScreenName isEqualToString:@"EnterTrainerEmail"]){
        self.lblTitle.text = NSLocalizedString(@"Enter Email ID", nil);
        [self.txtEnterName setKeyboardType:UIKeyboardTypeDefault];
    }
    
    
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (iOS_7) {
            [self.view setFrame:CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight)];
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 350, 205);
            self.navigationController.view.superview.center = self.view.center;
        }
    } else {
        if (iOS_7) {
            [self.view setFrame:CGRectMake(0, 0, kPotraitWidth, kPotraitHeight)];
            self.navigationController.view.superview.frame = CGRectMake(0, 0, 350, 205);
            self.navigationController.view.superview.center = self.view.center;
        }
    }
    
    
}

//-----------------------------------------------------------------------

- (void)viewDidAppear:(BOOL)animated {
    centerPoint = self.navigationController.view.superview.center;
}

//-----------------------------------------------------------------------

- (IBAction)btnDoneClicked:(id)sender {
    NSString *strValue = self.txtEnterName.text;
    if (allTrim(strValue).length == 0) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text", nil), kAppName);
        return;
    }
    if ([self.strScreenName isEqualToString:@"EnterTrainerName"]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.txtEnterName.text forKey:kTrainerName];
        [defaults synchronize];
    }
    else if ([self.strScreenName isEqualToString:@"EnterSheetName"]){
        [self.delegate createOrUpdateSheetForName:self.txtEnterName.text isUpdate:self.isUpdate];
    }

//TODO: Alert for Trainer's Email
    else if ([self.strScreenName isEqualToString:@"EnterTrainerEmail"]){
        
        //Check Email valid or not
        if(![commonUtility validateEmailWithString:strValue])    {
            [commonUtility alertMessage:NSLocalizedString(@"Please enter valid Email ID", nil)];
            return;
        }else{
            
            [self.delegate requestForPassword:self.txtEnterName.text];
        }
    }
    
    if([self.strScreenName isEqualToString:@"EnterTrainerName"]) {
        
        [self.navigationController dismissViewControllerAnimated:true completion:^{
             [self.delegate createOrUpdateSheetForName:nil isUpdate: NO];
        }];
        
    } else {
        [self.navigationController dismissViewControllerAnimated:true completion:nil];

    }
    

}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

}

//---------------------------------------------------------------------------------------------------------------
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if([self.strScreenName isEqualToString:@"EnterTrainerName"]) {
        NSRange lowercaseCharRange;
        lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        if (lowercaseCharRange.location != NSNotFound) {
            
            textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:[string uppercaseString]];
            return NO;
        }
    } else {
        NSRange lowercaseCharRange;
        lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        if (lowercaseCharRange.location != NSNotFound && textField.text.length== 0) {
            
            textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:[string uppercaseString]];
            return NO;
        }

    }
    
       return YES;
}



//-----------------------------------------------------------------------

-(void)btnCancelClicked{
    
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
    
}


//-----------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}


//-----------------------------------------------------------------------


- (void)deviceOrientationDidChange:(id *)center {
    [self setFrameOfFormSheet];
}

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
