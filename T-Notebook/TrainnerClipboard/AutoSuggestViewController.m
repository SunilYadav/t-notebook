    //
//  MedicineTagViewController.m
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "AutoSuggestViewController.h"


@implementation AutoSuggestViewController

@synthesize tblSuggestion = _tblSuggestion;
@synthesize delegate;
@synthesize txtEx = _txtEx;
@synthesize arrayExercise = _arrayExercise;
@synthesize arrayMain = _arrayMain;
@synthesize strSelExercise;


#pragma mark -
#pragma mark view lifecycle methods

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    self.txtEx.autoresizingMask = UIViewAutoresizingNone;
    [super viewDidLoad];
	self.tblSuggestion.delegate = self;
	self.tblSuggestion.dataSource = self;
	self.txtEx.delegate =self;
	self.txtEx.text = self.strSelExercise;

    self.arrayMain = [NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"defaultExercise"]];
    
    self.arrayExercise = [NSArray arrayWithArray:self.arrayMain];
    self.automaticallyAdjustsScrollViewInsets = NO;
	UIBarButtonItem * btnCancel = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",nil) style:UIBarButtonItemStylePlain target:self action:@selector(btncancel_click)];
	[self.navigationItem setLeftBarButtonItem:btnCancel];
	
	UIBarButtonItem * btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnDone_click)];
	[self.navigationItem setRightBarButtonItem:btnDone];
	self.title = NSLocalizedString(@"Exercises", nil);
	[self.txtEx becomeFirstResponder];
    if (iOS_7) {
        CGRect rect = self.txtEx.frame;
        rect.origin.y = rect.origin.y-44;
        self.txtEx.frame =rect;
        rect = self.tblSuggestion.frame;
        rect.origin.y = rect.origin.y-44;
        rect.size.height -= 44;
        self.tblSuggestion.frame = rect;
    }
}

//-------------------------------------------------------------------------------------------------------------

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.keyboardControls.delegate = nil;
    self.keyboardControls = nil;
}

//-----------------------------------------------------------------------

- (void) viewWillAppear:(BOOL)animated {
    START_METHOD
	[super viewWillAppear:YES];
    NSArray *fields = @[self.txtEx,self.txtEx];
    [self setKeyboardControls:[[KeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:(id)self];
    fields = nil;
    
    self.keyboardControls.activeField = self.txtEx;
    [self.keyboardControls.segmentedControl setEnabled:false forSegmentAtIndex:0];
    [self.keyboardControls.segmentedControl setEnabled:true forSegmentAtIndex:1];
}

//-----------------------------------------------------------------------

- (void)keyboardControls:(KeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(KeyboardControlsDirection)direction{
    [delegate getExercise:self.txtEx.text];
}

//-----------------------------------------------------------------------

- (void)keyboardControlsDonePressed:(KeyboardControls *)keyboardControls{
    	[delegate closeExercisePopover];
    [self.txtEx resignFirstResponder];
}

//-------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark action methods

- (IBAction) btnDone_click {
	[delegate getExercise:self.txtEx.text];
}

//--------------------------------------------------------------------------------------------------

- (IBAction) btncancel_click {
	[delegate closeExercisePopover];
}

//--------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark Custom methods

- (void) reloadTableView:(NSString *) str {
	if ([str isEqualToString:@""]) {
        self.arrayExercise = [NSArray arrayWithArray:self.arrayMain];
		[self.tblSuggestion reloadData];
	}
	else {
		NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", str];
        self.arrayExercise = [NSArray arrayWithArray:[self.arrayMain filteredArrayUsingPredicate:predicate]];
		[self.tblSuggestion reloadData];
	}
}


//-------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark UITextField delegate methods

//----------------------------------------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	
	NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

	[self reloadTableView:newString];
	[delegate exerciseValueChanged:newString];

    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
	return YES;
}

//-------------------------------------------------------------------------------------------------------------

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

#if defined(__IPHONE_6_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0

//-------------------------------------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return YES;
}

//-----------------------------------------------------------------------------------------------------------------------------

-(NSUInteger)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskAll;
}

#endif

//-----------------------------------------------------------------------------------------------------------------------------


- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	[delegate getExercise:self.txtEx.text];
	return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [delegate changePopOverFrame];
}

//-------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark UITableView delegate methods

//-------------------------------------------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section{
	return [self.arrayExercise count];
}

//--------------------------------------------------------------------------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	cell.textLabel.text = [self.arrayExercise objectAtIndex:indexPath.row];
	return cell;
}

//-------------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[delegate getExercise:[self.arrayExercise objectAtIndex:indexPath.row]];
}

//-------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark memory management methods

//-------------------------------------------------------------------------------------------------

- (void)viewDidUnload {
    self.tblSuggestion = nil;
    self.arrayExercise = nil;
	self.arrayMain = nil;
    [super viewDidUnload];
}

//-----------------------------------------------------------------------

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [delegate didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}
//-------------------------------------------------------------------------------------------------

@end
