//
//  ViewProgramCell.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewProgramCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblProgramName;
@property (strong, nonatomic) IBOutlet UILabel *lblDayDate;
@property (strong, nonatomic) IBOutlet UILabel *lblNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnCopy;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnShowDetail;
@property (weak, nonatomic) IBOutlet UIButton *buttonTempShowDeatil;
@property (strong, nonatomic) IBOutlet UIImageView *imgvFirstLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvSecondLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgvThirdLine;

-(void)configCell :(NSDictionary *)detail;
@end
