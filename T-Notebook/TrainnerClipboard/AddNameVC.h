//
//  AddNameVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AddNameDelegate <NSObject>
-(void)createOrUpdateSheetForName:(NSString*)strSheetName isUpdate:(BOOL)isUpadate;
-(void)requestForPassword:(NSString *)strEmail;

@end


@interface AddNameVC : UIViewController{
  id<AddNameDelegate> __weak delegate;
}
@property (nonatomic, weak) id<AddNameDelegate> delegate;

@property(nonatomic,strong) NSString  *strFiledContent;
@property(nonatomic,strong) NSString  *strScreenName;
@property(nonatomic) BOOL                  *isfromParQ;
@property(nonatomic,readwrite) BOOL isUpdate;


@end
