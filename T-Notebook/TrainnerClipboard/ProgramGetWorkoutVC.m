//
//  ProgramGetDataVC.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "ProgramGetWorkoutVC.h"
#import "ProgramViewController.h"
#import "ClientViewController.h"

@interface ProgramGetWorkoutVC () <UITextFieldDelegate>
{
    ProgramViewController * ProgramVCOBJ;
    __weak IBOutlet UIButton *btnAdd;
    __weak IBOutlet UILabel *lblFiledTitle;
}
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPurpose;
@property (strong, nonatomic)  UITextField *activeTxt;


@end

@implementation ProgramGetWorkoutVC

//-----------------------------------------------------------------------

#pragma mark - Memory Management Methods

//-----------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-----------------------------------------------------------------------

#pragma mark - TextField Delegate

//-----------------------------------------------------------------------

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.activeTxt = textField;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (void)deviceOrientationDidChange:(id *)center {
    
    if (iOS_7) {
        self.view.frame = CGRectMake(0,44, kLandscapeWidth-350, kLandscapeHeight+200);
        self.navigationController.view.superview.frame = CGRectMake(0,0,160, 500);
        
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (orientation == UIInterfaceOrientationLandscapeLeft) {
            self.navigationController.view.superview.center = CGPointMake(self.view.center.x - 10, self.view.center.y);
        } else if (orientation == UIInterfaceOrientationLandscapeRight) {
            self.navigationController.view.superview.center = CGPointMake(self.view.center.x + 100, self.view.center.y);
        } else {
            self.view.frame = CGRectMake(0, 44, kPotraitWidth, kPotraitHeight);
            self.navigationController.view.superview.frame = CGRectMake(0,0, 500, 160);
            self.navigationController.view.superview.center = self.view.center;
        }
    }
}

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    [btnAdd setTitle:NSLocalizedString(btnAdd.titleLabel.text, nil) forState:0];
    
    
    lblFiledTitle.text = NSLocalizedString(lblFiledTitle.text, nil);
    CGRect rect = [[AppDelegate sharedInstance] getWidth:lblFiledTitle.text font:lblFiledTitle.font height:21];
    [lblFiledTitle setFrame:CGRectMake(lblFiledTitle.frame.origin.x - 20, lblFiledTitle.frame.origin.y, rect.size.width + 5, lblFiledTitle.frame.size.height)];
}


//-----------------------------------------------------------------------

-(void)NavBarBtnTap :(UIBarButtonItem *)sender {
    
    if ([sender tag] == 201) {
        // back button
        [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([sender tag] == 202) {
        // done button
        [self.activeTxt resignFirstResponder];
        
        if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
        {
            if (self.txtName.text.length == 0 ) {
                DisplayAlertWithTitle(NSLocalizedString(@"Please enter all vlaues.", nil), kAppName)
            }
            else{
                [_delegate addNewWorkout:self.txtName.text :@"FromAdd"];
                self.txtName.text = nil;
                [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            [_delegate addNewWorkout:self.txtName.text :@"FromEdit"];
            self.txtName.text = nil;
            [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
}

//-----------------------------------------------------------------------

-(IBAction)btnAddWorkOutClicked:(id)sender{
    
    [self.activeTxt resignFirstResponder];
    
    if (self.txtName.text.length == 0 ) {
        DisplayAlertWithTitle(NSLocalizedString(@"Please enter text.", nil), kAppName)
    } else{
        
        [self.activeTxt resignFirstResponder];
        
        if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
        {
            if (self.txtName.text.length == 0 ) {
                DisplayAlertWithTitle(NSLocalizedString(@"Please enter all vlaues.", nil), kAppName)
            }
            else{
                [_delegate addNewWorkout:self.txtName.text :@"FromAdd"];
                self.txtName.text = nil;
                [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            if (self.isEditTemplateSheet) {
                [_delegate addNewWorkout:self.txtName.text :@"EditSheet"];
            }
            else{
                [_delegate addNewWorkout:self.txtName.text :@"FromEdit"];
            }
            [[self parentViewController]dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
}
//-----------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self localizeControls];

    btnAdd.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnAdd.layer.cornerRadius = 4.0;
    btnAdd.backgroundColor = kAppTintColor;
    self.txtName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtPurpose.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
    lblTitle.text = NSLocalizedString(@"Add Workout", nil);
    lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
    lblTitle.textColor = kAppTintColor;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.adjustsFontSizeToFitWidth=YES;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=lblTitle;
    [self addNavBarButtons];
    
    if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
    {
        lblTitle.text = NSLocalizedString(@"Add Workout", nil);
    }
    else
    {
        if (self.isEditTemplateSheet) {
            lblTitle.text =NSLocalizedString(@"Edit Program Name", nil) ;
            lblFiledTitle.text = NSLocalizedString(@"Program Name", nil);
            CGRect rect = [[AppDelegate sharedInstance] getWidth:lblFiledTitle.text font:lblFiledTitle.font height:21];
            [lblFiledTitle setFrame:CGRectMake(lblFiledTitle.frame.origin.x - 20, lblFiledTitle.frame.origin.y, rect.size.width + 5, lblFiledTitle.frame.size.height)];
        }
        else{
            lblTitle.text = NSLocalizedString(@"Edit Workout", nil);
        }
        self.txtName.text = self.strSheetName;
    }
    
    
    self.txtName.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    
}


//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated {
    START_METHOD
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (iOS_7) {
            self.view.frame = CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight);
            self.navigationController.view.superview.frame = CGRectMake(0,0, 500, 160);
            self.navigationController.view.superview.center = self.view.center;
        }
    } else {
        if (iOS_7) {
            self.view.frame = CGRectMake(0, 0, kPotraitWidth, kPotraitHeight);
            self.navigationController.view.superview.frame = CGRectMake(0,0, 500, 160);
            self.navigationController.view.superview.center = self.view.center;
        }
    }
}

//-----------------------------------------------------------------------


-(BOOL)shouldAutorotate {
    return true;
}

//-----------------------------------------------------------------------

@end
