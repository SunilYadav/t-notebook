//
//  ParQquestionView.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "ParQquestionView.h"
#import "SignatureVC.h"
#import "FileUtility.h"
#import "TearmsAndConditionVC.h"

 
// ===== Header Frame Const ===========//

#define klblHeaderX	10
#define klblHeaderW	660
#define klblHeaderH	25

//======== Yes or No Frame ==========//

#define klblYesX	575
#define klblYesW	35
#define klblYesH	25

#define klblNoX		645
#define klblNoW		35
#define klblNoH		25

#define	kimgYesX        520
#define kimgYesW	25
#define kimgYesH	25

#define	kimgNoX		600
#define kimgNoW		25
#define kimgNoH		25

//=========== Questions Frame===========//

#define klblQuestionX	15
#define klblQuestionW	510
#define klblQuestionH	21


//===== gap brtwwen headers and content ====//

#define kHQGap		12
#define	kQQGap		10


#define kSelectedBtnBGColor [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]


@implementation ParQquestionView
{
    //sunil
     UILabel * lblMainHeader;
    UILabel * lblHeader;
    UILabel * viewUnderLine1;
    UILabel * lblYes1;
    UILabel * lblNo1;
    
    UILabel * viewUnderLine2;
    UILabel * lblQuestion1;
    UITextView * txtAns1;
    UILabel * lblDescr1;
    UILabel *signHere1;
    UIButton * btnPdfLink1;
    UIButton * btnSaveParQ1;
    
    UILabel * lblMainHeader1;
    UILabel * lblHeader2;
    UILabel * lblYes2;
    UILabel * lblNo2;
    UILabel * lblQuestion2;
    UILabel * lblDescr2 ;
    UILabel *signHere2;
    UIButton * btnPdfLink2;
    
    //Sunil
    UILabel * lblQuestion3;
    UIButton * imgYes3;
    UIButton * imgNo3;
    UIButton * btnSaveParQ2;
}

@synthesize strClientId;
@synthesize termsAccepted;

int   currentYCoord = 5;
int   contentTag       = 1000;
static int oldValue = 0;

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        [self setFrame:frame];
        
    }
    currentYCoord = 5;
    contentTag = 1000;
    oldValue = 0;
    
    [self fetchAllDataToDisplay];
    [self setupLayout];
    return self;
}


//---------------------------------------------------------------------------------------------------------------

#pragma mark : Custom Methods

//---------------------------------------------------------------------------------------------------------------

- (void) imgYesClick:(id)sender {
    
    UIButton * btn = (UIButton *) sender;

    if([btn isSelected]) {
      // Set unselected
        [btn setSelected:FALSE];
        [btn setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];

    } else {
        // set selected
        [btn setSelected:TRUE];
        [btn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
    }
    
    
    int i = (int)[btn tag] + 1;
    UIButton * btn1 = (UIButton *) [self viewWithTag:i];
    [btn1 setSelected:FALSE];
    [btn1 setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
  
    //Commented by SUNIL
//    [self autoSaveContents:(int)btn.tag];
}

//---------------------------------------------------------------------------------------------------------------

- (void) imgNoClick:(id)sender {
    
    UIButton * btn = (UIButton *) sender;
    
    if([btn isSelected]) {
        [btn setSelected:FALSE];
        [btn setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
    } else {
        [btn setSelected:TRUE];
        [btn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
    }

    int i = (int)[btn tag] - 1;
    UIButton * btn1 = (UIButton *) [self viewWithTag:i];
    [btn1 setSelected:FALSE];
    [btn1 setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];

    //Commented by SUNIL
//    [self autoSaveContents:(int)btn.tag];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) openSignatureView:(id)sender {
    
    int tag = contentTag - 2;
    UIImageView *imgvew = (UIImageView *) [self viewWithTag:tag];
     [self.parqQuestDelegate showSignatureViewWithImage:imgvew.image];
    

}


//---------------------------------------------------------------------------------------------------------------

-(void)openPdf {
    
    [self.parqQuestDelegate openTheTermsAndConditions];
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)saveParQData {
    [self.parqQuestDelegate saveParQDataFromView];

}


//---------------------------------------------------------------------------------------------------------------

-(void)btnSubmit_click {
    
    if ([self.strClientId length]!=0) {
        int chk = 0;
        
        int datatag = 1000;
        int k = 0;
        for (int i = 1; i<= [self.aryCategory count]; i++) {
            if (i==1 || i==2) {
                datatag++;
                datatag++;
            }
            datatag++;
            datatag++;
            datatag++;
            datatag++;
            for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++,k++) {
                BOOL flag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionBool] boolValue];
                BOOL TextFlag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionText] boolValue];
                NSString * qID = [[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionId];
                datatag++;
                NSString * ansYesNo = @"0";
                NSString * ansText = @"";
                if (flag) {
                    UIButton * btn = (UIButton *)[self viewWithTag:datatag];
                    datatag++;
                    if([btn isSelected]) {
                      ansYesNo = @"1";
                    }
                    if ([btn imageForState:UIControlStateNormal] == self.imgSelected) {
                        ansYesNo = @"1";
                    }
                    else {
                        btn = (UIButton *)[self viewWithTag:datatag];
                        if([btn isSelected]) {
                            ansYesNo = @"2";
                        }
                        if ([btn imageForState:UIControlStateNormal] == self.imgSelected) {
                            ansYesNo = @"2";
                        }
                    }
                    datatag++;
                }
                if (TextFlag) {
                    UITextView * txtView = (UITextView *)[self viewWithTag:datatag];
                    datatag++;
                    datatag++;
                    ansText = txtView.text;
                }
                
                if ([self.aryAnswers count] == 0) {
                    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:self.strClientId,qID,ansYesNo,ansText,nil] forKeys:[NSArray arrayWithObjects:kClientId,kQuestionId,kBoolAnswer,kTextAnswer,nil]];
                    if ([[DataManager initDB] insertAnswerFor:dictInfo] == 1) {
                        chk = 1;
                    }
                }
                else {
                    NSString * ansID = [[self.aryAnswers objectAtIndex:k] objectForKey:kAnswerId];
                    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:ansYesNo,ansText,ansID,nil] forKeys:[NSArray arrayWithObjects:kBoolAnswer,kTextAnswer,kAnswerId,nil]];
                    if ([[DataManager initDB] updateAnswerFor:dictInfo] == 1) {
                        chk = 1;
                    }
                }
                
            }
        }
        if (chk == 0) {
            
            MESSAGE(@"ClientsSignature--->");
            
            int tag = contentTag - 2;
            UIImageView * signView2 = (UIImageView *) [self viewWithTag:tag];
            UIImage * img = [signView2 image];
            
            [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsSignature",[FileUtility basePath]]];
            
            NSString * imgNm = [NSString stringWithFormat:@"Signature%@",self.strClientId];
            NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
            [FileUtility createFileInFolder:@"ClientsSignature" withName:imgNm withData:imageData];
            
        }
        else {
        }
        
        if ([self.aryAnswers count] == 0) {
            int count = [[DataManager initDB] checkAnswerForClient:self.strClientId];
            if (count != 0) {
                for (int i = 1; i<= [self.aryCategory count]; i++) {
                    for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++) {
                        NSString * qID = [[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionId];
                        [self.aryAnswers addObject:[[DataManager initDB] answerForQuestion:qID :self.strClientId]];
                    }
                }
            }
        }
    }
    else {
    }
}

//---------------------------------------------------------------------------------------------------------------

- (void) autoSaveContents:(int) tag {
    START_METHOD
    if (![AppDelegate sharedInstance].isAutoSave || [self.strClientId isEqualToString:@"0"]) {
        [AppDelegate sharedInstance].isFieldUpdated = YES;
        return;
    }
   // oldValue = 0;
    DebugINTLOG(@"",tag);
    int datatag = 1000;
    int k = 0;
    for (int i = 1; i<= [self.aryCategory count]; i++) {
        if (i==1 || i==2) {
            datatag++;
            datatag++;
        }
        datatag++;
        datatag++;
        datatag++;
        datatag++;
        for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++,k++) {
            BOOL flag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionBool] boolValue];
            BOOL TextFlag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionText] boolValue];
            datatag++;
            if (flag) {
                NSString * ansYesNo = @"0";
                
                MESSAGE(@"self.aryAnswers-> %@",self.aryAnswers);
                
                NSString * ansID = [[self.aryAnswers objectAtIndex:k] objectForKey:kAnswerId];
                if (datatag == tag) {
                    UIButton * btn = (UIButton *)[self viewWithTag:datatag];
                    datatag++;
                    if([btn isSelected]) {
                         ansYesNo = @"1";
                    }
                    
                    if ([btn imageForState:UIControlStateNormal] == self.imgSelected) {
                        ansYesNo = @"1";
                    }
                    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                               kBoolAnswer,kfieldName,
                                               ansYesNo,kfieldValue,
                                               ansID,kAnswerId,
                                               @"2",kDictType,
                                               nil];
                    DebugLOG([dictInfo description]);
                    if ([[DataManager initDB] updateParqField:dictInfo]) {
                        DebugLOG(@"Not Updated");
                    }
                    else {
                        DebugLOG(@"Updated");
                    }
                }
                
                datatag++;
                
                if (datatag == tag) {
                    UIButton * btn = (UIButton *)[self viewWithTag:datatag];
                    datatag++;
                    
                    if([btn isSelected]) {
                        ansYesNo = @"2";
                    }
                    
                    if ([btn imageForState:UIControlStateNormal] == self.imgSelected) {
                        ansYesNo = @"2";
                    }
                    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                               kBoolAnswer,kfieldName,
                                               ansYesNo,kfieldValue,
                                               ansID,kAnswerId,
                                               @"2",kDictType,
                                               nil];
                    DebugLOG([dictInfo description]);
                    if ([[DataManager initDB] updateParqField:dictInfo]) {
                        DebugLOG(@"Not Updated");
                    }
                    else {
                        DebugLOG(@"Updated");
                    }
                }
                datatag++;
            }
            
            
            if (TextFlag) {
                if (datatag == tag) {
                    NSString * ansID = [[self.aryAnswers objectAtIndex:k] objectForKey:kAnswerId];
                    UITextView * txtView = (UITextView *)[self viewWithTag:datatag];
                    NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                               kTextAnswer,kfieldName,
                                               txtView.text,kfieldValue,
                                               ansID,kAnswerId,
                                               @"2",kDictType,
                                               nil];
                    DebugLOG([dictInfo description]);
                    if ([[DataManager initDB] updateParqField:dictInfo]) {
                        DebugLOG(@"Not Updated");
                    }
                    else {
                        DebugLOG(@"Updated");
                    }
                }
                datatag++;
                datatag++;
            }
        }
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)setupLayout {
    
    [self setBackgroundColor:[UIColor clearColor]];
    self.imgSelected = [UIImage imageNamed:@"checkbox_selected"];
    self.imgUnselected = [UIImage imageNamed:@"checkbox_unselected"];
    
}

//---------------------------------------------------------------------------------------------------------------

- (void) setSignature :(UIImage *) image {
    START_METHOD
    
    //Sunil
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke method for post signature image on server
        [self postRequestForSaveSignautreImageOnServer:image];
        
    }else{
        
        //Invoke method for save signature in local
        [self setSignatureOfClient:image];
    }
    
    END_METHOD
}

//---------------------------------------------------------------------------------------------------------------

- (void) fetchAllDataToDisplay {
    
    // get all the categories
    
    self.aryCategory = [[DataManager initDB]selectQuestionCategory];
    
    if(self.aryQuestions == nil) {
        _aryQuestions = [[NSMutableArray alloc]init];
    }
    
    if(self.aryAnswers == nil) {
        _aryAnswers = [[NSMutableArray alloc]init];
    }
    
    for (int i = 0; i<[self.aryCategory count]; i++) {
        
        NSString *strCatId = [[self.aryCategory objectAtIndex:i]valueForKey:kCategoryId];
        
        // get all the questions in the category
        NSArray *aryQuest = [[DataManager initDB]selectQuestionForCategory:strCatId];
        [self.aryQuestions addObject:aryQuest];
    }
}

//---------------------------------------------------------------------------------------------------------------

- (UIImage *)getImageIfExistForSignature {
    
    int imgtag = contentTag - 2;
    
    UIImageView *imgVew = (UIImageView *) [self viewWithTag:imgtag];
    UIImage *signImage = imgVew.image;
    
    return signImage;
    
}



//---------------------------------------------------------------------------------------------------------------

-(void)setupLayoutForViews {
    
    contentTag = 1000;
    currentYCoord = 5;
    BOOL  isDataAvailable = NO;
     int l = 0;
    int count = [[DataManager initDB] checkAnswerForClient:strClientId];  // set the client ID

    if (count != 0) {
        isDataAvailable = YES;
        for (int i = 1; i<= [self.aryCategory count]; i++) {
            for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++) {
                NSString * qID = [[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionId];
                [self.aryAnswers addObject:[[DataManager initDB] answerForQuestion:qID:strClientId]];
            }
        }
    }
    
    //========== Start Placing Items as per needed =====================//
   
    
    for (int i = 1; i<= [self.aryCategory count]; i++) {
        
        if(i== 1 || i == 2) {
            
           
            if(self.shouldSetLandscape) {
              lblMainHeader = [[UILabel alloc] initWithFrame:CGRectMake(klblHeaderX + 100,currentYCoord, klblHeaderW, klblHeaderH)];
            } else {
              lblMainHeader = [[UILabel alloc] initWithFrame:CGRectMake(klblHeaderX,currentYCoord, klblHeaderW, klblHeaderH)];
            }
           
            [self setDescriptionLabelColor];
            
            [lblMainHeader setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:16]];
            lblMainHeader.textAlignment = NSTextAlignmentCenter;
            lblMainHeader.tag = contentTag;
            contentTag++;
            
            if (i == 1) {
                lblMainHeader.text = @"PHYSICAL ACTIVITY READINESS QUESTIONNAIRE (PAR-Q)";
                
            }
            else {
                lblMainHeader.text = @"GENERAL & MEDICAL QUESTIONNAIRE";
            }
            
           
            [self addSubview:lblMainHeader];
            
            currentYCoord = currentYCoord + kHQGap + lblMainHeader.frame.size.height;
            viewUnderLine1 = [[UILabel alloc] initWithFrame:CGRectMake(20 ,currentYCoord, 690 , 1)];
            viewUnderLine1.tag = contentTag;
            contentTag++;
            
            [self addSubview:viewUnderLine1];
            
        }
        
        
        
        if(self.shouldSetLandscape) {
            lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(klblHeaderX + 80, currentYCoord, klblHeaderW, klblHeaderH)]; //klblHeaderW
        } else {
            lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(klblHeaderX, currentYCoord, klblHeaderW, klblHeaderH)]; //klblHeaderW
        }
        
      
        lblHeader.text =[[self.aryCategory objectAtIndex:i-1] valueForKey:kCategoryName];
        
        lblHeader.textAlignment = NSTextAlignmentCenter;
        lblHeader.backgroundColor = [UIColor clearColor];
        lblHeader.tag = contentTag;
        contentTag++;
        lblHeader.font = [UIFont fontWithName:kProximaNova_Regular size:17];
        [self addSubview:lblHeader];
        
        
        if(self.shouldSetLandscape) {
           lblYes1= [[UILabel alloc] initWithFrame:CGRectMake(klblYesX + 200, currentYCoord, klblYesW, klblYesH)];
        } else {
           lblYes1= [[UILabel alloc] initWithFrame:CGRectMake(klblYesX, currentYCoord, klblYesW, klblYesH)];
        }
        
        lblYes1.text = @"Yes";
        lblYes1.font = [UIFont fontWithName:kProximaNova_Regular size:17];
        
        lblYes1.backgroundColor = [UIColor clearColor];
        lblYes1.tag = contentTag;
        contentTag++;
        [self addSubview:lblYes1];
        
       
        
        if(self.shouldSetLandscape) {
          lblNo1 = [[UILabel alloc] initWithFrame:CGRectMake(klblNoX + 200, currentYCoord, klblNoW, klblNoH)];
        } else {
          lblNo1 = [[UILabel alloc] initWithFrame:CGRectMake(klblNoX, currentYCoord, klblNoW, klblNoH)];
        }
        
        lblNo1.text = @"No";
        lblNo1.font =[UIFont fontWithName:kProximaNova_Regular size:17];
      
        lblNo1.backgroundColor = [UIColor clearColor];
        lblNo1.tag = contentTag;
        contentTag++;
        [self addSubview:lblNo1];
        
        currentYCoord = currentYCoord + lblHeader.frame.size.height + 3;
        
        
        if(self.shouldSetLandscape) {
             viewUnderLine2 = [[UILabel alloc] initWithFrame:CGRectMake(20 ,currentYCoord,kLandscapeWidth - 145, 1)];
        } else {
             viewUnderLine2 = [[UILabel alloc] initWithFrame:CGRectMake(20 ,currentYCoord, 690 , 1)];
        }
        
       
        viewUnderLine2.tag = contentTag;
        [viewUnderLine2 setBackgroundColor:[UIColor darkGrayColor]];
        contentTag++;
        [self addSubview:viewUnderLine2];
       
         currentYCoord = currentYCoord + viewUnderLine2.frame.size.height + 8;
        
        
        //=========================== Ouestion Section =========================//
        
        for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++,l++) {
            
            NSString * str = [[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestion];
            
            if(self.shouldSetLandscape) {
             lblQuestion1 = [[UILabel alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, klblQuestionW + 150, klblQuestionW)];
                
            } else {
                lblQuestion1 = [[UILabel alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, klblQuestionW, klblQuestionW)];
            }
            
            lblQuestion1.text = [NSString stringWithFormat:@"%@",str];
            lblQuestion1.textAlignment = NSTextAlignmentLeft;
            lblQuestion1.lineBreakMode = NSLineBreakByWordWrapping;
            lblQuestion1.tag = contentTag;
            [lblQuestion1 setFont:[UIFont fontWithName:kProximaNovaAlt_Light size:18]];
            [self setDescriptionLabelColor];
            contentTag++;

            // ========== Try for calculation of expected height  ===========//
            
            NSDictionary *attributes = @{NSFontAttributeName : lblQuestion1.font, NSForegroundColorAttributeName : [UIColor darkGrayColor] };
            CGRect  expectedLabelSize = [str boundingRectWithSize:lblQuestion1.frame.size options:NSStringDrawingUsesFontLeading attributes:attributes context:nil];
            
            CGRect newFrame = lblQuestion1.frame;
            newFrame.size.height = expectedLabelSize.size.height;
            lblQuestion1.frame = newFrame;
            lblQuestion1.numberOfLines = 0;
            [lblQuestion1 sizeToFit];
            
            BOOL flag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionBool] boolValue];
            BOOL TextFlag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionText] boolValue];
            
            // ============= If It is True and false Question =============//
            
            if (flag) {
                UIButton * imgYes = [UIButton buttonWithType:UIButtonTypeCustom];
                if(self.shouldSetLandscape) {
                 imgYes.frame = CGRectMake(klblYesX + 200, currentYCoord, kimgYesW, kimgYesH);
                } else {
                 imgYes.frame = CGRectMake(klblYesX, currentYCoord, kimgYesW, kimgYesH);
                }
                
               [imgYes setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
               [imgYes setSelected:FALSE];
                imgYes.tag = contentTag;
                contentTag++;
                [imgYes addTarget:self action:@selector(imgYesClick:) forControlEvents:UIControlEventTouchUpInside];
                
                UIButton * imgNo = [UIButton buttonWithType:UIButtonTypeCustom];
                if(self.shouldSetLandscape) {
                   imgNo.frame = CGRectMake(klblNoX+ 200, currentYCoord, kimgNoW, kimgNoH);
                } else {
                  imgNo.frame = CGRectMake(klblNoX, currentYCoord, kimgNoW, kimgNoH);
                }
               
                [imgNo setImage:[UIImage imageNamed:@"checkbox_unselected"] forState:UIControlStateNormal];
                [imgNo setSelected:FALSE];
                imgNo.tag = contentTag;
                contentTag++;
                [imgNo addTarget:self action:@selector(imgNoClick:) forControlEvents:UIControlEventTouchUpInside];
                
                if (isDataAvailable) {
                    NSString * str = [[self.aryAnswers objectAtIndex:l] objectForKey:kBoolAnswer];
                    if ([str isEqualToString:@"1"]) {
                        [imgYes setSelected:TRUE];
                        [imgYes setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
                        
                    }
                    else if ([str isEqualToString:@"2"]) {
                        [imgNo setSelected:TRUE];
                        [imgNo setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
                    
                    }
                }
                [self addSubview:imgYes];
                [self addSubview:imgNo];
            }
            
            if (TextFlag) {
                currentYCoord = currentYCoord + lblQuestion1.frame.size.height + kQQGap;
                
                
                
                if(self.shouldSetLandscape) {
                    txtAns1 = [[UITextView alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, klblQuestionW + 150, 70)];
  
                } else {
                    txtAns1 = [[UITextView alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, klblQuestionW, 70)];

                }
                txtAns1.font =[UIFont fontWithName:kProximaNovaAlt_Light size:18];
                [txtAns1 setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
                txtAns1.tag = contentTag;
                txtAns1.delegate = (id)self;
                contentTag++;
                
                UIImageView  * imgTextBg;
                if(self.shouldSetLandscape) {
                    imgTextBg = [[UIImageView alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, klblQuestionW + 150, 70)];
                } else {
                     imgTextBg = [[UIImageView alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, klblQuestionW, 70)];
                }
              
                imgTextBg.tag = contentTag;
                contentTag++;
                imgTextBg.backgroundColor = [UIColor lightGrayColor];
                currentYCoord = currentYCoord + kQQGap + 80;
                [self addSubview:imgTextBg];
                [self addSubview:txtAns1];
                if (isDataAvailable) {
                    txtAns1.text = [[self.aryAnswers objectAtIndex:l] objectForKey:kTextAnswer];
                }
                else {
                    txtAns1.text = @"";
                }
            } else {
                currentYCoord = currentYCoord + kQQGap + lblQuestion1.frame.size.height;
            }
            [self addSubview:lblQuestion1];
        }
    }
    
    
    currentYCoord = currentYCoord +2;
    
    if(self.shouldSetLandscape) {
       lblDescr1 = [[UILabel alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, 656 + 100, 133)];
    } else {
       lblDescr1 = [[UILabel alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord, 656, 133)];
    }
    
   
    lblDescr1.text = @"If you have answered “Yes” to one or more of the above questions, consult your physician before engaging in physical activity. Tell your physician which questions you answered “Yes” to. After a medical evaluation, seek advice from your physician on what type of activity is suitable for your current condition.";
    
    [lblDescr1 setFont:[UIFont fontWithName:kProximaNovaAlt_Light size:18]];
    lblDescr1.lineBreakMode = NSLineBreakByTruncatingTail;
    lblDescr1.numberOfLines = 13;
    lblDescr1.backgroundColor = [UIColor clearColor];
    lblDescr1.tag = contentTag;
    contentTag++;
    [self addSubview:lblDescr1];
    currentYCoord = currentYCoord + 133;

    NSString * imgPath = [NSString stringWithFormat:@"%@/ClientsSignature/Signature%@.png",[FileUtility basePath],self.strClientId];
    
    //Invoke method for get Signature from server
    [self getClientSignatureImage:strClientId];
    
    
    MESSAGE(@"ientsSignature/Signatur-> imgPath:%@",imgPath);
    
    UIImage * img = [UIImage imageWithContentsOfFile:imgPath];
    if(([self.strClientId isEqualToString:@"0"])) {
        img = nil;
    }
    
    UIImageView * signView;
    
    if(self.shouldSetLandscape) {
     signView = [[UIImageView alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord +75, 345+50, 60)];
    } else {
     signView = [[UIImageView alloc] initWithFrame:CGRectMake(klblQuestionX, currentYCoord +75, 345, 60)];
    }
    
    [signView setImage:[UIImage imageNamed:@"Signature"]];
    signView.backgroundColor = [UIColor clearColor];
    signView.tag = contentTag;
    contentTag++;
    [self addSubview:signView];
    
    UIImageView * signView2;
    
    if(self.shouldSetLandscape) {
        signView2 = [[UIImageView alloc] initWithFrame:CGRectMake(klblQuestionX+ 120, currentYCoord+76, 220+50, 50)];
    } else {
       signView2 = [[UIImageView alloc] initWithFrame:CGRectMake(klblQuestionX+ 120, currentYCoord+76, 220, 50)];
    }
    
   
    if (img != nil) {
        [signView2 setImage:img];
    } else {
        [signView2 setImage:nil];
    }
    signView2.backgroundColor = [UIColor clearColor];
    signView2.tag = contentTag;
    [self addSubview:signView2];
    contentTag ++;
    
    // ---- Signature view --------------//
    
    UIButton * btnSignatureView = [UIButton buttonWithType:UIButtonTypeCustom];
    if(self.shouldSetLandscape) {
      btnSignatureView.frame = CGRectMake(klblQuestionX+120, currentYCoord +76, 220+ 50, 50);
    } else {
      btnSignatureView.frame = CGRectMake(klblQuestionX+120, currentYCoord +76, 220, 50);
    }
    
    btnSignatureView.tag = contentTag;
    btnSignatureView.backgroundColor = [UIColor clearColor];
    [btnSignatureView addTarget:self action:@selector(openSignatureView:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnSignatureView];
    contentTag++;
    
    
    if(self.shouldSetLandscape) {
     signHere1 = [[UILabel alloc]initWithFrame:CGRectMake(klblQuestionX+10, currentYCoord +68 , 100, 30)];
    } else {
     signHere1 = [[UILabel alloc]initWithFrame:CGRectMake(klblQuestionX+10, currentYCoord +68 , 100, 30)];
    }
   
    [signHere1 setBackgroundColor:[UIColor clearColor]];
    [signHere1 setText:NSLocalizedString(@"Sign Here", nil)];
    [signHere1 setTag:18000];
    [signHere1 setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:13]];
    [self addSubview:signHere1];

    
    btnPdfLink1 = [UIButton buttonWithType:UIButtonTypeCustom];
     [self setDescriptionLabelColor];
    if(self.shouldSetLandscape) {
        btnPdfLink1.frame = CGRectMake(klblQuestionX, currentYCoord+5, 152, 35);
   
    } else {
        btnPdfLink1.frame = CGRectMake(klblQuestionX, currentYCoord+5, 152, 35);
    }
    
    [btnPdfLink1 setTitle:NSLocalizedString(@"Liability Waiver", nil) forState:UIControlStateNormal];
    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(@"Liability Waiver", nil) font:btnPdfLink1.titleLabel.font height:35];
    
    btnPdfLink1.frame = CGRectMake(klblQuestionX, currentYCoord+5,rect.size.width + 30, 35);
    [btnPdfLink1 setBackgroundImage:[UIImage imageNamed:@"buttonNewNote"] forState:UIControlStateNormal];
    [btnPdfLink1 setBackgroundColor:[UIColor clearColor]];
    [btnPdfLink1.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [btnPdfLink1 addTarget:self action:@selector(openPdf) forControlEvents:UIControlEventTouchUpInside];
    [btnPdfLink1 setHidden:FALSE];
    [btnPdfLink1 setTag:18001];
    [self addSubview:btnPdfLink1];
    
    
    btnSaveParQ1 = [UIButton buttonWithType:UIButtonTypeCustom];
     [self setDescriptionLabelColor];
    CGRect rectOfbtnSaveParQ = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(@"Save Par-Q", nil) font:btnSaveParQ1.titleLabel.font height:30];
    
    if(self.shouldSetLandscape) {
     btnSaveParQ1.frame = CGRectMake(750, currentYCoord+100, rectOfbtnSaveParQ.size.width + 10, 30);
    } else {
      btnSaveParQ1.frame = CGRectMake(580, currentYCoord+100, rectOfbtnSaveParQ.size.width + 10, 30);
    }
    
    [btnSaveParQ1 setTitle:NSLocalizedString(@"Save Par-Q", nil) forState:UIControlStateNormal];
    [btnSaveParQ1 setBackgroundColor:kSelectedBtnBGColor];
    [btnSaveParQ1.titleLabel setFont:[UIFont fontWithName:krade_Gothic_LT_Bold_Condensed size:15]];
    [btnSaveParQ1.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [btnSaveParQ1 addTarget:self action:@selector(saveParQData) forControlEvents:UIControlEventTouchUpInside];
    [btnSaveParQ1 setHidden:FALSE];
    [btnSaveParQ1 setTag:18002];
    [self addSubview:btnSaveParQ1];

    currentYCoord = currentYCoord + 250;
    
    

}


//-----------------------------------------------------------------------

#pragma Mark : Orientation Methods 


//-----------------------------------------------------------------------

-(int) setLandscapeLayout {
    contentTag = 1000;
    currentYCoord = 5;
    [self setFrame:CGRectMake(0, self.frame.origin.y, kLandscapeWidth, 2500)];
    for (int i = 1; i<= [self.aryCategory count]; i++) {
        if (i==1 || i==2) {
            
            lblMainHeader1 = (UILabel *) [self viewWithTag:contentTag];
            lblMainHeader1.frame = CGRectMake(klblHeaderX + 100,currentYCoord, klblHeaderW, klblHeaderH);
            contentTag++;
            
            currentYCoord = currentYCoord + kHQGap + lblMainHeader1.frame.size.height;
            UIImageView * viewLine = (UIImageView *) [self viewWithTag:contentTag];
            viewLine.frame = CGRectMake(20 ,currentYCoord, 690 , 1);
            contentTag++;
        }

        lblHeader2 = (UILabel *) [self viewWithTag:contentTag];
        lblHeader2.frame = CGRectMake(klblHeaderX + 80, currentYCoord, klblHeaderW, klblHeaderH);
        contentTag++;
        
       lblYes2  = (UILabel *) [self viewWithTag:contentTag];
        lblYes2.frame = CGRectMake(klblYesX + 200, currentYCoord, klblYesW, klblYesH);
        contentTag++;
        
        lblNo2 = (UILabel *) [self viewWithTag:contentTag];
        lblNo2.frame = CGRectMake(klblNoX + 200, currentYCoord, klblNoW, klblNoH);
        contentTag++;
        currentYCoord = currentYCoord + lblHeader2.frame.size.height - 5;
        
        UIImageView * viewLine2 = (UIImageView *) [self viewWithTag:contentTag];
        viewLine2.frame = CGRectMake(20 ,currentYCoord,kLandscapeWidth - 145, 1);
        currentYCoord = currentYCoord + kHQGap + 5;
        contentTag++;
        
        for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++) {
            
            lblQuestion2 = (UILabel *) [self viewWithTag:contentTag];
            lblQuestion2.frame =CGRectMake(klblQuestionX, currentYCoord, klblQuestionW + 150, klblQuestionW);
            NSString * str = lblQuestion2.text;
            
            NSDictionary *attributes = @{NSFontAttributeName : lblQuestion2.font, NSForegroundColorAttributeName : [UIColor darkGrayColor] };
            CGRect  expectedLabelSize = [str boundingRectWithSize:lblQuestion2.frame.size options:NSStringDrawingUsesFontLeading attributes:attributes context:nil];

            
            CGRect newFrame = lblQuestion2.frame;
            newFrame.size.height = expectedLabelSize.size.height;
            lblQuestion2.frame = newFrame;
            lblQuestion2.numberOfLines = 0;
            lblQuestion2.backgroundColor = [UIColor clearColor];
            [lblQuestion2 sizeToFit];
            contentTag++;
            BOOL flag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionBool] boolValue];
            BOOL TextFlag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionText] boolValue];
            if (flag) {
                UIButton * imgYes = (UIButton *) [self viewWithTag:contentTag];
                imgYes.frame = CGRectMake(klblYesX + 200, currentYCoord, kimgYesW, kimgYesH);
                contentTag++;
                
                UIButton * imgNo = (UIButton *) [self viewWithTag:contentTag];
                imgNo.frame = CGRectMake(klblNoX+ 200, currentYCoord, kimgNoW, kimgNoH);
                contentTag++;
            }
            if (TextFlag) {
                currentYCoord = currentYCoord + lblQuestion2.frame.size.height + kQQGap;
                
                UITextView * txtAns = (UITextView *) [self viewWithTag:contentTag];
                txtAns.frame = CGRectMake(klblQuestionX, currentYCoord, klblQuestionW + 150, 70);
                contentTag++;
                
                UIImageView  * imgTextBg = (UIImageView *) [self viewWithTag:contentTag];
                imgTextBg.frame = CGRectMake(klblQuestionX, currentYCoord, klblQuestionW + 150, 70);
                currentYCoord = currentYCoord + kQQGap + 80;
                contentTag++;
            }
            else {
                currentYCoord = currentYCoord + kQQGap + lblQuestion2.frame.size.height;
            }
        }
    }
    currentYCoord = currentYCoord+10;
    lblDescr2 = (UILabel *) [self viewWithTag:contentTag];
    lblDescr2.frame = CGRectMake(klblQuestionX, currentYCoord, 656 + 100, 133);
    contentTag++;
    currentYCoord = currentYCoord + 130;
    
    UIImageView * signView = (UIImageView *) [self viewWithTag:contentTag];
    signView.frame = CGRectMake(klblQuestionX, currentYCoord +75, 345+50, 60);
    contentTag++;
    
    UIImageView * signView2 = (UIImageView *) [self viewWithTag:contentTag];
    signView2.frame = CGRectMake(klblQuestionX+ 135, currentYCoord+76, 220+35, 50);
    contentTag++;
    
    UIButton * btnSignatureView = (UIButton *) [self viewWithTag:contentTag];
    btnSignatureView.frame = CGRectMake(klblQuestionX+120, currentYCoord +76, 220+ 50, 50);
    contentTag++;
    
    
    
    // Chenge default tags
    //=================
    
    signHere2 = (UILabel *)[self viewWithTag:18000];
    [signHere2 setFrame:CGRectMake(klblQuestionX+10, currentYCoord +68 , 100, 30)];
    
    
    UIButton * btnPdfLink = (UIButton *)[self viewWithTag:18001];
    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(@"Liability Waiver", nil) font:btnPdfLink.titleLabel.font height:35];
    
    btnPdfLink.frame = CGRectMake(klblQuestionX, currentYCoord+5,rect.size.width + 30, 35);

    btnSaveParQ2 =  (UIButton *)[self viewWithTag:18002];
    
     [self setDescriptionLabelColor];
    CGRect rectOfbtnSaveParQ = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(@"Save Par-Q", nil) font:btnSaveParQ2.titleLabel.font height:30];

    [btnSaveParQ2 setFrame:CGRectMake(self.frame.size.width - (rectOfbtnSaveParQ.size.width + 120) , currentYCoord+100, rectOfbtnSaveParQ.size.width + 10, 30)];
    currentYCoord = currentYCoord + 250;
 
    
    return currentYCoord;
}

//-----------------------------------------------------------------------

-(int)setPotraitLayout {
    contentTag = 1000;
    currentYCoord = 0;
     [self setFrame:CGRectMake(0, self.frame.origin.y, kPotraitWidth, 2500)];
    for (int i = 1; i<= [self.aryCategory count]; i++) {
        if (i==1 || i==2) {
            
            lblMainHeader1 = (UILabel *) [self viewWithTag:contentTag];
            lblMainHeader1.frame = CGRectMake(klblHeaderX,currentYCoord, klblHeaderW, klblHeaderH);
            contentTag++;
            
            currentYCoord = currentYCoord + kHQGap + lblMainHeader1.frame.size.height;
            
            UIImageView * viewLine = (UIImageView *) [self viewWithTag:contentTag];
            viewLine.frame = CGRectMake(20 ,currentYCoord, 690 , 1);
            contentTag++;
        }
        
         lblHeader2 = (UILabel *) [self viewWithTag:contentTag];
        lblHeader2.frame = CGRectMake(klblHeaderX, currentYCoord, klblHeaderW, klblHeaderH);
        contentTag++;
        
        lblYes2 = (UILabel *) [self viewWithTag:contentTag];
        lblYes2.frame = CGRectMake(klblYesX , currentYCoord, klblYesW, klblYesH);
        contentTag++;
        
        lblNo2 = (UILabel *) [self viewWithTag:contentTag];
        lblNo2.frame = CGRectMake(klblNoX , currentYCoord, klblNoW, klblNoH);
        contentTag++;
        currentYCoord = currentYCoord + lblHeader2.frame.size.height - 5;
        
        UIImageView * viewLine2 = (UIImageView *) [self viewWithTag:contentTag];
        viewLine2.frame = CGRectMake(20 ,currentYCoord,690 , 1);
        currentYCoord = currentYCoord + kHQGap + 5;
        contentTag++;
        
        for (int j = 1; j<=[[self.aryQuestions objectAtIndex:i-1] count]; j++) {
            
            lblQuestion3 = (UILabel *) [self viewWithTag:contentTag];
            
            lblQuestion3.frame =CGRectMake(klblQuestionX, currentYCoord, klblQuestionW, klblQuestionW);
            NSString * str = lblQuestion3.text;
            
            NSDictionary *attributes = @{NSFontAttributeName : lblQuestion3.font, NSForegroundColorAttributeName : [UIColor darkGrayColor] };
            CGRect  expectedLabelSize = [str boundingRectWithSize:lblQuestion3.frame.size options:NSStringDrawingUsesFontLeading attributes:attributes context:nil];
            
            
            CGRect newFrame = lblQuestion3.frame;
            newFrame.size.height = expectedLabelSize.size.height;
            lblQuestion3.frame = newFrame;
            lblQuestion3.numberOfLines = 0;
            lblQuestion3.backgroundColor = [UIColor clearColor];
            [lblQuestion3 sizeToFit];
            contentTag++;
            BOOL flag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionBool] boolValue];
            BOOL TextFlag = [[[[self.aryQuestions objectAtIndex:i-1] objectAtIndex:j-1] valueForKey:kQuestionText] boolValue];
            if (flag) {
                imgYes3 = (UIButton *) [self viewWithTag:contentTag];
                imgYes3.frame = CGRectMake(klblYesX , currentYCoord, kimgYesW, kimgYesH);
                contentTag++;
                
                imgNo3 = (UIButton *) [self viewWithTag:contentTag];
                imgNo3.frame = CGRectMake(klblNoX, currentYCoord, kimgNoW, kimgNoH);
                contentTag++;
            }
            if (TextFlag) {
                currentYCoord = currentYCoord + lblQuestion3.frame.size.height + kQQGap;
                
                UITextView * txtAns = (UITextView *) [self viewWithTag:contentTag];
                txtAns.frame = CGRectMake(klblQuestionX, currentYCoord, klblQuestionW, 70);
                contentTag++;
                
                UIImageView  * imgTextBg = (UIImageView *) [self viewWithTag:contentTag];
                imgTextBg.frame = CGRectMake(klblQuestionX, currentYCoord, klblQuestionW, 70);
                currentYCoord = currentYCoord + kQQGap + 80;
                contentTag++;
            }
            else {
                currentYCoord = currentYCoord + kQQGap + lblQuestion3.frame.size.height;
            }
        }
    }
    currentYCoord = currentYCoord+2;
    lblDescr2 = (UILabel *) [self viewWithTag:contentTag];
    lblDescr2.frame = CGRectMake(klblQuestionX, currentYCoord, 656, 133);
    contentTag++;
    currentYCoord = currentYCoord + 133;
    
    UIImageView * signView = (UIImageView *) [self viewWithTag:contentTag];
    signView.frame = CGRectMake(klblQuestionX, currentYCoord +75, 345, 60);
    contentTag++;
    
    UIImageView * signView2 = (UIImageView *) [self viewWithTag:contentTag];
    signView2.frame = CGRectMake(klblQuestionX+ 120, currentYCoord+76, 220, 50);
    contentTag++;
    
    UIButton * btnSignatureView = (UIButton *) [self viewWithTag:contentTag];
    btnSignatureView.frame = CGRectMake(klblQuestionX+120, currentYCoord +76, 220, 50);
    contentTag++;

    
    // Chenge default tags
    //=================
    
    signHere2 = (UILabel *)[self viewWithTag:18000];
    [signHere2 setFrame:CGRectMake(klblQuestionX+10, currentYCoord +68 , 100, 30)];
    
    btnPdfLink2 = (UIButton *)[self viewWithTag:18001];
    CGRect rect = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(@"Liability Waiver", nil) font:btnPdfLink2.titleLabel.font height:35];
    
    btnPdfLink2.frame = CGRectMake(klblQuestionX, currentYCoord+5,rect.size.width + 30, 35);
    
    btnSaveParQ1 =  (UIButton *)[self viewWithTag:18002];
    [self setDescriptionLabelColor];
    CGRect rectOfbtnSaveParQ = [[AppDelegate sharedInstance] getWidth:NSLocalizedString(@"Save Par-Q", nil) font:btnSaveParQ1.titleLabel.font height:30];

    [btnSaveParQ1 setFrame:CGRectMake(self.frame.size.width - (rectOfbtnSaveParQ.size.width + 80) , currentYCoord+100, rectOfbtnSaveParQ.size.width + 10, 30)];
    currentYCoord = currentYCoord + 250;

    return currentYCoord;
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark : textview delegate Methods 

//---------------------------------------------------------------------------------------------------------------
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    [self.parqQuestDelegate textfieldBeginWithFrame:textView.frame];
    return YES;
}

//---------------------------------------------------------------------------------------------------------------

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if (![AppDelegate sharedInstance].isAutoSave) {
        [AppDelegate sharedInstance].isFieldUpdated = YES;
    }
    else {
        oldValue = 1;
    }
    
    [self.parqQuestDelegate textfieldBeginWithFrame:textView.frame];
    return YES;
    
    
    
}

//---------------------------------------------------------------------------------------------------------------

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [self.parqQuestDelegate textfieldEndWithFrame:textView.frame];
    if (oldValue == 1) {
        oldValue = 0;
        //Commented by SUNIL
//        [self autoSaveContents:(int)textView.tag];
    }
    
    return YES;
}

//Sunil
-(void)setDescriptionLabelColor{
    START_METHOD
    int skin = [commonUtility retrieveValue:KEY_SKIN].intValue;
    
    lblYes2.textColor   = [UIColor whiteColor];
    lblNo2.textColor    = [UIColor whiteColor];
    
    switch (skin) {
        case FIRST_TYPE:
            lblQuestion1.textColor = [UIColor whiteColor];
            lblDescr1.textColor = [UIColor whiteColor];
            signHere1.textColor = [UIColor whiteColor];
            
            [btnPdfLink1 setBackgroundImage:[UIImage imageNamed:@"green_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [btnSaveParQ1 setBackgroundColor:kFirstSkin];
            [btnPdfLink1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnSaveParQ1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
             [btnSaveParQ2 setBackgroundColor:kFirstSkin];
             [btnSaveParQ2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            lblHeader.textColor = [UIColor whiteColor];
            lblYes1.textColor   = [UIColor whiteColor];
            lblNo1.textColor    = [UIColor whiteColor];
            lblYes2.textColor   = [UIColor whiteColor];
            lblNo2.textColor    = [UIColor whiteColor];
            lblMainHeader.textColor = [UIColor whiteColor];
            
            break;
        case SECOND_TYPE:
            lblQuestion1.textColor = [UIColor whiteColor];
            lblDescr1.textColor = [UIColor whiteColor];
            signHere1.textColor = [UIColor whiteColor];
            
            [btnPdfLink1 setBackgroundImage:[UIImage imageNamed:@"red_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [btnSaveParQ1 setBackgroundColor:ksecondSkin];
            [btnPdfLink1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnSaveParQ1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btnSaveParQ2 setBackgroundColor:ksecondSkin];
            [btnSaveParQ2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            lblHeader.textColor = [UIColor whiteColor];
            lblYes1.textColor = [UIColor whiteColor];
            lblNo1.textColor    = [UIColor whiteColor];
            lblYes2.textColor   = [UIColor whiteColor];
            lblNo2.textColor    = [UIColor whiteColor];
            lblMainHeader.textColor = [UIColor whiteColor];
            
            
            
            break;
        case THIRD_TYPE:
            lblQuestion1.textColor = [UIColor darkGrayColor];
            lblDescr1.textColor = [UIColor darkGrayColor];
            signHere1.textColor = [UIColor darkGrayColor];
            
            [btnPdfLink1 setBackgroundImage:[UIImage imageNamed:@"pink_add_new_line_button.png"] forState:UIControlStateNormal];
            
            [btnSaveParQ1 setBackgroundColor:kThirdSkin];
            [btnPdfLink1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnSaveParQ1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btnSaveParQ2 setBackgroundColor:kThirdSkin];
            [btnSaveParQ2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            lblHeader.textColor = [UIColor blackColor];
            lblYes1.textColor = [UIColor blackColor];
            lblNo1.textColor    = [UIColor blackColor];
            lblYes2.textColor   = [UIColor blackColor];
            lblNo2.textColor    = [UIColor blackColor];
            lblMainHeader.textColor = [UIColor blackColor];
            break;
            
        default:
            lblHeader.textColor = [UIColor darkGrayColor];
            lblYes1.textColor = [UIColor darkGrayColor];
            lblNo1.textColor    = [UIColor darkGrayColor];
            lblYes2.textColor   = [UIColor darkGrayColor];
            lblNo2.textColor    = [UIColor darkGrayColor];
            lblMainHeader.textColor = [UIColor darkGrayColor];
            break;
    }
    
    END_METHOD
}

//---------------------------------------------------------------------------------------------------------------
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


//TODO: post Request For Save Signautre Image On Server

-(void)postRequestForSaveSignautreImageOnServer :(UIImage *) clientSignatureImage{
    START_METHOD
    
    
    //Craete Dictionry
    NSMutableDictionary *objDict    =   [[NSMutableDictionary alloc]init];
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN                     forKey:@"access_key"];
    [objDict setValue:@"signature_image"                    forKey:ACTION];
    [objDict setValue:self.strClientId                      forKey:kClientId];
    
    
    //Create the image data for send to post request to ssave the image i=on server
    NSData  *imageData  =   UIImageJPEGRepresentation(clientSignatureImage, 1.0f);
    
    //take var for file name
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"Signature%@.jpeg",self .strClientId];
        
    }else{
        strFileName     =   @"";
    }
    MESSAGE(@"postClientsSignatureOnServer: %@ andimageData: %@",strFileName,imageData);
    
    
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    
    //Post request when if here is  image data
    if(imageData !=nil){
        
        //Show indicator
        [TheAppController showHUDonView:nil];
        
        
    //Craete object of service handeler
    ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
    
    //Invoke method for post images on server
    [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                 success:^(NSDictionary *responseDataDictionary) {
                                     
                                     //Hide Indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"postClientsSignatureOnServer success dictionary: %@",responseDataDictionary);
                                     
                                     
                                     //Invoke method for save signature in local
                                     [self setSignatureOfClient:clientSignatureImage];
                                     
                                     
                                 } failure:^(NSError *error) {
                                     MESSAGE(@"Error");
                                     
                                     
                                     //Hide Indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     
                                 }];
    //If not image data then retur no actiobn
    }else{
    
        
        return;
    }
    
    
    
    END_METHOD
}


//Method for save signature in local
-(void)setSignatureOfClient :(UIImage *) image{
    START_METHOD
    
    int tag = contentTag - 2;
    UIImageView * signView2 = (UIImageView *) [self viewWithTag:tag];
    signView2.image = nil;
    [signView2 setImage:image];
    
    if ([AppDelegate sharedInstance].isAutoSave && ![self.strClientId isEqualToString:@"0"]) {
        UIImageView * signView2 = (UIImageView *) [self viewWithTag:tag];
        UIImage * img = [signView2 image];
        
        [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsSignature",[FileUtility basePath]]];
        NSString * imgNm = [NSString stringWithFormat:@"Signature%@",self.strClientId];
        NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
        [FileUtility createFileInFolder:@"ClientsSignature" withName:imgNm withData:imageData];
    } else {
        if (![self.strClientId isEqualToString:@"0"]) {
        }
    }
    END_METHOD
}



//TODO: GEt client Profile image  from server
-(void)getClientSignatureImage:(NSString *)strClientId{
    START_METHOD
    
    //No need to show indicator, it will download in background
    
    
    
    
    END_METHOD
}

@end
