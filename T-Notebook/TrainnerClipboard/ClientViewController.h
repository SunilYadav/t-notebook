//
//  ClientViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotesViewController.h"
#import "ProgramNameViewController.h"
#import "ViewAllProgramVC.h"
#import "ParQController.h"
#import "OptionsViewVC.h"
#import "ProgramViewController.h"
#import "AssessmentViewController.h"
#import  <MessageUI/MessageUI.h>

@interface ClientViewController : BaseViewController <AddNewProgramSheetDelegate,PARQViewControllerViewDelegate,OptionsDelegate,AssessmentDelegate,viewallDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>


@property (strong, nonatomic) NSString                      *strClientId;
@property (strong, nonatomic) NSString                      *strClientName;
@property (strong, nonatomic) NSString                      *strClientEmail;
@property (nonatomic)  BOOL                                 isFromClientTable;
@property (nonatomic , strong) NSString                     *selectedScreen;
@property (strong, nonatomic) IBOutlet UIView               *backgroundAddView;
@property (strong, nonatomic) IBOutlet UIView               *tabView;
@property (nonatomic)   BOOL                                    isDefaultVcChange;
@property (nonatomic) BOOL                                      isDataSavePending;
@property (nonatomic) BOOL                                      isDataToBeSaved;
@property (nonatomic) BOOL                                      isDataToBeEntered;
@property (nonatomic) BOOL                                      isOtherOptionsSelected;

+ (id)sharedInstance;
-(void)setUpLandscapeLayout;



@end
