//
//  ProgramViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatePickerController.h"
#import "ProgramGetDetailVC.h"
#import "ProgramGetWorkoutVC.h"
#import "ProgramNameViewController.h"
#import "UpdateNotesVC.h"
#import "FileUtility.h"
#import "PopoverController.h"
#import "OptionsViewVC.h"
#import  <MessageUI/MessageUI.h>
#import "TemplateSheetNameController.h"


@protocol viewallDelegate<NSObject>
-(void)viewAllDelegate:(NSString*)clientID;
-(void)openMail:(NSString*)mailID;
-(void)hideOptionsViewOnTap;
-(void)openMailForScreenShotImage:(UIImage*)image;
-(void)openFbForScreenShotImage:(UIImage *)image;
-(void)removePopOverForAddProgram;
-(void)updateLastEditedProgramForClient:(NSString *)strClientId;
@end

@interface ProgramViewController : BaseViewController<DatePickerDelegate,UIPopoverControllerDelegate,NewExerciseDelegate,AddNewWorkoutSheetDelegate,AddNewProgramSheetDelegate,updateNoteDelegate,popoverDelegate,OptionsDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate,ReloadSheetsDelegate>
{
    UIButton *selectedBtn;
}
@property (nonatomic, weak) id<viewallDelegate>		              delegate;
@property (nonatomic, strong) NSString				                    * selectedSheet;
@property (nonatomic, strong) NSString				                    * strSelectedSheet;


@property (nonatomic) NSInteger                                          No_OfSheet;
@property (nonatomic) NSInteger                                          blockSheetNo;
@property (nonatomic, strong) NSString				                    * strClientId;
@property (nonatomic, strong) NSString				                    * strClientName;
@property (nonatomic, strong) NSString                                * strProgramSheetName;
@property (strong,nonatomic) NSDictionary                            *detailsExercise;
@property BOOL isBackTap;
@property (nonatomic) NSInteger                                                    currentSheetId;
@property (weak, nonatomic) IBOutlet UITableView             *tableview;
@property (weak, nonatomic)     IBOutlet    UIButton           *btnAddNewLine;
@property (weak, nonatomic)     IBOutlet    UIButton           *btnAddWorkout;
@property (weak, nonatomic)     IBOutlet    UIView             *programView;
@property (weak, nonatomic) IBOutlet UIView                   *viewWithTbl;
@property BOOL isViewAllVisible;
@property (nonatomic) NSInteger                                 currentSheet;
@property (nonatomic) NSInteger                                 currentBlockNo;

@property (nonatomic) BOOL                                                          setAsLandscape;
-(void)setUpLandscapeOrientation;
-(void)setupPortraitOrientation;

+ (id)sharedInstance;
-(void)setLabelValue :(NSString *)ProgramName;
-(void)deleteSheet;
-(void)takeScreenShot;
- (void)takeScreenShotAndShareToFB;


@end
