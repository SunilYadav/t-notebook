//
//  ProgramNameViewController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "ProgramNameViewController.h"
#import "ProgramViewController.h"
#import "ClientViewController.h"
#import "ParQController.h"
#import "KeyboardControls.h"

#define kSelectedBtnBGColor [UIColor colorWithRed:72.0/255.0 green:197.0/255.0 blue:184.0/255.0 alpha:1.0]
#define kNotSelectedBtnBGColor [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1.0]
@interface ProgramNameViewController ()<UITextFieldDelegate>
{
    __weak IBOutlet UIButton *btnAddPrograme;
    ProgramViewController * ProgramVCOBJ;
    ClientViewController *clientVCOBJ;

}
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtProgramPurpose;
@property (strong, nonatomic) IBOutlet UITextField *txtWorkoutName;

@property (strong, nonatomic)  UITextField *activeTxt;
@property (weak, nonatomic) IBOutlet UILabel *lblproName;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPurpose;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkoutName;
@property (nonatomic, strong) KeyboardControls      *keyboardControls;
@end

@implementation ProgramNameViewController

//-----------------------------------------------------------------------

#pragma mark - TextField Delegate

//-----------------------------------------------------------------------

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeTxt = textField;
    [self.keyboardControls setActiveField:textField];
    
}

//-----------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    
    NSRange lowercaseCharRange;
    lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound && textField.text.length==0 ) {
        
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}
//-----------------------------------------------------------------------

#pragma mark - Custom Methods

//-----------------------------------------------------------------------

- (void) localizedControls {
    
    self.lblproName.text = NSLocalizedString(self.lblproName.text, nil);
    self.lblProgramPurpose.text = NSLocalizedString(self.lblProgramPurpose.text, nil);
    self.lblWorkoutName.text = NSLocalizedString(self.lblWorkoutName.text, nil);
    [btnAddPrograme setTitle:NSLocalizedString(btnAddPrograme.titleLabel.text,nil) forState:0];
    
    CGRect rect = [[AppDelegate sharedInstance] getWidth:self.lblproName.text font:self.lblproName.font height:21];
    CGRect rect1 = [[AppDelegate sharedInstance] getWidth:self.lblProgramPurpose.text font:self.lblProgramPurpose.font height:21];
    CGRect rect2 = [[AppDelegate sharedInstance] getWidth:self.lblWorkoutName.text font:self.lblWorkoutName.font height:21];
    
    CGFloat width = rect.size.width;
    if (rect1.size.width > width) {
        width = rect1.size.width;
    }
    if (rect2.size.width > width) {
        width = rect2.size.width;
    }
    
    if(width > 100) {
        [self.lblproName setFrame:CGRectMake(self.lblproName.frame.origin.x - 25, self.lblproName.frame.origin.y, width + 5, self.lblproName.frame.size.height)];
        [self.lblProgramPurpose setFrame:CGRectMake(self.lblProgramPurpose.frame.origin.x - 25, self.lblProgramPurpose.frame.origin.y, width + 5, self.lblProgramPurpose.frame.size.height)];
        [self.lblWorkoutName setFrame:CGRectMake(self.lblWorkoutName.frame.origin.x - 25, self.lblWorkoutName.frame.origin.y, width + 5, self.lblWorkoutName.frame.size.height)];
    }
}


//-----------------------------------------------------------------------

-(void)NavBarBtnTap :(UIBarButtonItem *)sender {
    
    if ([sender tag] == 201) {
        // back button
        
        [[self parentViewController]dismissViewControllerAnimated:YES completion:^{
            self.txtName.text = nil;
            self.txtProgramPurpose.text = nil;
            self.txtWorkoutName.text = nil;
        }];
         
        if (self.delegate && [self.delegate respondsToSelector:@selector(cancelProgramAndGoToParq)]) {
            [self.delegate cancelProgramAndGoToParq];
        }

        
    }else if ([sender tag] == 202) {
        // done button
        [self.activeTxt resignFirstResponder];
        if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
        {
        
        if (self.txtName.text.length == 0 || self.txtProgramPurpose.text.length == 0 || self.txtWorkoutName.text.length == 0 ) {
            DisplayAlertWithTitle(NSLocalizedString(@"Please enter all vlaues.", nil), kAppName)
        }
        else{
                [_delegate addNewProgram:self.txtName.text :self.txtProgramPurpose.text :self.txtWorkoutName.text];
           
            [[self parentViewController]dismissViewControllerAnimated:YES completion:^{
                self.txtName.text = nil;
                self.txtProgramPurpose.text = nil;
                self.txtWorkoutName.text = nil;
            }];
        }
        }
        
        else
        {
              [_delegate addNewProgram:self.txtName.text :@"isForDelete" :self.txtWorkoutName.text];
            [[self parentViewController]dismissViewControllerAnimated:YES completion:^{
                self.txtName.text = nil;
                self.txtProgramPurpose.text = nil;
                self.txtWorkoutName.text = nil;
            }];
        }
        
    }
}

//-----------------------------------------------------------------------

- (IBAction)btnAddprogrameClicked:(id)sender {
    
    START_METHOD
    [self.activeTxt resignFirstResponder];
    if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
    {
        
        if (self.txtName.text.length == 0 || self.txtProgramPurpose.text.length == 0 || self.txtWorkoutName.text.length == 0 ) {
            DisplayAlertWithTitle(NSLocalizedString(@"Please enter all vlaues.", nil), kAppName)
        }
        else{
            [_delegate addNewProgram:self.txtName.text :self.txtProgramPurpose.text :self.txtWorkoutName.text];
        }
    }
    
    else
    {
        [_delegate addNewProgram:self.txtName.text :@"isForDelete" :self.txtWorkoutName.text];
        [[self parentViewController]dismissViewControllerAnimated:YES completion:^{
            self.txtName.text = nil;
            self.txtProgramPurpose.text = nil;
            self.txtWorkoutName.text = nil;
        }];
        
    }


}
//-----------------------------------------------------------------------

#pragma mark - Keyboard Controls Delegate Methods

//-----------------------------------------------------------------------

- (void)keyboardControlsDonePressed:(KeyboardControls *)keyboardControls{
    [keyboardControls.activeField resignFirstResponder];
}
//-----------------------------------------------------------------------

- (void)keyboardControls:(KeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(KeyboardControlsDirection)direction{
    
}
//-----------------------------------------------------------------------

#pragma mark - Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self localizedControls];

    // Do any additional setup after loading the view.
    
    btnAddPrograme.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnAddPrograme.layer.cornerRadius = 4.0;
    btnAddPrograme.backgroundColor = kAppTintColor;
    self.txtProgramPurpose.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtWorkoutName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    [self addNavBarButtons];
    self.preferredContentSize = CGSizeMake(500, 250);

    
    UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
    lblTitle.text = NSLocalizedString(@"Add Program", nil);
    lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
    lblTitle.textColor = kAppTintColor;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.adjustsFontSizeToFitWidth=YES;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=lblTitle;
    
    if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
    {
        self.txtProgramPurpose.hidden = NO;
        self.txtWorkoutName.hidden = NO;
        self.lblProgramPurpose.hidden = NO;
        self.lblWorkoutName.hidden = NO;
        btnAddPrograme.frame = CGRectMake(200,187 ,99 , 36);
    }
    else
    {
         lblTitle.text = NSLocalizedString(@"Edit Program", nil);
        self.txtName.text =self.strSheetName;
        self.txtProgramPurpose.hidden = YES;
        self.txtWorkoutName.hidden = YES;
        self.lblProgramPurpose.hidden = YES;
        self.lblWorkoutName.hidden = YES;
        btnAddPrograme.frame = CGRectMake(200,100 ,99 , 36);

        
    }
    self.txtName.delegate = self;
    self.txtProgramPurpose.delegate = self;
    self.txtWorkoutName.delegate = self;
    
    NSArray *fields = @[self.txtName,self.txtProgramPurpose,self.txtWorkoutName];
    [self setKeyboardControls:[[KeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:(id)self];
    fields = nil;
  
}

//-----------------------------------------------------------------------

-(void)viewWillAppear:(BOOL)animated
{START_METHOD
    [super viewWillAppear:animated];
    
    CGRect rectOfMainScreen = [[UIScreen mainScreen] bounds];
    CGFloat xPos = 0.0;
    CGFloat yPos = 0.0;
    
    if (iOS_7) {
        if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)){
            xPos = (rectOfMainScreen.size.height / 2) - (500 / 2);
            yPos = (rectOfMainScreen.size.width / 2) - (294 / 2);
        } else {
            xPos = (rectOfMainScreen.size.width / 2) - (500 / 2);
            yPos = (rectOfMainScreen.size.height / 2) - (294 / 2);
        }
        
        if (self.isSetFrame) {
            self.navigationController.view.superview.frame = CGRectMake(xPos, yPos, 500, 150+44);
        } else {
            self.navigationController.view.superview.frame = CGRectMake(xPos, yPos, 500, 250+44);
        }
    }
    
    if ([self.strSheetName isEqualToString:@""] || !self.strSheetName)
    {
        self.txtProgramPurpose.hidden = NO;
        self.txtWorkoutName.hidden = NO;
        self.lblProgramPurpose.hidden = NO;
        self.lblWorkoutName.hidden = NO;
    }
    else
    {
       [self setTitle:NSLocalizedString(@"Edit Program", nil)];
        self.txtName.text =self.strSheetName;
        self.txtProgramPurpose.hidden = YES;
        self.txtWorkoutName.hidden = YES;
        self.lblProgramPurpose.hidden = YES;
        self.lblWorkoutName.hidden = YES;
        
    }
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//-----------------------------------------------------------------------

@end
