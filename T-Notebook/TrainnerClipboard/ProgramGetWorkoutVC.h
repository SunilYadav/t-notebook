//
//  ProgramGetDataVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddNewWorkoutSheetDelegate
-(void) addNewWorkout:(NSString *)WorkoutSheet :(NSString *)isFromWhere ;
@end

@interface ProgramGetWorkoutVC : BaseViewController
@property (nonatomic, unsafe_unretained) id<AddNewWorkoutSheetDelegate>			delegate;
@property (nonatomic, strong) NSString			*strSheetName;
@property (nonatomic, readwrite) BOOL			isEditTemplateSheet;
@end
