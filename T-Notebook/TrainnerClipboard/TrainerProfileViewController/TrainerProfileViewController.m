//
//  TrainerProfileViewController.m
//  TrainnerClipboard
//
//  Created by Sunil Yadav on 02/08/16.
//  Copyright © 2016 WiOS Monika R. Brahmbhatt. All rights reserved.
//

//Facebook library
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FacebookSDK/FacebookSDK.h>

#define BORDER_COLOR                            170/255.0f

#define     NULL_STRING                         @""
#import "TextFileManager.h"

//For Getting clients's image
#import "FileUtility.h"


#import "SignViewController.h"
#import "TrainerProfileViewController.h"

@interface TrainerProfileViewController ()<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIDocumentInteractionControllerDelegate>{
        
        UIDocumentInteractionController *documentInteractionController;
    UITextField *textfiledActive;
    UIImagePickerController     *_imgPickerController;
    NSData                      *_imageData;
}

@property (weak, nonatomic) IBOutlet UITextField *textFiledName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFiledPas;
@property (weak, nonatomic) IBOutlet UITextField *textFiledConfirmPass;
@property (weak, nonatomic) IBOutlet UILabel    *labelTheTraining;
@property (weak, nonatomic) IBOutlet UILabel    *labelNotebook;

@property (weak, nonatomic) IBOutlet UIButton       *buttonUpdate;
@property (weak, nonatomic) IBOutlet UITextField    *textFiledConfirmEmail;
@property (weak, nonatomic) IBOutlet UIButton       *buttonHome;
@property (weak, nonatomic) IBOutlet UIButton       *buttonProfileImage;
@property(nonatomic,strong) UIPopoverController		*popoverController;


@property (nonatomic , strong) UIImagePickerController         *picker;
@property (weak, nonatomic) IBOutlet UIView *viewFields;
@property (weak, nonatomic) IBOutlet UILabel *lableProgress;

@property (weak, nonatomic) IBOutlet UIView *viewShowLabel;

@property (weak, nonatomic) IBOutlet KeyBoardScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lableOr;
@property (weak, nonatomic) IBOutlet UIButton *buttonFb;

@end

@implementation TrainerProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    START_METHOD
    
    
    MESSAGE(@"viewDidLoad----> 233333");
    
    //
    [TheAppController hideHUDAfterDelay:0];
    
    self.buttonFb.hidden                =   NO;

    //Hide the View
    self.viewShowLabel.hidden = YES;

    //-------> TESTING PUSPOSE FOR MIGRATION
    
    /*
    [commonUtility saveValue:@"NO" andKey:KEY_UPGRADE_STATUS];
    [commonUtility saveValue:nil andKey:KEY_TRAINER_ID];
    */
    
//    [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:kisFullVersion];

  
    //    [TheAppController showHUDonView:nil];
    
    //<------
    
    
    /*
    //Sunil
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    //If not Upgrade then show alert for upgrade
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke method for show alert  for upgrade
        self.textFieldEmail.userInteractionEnabled          = NO;
        self.textFiledConfirmEmail.userInteractionEnabled   = NO;
        
    }else{
        //Invoke method for show alert  for upgrade
        self.textFieldEmail.userInteractionEnabled          = YES;
        self.textFiledConfirmEmail.userInteractionEnabled   = YES;
        
        
    }

    */
    //Set textfiled values
    [self setTextFiledValues];
    
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    //If not Upgrade then show alert for upgrade
    if([strUpgradeStatus isEqualToString:@"YES"]){
        
        //Invoke for get Image from server
        [self getTrainerImage];
        
        //set the textfileds frame
        [self setTheFrame];
        
    }else{
        
        //Get take the trainer ID
        NSString *strTrainerId = [commonUtility retrieveValue:KEY_TRAINER_ID];
       
        //If there is trainer id
        if(strTrainerId && strTrainerId.length>0){
            
        }else{
            
            // Get data for start migration from last stage
             [self    getAllDataFromLocalDbForMigrationForSaveInLocalFile];
            
            
            //Create alert for upgrade
            UIAlertView *alertView      = [[UIAlertView alloc]initWithTitle:NSLocalizedString(kAppName, @"") message:@"Please save file to secure your data for centralisation, it will be better if you save the file so that in future you can get the data back from this file if you get data loss. \n\n Just click the ‘OK’ and after that tap on the file’s content which is there below the alert. Now, You will see an icon on top right corner for upload/save file. Just tap on upload/save icon and save/upload your file. \n\nPlease tap on ‘DONE’ which on top left corner of the screen to proceed further in the migration process. \n\n Please do not remove or uninstall the app. For any query, Please contact T-Notebook Support at support@thetrainingnotebook.com" delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles: nil];
            
            //Set Random alert vi
            alertView.tag      = 1010;
            
            [alertView show];
        }
    }
    
    //create imagepicker
    [self createImagePicker];
    
    //Invoke method for make profile pic in round shape
    [self makeRoundProfilePic];
    
    END_METHOD
    
}


-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    
    //Set the button user interaction enabaled YES at time when c=view will appear
    _buttonHome.userInteractionEnabled  = YES;
    
    //Get take the trainer ID
    NSString *strTrainerId              = [commonUtility retrieveValue:KEY_TRAINER_ID];
    NSString *strUpgrade                =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    MESSAGE(@"viewWillAppear----> 233333 outside : %@ and strUpgrade : %@",strTrainerId,strUpgrade);

    //Do some workl n the main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //If there is trainer id, Auto login from the migration
        if(strTrainerId && strTrainerId.length>0 && strTrainerId.intValue>0 && strUpgrade && strUpgrade.length>0 && [strUpgrade isEqualToString:@"NO"]){
            
            MESSAGE(@"viewWillAppear----> 233333 inseide : %@ and strUpgrade : %@",strTrainerId,strUpgrade);

            //Invoke Method for migrate more clients
            [self getAllDataFromLocalDbForMigration:[commonUtility retrieveValue:KEY_TRAINER_ID]];
            
        }
        
    });
    
    END_METHOD
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextFiled Delegate

//Delegate method for Retun the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    START_METHOD
    
    //Resign KEyboard
    [textField resignFirstResponder];
    return YES;
    END_METHOD
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    START_METHOD
    textfiledActive =   textField;
    
    //No need to implement this
/*
    if(textField.tag == 2 || textField.tag == 3){
        
        //Sunil
        //Get Upgrade status
        NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
        
        
        //If not Upgrade then show alert for upgrade
        if([strUpgradeStatus isEqualToString:@"YES"]){
            
            [commonUtility alertMessage:@"You can't change the Email Address"];
            [textfiledActive resignFirstResponder];

            return NO;
            
        }else{
            return YES;
        }
        
        
    }else{
        return YES;
    }
    */
     return YES;
    END_METHOD
}


#pragma mark - Action Methods

//Action method for the save the Updated Edit PRofile of Trainer
- (IBAction)actionUpdateTrainerProfile:(id)sender {
    
    START_METHOD
    
    
    //Resign KEyboard
    if([textfiledActive isFirstResponder]){
        
        //Resign the keyboard
        [textfiledActive resignFirstResponder];
    }
    
    MESSAGE(@"Values Name: %@, Email: %@, Pss: %@, Conf:  %@", self.textFiledName.text, self.textFieldEmail.text,self.textFiledPas.text,self.textFiledConfirmPass.text);
    
    //Invoke Method for Validation
    if([self validateTextFieldValues]){
        
        //Check Network connection
        if([TheAppDelegate isNetworkAvailable]){
         
            //Set user interation button NO
            self.buttonHome.userInteractionEnabled  =   NO;
            
            //Invoke Method for make dictionry for trainer
            [self createDictionryTrainer:nil];
            
        }else{
            
            //Alert
            [commonUtility alertMessage:@"Network not available!" ];
        }
        
    }
    
    END_METHOD
    
}

//Method for move to Home Screen
- (IBAction)actionMoveToHomeScreen:(id)sender {
    
    START_METHOD
    
    //Pop view
    [self.navigationController popViewControllerAnimated:YES];
    
    END_METHOD
}



#pragma mark - Validate Textfields
/*!
 @Description : Methods for Validate Textfiled
 @Param       :
 @Return      :
 */
-(BOOL)validateTextFieldValues{
    START_METHOD
    //Get Upgrade status
    NSString *strUpgradeStatus         =        [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    
    //If not Upgrade then show alert for upgrade
    if([strUpgradeStatus isEqualToString:@"YES"]){
        //set the textfileds
        self.textFiledConfirmEmail.text = self.textFieldEmail.text;
    }
    
    
    //Check Email text  Filed blank or not
    if([self.textFiledName.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter full name.", nil)];
        return NO;
    }
    
    
    //Check user name for Character only
    else if(![commonUtility validateName:self.textFiledName.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Name must be only character", nil)];
        return NO;
    }
    
    
    //Check Email text  Filed blank or not
    else if([self.textFieldEmail.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter primary email address.", nil)];
        return NO;
    }
    
    //Check user name for Character only
    else if(![commonUtility validateEmailWithString:self.textFieldEmail.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Primary Email not valid", nil)];
        return NO;
    }
    
    
    
    
    //Check Email text  Filed blank or not
    else if([self.textFiledConfirmEmail.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter confirm email address.", nil)];
        return NO;
    }
    
    //Check user name for Character only
    else if(![commonUtility validateEmailWithString:self.textFiledConfirmEmail.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Confirm Email not valid", nil)];
        return NO;
    }
    
    
    
    
    //Check Phone textfiled Empty
    else if(![self.textFieldEmail.text isEqualToString:self.textFiledConfirmEmail.text])    {
        [commonUtility alertMessage:@"Primary email and confirm email must be same!"];
        return NO;
    }
    
    
    
    //Check User name Empty
    else if([self.textFiledPas.text  isEqualToString:NULL_STRING])    {
        [commonUtility alertMessage:@"Please enter the password"];
        return NO;
    }
    
    //Check Phone textfiled Empty
    else if([self.textFiledConfirmPass.text isEqualToString:NULL_STRING])    {
        [commonUtility alertMessage:@"Please enter the confirm password"];
        return NO;
    }
    
    
    //Check Phone textfiled Empty
    else if(![self.textFiledPas.text isEqualToString:self.textFiledConfirmPass.text])    {
        [commonUtility alertMessage:@"Password and confirm password must be same!"];
        return NO;
    }
    else
        return YES;
    END_METHOD
}



#pragma mark - Added MEthods

//TODO: Set TextFileds valuse
-(void)setTextFiledValues{
    START_METHOD
    //All Clients with Deatils
    NSArray *arrayAllClients = [NSArray arrayWithArray:[[DataManager initDB] getTrainerProfile]];

    MESSAGE(@"arry trainer : %@",arrayAllClients);
    
    
    if(arrayAllClients && arrayAllClients.count>0){
        
    //Get Dixtionry
    NSDictionary *dictProfile       = [commonUtility dictionaryByReplacingNullsWithStrings: [arrayAllClients objectAtIndex:0]];
 
    //Set Textfileds
    self.textFieldEmail.text        =   [dictProfile objectForKey:EMAIL];
    self.textFiledConfirmEmail.text =   [dictProfile objectForKey:EMAIL];
    self.textFiledName.text         =   [dictProfile objectForKey:USERNAME];
        
    self.textFiledConfirmPass.text  =   [dictProfile objectForKey:PASSWORD];
    self.textFiledPas.text          =   [dictProfile objectForKey:PASSWORD];
    }
    END_METHOD
}


//TODO: POST REQUEST FOR UPDATE TRAINER PROFILE

//Method for post request for SIGNIN
-(void)postRequestToServerForTrainerProfile:(NSDictionary *)params{
    START_METHOD
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@trainerProfile/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    
    MESSAGE(@"postRequestToServerForTrainerProfile-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Check
                               if([responseDataDictionary objectForKey:METADATA]){
                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   //Show Error Alert
                                   NSString *error= [dictError objectForKey:@"error"];
                                 
                                   
                                   //If there is no error
                                   if(error && error.intValue==0){
                                       
                                       //Get the taus for thr user upgration
                                       NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
                                       
                                       
                                       //Check Upgraded User
                                       if ([strUpgradeStatus isEqualToString:@"YES"]) {
                                           
                                           //Invoke method for take action on update profile
                                           [self takeActionOnUpdateProfile:responseDataDictionary];
                                           
                                       }else{//
                                           //Invoke the method for post the local sb to the server
                                           [self postLocalDbToServer:responseDataDictionary];
                                           
                                       }
                               
                                       
                                   }else{//If getting the erroe from the server show the alert to the user
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       
                                       //create alertView
                                       UIAlertView *alertMessage = [[UIAlertView alloc]
                                                                    initWithTitle:@"" message:[NSString stringWithFormat:@"%@",[dictError objectForKey:POPUPTEXT]]
                                                                    delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                       //Show alertView
                                       [alertMessage show];
                                       
//                                       [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                       
                                       self.viewFields.userInteractionEnabled = YES;
                                       
                                       MESSAGE(@"Eror : %@",error);
                                       
                                       //Set the button user interation enabled NO
                                       _buttonHome.userInteractionEnabled = NO;
                                       
                                       
                                       //Hide the indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       
                                       
                                       //Set UserInteraction Enable YES
                                       self.buttonHome.userInteractionEnabled  =   YES;
                                   }
                               }
                               
                           }failure:^(NSError *error) { // If gettingt the falire result from the server
                               
                               //Set the fields user interation user YES
                               self.viewFields.userInteractionEnabled = YES;
                               
                               MESSAGE(@"Eror : %@",error);
                               
                               //User interation enbamed set NO
                               _buttonHome.userInteractionEnabled = NO;

                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               MESSAGE(@"Hide indicateor--------->");
                               
                               //Show Alert For error
                               [commonUtility alertMessage:@"Error occurred. Please try again."];
                               
                               
                               //Set UserInteraction Enable YES
                               self.buttonHome.userInteractionEnabled  =   YES;
                               
                           }];
    END_METHOD
}

//TODO: POST MIGRATE TEXT FILE

//Method for post request for SIGNIN
-(void)postRequestToMigrateAllData:(NSString * )  strFilePath  andClientDict:(NSMutableDictionary *) dictClient{
    START_METHOD
    
    //Set the user interation enambld : NO
    self.viewFields.userInteractionEnabled = NO;

    //Set the view for show label hide in front of the user
    self.viewShowLabel.hidden = NO;

    //Carete URL by string
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:strFilePath];
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             USERNAME           :   self.textFiledName.text,
                             };
    
    
    //Make url for hit the sign api
    
    NSString *strUrl =[NSString stringWithFormat:@"%@migrationFileUpload",HOST_URL];
    
    MESSAGE(@"postRequestToMigrateAllData-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //invoke method for move the db file to the server
    [objService postDataWithFile:strUrl parameters:params filePath:fileURL
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce from file: %@", responseDataDictionary);
                               
                               //Check
                               if([responseDataDictionary objectForKey:METADATA]){
                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   //Show Error Alert
                                   NSString *error= [dictError objectForKey:@"error"];
                                   
                                   
                                   //If there is no error in the response
                                   if(error && error.intValue==0){
                                       
                                       //If geeting the dictionry of the client
                                       if(dictClient){
                                           
                                           MESSAGE(@"responce from dictClient102: %@", dictClient);
                                           
                                           //Invoke method for get all Image
                                           [self getClientsInfoForImages:dictClient];
                                           
                                           
                                       }else{
                                           
                                            //Invoke method for Send Message tosend the email
                                           [self sendMessageForMail];
                                       }
                                       
                                       /*
                                       //************  GET Trainer's Profile AND SAVE IN LOCAL DB
                                       NSString  *strAccessKey         =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:KEY_ACCESS];
                                       
                                       
                                       //Save Access key
                                       [commonUtility saveValue:strAccessKey andKey:KEY_ACCESS];
                                       */
                                       
                                       
                                   }else{
                                       
                                       
                                       //Hide the indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       MESSAGE(@"Hide indicateor--------->");
                                       
                                       self.buttonHome.userInteractionEnabled  = YES;
                                       self.viewFields.userInteractionEnabled  = YES;
                                       
                                       [self.navigationController popViewControllerAnimated:YES];
                                       
                                        [commonUtility alertMessage:@"Error occurred. Please try again."];
                                       //Show Alert For error
                                       
                                   }
                                   
                               }else{
                                   
                               }
                               
                                                              
                               
                           }failure:^(NSError *error) {
                               
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               MESSAGE(@"Hide indicateor--------->");
                               
                               self.buttonHome.userInteractionEnabled  = YES;
                               self.viewFields.userInteractionEnabled  = YES;
                               
                               [self.navigationController popViewControllerAnimated:YES];
                               [commonUtility alertMessage:@"Error occurred. Please try again."];
                               
                               //Show Alert For error
                               
                           }];
    END_METHOD
}


//TODO: POST MIGRATE TEXT FILE

//Method for post request for SIGNIN
-(void)postLocalDbToServer:(NSDictionary *)responseDataDictionaryGet{
    START_METHOD
    
    //Carete URL by string by invoking method for get local db path
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:[self getDBPath]];
    NSArray *arrayAllClients                    = [NSArray arrayWithArray:[[DataManager initDB] getTotalClientsCount]];

    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             EMAIL           : self.textFieldEmail.text,
                             @"device_udid"  : [commonUtility getUDID],
                             @"total_clients": [NSString stringWithFormat:@"%lu",(unsigned long)arrayAllClients.count],
                             @"total_images" : @"Sorry",
                             };
    
    //Make url for hit the sign api    
    NSString *strUrl =[NSString stringWithFormat:@"%@sqlUploadWithUserStatics",HOST_URL];
    
    MESSAGE(@"postRequestToMigrateAllData-> params=: total_clients %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postDataWithFile:strUrl parameters:params filePath:fileURL
                         success:^(NSDictionary *responseDataDictionary) {
                             
                             MESSAGE(@"responce from file: %@", responseDataDictionary);
                             
                             //Check
                             if([responseDataDictionary objectForKey:METADATA]){
                                 
                                 //Get Dictionary
                                 NSDictionary   *dictError    =  [responseDataDictionary objectForKey:METADATA];
                                 
                                 //Show Error Alert
                                 NSString *error= [dictError objectForKey:@"error"];

                                 
                                 if(error && error.intValue==0){

                                     //Set UserInteraction Enable YES
                                     self.buttonHome.userInteractionEnabled  =   NO;
                                     
                                     
                                     //Invoke method for take action on update profile
                                     [self takeActionOnUpdateProfile:responseDataDictionaryGet];
                                     
                                     
                                 }else{
                                     
                                     
                                     //Hide the indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"Hide indicateor--------->");
                                     
                                     self.buttonHome.userInteractionEnabled  = YES;
                                     self.viewFields.userInteractionEnabled  = YES;
                                     
                                     [self.navigationController popViewControllerAnimated:YES];
                                     
                                      [commonUtility alertMessage:@"Error occurred. Please try again."];
                                     //Show Alert For error
                                     
                                 }
                                 
                             }
                             
                             
                         }failure:^(NSError *error) {
                             
                             
                             //Hide the indicator
                             [TheAppController hideHUDAfterDelay:0];
                             MESSAGE(@"Hide indicateor--------->");
                             
                             self.buttonHome.userInteractionEnabled  = YES;
                             self.viewFields.userInteractionEnabled  = YES;
                             
                             [self.navigationController popViewControllerAnimated:YES];
                             
                             [commonUtility alertMessage:@"Error occurred. Please try again."];

                             //Show Alert For error
                             
                         }];
    END_METHOD
}

//TODO: MIGRATION : take local dat for server
//Method For Get all data for Migrations
-(void)getAllDataFromLocalDbForMigration:(NSString *)strTrainerId{
    
    START_METHOD

    //[self.buttonUpdate setTitle:@"Please Wait..." forState:UIControlStateNormal];
    [TheAppController showHUDonView:nil];
    
    //Show the view for the lable
    self.viewShowLabel.hidden = NO;
    

    //Set the button home userionteration enabled NO
    _buttonHome.userInteractionEnabled = NO;
    
    //Full Dictionary for make full json
    NSMutableDictionary     *objDictionary  =   [[NSMutableDictionary alloc]init];
    
    //Get All Question's category
    NSArray *arrayQuestionCategory = [NSArray arrayWithArray:[[DataManager initDB] selectQuestionCategory]];

     //Arry questions
    NSArray *arrayQuestion                  = [NSArray arrayWithArray:[[DataManager initDB] getAllQuestions]];
   
    //Template
    NSArray *arrayTemplates                 = [NSArray arrayWithArray:[[DataManager initDB] getAllTemplatesDetail]];
   
    
    //All Template Sheets (All Programs in all Template )
    NSArray *arrayTemplatePrograms          = [NSArray arrayWithArray:[[DataManager initDB] getAllTemplatePrograms]];
    
    
    //All Template block data (All workouts in in all Template )
    NSArray *arrayWorloutsTemplates         = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForTemplates]];
    
    
    MESSAGE(@"arrayWorloutsTemplates--->new:  %@",arrayWorloutsTemplates);
    
    
    //All Template Exercise (All Exercise in in all Template )
    NSArray *arrayExerciseTemplates         = [NSArray arrayWithArray:[[DataManager initDB] getProgramTemplateLines]];
 
    //All Clients with Deatils
    NSArray *arrayAllClients                = [NSArray arrayWithArray:[[DataManager initDB] getClientsDetail]];

 
    //Array for store all assessment
    NSMutableArray *arrayAllclientsWithinfo  =   [[NSMutableArray alloc]init];
    
    if(arrayAllClients && arrayAllClients.count>0){
    
    //Loop for all clients
    for (int i=0; i<1; i++) {
        
        //Invoke method for get count
       [self getRemainingCount];
        
        //get Client Dictionary
        NSMutableDictionary  *dictClient   =  [[NSMutableDictionary alloc]initWithDictionary:[arrayAllClients objectAtIndex:i]];
        
           MESSAGE(@"arrayAllClients-> dictClient: %@",dictClient);
        
        
        //************************* Answers  *******************
        NSArray     *arrayAnswer            =   [NSArray arrayWithArray:[[DataManager initDB] getAllAnswersByClientID:[dictClient objectForKey:kClientId]]];
        
        MESSAGE(@"arrayAllClients-> arrayAnswer: %@",arrayAnswer);
        [dictClient setValue: arrayAnswer forKey:@"answers"];
        
        
        //************************* All Programs  *******************
        
        //Get all Programs
        NSArray *arrayPrograms              =  [NSArray arrayWithArray:[[DataManager initDB] getProgramSheets:[dictClient objectForKey:kClientId] ]];
        
          MESSAGE(@"arrayAllClients-> arrayPrograms: %@",arrayPrograms);
       
        NSMutableArray *arrayProgram         =   [[NSMutableArray alloc]init];
        
        //For loop for get workout by serial
        for (int j=0;  j<arrayPrograms.count;j++) {
            
            //Create Dict for POst on server
            NSMutableDictionary *dictPrograms = [[NSMutableDictionary alloc]init];
            
            [dictPrograms setValue:[arrayPrograms objectAtIndex:j] forKey:@"program"];
            
            
             MESSAGE(@"arrayAllClients-> arrayPrograms: %@",dictPrograms);
            
            
            //************************* All Exercises  *******************
            //Get all Aseesments
            NSArray *arrayExercise          =  [NSArray arrayWithArray:[[DataManager initDB] getAllExercises:[dictClient objectForKey:kClientId] andProgramId:[[arrayPrograms objectAtIndex:j] objectForKey:kSheetId]]];
            
            
            [dictPrograms setValue:arrayExercise forKey:@"exercise"];
         
            
            
            //Get all Notes
            NSArray *arrayNote =  [NSArray arrayWithArray:[[DataManager initDB] getAllNotesUnderProgram:[dictClient objectForKey:kClientId] andSheetId:[[arrayPrograms objectAtIndex:j] objectForKey:kSheetId]]];
            
            
            [dictPrograms setValue:arrayNote forKey:@"notes"];
            [arrayProgram addObject: dictPrograms];
            
            MESSAGE(@"dictClient==? %@",arrayPrograms);
        }
      
        //************************* All Aseesments  *******************
        
        
        //Get all Aseesments
        NSArray *arrayAssess =  [NSArray arrayWithArray:[[DataManager initDB] getAllAssessmentSheetsFor:[dictClient objectForKey:kClientId] ]];
        
        
        //Create Dict for POst on server
        NSMutableDictionary *dictAssessment = [[NSMutableDictionary alloc]init];
        
        [dictAssessment setValue:arrayAssess forKey:@"assessment"];
        
        //Add aseessment dict in array
//        [arrayAssesmentSheets addObject:dictAssessment];
        
        //************************* All Aseesments  *******************
        
        
        //Get all Aseesments
        NSArray *arrayAssesmentSheetFullData =  [NSArray arrayWithArray:[[DataManager initDB] getAllAssessmentSheetFullData:[dictClient objectForKey:kClientId] ]];
        
        MESSAGE(@"Array Asseessemnt: %@",arrayAssesmentSheetFullData);
        
        NSMutableArray *arrayTemp   =   [[NSMutableArray alloc]init];
        if(arrayAssesmentSheetFullData && arrayAssesmentSheetFullData.count>0){
            
            //Set Dictionary
            NSMutableDictionary *dictTemp   =   [[NSMutableDictionary alloc]initWithDictionary:[arrayAssesmentSheetFullData objectAtIndex:0]];
            
            if([[dictClient objectForKey:kgender] isEqualToString:@"F"]){
          
            
            //SET VALUES FOR FEMALE
            [dictTemp setValue:[dictTemp objectForKey:@"fThree_Female_Triceps"]     forKey:@"fThree_Male_Chest"];
            [dictTemp setValue:[dictTemp objectForKey:@"fThree_Female_Suprailiac"]  forKey:@"fThree_Male_Abdomen"];
            [dictTemp setValue:[dictTemp objectForKey:@"fThree_Female_Thigh"]       forKey:@"fThree_Male_Thigh"];
                
                 [arrayTemp addObject:dictTemp];
                 [dictAssessment setValue:arrayTemp forKey:@"assessmentFullData"];
            }else{
                  [dictAssessment setValue:arrayAssesmentSheetFullData forKey:@"assessmentFullData"];
            }
           
            
        }
        
        [dictClient setValue:arrayProgram           forKey:@"clientprogram"];
        [dictClient setValue:dictAssessment         forKey:@"clientAsseessment"];
        
        //Add the object in the aaray
        [arrayAllclientsWithinfo addObject:dictClient];
        //------> 26 Dec
        
        //--->26 Dec
        NSString *strUser_type_old;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
            
            
            //Set the user type
            strUser_type_old    =   @"YES";
            
        }else{
            
            //Set the user type
            strUser_type_old    =   @"NO";
            
            //Set USER FUll Version
//            [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
            
        }
        
        //Update the User type status
        [objDictionary setValue:strUser_type_old                    forKey:USER_TYPE_OLD];
        [objDictionary setValue:strTrainerId                        forKey:ID];
        [objDictionary setValue:arrayAllclientsWithinfo             forKey:@"clients"];
        
        //************************* DICTIONARY FOR ALL *******************
        
        //Set all array in dictionary for make JSON
        
        
        
        MESSAGE(@"cleicnt full dictonary : %@",objDictionary);
        
        /*
        //Questions & Answers
        [objDictionary setValue:arrayQuestionCategory   forKey:@"questionCategory"];
        [objDictionary setValue:arrayQuestion           forKey:@"allQuestions"];
        
        
        //Templates
        [objDictionary setValue:arrayTemplates          forKey:@"templates"];
        [objDictionary setValue:arrayWorloutsTemplates  forKey:@"workoutsTemplates"];
        [objDictionary setValue:arrayExerciseTemplates  forKey:@"exerciseTemplates"];
        [objDictionary setValue:arrayTemplatePrograms   forKey:@"templatePrograms"];
        */
        
        
        //TODO: Write Text in text file
        
        //Carete objet of TextFileManager for write text
        TextFileManager *obj    =   [TextFileManager getSharedInstance];
        
        //Invoke method for write JSON in text file
        NSString *strFilePath   =   [obj writeTextToFile:objDictionary];
        
        
        //invoke methos for post file on server
        [self postRequestToMigrateAllData: strFilePath andClientDict:dictClient];
        
        
        //profileImageOfClient
        //getAllAssessmentImagesForSheet
        MESSAGE(@"objDictionary---> for migration 1101: %@ \n\n and client dict:  %@",objDictionary,dictClient);
        
        
        //<-----
        
    }
    }else{
        
           //--->26 Dec
    NSString *strUser_type_old;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        
        //Set the user type
        strUser_type_old    =   @"YES";
        
            }else{
        
        //Set the user type
        strUser_type_old    =   @"NO";
      
                //Set USER FUll Version
//                [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
        
    }

    //Update the User type status
    [objDictionary setValue:strUser_type_old                    forKey:USER_TYPE_OLD];

    [objDictionary setValue:strTrainerId                    forKey:ID];
    
     //************************* DICTIONARY FOR ALL *******************

    //Set all array in dictionary for make JSON
    
   
    
    MESSAGE(@"cleicnt full dictonary : %@",objDictionary);
    
 
    //Questions & Answers
    [objDictionary setValue:arrayQuestionCategory   forKey:@"questionCategory"];
    [objDictionary setValue:arrayQuestion           forKey:@"allQuestions"];
  
    
    //Templates
    [objDictionary setValue:arrayTemplates          forKey:@"templates"];
    [objDictionary setValue:arrayWorloutsTemplates  forKey:@"workoutsTemplates"];
    [objDictionary setValue:arrayExerciseTemplates  forKey:@"exerciseTemplates"];
    [objDictionary setValue:arrayTemplatePrograms   forKey:@"templatePrograms"];
    [objDictionary setValue:@"YES"                  forKey:@"migrationDone"];

//TODO: Write Text in text file
    
    //Carete objet of TextFileManager for write text
    TextFileManager *obj    =   [TextFileManager getSharedInstance];
    
    //Invoke method for write JSON in text file
    NSString *strFilePath   =   [obj writeTextToFile:objDictionary];
    
        
        //invoke methos for post file on server
        [self postRequestToMigrateAllData: strFilePath andClientDict:nil];
    
    
    //profileImageOfClient
    //getAllAssessmentImagesForSheet
    MESSAGE(@"objDictionary---> for migration 1101: %@",objDictionary);
    
    }    ////<----26 Dec

    END_METHOD
    
}


 #pragma mark -
 #pragma mark prior to database work methods

//Method for the DB path
 - (NSString *) getDBPath {
     
     START_METHOD
 NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
 NSString * documentsDir = [paths objectAtIndex:0];
     END_METHOD
 return [documentsDir stringByAppendingPathComponent:@"TrainerClipboardDatabase.sqlite"];
 }
 
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //For create the DB if
- (void) copyDatabaseIfNeeded{
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSError * error;
    NSString * dbPath = [self getDBPath];
    MESSAGE(@"defaultDBPath: %@",dbPath);
    
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if (success){
        success = [fileManager removeItemAtPath:dbPath error:&error];
    }
    
    
    if(success) {
        NSString * defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"TrainerClipboardDatabase.sqlite"];
        
        MESSAGE(@"defaultDBPath: %@",defaultDBPath);
        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
}

//Craete the table if not exist
 - (void) CreateTableIfNotExist_OLD{

     
     NSArray *array = [[DataManager initDB]getClientsID];
     int i;
     for(i=0;i<[array count];i++){
         [[DataManager initDB] createClientTablesIfNotExist:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
     }
     
 }
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 - (void) CreateTableIfNotExist{
 
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 NSString *previousVersion=[defaults objectForKey:@"version"];
 NSString *currentVersion=[self versionNumberString];
 
 if(previousVersion != nil) {
 return;
 }
 
 if(previousVersion == nil || [previousVersion compare:currentVersion options:NSNumericSearch] == NSOrderedAscending) {
 
 
 [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
 
 
 NSArray *array = [[DataManager initDB]getClientsID];
 int i;
 
 @try {
 
 for (i = 0; i< [array count]; i++) {
 [[DataManager initDB] createClientTablesIfNotExist:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
 [[DataManager initDB] addNewFieldsToTablesIfnotAddedForClientId:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
 }
 [defaults setObject:currentVersion forKey:@"version"];
 [defaults synchronize];
 
 [[NSNotificationCenter defaultCenter]postNotificationName:kAppUpdated object:nil];
 
 }
 @catch (NSException *exception) {
 [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
 
 [[NSNotificationCenter defaultCenter]postNotificationName:kAppUpdated object:nil];
 return;
 }
 
 }
 
 
 }

- (NSString *)versionNumberString {
    
    START_METHOD
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return majorVersion;
}
 //---------------------------------------------------------------------------------------------------------------


//Method for move to Sign In view controller
-(void)moveToLoginView{
    
    
    //Hide the indicator
    [TheAppController showHUDonView:nil];
    MESSAGE(@"Hide indicateor--------->");
    
    
    //Save lout Key
    [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
    
    //Get All The controllers
    NSMutableArray  *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    BOOL            success             =   NO;
    
    //Get Perticular controller and move
    for (UIViewController *objProspectViewController in allViewControllers) {
        
        //If Sign In
        if ([objProspectViewController isKindOfClass:[SignViewController class]])  {
        
            
            //Push view with delay for Alert message
            [self.navigationController popToViewController:objProspectViewController animated:NO];
            
            //Set YES
            success                     =   YES;
        }
    }
    
    //If not controller found then push
    if(!success){
        //Save lout Key
        [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
        
        //Create object of trainer profile view
        SignViewController *objMenuView  = (SignViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SignViewController"];
    
        
        //Push view
        [self.navigationController pushViewController:objMenuView animated:NO];
    }
    
}


#pragma mark - Handle Response fromServer

//Method for Take action on update profile
-(void)takeActionOnUpdateProfile: (NSDictionary *)responseDataDictionary{

    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //Get Dictionary
    NSMutableDictionary   *dictProfileData;
    
    
    //Check For Upload Image
    if([responseDataDictionary objectForKey:PAYLOAD]){
        
        //Get Dictionary
        NSDictionary   *dictProfilePayload      =   [responseDataDictionary objectForKey:PAYLOAD];
        
         dictProfileData        =   [[commonUtility dictionaryByReplacingNullsWithStrings:[dictProfilePayload objectForKey:RESPONSE_DATA] ]mutableCopy];
    }
    
    MESSAGE(@"responce: %@", responseDataDictionary);
    
    //Set UserInteraction Enable YES
//    self.buttonHome.userInteractionEnabled  =   YES;
    
    
    //If Success
    if([responseDataDictionary objectForKey:PAYLOAD]){
        
        //Check key for data payload
        if ([strUpgradeStatus isEqualToString:@"YES"]) {
            
            //Get Data and save in loacl
            [self updateTrainerProfile:responseDataDictionary];
            
            //Set the move to YES for move to next view
            [dictProfileData setValue:@"YES" forKey:@"move"];
            
            //Invoke method for Uplaod thee trainer profile pic
            [self postRequestForUploadTrinerProfilePic:dictProfileData];
            
        }else{
            //Set the move to NO for move to next view
            [dictProfileData setValue:@"NO" forKey:@"move"];
            
            //Invoke method for Uplaod thee trainer profile pic
            [self postRequestForUploadTrinerProfilePic:dictProfileData];
            
                //Get Dictionary
                NSDictionary   *dictProfilePayload      =   [responseDataDictionary objectForKey:PAYLOAD];
                
                //Get Dictionary
                NSDictionary   *dictProfileData         =   [commonUtility dictionaryByReplacingNullsWithStrings:[dictProfilePayload objectForKey:RESPONSE_DATA]];
                
                MESSAGE(@"Dict profile : %@",dictProfileData);
            
            //Save  the values for the Auto LOgin
            [commonUtility saveValue:[dictProfileData objectForKey:ID] andKey:KEY_TRAINER_ID];
            [commonUtility saveValue:self.textFieldEmail.text andKey:EMAIL];
            [commonUtility saveValue:self.textFiledPas.text andKey:PASSWORD];
            
            
            if(self.textFiledPas.text.length>0){
                
            }else{//If there is not valuse in textfiled come second time for migration
                [commonUtility saveValue:[commonUtility retrieveValue:FB_PASSWORD] andKey:PASSWORD];
                [commonUtility saveValue:[commonUtility retrieveValue:FB_EMAIL] andKey:EMAIL];
            }
            
            
            //Set the view feilds user interation Enbaled YES
            self.viewFields.userInteractionEnabled = YES;
            
            //for Migration invoke method to get all data
            [self getAllDataFromLocalDbForMigration:[dictProfileData objectForKey:ID]];
          
        }
        
        
    }else{//If Server respose a Error
        
        //Check
        if([responseDataDictionary objectForKey:METADATA]){
            
            //Get Dictionary
            NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
            
            //Show Error Alert
            [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
            
            //Hide Indicator
            [TheAppController hideHUDAfterDelay:0];
            MESSAGE(@"Hide indicateor--------->");
            
            
            //For Unauthorized user --->
            if( [dictError objectForKey:LIST_KEY]){
                
                NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                
                if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                    
                    //Show Error Alert
                    [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                    
                    //Move to dashbord for LOG OUT
                    [commonUtility  logOut];
                }
                
            }
            
            //<-----
        }
        
    }
    
    
}

//TODO; //LOCAL DB
-(void)updateTrainerProfile:(NSDictionary *)responseDataDictionary{
    START_METHOD
    //Check
    if([responseDataDictionary objectForKey:PAYLOAD]){
        
        //Get Dictionary
        NSDictionary   *dictProfilePayload      =   [responseDataDictionary objectForKey:PAYLOAD];
        
        //Get Dictionary
        NSDictionary   *dictProfileData         =   [commonUtility dictionaryByReplacingNullsWithStrings:[dictProfilePayload objectForKey:RESPONSE_DATA]];

        MESSAGE(@"Dict profile : %@",dictProfileData);
        
        //Insert Trainer Values
        int intValue = [[DataManager initDB] insertTrainerProfiles:dictProfileData];
        
        MESSAGE(@"insertTrainerProfiles-> =====: %d ",intValue);
        
      
    }
    
    END_METHOD
}

//TODO: GEt clients image on server

//Get client's image
-(void) getClientsInfoForImages:(NSDictionary *)objDictClient{
    START_METHOD
    
    //Invoke to get Client Profile Image
    [self getAllClientProfileImge:objDictClient];

    /*
        //Invoke method for get All Signature image
        [self signatureImageOfClient:objDictClient];
        
        
        //Get All client's Assessment Images
        [self getAllAssessmentImageInfo:[objDictClient objectForKey:kClientId]];
         
         */
    
    
    
}


//Method for get all the client's profile images
-(void)getAllClientProfileImge:(NSDictionary *)objDictClient{
    
    //Get the path of profile images of the clients
    NSString * imgPath =[NSString stringWithFormat:@"%@/ClientsProfileImage/ProfileImage%@.png",[FileUtility basePath],[objDictClient objectForKey:kClientId]];
    
    //Get the client image from the PAth
    UIImage * clientImage = [UIImage imageWithContentsOfFile:imgPath];
    NSData  *imageData;
    MESSAGE(@"All clients ID path: %@: and image: %@",imgPath,clientImage);
    
    //If image NIL
    if(clientImage == nil) {
        UIImage * imageTemp = nil;
        //Get Image data
        imageData   =   UIImageJPEGRepresentation(imageTemp, 1.0f);
    }else{
        
        //Get Image data
        imageData   =   UIImageJPEGRepresentation(clientImage, 1.0f);
        
    }
    
    //If image data then only post
    if(imageData != nil){
        
        //Invoke method for post the clients image on server
        [self postClientsImageOnServer:imageData andClientId:[objDictClient objectForKey:kClientId]];
    }else{
        
        //Invoke method for get client's Signature image
        [self signatureImageOfClient:objDictClient];
        
    }
    
}



//TODO: POST CLIENT's Image on server
-(void)postClientsImageOnServer:(NSData *)imageData andClientId:(NSString *)strClientID{
    START_METHOD
    
    
    //Craete Dictionry
    NSMutableDictionary *objDict            =   [[NSMutableDictionary alloc]init];
    
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN     forKey:@"access_key"];
    [objDict setValue:@"client_image"       forKey:ACTION];
    [objDict setValue:strClientID           forKey:kClientId];
    [objDict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID] forKey:@"user_id"];
    
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"ProfileImage%@.jpeg",strClientID];
        
    }else{
        strFileName     =   @"";
    }
    
    
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    
    MESSAGE(@"strUrl: %@ \n\n\nand strfilename: %@ \n\n\nandimageData: %@ \n\n\nand objDict:%@",strUrl,strFileName,imageData,objDict);

 
    //Craete object of service handeler
    ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
    
    //Invoke method for post images on server
    [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                 success:^(NSDictionary *responseDataDictionary) {
                                     
                                      MESSAGE(@"postClientsImageOnServer success dictionary: %@",responseDataDictionary);
                                     
                                     
                                     MESSAGE(@"responce from file: %@", responseDataDictionary);
                                     
                                     //Check
                                     if([responseDataDictionary objectForKey:METADATA]){
                                         
                                         
                                         NSMutableDictionary *obj = [[NSMutableDictionary alloc]init];
                                         [obj setValue:strClientID forKey:kClientId];
                                         
                                         //Invoke method for get All Signature image
                                         [self signatureImageOfClient:obj];
                                         
                                         
                                     }
                                     
                                     
                                     
                                     
                                 } failure:^(NSError *error) {
                                     
                                     
                                     //Hide the indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"Hide indicateor--------->");
                                     
                                     self.buttonHome.userInteractionEnabled  = YES;
                                     self.viewFields.userInteractionEnabled  = YES;
                                     
                                     [self.navigationController popViewControllerAnimated:YES];
                                     
                                     [commonUtility alertMessage:@"Error occurred. Please try again."];
                                     //Show Alert For error
                                     
                                 }];
    
    END_METHOD
}



//TODO: GEt clients signature on server

//Get client's image
-(void) signatureImageOfClient:(NSDictionary *)objDictClient{
    START_METHOD
    
  
    //Get the path of sig
        NSString * imgPath =[NSString stringWithFormat:@"%@/ClientsSignature/Signature%@.png",[FileUtility basePath],[objDictClient objectForKey:kClientId]];
        
    
    //Get the Image from the Image path
        UIImage * clientImage = [UIImage imageWithContentsOfFile:imgPath];
        NSData  *imageData;
        MESSAGE(@"All clients ID path: %@: and image: %@",imgPath,clientImage);
        
        
        if(clientImage == nil) {
            UIImage * imageTemp = nil;
            //Get Image data
            imageData   =   UIImageJPEGRepresentation(imageTemp, 1.0f);
        }else{
            
            //Get Image data
            imageData   =   UIImageJPEGRepresentation(clientImage, 1.0f);
            
        }
    
    //If  image then only send on server
    if(imageData != nil){
        
        //Invoke method for post the signature image on server
        [self postClientsSignatureOnServer:imageData andClientId:[objDictClient objectForKey:kClientId]];
        
    }else{
        
        //Get All client's Assessment Images
        [self getAllAssessmentImageInfo:[objDictClient objectForKey:kClientId]];
    }
    
}

//TODO: POST CLIENT's signature on server
-(void)postClientsSignatureOnServer:(NSData *)imageData andClientId:(NSString *)strClientID{
    START_METHOD
    
    
    //Craete Dictionry
    NSMutableDictionary *objDict            =   [[NSMutableDictionary alloc]init];
    
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN                             forKey:@"access_key"];
    [objDict setValue:@"signature_image"                            forKey:ACTION];
    [objDict setValue:strClientID                                   forKey:kClientId];
    [objDict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID]  forKey:@"user_id"];
    
    
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"Signature%@.jpeg",strClientID];
        
    }else{
        strFileName     =   @"";
    }
    MESSAGE(@"postClientsSignatureOnServer: %@ andimageData: %@ and dict:%@",strFileName,imageData,objDict);
    
    
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    
     
     //Craete object of service handeler
     ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
     
     //Invoke method for post images on server
     [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                  success:^(NSDictionary *responseDataDictionary) {
                                      
                                      MESSAGE(@"postClientsImageOnServer success dictionary: %@",responseDataDictionary);
                                      
                                      
                                      MESSAGE(@"responce from file: %@", responseDataDictionary);
                                      
                                      //Check
                                      if([responseDataDictionary objectForKey:METADATA]){
                                          
                                          
                                          NSMutableDictionary *obj = [[NSMutableDictionary alloc]init];
                                          [obj setValue:strClientID forKey:kClientId];
                                          
                                          //Get All client's Assessment Images
                                          [self getAllAssessmentImageInfo:[obj objectForKey:kClientId]];
                                          
                                          
                                      }
                                      
                                  } failure:^(NSError *error) {
                                      
                                      
                                      //Hide the indicator
                                      [TheAppController hideHUDAfterDelay:0];
                                      MESSAGE(@"Hide indicateor--------->");
                                      
                                      self.buttonHome.userInteractionEnabled  = YES;
                                      self.viewFields.userInteractionEnabled  = YES;
                                      
                                      [self.navigationController popViewControllerAnimated:YES];
                                      
                                      [commonUtility alertMessage:@"Error occurred. Please try again."];

                                      //Show Alert For error
                                      
                                  }];
    //Invoke method for
    
    
    END_METHOD
}


//Method for get all the assessment images info
-(void)getAllAssessmentImageInfo:(NSString *)strClientId{
    START_METHOD
    
    //Get all the assess ment image array
    NSArray *arrayAllAssessmentData = [NSArray arrayWithArray:[[DataManager initDB] getAllAssessmentImages:strClientId]];
    
    MESSAGE(@"Objdict asssessment arrayAllClients : %@",arrayAllAssessmentData);

    
    //If the assessment array has images
    if(arrayAllAssessmentData && arrayAllAssessmentData.count<1){
        
        //Dictionr y for get the info from the array
        NSMutableDictionary *objDict    =   [[NSMutableDictionary alloc]init];
        
        //Set the client ID  to the Dictionary
        [objDict setValue:strClientId forKey:kClientId];
        
        //Invoke method for update the client status Migrated YES
        [[DataManager initDB] updateClientMigrationStatus:objDict ];
        
        //Invoke Method for migrate more clients
        [self getAllDataFromLocalDbForMigration:[commonUtility retrieveValue:KEY_TRAINER_ID]];
        
    }
    
    //Loop for the Assessemnt array
    for (int i=0; i<arrayAllAssessmentData.count; i++) {
        
        //Get the assessment dict from the Array
        NSMutableDictionary  *objDict   =   [[arrayAllAssessmentData objectAtIndex:i] mutableCopy];
        [objDict setValue:strClientId forKey:kClientId];
        
        MESSAGE(@"Objdict asssessment : %@",objDict);
        
        NSInteger sheetId = [[objDict objectForKey:@"nSheetID"] integerValue];
        
        NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%ld",strClientId,(long)sheetId];
        NSString * dirPath = [NSString stringWithFormat:@"%@/%@/",[FileUtility basePath],imageFolder];
        NSString * strPath = [NSString stringWithFormat:@"%@%@.png",dirPath,[objDict objectForKey:kImageID]];
        
        
        UIImage * clientImage = [UIImage imageWithContentsOfFile:strPath];
        NSData  *imageData;
    
    
    if(clientImage == nil) {
        UIImage * imageTemp = nil;
        //Get Image data
        imageData   =   UIImageJPEGRepresentation(imageTemp, 1.0f);
    }else{
        
        //Get Image data
        imageData   =   UIImageJPEGRepresentation(clientImage, 1.0f);
        
    }
        
        MESSAGE(@"All clients assessment  path: %@: and image: %@ and imageData: %@",strPath,clientImage,imageData);

        //First check and if image data then only send
        if(imageData != nil){
            
            MESSAGE(@"arrayAllAssessmentData--> %lu and i =%d",(unsigned long)arrayAllAssessmentData.count,i);
            
            if(i== arrayAllAssessmentData.count-1){
            
                [objDict setValue:@"YES" forKey:@"next"];
            }
            
            //Invoke method for post on server
            [self postClientAssessmentImageOnServer:imageData andInfo:objDict];
            
            
        }else  if(i== arrayAllAssessmentData.count-1){
            
            
            //Invoke method for update the client status Migrated YES
            [[DataManager initDB] updateClientMigrationStatus:objDict ];
            
            //Invoke Method for migrate more clients
            [self getAllDataFromLocalDbForMigration:[commonUtility retrieveValue:KEY_TRAINER_ID]];
        }

    }
    
}


//TODO: POST CLIENT's signature on server
-(void)postClientAssessmentImageOnServer:(NSData *)imageData andInfo:(NSMutableDictionary *)objDict{
    START_METHOD
    
    MESSAGE(@"postClientAssessmentImageOnServer---objDict > %@",objDict);
    
    //Show indicator
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN                             forKey:@"access_key"];
    [objDict setValue:@"assessmentImage"                            forKey:ACTION];
    [objDict setValue:[commonUtility retrieveValue:KEY_TRAINER_ID]  forKey:@"user_id"];
    
    
    
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"assessment%@_%@.jpeg",[objDict objectForKey:kClientId],[objDict objectForKey:@"nSheetID"]];
        
    }else{
        strFileName     =   @"";
    }
    
    MESSAGE(@"postClientAssessmentImageOnServer: %@ andimageData: %@ nad objDict: %@",strFileName,imageData,objDict);
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    //Craete object of service handeler
    ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
    
    //Invoke method for post images on server
    [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                 success:^(NSDictionary *responseDataDictionary) {
                                     
                                     MESSAGE(@"postClientAssessmentImageOnServer success dictionary: %@",responseDataDictionary);
                                     
                                     //Check
                                     if([responseDataDictionary objectForKey:METADATA]){
                                         
                                         //For next Action
                                         if([objDict objectForKey:@"next"]){
                                             
                                             //Invoke method for update the client status Migrated YES
                                             [[DataManager initDB] updateClientMigrationStatus:objDict ];
                                             
                                             
                                             //Invoke Method for migrate more clients
                                             [self getAllDataFromLocalDbForMigration:[commonUtility retrieveValue:KEY_TRAINER_ID]];
                                             
                                         }
                                         
                                         
                                     }
                                     
                                 } failure:^(NSError *error) {
                                     
                                     
                                     //Hide the indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"Hide indicateor--------->");
                                     
                                     self.buttonHome.userInteractionEnabled  = YES;
                                     self.viewFields.userInteractionEnabled  = YES;
                                     
                                     [self.navigationController popViewControllerAnimated:YES];
                                     
                                     [commonUtility alertMessage:@"Error occurred. Please try again."];

                                     //Show Alert For error
                                     
                                 }];
    //Invoke method for
    
    
    
    END_METHOD
}



//Set the frame for the trainer prfile view
-(void)setTheFrame{
    START_METHOD

    CGRect frameConfirmButton           = self.buttonUpdate.frame;
    frameConfirmButton.origin.y         = self.textFiledConfirmPass.frame.origin.y+self.textFiledConfirmPass.frame.size.height+50;
    self.buttonUpdate.frame             =   frameConfirmButton;
    
    
    /*
    CGRect frameScrollView                    = self.scrollView.frame;
    frameScrollView.origin.y                  = self.scrollView.frame.size.height- 240;
    self.scrollView.frame               =   frameScrollView;
    
    self.textFiledConfirmEmail.hidden   =   YES;
    
    CGRect framePass                    =   self.textFiledPas.frame;
    framePass.origin.y                  =   self.textFiledConfirmEmail.frame.origin.y;
    self.textFiledPas.frame             =   framePass;
    
    
    CGRect frameConfirmPass             =   self.textFiledConfirmPass.frame;
    frameConfirmPass.origin.y           = self.textFiledPas.frame.origin.y+self.textFiledPas.frame.size.height+ 20;
    self.textFiledConfirmPass.frame     =   frameConfirmPass;
    
    
  
    
    */
    self.buttonFb.hidden                =   YES;
    self.lableOr.hidden                =   YES;
    
    END_METHOD
}


//Action method for the set the prfile pic
- (IBAction)actionSetProfilePic:(id)sender {
    START_METHOD

    //Invoke method for show the action list for upload the profile photo
    [self createActionSheet];
    
    END_METHOD
}



/*
 @Description : Method for create ActionSheet
 @Params      : UILongPressGestureRecognizer
 Return Value : void
 */
-(void)createActionSheet{
    START_METHOD
    
    //Show the pop up
    UIActionSheet *popUp = [[UIActionSheet alloc] initWithTitle:@"Action Sheet" delegate:self cancelButtonTitle:@"Cancel " destructiveButtonTitle:Nil otherButtonTitles:@"Take Photo",@"Open Gallary",nil];
    
    //Show the pop up for the show the Option
    [popUp showInView:self.view];
    END_METHOD
}
/*
 @Description : Delegaet Method for click button in ActionSheet
 @Params      : objects: alertView :buttonIndex
 Return Value : void
 */
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex{
    START_METHOD
    switch (buttonIndex)
    {
            //for  the open camera
        case 0:
            
            //Invoke method for the open the camera
            [self openCamera];
            break;
            
            //For Gallary
        case 1:
            
            //For open the gallary
            [self openGallery];
            break;
    }
    END_METHOD
}


#pragma mark - Image Picker

//Method for create the Image picker
-(void)createImagePicker
{
    START_METHOD
    _imgPickerController                = [[UIImagePickerController alloc]init];
    _imgPickerController.delegate       = self;
    END_METHOD
}

#pragma mark- Custom Methods

//-----------------------------------------------------------------------

//Method for the open camera
- (void) openCamera {
    START_METHOD
    
    //IF source avaialbel
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _imgPickerController = nil;
        if (_imgPickerController == nil) {
            _imgPickerController = [[UIImagePickerController alloc] init];
            [_imgPickerController setDelegate:(id)self];
        }
        
        [_imgPickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        [_imgPickerController setAllowsEditing:TRUE];
        [self performSelector: @selector(presentViewController) withObject: nil afterDelay: 0];
        
    }
    else {// If not source then show thje alert for this
        DisplayAlertWithTitle(NSLocalizedString(@"This device do not support camera", nil),kAppName);
    }
}

//-----------------------------------------------------------------------'

//Methos for open the gallary
- (void) openGallery {
    
    START_METHOD
    self.popoverController = nil;
    _imgPickerController = nil;
    if (_imgPickerController == nil) {
        _imgPickerController = [[UIImagePickerController alloc] init];
        [_imgPickerController setDelegate:(id)self];
    }
    _imgPickerController.sourceType =   UIImagePickerControllerSourceTypePhotoLibrary;
    [_imgPickerController setAllowsEditing:TRUE];
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:_imgPickerController];
    self.popoverController = popover;
    [self performSelector: @selector(presentModelController) withObject: nil afterDelay: 0];
    END_METHOD
}

//-----------------------------------------------------------------------

//Present the model controller
-(void)presentModelController{
    
    START_METHOD
    CGRect rect = CGRectMake(40, 40, 600, 600);
    [_popoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:0 animated:YES];
    
    END_METHOD
}

//-----------------------------------------------------------------------

//Method for persent the image picker view
-(void)presentViewController{
    START_METHOD
    
    [self presentViewController:_imgPickerController animated:YES completion:^{
    }];
    
    END_METHOD
    
}

#pragma mark - Delegates methods of UIImagePickerController

/*!
 @Description : Methods for set image on imageviw and get image data
 @Param       : :(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
 @Return      : void
 */
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    START_METHOD
    
    [self dismissViewControllerAnimated:YES  completion:Nil];
    [self.popoverController dismissPopoverAnimated:YES];
    
    UIImage *orgImage = info[UIImagePickerControllerEditedImage];

    [self.buttonProfileImage setImage:orgImage forState:UIControlStateNormal];
    
    //Invoke method to set original image becaoz without it rotate image 90 from server
    _imageData   = UIImageJPEGRepresentation(orgImage, 0.1);
    MESSAGE(@"_imageData : %@",_imageData);
    END_METHOD
    
    
}


//TODO : Make Round view

/*!
 @Description : Methods for make Login view shape circle
 @Param       : no
 @Return      : void
 */
-(void)makeRoundProfilePic
{
    START_METHOD
    //Make rounded
    self.buttonProfileImage.layer.cornerRadius = self.buttonProfileImage.frame.size.width/2 ;
    self.buttonProfileImage.clipsToBounds = YES;
    
    //Set border color
    self.buttonProfileImage.layer.borderColor = [UIColor colorWithRed:BORDER_COLOR green:BORDER_COLOR blue:BORDER_COLOR alpha:1.0].CGColor;
    
    //Set border width
    self.buttonProfileImage.layer.borderWidth = 1.5f;
    
    //Set border color
    self.buttonProfileImage.layer.borderColor = [UIColor colorWithRed:BORDER_COLOR green:BORDER_COLOR blue:BORDER_COLOR alpha:1.0].CGColor;
    
    //Set border width
    self.buttonProfileImage.layer.borderWidth = 1.5f;
    END_METHOD
}



//TODO: post Request For Save client profile Image On Server
-(void)postRequestForUploadTrinerProfilePic :(NSDictionary *)responseDataDict{
    
    MESSAGE(@"postRequestForUploadTrinerProfilePic--> %@",responseDataDict);
    UIImage * img = (UIImage *)[self.buttonProfileImage imageForState:UIControlStateNormal];
    
    
    NSData *imageData;
    
    if(img == nil){
        
        UIImage *imgTemp    =   nil;
        imageData       =   UIImageJPEGRepresentation(imgTemp, 1.0f);
    }else{
        
        imageData       =   UIImageJPEGRepresentation( img, 1.0f);
    }
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //Craete Dictionry
    NSMutableDictionary *objDict            =   [[NSMutableDictionary alloc]init];
    
    //Set Object
    [objDict setValue:USER_ACCESS_TOKEN     forKey:@"access_key"];
    [objDict setValue:@"userProfileImage"       forKey:ACTION];
    [objDict setValue:[responseDataDict objectForKey:@"id"]           forKey:@"user_id"];
    
    
    NSString *strFileName;
    
    //If image data
    if(imageData){
        strFileName    =   [NSString stringWithFormat:@"ProfileImage%@.jpeg",[responseDataDict objectForKey:@"user_id"]];
        
    }else{
        strFileName     =   @"";
    }
    
    
    
    //Url for send image
    NSString *strUrl =[NSString stringWithFormat:@"%@saveImageData",HOST_URL];
    
    
    MESSAGE(@"strUrl: %@ \n\n\nand strfilename: %@ \n\n\nandimageData: %@ \n\n\nand objDict:%@",strUrl,strFileName,imageData,objDict);
    
    
    if(imageData!=nil){
        
        
        //Craete object of service handeler
        ServiceHandler *objServiceHandeler  =   [[ServiceHandler alloc]init];
        
        //Invoke method for post images on server
        [objServiceHandeler postImageWithURl:strUrl parameters:objDict andImageData:imageData WithImageName:strFileName
                                     success:^(NSDictionary *responseDataDictionary) {
                                         
                                         if([responseDataDict objectForKey:@"move"] && [[responseDataDict objectForKey:@"move"] isEqualToString:@"YES"] ){
                                         
                                         //Get Data and save in loacl
                                         [self updateTrainerProfile:responseDataDictionary];
                                         
                                         
                                         //Move to last view
                                         [self.navigationController popViewControllerAnimated:YES];
                                         
                                         MESSAGE(@"userProfileImage success dictionary: %@",responseDataDictionary);
                                         
                                         //Show alert
                                         [commonUtility alertMessage:@"Profile successfully updated"];
                                    
                                         
                                         _buttonHome.userInteractionEnabled = NO;
                                         
                                         [TheAppController hideHUDAfterDelay:0.5];
                                             
                                             MESSAGE(@"Hide indicateor--------->");
                                         
                                         }
                                         
                                     } failure:^(NSError *error) {
                                         MESSAGE(@"Error");
                                         
                                         self.viewFields.userInteractionEnabled = YES;
                                         [self.buttonUpdate setTitle:@"Update" forState:UIControlStateNormal];
                                         
                                         self.viewShowLabel.hidden = YES;
                                         
                                         
                                         [commonUtility alertMessage:@"Error occurred. Please try again."];


                                         //Hide Indicator
                                         [TheAppController hideHUDAfterDelay:0];
                                         MESSAGE(@"Hide indicateor--------->");
                                         
                                     }];
    }else{
        
        //Hide Indicator
        [TheAppController hideHUDAfterDelay:0];
        MESSAGE(@"Hide indicateor--------->");
        
        return;
    }
    
    END_METHOD
}



//TODO: GEt client Profile image  from server
-(void)getTrainerImage{
    START_METHOD
    
    //All Clients with Deatils
    NSArray *arrayAllClients = [NSArray arrayWithArray:[[DataManager initDB] getTrainerProfile]];
    
    MESSAGE(@"arry trainer : %@",arrayAllClients);
    
    //
    if(arrayAllClients && arrayAllClients.count>0){
        
        //Get Dixtionry
        NSDictionary *dictProfile       = [commonUtility dictionaryByReplacingNullsWithStrings: [arrayAllClients objectAtIndex:0]];
        
        
    [TheAppController showHUDonView:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictProfile objectForKey:@"profile_image"]]];
    
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //Set Image
                    [self.buttonProfileImage setImage:image forState:UIControlStateNormal];
                    
                    //HIde the Indicator
                    [TheAppController hideHUDAfterDelay:0];
                    MESSAGE(@"Hide indicateor--------->");
                });
            }else{
                //HIde the Indicator
                [TheAppController hideHUDAfterDelay:0];
            }
        }else{
            //HIde the Indicator
            [TheAppController hideHUDAfterDelay:0];
            MESSAGE(@"Hide indicateor--------->");
        }
       
        
    }];
    [task resume];
    }
    
    END_METHOD
}

//Method for count the
-(void)getRemainingCount{
    START_METHOD
        MESSAGE(@"Hide indicateor--------->");
    
    //Get the array all the clients (Total clients)
    NSArray *arrayAllClients                    = [NSArray arrayWithArray:[[DataManager initDB] getTotalClientsCount]];
    
    //Get the array of the without migration
    NSArray *arrayAllWithoutMigrationClients    = [NSArray arrayWithArray:[[DataManager initDB] getClientsCountWithoutMigration]];
    
    
    MESSAGE(@"arrayAllClientsarrayAllClients : %@ and %@",arrayAllClients,arrayAllWithoutMigrationClients);
    
    //Take the str variable for the count
    NSString *strCount;
    
    //If there is clients for migration
       if (arrayAllClients && arrayAllClients.count>0 && arrayAllWithoutMigrationClients && arrayAllWithoutMigrationClients.count>0) {
           
        NSDictionary *dictTotalClient       = [arrayAllClients objectAtIndex:0];
        NSDictionary *dictNotMigratedClient = [arrayAllWithoutMigrationClients objectAtIndex:0];
        
           //Get the total clients count
        NSString  *strTotal                 = [dictTotalClient objectForKey:@"count"] ;
           
           //get the count which are not migrated
        NSString  *strNotMigrated           = [dictNotMigratedClient objectForKey:@"count"] ;
        
           //Set the message with the count of numbers
        strCount = [NSString stringWithFormat:@"%d / %d ",strNotMigrated.intValue,  strTotal.intValue];
    }
    
    
    //Set the lable text with the string count for show the progress of migragtion
    _lableProgress.text = strCount;
    
    END_METHOD
    
}


//Method for post request to send the message Email to the users
-(void)sendMessageForMail{
    START_METHOD
   
    //--->26 Dec
    NSString *strUser_type_old;
    
    //Get the App version with the current status
    NSString *strApp_version;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        
        //Set the user type
        strUser_type_old    =   @"YES";
        
        //Set the Version with the App name
        strApp_version       =   [NSString stringWithFormat:@"Full_%@",[commonUtility retrieveUpdatedVersion]];
        
    }else{
        
        //Set the user type
        strUser_type_old    =   @"NO";
        
        
        //Set the app version with the App name
        strApp_version       =   [NSString stringWithFormat:@"Light_%@",[commonUtility retrieveUpdatedVersion]];
        
        
        //Set USER FUll Version
        [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
        
    }
    
    MESSAGE(@"sendMessageForMail--> ");
    
    //Set the bundleidentifier for the know that app is full or lite in the device
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             USER_ID              :   [commonUtility retrieveValue:KEY_TRAINER_ID],
                             USER_TYPE_OLD        :  strUser_type_old,
                             @"app_version"       :  strApp_version,
                             @"bundleidentifier": [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]],

                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@SendEmailToUser",HOST_URL];
    
    
    MESSAGE(@"sendMessageForMail-> params=: %@ and sendMessageForMail Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"SendEmailToUser->responseDataDictionary: %@",responseDataDictionary);
                               
                               //Check
                               if([responseDataDictionary objectForKey:METADATA]){
                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   //Show Error Alert
                                   NSString *error= [dictError objectForKey:@"error"];
                                   
                                   //If not error
                                   if(error && error.intValue==0){
                                       
                                       //invoke method for Update DB
                                       [self copyDatabaseIfNeeded];
                                       
                                       //Set Upgrade yes
                                       [commonUtility saveValue:@"YES" andKey:KEY_UPGRADE_STATUS];
                                       
                                       //Set Auto Login: YES
                                       [commonUtility saveValue:@"YES" andKey:AUTO_LOGIN];
                                       
                                       //Set UserInteraction Enable YES
                                       self.buttonHome.userInteractionEnabled  =   YES;
                                       
                                       //Set the view fields user interaction enabled YES
                                       self.viewFields.userInteractionEnabled = YES;
                                       
                                       
                                       //Move to Sign In view
                                       [self moveToLoginView];
                                       
                                       MESSAGE(@"Hide indicateor--------->");
                                       
                                       
                                   }else{
                                       
                                       
                                       //Hide the indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                       MESSAGE(@"Hide indicateor--------->");
                                       
                                       //Set the iuser ibteratin enabled is YES
                                       self.buttonHome.userInteractionEnabled  = YES;
                                       self.viewFields.userInteractionEnabled  = YES;
                                       
                                       //If geeting the error , Move to the lat view
                                       [self.navigationController popViewControllerAnimated:YES];
                                       
                                       //Show the alert for the Erro
                                       [commonUtility alertMessage:@"Error occurred. Please try again."];
                                       //Show Alert For error
                                       
                                   }
                                   
                               }
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                               [commonUtility alertMessage:@"Error occurred. Please try again."];
                               
                               
                           }];
    END_METHOD
}


//Method for get the all the from the existing Db andcreate the Back up file
-(void)getAllDataFromLocalDbForMigrationForSaveInLocalFile{
    
    START_METHOD
    
    //Full Dictionary for make full json
    NSMutableDictionary     *objDictionary  =   [[NSMutableDictionary alloc]init];
    
    //Get All Question's category
    NSArray *arrayQuestionCategory = [NSArray arrayWithArray:[[DataManager initDB] selectQuestionCategory]];
    
    //Arry questions
    NSArray *arrayQuestion = [NSArray arrayWithArray:[[DataManager initDB] getAllQuestions]];
    
    //Template
    NSArray *arrayTemplates = [NSArray arrayWithArray:[[DataManager initDB] getAllTemplatesDetail]];
    
    
    //All Template Sheets (All Programs in all Template )
    NSArray *arrayTemplatePrograms = [NSArray arrayWithArray:[[DataManager initDB] getAllTemplatePrograms]];
    
    
    //All Template block data (All workouts in in all Template )
    NSArray *arrayWorloutsTemplates = [NSArray arrayWithArray:[[DataManager initDB] getBlockDataForTemplates]];
    
    
    //All Template Exercise (All Exercise in in all Template )
    NSArray *arrayExerciseTemplates = [NSArray arrayWithArray:[[DataManager initDB] getProgramTemplateLines]];
    
    //All Clients with Deatils
    NSArray *arrayAllClients = [NSArray arrayWithArray:[[DataManager initDB] getClientsDetail]];
    
    
    //Array for store all assessment
    NSMutableArray *arrayAllclientsWithinfo  =   [[NSMutableArray alloc]init];
    
    
    
    //Loop for all clients
    for (int i=0; i<arrayAllClients.count; i++) {
        
        //get Client Dictionary
        NSMutableDictionary  *dictClient   =  [[NSMutableDictionary alloc]initWithDictionary:[arrayAllClients objectAtIndex:i]];
        
        MESSAGE(@"arrayAllClients-> dictClient: %@",dictClient);
        
        
        //************************* Answers  *******************
        NSArray     *arrayAnswer    =   [NSArray arrayWithArray:[[DataManager initDB] getAllAnswersByClientID:[dictClient objectForKey:kClientId]]];
        
        MESSAGE(@"arrayAllClients-> arrayAnswer: %@",arrayAnswer);
        [dictClient setValue: arrayAnswer forKey:@"answers"];
        
        
        //************************* All Programs  *******************
        
        //Get all Programs
        NSArray *arrayPrograms =  [NSArray arrayWithArray:[[DataManager initDB] getProgramSheets:[dictClient objectForKey:kClientId] ]];
        
        MESSAGE(@"arrayAllClients-> arrayPrograms: %@",arrayPrograms);
        
        NSMutableArray *arrayProgram    =   [[NSMutableArray alloc]init];
        
        //For loop for get workout by serial
        for (int j=0;  j<arrayPrograms.count;j++) {
            
            //Create Dict for POst on server
            NSMutableDictionary *dictPrograms = [[NSMutableDictionary alloc]init];
            
            [dictPrograms setValue:[arrayPrograms objectAtIndex:j] forKey:@"program"];
            
            
            MESSAGE(@"arrayAllClients-> arrayPrograms: %@",dictPrograms);
            
            
            //************************* All Exercises  *******************
            //Get all Aseesments
            NSArray *arrayExercise =  [NSArray arrayWithArray:[[DataManager initDB] getAllExercises:[dictClient objectForKey:kClientId] andProgramId:[[arrayPrograms objectAtIndex:j] objectForKey:kSheetId]]];
            
            
            [dictPrograms setValue:arrayExercise forKey:@"exercise"];
            
            
            
            //Get all Notes
            NSArray *arrayNote =  [NSArray arrayWithArray:[[DataManager initDB] getAllNotesUnderProgram:[dictClient objectForKey:kClientId] andSheetId:[[arrayPrograms objectAtIndex:j] objectForKey:kSheetId]]];
            
            
            [dictPrograms setValue:arrayNote forKey:@"notes"];
            [arrayProgram addObject: dictPrograms];
            
            MESSAGE(@"dictClient==? %@",arrayPrograms);
        }
        
        //************************* All Aseesments  *******************
        
        
        //Get all Aseesments
        NSArray *arrayAssess =  [NSArray arrayWithArray:[[DataManager initDB] getAllAssessmentSheetsFor:[dictClient objectForKey:kClientId] ]];
        
        
        //Create Dict for POst on server
        NSMutableDictionary *dictAssessment = [[NSMutableDictionary alloc]init];
        
        [dictAssessment setValue:arrayAssess forKey:@"assessment"];
        
        //Add aseessment dict in array
        //        [arrayAssesmentSheets addObject:dictAssessment];
        
        //************************* All Aseesments  *******************
        
        
        //Get all Aseesments
        NSArray *arrayAssesmentSheetFullData =  [NSArray arrayWithArray:[[DataManager initDB] getAllAssessmentSheetFullData:[dictClient objectForKey:kClientId] ]];
        
        MESSAGE(@"Array Asseessemnt: %@",arrayAssesmentSheetFullData);
        
        NSMutableArray *arrayTemp   =   [[NSMutableArray alloc]init];
        if(arrayAssesmentSheetFullData && arrayAssesmentSheetFullData.count>0){
            
            //Set Dictionary
            NSMutableDictionary *dictTemp   =   [[NSMutableDictionary alloc]initWithDictionary:[arrayAssesmentSheetFullData objectAtIndex:0]];
            
            if([[dictClient objectForKey:kgender] isEqualToString:@"F"]){
                
                
                //SET VALUES FOR FEMALE
                [dictTemp setValue:[dictTemp objectForKey:@"fThree_Female_Triceps"] forKey:@"fThree_Male_Chest"];
                [dictTemp setValue:[dictTemp objectForKey:@"fThree_Female_Suprailiac"] forKey:@"fThree_Male_Abdomen"];
                [dictTemp setValue:[dictTemp objectForKey:@"fThree_Female_Thigh"] forKey:@"fThree_Male_Thigh"];
                
                [arrayTemp addObject:dictTemp];
                [dictAssessment setValue:arrayTemp forKey:@"assessmentFullData"];
            }else{
                [dictAssessment setValue:arrayAssesmentSheetFullData forKey:@"assessmentFullData"];
            }
            
            
        }
        
        [dictClient setValue:arrayProgram forKey:@"clientprogram"];
        [dictClient setValue:dictAssessment forKey:@"clientAsseessment"];
        [arrayAllclientsWithinfo addObject:dictClient];
        
        
    }
    
    
    
    NSString *strUser_type_old;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        
        //Set the user type
        strUser_type_old    =   @"YES";
        
    }else{
        
        //Set the user type
        strUser_type_old    =   @"NO";
        
        //Set USER FUll Version
//        [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
        
    }
    
    //Update the User type status
    [objDictionary setValue:strUser_type_old                    forKey:USER_TYPE_OLD];
    
    [objDictionary setValue:@"11"                    forKey:ID];
    [objDictionary setValue:arrayAllclientsWithinfo         forKey:@"clients"];
    
    //************************* DICTIONARY FOR ALL *******************
    
    //Set all array in dictionary for make JSON
    
    
    
    MESSAGE(@"cleicnt full dictonary : %@",objDictionary);
    
    
    //Questions & Answers
    [objDictionary setValue:arrayQuestionCategory   forKey:@"questionCategory"];
    [objDictionary setValue:arrayQuestion           forKey:@"allQuestions"];
    
    
    //Templates
    [objDictionary setValue:arrayTemplates          forKey:@"templates"];
    [objDictionary setValue:arrayWorloutsTemplates  forKey:@"workoutsTemplates"];
    [objDictionary setValue:arrayExerciseTemplates  forKey:@"exerciseTemplates"];
    [objDictionary setValue:arrayTemplatePrograms   forKey:@"templatePrograms"];
    
    //TODO: Write Text in text file
    
    //Carete objet of TextFileManager for write text
    TextFileManager *obj    =   [TextFileManager getSharedInstance];
    
    //Invoke method for write JSON in text file
    NSString *strFilePath   =   [obj writeTextToFile:objDictionary];
    
    
    //invoke methos for post file on server
    //    [self postRequestToMigrateAllData: strFilePath];
    
    
    
    NSURL *fileURL = [NSURL fileURLWithPath:strFilePath];
    
    
    
    NSLog(@"pathn to odf : %@", fileURL);
    if (fileURL) {
        
        if (![documentInteractionController presentPreviewAnimated:YES])
        {
            NSLog(@"presentPreviewAnimated--> YES");
            
            //present openIn
        }else{
            NSLog(@"presentPreviewAnimated--> NOT");
            
            
        }
        
        
        // Initialize Document Interaction Controller
        documentInteractionController= [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        
        // Configure Document Interaction Controller
        [documentInteractionController setDelegate:self];
        
        // Present Open In Menu
        [documentInteractionController presentOpenInMenuFromRect:[self.viewShowLabel frame] inView:self.view animated:YES];
        
        UIDocumentInteractionController *controller = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        controller.delegate = self;
        [controller presentPreviewAnimated:YES];
    }
    
    
    
    //profileImageOfClient
    //getAllAssessmentImagesForSheet
    MESSAGE(@"objDictionary---> for migration 1101: %@",objDictionary);
    
    END_METHOD
    
}

#pragma mark - UIDocumentInteractionControllerDelegate

//===================================================================
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    START_METHOD
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{START_METHOD
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{START_METHOD
    return self.view.frame;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    START_METHOD
    
}

//Migration with the Facebook
- (IBAction)actionButtonMigrationWithSignUp:(id)sender {
    START_METHOD
    
    
    //Create object for Logion manager for login into facebok
    FBSDKLoginManager   *login  = [[FBSDKLoginManager alloc] init];
    
    //Set Behavioir for Login by popup or Web view
    login.loginBehavior         = FBSDKLoginBehaviorWeb;
    
    //Set Permission for user
    [login logInWithReadPermissions: @[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        //Check If Error
        if (error) {
            
            [TheAppController hideHUDAfterDelay:0];
            MESSAGE(@"Process error");
            //Show Alert
            //            [CommonUtility alertMessage:OPERATION_FAILED];
            
            //Else if canceled by user
        } else if (result.isCancelled) {
            [TheAppController hideHUDAfterDelay:0];
            
            MESSAGE(@"Cancelled");
            
        } else {
            //if successfully Logged In invoke method for Access facebook info
            [self getFacebookUserInfo];
        }
        
    }];
    
    END_METHOD
}



//TODO: Get FB user info
-(void)getFacebookUserInfo{
    START_METHOD
    NSString *fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [TheAppController showHUDonView:nil];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields=id,name,email" parameters:nil] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                
                
                //If succes in getting info
                MESSAGE(@"result: %@",result);
                
                //Get info in Dictionary
                NSDictionary *dictFbUser =  (NSDictionary *)result;
                
                MESSAGE(@"dictFbUser: %@",dictFbUser);
                
                //If Facebook account has email id
                if([dictFbUser objectForKey:EMAIL] ){
                    
                    //Check Network connection
                    if([TheAppDelegate isNetworkAvailable]){
                        
                        //Invoke method for post Request for Migration
                        [self createDictionryTrainer:dictFbUser];
                        
                    }else{
                        
                        [TheAppController hideHUDAfterDelay:0];
                        
                        
                        [commonUtility alertMessage:@"Network not available!" ];
                    }
                    
                    
                    
                }else{//If Facebook account has NOT email id
                    
                    [TheAppController hideHUDAfterDelay:0];
                    
                    
                    //Craete alert message for if there is alredy the email id existing in the DB
                    NSString *alertMessage  =   NSLocalizedString(@"Sorry, There is no email id registered with your Facebook account. So, Please register yourself with Sign Up process.", nil);
                    
                    //Create alert for upgrade
                    UIAlertView *alertView      = [[UIAlertView alloc]initWithTitle:kAppName message:alertMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
                    
                    [alertView show];
                    
                }
                
                
            }else{
                
                //Hide the Indicatior
                [TheAppController hideHUDAfterDelay:0];
                
            }
        }];
    }
    END_METHOD
}

//Create the dictionry for the trainer profile for send for the migration process
-(void)createDictionryTrainer:(NSDictionary *)dictFb{
    START_METHOD
    
    _buttonHome.userInteractionEnabled = NO;
    
    //Get Upgarde Status
    NSString *strUpgradeStatus   =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //get User Id
    NSString *strUserId;
    
    //Check Upgraded User
    if ([strUpgradeStatus isEqualToString:@"YES"]) {
        
        //Set Trainer ID
        strUserId   =   [commonUtility retrieveValue:KEY_TRAINER_ID];
        
    }else{
        
        
        strUserId   =   @"0";
    }
    
    //    {"userID":"0","password":"vikash","email":"v1dey341111111111110@gmail.com","username":"vikash","user_type_old":"YES","app_version":"Full_4.0"}
    
    NSString *strUser_type_old  = @"NO";
    
    //Get the App version with the current status
    NSString *strApp_version;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        //Set the Version with the App name
        strApp_version       =   [NSString stringWithFormat:@"Full_%@",[commonUtility retrieveUpdatedVersion]];
    }else{
        
        //Set the app version with the App name
        strApp_version       =   [NSString stringWithFormat:@"Light_%@",[commonUtility retrieveUpdatedVersion]];
    }
    
    
    //If came from the FAcebook migration then enter this block
    if(dictFb ){
        
        //Generate the randome number for the craet the password
        int intRandomNumber = (arc4random()%10001) + 10000;
        
        MESSAGE("Random number: %u", intRandomNumber);
        
        //Create dictionaryb for send param in api for Trainer's Profile
        NSDictionary *params = @{
                                 USERNAME           :   [dictFb objectForKey: @"name"],
                                 PASSWORD           :   [NSString stringWithFormat:@"pass%d",intRandomNumber],
                                 EMAIL              :   [dictFb objectForKey: EMAIL],
                                 @"userID"          :   strUserId,
                                 USER_TYPE_OLD      :   strUser_type_old,
                                 @"app_version"     :   strApp_version,
                                 DEVICE_UDID        :   [commonUtility getUDID],
                                 @"migration"       :   @"fbMigration",
                                 @"facebook_id"     :[dictFb objectForKey: @"id"],
                                 @"bundleidentifier": [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]],
                                 };
        
        
        //Save the info pass using the Auto login process
        [commonUtility saveValue:[params objectForKey:PASSWORD] andKey:FB_PASSWORD];
        [commonUtility saveValue:[params objectForKey:EMAIL] andKey:FB_EMAIL];
        
        //Invoke method for Update Trainer's profile
        [self postRequestToServerForTrainerProfile:params];
        
    }else{//If came from the mannual migration process by entering the imnformation in the fields
        
        //Create dictionaryb for send param in api for Trainer's Profile
        NSDictionary *params = @{
                                 USERNAME           :   self.textFiledName.text,
                                 PASSWORD           :   self.textFiledPas.text,
                                 EMAIL              :   self.textFieldEmail.text,
                                 @"userID"          :   strUserId,
                                 USER_TYPE_OLD      :   strUser_type_old,
                                 @"app_version"     :   strApp_version,
                                 DEVICE_UDID        :   [commonUtility getUDID],
                                 @"migration"       :   @"YES",
                                 @"bundleidentifier": [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]],

                                 };
        
        //Invoke method for Update Trainer's profile
        [self postRequestToServerForTrainerProfile:params];
        
    }
    
    END_METHOD
}

@end
