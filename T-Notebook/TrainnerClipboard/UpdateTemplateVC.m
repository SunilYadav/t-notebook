//
//  UpdateTemplateVC.m
//  T-Notebook
//
//  Created by WLI on 29/06/15.
//  Copyright (c) 2015 WLI All rights reserved.
//
// Purpose : Update listing of program templates


#import "UpdateTemplateVC.h"

@interface UpdateTemplateVC () {
    
    __weak IBOutlet UIButton *btnUpdate;
}

@property(nonatomic,weak) IBOutlet UITextField *txtEnterName;
@property(nonatomic,weak) IBOutlet UILabel *lblTitle;



@end

@implementation UpdateTemplateVC

//-----------------------------------------------------------------------

#pragma mark - Memory Management Methods

//-----------------------------------------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//-----------------------------------------------------------------------

#pragma mark - Custom Method

//-----------------------------------------------------------------------

- (void) localizeControls {
    
    [btnUpdate setTitle:NSLocalizedString(btnUpdate.titleLabel.text,nil) forState:0];
    self.lblTitle.text = NSLocalizedString(self.lblTitle.text, nil);
}


//-----------------------------------------------------------------------

#pragma mark - View Life Cycle Methods

//-----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self localizeControls];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = false;
    btnUpdate.titleLabel.font = [UIFont fontWithName:@"TradeGothic" size:21];
    btnUpdate.layer.cornerRadius = 4.0;
    btnUpdate.backgroundColor = kAppTintColor;
    
    UILabel*lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width-135, 40)];
    lblTitle.text = NSLocalizedString(@"Edit Template Name", nil);
    lblTitle.font = [UIFont fontWithName:@"TradeGothic" size:21];
    lblTitle.textColor = kAppTintColor;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.adjustsFontSizeToFitWidth=YES;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView=lblTitle;
    
    
    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(btnCancelClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -15;
    UIBarButtonItem *leftBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, leftBtn, nil] animated:NO];
    
    self.txtEnterName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    self.txtEnterName.text = self.oldTemplateName;
}

//-----------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    START_METHOD
    [super viewWillAppear:animated];
    
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        if (iOS_7) {
            self.view.frame = CGRectMake(0, 0, kLandscapeWidth, kLandscapeHeight);
            self.navigationController.view.superview.frame = CGRectMake(0,0, 500, 160);
            self.navigationController.view.superview.center = self.view.center;
        }
    } else {
        if (iOS_7) {
            self.view.frame = CGRectMake(0, 0, kPotraitWidth, kPotraitHeight);
            self.navigationController.view.superview.frame = CGRectMake(0,0, 500, 160);
            self.navigationController.view.superview.center = self.view.center;
        }
    }
    
}

//-----------------------------------------------------------------------

-(void)btnCancelClicked{
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

//-----------------------------------------------------------------------

#pragma Mark - Action Methods

//-----------------------------------------------------------------------

- (IBAction)updateTemplateName :(id)sender {
    
    [self.txtEnterName resignFirstResponder];
    
    if (self.txtEnterName.text.length == 0 ) {
        DisplayAlertWithTitle(@"Please enter text.", kAppName)
    } else{
        [self.txtEnterName resignFirstResponder];
        [_delegate updateTemplateName:self.txtEnterName.text];
        self.txtEnterName.text = nil;
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
}

//-----------------------------------------------------------------------

@end
