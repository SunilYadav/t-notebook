 //
//  SignViewController.m
//  TrainnerClipboard
//
//  Created by Sunil Yadav on 02/08/16.
//  Copyright © 2016 WiOS Monika R. Brahmbhatt. All rights reserved.
//

//Facebook library
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FacebookSDK/FacebookSDK.h>

#import "AFNetworking.h"
#import "ViewController.h"
#import "AddNameVC.h"

#import "SignViewController.h"

@interface SignViewController ()<AddNameDelegate>{
    
    //Take textfiled for identify active textfield
    UITextField                 *textfiledActive;
}


//Buttons
@property (weak, nonatomic) IBOutlet UIView     *viewSignUp;
@property (weak, nonatomic) IBOutlet UIView     *viewSignIn;

//Login Textfields
@property (weak, nonatomic) IBOutlet UITextField *textFiledEmailLogin;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassLogin;

//Sign Up Textfileds
@property (weak, nonatomic) IBOutlet UITextField *textFiledName;
@property (weak, nonatomic) IBOutlet UITextField *textFiledEmailSignUp;
@property (weak, nonatomic) IBOutlet UITextField *textFiledPassSignUp;
@property (weak, nonatomic) IBOutlet UITextField *textFiledConfmPass;
@property (weak, nonatomic) IBOutlet UITextField *textFiledConfirmEmail;


//Buttons
@property (weak, nonatomic) IBOutlet UIButton   *buttonSignIn;
@property (weak, nonatomic) IBOutlet UIButton   *buttonSignUp;
@property (weak, nonatomic) IBOutlet UIButton   *buttonForgot;
@property (weak, nonatomic) IBOutlet UIButton   *buttonActionSign;



@end

@implementation SignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MESSAGE_TEST(@"SignViewController- viewidiload");
    MESSAGE(@"SignViewController- viewidiload MEssage");
    
    START_METHOD
    
    //iNvoke method for monitor network
    [self monitorInternetConnection];
    
    
    //Set buttons background color
    [self.buttonSignIn setBackgroundColor:kOldSkin];
    [self.buttonSignUp setBackgroundColor:[UIColor lightGrayColor]];
 
    //Invoke method for set navigation button
     [self setNavigationBarButton];
        MESSAGE_TEST(@"SignViewController- viewidiload  end");
//    [self postLocalDbToServer];
   
    END_METHOD
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
    // Dispose of any resources that can be recreated.
}


//method for check logged status
-(void)viewWillAppear:(BOOL)animated{
    START_METHOD
    MESSAGE_TEST(@"viewWillAppear-->start");
    
    //GEt Login status
    NSString *strLoginStatus        =   [commonUtility retrieveValue:KEY_LOGIN_STATUS];

    //Get User ID
    NSString *strUpgradeStatus      =   [commonUtility retrieveValue:KEY_UPGRADE_STATUS];
    
    //All Clients with Deatils
    NSArray *arrayAllClients = [NSArray arrayWithArray:[[DataManager initDB] getClients]];

    
    MESSAGE_TEST(@"viewWillAppear-strUpgradeStatus->? %@ and count: %lu and strLoginStatus: %@",strUpgradeStatus,(unsigned long)arrayAllClients.count,strLoginStatus);
    
    //FOR OLD CLIENTS:  All Clients count Gfreater then 0 AND not upgrareded user
    if(arrayAllClients.count>0 && ![strUpgradeStatus isEqualToString:@"YES"]){
        
        MESSAGE_TEST(@"viewWillAppear--//FOR OLD CLIENTS:-- block");
        
        
        //For Old User, He can Add any number of clients for full version
        [commonUtility saveValue:@"YES" andKey:KEY_STATUS_ADD_CLIENT];

        //For Old User, Which is not upgraded
        [commonUtility saveValue:@"YES" andKey:KEY_OLD_USER];

        
        //MOve to home screen
        [self moveToHomeScreen];
        
        return;
    }
    
    //if login then move to Home Screen AND if there is no user ID means it is old user with without upgraded account
    if([strLoginStatus isEqualToString:@"YES"]){
        
        MESSAGE_TEST(@"viewWillAppear--//FOR LOGIN CLIENTS:-- block");
        
        //MOve to home screen
        [self moveToHomeScreen];
        
    }else{
    

//TODO: Came from migration
    
    //GEt Login status
    NSString *strTrainerId        =   [commonUtility retrieveValue:KEY_TRAINER_ID];
    NSString *strAutoLogin        =   [commonUtility retrieveValue:AUTO_LOGIN];
    
    MESSAGE_TEST(@"sign in 137:  strTrainerId===> %@ and strLoginStatus: %@ and strAutoLogin: %@",strTrainerId,strLoginStatus,strAutoLogin);
    
    if(strTrainerId && strTrainerId.length>0 && strTrainerId.intValue >0 && strAutoLogin && [strAutoLogin isEqualToString:@"YES"]){
        
        MESSAGE_TEST(@"sign in 141: strTrainerId===> %@ ",strTrainerId);
        
        [TheAppController showHUDonView:nil];
       
        //Create dictionaryb for send param in api for Trainer's Profile
        NSDictionary *params = @{
                                 EMAIL              :  [commonUtility retrieveValue:EMAIL],
                                 PASSWORD           :   [commonUtility retrieveValue:PASSWORD],
                                 DEVICE_UDID        :   [commonUtility getUDID],
                                 };
        
        //Invoke method for post request for Auto Login
        [self postRequestToServerForSignIn:params];
        
    }else{
        [TheAppController hideHUDAfterDelay:0];
    }
    
    }
    
    END_METHOD
}


#pragma mark - TextFiled Delegate

//Deleagte method for Resign keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    START_METHOD
    
    //Resign Keyboard
    [textField resignFirstResponder];
    return YES;
    END_METHOD
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    START_METHOD
    
    //Set the active textfiled
    textfiledActive     =   textField;
    
    return YES;
    
    END_METHOD
}
#pragma mark - Action Methods

//Method for show the Sign In view and hide the Sign up view
- (IBAction)actionShowSignInView:(id)sender {
    START_METHOD
    //Show the Login View
    self.viewSignIn.hidden = NO;
    
    ////Hide the Sign Up view
    self.viewSignUp.hidden = YES;
    
    //Set buttons background color
    [self.buttonSignIn setBackgroundColor:kOldSkin];
    [self.buttonSignUp setBackgroundColor:[UIColor lightGrayColor]];

    END_METHOD
}

///Method for Show sign up
- (IBAction)actionShowSignUpView:(id)sender {
    START_METHOD
    
    //Hide the Sign in view
    self.viewSignIn.hidden      = YES;
    
    //Shoe the Sign Up view
    self.viewSignUp.hidden      = NO;
    
    //Set buttons background color
    [self.buttonSignUp setBackgroundColor:kOldSkin];
    [self.buttonSignIn setBackgroundColor:[UIColor lightGrayColor]];
    
    END_METHOD
}


//Action methods for the Sign up
- (IBAction)actionSignUp:(id)sender {
    START_METHOD
    
    //Resign KEyboard
    if([textfiledActive isFirstResponder]){
    
        [textfiledActive resignFirstResponder];
    }
  
    //Invoke method for valodate the textfiedls
    if ([self validateTextFieldValuesForSignUp]) {
        
     
        //Check Network connection
        if([TheAppDelegate isNetworkAvailable]){
            
            //Invoke method for post Request for Sign Up
            [self postRequestToServerForSignUp];
            
        }else{
           
            [commonUtility alertMessage:@"Network not available!" ];

        }
      
    }
    END_METHOD
}

//Action method for Sign In
- (IBAction)actionSignIn:(id)sender {
    
    START_METHOD
    
    //Resign KEyboard
    if([textfiledActive isFirstResponder]){
        
        [textfiledActive resignFirstResponder];
    }
    
    
    //Invoke method for valodate the textfiedls
    if ([self validateTextFieldValuesForSignIn]) {
        
        //Check Network connection
        if([TheAppDelegate isNetworkAvailable]){
            
            
            //Create dictionaryb for send param in api for Trainer's Profile
            NSDictionary *params = @{
                                     EMAIL              :   self.textFiledEmailLogin.text,
                                     PASSWORD           :   self.textFieldPassLogin.text,
                                     DEVICE_UDID        :   [commonUtility getUDID],
                                     @"bundleidentifier": [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]],
                                     @"deviceInfo"      : [NSString stringWithFormat:@"%@",[commonUtility getDeviceInfo]],

                                     };
            
            
            //Invoke method for post Request for Sign IN
            [self postRequestToServerForSignIn:params];
            
        }else{
        
            [commonUtility alertMessage:@"Network not available!" ];
        }
   
    }
    
    
    END_METHOD
}

//Action for Forgot the password
- (IBAction)actionForgotPassword:(id)sender {
    START_METHOD
    
     AddNameVC *objAddNameVC        = (AddNameVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"AddNameView"];
     objAddNameVC.delegate          = self;
    
     objAddNameVC.strScreenName     = @"EnterTrainerEmail";
     objAddNameVC.strFiledContent   = nil;
    
     UINavigationController *modalController    = [[UINavigationController alloc]initWithRootViewController:objAddNameVC];
     modalController.modalTransitionStyle       = UIModalTransitionStyleFlipHorizontal;
     modalController.modalPresentationStyle     =  UIModalPresentationFormSheet;
    
    //Check the ios version
     if (iOS_8) {
     CGPoint frameSize = CGPointMake(350, 160);
     modalController.preferredContentSize = CGSizeMake(frameSize.x, frameSize.y);
     }
     
     [self.navigationController presentViewController:modalController animated:YES completion:^{
     
     }];
     
    END_METHOD
}

#pragma mark -  delegate for Forgot Password
-(void)requestForPassword:(NSString *)strEmail{
    START_METHOD
    
        //Check Network connection
        if([TheAppDelegate isNetworkAvailable]){
            
            //Invoke method for post Request for Sign IN
            [self postRequestForForgotPasssword:strEmail];
            
        }else{
            
            [commonUtility alertMessage:@"Network not available!" ];
        }
    END_METHOD
}


//Post request for Forgot pass
-(void)postRequestForForgotPasssword:(NSString *)strEmail{
    START_METHOD
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             @"email_address"              :   strEmail,
                             @"bundleidentifier": [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]],

                             
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@forgetPassword",HOST_URL];
    
    
    MESSAGE(@"requestForPassword-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   //Show Alrt
                                   UIAlertView *objAlert    =   [[UIAlertView alloc]initWithTitle:kAppName message:@"\nPassword has been sent to your registered Email ID. \n\nPlease check your registered Email ID." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                   [objAlert show];
                                   
                                   
                               }else{//If Not existed Email ID
                                   
                                   //Check
                                   if([responseDataDictionary objectForKey:METADATA]){
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       
                                       //Show Alrt
                                       UIAlertView *objAlert        =   [[UIAlertView alloc]initWithTitle:kAppName message:[NSString stringWithFormat:@"\n%@\n\nPlease try again with your registered Email ID.",[dictError objectForKey:POPUPTEXT]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                       [objAlert show];
                                       
                                       
                                   }
                               }
                               
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                                [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                               //Save Login Status
                               [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
                               
                               //
                               
                               
                           }];
    END_METHOD

}

#pragma mark - Validate Textfields
/*!
 @Description : Methods for Validate Textfiled
 @Param       :
 @Return      :
 */
-(BOOL)validateTextFieldValuesForSignIn{
    START_METHOD
    
    //Check Email text  Filed blank or not
    if([self.textFiledEmailLogin.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter primary email address.", nil)];
        return NO;
    }
    
    //Check user name for Character only
    else if(![commonUtility validateEmailWithString:self.textFiledEmailLogin.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Email not valid", nil)];
        return NO;
    }
  
    
    //Check User name Empty
    else if([self.textFieldPassLogin.text  isEqualToString:NULL_STRING])    {
        [commonUtility alertMessage:@"Please enter the password"];
        return NO;
    }
  
    else
        return YES;
    END_METHOD
}




#pragma mark - Validate Textfields
/*!
 @Description : Methods for Validate Textfiled
 @Param       :
 @Return      :
 */
-(BOOL)validateTextFieldValuesForSignUp{
    START_METHOD
    
    //Check Email text  Filed blank or not
    if([self.textFiledName.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter first name.", nil)];
        return NO;
    }
    
    
    //Check user name for Character only
    else if(![commonUtility validateName:self.textFiledName.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Name must be only character", nil)];
        return NO;
    }
    
    
    //Check Email text  Filed blank or not
   else if([self.textFiledEmailSignUp.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter primary email address.", nil)];
        return NO;
    }
    
    //Check user name for Character only
    else if(![commonUtility validateEmailWithString:self.textFiledEmailSignUp.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Primary Email not valid", nil)];
        return NO;
    }
    
    
    
    
    //Check Email text  Filed blank or not
    else if([self.textFiledConfirmEmail.text  isEqualToString:NULL_STRING]) {
        
        //Show Alert for enter email address
        [commonUtility alertMessage:NSLocalizedString(@"Please enter confirm email address.", nil)];
        return NO;
    }
    
    //Check user name for Character only
    else if(![commonUtility validateEmailWithString:self.textFiledConfirmEmail.text])    {
        [commonUtility alertMessage:NSLocalizedString(@"Confirm Email not valid", nil)];
        return NO;
    }
    
    
  
    
    //Check Phone textfiled Empty
    else if(![self.textFiledEmailSignUp.text isEqualToString:self.textFiledConfirmEmail.text])    {
        [commonUtility alertMessage:@"Primary email and confirm email must be same!"];
        return NO;
    }
    
    
  
    //Check User name Empty
    else if([self.textFiledPassSignUp.text  isEqualToString:NULL_STRING])    {
        [commonUtility alertMessage:@"Please enter the password"];
        return NO;
    }
    
    //Check Phone textfiled Empty
    else if([self.textFiledConfmPass.text isEqualToString:NULL_STRING])    {
        [commonUtility alertMessage:@"Please enter the confirm password"];
        return NO;
    }
    
   
    
    //Check Phone textfiled Empty
    else if(![self.textFiledConfmPass.text isEqualToString:self.textFiledPassSignUp.text])    {
        [commonUtility alertMessage:@"Password and confirm password must be same!"];
        return NO;
    }
    else
        return YES;
    END_METHOD
}


#pragma  mark -  Added Methods

/*!
 @Description : Methods for check Internet connection monitor Internet Connection
 @Param       :
 @Return      :
 */
- (void) monitorInternetConnection
{
    START_METHOD
    
    //use for network flag
    AppDelegate *objAppDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    
    //USe for monitor network
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {   // -- Reachable -- //
                MESSAGE_TEST(@"Reachable------>");
                
                //SET YES FOR ACCESS
                objAppDelegate.isNetworkAvailable = true;
                
            }
                break;
            case AFNetworkReachabilityStatusNotReachable:
            {   // -- Not reachable -- //
                MESSAGE_TEST(@"Not Reachable------?");
                [commonUtility alertMessage:@"Network Not Available"];
                objAppDelegate.isNetworkAvailable = false;
                
                //Hide Indicator if not Network
                [TheAppController hideHUDAfterDelay:0];
            }
                break;
            default:break;
                
        }
    }];
    
    END_METHOD
}


//TODO: SERVER HANDLER


//Method for post request for Sign Up
-(void)postRequestToServerForSignUp{
    START_METHOD
    
    
    //Get the App version
    NSString *strApp_version;
    
    
    //Check if full version
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kisFullVersion] isEqualToString:@"Y"]) {
        
        
        //Set the Version with the App name
        strApp_version       =   [NSString stringWithFormat:@"Full_%@",[commonUtility retrieveUpdatedVersion]];
    }else{
        
        //Set the app version with the App name
        strApp_version       =   [NSString stringWithFormat:@"Light_%@",[commonUtility retrieveUpdatedVersion]];
        
        
    }
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             USERNAME           :   self.textFiledName.text,
                             PASSWORD           :   self.textFiledPassSignUp.text,
                             EMAIL              :   self.textFiledEmailSignUp.text,
                             @"userID"          :   @"0",
                             USER_TYPE_OLD      :   @"NO",
                             @"app_version"    :   strApp_version,
                             DEVICE_UDID        :   [commonUtility getUDID],
                             @"signUp"           : @"YES",
                             @"bundleidentifier": [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]],

                             
                             };
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@trainerProfile",HOST_URL];
    
    MESSAGE_TEST(@"params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
     
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce: %@", responseDataDictionary);
                               
                               
                               //Invoke method for take action on update profile
                               [self takeActionOnSignUp:responseDataDictionary];
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                               [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                               //Save Login Status
                               [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
                               
                           }];
    END_METHOD
}



//Method for post request for SIGNIN
-(void)postRequestToServerForSignIn:(NSDictionary *)params{
    START_METHOD
   
    
   
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@login",HOST_URL];
    
    
    MESSAGE_TEST(@"postRequestToServerForSignIn -> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrl:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE_TEST(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                               
                               //Check
                               if([responseDataDictionary objectForKey:METADATA]){
                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   //Show Error Alert
                                   NSString *error= [dictError objectForKey:@"error"];
                                   
                                   
                                   //If there is no error
                                   if(error && error.intValue==0){
                                       
                                       //Set Auto Login : NO
                                       [commonUtility saveValue:@"NO" andKey:AUTO_LOGIN];
                                       
                                       //Invoke method for handle responce
                                       [self takeActionOnLogin:responseDataDictionary];
                                       
                                   }else{//If getting the erroe from the server show the alert to the user
                                       
                                       //Get Dictionary
                                       NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                       
                                       //Show Error Alert
                                       
                                       //create alertView
                                       UIAlertView *alertMessage = [[UIAlertView alloc]
                                                                    initWithTitle:@"" message:[NSString stringWithFormat:@"%@",[dictError objectForKey:POPUPTEXT]]
                                                                    delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                       //Show alertView
                                       [alertMessage show];
                                       
                                       
                                       MESSAGE(@"Eror : %@",error);
                                       
                                       
                                       //Hide the indicator
                                       [TheAppController hideHUDAfterDelay:0];
                                   }
                               }
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                               [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                               //Save Login Status
                               [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
                               
                               //
                               
                               
                           }];
    END_METHOD
}



#pragma mark - Handle Response fromServer

//Method for Take action on update profile
-(void)takeActionOnSignUp: (NSDictionary *)responseDataDictionary{
    
    //Get Upgarde Status
    
    MESSAGE(@"responce: %@", responseDataDictionary);
    
    //If Success
    if([responseDataDictionary objectForKey:PAYLOAD]){
        //************  GET Trainer's Profile AND SAVE IN LOCAL DB
        NSDictionary *dictTrainerProfile =  [ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] ;
        MESSAGE(@"arrayUserData : dictTrainerProfile: %@",dictTrainerProfile);
        
        //Get Data and save in loacl
        [self saveTrainerProfile:dictTrainerProfile];
        
        
        
        //Show Alert
        [commonUtility alertMessage:@"Congratulations!\n You are successfully registered"];
        
        //Save Upgrade Status
        [commonUtility saveValue:@"YES" andKey:KEY_UPGRADE_STATUS];
        
        
        //Save Login Status
        [commonUtility saveValue:@"YES" andKey:KEY_LOGIN_STATUS];
        
        //For make Free version at time of Sign UP
        [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:kisFullVersion];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //Get Dictionary
        NSDictionary   *dictProfileData         =   [commonUtility dictionaryByReplacingNullsWithStrings:dictTrainerProfile];
        
        MESSAGE(@"dictProfileData->hsdghs  %@",dictProfileData);
        
        
        
        if([dictProfileData objectForKey:kisFullVersion] && [[dictProfileData objectForKey:kisFullVersion] isEqualToString:@"YES"]){
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
        }
        
        
        //Hide Indicator
        [TheAppController hideHUDAfterDelay:0];
        
        //Invoke method for move to Home Screen
        [self moveToHomeScreen];
        
        
    }else{//If Server respose a Error
        
        //Check
        if([responseDataDictionary objectForKey:METADATA]){
            
            //Get Dictionary
            NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
            
            //Show Error Alert
            [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
            
            //Hide Indicator
            [TheAppController hideHUDAfterDelay:0];
            
            
            
            //For Unauthorized user --->
            if( [dictError objectForKey:LIST_KEY]){
                
                NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                
                if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                    
                    //Show Error Alert
                    [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                    
                    
                    //Move to dashbord for login
                    [commonUtility  logOut];
                }
                
            }
            
            //<-----
        }
    }
    
    
}

//TODO; //LOCAL DB
-(void)saveTrainerProfile:(NSDictionary *)responseDataDictionary{
    START_METHOD
 
    
        //Get Dictionary
        NSDictionary   *dictProfileData         =   [commonUtility dictionaryByReplacingNullsWithStrings:responseDataDictionary];
    
        MESSAGE(@"Dict profile : %@",dictProfileData);
    
    
        //Save Trainer Id
        [commonUtility saveValue:[dictProfileData objectForKey:ID] andKey:KEY_TRAINER_ID];
        [commonUtility saveValue:[dictProfileData objectForKey:@"sFirstName"] andKey:kTrainerName];
    
        //Set the user Type old or new
        [commonUtility saveValue:[dictProfileData objectForKey:USER_TYPE_OLD] andKey:KEY_OLD_USER];
    
    
        //Save the no. of client Plan
        [commonUtility saveValue:[dictProfileData objectForKey:TOTAL_CLIENT] andKey:TOTAL_CLIENT];
    
        //If User type new then Full Access to user
        if([[dictProfileData objectForKey:USER_TYPE_OLD] isEqualToString:@"NO"]){
        
        //Set YES for  full acccess
        [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
      
    }else{
        //Save the no. of client Plan
        [commonUtility saveValue:@"0" andKey:TOTAL_CLIENT];
    }
    
    //Insert Trainer Values
        int intValue = [[DataManager initDB] insertTrainerProfiles:dictProfileData];
        
        MESSAGE(@"insertTrainerProfiles-> =====: %d ",intValue);
        
    
    END_METHOD
}



#pragma mark - Handle Response fromServer

//Method for Take action on update profile
-(void)takeActionOnLogin: (NSDictionary *)responseDataDictionary{
    
    
    MESSAGE(@"takeActionOnLogin: -> responce: %@", responseDataDictionary);
    
    //If Success
    if([responseDataDictionary objectForKey:PAYLOAD]){
        
        
        //************  GET Trainer's Profile AND SAVE IN LOCAL DB
        NSString *strStaus =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"uploadSqliteFile"];
         MESSAGE(@"uploadSqliteFile---> %@",strStaus);
        if(strStaus && strStaus.length>1 && [strStaus isEqualToString:@"YES"]){
            
           
            //Mail the sqlite file
            [self postLocalDbToServer];
        }
        
        
        
        //************  GET Trainer's Profile AND SAVE IN LOCAL DB
        NSString  *strAccessKey         =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:KEY_ACCESS];


        //Save Access key
        [commonUtility saveValue:strAccessKey andKey:KEY_ACCESS];
        
        
        
        //************  GET Trainer's Profile AND SAVE IN LOCAL DB
        NSDictionary *dictTrainerProfile =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"trainer_profile"];
        MESSAGE(@"arrayUserData : dictTrainerProfile: %@",dictTrainerProfile);
        
        
        //Save Clients
        [self saveTrainerProfile:dictTrainerProfile];
        
        
        //Invoke method for get The Add clients Status
        [self checkAddClientStatus];
        
       
        //************  GET CLEINTS LIST AND SAVE IN LOCAL DB
        NSArray *arrayclients           =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"client_list"];
        MESSAGE(@"arrayUserData : %@",arrayclients);
      
        
        //Save Clients
        [self saveClients:arrayclients];
       
        
        //************  GET CLEINTS ANSWERS AND SAVE IN LOCAL DB
        NSArray *arrayclientsAnswers    =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"answer_list"];
        MESSAGE(@"arrayUserData arrayclientsAnswers: %@",arrayclientsAnswers);
        
        //Save Client's Answers
        [self saveClientsAnswers:arrayclientsAnswers];
        
        
        
        //************  GET CLEINT'S PROGRAMS LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsprogram    =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"program_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsprogram: %@",arrayclientsprogram);
        
        //Save Client's program
        [self saveClientsProgram:arrayclientsprogram];
        
        
         //************  GET CLEINT'S Workouts and NOTES LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsWorkoutsAndNotes =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"workout_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsWorkoutsAndNotes: %@",arrayclientsWorkoutsAndNotes);
        
        //Save Clients workouts and notes
        [self saveClientsWorkoutsAndNotes:arrayclientsWorkoutsAndNotes];
        
        
    
        //************  GET CLEINT'S Exercise LIST AND SAVE IN LOCAL DB
        NSArray *arrayclientsExercise   =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"exercise_list"];
        
        MESSAGE(@"arrayUserData -> arrayclientsExercise: %@",arrayclientsExercise);
        
        //Save Client's exercise
        [self saveClientsExercise:arrayclientsExercise];
      
      
        //#************  GET CLEINT'S Assessment and SAVE IN LOCAL DB
        NSArray *arrayclientAssessment  =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"clientAsseessment"];
        
        MESSAGE(@"arrayUserData -> arrayclientAssessment: %@",arrayclientAssessment);
        
        //Save Client's asseessments
        [self saveClientsAssessment:arrayclientAssessment];
        
        
        
        //#************  GET CLEINT'S Assessment DATA and SAVE IN LOCAL DB
        NSArray *arrayclientAssessmentData  =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"assessmentFullData"];
        
        MESSAGE(@"arrayUserData -> arrayclientAssessmentData: %@",arrayclientAssessmentData);
        
        //Save Client's asseessments
        [self saveClientsAssessmentData:arrayclientAssessmentData];
        
        
        
        
        //#************  GET CLEINT'S TEMPLATES DATA and SAVE IN LOCAL DB
        NSArray *arrayTemplate              =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"templates"];
        
        MESSAGE(@"arrayUserData -> templates: %@",arrayTemplate);
        
        //Save Client's asseessments
        [self saveClientsTemplates:arrayTemplate];
    
        
        
        //#************  GET CLEINT'S TEMPLATE's EXERCISE DATA and SAVE IN LOCAL DB
        NSArray *arrayTemplateExercise      =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"exerciseTemplates"];
        
        MESSAGE(@"arrayUserData -> arrayTemplateExercise: %@",arrayTemplateExercise);
        
        //Save Client's asseessments
        [self saveClientsTemplatesExercise:arrayTemplateExercise];
        
        
        
        //#************  GET CLEINT'S TEMPLATE's PROGRAM DATA and SAVE IN LOCAL DB
        NSArray *arrayTemplatePrograms      =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"templatePrograms"];
        
        MESSAGE(@"arrayUserData -> arrayTemplatePrograms: %@",arrayTemplatePrograms);
        
        //Save Client's asseessments
        [self saveClientsTemplatesProgram:arrayTemplatePrograms];
        
        
        
        //#************  GET CLEINT'S TEMPLATE's Workouts DATA and SAVE IN LOCAL DB
        NSArray *arrayTemplateWorkouts      =  [[ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] objectForKey:@"workoutsTemplates"];
        
        MESSAGE(@"arrayUserData -> arrayTemplateWorkouts: %@",arrayTemplateWorkouts);
        
        //Save Client's asseessments
        [self saveClientsTemplatesWorkouts:arrayTemplateWorkouts];
        
        
        //Show alert on successfully Login
        [commonUtility alertMessage:@"Login Successful"];
        
        //Save Login Status
        [commonUtility saveValue:@"YES" andKey:KEY_LOGIN_STATUS];
        
        //Save Upgrade Status
        [commonUtility saveValue:@"YES" andKey:KEY_UPGRADE_STATUS];
        
        //Hide the indicator
        [TheAppController hideHUDAfterDelay:0];
        
        
        //Get Dictionary
        NSDictionary   *dictProfileData         =   [commonUtility dictionaryByReplacingNullsWithStrings:dictTrainerProfile];
        
        if([dictProfileData objectForKey:kisFullVersion] && [[dictProfileData objectForKey:kisFullVersion] isEqualToString:@"YES"]){
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:kisFullVersion];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        //Invoke method for move to Home view
        [self moveToHomeScreen];
        
        
    }else{//If Server respose a Error
        
        //Check
        if([responseDataDictionary objectForKey:METADATA]){
            
            //Get Dictionary
            NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
            
            //Show Error Alert
            [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
            
            //Hide Indicator
            [TheAppController hideHUDAfterDelay:0];
            
            
            //For Unauthorized user --->
            if( [dictError objectForKey:LIST_KEY]){
                
                NSDictionary *objDict    =   [dictError objectForKey:LIST_KEY];
                
                if(objDict && [[objDict objectForKey:STATUS] isEqualToString:@"false"]){
                    
                    //Show Error Alert
                    [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                    
                    
                    //Move to dashbord for login
                    [commonUtility  logOut];
                }
                
            }
            
            //<-----
        }
    }
    
    
}

//Move to Edit Profile view
-(void)moveToHomeScreen{
    
    MESSAGE_TEST(@"moveToHomeScreen--->1 ");
    
    //Craerte object for Home Screen
    ViewController *objMenuView  = (ViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        MESSAGE_TEST(@"moveToHomeScreen--->2 ");
    //Push home screen
    [self.navigationController pushViewController:objMenuView animated:NO];
        MESSAGE_TEST(@"moveToHomeScreen--->3 ");
}




//Save clients in local DB
-(void)saveClients:(NSArray *)arrayclients{
    
    START_METHOD
    
    //If array has data
    if (arrayclients && arrayclients.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclients.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictClientsInfo = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclients objectAtIndex:i]] mutableCopy];
            
            //Set KEys valuse
            [dictClientsInfo setValue:[dictClientsInfo objectForKey:@"emer1_sPhone_Evening"] forKey:kEmergencyConHNo1];
            [dictClientsInfo setValue:[dictClientsInfo objectForKey:@"emer2_sPhone_Evening"] forKey:kEmergencyConHNo2];
            [dictClientsInfo setValue:[dictClientsInfo objectForKey:@"nClientID"] forKey:kClientId];
            
            
            //If user is old
            if ([[commonUtility retrieveValue:KEY_OLD_USER] isEqualToString:@"YES"]){
            
            [dictClientsInfo setValue:@"YES" forKey:STATUS];
            }else{
                [dictClientsInfo setValue:[dictClientsInfo objectForKey:CLIENT_STATUS] forKey:STATUS];

            }
            MESSAGE(@"cleints info: %@",dictClientsInfo);
            
            
            //Save in local db
            [[DataManager initDB] insertNewClientUpgraded:[dictClientsInfo mutableCopy]];
            
            
            //Invoke method for update the client status Migrated YES
            [[DataManager initDB] updateClientMigrationStatus:dictClientsInfo ];
            
            
            //Invoke method for get Clients Image from server and save in loacl
            [self getClientProfileImage:dictClientsInfo ];
            
            //Invoke method for get Client Signature
            [self getClientSignatureImage:dictClientsInfo];
            
        }
        
    }

    END_METHOD
}

//Save client's Answers in local DB
-(void)saveClientsAnswers:(NSArray *)arrayclientAnswers{
    
    START_METHOD
    
    //If array has data
    if (arrayclientAnswers && arrayclientAnswers.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientAnswers.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictAnswers = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientAnswers objectAtIndex:i]]mutableCopy];
            
            [dictAnswers setValue:[dictAnswers objectForKey:@"sSheetName"] forKey:ksheetName];
            MESSAGE(@"cleints dictAnswers: %@",dictAnswers);
            
            //Invoke method for insert programs
            [[DataManager initDB] insertAnswerFor:dictAnswers];
            
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsProgram:(NSArray *)arrayclientsPrograms{
    
    START_METHOD
    
    //If array has data
    if (arrayclientsPrograms && arrayclientsPrograms.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsPrograms.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictProgram = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsPrograms objectAtIndex:i]]mutableCopy];
            
            [dictProgram setValue:[dictProgram objectForKey:@"sSheetName"] forKey:ksheetName];
            MESSAGE(@"cleints dictProgram: %@",dictProgram);
            
            //Invoke method for insert programs
            [[DataManager initDB] insertProgram:dictProgram];
            
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsExercise:(NSArray *)arrayclientsExercise{
    
    START_METHOD
    
    //If array has data
    if (arrayclientsExercise && arrayclientsExercise.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsExercise.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictExercise = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsExercise objectAtIndex:i]]mutableCopy];
            
            
            [dictExercise setValue:[dictExercise objectForKey:@"sDate"] forKey:kEXDate];
            [dictExercise setValue:[dictExercise objectForKey:@"program_exercise_id"] forKey:kBlockDataId];
            
            MESSAGE(@"cleints dictExercise: %@",dictExercise);
            
            //Invoke method for insert exercise
            NSInteger success = [[DataManager initDB] insertProgramData:dictExercise forClient:[dictExercise objectForKey:kClientId]];
            
        }
        
    }
}

//Save client's workouts and notes in local DB
-(void)saveClientsWorkoutsAndNotes:(NSArray *)arrayclientsWorkoutAndNotes{
    
    START_METHOD
    //If array has data
    if (arrayclientsWorkoutAndNotes && arrayclientsWorkoutAndNotes.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsWorkoutAndNotes.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictWorkoutAndNotes = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsWorkoutAndNotes objectAtIndex:i]]mutableCopy];
            
            MESSAGE(@"cleints dictWorkoutAndNotes: %@",dictWorkoutAndNotes);
            
            [[DataManager initDB]insertSheetBlocksForSheet:[dictWorkoutAndNotes objectForKey:kSheetId] date:[dictWorkoutAndNotes objectForKey:@"dBlockdate"] blockNo:[dictWorkoutAndNotes objectForKey:kBlockNo] clientId:[dictWorkoutAndNotes objectForKey:kClientId] blockTitle:[dictWorkoutAndNotes objectForKey:kBlockTitle] blockNotes:[dictWorkoutAndNotes objectForKey:kBlockNotes] andBlockId:[dictWorkoutAndNotes objectForKey:kBlockId]];
            //sunil-> date: 02 sept 2016
            
        }
        
    }
}


//Save client's asseessments and notes in local DB
-(void)saveClientsAssessment:(NSArray *)arrayclientsAssessments{
    
    START_METHOD
    
    MESSAGE(@"arrayclientsAssessments->00: %@",arrayclientsAssessments);
    //If array has data
    if (arrayclientsAssessments && arrayclientsAssessments.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsAssessments.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictAssessment = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsAssessments objectAtIndex:i]]mutableCopy];
            
            
            MESSAGE(@"cleints dictAssessment: %@",dictAssessment);
            [[DataManager initDB] insertNewAssessmentSheetCleintID:[dictAssessment objectForKey:kClientId] sheetName:[dictAssessment objectForKey:ksheetName] andSheetID:[dictAssessment objectForKey:kSheetId]];

//            
//            //Invoke methdo for get Assessment Image
//            [self getAssessmentImage:[dictAssessment objectForKey:@"assessmentImage"] andClientId:[dictAssessment objectForKey:kClientId]];
//            
        }
        
    }
}


//Save client's asseessments's data and notes in local DB
-(void)saveClientsAssessmentData:(NSArray *)arrayclientsAssessmentsData{
    
    START_METHOD
    //If array has data
    if (arrayclientsAssessmentsData && arrayclientsAssessmentsData.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayclientsAssessmentsData.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictAssessmentData = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayclientsAssessmentsData objectAtIndex:i]]mutableCopy];
            
            //Set Values
            
            NSString *strGender =   [dictAssessmentData objectForKey:kgender];
            [[NSUserDefaults standardUserDefaults] setObject:strGender forKey:kgender];
//            if([strGender isEqualToString:@"M"]){
            
                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Male_Chest" ] forKey:kThree_Data1];
                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Male_Abdomen" ] forKey:kThree_Data2];
                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Male_Thigh" ] forKey:kThree_Data3];
                
//            }else{
//                
//                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Female_Triceps" ] forKey:kThree_Data1];
//                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Female_Suprailiac" ] forKey:kThree_Data2];
//                [dictAssessmentData setValue:[dictAssessmentData objectForKey:@"fThree_Female_Thigh" ] forKey:kThree_Data3];
//                
//
//                
//            }
            
            MESSAGE(@"cleints dictWorkoutAndNotes: %@",dictAssessmentData);
            
           [[DataManager initDB] updateAssessmentSheetFor:dictAssessmentData];
            
            
            
            //Invoke methdo for get Assessment Image
            [self getAssessmentImage:[dictAssessmentData objectForKey:@"assessmentImage"] andClientId:[dictAssessmentData objectForKey:kClientId]];
            
            
            
        }
        
    }
}

//TODO: TEMPLATE INSERTION

//Save clients in local DB
-(void)saveClientsTemplates:(NSArray *)arrayTemplate{
    
    START_METHOD
    
    //If array has data
    if (arrayTemplate && arrayTemplate.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayTemplate.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictTemplate = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayTemplate objectAtIndex:i]]mutableCopy];
            
            
            MESSAGE(@"cleints dictExercise: %@",dictTemplate);
            
            //Invoke method for insert exercise
            NSInteger success = [[DataManager initDB] addTemplate:dictTemplate];
        }
        
    }
}



//Save clients in local DB
-(void)saveClientsTemplatesProgram:(NSArray *)arrayTemplatePrograms{
    
    START_METHOD
    
    //If array has data
    if (arrayTemplatePrograms && arrayTemplatePrograms.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayTemplatePrograms.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictTemplateProgram = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayTemplatePrograms objectAtIndex:i]] mutableCopy ];
            [dictTemplateProgram setValue:[dictTemplateProgram objectForKey:@"sheetName"] forKey:@"sSheetName"];
            
            
            MESSAGE(@"cleints dictExercise: %@",dictTemplateProgram);
            
            //Invoke method for insert exercise
            [[DataManager initDB] addSheetDetail:dictTemplateProgram];
        }
        
    }
}


//Save clients in local DB
-(void)saveClientsTemplatesWorkouts:(NSArray *)arrayTemplateWorkouts{
    
    START_METHOD
    
    //If array has data
    if (arrayTemplateWorkouts && arrayTemplateWorkouts.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayTemplateWorkouts.count; i++) {
            
            //Get clint dict info
            NSMutableDictionary *dictTemplateWorkouts = [[commonUtility dictionaryByReplacingNullsWithStrings:[arrayTemplateWorkouts objectAtIndex:i]] mutableCopy];
            
            [dictTemplateWorkouts setValue:[dictTemplateWorkouts objectForKey:@"nBlockNo"] forKey:@"blockNo"];
                        [dictTemplateWorkouts setValue:[dictTemplateWorkouts objectForKey:@"nSheetID"] forKey:@"sheetID"];

            [dictTemplateWorkouts setValue:[dictTemplateWorkouts objectForKey:@"sSheetName"] forKey:@"sBlockTitle"];
            
            
            NSString *str =[dictTemplateWorkouts objectForKey:@"dBlockdate"];
            
            [dictTemplateWorkouts setValue:str forKey:@"strDate"];

            
            MESSAGE(@"cleints dictTemplateWorkouts: %@ and : %@",[dictTemplateWorkouts objectForKey:@"dBlockdate"],str);
            
            

            //Invoke method for insert exercise
            [[DataManager initDB] insertTemplateSheet_Blocks:dictTemplateWorkouts];
        }
        
    }
}



//Save clients in local DB
-(void)saveClientsTemplatesExercise:(NSArray *)arrayTemplateExercise{
    
    START_METHOD
    
    //If array has data
    if (arrayTemplateExercise && arrayTemplateExercise.count>0) {
        
        //Iterate all the clients
        for (int i=0; i<arrayTemplateExercise.count; i++) {
            
            //Get clint dict info
            NSDictionary *dictTemplateExercise = [commonUtility dictionaryByReplacingNullsWithStrings:[arrayTemplateExercise objectAtIndex:i]];
            
            
            MESSAGE(@"cleints dictExercise: %@",dictTemplateExercise);
            
            //Invoke method for insert exercise
            [[DataManager initDB] insertTemplate_ProgramedData:dictTemplateExercise];
        }
        
    }
}


//TODO: GEt client Profile image  from server
-(void)getClientProfileImage:(NSDictionary  *)dictClient{
    START_METHOD
    [TheAppController showHUDonView:nil];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictClient objectForKey:@"image"]]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //Invoke method for save in local
                    [self saveClientsProfileImage:[dictClient objectForKey:kClientId] andImage:image];
                
                
                });
            }
        }
    }];
    [task resume];
    
    END_METHOD
}



//Method for save signature in local
-(void)saveClientsProfileImage :(NSString *) clientId andImage: (UIImage *)img{
    
    START_METHOD
    if(![clientId isEqualToString:@"0"]) {
        
        [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsProfileImage",[FileUtility basePath]]];
        NSString * imgNm = [NSString stringWithFormat:@"ProfileImage%@",clientId];
        NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
        [FileUtility createFileInFolder:@"ClientsProfileImage" withName:imgNm withData:imageData];
    }
        [TheAppController hideHUDAfterDelay:0.0];
    
    
    //CRAETE NOTIFICATION AND SEND WHEN SUCEECC
    [[NSNotificationCenter defaultCenter] postNotificationName:kReloadTable object:nil];
    
    
    END_METHOD
}



//TODO: GEt client Profile image  from server
-(void)getClientSignatureImage:(NSDictionary  *)dictClient{
    START_METHOD
    

     [TheAppController showHUDonView:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictClient objectForKey:@"blob_signature"]]];
    
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //Invoke method for save in local
                    [self saveClientsSignatureImage:[dictClient objectForKey:kClientId ] andImage:image];
                    
                    
                });
            }
        }
    }];
    [task resume];
    
    
    END_METHOD
}


//TODO: SAVE SIGNATURE IMAGE
//Method for save signature in local
-(void)saveClientsSignatureImage :(NSString *) strClientId andImage:(UIImage *)img{

    START_METHOD

    
    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/ClientsSignature",[FileUtility basePath]]];
    NSString * imgNm = [NSString stringWithFormat:@"Signature%@",strClientId];
    NSData *imageData = UIImageJPEGRepresentation(img, 1.0);
    [FileUtility createFileInFolder:@"ClientsSignature" withName:imgNm withData:imageData];
 
    
        [TheAppController hideHUDAfterDelay:0.0];
    END_METHOD
}


//TODO: GEt client Profile image  from server
-(void)getAssessmentImage:(NSArray *)arryaAssessmentImages andClientId:(NSString *)strClientId{
    START_METHOD
    
    for (int i =0; i<arryaAssessmentImages.count; i++) {
        
         [TheAppController showHUDonView:nil];
        
        NSDictionary *dictAssessment    =   [[arryaAssessmentImages objectAtIndex:i] mutableCopy];
        [dictAssessment setValue:strClientId forKey:kClientId];
        
        
        MESSAGE(@"dictAssessment---> asimage: %@",dictAssessment);
        
        //Invoke Method for Update the Tilte of image
        NSDictionary * dictInfo = [NSDictionary dictionaryWithObjectsAndKeys:strClientId,kClientId,
                                  [dictAssessment objectForKey:@"assessment_id"],ksheetID,
                                   [dictAssessment objectForKey:@"type_id"],khasImage,
                                  [NSString stringWithFormat:@"%d",(i+1)],kImageID,
                                  [dictAssessment objectForKey:@"image_name"],kImageName,nil];
        
        [[DataManager initDB] updateAssessmentImageTitlefor:dictInfo];
            
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[dictAssessment objectForKey:@"url_name"]]];
        
        MESSAGE(@"assessment url image: %@",url);
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            
                                MESSAGE(@"assessment url image: success image: %@",image);
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    MESSAGE(@"assdispatch_get_main_queue");

                    
                    //Invoke method for save in local
                    [self saveAssessmentImage:dictAssessment andImage:image];
                    
                    
                });
            }
        }
    }];
    [task resume];
    }
    
    END_METHOD
}

-(void)saveAssessmentImage:(NSDictionary *)objDict andImage:(UIImage *) image{
    
    START_METHOD
//    
//    "assessment_image_id": "111",
//    "assessment_id": "788",
//    "type_id": "2",
//    "url_name": "http://52.26.135.139/assets/images/1320335643090171787.jpg",
//    "image_name": "text"
//    
//    
    
    MESSAGE(@"arrya assessment images: %@ and image: %@",objDict , image);
    
    
    NSString * imageFolder = [NSString stringWithFormat:@"AssessmentData/Client-%@ Data/sheet-%@",[objDict objectForKey:kClientId],[objDict objectForKey:@"assessment_id"]];
    [FileUtility createDirectoryIfNeededAtPath:[NSString stringWithFormat:@"%@/%@",[FileUtility basePath],imageFolder]];
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    [FileUtility createFileInFolder:imageFolder withName:[objDict objectForKey:@"type_id"] withData:imageData];
    
    [TheAppController hideHUDAfterDelay:0.0];
    END_METHOD
}


//TODO: UPDATE THE ASSESSMENT IMAGE TITLE
-(void)updateAssessmentImageTitlefor{
    
}



//TODO: Send mail to the User for Purchase new subcription
-(void)checkAddClientStatus{
    START_METHOD
    MESSAGE(@"checkAddClientStatus--> ");
    
    
    //Create dictionaryb for send param in api for Trainer's Profile
    NSDictionary *params = @{
                             @"user_id"          :   [commonUtility retrieveValue:KEY_TRAINER_ID],
                             };
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@trainerAddClientStatus/?access_key=%@",HOST_URL,USER_ACCESS_TOKEN];
    
    MESSAGE(@"params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:params
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"responce:checkAddClientStatus: %@", responseDataDictionary);
                               
                               //If Success
                               if([responseDataDictionary objectForKey:PAYLOAD]){
                                   
                                   //************  GET Trainer's Profile AND SAVE IN LOCAL DB
                                   NSDictionary *dictTrainerProfile =  [ [responseDataDictionary objectForKey:PAYLOAD]  objectForKey:RESPONSE_DATA] ;
                                   MESSAGE(@"arrayUserData : dictTrainerProfile: %@",dictTrainerProfile);
                                   
                                   //For Old User, He can Add any number of clients for full version
                                   [commonUtility saveValue:[dictTrainerProfile objectForKey:@"status"] andKey:KEY_STATUS_ADD_CLIENT];
                                   
                               }
                               
                               //Check if get error for Expire token
                               if([responseDataDictionary objectForKey:METADATA]){
                                   
                                   //Get Dictionary
                                   NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                   
                                   NSString *strErrorMessage = [dictError objectForKey:POPUPTEXT];
                                   
                                   
                                   if(strErrorMessage.length>0){
                                   //Show Error Alert
                                   [commonUtility alertMessage:[dictError objectForKey:POPUPTEXT]];
                                   }
                                   
                                   
                               }
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               
                           }];
    END_METHOD
}


//TODO: FACEBOOK IMPLEMENTAION


//TODO: Login in Facebook
-(void)loginInFacebook{
    START_METHOD
   
    
    //Create object for Logion manager for login into facebok
    FBSDKLoginManager   *login  = [[FBSDKLoginManager alloc] init];
    
    //Set Behavioir for Login by popup or Web view
    login.loginBehavior         = FBSDKLoginBehaviorWeb;
    
    //Set Permission for user
    [login logInWithReadPermissions: @[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        //Check If Error
        if (error) {
            
            [TheAppController hideHUDAfterDelay:0];
            MESSAGE(@"Process error");
            //Show Alert
//            [CommonUtility alertMessage:OPERATION_FAILED];
            
            //Else if canceled by user
        } else if (result.isCancelled) {
            [TheAppController hideHUDAfterDelay:0];

            MESSAGE(@"Cancelled");
            
        } else {
            //if successfully Logged In invoke method for Access facebook info
            [self getFacebookUserInfo];
        }
        
    }];
    
    END_METHOD
}


//TODO: Get FB user info
-(void)getFacebookUserInfo{
    START_METHOD
    NSString *fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [TheAppController showHUDonView:nil];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields=id,name,email" parameters:nil] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                

                //If succes in getting info
                MESSAGE(@"result: %@",result);
                
                //Get info in Dictionary
                NSDictionary *dictFbUser =  (NSDictionary *)result;
                
                MESSAGE(@"dictFbUser: %@",dictFbUser);
              
                //If Facebook account has email id
                if([dictFbUser objectForKey:EMAIL] ){
                    
                    //Check Network connection
                    if([TheAppDelegate isNetworkAvailable]){
                        
                        //Invoke method for post Request for Sign IN
                        [self postRequestToServerForSignInByFb:dictFbUser];
                        
                    }else{
                        
                        [TheAppController hideHUDAfterDelay:0];

                        
                        [commonUtility alertMessage:@"Network not available!" ];
                    }
                    
                    
                    
                }else{//If Facebook account has NOT email id
                 
                    [TheAppController hideHUDAfterDelay:0];

                    
                    //Craete alert message
                    NSString *alertMessage  =   NSLocalizedString(@"Sorry, There is no email id registered with your Facebook account. So, Please register yourself with Sign Up process.", nil);
                    
                    //Create alert for upgrade
                    UIAlertView *alertView      = [[UIAlertView alloc]initWithTitle:kAppName message:alertMessage delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
                    
                    [alertView show];
                    
                }
                
                
            }else{
                
                
                [TheAppController hideHUDAfterDelay:0];

            }
        }];
    }
    END_METHOD
}
- (IBAction)actionLoginWithFb:(id)sender {
    
    //Resign KEyboard
    if([textfiledActive isFirstResponder]){
        
        [textfiledActive resignFirstResponder];
    }
    
    //Invoke method for login with facebook
    [self    loginInFacebook];
}


//Method for post request for SIGNIN through Facebook
-(void)postRequestToServerForSignInByFb:(NSDictionary *)dictFb{
    START_METHOD
    
    NSMutableDictionary *paramsDict     =   [dictFb mutableCopy];
    
    
    [paramsDict setValue: [dictFb objectForKey: EMAIL]      forKey:EMAIL];
    [paramsDict setValue: [dictFb objectForKey: @"id"]      forKey:@"facebook_id"];
    [paramsDict setValue: [dictFb objectForKey: @"name"]    forKey:@"first_name"];
    [paramsDict setValue: @""                               forKey:@"last_name"];
    [paramsDict setValue: [commonUtility getUDID]           forKey:@"device_udid"];
    
    
    //Make url for hit the Trainer Profile
    NSString *strUrl =[NSString stringWithFormat:@"%@facebookLogin",HOST_URL];
    
    MESSAGE(@"postRequestToServerForTrainerProfile-> params=: %@ and Url : %@",paramsDict,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postRequestWitHUrlWithFormData:strUrl parameters:paramsDict
                           success:^(NSDictionary *responseDataDictionary) {
                               
                               MESSAGE(@"postRequestWitHUrl->responce: %@", responseDataDictionary);
                               
                               //Invoke method for handle responce
                               [self takeActionOnLogin:responseDataDictionary];
                               
                               
                           }failure:^(NSError *error) {
                               MESSAGE(@"Eror : %@",error);
                               
                               //Hide the indicator
                               [TheAppController hideHUDAfterDelay:0];
                               
                               //Show Alert For error
                               [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                               
                               //Save Login Status
                               [commonUtility saveValue:@"NO" andKey:KEY_LOGIN_STATUS];
                               
                               //
                               
                               
                           }];
    END_METHOD
}


//TODO: POST MIGRATE TEXT FILE

//Method for post request for SIGNIN
-(void)postLocalDbToServer{
    START_METHOD
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dbPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"TrainerClipboardDatabaseBackUp.sqlite"];
    
    
    MESSAGE(@"dbPath: copyDatabaseIfNeededForBackup: %@",dbPath);
    
    //Carete URL by string by invoking method for get local db path
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:dbPath];
    
    //Create dictionaryb for send param in api for Trainer's Profile
//    EMAIL           :   @"support@thetrainingnotebook.com",

    NSDictionary *params = @{
                             EMAIL           :   self.textFiledEmailLogin.text,
                             @"device_udid"  : [commonUtility getUDID],
                             @"total_clients": @"Sorry",
                             @"total_images"  : @"Sorry",
                             };
    
    //Make url for hit the sign api
    
    NSString *strUrl =[NSString stringWithFormat:@"%@sqlUploadWithUserStatics",HOST_URL];
    
    MESSAGE(@"postRequestToMigrateAllData-> params=: %@ and Url : %@",params,strUrl);
    
    //Invoke method for Encrypt the message by SHA1
    ServiceHandler *objService = [[ServiceHandler alloc]init];
    
    //Show indicator
    [TheAppController showHUDonView:nil];
    
    //invoke method
    [objService postDataWithFile:strUrl parameters:params filePath:fileURL
                         success:^(NSDictionary *responseDataDictionary) {
                             
                             MESSAGE(@"responce from file: %@", responseDataDictionary);
                             
                             //Check
                             if([responseDataDictionary objectForKey:METADATA]){
                                 
                                 //Get Dictionary
                                 NSDictionary   *dictError    =   [commonUtility dictionaryByReplacingNullsWithStrings: [responseDataDictionary objectForKey:METADATA]];
                                 
                                 //Show Error Alert
                                 NSString *error= [dictError objectForKey:@"error"];
                                 
                                 
                                 if(error && error.length>0 && [error isEqualToString:@"faflse"]){
                                     
                                     //Hide the indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     
                                     MESSAGE(@"Hide indicateor--------->");
                                     
                                     //Show Alert For error
                                     
                                     [commonUtility alertMessage:@"File Uploaded!"];
                                     
                                     
                                 }else{
                                     
                                     
                                     //Hide the indicator
                                     [TheAppController hideHUDAfterDelay:0];
                                     MESSAGE(@"Hide indicateor--------->");
                                     
                                     //Show Alert For error
                                     
                                     [commonUtility alertMessage:@"File not uploaded. Please try again!"];
                                     
                                 }
                                 
                             }
                             
                             
                             
                             
                         }failure:^(NSError *error) {
                             MESSAGE(@"Eror : %@",error);
                             
                             //Hide the indicator
                             [TheAppController hideHUDAfterDelay:0];
                             MESSAGE(@"Hide indicateor--------->");
                             
                             //Show Alert For error
                             [commonUtility alertMessage:@"No internet detected, please connect to internet!"];
                             
                         }];
    END_METHOD
}


/*
 -----------------
 */
@end


