//
//  NotesViewController.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol NotesVCDelegate <NSObject>
-(void)setProgrameWorkoutModuleForNotes:(NSDictionary*)dictNotes;
@end


@interface NotesViewController : UIViewController{
    id<NotesVCDelegate> __weak delegate;
}
@property (nonatomic, weak) id<NotesVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIView          *notesview;
@property (weak , nonatomic) IBOutlet UILabel        *lblClientName;
@property (weak , nonatomic) IBOutlet UILabel        *lblNotes;
@property (strong, nonatomic) NSMutableArray       *arrtemp;

@property (nonatomic , strong) NSString                 *strClientId;
@property (nonatomic , strong) NSString                 *strClientName;
@property (nonatomic) BOOL                              setAsLandscape;


- (IBAction)newNoteTap:(id)sender;
-(void)setLandscapeOrientation;
-(void)setportraitOrientation;
@end
