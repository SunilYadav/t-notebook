//
//  MedicineTagViewController.h
//  PT
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardControls.h"
@protocol AutoSuggestViewDelegate

-(void) getExercise:(NSString *)Exercise;
-(void)closeExercisePopover;
- (void) exerciseValueChanged:(NSString *)exe;
-(void)changePopOverFrame;
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;

@end


@interface AutoSuggestViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate> {
	id<AutoSuggestViewDelegate> __unsafe_unretained delegate;
	UITableView			* _tblSuggestion;
	UITextField			* _txtEx;	
    NSArray             * _arrayMain;
    NSArray             * _arrayExercise;
	NSString			* strSelExercise;

}
@property (nonatomic, strong) KeyboardControls      *keyboardControls;
@property (nonatomic, strong) NSString					* strSelExercise;
@property (nonatomic, strong) IBOutlet UITableView		* tblSuggestion;
@property (nonatomic, strong) IBOutlet UITextField		* txtEx;
@property (nonatomic, unsafe_unretained) id<AutoSuggestViewDelegate> delegate;
@property (nonatomic, strong) NSArray                   * arrayMain;
@property (nonatomic, strong) NSArray                   * arrayExercise;

- (IBAction) btnDone_click;
- (IBAction) btncancel_click; 
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
@end
