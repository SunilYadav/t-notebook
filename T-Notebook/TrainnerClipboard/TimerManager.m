//
//  TimerManager.m
//  T-Notebook
//
//  Created by WLI on 03/12/14.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "TimerManager.h"

static TimerManager     *timerManagerObject;

@implementation TimerManager


-(id)init {
    timerManagerObject = self;
    return timerManagerObject;
}

//---------------------------------------------------------------------------------------------------------------

- (void) runTimer {
    
    if(self.intTotalTime > 1){ // check for total remaining time is greater than 1 or not.
        
        [[AppDelegate sharedInstance] setIntRemaningTimeForPhase:self.intRemaningTimeForPhase];
        
        if(self.intRemaningTimeForPhase > 1){ // Check for remaining time for running phase
            
            if(self.intRemaningTimeForPhase <= 4){
                if (![[AppDelegate sharedInstance] isFromBackground]) {
                    [[AppDelegate sharedInstance] playSound:3];
                }
            }
            self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
            [self changeValue:self.strCenterTimer forselector:kisCenterTime];
            [self setIntRemaningTimeForPhase:(self.intRemaningTimeForPhase - 1)];
         
        }else{ // Current phase is complete than need to find for next one.
            
            if(self.intSelectedSet == 1){
            
                if(self.intRestTime> 0){ // Check for the rest time in cycle.  ::::::::::: Rest
                    
                    if (![AppDelegate sharedInstance].isFromBackground) {
                        [[AppDelegate sharedInstance] playSound:2];
                       
                    }
                    
                    [self setIntSelectedSet:2]; // change the set
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",self.strRest];
                    [self changeImageWithSelector:kisRest];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    [self setIntRemaningTimeForPhase:self.intRestTime];
                    
                    
                } else if([self.strSets intValue] > 1){ // Check for another cycle.
                    
                    if(![AppDelegate sharedInstance].isFromBackground) {
                        [[AppDelegate sharedInstance] playSound:1];
                    }
                    [self setIntSelectedSet:1];
                    self.strSets = [self getDecreasedValue:self.strSets]; // change cycle
                    
                    [self changeValue:self.strSets forselector:kisSet];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",self.strWork];
                    [self changeImageWithSelector:kisWork];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];

                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    [self setIntRemaningTimeForPhase:self.intWorkTime];
                   
                }else if (self.intSetRestTime  > 0){ // if all cycle is completed than check for circuit rest
                    
                    if(![AppDelegate sharedInstance].isFromBackground) {
                      [[AppDelegate sharedInstance] playSound:2];
                    }
                    
                    [self setIntSelectedSet:3];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",self.strSetrest];
                    [self changeImageWithSelector:kisSetRest];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    [self setIntRemaningTimeForPhase:self.intSetRestTime];

            
                }else if([self.strCircuit intValue] > 1){ // check for another Circuit
                    
                    
                    if(![AppDelegate sharedInstance].isFromBackground) {
                          [[AppDelegate sharedInstance] playSound:1];
                    }
                    
                    [self setIntSelectedSet:1];
                    
                    self.strCircuit = [self getDecreasedValue:self.strCircuit];
                    [self changeValue:self.strCircuit forselector:kisCircuit];
                    
                    self.strSets = [self getDecreasedValue:[NSString stringWithFormat:@"%ld",(long)(self.intNumberOfSets + 1)]];
                    [self changeValue:self.strSets forselector:kisSet];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",self.strWork];
                    [self changeImageWithSelector:kisWork];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    [self setIntRemaningTimeForPhase:self.intWorkTime];
                 
                }
                
            } else if (self.intSelectedSet == 2){ // if currently playing state is rest
                
                if([self.strSets intValue] > 1){ // Check for another cycle.
                    
                    if(![AppDelegate sharedInstance].isFromBackground) {
                         [[AppDelegate sharedInstance] playSound:1];
                    }
                   
                    [self setIntSelectedSet:1];
                    
                  //  [[AppDelegate sharedInstance] setAppIntSelectedSet:1];
                    self.strSets = [self getDecreasedValue:self.strSets]; // change cycle
                    [self changeValue:self.strSets forselector:kisSet];
                    self.strCenterTimer= [NSString stringWithFormat:@"%@:",self.strWork];
                    [self changeImageWithSelector:kisWork];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
            
                    [self setIntRemaningTimeForPhase:self.intWorkTime];
                 
                   
                    
                } else if (self.intSetRestTime > 0){ // if all cycle is completed than check for circuit rest
                    
                    if(![AppDelegate sharedInstance].isFromBackground) {
                         [[AppDelegate sharedInstance] playSound:2];
                    }
                    [self setIntSelectedSet:3];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",self.strSetrest];
                    [self changeImageWithSelector:kisSetRest];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                   self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    [self setIntRemaningTimeForPhase:self.intSetRestTime];

                }else if([self.strCircuit intValue] > 1){ // check for another Circuit
                    
                    if(![AppDelegate sharedInstance].isFromBackground) {
                        [[AppDelegate sharedInstance] playSound:1];

                    }
                    
                    [self setIntSelectedSet:1];
                   
                    self.strCircuit = [self getDecreasedValue:self.strCircuit];
                    [self changeValue:self.strCircuit forselector:kisCircuit];
                    
                    self.strSets = [self getDecreasedValue:[NSString stringWithFormat:@"%ld",(long)(self.intNumberOfSets + 1)]];
                    [self changeValue:self.strSets forselector:kisSet];
                    
                    self.strCenterTimer =  [NSString stringWithFormat:@"%@:",self.strWork];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    self.strCenterTimer= [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                    [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    
                    [self setIntRemaningTimeForPhase:self.intWorkTime];
                    
                    [self changeImageWithSelector:kisWork];
                }
                
            } else if (self.intSelectedSet == 3){ // if currently playing state is  CYCLE REST
                
                 if([self.strCircuit intValue] > 1){ // check for another circuit is available
                    if(![AppDelegate sharedInstance].isFromBackground) {
                         [[AppDelegate sharedInstance] playSound:1];
                    }
                    
                    [self setIntSelectedSet:1];
                 
                    self.strCircuit = [self getDecreasedValue:self.strCircuit];
                    [self changeValue:self.strCircuit forselector:kisCircuit];
                    
                    self.strSets = [self getDecreasedValue:[NSString stringWithFormat:@"%ld",(long)(self.intNumberOfSets + 1)]];
                    [self changeValue:self.strSets forselector:kisSet];
                    
                    self.strCenterTimer = [NSString stringWithFormat:@"%@:",self.strWork];
                     [self changeValue:  self.strCenterTimer  forselector:kisCenterTime];
                     [self changeImageWithSelector:kisWork];
                     
            
                     self.strCenterTimer = [NSString stringWithFormat:@"%@:",[self getDecreasedValuein_TimeFormat:self.strCenterTimer]];
                     [self changeValue:self.strCenterTimer forselector:kisCenterTime];
                    [self setIntRemaningTimeForPhase:self.intWorkTime];
                 
                }
            }
        }
        
        self.strSessionTimer = [self getDecreasedValuein_TimeFormat:[NSString stringWithFormat:@"%@:",self.strSessionTimer]];
        [self changeValue:self.strSessionTimer forselector:kisSessionTime];
        [self setIntTotalTime:(self.intTotalTime - 1)];
        
    } else {
        
        
        if (![[AppDelegate sharedInstance] isFromBackground]) {
            [[AppDelegate sharedInstance] playSound:4];
        }
        [self.intervalTimer invalidate];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(setMiliseconds) object:nil];
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:kIsIntervalPlaying];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self setIsTimerRunning:FALSE];
        [self changeImageWithSelector:kisWork];
        [AppDelegate sharedInstance].appIsTimerRunning = FALSE;
        [self timerFinishedRunning];
        
    }
}

//---------------------------------------------------------------------------------------------------------------

- (NSString *) getDecreasedValuein_TimeFormat : (NSString *) strCurrentValue{
    
    strCurrentValue = [strCurrentValue substringToIndex:([strCurrentValue length] - 1)];
    
    NSString * strText = strCurrentValue;
    
    if([strCurrentValue length] <= 0){
        self.strMilsecond = [[NSNumber numberWithInt:0] stringValue];
        return @"00:00";
    }
    
    NSArray * arrStr = [strText componentsSeparatedByString:@":"];
    

    int hh1 = 0;
    int mm1 = 0;
    int ss1 = 0;
    
    if ([arrStr count] == 2) {
        mm1 = [[arrStr objectAtIndex:0] intValue];
        ss1 = [[arrStr objectAtIndex:1] intValue];
    }
    else {
        hh1 = [[arrStr objectAtIndex:0] intValue];
        mm1 = [[arrStr objectAtIndex:1] intValue];
        ss1 = [[arrStr objectAtIndex:2] intValue];
    }
    
    if (ss1 - 1 >= 0) {
        ss1--;
    }
    else {
        if (mm1-1 >= 0) {
            ss1 = 59;
            mm1 --;
        }
        else {
            if (hh1-1 >= 0) {
                mm1 = 59;
                hh1--;
            }
            else {
                self.strMilsecond = [[NSNumber numberWithInt:0] stringValue];
                return @"00:00";
            }
        }
    }
    strText = @"";
    
    if (hh1>0) {
        if (hh1<=9) {
            strText = [NSString stringWithFormat:@"0%d:",hh1];
        }
        else {
            strText = [NSString stringWithFormat:@"%d:",hh1];
        }
    }
    
    if (mm1<=9) {
        strText = [strText stringByAppendingFormat:@"0%d:",mm1];
    }
    else {
        strText = [strText stringByAppendingFormat:@"%d:",mm1];
    }
    
    if (ss1<=9) {
        strText = [strText stringByAppendingFormat:@"0%d",ss1];
    }
    else {
        strText = [strText stringByAppendingFormat:@"%d",ss1];
    }
    return strText;
    
}

//---------------------------------------------------------------------------------------------------------------

- (NSString *) getDecreasedValue : (NSString *) strCurrentValue {
    
    
    if([strCurrentValue length] <= 0){
        strCurrentValue = @"01";
    }
    
    NSString * strText = strCurrentValue;
    int val;
    val = [strText intValue];
    
    if (val - 1 > 0) {
        val--;
    }
    
    if (val<=9) {
        strText = [NSString stringWithFormat:@"0%d",val];
    }
    else {
        strText = [NSString stringWithFormat:@"%d",val];
    }
    return strText;
}

//---------------------------------------------------------------------------------------------------------------

- (NSString *) getMilisecInTimeFormat : (NSNumber *) timeInterVal {
    
    int totalsecond = [timeInterVal intValue];
    int milsec = totalsecond % 100;
    
    NSString *msec;
    if (milsec < 10) {
        if (milsec < 0) {
            NSArray *value = [[NSString stringWithFormat:@"%d",milsec] componentsSeparatedByString:@"-" ];
            msec = [NSString stringWithFormat:@"-0%@",[value objectAtIndex:1]];
        } else {
            msec = [NSString stringWithFormat:@"0%d",milsec];
        }
    } else {
        msec = [NSString stringWithFormat:@"%d",milsec];
    }
    
    return [NSString stringWithFormat:@"%@",msec];
}


//---------------------------------------------------------------------------------------------------------------

- (void) setMiliseconds {
    
    [AppDelegate sharedInstance].seconds_InterVal = [NSNumber numberWithInt:([[AppDelegate sharedInstance].seconds_InterVal intValue] - 1)];
    self.strMilsecond = [self getMilisecInTimeFormat:[AppDelegate sharedInstance].seconds_InterVal];
    if([self.strMilsecond isEqualToString:@"00"]){
        [AppDelegate sharedInstance].seconds_InterVal = [NSNumber numberWithInteger:99];
        self.strMilsecond = @"99";
        [self runTimer];
    }
    
}

//---------------------------------------------------------------------------------------------------------------

- (void)startTimer {
    
   _intervalTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(setMiliseconds) userInfo:nil repeats:YES];
    [AppDelegate sharedInstance].seconds_InterVal = [NSNumber numberWithInteger:99];
    [self performSelector:@selector(setMiliseconds) withObject:nil afterDelay:0.01];
    [self performSelector:@selector(setBlinkImage) withObject:nil afterDelay:0.5];
}


//---------------------------------------------------------------------------------------------------------------

-(void)pauseTimer {
    [self.intervalTimer invalidate];
   
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(runTimer) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(setMiliseconds) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(setBlinkImage) object:nil];
}

//---------------------------------------------------------------------------------------------------------------

- (void)changeValue:(NSString *)newValue forselector:(NSString *)selector {
    
    if(self.timeDelegate != nil) {
        if([self.timeDelegate respondsToSelector:@selector(changeValuesOnTimer:ForTheSelector:)]) {
             [self.timeDelegate changeValuesOnTimer:newValue ForTheSelector:selector];
            if([selector isEqualToString:kisCenterTime]) {
                NSDictionary *dict = @{@"Time" : newValue , @"selector" : selector };
                 [[NSNotificationCenter defaultCenter] postNotificationName:kDashboardNotification object:nil userInfo:dict];
            }
        }
    } else {
        if([selector isEqualToString:kisCenterTime]) {
            NSDictionary *dict = @{@"Time" : newValue , @"selector" : selector };
            [[NSNotificationCenter defaultCenter] postNotificationName:kDashboardNotification object:nil userInfo:dict];
        }
    }

}

//---------------------------------------------------------------------------------------------------------------

-(void)changeImageWithSelector:(NSString *)selector {
    
    if(self.timeDelegate != nil) {
        if([self.timeDelegate respondsToSelector:@selector(changePhaseOfTimerWithSelector:)]) {
            [self.timeDelegate changePhaseOfTimerWithSelector:selector];
            NSDictionary *dict = @{@"selector" : selector };
            [[NSNotificationCenter defaultCenter] postNotificationName:kphaseImageNotification object:nil userInfo:dict];
        }
    } else {
         NSDictionary *dict = @{@"selector" : selector };
        [[NSNotificationCenter defaultCenter] postNotificationName:kphaseImageNotification object:nil userInfo:dict];

    }
    
}

//---------------------------------------------------------------------------------------------------------------

-(void)timerFinishedRunning {
    
    if(self.timeDelegate != nil) {
        if([self.timeDelegate respondsToSelector:@selector(timerTimeCompleteResetValues)]) {
            [self.timeDelegate timerTimeCompleteResetValues];
                
            NSDictionary *dict = @{@"Time" : @"00:00:" , @"selector" : kisWork };
            [[NSNotificationCenter defaultCenter] postNotificationName:kDashboardNotification object:nil userInfo:dict];
            
            NSDictionary *dict2 = @{@"selector" : @"" };
            [[NSNotificationCenter defaultCenter] postNotificationName:kphaseImageNotification object:nil userInfo:dict2];
            
        }
    }  else {
        
        NSDictionary *dict = @{@"Time" : @"00:00:" , @"selector" : kisWork };
        [[NSNotificationCenter defaultCenter] postNotificationName:kDashboardNotification object:nil userInfo:dict];
        
        NSDictionary *dict2 = @{@"selector" : @"" };
        [[NSNotificationCenter defaultCenter] postNotificationName:kphaseImageNotification object:nil userInfo:dict2];
        
    }
}


//---------------------------------------------------------------------------------------------------------------

-(void)setBlinkImage {
    
    if([AppDelegate sharedInstance].appIsTimerRunning) {
        [self performSelector:@selector(setBlinkImage) withObject:nil afterDelay:0.5];
        if(self.intSelectedSet == 1) {
            // Work

            
            if(self.isBlinkImageSet) {
                self.isBlinkImageSet = FALSE;
                [self.timeDelegate setBlinkImagesFor:kisWork andType:self.isBlinkImageSet];
                
                NSDictionary *dict = @{@"selector" : kisWork , kisForBlink : [NSNumber numberWithBool:self.isBlinkImageSet] };
                [[NSNotificationCenter defaultCenter] postNotificationName:kblinkTimerNotification object:nil userInfo:dict];
                
            } else {
                
                self.isBlinkImageSet = TRUE;
                [self.timeDelegate setBlinkImagesFor:kisWork andType:self.isBlinkImageSet];
                
                NSDictionary *dict = @{@"selector" : kisWork , kisForBlink: [NSNumber numberWithBool:self.isBlinkImageSet] };
                [[NSNotificationCenter defaultCenter] postNotificationName:kblinkTimerNotification object:nil userInfo:dict];
                
            }
        }
        
        if (self.intSelectedSet == 2) {
            // Rest
            if(self.isBlinkImageSet) {
                self.isBlinkImageSet = FALSE;
                [self.timeDelegate setBlinkImagesFor:kisRest andType:self.isBlinkImageSet];
                
                NSDictionary *dict = @{@"selector" : kisRest , kisForBlink : [NSNumber numberWithBool:self.isBlinkImageSet] };
                [[NSNotificationCenter defaultCenter] postNotificationName:kblinkTimerNotification object:nil userInfo:dict];

                
            } else {
                self.isBlinkImageSet = TRUE;
                [self.timeDelegate setBlinkImagesFor:kisRest andType:self.isBlinkImageSet];
                
                NSDictionary *dict = @{@"selector" : kisRest , kisForBlink: [NSNumber numberWithBool:self.isBlinkImageSet] };
                [[NSNotificationCenter defaultCenter] postNotificationName:kblinkTimerNotification object:nil userInfo:dict];

                
            }
        }
        
        if (self.intSelectedSet == 3) {
            // Set Rest
            if(self.isBlinkImageSet) {
                
                self.isBlinkImageSet = FALSE;
                [self.timeDelegate setBlinkImagesFor:kisSetRest andType:self.isBlinkImageSet];
                
                NSDictionary *dict = @{@"selector" : kisSetRest , kisForBlink : [NSNumber numberWithBool:self.isBlinkImageSet] };
                [[NSNotificationCenter defaultCenter] postNotificationName:kblinkTimerNotification object:nil userInfo:dict];
                
            } else {
                self.isBlinkImageSet = TRUE;
                [self.timeDelegate setBlinkImagesFor:kisSetRest andType:self.isBlinkImageSet];
                
                NSDictionary *dict = @{@"selector" : kisSetRest , kisForBlink : [NSNumber numberWithBool:self.isBlinkImageSet] };
                [[NSNotificationCenter defaultCenter] postNotificationName:kblinkTimerNotification object:nil userInfo:dict];

            }
        }

    }

}


//---------------------------------------------------------------------------------------------------------------

+(TimerManager *) sharedInstance {
    
    if(timerManagerObject == nil) {
        timerManagerObject = [[TimerManager alloc]init];
    }
    return timerManagerObject;
}

@end
