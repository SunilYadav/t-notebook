//
//  AddNewSheetViewController.m
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "AddNewSheetViewController.h"

@interface AddNewSheetViewController ()
@property (nonatomic, unsafe_unretained) id<AddNewSheetDelegate>			delegate;

@end

@implementation AddNewSheetViewController

//--------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark custom methods

- (void) cancelAddSheet {
    [_delegate closeTagPopover];
}

//--------------------------------------------------------------------------------------------------------------

- (void) addSheet {
    NSString * strName = @"programsheetname";
    if ([strName length] == 0) {
        strName = @"";
    }
        [_delegate addNewSheet:strName];
}

//-----------------------------------------------------------------------------------------------------------------------------


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
