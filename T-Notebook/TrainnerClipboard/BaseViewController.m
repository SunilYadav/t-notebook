//
//  BaseViewController.h
//  
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import "BaseViewController.h"


@implementation BaseViewController


// -----------------------------------------------------------------------------------


#pragma mark -
#pragma mark Memory methods

// -----------------------------------------------------------------------------------

- (void)dealloc {
    
    // NSLog(@"Base dealloc");
    
}

//---------------------------------------------------------------------------------------------------------------

#pragma mark - Navigation-Bar Method

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(void)setNavigationBarButton
{
    NSLog(@"setNavigationBarButton --> start");
   
        UIButton * btnRigth = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnRigth setFrame:CGRectMake(0, 0, 35, 30)];
        [btnRigth setImage:[UIImage imageNamed:@"setting"] forState:UIControlStateNormal];
        UIBarButtonItem * btnRight = [[UIBarButtonItem alloc] initWithCustomView:btnRigth];
        [self.navigationItem setRightBarButtonItem:btnRight];
        
        UIButton * btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnLeft setFrame:CGRectMake(0, 0, 50, 44)];
        [btnLeft setImage:[UIImage imageNamed:@"Help-btn"] forState:UIControlStateNormal];
        UIBarButtonItem * btnBarLeft = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -20;
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, btnBarLeft, nil] animated:NO];
    
        NSLog(@"setNavigationBarButton --> end");
 
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-(BOOL)prefersStatusBarHidden
{
    return NO;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)NavBarBtnTap:(id)sender{
    
}

//-----------------------------------------------------------------------

-(void)addNavBarButtons{
    
    self.navigationController.navigationBarHidden = NO;
    UIButton * btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"close_pop_up"] forState:UIControlStateNormal];
    btnBack.layer.cornerRadius = 4.0;
    [btnBack addTarget:self action:@selector(NavBarBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setTag:201];
    [btnBack setFrame:CGRectMake(0, 5, 34,34)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -15;
    UIBarButtonItem *leftBtn =  [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, leftBtn, nil] animated:NO];
    
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#pragma mark -
#pragma mark UIView lifeCycel

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    [super viewDidLoad];
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBarHidden = YES;
  
}




@end
