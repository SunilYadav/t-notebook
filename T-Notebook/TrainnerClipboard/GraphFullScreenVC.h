//
//  GraphFullScreenVC.h
//  T-Notebook
//
//  Created by WLI.
//  Copyright (c) 2014 WLI All rights reserved.
//

#import <UIKit/UIKit.h>
#import  <MessageUI/MessageUI.h>

@interface GraphFullScreenVC : UIViewController<MFMailComposeViewControllerDelegate>{
    
}
@property (nonatomic, strong) NSString                              * strClientID;
@property (nonatomic, strong) NSNumber                              * chartTag;
@property (nonatomic, strong) NSString                               * strAxies;


@property(nonatomic,readwrite) BOOL isFromTermsAndConditions;
@property(nonatomic,strong) UIImage *imgSignature;
@property (nonatomic, strong) NSString                               * strName;
@property (nonatomic , strong) NSString                              *strEmail;
@property (nonatomic) BOOL                                           isLandscapeMode;
@end
