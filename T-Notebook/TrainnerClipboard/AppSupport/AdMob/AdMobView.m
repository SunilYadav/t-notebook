//
//  AdMobView.m
//  TrainnerClipboard
//
//  Created by WLI.
//
//

#import "AdMobView.h"

@implementation AdMobView

@synthesize delegate;
@synthesize bannerView_;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //CGSize addSize = CGSizeFromGADAdSize(kGADAdSizeSmartBannerPortrait);
        CGSize customSize = CGSizeMake(730, 90);
        GADAdSize addSize = GADAdSizeFromCGSize(customSize);
        self.bannerView_ = [[GADBannerView alloc] initWithFrame:CGRectMake(0.0, 40, addSize.size.width, addSize.size.height)];
        
        self.bannerView_.adUnitID = ADMOB_ID;
        UINavigationController *navCtrol = (UINavigationController*)([AppDelegate sharedInstance].window.rootViewController);
        UIViewController* root = (UIViewController*)([[navCtrol viewControllers] firstObject]);
        self.bannerView_.rootViewController = root;
        self.bannerView_.delegate = (id)root;
        [self addSubview:bannerView_];
        
        GADRequest *r = [[GADRequest alloc] init];
       // r.testing = YES;
        [bannerView_ loadRequest:r];
        
        self.btnRemoveAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnRemoveAdd.hidden = TRUE;
        [self.btnRemoveAdd setFrame:CGRectMake(addSize.size.width - 15, 30, 25, 25)];
        [self.btnRemoveAdd addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.btnRemoveAdd.backgroundColor = [UIColor clearColor];
        [self.btnRemoveAdd setImage:[UIImage imageNamed:@"Ad_Delete"] forState:UIControlStateNormal];
        [self addSubview:self.btnRemoveAdd];
    }
    return self;
}
//---------------------------------------------------------------------------------------------------------------'


//-----------------------------------------------------------------------

-(void)cancelClicked:(id)sender{
    [self.delegate cancelButtonClicked];
}


@end
