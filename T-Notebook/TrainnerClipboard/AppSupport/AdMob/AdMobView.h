//
//  AdMobView.h
//  TrainnerClipboard
//
//  Created by WLI.
//
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"


@protocol adViewDelegate;

@interface AdMobView : UIView{
    GADBannerView *bannerView_;
   __unsafe_unretained  id<adViewDelegate>  delegate;
    
    //__unsafe_unretained id<adViewDelegate> delegate;
   // __unsafe_unretained id <adViewDelegate> delegate;
}
@property (nonatomic, unsafe_unretained) id<adViewDelegate> delegate;
@property(nonatomic,strong) UIButton               *btnRemoveAdd;
@property(nonatomic,strong) GADBannerView  *bannerView_;


@end

@protocol adViewDelegate<NSObject>
@optional
- (void)cancelButtonClicked;
@end
