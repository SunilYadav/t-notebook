//
//  CHLogs.h
//  LimitlessMax
//
//  Created by Sunil on 12/04/15.
//  Copyright (c) 2014 ChromeInfotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Logs : NSObject

//#define  LOGS_ON //Use for Logs

#ifdef   LOGS_ON

    #define  START_METHOD           NSLog(@"\n\n %s Method Start",__FUNCTION__);
    #define  END_METHOD             NSLog(@"\n\n%s Method End  ",__FUNCTION__);

    //Message without Line number
    //  #define MESSAGE(s, ...)         NSLog( @"[%@ %@] %@",NSStringFromClass([self class]), NSStringFromSelector(_cmd),[NSString stringWithFormat:(s), ##__VA_ARGS__] )

    //Message with line number
    #define MESSAGE(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#else
    #define  START_METHOD           NSLog(@"");
    #define  END_METHOD             NSLog(@"");
    #define MESSAGE(...)

#endif

//Log for use condition YES/NO
#define  LOG_ON                   false


@end
